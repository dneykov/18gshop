<?php
require_once 'main.php';

if(isset($_POST["sendmail"])){
	$errors[] = NULL;
	$err = false;
	if($_POST['mail'] == "") {
		$errors[] = "Моля попълнете вашия Е-майл";
		$err = true;
		$_SESSION['passstatus'] = "error1";
		header("Location: ".url."fancy_login.php?p=forgotpass");
	} 
	if(!$err) {

		$maill = $_POST['mail'];
    	$maill = trim($maill);

		$stm = $pdo->prepare('SELECT * FROM `members` WHERE `mail` = ? LIMIT 1');
	    $stm -> bindValue(1, $maill, PDO::PARAM_STR);
	    $stm -> execute();

	    if($stm->rowCount() == 0 ){
	    	$errors[] = "Няма потребител с този Е-майл";
	    	$_SESSION['passstatus'] = "error2";
	    	header("Location: ".url."fancy_login.php?p=forgotpass");
    	}
	    else {

	    	$row_member = $stm -> fetch();
		    $rand= rand(1, 9999999);
		    $key = sha1("forgotpass".$rand);

		    $stm = $pdo->prepare('INSERT INTO `forgot_pass`(`id`, `user_mail`, `key`) VALUES ("",?,?)');
		    $stm -> bindValue(1, $maill, PDO::PARAM_STR);
		    $stm -> bindValue(2, $key, PDO::PARAM_STR);
		    $stm -> execute();

		    $link = url."forgot_pass_change.php?submitpass=true&mail=".$maill."&key=".$key;

			$mail = new PHPMailer();

			$mail->Host = "localhost";
			$mail->CharSet="utf-8";

			$mail->From = "noreply@18gshop.com";
			$mail->FromName = "";
			$mail->AddAddress($maill);
			$mail->WordWrap = 99999;
			$mail->IsHTML(TRUE);  

			$mail->Subject = "Забравена парола";

			$text = "Здравейте,<br />";
			$text .= "Това е автоматично съобщение, което ще Ви позволи промяна на паролата.<br />";
			$text .= "За да влезете в профила си моля въведете новата си парола <a href='".$link."'> тук </a><br /><br /><br />";
			$text .= "Екипът на 18gshop<br />";

			$mail->Body    = $text;
			$mail->AltBody = $text;

			if(!$mail->Send())
			{
			    echo "Възникна грешка, моля опитайте оново";

			}

			$_SESSION['passstatus'] = "successsend";
			header("Location: ".url."fancy_login.php?p=forgotpass");
		}
    
    }
	
}

if(isset($_POST["updatepass"])){
	$errors[] = NULL;
	$err = false;
	if(!isset($_POST['pass']) || !isset($_POST['passagain'])) {
		$errors[] = "Моля попълнете всички полета";
		$err = true;
	} else {

		if(($_POST['pass']) !== ($_POST['passagain'])) {
			$errors[] = "Паролите не съвпадат";
			$err = true;
		}

		if(!$err) {

			$pass = $_POST['pass'];
	    	$pass = trim($pass);
	    	$pass = sha1($pass);
	    	$mail = $_POST['mail'];
	    	$mail = trim($mail);
	    	$key = $_POST['key'];
	    	$key = trim($key);

			$stm = $pdo->prepare('SELECT * FROM `forgot_pass` WHERE `user_mail` = ?  AND `key`= ? LIMIT 1');
		    $stm -> bindValue(1, $mail, PDO::PARAM_STR);
		    $stm -> bindValue(2, $key, PDO::PARAM_STR);
		    $stm -> execute();

		    if($stm->rowCount() == 0 ) $errors[] = "Възникна Грешка";
		    else {

			    $stm = $pdo->prepare('UPDATE `members` SET `password` = ? WHERE `mail` = ? LIMIT 1');
			    $stm -> bindValue(1, $pass, PDO::PARAM_STR);
			    $stm -> bindValue(2, $mail, PDO::PARAM_STR);
			    $stm -> execute();

			   	$stm = $pdo->prepare('DELETE FROM `forgot_pass` WHERE `user_mail` = ?  AND `key`= ? LIMIT 1');
			    $stm -> bindValue(1, $mail, PDO::PARAM_STR);
			    $stm -> bindValue(2, $key, PDO::PARAM_STR);
			    $stm -> execute();

				$updatesuccess = true;
			}
    	}
	}
}
