<?php
$artikul = new artikul_admin((int)$_GET['id']);

$kategoriq = new kategoriq((int)$artikul->getId_kategoriq());

$vid = new vid((int)$artikul->getId_vid());


$model = new model((int)$artikul->getId_model());

$etiket = NULL;

if ($artikul->getEtiketizapisi()) foreach ($artikul->getEtiketizapisi() as $v) {

    $etiket[] = $v->getId_etiket();

}

$artikul_drugi = NULL;

$stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE ( `id_vid` = ? AND `id` != ? ) ORDER BY RAND() LIMIT 8');
$stm->bindValue(1, $artikul->getId_vid(), PDO::PARAM_INT);
$stm->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
$stm->execute();


if ($stm->rowCount()) foreach ($stm->fetchAll() as $v) {
    $artikul_drugi[] = new artikul($v);
}


// ------------------------------Start Last Seen----------------------------------------
$lastSeen = array();

if (isset($_COOKIE["LastSeen"])) {
    $lastSeen = unserialize(stripslashes(rawurldecode($_COOKIE["LastSeen"])));
}

if (is_array($lastSeen)) {
    foreach ($lastSeen as $id) {
        if (isProductAvailable((int)$id)) {
            $lastSeenProducts[] = new artikul((int)$id);
        }
    }

    array_unshift($lastSeen, $artikul->getId());

    $lastSeen = array_unique($lastSeen);

    array_splice($lastSeen, 14);

    setcookie("LastSeen", serialize($lastSeen), strtotime('+360 days'), '/');
}

// ------------------------------End Last Seen----------------------------------------
