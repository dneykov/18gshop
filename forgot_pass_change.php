<?php
require_once 'main.php';
if(isset($_POST["updatepass"])){
	$errors[] = NULL;
	$err = false;
	if($_POST['pass'] == "" || $_POST['passagain'] == "") {
		$errors[] = "Моля попълнете всички полета";
		$err = true;
	} else {

		if(($_POST['pass']) !== ($_POST['passagain'])) {
			$errors[] = "Паролите не съвпадат";
			$err = true;
		}

		if(!$err) {

			$pass = $_POST['pass'];
	    	$pass = trim($pass);
	    	$pass = sha1($pass);
	    	$mail = $_POST['mail'];
	    	$mail = trim($mail);
	    	$key = $_POST['key'];
	    	$key = trim($key);

			$stm = $pdo->prepare('SELECT * FROM `forgot_pass` WHERE `user_mail` = ?  AND `key`= ? LIMIT 1');
		    $stm -> bindValue(1, $mail, PDO::PARAM_STR);
		    $stm -> bindValue(2, $key, PDO::PARAM_STR);
		    $stm -> execute();

		    if($stm->rowCount() == 0 ) $errors[] = "Линка за смяна на парола е вече използван";
		    else {

			    $stm = $pdo->prepare('UPDATE `members` SET `password` = ? WHERE `mail` = ? LIMIT 1');
			    $stm -> bindValue(1, $pass, PDO::PARAM_STR);
			    $stm -> bindValue(2, $mail, PDO::PARAM_STR);
			    $stm -> execute();

			   	$stm = $pdo->prepare('DELETE FROM `forgot_pass` WHERE `user_mail` = ?  AND `key`= ? LIMIT 1');
			    $stm -> bindValue(1, $mail, PDO::PARAM_STR);
			    $stm -> bindValue(2, $key, PDO::PARAM_STR);
			    $stm -> execute();

				$updatesuccess = true;
			}
    	}
	}
}
require dir_root_template."forgot_pass_change.php";
?>