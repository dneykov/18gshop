<html>
	<head>
		<link rel="stylesheet" href="<?php echo url_template_folder; ?>css/css3.css" type="text/css" >
		<script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<style type="text/css">
			h3{
				color:#4d4d4d;
				font-size:16pt;
				margin:10px 10px 0;
				font-weight: normal;
				text-align:left;
			}
			.title{
				color:#4d4d4d;
				font-size:9pt;
				margin:0 10px;
				text-align:left;
			}
			.title a{
				color:#4d4d4d;
				font-weight: bold;
				font-size:12pt;
			}
			.submit {
				background: none repeat scroll 0 0 #FF671F;
				border: medium none;
				color: #fff;
				cursor: pointer;
				float: right;
				margin: 34px 0 0;
				padding: 0 8px 2px;
				text-decoration: none;
			}
			.error{
				color: #FE0000;
				font-size:9pt;
				margin:10px;
				clear:both;
			}
			input, textarea {
				font-size: 13px;
			}
			input:hover, input:focus {
				border: 1px solid #FF671F;
			}
		</style>
	</head>
	<body>
		<ul id="main_menu">
			<li><a <?php if(isset($_GET['p'])&&$_GET['p']=='howtobuy') echo 'class="active"'; ?> href="<?php echo url.'fancy_qbs_group.php?p=howtobuy'; if(isset($group)) echo '&id='.$group->getId();?>">Как да поръчам</a></li><?php
				if(isset($group)){
					$tmp_class='';
					if(isset($_GET['p'])&&$_GET['p']=='sendquestion') $tmp_class='class="active"';
					echo '<li><a '.$tmp_class.' href="'.url.'fancy_qbs_group.php'.'?id='.$group->getID().'&p=sendquestion" >Въпрос</a></li>';
				}
			?>					
		    <div style="height:3px;width: 100%;background: #FF671F;"></div>
		</ul>
		<div style="height:3px;color:#fcc900;background:#fcc900;"></div>
		<div class="main" style="padding-left: 25px;">
			<?php
				$page=$_GET['p'];
				switch ($page) {
					case "sendquestion":
						require 'form_send_question_group.php';
						break;
					case "howtobuy":
						require 'howtobuy.php';
						break;
				}
			?>
		</div>
	</body>
</html>


