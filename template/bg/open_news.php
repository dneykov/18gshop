<?php
function _is_youtube($url)
{
    return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url) || preg_match('/youtube\.com\/embed/i', $url) || preg_match('/youtube\.com\/v/i', $url));
}
function _is_vimeo($url)
{
    return (preg_match('/vimeo\.com/i', $url));
}
function youtube_id_from_url($url) {
    $pattern =
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
    ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}
if(isset($_GET['newid'])){
    $new_id = (int)$_GET['newid'];
    $new = new news($new_id);
?>
<div class="product_navigation">
	<?php
		$__url=url.'index.php';
		echo '<a href="'.$__url.'">' . $pageTitle . '</a>';
		echo ' - <a href="'.$__url.'?news">новини</a>';
                echo ' - <a href="#">'.$new->getTitle().'</a>';
	?>
</div>
</center>
<div style="clear: both; height: 20px;"></div>
<style>
    #news_box {
        width: 1000px;
        margin: 0 auto;
    }
    #news_img {
        width: 480px;
        border-right: 1px solid #CCC;
        float: left;
        display: inline-block;
    }
    #news_info {
        margin-left: 30px;
        width: 450px;
        float: left;
        display: inline-block;
    }
</style>
<div id="news_box">
    <div id="news_img">
            <?php
                if($new->getType()=='news'){
                        echo '<a TARGET="_blank" href="'.$new->getUrl().'" ><img style="width:450px;" src="'.url.$new->getImage_Original().'"/></a>';
                }else{
                    if(_is_vimeo($new->getImage())){
                        $src = str_replace('vimeo.com/','player.vimeo.com/video/',$new->getImage());
                        echo '<iframe style="width:450px; height:350px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
                    } elseif(_is_youtube($new->getImage())) {
                        $src = str_replace('watch?v=','embed/',$new->getImage());

                        echo '<iframe style="width:450px; height: 350px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
                    }
                        //echo '<iframe style="width:450px; height: 340px;" src="'.$new->getImage().'" frameborder="0"></iframe>';
                }
            ?>
        <?php echo '<div style="float: right; padding:20px 0px 20px 0px; margin-right: 30px;">
                <span style="font-size: 11px; color: #333;" class="share-link">сподели</span>
                &nbsp;&nbsp;
                <a target="_blank" class="share-link" href="https://www.facebook.com/sharer.php?u='.curPageURL().'">
                <img src="'.url.'images/icons/share_facebook.png" alt="Facebook" border="0" style="vertical-align:text-top;position:relative;margin-top: -2px;" />
                </a>
                &nbsp;&nbsp;
                <a target="_blank" class="share-link" href="https://twitter.com/home?status='.urlencode($new->getTitle()).'+'.curPageURL().'">
                <img src="'.url.'images/icons/share_twitter.png" alt="Twitter" border="0" style="vertical-align:text-top;margin-top: -2px;" />
                </a>
                &nbsp;&nbsp;
                <a target="_blank" class="share-link" href="https://plus.google.com/share?url='.curPageURL().'">
                <img src="'.url.'images/icons/share_google.png" alt="Google+" border="0" style="vertical-align:text-top;position:relative;margin-top: -2px;" />
                </a>
                &nbsp;&nbsp;
                <a target="_blank" class="share-link" href="https://pinterest.com/pin/create/button/?url='.curPageURL().'&amp;media='.url.$new->getImage_b().'&amp;description='.$new->getTitle().'">
                <img src="'.url.'images/icons/share_pinterest.png" alt="Pin it!" border="0" style="vertical-align:text-top;position:relative;margin-top: -2px;" />
                </a>

                </div>';
        ?>
    </div>
    <div id="news_info">
        <?php $date = new DateTime($new->getDate()); ?>
        <span><a TARGET="_blank" href="<?php echo $new->getUrl(); ?>"><h3 style="margin-top:0px;display:inline;"><?php echo $new->getTitle(); ?></h3></a></span>
        <span class="date" style="font-size:10pt;color:#b2b2b2;"><?php echo date_format($date, 'd-m-Y'); ?></span><div style="clear:both; height: 20px;"></div>
        <?php echo $new->getText(); ?>
    </div>
</div>
<div style="clear: both;"></div>
<center>
    <div style="width: 1020px; border-top: 1px solid #CCC; margin-top: 30px; border-bottom: 1px solid #CCC;">
        <?php
        }
        $stm = $pdo->prepare('SELECT * FROM `news` ORDER BY `id` DESC');
        $stm -> execute();
        $a = $stm->fetchAll();
        foreach($a as $n){
            $image = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $n['image']);
            echo '<a href="'.$__url.'?newid='.$n['id'].'"><div style="display: inline-block; width: 320px; margin-left: 10px; margin-top: 30px; margin-bottom: 20px; margin-right: 10px; margin-bottom: 10px; border-bottom: 1px solid #ccc;">';
                    $date = new DateTime($n['news_date']);
                    echo '<span class="date" style="float: left; margin-top: 5px; margin-bottom: 5px; font-size:10pt;color:#b2b2b2;">'.date_format($date, 'd-m-Y').'</span><div style="clear:both;"></div>';
                    if($n['Type']=='news'){
                            echo '<img style="max-width:400px; max-height:300px;margin:0px 20px 10px 0px; float: left;" src="'.url.$image.'"/>';
                    }else{
                        if(_is_vimeo($n['image'])){
                            $link = str_replace('https://vimeo.com/', 'https://vimeo.com/api/v2/video/', $n['image']) . '.php';

                            $html_returned = unserialize(file_get_contents($link));

                            $thumb_url = $html_returned[0]['thumbnail_large'];
                            echo '<img style="width:320px;margin:0px 20px 10px 0px; float: left;" src="'.$thumb_url.'"/>';

                        } elseif(_is_youtube($n['image'])) {
                            $src = str_replace('watch?v=','embed/',$n['image']);
                            echo '<img style="max-width:320px; max-height:225px;margin:0px 20px 10px 0px; float: left;" src="https://img.youtube.com/vi/'.youtube_id_from_url($n['image']).'/0.jpg"/>';
                        }
                    }
                    echo '<span style="font-size: 16px; float: left; min-height: 40px;"> '.$n['title_'.lang_prefix].'</span>';
           echo '</div></a>';
        }
        ?>
    </div>
    <div style="clear: both; height: 40px;"></div>
