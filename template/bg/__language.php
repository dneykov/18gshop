<?php

define('lang_login_error_pass_or_mail', 'Моля въведете валидна е-поща или парола!');

define('lang_login_register_error_mail_lenght_long', 'прекалено дълга е-поща');
define('lang_login_register_error_mail_invalid', 'невалидна е-поща');
define('lang_login_register_error_mail_already_used', 'имейл адреса mail@mail.com вече е регисттиран, моля въведете друг имейл адрес');

define('lang_login_register_error_pass_lenght_short', 'паролата да е повече от 4 знака');
define('lang_login_register_error_pass_lenght_long', 'паролата < 200 знака');

define('lang_login_register_error_name_last_lenght_short', 'фамилията трябва да е поне 3 символа');
define('lang_login_register_error_name_last_lenght_long', 'фамилията трябва да е до 200 символа');
define('lang_login_register_error_name_first_lenght_short', 'името трябва да е поне 3 символа');
define('lang_login_register_error_name_first_lenght_long', 'името трябва да е до 200 символа');

define('lang_login_register_complete', 'zapisana reg');

define('lang_login_logout', 'изход');
define('lang_login_login', 'вход');

define('lang_error_passwords_not_match', 'грешно потвърждение на парола');

define('lang_login_register_error_terms', 'общите условия трябва да бъдат приети');
define('lang_login_register_error_privacy', 'обработката на личните данни трябва да бъде потвърдена');

define('lang_order_address_data_error_gsm', 'невалиден мобилен телефон');
define('lang_order_address_data_error_home_telephone', 'невалиден домашен телефон');
define('lang_order_address_data_error_city', 'невалиден град');
define('lang_order_address_data_error_street', 'невалидена улица');
define('lang_order_address_data_error_number', 'невалиден номер на улица');
define('lang_order_address_data_error_vhod', 'невалиден вход');
define('lang_order_address_data_error_etaj', 'невалиден етаж');
define('lang_order_address_data_error_apartament', 'невалиден апартамент');

define('lang_success_save', 'промените бяха запазени');

define('lang_order_success_send', 'поръчката беше изпратена успешно');
define('lang_currency_append', 'лв.');
define('lang_currency_prepend', '');
define('lang_order_success_send_email', 'Информация за поръчката е изпратена на ');
define('lang_currency_coef', 1);

define('lang_order_comment_text', "Данни за фактура или друга допълнителна информация свързана с поръчката.");
define('lang_order_subject', 'Приета поръчка от ');
define('lang_order_approved', 'Одобрена поръчка от ');
define('lang_order_approved_msg1', 'Вашата поръчка е одобрена и ще бъде доставена.');
define('lang_order_approved_msg2', 'Като важен клиент за нас ви предлагаме специално подбрани продукти към вашия избор.');
define('lang_order_msg1', "Здравейте,преди изпълнение на поръчката ви ще се свържем с вас за потвърждение. Статуса на поръчката си може лесно да следите във вашия профил, благодарим ви че пазарувате при нас !");
define('lang_from', 'от');
define('lang_order_msg_nom', "Номерът на Вашата поръчка е: ");
define('lang_order_msg1_1', "Можете да проследявате нейния статус във Вашия профил.");
define('lang_order_msg_kod', "Код");
define('lang_order_msg_art', "Артикул");
define('lang_order_msg_opt', "Опции");
define('lang_order_msg_br', "Брой");
define('lang_order_msg_pr', "Цена");
define('lang_order_msg_preorder', "Остатък*");
define('lang_order_msg_deposit', "Аванс*");
define('lang_order_msg_total', "Общо");
define('lang_order_msg_total2', "Всичко:");
define('lang_free_shipping', "Безплатна доставка");
define('lang_order_deliv', "Доставка");
define('lang_order_delivery', "доставка:");
define('with_free_delivery', "с безплатна доставка");
define('lang_order_tmp_cash', "Поръчката ще бъде доставена на посочения от вас адрес. Плащането на поръчаният продукт, ще бъде извършено с куриерската фирма. ");
define('lang_order_msg3_cash', "Моля свържете се с нас ако има забавяне или нередности в пратката.");
define('lang_order_tmp_bank', "Начин на плащане: Паричен превод (превеждате общата сума, включително стойността на доставката, на посочената банкова сметка)");
define('lang_order_tmp_bank_details', "Данни за извършване на превода:");
define('lang_order_msg3_bank', "Поръчката Ви ще бъде доставена до посочения от Вас адрес до два работни дни след получаване на плащането. При получаване на пратката, не дължите нищо на куриера.");
define('lang_order_preorde_desc', "* само за продукти с предварителна поръчка.");
define('lang_size', 'размер:');
define('cash_on_delivery', 'Наложен платеж');
define('bank_transfer', 'Паричен превод');
define('payment_type', 'Начин на плащане:');
define('iban', 'IBAN:');
define('bank_details', 'банкови детайли:');
define('lang_order_company_name_short', 'име на фирма трябва да е поне 3 символа');
define('lang_order_company_name_long', 'име на фирма не трябва да надвишава 200 символа');
define('lang_order_company_city_error', 'невалиден град на фирма');
define('lang_order_company_street_error', 'невалиден адрес на фирма');
define('lang_order_company_mol_error', 'невалиден М.О.Л');
define('lang_order_company_eik_vat_error', 'невалиден ЕИК или ЗДДС');
define('pickup_delivery', 'Вземете на място от нас');
define('lang_enduser_ref_price', 'Краен клиент:');
define('lang_in', 'в');