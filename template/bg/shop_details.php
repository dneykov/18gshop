<?php

$stm = $pdo->prepare('SELECT * FROM `zz_regioni_magazini` LIMIT 1');
$stm->execute();

$v = $stm->fetch();


if ($_POST) {
    if (isset($_POST['content']) && ($_POST['content'] != 'напишете вашия въпрос тук')) {
        $content = trim($_POST['content']);
        $err = null;
    } else {
        $content = '';
    }
    if (isset($_POST['name']) && ($_POST['name'] != 'име')) {
        $name = trim($_POST['name']);
        $err = null;
    } else {
        $name = '';
    }
    if (isset($_POST['email']) && ($_POST['email'] != 'е-поща или телефон') && (check_email_address($_POST['email']))) {
        $email = trim($_POST['email']);
        $err = null;
    } else {
        $email = '';
    }
    if (isset($_POST['subject']) && ($_POST['subject'] != 'заглавие')) {
        $subject = trim($_POST['subject']);
        $err = null;
    } else {
        $subject = '';
    }
    if (empty($content)) $err[] = 'въпрос';
    if (empty($name)) $err[] = 'име';
    if (empty($email)) $err[] = 'email';
    if (empty($subject)) $err[] = 'заглавие';
    if ($err === NULL) {
        $msg = "Запитване от " . $name . " " . $email . "<br/>";
        $msg = $msg . "Въпрос: " . htmlspecialchars($_POST['content']);


        $mail = new PHPMailer();
        $mail->Host = "localhost";  // specify main and backup server
        $mail->CharSet = "utf-8";

        $mail->From = mail_office;
        $mail->FromName = $name;
        $mail->AddAddress(mail_office);
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        $mail->IsHTML(TRUE);                                  // set email format to HTML

        $mail->Subject = $subject;
        $mail->Body = $msg;


        $sendRez = $mail->Send();
    }
}

?>
<div id="contactimages" class="jThumbnailScroller">
    <div class="jTscrollerContainer">
        <div class="jTscroller">
            <?php
            if (isset($images)) {
                foreach ($images as $img) {


                    echo '<a rel="fancybox-thumb" href="' . url . $img . '" ><img style="height:450px;" src="' . url . $img . '"/></a>';


                }
            }

            ?>
        </div>
    </div>
    <a class="jTscrollerPrevButton" href="#" style="display: none;"></a>
    <a class="jTscrollerNextButton" href="#"></a>
</div>
</center>
<script type="text/javascript">
    $(function () {
        $('#contactimages .jTscrollerContainer .jTscroller a').fancybox({

            prevEffect: 'fade',
            nextEffect: 'fade',
            nextSpeed: 700,
            prevSpeed: 700,
            helpers: {
                title: {
                    type: 'outside'
                },
                overlay: {
                    opacity: 0.8,
                    css: {
                        'background-color': '#000'
                    }
                },
                thumbs: {
                    width: 100,
                    height: 63
                }
            }
        });
    });
</script>
<div style="height:400px;width: 1280px; margin: 10px auto;">
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAPS_API_KEY; ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var geocoder = new google.maps.Geocoder();
            var center = new google.maps.LatLngBounds();
            geocoder.geocode({'address': '<?php echo $v['googleaddress']; ?>'}, function (res, stat) {
                if (stat == google.maps.GeocoderStatus.OK) {
                    // add the point to the LatLngBounds to get center point, add point to markers
                    center.extend(res[0].geometry.location);
                    var mapOptions = {
                        center: center.getCenter(),
                        zoom: 15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById("shop_<?php echo $v['id']; ?>"),
                        mapOptions);
                    var marker = new google.maps.Marker({
                        position: center.getCenter(),
                        map: map,
                        icon: '<?php echo url; ?>/images/logo_gm.png'
                    });
                }
            });


        });
    </script>
    <?php


    //echo '<img style="width:813px;height:500px;margin:0px" src="http://maps.google.com/maps/api/staticmap?center='.$v['googleaddress'].'&zoom=14&size=813x500&maptype=roadmap&markers=color:blue|label:A|'.$v['googleaddress'].'&sensor=false"/>';
    echo '<div style="float:right; width: 450px; margin:0 10px;text-align: left;"><div>' . nl2br(htmlspecialchars($v['address_' . lang_prefix])) . '</div>';
    echo '<div>' . nl2br(htmlspecialchars($v['info_' . lang_prefix])) . '</div></div>';

    echo '<div style="float: left; margin: 0 0 0 32px;overflow: hidden; height: 400px;width: 740px; transform: translateZ(0px); background-color: rgb(229, 227, 223);"  id="shop_' . $v['id'] . '"></div>';


    ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mailform input[name=name]').click(function () {
                $(this).val('');
            });
            $('#mailform input[name=email]').click(function () {
                $(this).val('');
            });
            $('#mailform input[name=subject]').click(function () {
                $(this).val('');
            });
            $('#mailform textarea').click(function () {
                $(this).text('');
            });
        });
    </script>
    <form id="mailform" name="mailform" action="" method="post" style="float:right;margin:30px 15px 0;clear:right;">
        <table>
            <tr>
                <td><input name="name" type="text" <?php if (isset($_POST['name'])) {
                        echo 'value="' . $_POST['name'] . '"';
                    } else {
                        if (isset($__user)) {
                            echo 'value="' . $__user->getName() . ' ' . $__user->getName_last() . '"';
                        }
                    } ?> placeholder="име"
                           style="font-size: 13px;width:200px;padding:0 0 0 3px;margin:5px 0;height: 26px;"/></td>
                <td align="right"><input name="email" type="text" <?php if (isset($_POST['email'])) {
                        echo 'value="' . $_POST['email'] . '"';
                    } else {
                        if (isset($__user)) {
                            echo 'value="' . $__user->getMail() . '"';
                        }
                    } ?> placeholder="е-поща или телефон"
                                         style="font-size: 13px;width:200px;padding:0 0 0 3px;margin:5px 0;height: 26px;"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input style="width:450px;padding:0 0 0 3px;margin:5px 0;font-size: 13px;height: 26px;"
                                       name="subject" type="text" <?php if (isset($_POST['subject'])) {
                        echo 'value="' . $_POST['subject'] . '"';
                    } ?> placeholder="заглавие"/></td>
            </tr>
            <tr>
                <td colspan="2"><textarea name="content"
                                          style="width:450px;height:150px;padding:0 0 0 3px;margin:5px 0;font-size: 13px;"
                                          placeholder="напишете вашия въпрос тук"><?php if (isset($_POST['content'])) {
                            echo $_POST['content'];
                        } ?></textarea></td>
            </tr>
        </table>
        <input value="изпрати" type="submit"
               style="display: block;cursor: pointer;background:#FF671F;color:#fff;width: 100px;height: 25px;padding-bottom:3px;float:right;margin-top:5px; margin-right: 2px; border: none;"/>
    </form>
    <?php
    if ((isset($sendRez)) && ($sendRez)) {
        echo '<div style="text-align: right;margin-right:10px;color:#FF671F; clear: both;font-size: 12px;">Съобщението е изпратено успешно.</div>';
    }
    if ((isset($err)) && (sizeof($err) > 0)) {
        $t = '';
        foreach ($err as $e) {
            $t .= ', ' . $e;

        }
        $t = trim($t, ', ');
        echo '<div style="text-align: right;margin-right:10px;color:#FF671F; clear: both;font-size: 12px;">Моля въведете ' . $t . '</div>';
    }
    ?>
    <div style="clear:both;"></div>
</div>

<div style="clear:both; height:40px;"></div>
</div>

