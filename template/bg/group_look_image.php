<?php
require '../../__top.php';

if(isset($_GET['id']) && isset($_GET['image'])){
    $group = new paket((int)$_GET['id']);
    $artikuli = $group->getProducts();
?>
<style>
    body {
        background-color: #FFF;
    }
    #leftCol {
        
        width: 200px;
        height: 100%;
        display: inline-block;
        float: left;
        margin-right: 20px;
    }
    #rightCol {
        float: left;
        width: -moz-calc(100% - 230px);
        width: -webkit-calc(100% - 230px);
        width: calc(100% - 230px);
        height: 100%;
        position: relative;
    }
    #wrapper{
        width: 100%;
        height: 100%;
        max-height: 100%;
        table-layout: fixed;
    }
    #wrapper tr {
        height: 100%;
    }
    #wrapper td {
        width: 100%;
        height: 100%;
        vertical-align: middle;
        text-align: center;
    }
    .viewport {
        position: relative;
        text-align: center;
        vertical-align: middle;
    }
    img.i
    {
        position: absolute;
        max-width: 100%;
        top: 10%;
        left: 10%;
    }
    img.i:empty
    {
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }
    div.i
    {
        position: absolute;
        max-width: 80%;
        top: 10%;
        left: 10%;
    }
    div.i:empty
    {
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }
    @media screen and (orientation: portrait) {
        img.i { max-width: 90%; }
        div.i { max-width: 90%; }
    }
    @media screen and (orientation: landscape) {
        img.i { max-height: 90%; }
        div.i { max-height: 90%; }
    }
    #artikul_zoom {
        display: none;
        z-index: 3;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-color: #FFF;
        cursor: move;
    }
    #zoom_box {
        width: 100%;
        height: 100%;
        overflow: hidden;
        margin: 0 auto;
    }
    #zoom_close {
        position: absolute;
        top: -6px;
        right: 15px;
        width: 24px;
        background: none;
        font-weight: bold;
        color: #000;
        height: 24px;
        line-height: 17px;
        text-align: center;
        cursor: pointer;
        font-size: 14px;
        z-index: 2;
    }
    #zoom_close img{
        width: 24px;
        height: 24px;
    }
    #zoom_open {
        display: none;
        position: absolute;
        top: -6px;
        right: 15px;
        width: 24px;
        background: none;
        font-weight: bold;
        color: #000;
        height: 24px;
        line-height: 17px;
        text-align: center;
        cursor: pointer;
        font-size: 14px;
        z-index: 1;
    }
    #zoom_open img{
        width: 24px;
        height: 24px;
    }
    #zoom_img_box{

    }
    img.canzoom {
        cursor: url(images/spyglass_z.cur),auto;
    }
    .viewport img{
        visibility: hidden;
    }
    #dvLoading {
        background:#FFF url(images/images/bx_loader.gif) no-repeat center center;
        height: 100%;
        width: 100%;
        z-index: 10000;
    }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo url; ?>js/jquery.kinetic.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('.viewport').each(function(){
          var parentHeight = $(this).parent().height();
          $(this).height(parentHeight);   
        });
        $('.viewport img').load(function(){
            $('#dvLoading').css({
                'display': 'none'
            });
            $('.viewport img').css({
                'visibility': 'visible'
            })
        });
        var nowImg = $('#get').val();
        var y = $('#'+ nowImg).height();
        var x = $('#'+ nowImg).width();
        $('#'+ nowImg).css({
            border : '1px solid #FF671F'
        });
        $('#zoom_box').kinetic();
        var screenh = $(window).height();
        var screenw = $(window).width();
        var img = new Image();
        img.src = $('.i').attr("src");
        img.onload = function() {
           var imgH = this.height,
               imgW = this.width;
           if((imgH > screenh)||(imgW > screenw)){
                $( ".i" ).attr( "class", "i canzoom");
                $( "#zoom_open" ).css( "display", "block");
           }
        }
    });
    function replaceImg(img, n){
        $('.viewport img').css({
            'visibility': 'hidden'
        })
        $('#dvLoading').css({
            'display': 'block'
        });
        $( "#artikul_zoom" ).css("display", "none");
        var nowImg = $('#get').val();
        var nowy = $('#'+ nowImg).height();
        var nowx = $('#'+ nowImg).width();
        var ny = $('#'+ n).height();
        var nx = $('#'+ n).width(); 
        $( ".i" ).replaceWith( "<img onclick='zoomImg(\""+img+"\")' class='i' src='"+img+"' />" );
        $("#zoom_open_link").replaceWith( "<img id='zoom_open_link' onclick='zoomImg(\""+img+"\")' src='../../images/nav/z_in.png' />" );
        var screenh = $(window).height();
        var screenw = $(window).width();
        var img = new Image();
        img.src = $('.i').attr("src");
        img.onload = function() {
            $('#dvLoading').css({
                'display': 'none'
            });
            $('.viewport img').css({
                'visibility': 'visible'
            })
            var imgH = this.height,
               imgW = this.width;
            if((imgH > screenh)||(imgW > screenw)){
                $( ".i" ).attr( "class", "i canzoom");
                $( "#zoom_open" ).css( "display", "block");
            }
            else {
                $( "#zoom_open" ).css( "display", "none");
            }
        }
        $('#'+ n).css({
            border : '1px solid #FF671F'
        });
        $('#'+ nowImg).css({
            border : '1px solid #FFFFFF'
        });
        $('#get').val( n );
    }
    function zoomImg(img){
        var screenh = $(window).height();
        var screenw = $(window).width();
        var imgg = new Image();
        imgg.src = $('.i').attr("src");
        imgg.onload = function() {
           var imgH = this.height,
               imgW = this.width;
           if((imgH > screenh)||(imgW > screenw)){
                $( "#artikul_zoom" ).css("display", "block");
                $( "#zoom_img" ).replaceWith( "<img id='zoom_img' name='zoom_img' src="+img+">");
                $( "#zoom_box" ).animate({ scrollTop: $( "#zoom_box" ).offset().top + ($( "#zoom_box" ).height() / 2)}, 10);
           }
        }
    }
    function closeZoomImg(){
        $( "#artikul_zoom" ).css("display", "none");
    }
    function replaceVideo(video, n){
        $( "#artikul_zoom" ).css("display", "none");
        $( "#zoom_open" ).css( "display", "none");
        var nowImg = $('#get').val();
        var text = '';
        text += "<div class='i'";
        text += "<object style='width: 90%; height: 80%;'>";
        text += "<param name='movie' value='"+video+"?version=3&amp;hl=bg_BG'></param>";
        text += "<param name='allowFullScreen' value='true'></param>";
        text += "<param name='allowscriptaccess' value='always'></param>";
        text += "<param name='quality' value='high'>";
        text += "<param name='wmode' value='window/opaque'>";
        text += "<param name='wmode' value='transparent'> ";
        text += "<param name='wmode' value='opaque' />";
        text += "<embed src='"+video+"?version=3&amp;hl=bg_BG' type='application/x-shockwave-flash' width='100%' height='100%' allowscriptaccess='always' allowfullscreen='true'></embed>";
        text += "</object>";
        text += "</div>";
        $( ".i" ).replaceWith( text );
        $('#'+ n).css('border', '1px solid #FF671F');
        $('#'+ nowImg).css('border', '1px solid #FFFFFF');
        $('#get').val( n );
    }
</script>
<input type="hidden" name="get" id="get" value="<?php echo $_GET['img']?>">
<div id="leftCol">
    <div style="height:50px"></div>
    <?php
    //echo '<img id="big" onclick="javascript: replaceImg(\''.url.$group->getKartinka_b().'\', \'big\')" style="display:none;border: 1px solid #FFFFFF; cursor: pointer;display: block; max-width: 95px; margin: 0 auto; margin-bottom: 20px;" src="'.url.$group->getKartinka_t().'"/>';
    $n = 1;
    foreach($artikuli as $ar){
        $artikul = new artikul((int)$ar);	
        echo '<img id="'.$n.'" onclick="javascript: replaceImg(\''.url.$artikul->getKartinka_b().'\', \''.$n.'\')" style="border: 1px solid #FFFFFF; cursor: pointer; display: block; max-width: 95px; margin: 0 auto; margin-bottom: 20px;" src="'.url.$artikul->getKartinka_t().'"/>';
        $n++;
    }
    ?>
</div>
<div id="rightCol">
   <div id="zoom_open"><img id="zoom_open_link" onclick="zoomImg('<?php echo url.$_GET["image"]; ?>')" src="<?php echo '../../images/nav/z_in.png';?>"></div>
    <div id="artikul_zoom"><div id="zoom_close" onclick="javascript: closeZoomImg();"><img src="<?php echo '../../images/nav/z_out.png';?>"></div><center><div id="zoom_box"><div id="zoom_img_box"><img id="zoom_img" name="zoom_img"/></div></div></center></div>
    <div class="viewport">
        <div id="dvLoading"></div>
        <?php
            if(isset($_GET['v'])){
                echo "<div class='i'";
                echo "<object style='width: 90%; height: 80%;'>";
                echo "<param name='movie' value='".$_GET['image']."?version=3&amp;hl=bg_BG'></param>";
                echo "<param name='allowFullScreen' value='true'></param>";
                echo "<param name='allowscriptaccess' value='always'></param>";
                echo "<param name='quality' value='high'>";
                echo "<param name='wmode' value='window/opaque'>";
                echo "<param name='wmode' value='transparent'> ";
                echo "<param name='wmode' value='opaque' />";
                echo "<embed src='".$_GET['image']."?version=3&amp;hl=bg_BG' type='application/x-shockwave-flash' width='100%' height='100%' allowscriptaccess='always' allowfullscreen='true'></embed>";
                echo "</object>";
                echo "</div>";
            }
            else {
        ?>
            <img onclick="zoomImg('<?php echo url.$_GET["image"]; ?>')" class="i" src="<?php echo url.$_GET['image']; ?>" alt="loading">
        <?php
            }
        ?>
    </div>
</div>
<?php
}
?>