<?php
$isAvaliable = $artikul->isAvaliable();
$isOnline = $artikul->isOnline();

$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :preorder LIMIT 1");
$stm->bindValue(':preorder', "preorder_" . lang_prefix, PDO::PARAM_STR);
$stm->execute();
$preOrderDb = $stm->fetch();
$preOrderConfirmationDescriptions = $preOrderDb['value'];

function _is_youtube($url)
{
    return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url) || preg_match('/youtube\.com\/embed/i', $url) || preg_match('/youtube\.com\/v/i', $url));
}

function _is_vimeo($url)
{
    return (preg_match('/vimeo\.com/i', $url));
}

function youtube_id_from_url($url)
{
    $result = preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}

?>
<style>
    .filter {
        display: none !important;
    }
</style>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.serialScroll-1.2.1.js"></script>

<div style="clear:both; margin-top:10px;">
    <div id="proddivdetail">
        <script type="text/javascript">
            function hideModal() {
                $(".modal").hide();
            }

            var addToCartRules = [];

            function validate(rules, preorder) {

                for (x in rules) {
                    if ($("#" + rules[x][0]).val() == null || $("#" + rules[x][0]).val() == "") {
                        $(".modal-text-inside").text('Моля изберете ' + rules[x][2] + ' на ' + rules[x][1]);
                        $(".modal").show();
                        return false;
                    }
                }

                if (preorder) {
                    showPreorderConfirmation();
                    return false;
                } else {
                    return true;
                }
            }

            function updatePriceBySize(el) {
                var additional_price = 0;
                var option = $(el).attr("id").split("_");
                var productID = option[1];
                var tagID = option[3];
                if (typeof tagID === "undefined") {
                    tagID = $("#price_tag").val();
                }

                $(".upgrade_option_prices").each(function (index, el) {
                    additional_price += parseInt($(el).val()) || 0;
                });

                $.ajax({
                    type: "POST",
                    url: "ajax/get_price_by_size.php",
                    data: {
                        product_id: productID,
                        tag_id: tagID,
                        additional_price: additional_price
                    },
                    cache: false,
                    success: function (html) {
                        var result = jQuery.parseJSON(html);
                        $("#item_price").html(result.price);
                        $(".delivery-box").html(result.delivery);
                        if (parseInt(result.quantity) > 0) {
                            $(".quantity .count").html(result.quantity);
                            $(".quantity").css({"visibility":"visible"});
                        } else {
                            $(".quantity").css({"visibility":"hidden"});
                        }
                        $('.warehouse-box .warehouse-code').html(result.code);
                        $('.warehouse-box').show();
                    }
                });
            }

            function showPreorderConfirmation() {
                $("#pre-order-dialog").dialog({
                    autoOpen: true,
                    title: "Предварителна поръчка",
                    modal: true,
                    width: 400,
                    resizable: false,
                    draggable: false,
                    position: {
                        my: "center",
                        at: "center",
                        of: window
                    },
                    buttons: {
                        "Добави в количката": function () {
                            $(".btnaddprod").submit();
                            $(this).dialog("destroy");
                        },
                        "Откажи": {
                            click: function () {
                                $(this).dialog("destroy");
                            },
                            text: 'Откажи',
                            class: 'btn-cancel'

                        }
                    },
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    }
                });

                return false;
            }

            $(document).ready(function () {

                //$("#collection_container").height(500 - $(".proddetailinfo").height() + $("#collection_container").height());
                $('.product_a_image').fancybox({
                    'type': 'iframe',
                    'margin': 37,
                    'width': 1920,
                    'height': 1200
                });
                $('.questionsBar div a').fancybox({
                    'type': 'iframe',
                    'padding': 0,
                    'width': 800,
                    'height': 600
                });
                $('#addtocart').ajaxForm({
                    success: function (responseText) {
                        $.fancybox({
                            'type': 'iframe',
                            'href': '<?php echo url ?>/fancy_login.php?p=cart',
                            'padding': 0,
                            'width': 800,
                            'height': 800
                        });
                        $('#main_menu a.cart').load('<?php echo url ?>/template/bg/top_menu_shopping_cart.php');
                    }
                });

                $('.questionsBar div:last').css('border', 'none');
                var discriptionh = $('.product_description').height();
                if (discriptionh > 100) {
                    $('.product_description').css({
                        position: 'relative',
                        height: '100px',
                        overflow: 'hidden'
                    });
                    $('.showmoredis').css({
                        display: 'block'
                    });
                }
                $('.showmoredis').click(function normalhdis() {
                    $('.product_description').css({
                        height: discriptionh + 25 + 'px',
                    });
                    $('.hidemoredis').css({
                        display: 'block'
                    });
                    $('.showoredis').css({
                        display: 'none'
                    });
                })
                $('.hidemoredis').click(function normalhdis() {
                    $('.product_description').css({
                        height: '100px',
                    });
                    $('.hidemoredis').css({
                        display: 'none'
                    });
                    $('.showoredis').css({
                        display: 'block'
                    });
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                })
            });

            function movetocenter(url, img, n) {
                image = new Image();
                image.src = img;
                image.onload = function () {
                    var width = image.width;
                    var nowImg = $('#nowcenterimg').val();
                    var nowy = $('#img' + nowImg).height();
                    var nowx = $('#img' + nowImg).width();
                    var ny = $('#img' + n).height();
                    var nx = $('#img' + n).width();
                    if (width > 320) {
                        var ImgClass = "class='item_img'";
                    } else {
                        var ImgClass = "class='item_img_small'";
                    }
                    var text = '<a id="proddetailscenterimg" class="product_a_image" title="" href="' + url + '"><img ' + ImgClass + ' alt style="max-width: ' + width + 'px; min-width: 50px;" src="' + img + '" /></a>';
                    $("#proddetailscenterimg").replaceWith(text);
                    $('#img' + n).css({
                        border: '1px solid #FF6720'
                    });
                    $('#img' + nowImg).css({
                        border: '1px solid #FFFFFF'
                    });
                    $('#nowcenterimg').val(n);
                };
            }
        </script>
        <style>
            #proddetailscenterimg a {
                cursor: url(<?php echo url; ?>images/spyglass_z.cur);
            }
        </style>
        <input type="hidden" name="nowcenterimg" id="nowcenterimg" value="big">
        <div class="proddetailimage">
            <?php
            $filename = $artikul->getKatinka();
            $width = 0;
            if (file_exists($filename)) {
                $size = getimagesize($artikul->getKatinka());
                $width = $size[0];
            }

            $uwsidth = $width;
            $art_images = $artikul->getKatinka_dopalnitelni(false);
            if (isset($art_images)) {
                for ($i = 1; $i <= 12; $i++) {
                    if (isset($art_images[$i])) {
                        $image = $art_images[$i];
                        if (!is_null($image) && file_exists($image->getKatinka())) {
                            $size = getimagesize($image->getKatinka());
                            $width = $size[0];
                            $uwsidth += $width + 10;
                        }
                    }
                }
            }

            $img = $artikul->getKatinka();
            if (file_exists($img)) {
                list($width, $height, $type, $attr) = getimagesize($img);
                if ($width > 320) {
                    $ImgClass = "class='item_img'";
                } else {
                    $ImgClass = "class='item_img_small'";
                }
            } else {
                $ImgClass = "class='item_img_small'";
            }
            ?>
            <?php
            $art_images = $artikul->getKatinka_dopalnitelni();
            $tmp = $artikul->getVideo();
            $has_art_images = false;

            if (isset($art_images) && (sizeof($art_images) > 0)) {
                foreach ($art_images as $image) {
                    if (isset($image)) $has_art_images = true;
                }
            }
            echo '<div id="proddetaildopimgbox" style="margin-right: 40px;">';

            $brand = $artikul->getBrand();

            ?>
            <a href="<?php echo url; ?>index.php?branch=<?php echo $brand->getIme(); ?>"><img
                        src="<?php echo $brand->getImage(); ?>" style="max-width:100px; margin-bottom: 20px;"/></a>
            <div style="clear: both;"></div>
            <?php
            if ($has_art_images) {
                echo '<div style="border: 1px solid #FF6720; margin-bottom: 20px;" id="imgbig"><a onclick="movetocenter(\'' . url_template_folder . 'look_image.php?id=' . $artikul->getId() . '&image=' . $artikul->getKartinka_b() . '&img=big\', \'' . $artikul->getKatinka() . '\', \'big\');return false;" href="#"><img class="prodimgtumb" style="max-width: 95px; margin-bottom: 0;" src="' . $artikul->getKartinka_t() . '"/></a></div>';
                $n = 1;

                foreach ($art_images as $key => $image) {
                    if (!is_null($image)) {
                        echo '<div style="border: 1px solid #FFFFFF; margin-bottom: 20px;" id="img' . $n . '"><a onclick="movetocenter(\'' . url_template_folder . 'look_image.php?id=' . $artikul->getId() . '&image=' . $image->getKartinka_b() . '&img=' . $n . '\', \'' . $image->getKatinka() . '\', \'' . $n . '\');return false;" href="#"><img class="prodimgtumb" style="max-width: 95px; margin-bottom: 0;" src="' . $image->getKartinka_t() . '"/></a></div>';
                        $n++;
                    }
                }
            }
            if (isset($tmp) && (!empty($tmp))) {
                $src = str_replace('youtube.com/v/', 'youtube.com/embed/', $artikul->getVideo());
                if (_is_vimeo($tmp)) {
                    $link = str_replace('https://vimeo.com/', 'https://vimeo.com/api/v2/video/', $tmp) . '.php';

                    $html_returned = unserialize(file_get_contents($link));

                    $thumb_url = $html_returned[0]['thumbnail_large']; ?>
                    <a href="<?php echo url_template_folder . 'look_image.php?id=' . $artikul->getId() . '&image=' . $src . '&v=yes&img=video'; ?>"
                       class="product_a_image"><img
                                style="display: block; max-width: 100px; margin: 0 auto; margin-bottom: 20px;"
                                src="<?php echo $thumb_url; ?>"/></a>
                    <?php
                } else if (_is_youtube($tmp)) { ?>
                    <a href="<?php echo url_template_folder . 'look_image.php?id=' . $artikul->getId() . '&image=' . $src . '&v=yes&img=video'; ?>"
                       class="product_a_image"><img
                                style="display: block; max-width: 100px; margin: 0 auto; margin-bottom: 20px;"
                                src="https://img.youtube.com/vi/<?php echo youtube_id_from_url($tmp); ?>/0.jpg"/></a>
                    <?php
                }
            }
            echo '</div>';
            echo '<div id="proddetailbigimgbox" style="min-width: 50px;">';
            ?>
            <a id="proddetailscenterimg" class="product_a_image"
               href="<?php echo url_template_folder . 'look_image.php?id=' . $artikul->getId() . '&image=' . $artikul->getKartinka_b(); ?>&img=big"><img <?php echo $ImgClass; ?>
                        style="max-width: <?php echo $width; ?>px; min-width: 50px;"
                        src="<?php echo $artikul->getKatinka(); ?>"/></a>
            <div style="clear: both; height: 10px;"></div>
            <?php
            $dop_artikuli = $artikul->getAddArtikuli();
            echo '</div>';
            ?>
            <?php if ($artikul->getDescription()) : ?>
                <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-bottom: 15px;"></div>
            <?php endif; ?>
            <div class="product_description"><?php echo nl2br($artikul->getDescription()); ?>
                <a href="javascript: void(0);" class="showmoredis" style="color: rgb(128, 128, 128);">прочети още <img
                            src="<?php echo url . "/images//nav/showmore.png"; ?>"></a>
                <a href="javascript: void(0);" class="hidemoredis" style="color: rgb(128, 128, 128);">скрий</a>
            </div>
        </div>
        <?php
        $brand = $artikul->getBrand();
        ?>
        <div class="proddetailinfo">
            <h3 class="prodinfoname">
                <div style="font-size: 13px; color: #999999; font-weight: normal; margin-bottom: 3px; margin-top: -3px;"><?php echo $artikul->getTitle(); ?>
                    &nbsp;<div style="display: inline-block; color:#999;font-size:10px;font-weight: normal">
                        #<?php echo $artikul->getId(); ?>
                        <span class="warehouse-box" style="<?php if (empty($artikul->getWarehouseCode())) : ?>display: none;<?php endif; ?>">
                            / <span class="warehouse-code"><?php echo $artikul->getWarehouseCode(); ?></span>
                        </span>
                    </div>
                </div>
                <div><strong class="prodcatmarka"><?php echo $artikul->getIme_marka(); ?></strong></div>
                <div class="prodcat"><?php echo $artikul->getIme(); ?></div>
                <style>
                    .balloon {
                        display: inline-block;
                        border: 1px solid rgb(207, 207, 207);
                        margin-right: 4px;
                        width: 84px;
                        height: 63px;
                        background-color: #FFF;
                    }

                    .balloon:hover {
                        border: 1px solid #FF671F;
                    }
                </style>
                <?php
                if ($dop_artikuli != "") {
                    $dop_art_arr = explode(",", $dop_artikuli);
                    echo "<div style='font-size: 13px; color: #808080; margin-top: 5px; margin-bottom: 5px;'>+</div>";
                    foreach ($dop_art_arr as $dart) {
                        $dr = new artikul((int)$dart);

                        if (empty($dr->getId())) continue;

                        echo "<div class='balloon'><a style='font-size: 13px;' href='" . url . "index.php?id=" . $dr->getId() . "' target='_blank'><table style='border-collapse:collapse;'><tr><td style='width:84px;height:63px;text-align:center;'><img src='" . $dr->getKartinka_t() . "' style='margin:0 auto; display: block; max-width: 84px; max-height: 63px;' alt='" . $dr->getIme_marka() . " " . $dr->getIme() . "' title='" . $dr->getIme_marka() . " " . $dr->getIme() . "'/></td></tr></table></a></div>";
                    }
                    echo '<div style="clear: both;"></div>';
                }
                ?>
            </h3>
            <div id="collection_container" style="text-align: left;">
                <?php if (count($artikul->getCollectionProducts())) : ?>
                    <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-bottom: 15px;"></div>
                    <div style="margin-bottom: 5px; font-size: 13px; color: #999999; font-weight: normal;">цветове</div>
                    <?php
                    foreach ($artikul->getCollectionProducts() as $v) {
                        $collectionProduct = new artikul((int)$v);
                        if ((!$collectionProduct->isAvaliable() || !$collectionProduct->isOnline()) && $collectionProduct->getId() != $artikul->getId()) continue;
//                        if(!$collectionProduct->isAvaliable() && $collectionProduct->getId() != $artikul->getId()) continue;
                        $current = '';
                        $border = 'border: 1px solid #ccc;';
                        if ($collectionProduct->getId() == $artikul->getId()) $current = 'current';
                        if ($collectionProduct->IsNew()) $border = 'border: 1px solid #01A0E2;';
                        echo "<a title='" . $collectionProduct->getIme() . "' href='" . url . "index.php?id=" . $collectionProduct->getId() . "' style='margin-bottom: 3px; display: inline-block;'><div class='collection-item " . $current . "' style='" . $border . " width: 64px; height: 60px; background-color: #fff; display: flex; margin-right: 5px; float: left; align-items: center;'><img style='max-width: 55px; height: auto; max-height: 55px; display: block; text-align: center; margin: 0 auto; vertical-align: middle;' src='" . url . $collectionProduct->getKartinka_t() . "' alt='' /></div></a>";

                    }
                    ?>
                <?php endif; ?>
            </div>
            <div class="specification">
                <?php echo nl2br($artikul->getSpecification()); ?>
            </div>
            <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-top: 15px; margin-bottom: 15px;"></div>
            <?php
            echo '<div class="questionsBar">';
            $manual = $artikul->getManual();
            if ((isset($manual)) && (!empty($manual))) {
                ?>
                <div><a style="font-size: 13px;"
                        href="<?php echo url . 'fancy_qbs.php' . '?id=' . $artikul->getID() . '&p=manual'; ?>">кой<br/>размер
                        съм?</a></div> <?php
            }
            echo '<div style="padding-left:0px;"><a style="font-size: 13px;" class="thickbox" href="' . url . 'fancy_qbs.php?p=howtobuy&id=' . $artikul->getID() . '">как<br />да поръчам?</a></div>';
            echo '<div><a style="font-size: 13px;" class="thickbox" href="' . url . 'fancy_qbs.php' . '?id=' . $artikul->getID() . '&p=sendquestion">въпрос<br />за продукта</a></div>';
            echo '</div>';
            $t = $artikul->getCena();
            if ($artikul->getCena_promo() > 0) {
                $t = $artikul->getCena_promo();
            }
            ?>
            <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-top: 15px; margin-bottom: 15px;"></div>
            <form id="addtocart" action="<?php echo url . 'cart.php'; ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="addToCard" value="<?php echo $artikul->getID(); ?>"/>
                <input type="hidden" name="type" value="product"/>
                <input name="counts" value="1" type="hidden"/>
                <div class="prodinfotext">
                    <?php
                    $showedsize = false;
                    if ($isAvaliable) {
                        $artikul_etiket_grup = NULL;
                        $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ? ORDER BY `id` ');
                        $stm->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                        $stm->execute();
                        foreach ($stm->fetchAll() as $v) {
                            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ORDER BY `id` ');
                            $stm->bindValue(1, $v['id_etiket'], PDO::PARAM_INT);
                            $stm->execute();
                            $temp = new etiket($stm->fetch());
                            $temp_grupa = new etiket_grupa((int)$temp->getId_etiketi_grupa());
                            $artikul_etiket_grup[$temp->getId_etiketi_grupa()] = $temp_grupa;
                            unset($temp);
                            unset($temp_grupa);
                        }
                        if (isset($artikul_etiket_grup) && (sizeof($artikul_etiket_grup) > 0)) {
                            foreach ($artikul_etiket_grup as $v) {
                                if ($v->getSelect_menu()) {
                                    echo '<div style="margin-top: 15px;margin-bottom: 10px; color: #808080; font-size: 13px; " class="ime_na_opciqta">избери ' . $v->getIme() . '</div>';
                                    $showedsize = true;
                                    if ($dop_artikuli != "") {
                                        echo '<div style="font-size: 13px; margin-top: 10px; margin-bottom: 10px; color: #808080;"><span style="color: #444;">' . $artikul->getIme_marka() . ' / ' . $artikul->getIme() . '</span></div>';
                                    }
                                    $tmp_br_etiketi = 0;
                                    $tmp_first_id = 0;
                                    $tmp_first_ime = "";
                                    echo '<input type="hidden" name="option' . $artikul->getId() . '" id="option' . $artikul->getId() . '"/>';
                                    echo '<input type="hidden" name="price_tag" id="price_tag"/>';
                                    if ($v->getEtiketi()) {
                                        foreach ($v->getEtiketi() as $e) {
                                            if ($artikul->getEtiketizapisi()) if (in_array($e->getId(), $artikul->getEtiketi_array_ids())) {
                                                $tmp_br_etiketi++;
                                                $tmp_first_ime = $e->getIme();
                                                $tmp_first_id = $e->getId();
                                                echo '<a id="option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . '" onclick="javascript:$(\'#option' . $artikul->getId() . '\').val(\'' . $e->getIme() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);updatePriceBySize(this);$(\'#price_tag\').val(\'' . $e->getId() . '\');return false;" href="#">' . $e->getIme() . '</a>';
//                                                echo '<a id="option_'.$artikul->getId().'_'.$v->getId().'_'.$e->getId().'" onclick="javascript:$(\'#option'.$artikul->getId().'\').val(\''.$e->getIme().'\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);return false;" href="#">'.$e->getIme().'</a>';
                                                if (isset($etiket) && in_array($e->getId(), $etiket)) {
                                                    echo '
                                                                                                <script type="text/javascript">
                                                                                                        updatePriceBySize( $("#option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . '") );
                                                                                                        $(\'#price_tag\').val(\'' . $e->getId() . '\');
                                                                                                        $(\'#option' . $artikul->getId() . '\').val(\'' . $e->getIme() . '\');
                                                                                                        $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . ').toggleClass(\'active\',true);
                                                                                                        $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . ').siblings().toggleClass(\'active\',false);
                                                                                                </script>
                                                                                                ';
                                                }
                                            }
                                        }
                                    }
                                    //echo '</div>';
                                    if ($tmp_br_etiketi == 1) {
                                        echo '
                                                                    <script type="text/javascript">
                                                                            updatePriceBySize( $("#option_' . $artikul->getId() . '_' . $v->getId() . '_' . $tmp_first_id . '") );
                                                                            $(\'#price_tag\').val(\'' . $tmp_first_id . '\');
                                                                            $(\'#option' . $artikul->getId() . '\').val(\'' . $tmp_first_ime . '\');
                                                                            $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $tmp_first_id . ').toggleClass(\'active\',true);
                                                                            $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $tmp_first_id . ').siblings().toggleClass(\'active\',false);
                                                                    </script>
                                                                    ';
                                    }
                                    echo '
                                                            <script type="text/javascript">
                                                                    addToCartRules.push(["option' . $artikul->getId() . '","' . $artikul->getIme() . '","' . $v->getIme() . '"]);
                                                            </script>
                                                    ';
                                }
                            }
                        }

                        foreach ($artikul->getUpgradeGroups() as $upgradeGroup) {
                            if (count($artikul->getUpgradeOptions($upgradeGroup->getId())) > 0) {
                                $defaultUpgradeOption = $artikul->getDefaultUpgradeOptions($upgradeGroup->getId());

                                echo '<div id="upgrage_group_' . $upgradeGroup->getId() . '">';
                                echo '<input type="hidden" name="upgrade_option[' . $upgradeGroup->getId() . '][upgrade_name]" id="upgrade_name_' . $upgradeGroup->getId() . '" value="' . $upgradeGroup->getIme() . '"/>';
                                echo '<input type="hidden" name="upgrade_option[' . $upgradeGroup->getId() . '][name]" id="upgrade_option_' . $upgradeGroup->getId() . '" value="' . $defaultUpgradeOption->getIme() . '"/>';
                                echo '<input type="hidden" class="upgrade_option_prices" name="upgrade_option[' . $upgradeGroup->getId() . '][additional_price]" id="upgrade_option_price_' . $upgradeGroup->getId() . '" value="0"/>';

                                echo '<div style="margin-top: 15px;margin-bottom: 10px; color: #808080; font-size: 13px; " class="ime_na_opciqta">' . $upgradeGroup->getIme() . '</div>';
                                echo '<a class="active" style="width: calc(100% - 20px); text-align: left; margin-bottom: 8px; padding: 0 10px; font-size: 13px;" id="upgrade_' . $artikul->getId() . '_' . $defaultUpgradeOption->getId() . '" onclick="javascript:$(\'#upgrade_option_' . $upgradeGroup->getId() . '\').val(\'' . $defaultUpgradeOption->getIme() . '\');$(\'#upgrade_option_price_' . $upgradeGroup->getId() . '\').val(\'' . $defaultUpgradeOption->getPriceByUser() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);updatePriceBySize(this);return false;" href="#">' . $defaultUpgradeOption->getIme() . '<span style="text-align: right; display: inline-block; float: right;">0 лв.</span></a>';

                                foreach ($artikul->getUpgradeOptions($upgradeGroup->getId()) as $upgradeOption) {
                                    echo '<a style="width: calc(100% - 20px); text-align: left; margin-bottom: 8px; padding: 0 10px; font-size: 13px;" id="upgrade_' . $artikul->getId() . '_' . $defaultUpgradeOption->getId() . '" onclick="javascript:$(\'#upgrade_option_' . $upgradeGroup->getId() . '\').val(\'' . $upgradeOption->getIme() . '\');$(\'#upgrade_option_price_' . $upgradeGroup->getId() . '\').val(\'' . $upgradeOption->getPriceByUser() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);updatePriceBySize(this);return false;" href="#">' . $upgradeOption->getIme() . '<span style="text-align: right; display: inline-block; float: right;">' . number_format($upgradeOption->getPriceByUser() / CURRENCY_RATE, 2, '.', '') . ' лв.</span></a>';
                                }

                                echo '</div>';
                            }
                        }
                    }
                    ?>
                    <?php
                    if ($dop_artikuli != "") {
                        $dop_art_arr = explode(",", $dop_artikuli);
                        foreach ($dop_art_arr as $dart) {
                            $dr = new artikul((int)$dart);
                            if ($dr->isAvaliable()) {
                                $add_artikul_etiket_grup = NULL;
                                $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ? ORDER BY `id` ');
                                $stm->bindValue(1, $dr->getId(), PDO::PARAM_INT);
                                $stm->execute();
                                foreach ($stm->fetchAll() as $v) {
                                    $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ORDER BY `id` ');
                                    $stm->bindValue(1, $v['id_etiket'], PDO::PARAM_INT);
                                    $stm->execute();
                                    $temp = new etiket($stm->fetch());
                                    $temp_grupa = new etiket_grupa((int)$temp->getId_etiketi_grupa());
                                    $add_artikul_etiket_grup[$temp->getId_etiketi_grupa()] = $temp_grupa;
                                    unset($temp);
                                    unset($temp_grupa);
                                }
                                if (isset($add_artikul_etiket_grup) && (sizeof($add_artikul_etiket_grup) > 0)) {
                                    foreach ($add_artikul_etiket_grup as $v) {
                                        if (!$showedsize) echo '<div style="margin-top: 15px;margin-bottom: 10px; color: #808080; font-size: 13px; ">' . $v->getIme() . '</div>';
                                        if ($v->getSelect_menu()) {
                                            echo '<div style="font-size: 13px; margin-top: 10px; margin-bottom: 10px;"><span style="color: #444;">' . $dr->getIme_marka() . ' / ' . $dr->getIme() . '</span></div>';
                                            echo '<div class="prodinfotext">';
                                            echo '<input type="hidden" name="option' . $dr->getId() . '" id="option' . $dr->getId() . '"/>';
                                            $etiketii = null;
                                            $pdo = PDOX::vrazka();
                                            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id_etiketi_grupa` = :idgroupa ORDER BY `ordr`');
                                            $stm->bindValue(":idgroupa", $v->getId(), PDO::PARAM_INT);
                                            $stm->execute();
                                            if ($stm->rowCount() > 0)

                                                foreach ($stm->fetchAll() as $d) {
                                                    $temp_etiket = new etiket((int)$d['id']);
                                                    $etiketii[] = $temp_etiket;
                                                    unset($temp_etiket);
                                                }

                                            if ($etiketii) {
                                                foreach ($etiketii as $e) {
                                                    if ($dr->getEtiketizapisi()) if (in_array($e->getId(), $dr->getEtiketi_array_ids())) {
                                                        echo '<a id="option_' . $dr->getId() . '_' . $v->getId() . '_' . $e->getId() . '" onclick="javascript:$(\'#option' . $dr->getId() . '\').val(\'' . $e->getIme() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);return false;" href="#">' . $e->getIme() . '</a>';
                                                    }
                                                }
                                            }
                                            echo '</div>';
                                            echo '
                                                        <script type="text/javascript">
                                                                addToCartRules.push(["option' . $dr->getId() . '","' . $dr->getIme() . '","' . $v->getIme() . '"]);
                                                        </script>
                                                        ';
                                        }
                                    }
                                }
                                unset($add_artikul_etiket_grup);
                            }
                            unset($dr);
                        }
                    }
                    ?>
                    <div style="clear:both;"></div>
                    <?php if($artikul->totalQuantity() > 0) : ?>
                        <div class="quantity" style="<?php if (empty($artikul->getWarehouseCode())) : ?>visibility: hidden;<?php endif; ?>float: right;margin-top: 5px;font-size:8pt;color: #808080;width: 170px;text-align: center;">налични <span class="count" style="color: #FF671F;"><?php echo $artikul->totalQuantity(); ?></span><span style="color: #FF671F;"> бр.</span></div>
                    <?php endif; ?>
                    <div style="clear:both;"></div>
                    <div id="item_price" class="price-box"
                         style="float: left; margin-top: 12px; width: 180px; text-align:center;">
                        <?php if (isset($__user) && $__user->is_partner()) : ?>
                            <?php if ($artikul->getCenaDostavna() > 0) : ?>
                                <span class="prodpricepromo"
                                      style="width: 100%;float:right;font-size:20px;font-style: italic; color: #FF6720;"><?php $tmp_cen = explode(".", number_format($artikul->getCenaDostavna(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;">лв.</span></span>
                            <?php else : ?>
                                <?php if ($artikul->getCena_promo()) : ?>
                                    <span class="prodpricenormal" style="float:left;margin-right:10px;"><span
                                                style="color:#424242;font-size:12pt;font-style: italic;"> <?php $tmp_cen = explode(".", number_format($artikul->getCena(), 2, '.', ''));
                                            echo $tmp_cen[0]; ?> <sup
                                                    style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span
                                                    style="font-size:10px;">лв.</span></span></span>
                                    <span class="prodpricepromo"
                                          style="float:right;font-size:20px;font-style: italic; color: #FF671F;"><?php $tmp_cen = explode(".", number_format($artikul->getCena_promo(), 2, '.', ''));
                                        echo $tmp_cen[0]; ?> <sup
                                                style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                                style="font-size:10px;">лв.</span></span>
                                <?php else : ?>
                                    <span class="prodpricepromo"
                                          style="width: 100%;float:right;font-size:20px;font-style: italic; color: #FF671F;"><?php $tmp_cen = explode(".", number_format($artikul->getCena(), 2, '.', ''));
                                        echo $tmp_cen[0]; ?> <sup
                                                style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                                style="font-size:10px;">лв.</span></span>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php else : ?>
                            <?php if ($artikul->getCena_promo()) : ?>
                                <span class="prodpricenormal"
                                      style="float:left;color:#424242;font-size:12pt;font-style: italic;"> <?php $tmp_cen = explode(".", number_format($artikul->getCena(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;">лв.</span></span></span>
                                <span class="prodpricepromo"
                                      style="float:right;font-size:20px;font-style: italic; color: #FF671F;"><?php $tmp_cen = explode(".", number_format($artikul->getCena_promo(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;">лв.</span></span>
                            <?php else : ?>
                                <span class="prodpricepromo"
                                      style="width: 100%;float:right;font-size:20px;font-style: italic; color: #424244;"><?php $tmp_cen = explode(".", number_format($artikul->getCena(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;">лв.</span></span>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (isset($__user) && $__user->is_partner()) : ?>
                            <?php if ($artikul->getCena_promo() > 0) : ?>
                                <div style="position: absolute;bottom: 10px;margin-top: 5px; margin-right: 25px; font-size:8pt; color: #FF671F;">
                                    Краен
                                    клиент: <?php $tmp_cen = explode(".", number_format($artikul->getCena_promo(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup><?php echo $tmp_cen[1]; ?></sup> <span
                                            style="font-size:10px;">лв.</span></div>
                            <?php else : ?>
                                <div style="position: absolute;bottom: 10px;margin-top: 5px; margin-right: 25px; font-size:8pt; color: #FF671F;">
                                    Краен
                                    клиент: <?php $tmp_cen = explode(".", number_format($artikul->getCena(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup><?php echo $tmp_cen[1]; ?></sup> <span
                                            style="font-size:10px;">лв.</span></div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php
                        if ($artikul->getCena_promo() > 0) {
                            $tmpraz = (($artikul->getCena() / CURRENCY_RATE) - ($artikul->getCena_promo() / CURRENCY_RATE));
                            $tmp_razf = explode(".", number_format($tmpraz, 2, '.', ''));
                            echo '<div class="saving-box">';
                            echo '<span style="float: left;margin-top: 2px;margin-left: 5px;font-size: 8pt;color: #FF671F;">спестявате ' . lang_currency_prepend . '' . $tmp_razf[0] . '<sup>' . $tmp_razf[1] . '</sup> ' . lang_currency_append . ' (-' . round((($tmpraz / ($artikul->getCena() / CURRENCY_RATE)) * 100), 0) . '%)</span>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                    <?php
                    if ($isAvaliable && $isOnline) {
                        echo '<input onclick="return validate(addToCartRules, ' . ($artikul->isPreorder() ? 'true' : 'false') . ');" style="' . ($artikul->isPreorder() ? 'background-color:#29AC92;' : '') . 'float:right; margin-top: 2px; height: 36px; line-height: 30px;" class="btnaddprod" type="submit" value="добави в количката"/>';
                    } else {
                        echo '<div style="float:right; margin-top: 2px; height: 30px; line-height: 30px; background-color: #333; cursor: default; width: 150px; text-align: center;" class="btnaddprod" >разпродадено</div>';
                    }

                    $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
                    $ds->execute();
                    $d = $ds->fetch();
                    $dostavka_cena = $d['delivery_price'];
                    $dostavka_free = $d['free_delivery'];
                    $additional_delivery = $artikul->getAdditionalDelivery();

                    if ($dop_artikuli != "") {
                        $dop_art_arr = explode(",", $dop_artikuli);
                        foreach ($dop_art_arr as $dart) {
                            $dr = new artikul((int)$dart);
                            if ($dr->isAvaliable()) {
                                $additional_delivery += $dr->getAdditionalDelivery();
                            }
                        }
                    }

                    $ac = 0;
                    if ($artikul->getCena_promo() > 0) {
                        $ac = $artikul->getCena_promo();
                    } else {
                        $ac = $artikul->getCena();
                    }

                    echo '<div class="delivery-box">';

                    if ($artikul->isPreorder()) {
                        if ($artikul->getCena_promo() > 0) {
                            echo '<div style="float: right; margin-top: 5px; margin-right: 25px; font-size:8pt; color: #808080;">с безплатна доставка</div>';
                        } else {
                            echo '<div style="float: right; margin-top: 5px; margin-right: 25px; font-size:8pt; color: #808080;">с безплатна доставка</div>';
                        }
                    } else if ($dostavka_free < $ac) {
                        if ($additional_delivery != '' && $additional_delivery > 0) {
                            echo '<div style="float: right; margin-top: 5px; margin-right: 40px; font-size:8pt; color: #808080;">доставка: ' . $additional_delivery . ' лв.</div>';
                        } else {
                            if ($artikul->getCena_promo() > 0) {
                                echo '<div style="float: right; margin-top: 5px; margin-right: 25px; font-size:8pt; color: #808080;">с безплатна доставка</div>';
                            } else {
                                echo '<div style="float: right; margin-top: 5px; margin-right: 25px; font-size:8pt; color: #808080;">с безплатна доставка</div>';
                            }
                        }
                    } else {
                        if ($additional_delivery != '' && $additional_delivery > 0) {
                            $total_delivery = $dostavka_cena + $additional_delivery;
                            echo '<div style="float: right; margin-top: 5px; margin-right: 40px; font-size:8pt; color: #808080;">доставка: ' . $total_delivery . ' лв.</div>';
                        } else {
                            if ($artikul->getCena_promo() > 0) {
                                echo '<div style="float: right; margin-top: 5px; margin-right: 40px; font-size:8pt; color: #808080;">доставка: ' . $dostavka_cena . ' лв.</div>';
                            } else {
                                echo '<div style="float: right; margin-top: 5px; margin-right: 40px; font-size:8pt; color: #808080;">доставка: ' . $dostavka_cena . ' лв.</div>';
                            }
                        }
                    }
                    echo '</div>';
                    ?>
                    <div class="clear"></div>
                    <?php if ($artikul->isPreorder()) : ?>
                        <div style="margin-top: 15px; margin-bottom: 10px; font-size:10pt; color: #29AC92;"><?php echo $artikul->getPreorderDescription(); ?></div>
                        <div id="pre-order-dialog">
                            <?php echo $preOrderConfirmationDescriptions; ?>
                        </div>​
                    <?php endif; ?>
                    <div style="clear:both;"></div>
                </div>
            </form>
            <!--<div style="float: left; margin-left: 5px; margin-top: 30px; margin-bottom: 30px;">-->
            <?php
            //							echo '<span><iframe src="http://www.facebook.com/plugins/like.php?href='.curPageURL().'&amp;locale=bg_BG&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" style="border:none; overflow:hidden;height:20px; " allowTransparency="true"></iframe></span>';
            ?>
            <!--</div>-->
        </div>
        <div style="float: right;text-align: left;width:100px;clear: right;margin-top:10px; margin-right: 310px;">
            <?php
            echo '<span><iframe src="//www.facebook.com/plugins/like.php?href=' . curPageURL() . '&amp;locale=bg_BG&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;colorscheme=dark" scrolling="no" frameborder="0" style="border:none; overflow:hidden;height:20px; " allowTransparency="true"></iframe></span>';
            ?>
        </div>
    </div>
    <div style="clear:both; height:20px"></div>
</div>
<div style="clear:both;"></div>
<div style="clear:both; height: 10px;"></div>
<style>
    #p-details-left-col {
        text-align: left;
        display: inline-block;
        vertical-align: top;
        margin-left: 20px;
        width: 160px;
    }

    #p-details-right-col {
        text-align: left;
        width: 1260px;
        margin: 0 auto;
        vertical-align: top;
        padding-bottom: 30px;
    }

    .lastseen {
        width: 1260px;
        margin: 0 auto;
    }
</style>
</center>
<div id="p-details-left-col">
    <?php
    // if((isset($banners))&&(sizeof($banners)>0)&&(!isset($_GET['hotOffers']))&&(!isset($_GET['branch']))){
    //      foreach($banners as $tmp_banner){
    //          echo '<a class="banner_item" ';
    //              if($tmp_banner->getDistinationURL()!='') echo ' href="'.$tmp_banner->getDistinationURL();
    //              echo '">';
    //              echo '<img style="width:160px;height:400px;" src="'.url.$tmp_banner->getImage_b().'" />';
    //          echo '</a>';
    //       }
    //  }
    ?>
</div>
<script>
    $(document).ready(function () {
        var width = $("#p-details-right-col").width(),
            broi = width / 270,
            product_id = <?php echo $artikul->getId(); ?>,
            cat = <?php echo $artikul->getId_kategoriq(); ?>,
            vid = <?php echo $artikul->getId_vid(); ?>,
            marka = <?php echo $artikul->getId_model(); ?>,
            except = <?php echo $artikul->getId(); ?>;
        $.ajax({
            type: "POST",
            url: "ajax/ajax_product_same_artikuli.php",
            data: "cat=" + cat + "&vid=" + vid + "&marka=" + marka + "&broi=" + broi + "&except=" + except,
            cache: false,
            success: function (html) {
                if (html != "") {
                    $('#products-same').html(html);
                    $('#products-same-title').show();
                }
            }
        });
        $.ajax({
            type: "POST",
            url: "ajax/ajax_product_category_seen.php",
            data: "broi=" + broi,
            cache: false,
            success: function (html) {
                $('#categories-seen').html(html);
            }
        });
        $.ajax({
            type: "POST",
            url: "ajax/ajax_product_group_products.php",
            data: "vid=" + vid + "&broi=" + broi,
            cache: false,
            success: function (html) {
                $('#group-products').html(html);
            }
        });
        $.ajax({
            type: "POST",
            url: "ajax/ajax_related_products.php",
            data: "broi=" + broi + "&product_id=" + product_id,
            cache: false,
            success: function (html) {
                if (html != "") {
                    $('#related-products').html(html);
                    $('#related-products-title').show();
                }
            }
        });
    });
</script>
<div id="p-details-right-col">
    <?php
    $strRelatedProducts = $artikul->getRelatedProducts();
    if ($strRelatedProducts != "") { ?>
        <div id="related-products-title"
             style="display: none; padding-left:22px;padding-right:10px;background:#FFFFFF;position: relative;z-index: 1;color:#01A0E2;margin-bottom:10px;">
            Подходяща комбинация с тези продукти
        </div>
        <div id="related-products"></div>

        <?php
    } else if (count($etiketi_grupi)) { ?>
        <div id="products-same-title"
             style="display: none; padding-left:22px;padding-right:10px;background:#FFFFFF;position: relative;z-index: 1;color:#808080;margin-bottom:10px;">
            Други
            <?php
            echo mb_strtolower($artikul->getIme_kategoriq(), 'UTF-8');
            echo ' ' . mb_strtolower($artikul->getIme_vid(), 'UTF-8');
            echo ' марка ' . mb_strtolower($artikul->getIme_marka(), 'UTF-8');
            ?>
        </div>
        <div id="products-same"></div>
        <?php
    }
    ?>
    <div style="clear:both; height: 30px;"></div>
    <?php if (isset($catSeen)) { ?>
        <div style="padding-left:22px;padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;color:#808080;margin-bottom:10px;">
            Предпочитани от вас
        </div>
        <div id="categories-seen"></div>
        <div style="clear:both; height: 30px;"></div>
    <?php } ?>
    <div id="group-products"></div>
</div>
<div style="clear:both;"></div>
<?php
if (isset($lastSeenProducts) && (sizeof($lastSeenProducts) > 0)) {
    ?>
    <div class="lastseen">
        <div class="title">
            <div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">
                Последно разглеждани
            </div>
            <div style="border-bottom: 1px dashed #CCC;display: block;height: 5px;margin: -14px 0 14px 0px;position: relative;"></div>
        </div>
        <?php
        if (isset($lastSeenProducts) && (sizeof($lastSeenProducts) > 0)) {
            $broikaPoslednoRazglejdani = 0;
            foreach ($lastSeenProducts as $product) {
                $broikaPoslednoRazglejdani++;
                if ($broikaPoslednoRazglejdani == 11) {
                    break;
                }
                if ($product->isAvaliable()) {
                    echo '<a href="index.php?id=' . $product->getId() . '"><table class="imageCenter"><tr><td><img src="' . $product->getKartinka_t() . '" title="' . $product->getIme_marka() . ' ' . $product->getIme() . '"></td></tr></table></a>';
                }
            }
        }
        ?>
    </div>
    <div style="clear:both; border-bottom: 1px dashed #CCC;width: 1280px; margin: 0 auto;"></div>
    <?php
}
?>
<div style="clear:both; height: 30px;"></div>

<div class="modal">
    <div class="modal-content">
        <div class="modal-body" style="text-align: left !important;">
            <div style="width: 100%; height: 40px;" class="modal-text-inside"></div>
            <button onclick="hideModal();"
                    style="width: 100px;background-color: #FF671F; color: white;position: relative;top: 45px;display: block; text-align: center; line-height: 36px;height:36px; font-size: 13px; float: right;cursor: pointer;margin-left: 10px; margin-right: 10px;">
                OK
            </button>
        </div>
    </div>
</div>

<center>
