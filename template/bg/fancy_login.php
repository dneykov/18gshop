<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" href="<?php echo url_template_folder; ?>css/css3.css" type="text/css" >
		<script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
		<style type="text/css">
			h3{
				color:#4d4d4d;
				font-size:16pt;
				margin:10px 10px 0;
				font-weight: normal;
				text-align:left;
			}
			.title{
				color:#4d4d4d;
				font-size:9pt;
				margin:0 10px;
				text-align:left;
			}
			.title a{
				color:#4d4d4d;
				font-weight: bold;
				font-size:12pt;
			}
			.submit {
				background: none repeat scroll 0 0 #FF671F;
				border: medium none;
				color: #fff;
				cursor: pointer;
				float: right;
				margin: 34px 0 0;
				padding: 0 8px 2px;
				text-decoration: none;
			}
			.error{
				color: #FF671F;
				font-size:9pt;
				margin:10px;
				clear:both;
			}
			input, textarea {
				font-size: 13px;
			}
			input:hover, input:focus {
				border: 1px solid #FF671F;
			}
		</style>
	</head>
	<body>
		<ul id="main_menu">
			<li><a <?php if(isset($_GET['p'])&&(($_GET['p']=='cart')||($_GET['p']=='ordrdetails'))) echo 'class="active"'; ?> href="<?php echo url.'fancy_login.php?p=cart';?>">Вашата поръчка</a></li><?php
				if(!isset($__user)){
					$tmp_class='';
					if(isset($_GET['p'])&&$_GET['p']=='login') $tmp_class='class="active"';
					echo '<li><a '.$tmp_class.' href="'.url.'fancy_login.php?p=login" >Вход</a></li>';

					$tmp_class='';
					if(isset($_GET['p'])&&$_GET['p']=='registation') $tmp_class='class="active"';
					echo '<li><a '.$tmp_class.' href="'.url.'fancy_login.php?p=registation">Регистрация</a></li>	';
				}
			?>
                        <div style="height:3px;width: 100%;background: #FF671F;"></div>
		</ul>
		<div style="height:3px;color:#fcc900;background:#fcc900;"></div>
		<div class="main">
			<?php
				$page=$_GET['p'];
				switch ($page) {
					case "cart":
						require_once dir_root_template.'cart.php';
						break;
					case "login":
						require_once dir_root_template.'login.php';
						break;
					case "registation":
						require_once dir_root_template.'register_form.php';
						break;
					case "ordrdetails":
						require_once dir_root_template.'order_details.php';
						break;
					case "forgotpass":
						require_once dir_root_template.'forgot_pass.php';
						break;
				}
			?>
		</div>
	</body>
</html>


