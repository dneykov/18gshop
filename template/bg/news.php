<div class="product_navigation">
	<?php
		$__url=url.'index.php';
		echo '<a href="'.$__url.'">' . $pageTitle . '</a>';
		echo ' - <a href="#">новини</a>';
	?>
</div>
<div class="clear"></div>
<div>
<?php
function _is_youtube($url)
{
    return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url) || preg_match('/youtube\.com\/embed/i', $url) || preg_match('/youtube\.com\/v/i', $url));
}
function _is_vimeo($url)
{
    return (preg_match('/vimeo\.com/i', $url));
}
function youtube_id_from_url($url) {
    $pattern =
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
    ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}
if(isset($_GET['newsid'])){
    $active_news=$_GET['newsid'];
}else{
    $active_news=$news[0]->getID();
}
foreach($news as $n){
    echo '<a href="'.$__url.'?newid='.$n->getId().'"><div style="display: inline-block; width: 320px; margin-left: 10px; margin-top: 50px; margin-right: 10px; margin-bottom: 10px; border-bottom: 1px solid #ccc;">';
            $date = new DateTime($n->getDate());
            echo '<span class="date" style="float: left; margin-top: 5px; margin-bottom: 5px; font-size:10pt;color:#b2b2b2;">'.date_format($date, 'd-m-Y').'</span><div style="clear:both;"></div>';
            if($n->getType()=='news'){
                    echo '<img style="max-width:400px; max-height:300px;margin:0px 20px 10px 0px; float: left;" src="'.url.$n->getImage_t().'"/>';
            }else{
                if(_is_vimeo($n->getImage())){
                    $link = str_replace('https://vimeo.com/', 'https://vimeo.com/api/v2/video/', $n->getImage()) . '.php';

                    $html_returned = unserialize(file_get_contents($link));

                    $thumb_url = $html_returned[0]['thumbnail_large'];
                    echo '<img style="width:320px;margin:0px 20px 10px 0px; float: left;" src="'.$thumb_url.'"/>';

                } elseif(_is_youtube($n->getImage())) {
                    $src = str_replace('watch?v=','embed/',$n->getImage());
                    echo '<img style="max-width:320px; max-height:300px;margin:0px 20px 10px 0px; float: left;" src="https://img.youtube.com/vi/'.youtube_id_from_url($n->getImage()).'/0.jpg"/>';
                }
            }
            echo '<span style="font-size: 16px; float: left; min-height: 40px;"> '.$n->getTitle().'</span>';
   echo '</div></a>';
}
?>
</div>
<div style="clear:both; height:40px;"></div>
</div>
