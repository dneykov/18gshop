<?php
$lg='bg';
?>
<script type="text/javascript">
    if ($('#shopcard table').length == 0) {
        parent.$('#main_menu a.cart').load('<?php echo url ?>/template/bg/top_menu_shopping_cart.php');
    }
    function change(id, num)
    {
        if (num != "") {
            var newid = id.substr(4);
            parent.$('.top_panel a.cart').load('<?php echo url ?>/template/bg/top_menu_shopping_cart.php');

            $.ajax({
                url: "cart.php",
                type: "POST",
                data: "updateNumber=" + num + "&podID=" + newid,
                success: function (data) {
                    if (jQuery('.multiplier').val() != '') {
                        window.location.reload();
                        parent.$('#main_menu a.cart').load('/template/bg/top_menu_shopping_cart.php');
                    }


                }
            });
        }
    }

    parent.$('#nav-main .cart').load('<?php echo url_template_folder ?>top_menu_shopping_cart.php');

    var tmp = $(document).height();
    $('html, body').animate({scrollTop: tmp}, '2500');
</script>
<div id="shopcard" style="padding:10px;">
	<FORM METHOD="POST" ACTION="fancy_login.php?p=ordrdetails" >
			<input type="hidden" name="comment" />
				


<?php
if(!isset($_SESSION['cart'])||(sizeof($_SESSION['cart'])<=0)){
    echo '<div style="position: absolute;top: 48%;right: 152px; color: #333;">'.(($lg=='en') ? 'There are no items in the shopping cart' : 'Използвайте бутона: "добави в количката" за да добавите<br/>продуктите които искате да купите.').'</div>';
}

$all=0;
$allPreorder=0;
if(isset($_SESSION['cart'])&&(sizeof($_SESSION['cart'])>0)){
	echo '<table style="width:770px;" cellspacing="0">';
	foreach($_SESSION['cart'] as $key => $val){
		if($_SESSION['cart'][$key]['type']=="product"){
		    $artikul = new artikul((int)$key);
    		$add_artikuli = $artikul->getAddArtikuli();
            if($add_artikuli != ''){
                $line = "";
            }
            else {
                $line = "border-bottom: solid 1px #999;";
            }

            $price = number_format($_SESSION['cart'][$key]['price'], 2, '.', '');
    		$price= explode('.',$price);

    		echo '
            <tr>
                <td style="'.$line.'padding:20px 0;width:130px; text-align: center;">
                    <img alt="'.stripslashes($artikul->getIme()).'" src="'.$artikul->getKartinka_t().'" style="max-width:110px; max-height: 135px;" />	 
                </td>
                <td valign="bottom" style="'.$line.'padding:23px 0;">
                    <span style="font-size:10pt;color:#000;">'.$artikul->getIme_marka().'</span><br/>
                    <span style="font-size:10pt;color:#424242" class="prodcat">'.$artikul->getIme().'</span>';
                    $flag=false;
                    if(isset($_SESSION['cart'][$key]['options'])){
                        foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal){   
                            $expl_arr = explode(",", $optionVal);
                            $option='';
                            foreach($expl_arr as $keyy => $vall) {
                                if($keyy == 0){
                                    $artikulid = (int)$vall;
                                }
                                if($keyy == 1 && $artikulid == $key){
                                    echo " - ".$vall;
                                    $flag = true;
                                }
                            }
                        }
                    }
                    if($flag==false){
                        echo "&nbsp;";
                    }

                    if(isset($_SESSION['cart'][$key]['upgrade_option'])) {
                        foreach ($_SESSION['cart'][$key]['upgrade_option'] as $option) {
                            echo "<div>" . $option['upgrade_name'] . ": " . $option['name'] . "</div>";
                        }
                    } else {
                        echo '<br>';
                    }

                    if($artikul->isPreorder()) {
						echo '<span style="color: #FB671F;position: absolute;margin-top: 2px;">с предварителна поръчка</span>';
					}
					if($artikul->isPreorder()) {
						echo '<span style="color: #FB671F;position: absolute;left: 48%;margin-top: 2px;">авансово плащане ' . $_SESSION['cart'][$key]['preorder']['deposit_percent'] . '%</span>';
					}
                echo '</td>';
                echo '
                <td valign="bottom" style="'.$line.'padding:20px 0;"><span style="margin-right: 5px;">';
                        echo "&nbsp;";
                echo '</td>';
                echo '
                <td valign="bottom" style="'.$line.'padding:23px 0; text-align: center;"><input style="width:40px;margin: 0 0 4px;" type="text" onKeyUp="change(this.id, this.value);" value="'.$_SESSION['cart'][$key]['count'].'" id="list'.$key.'" NAME="count'.$artikul->getId().'" class="multiplier"/></td>
                <td valign="bottom" style="'.$line.'padding:26px 0;font-style:italic;text-align: right; padding-right: 10px;"><span class="priceitem" id="price'.$artikul->getId().'" >'.$price[0].'</span><sup style="font-size:8pt;">'.$price[1].'</sup>лв.';

				if($artikul->isPreorder()) {
                    $deposit_price = number_format($_SESSION['cart'][$key]['preorder']['deposit_price'], 2, '.', '');
					$deposit_price = explode('.',$deposit_price);

					echo '<br/><span style="color: #FB671F; position: absolute;margin-top: 3px; right: 65px;"><span class="priceitem" id="price'.$artikul->getId().'" >'.$deposit_price[0].'</span><sup style="font-size:8pt;">'.$deposit_price[1].'</sup>лв.</span></td>';
				}

				echo '<td valign="bottom" style="'.$line.'padding:25px 0;"><a style="color:#FB671F; font-size: 16pt;" class="removeitem" href="fancy_login.php?p=cart&del='.$key.(($lg=='en') ? '&lg=en' : '').'">'.(($lg=='en') ? 'Delete' : 'x').'</a></td>
            </tr>';
            if($add_artikuli != ''){
                $add_art_arr = explode(",", $add_artikuli);
                $n = 0;
                foreach($add_art_arr as $dd) {
                    $n++;
                }
            }
            if($add_artikuli != ''){
                $add_art_arr = explode(",", $add_artikuli);
                $d = 0;
                foreach($add_art_arr as $dd) {
                    $dr = new artikul((int)$dd);

                    if (empty($dr->getId())) continue;

                    $d++;
                    if($n == $d){
                        $line = "border-bottom: solid 1px #999;";
                    }
                    echo '<tr><td><span style="margin-left: 60px; font-size: 17px; color: #424242;">+</span></td><td></td><td></td><td></td><td></td><td></td></tr>';
                    echo '
                        <tr>
                            <td style="'.$line.'padding:20px 0;width:130px; text-align: center;">
                                <img alt="'.stripslashes($dr->getIme()).'" src="'.$dr->getKartinka_t().'" style="max-width:110px; max-height: 135px;" />	 
                            </td>
                            <td valign="bottom" style="'.$line.'padding:20px 0;">
                                <span style="font-size:10pt;color:#000;">'.$dr->getIme_marka().'</span><br/>
                                <span style="font-size:10pt;color:#424242" class="prodcat">'.$dr->getIme().'</span>';
                                $flag = false;
                                if(isset($_SESSION['cart'][$key]['options'])){
                                    foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal){   
                                        $expl_arr = explode(",", $optionVal);
                                        foreach($expl_arr as $keyy => $vall) {
                                            if($keyy == 0){
                                                $artikulid = (int)$vall;
                                            }
                                            if($keyy == 1 && $artikulid == (int)$dd){
                                                echo ' - '.$vall;
                                                $flag = true;
                                            }
                                        }
                                    }
                                }
                                if($flag == false){
                                    echo "&nbsp;";
                                }
                            echo '</td>';
                        echo '<td valign="bottom" style="'.$line.'padding:20px 0;"><span style="margin-right: 5px;">';
                            echo "&nbsp;";
                        echo '</td>';
                        echo '
                            <td valign="bottom" style="'.$line.'padding:20px 0;">&nbsp;</td>
                            <td valign="bottom" style="'.$line.'padding:20px 0;">&nbsp;</td>
                            <td valign="bottom" style="'.$line.'padding:20px 0;">&nbsp;</td>
                        </tr>
                        ';
                }
            }
			if($artikul->isPreorder()) {
				$allPreorder += $_SESSION['cart'][$key]['preorder']['deposit_price'] * ((int)$_SESSION['cart'][$key]['count']);
                $all += $_SESSION['cart'][$key]['price'] * ((int)$_SESSION['cart'][$key]['count']);
			} else {
                $all += $_SESSION['cart'][$key]['price'] * ((int)$_SESSION['cart'][$key]['count']);
                $allPreorder += $_SESSION['cart'][$key]['price'] * ((int)$_SESSION['cart'][$key]['count']);
			}
        } else if ($_SESSION['cart'][$key]['type'] == "group"){
            $group = new paket((int)$key);
            $artikuli = $group->getProducts();
            $price= number_format($_SESSION['cart'][$key]['price'], 2, '.', '');
            $price= explode('.',$price);
            $line="";
            $n = 0;
            foreach($artikuli as $dd) {
                $dr = new artikul((int)$dd);
                if($n != 0) echo '<tr><td><span style="margin-left: 60px; font-size: 17px; color: #424242;">+</span></td><td></td><td></td><td></td><td></td><td></td></tr>';
                echo '
                    <tr>
                        <td style="'.$line.'padding:20px 0;width:130px; text-align: center;">
                            <img alt="'.stripslashes($dr->getIme()).'" src="'.$dr->getKartinka_t().'" style="max-width:110px; max-height: 135px;" />     
                        </td>
                        <td valign="bottom" style="'.$line.'padding:20px 0;">
                            <span style="font-size:10pt;color:#000;">'.$dr->getIme_marka().'</span><br/>
                            <span style="font-size:10pt;color:#424242" class="prodcat">'.$dr->getIme().'</span>';
                            $flag = false;
                            if(isset($_SESSION['cart'][$key]['options'])){
                                foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal){   
                                    $expl_arr = explode(",", $optionVal);
                                    foreach($expl_arr as $keyy => $vall) {
                                        if($keyy == 0){
                                            $artikulid = (int)$vall;
                                        }
                                        if($keyy == 1 && $artikulid == (int)$dd){
                                            echo ' - '.$vall;
                                            $flag = true;
                                        }
                                    }
                                }
                            }
                            if($flag == false){
                                echo "&nbsp;";
                            }
                        echo '</td>';
                    echo '<td valign="bottom" style="'.$line.'padding:20px 0;"><span style="margin-right: 5px;">';
                        echo "&nbsp;";
                    echo '</td>';
                    echo '
                        <td valign="bottom" style="'.$line.'padding:20px 0;">&nbsp;</td>
                        <td valign="bottom" style="'.$line.'padding:20px 0;">&nbsp;</td>
                        <td valign="bottom" style="'.$line.'padding:20px 0;">&nbsp;</td>
                    </tr>
                    ';
                $n++;
            }
            $line = "border-bottom: solid 1px #999;";
            echo '
            <tr>
                <td style="'.$line.'padding:10px 0;width:130px; text-align: center;"></td>
                <td valign="bottom" style="'.$line.'padding:20px 0;">
                    <span style="font-size:10pt;color:#424242" class="prodcat">'.$group->getName().'</span>';
                echo "&nbsp;";
                echo '</td>';
                echo '
                <td valign="bottom" style="'.$line.'padding:20px 0;"><span style="margin-right: 5px;">';
                        echo "&nbsp;";
                echo '</td>';
                echo '
                <td valign="bottom" style="'.$line.'padding:20px 0;"><input style="width:40px;margin: 0 0 4px;" type="text" onKeyUp="change(this.id, this.value);" value="'.$_SESSION['cart'][$key]['count'].'" id="list'.$key.'" NAME="count'.$group->getId().'" class="multiplier"/></td>
                <td valign="bottom" style="'.$line.'padding:20px 0;font-style:italic;text-align: right;padding-right: 10px;"><span class="priceitem" id="price'.$group->getId().'" >'.$price[0].'</span><sup style="font-size:8pt;">'.$price[1].'</sup>лв.</td>
                <td valign="bottom" style="'.$line.'padding:20px 0;"><a style="color:#FB671F; font-size: 16pt;" class="removeitem" href="fancy_login.php?p=cart&del='.$key.(($lg=='en') ? '&lg=en' : '').'">'.(($lg=='en') ? 'Delete' : 'x').'</a></td>
            </tr>';
            echo '<tr><td colspan="6" style="height: 5px;"></td></tr>';
            $all += ($_SESSION['cart'][$key]['price'])*((int)$_SESSION['cart'][$key]['count']);
			$allPreorder += ($_SESSION['cart'][$key]['price'])*((int)$_SESSION['cart'][$key]['count']);
        }
    }
	echo '</table>';
}
?>
<div style="clear:both;height:100px;"></div>
                        <div style="position: fixed; width: 100%; bottom: 0; background-color: #FFF;">
				<?php
				
				if(isset($_SESSION['cart'])&&(sizeof($_SESSION['cart'])>0)){						
					$total=number_format($allPreorder, 2, '.', '');
					$total= explode('.',$total);
					?>
                                                <span id="allcost" style="color: #424242; margin-top:0px;font-size:13px;width:175px;float:right;">сума <span style="font-style:italic; font-size: 18px;"><?php echo $total[0]; ?></span> <sup style="font-style:italic;font-size:8pt;"><?php echo $total[1]; ?></sup><span style="font-size: 11px;"><?php echo (($lg=='en') ? 'BGN.' : ' лв.'); ?></span></span>
						<a href="#" onclick="javascript:parent.$.fancybox.close();" style="display: block; background-color: #333;margin-right:160px;margin-top: 27px;clear: left;color: #FFF;float: left;font-size: 11pt;text-decoration: none; height: 21px; padding-left: 10px; padding-right: 10px; height: 36px; line-height: 32px;width: auto;text-align: center;">продължи пазаруването</a>
					
					<?php
				}
				?>		
							
					

				
				<?php 
					if((isset($_SESSION['cart']))&&(!empty($_SESSION['cart']))){
						if(isset($__user)){
							?>
							<!-- <INPUT TYPE="submit" value="<?php echo(($lg=='en') ? 'Check out' : 'поръчай'); ?>" class="submit"> -->
							<a style="clear:right;margin-bottom:5px;margin-top:5px;width:190px;text-align:center; background-color: #FB671F; margin-right: 20px; height: 32px; line-height: 30px;" id="submitshoppingcard"  class="submit" href="<?php echo url;?>fancy_login.php?p=ordrdetails" >поръчай</a>
                            <div style="clear:both"></div>
						<?php
						}else{
						?>
							<!-- <INPUT TYPE="submit" value="<?php echo(($lg=='en') ? 'Check out' : 'поръчай'); ?>" class="submit"> -->
							
							<a style="clear:right;margin-bottom:5px;margin-top:5px;width:190px;text-align:center; background-color: #FB671F; margin-right: 20px; height: 32px; line-height: 30px;" id="submitshoppingcard"  class="submit" href="<?php echo url;?>fancy_login.php?p=login" >поръчай</a>
                            <div style="clear:both"></div>
                    	<?php
						}

                        $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
                        $ds->execute();
                        $d = $ds->fetch();
                        $dostavka_cena = $d['delivery_price'];
                        $dostavka_free = $d['free_delivery'];

						if($dostavka_free>$all){
                            if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                                $dostavka_cena +=  $_SESSION['cart']['total_additional_delivery'];
                            }

    						$all+=$dostavka_cena;
    						$tmp=number_format($dostavka_cena, 2, '.', '');
    						$tmp= explode('.',$tmp);
    						?>
    						<span id="allcost" style="margin-top:0px;font-size:11pt;width:188px;float:right; margin-bottom: 5px; color: #808080;">доставка <span style="font-style:italic;"><?php echo $tmp[0]; ?></span><sup style="font-style:italic;font-size:8pt;"><?php echo $tmp[1]; ?></sup><?php echo (($lg=='en') ? 'BGN.' : ' лв.'); ?></span>
    					<?php
                        }else{
						    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                                $tmp=number_format($_SESSION['cart']['total_additional_delivery'], 2, '.', '');
                                $tmp= explode('.',$tmp);
						        $delivery_text = '<span id="allcost" style="margin-top:0px;font-size:11pt;width:188px;float:right; margin-bottom: 5px; color: #808080;">доставка <span style="font-style:italic;">' . $tmp[0] . '</span><sup style="font-style:italic;font-size:8pt;">' . $tmp[1] . '</sup>' . (($lg=='en') ? 'BGN.' : ' лв.') . '</span>';
                            } else {
                                $delivery_text = '<span id="allcost" style="margin-top:0px;font-size:11px;width:180px;float:right; margin-bottom: 5px; color: #808080;">с безплатна доставка</span>';
                            }

                            echo $delivery_text;
                        }
					}
				?>
                        </div>
		</FORM>
		
	</div>
