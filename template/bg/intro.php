<style>
    .bx-viewport {
        max-height: 664px;
    }
    .adv-text {
        position: absolute;
        padding: 10px;
        z-index: 3;
    }
    .adv-place-1 {
        top: 20px;
        left: 0px;
        text-align: left;
    }
    .adv-place-2 {
        top: 20px;
        left: 50%;
        transform:translate(-50%, 0);
        text-align: center;
    }
    .adv-place-3 {
        top: 20px;
        right: 0px;
        text-align: right;
    }

    .adv-place-4 {
        top: calc(50% - 25px);
        left: 0px;
        text-align: left;
    }
    .adv-place-5 {
        top: calc(50% - 25px);
        left: 50%;
        transform:translate(-50%, 0);
        text-align: center;
    }
    .adv-place-6 {
        top: calc(50% - 25px);
        right: 0px;
        text-align: right;
    }

    .adv-place-7 {
        bottom: 20px;
        left: 0;
        text-align: left;
    }
    .adv-place-8 {
        bottom: 20px;
        left: 50%;
        transform:translate(-50%, 0);
        text-align: center;
    }
    .adv-place-9 {
        bottom: 20px;
        right: 0;
        text-align: right;
    }
    .adv-text a {
        color: inherit;
        font-size: inherit;
        font-family: inherit;
    }
</style>
<div class="big-banners">
    <div class="banners-slider">
    <?php
    if(($intro_banners)&&(sizeof($intro_banners)>0)){
        echo '<ul id="slider">';
        $n = 1;
        foreach($intro_banners as $intro_baner){
            if ($intro_baner->isOnlyPartners()) {
                if (!isset($__user)) continue;

                if (isset($__user) && !$__user->is_partner()) continue;
            }

            echo '<li class="panel"><div style="position:relative;">';
            $adv_texts = unserialize($intro_baner->getTexts());

            if(count($adv_texts) && is_array($adv_texts)){
                foreach($adv_texts as $text){
                    if($text['content'] != "") {
                        list($r, $g, $b) = sscanf($text['bgcolor'], "#%02x%02x%02x");

                        echo '<div class="adv-text adv-place-'.$text['place'].'" style="background-color: rgba('.$r.', '.$g.', '.$b.', ';
                        if((int)$text['opacity'] == 100) echo '1';
                        else echo '0.'.$text['opacity'];
                        echo '); color: '.$text['color'].';">';
                        echo $text['content'];
                        echo '</div>';
                    }
                }
            }
            echo '<img id="bnr-big-'.$n.'"style="width:100%;margin:0px;height:100%;min-height: 500px;" src="'.url.$intro_baner->getImage().'" alt="' . $pageTitle . '"/>';
            //echo '<div class="bnr-mask"></div>';
            echo '<a title="' . $pageTitle . '" class="banner_item" style="width:100%; height: 100%; position: absolute; top: 0; left: 0;margin:0px; z-index: 2"';
            if($intro_baner->getDistinationURL()!='') echo ' href="'.$intro_baner->getDistinationURL();
            echo '"></a>';
            echo '</div></li>';
            $n++;
        }
        echo '</ul><div id="nav"></div>';
    }
    ?>
    </div>
</div>
<div id="FourBannersBox">
    <?php
    $stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=3 order by `id` DESC');
    $stm -> execute();
    $banners = $stm -> fetchAll();
    shuffle ($banners);
    $banners = array_slice($banners, 0, 3);
    $news=null;
    $last_element = '';
    foreach ($banners as $v)
    {
        if ($v === end($banners)) {
            $last_element = 'last';
        }
        echo '<div class="'.$last_element.' four-banners-bnr"><div><a href="'.htmlspecialchars($v['url']). '">';
        echo '<img id="thrban_'.$v['id'].'" src="'. url.htmlspecialchars( $v['image']) . '" style="width:100%" alt="' . $pageTitle . '"/>';
        if($v['adv_text_' . lang_prefix]) {
            echo '<div class="thrban_info" id="thrban_info_' . $v['id'] . '"><p>' . $v['adv_text_' . lang_prefix] . '</p></div>';
        }
        echo '</a></div></div>';
    }
    $array = array();
    ?>
    <div class="clear"></div>
</div>

</center>
<div id="theBox">
    <div id="theBoxLeftCol" style="font-size: 12px; padding-top: 20px;">
        <?php
        $stm = $pdo->prepare('SELECT * FROM `news` ORDER BY `news_date` DESC LIMIT 4');
        $stm -> execute();
        $rez = $stm -> fetchAll();
        $news=null;
        foreach ($rez as $n)
        {
            $news[]= new news($n);
        }
        foreach($news as $n)
        {
            $date = new DateTime($n->getDate());
            echo "<div class='news_box_intro'>";
            echo "<div class='datetimeintro'><a href='index.php?newid=".$n->getId()."'><span style='font-size: 13px; font-weight: bold; color: #333;'>".$n->getTitle()."</span></a></div>";
            echo "<div class='new_text_intro'><a href='index.php?newid=".$n->getId()."'>".mb_substr(strip_tags($n->getText()), 0, 200)."... <span style='font-size: 10px; color: #333;'>прочети още</span></a></div>";
            echo "</div>";
        }
        ?>
    </div>
	<div id="theBoxRightCol">
		<div style="clear: both;"></div>
        <?php if (count($artikuli) > 0): ?>
            <?php //show text ?>
        <?php endif; ?>
		<div id="theBoxRightCol-new"></div>
		<div style="clear: both; height: 20px;"></div>
        <?php if ($promoProductsCount > 0): ?>
            <?php //show text ?>
        <?php endif; ?>
		<div id="theBoxRightCol-promo"></div>
	</div>
	<div style="clear: both;"></div>
</div>
<center>




