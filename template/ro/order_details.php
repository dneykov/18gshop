<?php
$page_current = 'order_details';
?>
<div>
    <?php
    if (isset($__user)) {
        if (isset($sucessSend) && ($sucessSend == true)) {
            echo '<div style="color: #333;font-size: 16px; position: absolute;text-align: center;top: 43%;width:100%;">Comanda este acceptată cu succes înainte de a executa comanda dumneavoastră vă va contacta pentru confirmare. Starea comenzii poate ține cu ușurință evidența contului dvs., vă mulțumesc pentru cumpărături cu noi!</div>' . '</div>';
            echo '<script type="text/javascript">
													setTimeout("timeout_trigger()", 5000);
													function timeout_trigger() {
														parent.$.fancybox.close();
														parent.window.location = parent.window.location.href;
														parent.window.location.reload();
													}
													
											  </script>';
        } else if (isset($isCartEmpty) && ($isCartEmpty == true)) {
            require_once dir_root_template . 'cart.php';
        } else {
            require dir_root_template . 'order_details_form.php';
        }
    } else {
        echo '<div style="display: inline-block;color: #000;margin:10px 0;text-align:left;width: 250px;">потребителски вход</div>';
        require dir_root_template . 'login_form.php';
    }

    ?>
</div>
<div style="clear:both;height:1px"> &nbsp;</div>
