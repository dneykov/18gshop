<script>
    (function($) {
        $(document).ready(function(){
            $('li.headlink').hover(
                function() {
                    $('div', this).css('display', 'block');
                    $('div.menucontent', this).css('display', 'inline-block');
                    $(this).css({
                        'background': '#ffffff'
                    });
                    $('a.header_link', this).css({
                        'color': '#333'
                    });

                },
                function() {
                    $('div', this).css('display', 'none');
                    $('a.header_link',this).css('color', '#333');
                    $(this).css({
                        'background': 'none'
                    });
                }
            );

            $('div.menucontent div').hover(
                function() {
                    //$(this).css('background', 'rgb(255, 103, 31)');
                    $('a.dropdownitem', this).css('color', 'rgb(255, 103, 31)');
                },
                function() {
                    //$(this).css('background', 'none');
                    $('a.dropdownitem', this).css('color', '#333');
                }
            );


            var totalWidth = 0;
            $('#main_menu ul li').each(function() {
                totalWidth += $(this).width();
            });

            $('body').css('min-width', 1260);
            $('.header').css('min-width', 1260);
            $('#main_menu a.cart').fancybox({
                'type'			: 'iframe',
                'padding'		: 0,
                'width'			: 800,
                'height'		: 800
            });
        });
    })(jQuery);
</script>
<div id="main_menu">
    <div class="menu-inner">
        <div class="lang-container-mini">
            <a class="change-lang" href="javascript:void(0)" onclick="language_set(32);">BG</a>
            <a class="change-lang" href="javascript:void(0)" onclick="language_set(37);">EN</a>
        </div>
        <div id="logoc">
            <a href="<?php echo url; ?>"><img src="<?php echo url.'images/logo_c.png';?>" alt=""></a>
        </div>
        <ul>
            <?php
            if(isset($all_kategorii)&&sizeof($all_kategorii)>0){
                foreach($all_kategorii as $tmp_kategoriq){
                    ?>
                    <li class="headlink <?php if(isset($kategoriq)&&($kategoriq->getId()==$tmp_kategoriq->getID())) echo " current";?>">
                        <a class="header_link<?php if(isset($kategoriq)&&($kategoriq->getId()==$tmp_kategoriq->getID())) echo " current";?>" href="index.php?category=<?php echo $tmp_kategoriq->getID(); ?>"><?php echo $tmp_kategoriq->getIme(); ?></a>
                        <div class="menucontent">
                            <?php
                            $tmp_vidove= $tmp_kategoriq->getVidove();

                            $t_url="index.php?category=".$tmp_kategoriq->getID();
                            foreach ($tmp_vidove as $v) {
                                echo '<div >';
                                echo '<a class="dropdownitem" href="'.$t_url.'&mode='.$v->getId().'">';
                                echo $v->getIme();

                                echo '</a>';
                                echo '</div>';
                            }
                            ?>
                            <?php if($tmp_kategoriq->getBroi_artikuli($branch=Null,$promo=true) > 0) : ?>
                                <a style="height: 30px; background-color: #333; text-align: left; display: block; margin-left: 0px; margin-right: 0px; padding-left: 10px; text-weight: normal; font-family:Verdana; line-height: 30px; margin-top: 20px;color: #fff;" href="<?php echo url.'index.php?hotOffers&category='.$tmp_kategoriq->getID(); ?>">% outlet</a>
                            <?php endif; ?>
                            <?php if($tmp_kategoriq->getBundleCount()) : ?>
                                <a style="height: 30px; background-color: #FF671F; text-align: left; display: block; margin-left: 0px; margin-right: 0px; padding-left: 10px; text-weight: normal; font-family:Verdana; line-height: 30px;color: #fff;" href="<?php echo url.'index.php?bundles&category='.$tmp_kategoriq->getID(); ?>">pachet</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <?php
                }
            }
            ?>
            <li class="headlink"><a class="brands-popup" href="fancy_brands.php" title="марки" style="color: #01A0E2;">BRANDS</a></li>
            <li class="headlink">
                <a title="намаления" href="javascript:void(0)" style="font-size:18px;font-family:Arial;color: rgb(255, 103, 31);font-weight: bold; padding: 0 14px; margin: 0;"> % </a>
                <div class="menucontent" style="left: -132px; width: 240px; top: 46px;">
                    <div>
                        <a class="dropdownitem" style="text-align: right; position: relative; right: 100px;" href="<?php echo url; ?>index.php?hotOffers">% outlet</a>
                    </div>
                    <div>
                        <a class="dropdownitem" style="text-align: right; position: relative; right: 100px;" href="<?php echo url; ?>index.php?bundles">% pachet</a>
                    </div>
                </div>
            </li>

        </ul>
        <a class="cart" href="fancy_login.php?p=cart" style="float: right;text-decoration:none;height:35px;padding: 0 10px 2px 0;">
            <div style="display:inline-block;height:22px; padding-top: 6px; float: left;">
                <img src="images/icons/card.png" style="height: 22px;width: 30px;"/>
            </div>
            <div style="display:inline-block;font-size: 12pt;color:#333;height:35px; line-height: 35px;">
                <?php

                if(isset($_SESSION['cart'])){
                    $count=0;
                    foreach($_SESSION['cart'] as $key => $val){
                        $count+=$_SESSION['cart'][$key]['count'];
                    }
                    echo $count ? $count : '0';
                }else{
                    echo '0';
                }
                ?>
            </div>
            <div style="clear: both;"></div>
        </a>
    </div>
</div>
<?php
$stm = $pdo->prepare('SELECT * FROM `header_links` WHERE `type` = "center" ');
$stm -> execute();
$headerText = $stm->fetch();
?>
<?php if(!empty($headerText['text_'.lang_prefix])) : ?>
    <div class="header-text">
        <div class="inner-header-text">
            <?php
            if(!empty($headerText['url_'.lang_prefix])) :
                echo '<a href="'.$headerText['url_'.lang_prefix].'">'.$headerText['text_'.lang_prefix].'</a>';
            else :
                echo $headerText['text_'.lang_prefix];
            endif;
            ?>
        </div>
    </div>
<?php endif; ?>
