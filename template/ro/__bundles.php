<?php
function print_packet($v)
{
    ?>

    <div class="proddiv">
        <a href="<?php echo url; ?>index.php?gid=<?php echo $v->getId(); ?>" title="<?php echo $v->getName(); ?>">
            <div class="promo">
                <div style="background-color: #FF671F; width: 16px; height: 40px; color: #fff;">
                    <span class="rotate-90"><?php echo "-".round($v->getPercent(),0)."%"; ?></span>
                </div>
            </div>
            <div class="prodimgdiv">
                <table class='imageCenter'>
                    <tr>
                        <td><img alt="<?php echo $v->getName(); ?>" src="<?php echo url . $v->getKartinka_t(); ?>"
                                 border="0"/></td>
                    </tr>
                </table>

            </div>

            <div class="prod1">
                <div class="prodnamebox">
                    <span class="prodcat"><?php echo $v->getName(); ?></span>
                </div>
                <div class="prodpricebox">
                    <span class="prodpricenormal"> <span
                                style="color:#424242;font-size:12pt;font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getPrice_normal() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup
                                    style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><?php echo lang_currency_append; ?></span></span>
                    <span class="prodpricepromo"
                          style="font-size:15pt;font-style: italic;color:rgb(255, 103, 31);"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getPrice() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><?php echo lang_currency_append; ?></span>
                </div>
            </div>
        </a>
    </div>
    <?php
}

?>


<div style="overflow:hidden; min-height: 400px;" class="products">
    <?php
    if ($bundleProducts) {
        echo '<div style="overflow:hidden;" class="all_products">';
        foreach ($bundleProducts as $v) {
            print_packet($v);
        }
        echo '</div>';
    }
    ?>
</div>
<div id="scroll-to-top" style="cursor: pointer;display:none;position: fixed;right:5px;bottom:0px;z-index: 200;"><img
            src="<?php echo url; ?>/images/nav/up.png" alt=""/></div>
<script>
    $(function () {
        var scrollBtn = $("#scroll-to-top");

        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    scrollBtn.fadeIn('fast');
                } else {
                    scrollBtn.fadeOut('fast');
                }
            });

            scrollBtn.click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
    });
</script>
</div>
