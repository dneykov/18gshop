<div class="product_navigation">
	<?php
		$__url=url.'index.php';
		echo '<a style="color: #333;" href="'.$__url.'">' . $pageTitle . '</a>';
		if(isset($kategoriq)){
			echo ' <span style="color: #333;">/</span> <a style="color: #333;" href="'.$__url.'?category='.$kategoriq->getId().'">'.mb_strtolower($kategoriq->getIme(), 'UTF-8').'</a>';
			$__url=$__url.'?category='.$kategoriq->getId();
			if(isset($vid)){
				echo ' <span style="color: #333;">/</span> <a style="color: #333;" href="'.$__url.'&mode='.$vid->getId().'">'.mb_strtolower($vid->getIme(), 'UTF-8').'</a>';
				$__url=$__url.'&mode='.$vid->getId();
			}
			if(isset($model)){
				echo ' <span style="color: #333;">/</span> <a style="color: #333;" href="'.$__url.'&model='.$model->getId().'">brand '.mb_strtolower($model->getIme(), 'UTF-8').'</a>';
				$__url=$__url.'&model='.$model->getId();
			}
			if(isset($etiketi_grupi)){
				foreach ($etiketi_grupi as $v){
					if (($v->getEtiketi())){
						foreach ($v->getEtiketi() as $e){
							if ($etiket && in_array($e->getId(), $etiket)){
								echo ' <span style="color: #333;">/</span> <a style="color: #333;" href="'.$__url.'&tag[]='.$e->getId().'">'.mb_strtolower($v->getIme(), 'UTF-8').' '.mb_strtolower($e->getIme(), 'UTF-8').'</a>';
								$__url=$__url.'&tag[]='.$e->getId();
							}
						}
					}
				}
			}
		}
	?>
</div>
<?php
$url = end($__navigation_link);
?>
<div class="filter">
    <span><a href="<?php echo $url['url']; ?>&filter=last" class="<?php if(isset($_GET['filter']) && $_GET['filter'] == 'last' || !isset($_GET['filter'])) {echo 'current'; } ?>">Nou</a></span>
    <span><a href="<?php echo $url['url']; ?>&filter=low-price" class="<?php if(isset($_GET['filter']) && $_GET['filter'] == 'low-price') {echo 'current'; } ?>">Pret crescator</a></span>
    <span class="last"><a href="<?php echo $url['url']; ?>&filter=high-price" class="<?php if(isset($_GET['filter']) && $_GET['filter'] == 'high-price') {echo 'current'; } ?>">Pret descrescator</a></span>
</div>
<div class="clear"></div>
