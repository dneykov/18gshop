<style type="text/css">
	#wrapper {
		width: 1000px;
		margin: 0 auto;
		text-align: left;
	}
	#leftCol {
		width: 190px;
		float: left;
		min-height: 400px;
		padding-top: 13px;
	}
	#rightCol {
		width: 800px;
		margin-left: 10px;
		float: left;
	}
	#leftCol ul {
		margin: 0;
		padding: 0;
	}
	#leftCol ul li {
		list-style: none;
		padding-bottom: 10px;
	}
	#leftCol a {
		color: #808080;
		font-size: 13px;
	}
	#leftCol>ul>li>a{
		color: #333;
		font-size: 14px;
	}
	#leftCol>ul>li>ul>li>a{
		padding-left: 10px;
	}
	#nav-bar {
		height: 20px;
		font-size: 12px;
		padding-bottom: 20px;
		padding-left: 10px;
		width: 100%;
	}
	#nav-bar a{
		font-size: 12px;
	}
	.c-article {
		width: 720px;
		padding-left: 10px;
		padding-right: 10px;
		padding-bottom: 20px;
		display: none;
	}
/*	.c-article:nth-child(2) {
		display: block;
	}*/
	.a-name {
		display: block;
		width: 720px;
		padding-bottom: 5px;
		padding-top: 5px;
		padding-left: 25px;
		background: url(images//nav/str_i_f.png) 5px 10px no-repeat;
		padding-right: 10px;
		margin-top: 10px;
		border-bottom: 1px solid #CCC;
	}
	.a-text {
		margin-top: 10px;
	}
	.a-img {
		width: 100%;
		margin-top: 10px;
		text-align: center;
	}
	.catLink{
		display: block;
		height: 28px;
		line-height: 26px;
		padding-left: 10px;
	}
</style>
<script type="text/javascript">
	function toggleCont(x) {
		if($('.c-article').is(':visible')) {
			$('.c-article').slideUp("fast");
			$('.a-name').css({
				'background': 'url(images//nav/str_i_f.png) 5px 10px no-repeat',
				'color': '#333333'
			});
		}
		setTimeout(function(){
			if($('#a-'+x).is(":hidden")) {
	        $('#a-'+x).slideDown("fast");
	       	$('#a-link-'+x).css({
	       		'background': 'url(images//nav/str_i_down.png) 5px 10px no-repeat',
	       		'color': '#FF671F'
	       	});
	       	$('html, body').animate({ scrollTop: ($('#a-'+x).offset().top - 100) }, 'slow');
		    } else {
		        $('#a-'+x).hide();
		        $('#a-link-'+x).css('background', 'url(images//nav/str_i_f.png) 5px 10px no-repeat');
		        $('#a-link-'+x).css('color', '#333333');
		    }
		}, 200);
	}; 
</script>
<div id="wrapper">
	<div id="nav-bar" style="clear:both;
         text-align:left;
         margin-left:55px;
         margin-top: 5px;
         font-size:9pt;">
        <a href="<?php echo url; ?>"><?php echo $pageTitle; ?></a>
        <span style="color: #333;">/</span>
		<a href="<?php echo url.'index.php?help'; ?>" style="color: #333;">ajutor
		<?php
		if(isset($_GET['c'])){
			$stm = $pdo->prepare("SELECT * FROM `help_groups` WHERE `id` = ? LIMIT 1");
			$stm -> bindValue(1, (int)$_GET['c'], PDO::PARAM_INT);
			$stm -> execute();
			$cat = $stm->fetch();
			echo " / <a href='".url."index.php?help&c=".$cat['id']."'>".$cat['name_ro']."</a>";
		}
		if(isset($_GET['t'])){
			$stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `id` = ? LIMIT 1");
			$stm -> bindValue(1, (int)$_GET['t'], PDO::PARAM_INT);
			$stm -> execute();
			$t = $stm->fetch();
			echo " / <a href='".url."index.php?help&c=".$_GET['c']."&t=".$t['id']."'>".$t['name_ro']."</a>";
		}
		?>
	</div>
	<div style="padding-left: 10px;">
		<div class="f-label" style="padding-top: 2px;">ajutor</div>
        <div class="f-icon"><img src="<?php echo url; ?>images/f_info.png" alt=""></div>
	</div>
	<div style="clear: both"></div>
	<div id="leftCol">
		<ul>
            <?php
                $stm = $pdo->prepare("SELECT * FROM `help_groups` ORDER BY `id` ASC");
                $stm->execute();
                foreach($stm->fetchAll() as $v){
                	$temp = $pdo->prepare("SELECT * FROM `help_articles` WHERE `cat_id` = ?");
					$temp-> bindValue(1, (int)$v['id'], PDO::PARAM_INT);
					$temp -> execute();
					if($temp->rowCount() > 0){
	                    echo "<li>";

	                    echo "<a class='catLink' href='".url."index.php?help&c=".$v['id']."'";
	                    if(isset($_GET['c']) && ($_GET['c'] == $v['id'])){
							echo "style='color: #FF671F'";
						}
	                    echo ">".$v['name_ro']."</a>";
	                    if(isset($_GET['c']) && ($_GET['c'] == $v['id'])){
		                    $tags = $pdo->prepare("SELECT * FROM `help_tags` WHERE `cat_id` = ? ORDER BY `id` ASC");
							$tags-> bindValue(1, (int)$v['id'], PDO::PARAM_INT);
							$tags -> execute();

							echo "<ul>";
							foreach ($tags->fetchAll() as $c) {
								$tp = $pdo->prepare("SELECT * FROM `help_tags_zapisi` WHERE `tag_id` = ?");
								$tp-> bindValue(1, (int)$c['id'], PDO::PARAM_INT);
								$tp -> execute();
								if($tp->rowCount() > 0){
									echo "<li>";
									echo "<a href='".url."index.php?help&c=".$v['id']."&t=".$c['id']."' ";
									if(isset($_GET['t']) && ($_GET['t'] == $c['id'])){
										echo "style='color: #FF671F'";
									}
									echo " >".$c['name_ro']."</a>";
									echo "</li>";
								}
							}
							if($tags->rowCount() > 0){
								echo "<li><div style='height: 10px;'></div></li>";
							}
							echo "</ul>";
						}

						echo "</li>";
					}
                }
            ?>
        </ul>
	</div>
	<div id="rightCol">
		<?php 
			if(isset($_GET['c']) && !isset($_GET['t'])){
                $articles = $pdo->prepare("SELECT * FROM `help_articles` WHERE `cat_id`=? ORDER BY `id` ASC");
				$articles->bindValue(1, (int)$_GET['c'], PDO::PARAM_INT);
                $articles->execute();
                if($articles->rowCount() == 1) {
                	?>
                	<style>
                	.c-article {
                		display: block;
                	}
                	.a-name {
                		background: url(images//nav/str_i_down.png) 5px 10px no-repeat;
	       				color: #FF671F;
                	}
                	</style>
                	<?php
                }

                foreach($articles->fetchAll() as $c){
                	$url = $c['media'];
                	$type = $c['type'];

                	echo "<a href='#' onclick='toggleCont(\"".$c['id']."\")'><div class='a-name' id='a-link-".$c['id']."'>".$c['title_ro']."</div></a>";
                	echo "<div class='c-article' id='a-".$c['id']."'>";
                	if($type == "photo" && $url != ""){
                		$src = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $url );
                		echo "<div class='a-img'><img src='".url.$url."'></div>";
                	} else if($type == "video" && $url != ""){
                        function _is_youtube($url)
                        {
                            return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url));
                        }
                        function _is_vimeo($url)
                        {
                            return (preg_match('/vimeo\.com/i', $url));
                        }
                        if(_is_vimeo($url)){
                            $src = str_replace('vimeo.com/','player.vimeo.com/video/',$url);
                            echo '<iframe style="width:720px; height:405px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
                        } elseif(_is_youtube($url)) {
                            $src = str_replace('watch?v=','embed/',$url);

                            echo '<iframe style="width:720px; height:405px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
                        }
                    }
                	echo "<div class='a-text'>".$c['text_ro']."</div>";
                	echo "</div>";
                }
			} else if(isset($_GET['c']) && isset($_GET['t'])){

                $articles = $pdo->prepare("SELECT * FROM `help_tags_zapisi` WHERE `tag_id`=? ORDER BY `id` ASC");
				$articles->bindValue(1, (int)$_GET['t'], PDO::PARAM_INT);
                $articles->execute();
                if($articles->rowCount() == 1) {
                	?>
                	<style>
                	.c-article {
                		display: block;
                	}
                	.a-name {
                		background: url(images/nav/str_i_down.png) 5px 10px no-repeat;
	       				color: #FF671F;
                	}
                	</style>
                	<?php
                }
                foreach($articles->fetchAll() as $c){

                	$art = $pdo->prepare("SELECT * FROM `help_articles` WHERE `id`=? LIMIT 1");
					$art->bindValue(1, (int)$c['article_id'], PDO::PARAM_INT);
	                $art->execute();
	                $a = $art->fetch();

                	$url = $a['media'];
                	$type = $a['type'];

                	echo "<a href='#' onclick='toggleCont(\"".$a['id']."\")'><div class='a-name'>".$a['title_ro']."</div></a>";
                	echo "<div class='c-article' id='a-".$a['id']."'>";
                	if($type == "photo" && $url != ""){
                		$src = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $url );
                		echo "<div class='a-img'><img src='".url.$url."'></div>";
                	} else if($type == "video" && $url != ""){
                		$src = str_replace('watch?v=','embed/',$url);
                        echo '<iframe style="width:720px; height:405px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
                    }
                	echo "<div class='a-text'>".$a['text_ro']."</div>";
                	echo "</div>";

                }
			} else {
				$stm = $pdo->prepare("SELECT `value` FROM `dict` WHERE `key` = 'info_img' ");
				$stm -> execute();
				$i = $stm->fetch();
				$img = $i['value'];
				echo '<img src="'.url.$img.'" alt="" style="margin-top: 20px; margin-left: 35px;">';
			}
		?>
	</div>
</div>
<div style="clear: both; height: 60px;"></div>
</div>