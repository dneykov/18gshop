<?php
$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :preorder LIMIT 1");
$stm->bindValue(':preorder', "bank_details_" . lang_prefix, PDO::PARAM_STR);
$stm->execute();
$bankDetailsDb = $stm->fetch();
$bankDetails = $bankDetailsDb['value'];

$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :iban LIMIT 1");
$stm->bindValue(':iban', 'iban', PDO::PARAM_STR);
$stm->execute();
$ibanDb = $stm->fetch();
$iban = $ibanDb['value'];
?>
<style type="text/css">
    #CostsBox {
        clear: both;
    }
    #payment, #delivery_method {
        margin-top: 10px;
        text-align: left;
        display: inline-block;
        vertical-align: top;
    }
    #payment {
        margin-right: 40px;
        float: right;
        margin-bottom: 25px;
    }
    #payment #details {
        display: none;
    }
    #payment span, #payment label, #delivery_method span, #delivery_method label {
        font-size: 13px;
    }
    #payment span, #delivery_method span {
        display: inline-block;
        margin-bottom: 5px;
    }
    #payment label, #delivery_method label {
        display: inline-block;
        margin-right: 5px;
    }
    #payment #details, #payment #details > div, #payment #details strong {
        font-size: 13px;
    }
    @media screen and (max-width: 630px){
        .container-options {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -moz-box-orient: vertical;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            /* optional */
            -webkit-box-align: start;
            -moz-box-align: start;
            -ms-flex-align: start;
            -webkit-align-items: flex-start;
            align-items: flex-start;
        }

        .container-options #payment {
            -webkit-box-ordinal-group: 2;
            -moz-box-ordinal-group: 2;
            -ms-flex-order: 2;
            -webkit-order: 2;
            order: 2;
        }

        .container-options #delivery_method {
            -webkit-box-ordinal-group: 1;
            -moz-box-ordinal-group: 1;
            -ms-flex-order: 1;
            -webkit-order: 1;
            order: 1;
        }
    }
    @media screen and (max-width: 375px){
        #payment {
            margin-right: 0;
        }
    }
</style>
<script>
    $(document).ready(function(){
        $('#country').change(function() {
            var id = $('#country').val();
            var cost = $('#ajax_cost').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_delivery_ro.php",
                data: "id="+id+"&cost="+cost,
                cache: false,
                success: function(html){
                    $('#ajax_delivery').html(" ");
                    $('#ajax_delivery').append(html);
                }
            });
        });

        $('input[name=payment_type]').change(function() {
            if ($(this).val() == "bank") {
                $("#payment #details").show();
            } else {
                $("#payment #details").hide();
            }
        });

        $('input[name=delivery_method]').change(function() {
            if ($(this).val() == "courier") {
                $("#ajax_delivery #allcost").show();
            } else {
                $("#ajax_delivery #allcost").hide();
            }
        });

        $("#register").click(function () {
            $("#register").hide();
            $(".error").hide();
        });

        $('#individual-form input :enabled').prop("disabled", true);
        if($("input[name=account_type]:checked").val() == '0') {
            $("#legal-form").hide();
            $("div.cart-price").css("margin-top", "0px");
            $("#legal-form :input").prop("disabled", true);
            $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
        } else if($("input[name=account_type]:checked").val() == '1') {
            $("#legal-form").show();
            $("div.cart-price").css("margin-top", "-30px");
            $("#legal-form :input").prop("disabled", false);
        }
        $("input[name=account_type]").change(function () {
            if($(this).val() == '0') {
                $("#legal-form").hide();
                $("div.cart-price").css("margin-top", "0px");
                $("#legal-form :input").prop("disabled", true);
                $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
            } else {
                $("#legal-form").show();
                $("div.cart-price").css("margin-top", "-30px");
                $("#legal-form :input").prop("disabled", false);
            }
        });
    });
</script>
<style type="text/css">
    .registrationDetails table tr{
        margin:10px 0;
    }
    .registrationDetails table tr td{
        font-size: 9pt;
    }
    .left-side input, .right-side input{
        width:100%;
        margin-right: 10px;
        margin-bottom: 5px;
        padding: 5px;
    }
    .need {
        color: red;
    }
    .error {
        margin: 10px 10px 0 10px;
    }
    #legend {
        font-size: 11px;
    }
    .left-side {
        width: 350px;
        padding: 0;
        margin: 0 50px 0 0;
        float: left;
    }
    .row-half {
        width: 170px;
        display: block;
        float: left;
    }
    .right-side .row-half {
        width: 147px;
        display: block;
        float: left;
    }
    .row-half:first-child {
        margin-right: 10px;
    }
    form label {
        font-size: 13px !important;
        vertical-align: text-bottom;
        font-family: Verdana;
        height: 18px;
        display: inline-block;
        margin-bottom: 2px;
        margin-right: 10px;
    }
    .right-side {
        width: 305px;
        float: left;
    }
    .row {
        clear: both;
    }
    div.three-fourth {
        width: 237px;
        margin-right: 10px;
        float: left;
    }
    div.one-fourth {
        width: 58px;
        float: left;
    }
    div.one-fifth {
        width: 69px;
        margin-right: 15px;
        float: left;
    }
    div.one-fifth input {
        width: 100%;
    }
    div.one-fifth:nth-child(3n) {
        margin-right: 0;
    }
    input[type=radio] {
        margin-right: 0;
        margin-bottom: 0;
        height: 18px;
    }
    .top-row {
        margin-bottom: 10px;
    }
    .top-row span {
        display: block;
        float: left;
        font-size: 13px;
        line-height: 18px;
    }
    #legal-form {
        display: none;
    }

    input[name=edit_mail],
    input[name=gsm],
    input[name=cumparator],
    input[name=iban],
    input[name=bank],
    input[name=street2],
    input[name=postcode] {
        width:100%;
    }

    .terms {
        margin-top: 0;
        clear: left;
        float: right;
        margin-right: 78px;
    }

    .clear {
        clear: both;
    }

    input:hover, input:focus {
        border: 1px solid #FF671F;
    }

    .left-side div.three-fourth {
        width: 282px;
    }
    .loader {
        background-image: url("images/ajax-loader.gif") !important;
    }
    #payment {
        width: 305px;
        margin-right: 75px;
    }
</style>
<script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
<div class="registrationDetails">
    <form action="fancy_login.php?p=ordrdetails" method="post" style="margin:32px 10px 0;">
        <div class="top-row">
            <input type="radio" id="individual" style="width: auto;" name="account_type" value="0" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '0') echo "checked"; ?> />
            <label for="individual">Individual person</label>
            <input type="radio" id="legal_entity" style="width: auto;" name="account_type" value="1" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '1') echo "checked" ?> />
            <label for="legal_entity">Legal entity</label>
        </div>
        <div id="individual-form">
            <div class="left-side">
                <div class="row">
                    <div class="row-half">
                        <label for="name">prenume<span class="need">*</span></label>
                        <input type="text" class="always-disabled" autocomplete="off" disabled name="register_name" value="<?php if(isset($_POST['register_name'])) echo $_POST['register_name']; else echo $__user->getName(); ?>" />
                    </div>
                    <div class="row-half">
                        <label for="name">nume<span class="need">*</span></label>
                        <input type="text" class="always-disabled" autocomplete="off" disabled name="register_name_last" value="<?php if(isset($_POST['register_name_last'])) echo $_POST['register_name_last']; else echo $__user->getName_last(); ?>" />
                    </div>
                </div>
                <div class="row">
                    <label for="name">e-mail<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="edit_mail" value="<?php if(isset($_POST['edit_mail'])) echo $_POST['edit_mail'];else{echo $__user->getMail();} ?>"/></td>
                </div>
                <div class="row">
                    <label for="name">telefon<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm'];else{echo $__user->getPhone_mobile();} ?>" />
                </div>
                <div class="row">
                    <label for="name">cometariu</label>
                    <textarea name="comment" style="width:350px;height:70px; margin-right: 20px;"><?php if(isset($_POST['comment'])) echo $_POST['comment']; ?></textarea>
                </div>
            </div>
            <div class="right-side">
                <div class="row">
                    <label for="country">country<span class="need">*</span></label>
                    <select id="country" name="country" style=" width: 305px; margin-bottom: 15px;">
                        <?php
                        $stm = $pdo -> prepare("SELECT * FROM `locations`");
                        $stm -> execute();
                        foreach ($stm -> fetchAll() as $c) {
                            echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input style="width:305px;" type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; else{echo $__user->getAdress_town();} ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">strada<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; else{echo $__user->getAdress_street();} ?>" />
                    </div>
                    <div class="one-fourth">
                        <label for="name">numar<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; else{echo $__user->getAdress_number();} ?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="one-fifth">
                        <label for="name">bl.</label>
                        <input type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" />
                    </div>
                    <div class="one-fifth">
                        <label for="name">et.</label>
                        <input type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" />
                    </div>
                    <div class="one-fifth">
                        <label for="name">ap.</label>
                        <input type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; else{echo $__user->getAdress_ap();}?>" />
                    </div>
                </div>
            </div>
        </div>
        <div id="legal-form">
            <div class="left-side" style="margin-top: 20px;">
                <div class="row">
                    <label for="name">company name<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="company_name" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name']; else{echo $__user->getCompanyName();} ?>"/>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input type="text" autocomplete="off" name="company_city" value="<?php if(isset($_POST['company_city'])) echo $_POST['company_city']; else{echo $__user->getCompanyCity();} ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">street<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_street" value="<?php if(isset($_POST['company_street'])) echo $_POST['company_street']; else{echo $__user->getCompanyStreet();} ?>" />
                    </div>
                    <div class="one-fourth">
                        <label for="name">number</label>
                        <input type="text" autocomplete="off" name="company_number" value="<?php if(isset($_POST['company_number'])) echo $_POST['company_number']; else{echo $__user->getCompanyNumber();} ?>" />
                    </div>
                </div>
            </div>
            <div class="right-side" style="margin-top:20px;">
                <div class="row">
                    <label for="name">f.l.p<span class="need">*</span></label>
                    <input type="text" autocomplete="off" name="company_mol" value="<?php if(isset($_POST['company_mol'])) echo $_POST['company_mol']; else{echo $__user->getCompanyMol();} ?>" />
                </div>
                <div class="row">
                    <div class="row-half">
                        <label for="name">bulstat<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_eik" value="<?php if(isset($_POST['company_eik'])) echo $_POST['company_eik']; else{echo $__user->getCompanyEik();} ?>" />
                    </div>
                    <div class="row-half">
                        <label for="name">vat<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_vat" value="<?php if(isset($_POST['company_vat'])) echo $_POST['company_vat']; else{echo $__user->getCompanyVat();} ?>" />
                    </div>
                </div>
                <div class="row">
                    <label for="name">phone</label>
                    <input type="text" autocomplete="off" name="company_phone" value="<?php if(isset($_POST['company_phone'])) echo $_POST['company_phone']; else{echo $__user->getCompanyPhone();} ?>" />
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="delivery_method">
            <span>Delivery type:</span>
            <br>
            <input type="radio" id="delivery_courier" name="delivery_method" value="courier" <?php if(isset($_POST['delivery_method']) && $_POST['delivery_method'] == 'courier') echo 'checked'; else if(!isset($_POST['delivery_method'])) { echo 'checked'; } ?> />
            <label for="delivery_courier">With courier</label>
            <input type="radio" id="delivery_pickup" name="delivery_method" value="pickup" <?php if(isset($_POST['delivery_method']) && $_POST['delivery_method'] == 'pickup') echo 'checked'; ?> />
            <label for="delivery_pickup">Pickup from us</label>
        </div>
        <div id="payment">
            <span>Payment type:</span>
            <br>
            <input type="radio" id="payment_cash" name="payment_type" value="cash" <?php if(isset($_POST['payment_type']) && $_POST['payment_type'] == 'cash') echo 'checked'; else if(!isset($_POST['payment_type'])) { echo 'checked'; } ?> />
            <label for="payment_cash">Pay at delivery</label>
            <?php if($iban != "") : ?>
                <input type="hidden" name="iban" value="<?php echo $iban; ?>"/>
                <input type="hidden" name="bank_details" value="<?php echo $bankDetails; ?>"/>
                <input type="radio" id="payment_bank" name="payment_type" value="bank" <?php if(isset($_POST['payment_type']) && $_POST['payment_type'] == 'bank') echo 'checked'; ?> />
                <label for="payment_bank">Bank transfer</label>
                <br>
                <br>
                <div id="details" <?php if(isset($_POST['payment_type']) && $_POST['payment_type'] == 'bank') echo 'style="display: block;"'; ?>>
                    <strong>IBAN: </strong>
                    <span><?php echo $iban; ?></span>
                    <br>
                    <div><strong>Bank details</strong></div>
                    <div><?php echo nl2br($bankDetails); ?></div>
                </div>
            <?php endif; ?>
        </div>
        <input type="hidden" name="buy" value="buy"/>
        <div id="CostsBox">
            <?php
            $allcost=0;
            $allPreorder=0;
            if(isset($_SESSION['cart'])){
                foreach ($_SESSION['cart'] as $key => $val) {
                    if ($_SESSION['cart'][$key]) {
                        $artikul = new artikul((int)$key);
                        if($artikul->isPreorder()) {
                            $allPreorder += $_SESSION['cart'][$key]['preorder']['deposit_price'] * $_SESSION['cart'][$key]['count'];
                        } else {
                            $allPreorder += $_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count'];
                        }

                        $allcost += $_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count'];
                    } else {
                        $group = new paket((int) $key);
                        $allcost+=$group->getPrice()*$_SESSION['cart'][$key]['count'];
                        $allPreorder+=$group->getPrice()*$_SESSION['cart'][$key]['count'];
                    }
                }
            }
            $total=number_format($allPreorder / CURRENCY_RATE, 2, '.', '');
            $total= explode('.',$total);
            ?>
            <input type="hidden" id="ajax_cost" value="<?php echo $allPreorder / CURRENCY_RATE; ?>">
            <span id="allcost" style="color: #424242; margin-top:0px;font-size:13px;width:185px;float:right;">total <span style="font-style:italic; font-size: 18px;"><?php echo $total[0]; ?></span> <sup style="font-style:italic;font-size:8pt;"><?php echo $total[1]; ?></sup> Lei</span><br />
            <div style="clear: both;"></div>
            <?php
            $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
            $ds->execute();
            $d = $ds->fetch();
            $dostavka_cena = $d['delivery_price'];
            $dostavka_free = $d['free_delivery'];

            ?>
            <div id="ajax_delivery">
                <?php
                if($dostavka_free>$allcost){
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $dostavka_cena +=  $_SESSION['cart']['total_additional_delivery'];
                    }

                    $tmp=number_format($dostavka_cena / CURRENCY_RATE, 2, '.', '');
                    $tmp= explode('.',$tmp);
                    ?>
                    <span id="allcost" style="margin-top:0px;font-size:10pt;width:188px;float:right; margin-bottom: 5px; color: #808080;">delivery <span style="font-style:italic;"><?php echo $tmp[0]; ?></span><sup style="font-style:italic;font-size:8pt;"><?php echo $tmp[1]; ?></sup> Lei</span>
                    <?php
                }else{
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $tmp=number_format($_SESSION['cart']['total_additional_delivery'] / CURRENCY_RATE, 2, '.', '');
                        $tmp= explode('.',$tmp);
                        $delivery_text = '<span id="allcost" style="margin-top:0px;font-size:11pt;width:188px;float:right; margin-bottom: 5px; color: #808080;">transport <span style="font-style:italic;">' . $tmp[0] . '</span><sup style="font-style:italic;font-size:8pt;">' . $tmp[1] . '</sup> Lei</span>';
                    } else {
                        $delivery_text = '<span id="allcost" style="margin-top:0px;font-size:11px;width:160px;float:right; margin-bottom: 5px; color: #808080;">transport gratuit</span>';
                    }

                    echo $delivery_text;
                }
                ?>
            </div>
        </div>
        <div style="margin-top:30px;">
            <input  id="register" style="width:180px;float:right;margin: 0 35px 0 0;height: 36px; line-height: 32px;" type="submit" name="submit" value="checkout" class="submit">
            <div id="form-loader" style="width: 180px; float: right; margin: 0 35px 0 0; height: 36px; line-height: 32px; background: #FF671F url(images/ajax-loader.gif) no-repeat center center; position: absolute; right: 10px; z-index: -1;"></div>
        </div>
    </form>
    <?php
    $tmp_msg='';
    if((isset($user_edit_error))&&($user_edit_error)) foreach ($user_edit_error as $v) {
        $tmp_msg= $tmp_msg.', '.$v;
    } ?>
    <?php if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
        $tmp_msg= $tmp_msg.', '.$v;
    }
    $tmp_msg=trim($tmp_msg,', ');
    if(!empty($tmp_msg)){
        echo '<div class="error">';
        echo $tmp_msg;
        echo '</div>';
    }
    ?>
</div>
