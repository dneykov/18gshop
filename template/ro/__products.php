<?php

function print_product($v)
{
    global $__user;
    $isAvaliable = $v->isAvaliable();
    $isOnline = $v->isOnline();
    ?>

    <div class="proddiv">
        <a href="<?php echo url; ?>index.php?id=<?php echo $v->getId() . remove_querystring_var('&' . $_SERVER['QUERY_STRING'], 'id'); ?>"
           title="<?php echo $v->getIme_marka() . ' ' . $v->getIme(); ?>">
            <?php
            $brnd = $v->getBrand();
            if ($brnd->getBWImage() != "") {
                ?>
                <div class="brand_image"><img src="<?php echo $brnd->getBWImage(); ?>" alt="<?php echo $v->getIme(); ?>"
                                              class="brand_img"/></div>
                <?php
            }
            ?>
            <div class="promo">
                <?php if ($isAvaliable && $isOnline) : ?>
                    <?php
                    if (($v->getCena_promo())) {
                        $tmpraz=($v->getCena() - $v->getCena_promo() );
                        if ($v->IsNew()) {
                            echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff;right: 20px; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                        } else {
                            echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                        }
                    }

                    if($v->getAddArtikuli() != ""){
                        echo '<div style="float: left; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <img src="'.url.'images/icons/gift.png" alt="promo" />
                                    </div>';
                    }

                    if ($v->isPreorder() != "") {
                        echo '<div style="float: left; background-color: #29AC92; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90">PRE</span>
                                    </div>';
                    }

                    if ($v->IsNew()) {
                        echo '<div style="float: left; background-color: #01A0E2; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90" style="font-size: 11px; padding-top: 17px;">NEW</span>
                                    </div>';
                    }
                    ?>
                <?php else : ?>
                    <span style="color: #ff671f; display: inline-block; margin-left: 30px; font-size: 11px; padding-top: 20px; text-transform: uppercase;">разпродадено</span>
                <?php endif; ?>
            </div>
            <div class="prodimgdiv">
                <table class='imageCenter'>
                    <tr>
                        <td><img alt="<?php echo $v->getIme_marka() . ' ' . $v->getIme(); ?>"
                                 src="<?php echo url . $v->getKartinka_t(); ?>" border="0"/></td>
                    </tr>
                </table>

            </div>

            <div class="prod1">
                <div class="prodnamebox" style="height: 35px; width: 130px;">
                    <span class="prodname"><?php echo $v->getIme_marka(); ?></span>
                    /
                    <span class="prodcat"><?php echo $v->getIme(); ?></span>
                </div>
            </div>

            <div class="proddiv-specification" style="float: left; width: 130px;">
                <?php
                $linesSpecification = explode(PHP_EOL, $v->getSpecification());
                $newLinesSpecification = implode(PHP_EOL, array_slice($linesSpecification, 0, 5)) . PHP_EOL;
                echo nl2br($newLinesSpecification);
                ?>
            </div>

            <!--<div ><font class="prodcode"><?php echo $v->getKod(); ?></font></div>-->
            <div class="prodpricebox" style="width: 110px;">
                <?php if (isset($__user) && $__user->is_partner()) : ?>
                    <?php if ($v->getCenaDostavna() > 0) : ?>
                        <span class="prodpricepromo"
                              style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCenaDostavna() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                    <?php else : ?>
                        <?php if ($v->getCena_promo()) : ?>
                            <span class="prodpricenormal"> <span
                                        style="color:#424242;font-size:12pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup> Lei</span></span>
                            <span class="prodpricepromo"
                                  style="font-size:15pt;font-style: italic;color:#ff671f;"><?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                        <?php else : ?>
                            <span class="prodpricepromo"
                                  style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if ($v->getCena_promo()) : ?>
                        <span class="prodpricenormal"> <span
                                    style="color:#424242;font-size:12pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup> Lei</span></span>
                        <span class="prodpricepromo"
                              style="font-size:15pt;font-style: italic;color:#ff671f;"><?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                    <?php else : ?>
                        <span class="prodpricepromo"
                              style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </a>
    </div>


    <?php
}

?>


<div style="overflow:hidden;" class="products">
    <?php
    if (isset($artikuli) and (sizeof($artikuli) > 0) && (isset($artikul))) {
        ?>
        <div style="position: relative;background: none repeat scroll 0 0 #FFFFFF;color:#808080;text-align: left;">
            <div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">
                <?php
                if (isset($kategoriq)) {
                    echo mb_strtolower($kategoriq->getIme(), 'UTF-8');
                    if (isset($vid)) {
                        echo ' ' . mb_strtolower($vid->getIme(), 'UTF-8');
                    }
                    if (isset($model)) {
                        echo ' brand ' . mb_strtolower($model->getIme(), 'UTF-8');
                    }
                    if (isset($etiketi_grupi)) {
                        foreach ($etiketi_grupi as $v) {
                            if (($v->getEtiketi())) {
                                foreach ($v->getEtiketi() as $e) {
                                    if ($etiket && in_array($e->getId(), $etiket)) {
                                        echo ' ' . mb_strtolower($v->getIme(), 'UTF-8') . ' ' . mb_strtolower($e->getIme(), 'UTF-8');
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
            </div>
            <div style="display: block;height: 5px;margin: -14px 0 14px 0px;position: relative;"></div>
        </div>
        <?php
    }
    if (!$artikuli) { ?><?php } else { ?>

        <?php
        echo '<div style="overflow:hidden;" class="all_products">';
        foreach ($artikuli as $v) {
            if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

            if (isset($__user) && !$__user->is_partner() && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

            print_product($v);

        } // foreach artikuli
        echo '</div>';
    } // ako ima artikuli


    $need_position = $min_prod_per_page - $artikul_broi;
    if ($artikul_broi < $min_prod_per_page) {
        if (isset ($_GET['hotOffers'])) {
            $tmppromo = true;
        } else {
            $tmppromo = false;
        }
        if (isset($etiket)) {
            if (isset($kategoriq) && isset($vid) && isset($model)) {
                $add_artikuli = getArticuli($kategoriq, $vid, NULL, $need_position, $artikuli_idta, $etiket, 'id', $tmppromo);
            }
        } else {
            if (isset($kategoriq) && isset($vid) && isset($model)) {
                $add_artikuli = getArticuli($kategoriq, $vid, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
                if (!isset($add_artikuli) || ($add_artikuli['count'] <= 0)) {
                    $add_artikuli = getArticuli($kategoriq, NULL, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
                }
            } else if (isset($kategoriq) && isset($vid)) {
                $add_artikuli = getArticuli($kategoriq, NULL, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
            } else if (isset($kategoriq) && isset($model)) {
                $add_artikuli = getArticuli($kategoriq, NULL, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
            }
        }

        if (isset($add_artikuli)) {
            $need_position -= $add_artikuli['count'];
        }

    }
    if (isset($add_artikuli) && ($add_artikuli['count'] > 0)) {
        echo '<div style="clear: both; height:60px;"></div><div style="position: relative;background: none repeat scroll 0 0 #FFFFFF;color:#808080;text-align: left;"><div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">' . $add_artikuli['title'] . '</div><div style="display: block;height: 5px;margin: -14px 0 14px 0px;position: relative;"> </div></div>';
        foreach ($add_artikuli['itams'] as $v) {
            if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

            if (isset($__user) && !$__user->is_partner() && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

            print_product($v);
        }
    }

    if (($need_position > 0) && (sizeof($last_artikuli) > 0)) {
        echo '<div style="clear: both; height:60px;"></div><div style="position: relative;background: none repeat scroll 0 0 #FFFFFF;color:#808080;text-align: left;"><div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">newest products</div><div style="display: block;height: 5px;margin: -14px 0 14px 0px;position: relative;"> </div></div>';
        foreach ($last_artikuli as $v) {
            if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

            if (isset($__user) && !$__user->is_partner() && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

            print_product($v);
        }
    }


    ?>
</div>
<div id="scroll-to-top" style="cursor: pointer;display:none;position: fixed;right:5px;bottom:0px;z-index: 200;"><img
            src="<?php echo url; ?>/images/nav/up.png" alt=""/></div>
<script>
    $(function () {
        $('.all_products').endlessPager('.pager', '.proddiv', 1300, '.all_products');


        var scrollBtn = $("#scroll-to-top");

        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    scrollBtn.fadeIn('fast');
                } else {
                    scrollBtn.fadeOut('fast');
                }
            });

            scrollBtn.click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
    });
</script>
</div>
