<?php
if(isset($_GET['user']) && isset($_GET['r'])){
    if(isset($__user)){
        if(isset($_POST['submit'])){
            $user_edit_error = NULL;
            $order_address_data_error = NULL;

            $username_edit_mail = $_POST['user_mail'];

            if (!filter_var($username_edit_mail, FILTER_VALIDATE_EMAIL)) $user_edit_error[] = lang_login_register_error_mail_invalid;

            if(mb_strlen($username_edit_mail) > 200) $user_edit_error[] = lang_login_register_error_mail_lenght_long;
            $stm_check_mail = $pdo->prepare("SELECT `mail` FROM `members` WHERE LOWER(`mail`) = LOWER(?) and id != ? LIMIT 1");
            $stm_check_mail -> bindValue(1, $username_edit_mail, PDO::PARAM_STR);
            $stm_check_mail -> bindValue(2, $__user->getId(), PDO::PARAM_INT);
            $stm_check_mail -> execute();
            //ако името вече го има показва грешка
            if ($stm_check_mail->rowCount() >0 ) { $user_edit_error[] = str_replace("mail@mail.com", $username_edit_mail, lang_login_register_error_mail_already_used); }

            $changepass=false;

            if($_POST['user_pass'] != "" || $_POST['user_passAgain'] != "" || $_POST['user_oldPass'] != ""){
                $input_old_pass = trim($_POST['user_oldPass']);
                if($input_old_pass == "") $user_edit_error[] = "невалидна парола";

                $stm_check_old_pass = $pdo->prepare("SELECT `password` FROM `members` WHERE `id`=? LIMIT 1");
                $stm_check_old_pass -> bindValue(1, $__user->getId(), PDO::PARAM_INT);
                $stm_check_old_pass -> execute();
                $check_old_pass = $stm_check_old_pass->fetch();
                $old_pass = $check_old_pass['password'];

                if(sha1($input_old_pass) != $old_pass) $user_edit_error[] = "невалидна стара парола";
                if(trim($_POST['user_pass']) == "") $user_edit_error[] = "невалидна нова парола";
                if(trim($_POST['user_pass']) != trim($_POST['user_passAgain'])) $user_edit_error[] = "паролите не съвпадат";
            }

            if(isset($_POST['user_pass'])&&isset($_POST['user_passAgain'])&&(trim($_POST['user_mail'])!="")&&(trim($_POST['user_passAgain'])!="")){
                if($_POST['user_pass']!=$_POST['user_passAgain']) {$user_edit_error[] = lang_error_passwords_not_match;}
                $username_edit_pass = $_POST['user_pass'];
                if(mb_strlen($username_edit_pass) < 4) $user_edit_error[] = lang_login_register_error_pass_lenght_short;
                if(mb_strlen($username_edit_pass) > 200) $user_edit_error[] = lang_login_register_error_pass_lenght_long;
                $changepass=true;
            }

            if($user_edit_error===NULL){
                if($changepass){
                    $username_edit_pass = sha1($username_edit_pass);

                    $stm = $pdo->prepare("UPDATE `members` SET `mail`=? , `password`=? WHERE id=?");
                    $stm->bindValue(1, $username_edit_mail, PDO::PARAM_STR);
                    $stm->bindValue(2, $username_edit_pass, PDO::PARAM_STR);
                    $stm->bindValue(3, $__user->getId(), PDO::PARAM_INT);

                    $stm->execute();
                } else {
                    $stm = $pdo->prepare("UPDATE `members` SET `mail`=? WHERE id=?");
                    $stm->bindValue(1, $username_edit_mail, PDO::PARAM_STR);
                    $stm->bindValue(2, $__user->getId(), PDO::PARAM_INT);
                    $stm->execute();
                }
            }

            if($_POST['account_type'] == '0') {
                $order_addr_gsm = $_POST['gsm'];
                $order_addr_city = $_POST['city'];
                $order_addr_street = $_POST['street'];
                $order_addr_number = $_POST['number'];
                $order_addr_vhod = $_POST['vhod'];
                $order_addr_etaj = $_POST['etaj'];
                $order_addr_apartament = $_POST['apartament'];

                if(mb_strlen($order_addr_gsm) < 9) $order_address_data_error[] = lang_order_address_data_error_gsm;
                if(mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

                if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
                if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

                if(mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
                if(mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

                if(mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
                if(mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;

                if(mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;
                if(mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;
                if(mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;

                if($order_address_data_error==null){
                    $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=?,`account_type`=?,`country`=? WHERE id=?");
                    $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
                    $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
                    $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
                    $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
                    $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
                    $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
                    $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
                    $stm->bindValue(8, $_POST['account_type'], PDO::PARAM_STR);
                    $stm->bindValue(9, $_POST['country'], PDO::PARAM_STR);
                    $stm->bindValue(10, $__user->getId(), PDO::PARAM_INT);

                    $stm->execute();
                    require '__members.php';
                }
            } else if($_POST['account_type'] == '1') {
                $order_addr_gsm = $_POST['gsm'];
                $order_addr_city = $_POST['city'];
                $order_addr_street = $_POST['street'];
                $order_addr_number = $_POST['number'];
                $order_addr_vhod = $_POST['vhod'];
                $order_addr_etaj = $_POST['etaj'];
                $order_addr_apartament = $_POST['apartament'];

                $company_name = $_POST['company_name'];
                $company_city = $_POST['company_city'];
                $company_street = $_POST['company_street'];
                $company_number = $_POST['company_number'];
                $company_mol = $_POST['company_mol'];
                $company_eik = $_POST['company_eik'];
                $company_vat = $_POST['company_vat'];
                $company_phone = $_POST['company_phone'];

                if (mb_strlen($order_addr_gsm) < 9) $order_address_data_error[] = lang_order_address_data_error_gsm;
                if (mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

                if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
                if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

                if (mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
                if (mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

                if (mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
                if (mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;

                if (mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;

                if (mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;

                if (mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;

                if (mb_strlen($company_name) < 3) $order_address_data_error[] = lang_order_company_name_short;
                if (mb_strlen($company_name) > 200) $order_address_data_error[] = lang_order_company_name_long;

                if (mb_strlen($company_city) < 2) $order_address_data_error[] = lang_order_company_city_error;
                if (mb_strlen($company_city) > 200) $order_address_data_error[] = lang_order_company_city_error;

                if (mb_strlen($company_street) < 2) $order_address_data_error[] = lang_order_company_street_error;
                if (mb_strlen($company_street) > 200) $order_address_data_error[] = lang_order_company_street_error;

                if (mb_strlen($company_mol) < 2) $order_address_data_error[] = lang_order_company_mol_error;
                if (mb_strlen($company_mol) > 200) $order_address_data_error[] = lang_order_company_mol_error;

                if (mb_strlen($company_eik) < 2 && mb_strlen($company_vat) < 2) $order_address_data_error[] = lang_order_company_eik_vat_error;
                if (mb_strlen($company_eik) > 200 && mb_strlen($company_vat) > 200) $order_address_data_error[] = lang_order_company_eik_vat_error;

                if($order_address_data_error==null){
                    $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=?,`country`=?,`company_name`=?,`company_city`=?,`company_street`=?,`company_number`=?,`company_mol`=?,`company_eik`=?,`company_vat`=?,`company_phone`=?,`account_type`=? WHERE id=?");
                    $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
                    $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
                    $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
                    $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
                    $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
                    $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
                    $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
                    $stm->bindValue(8, $_POST['country'], PDO::PARAM_STR);
                    $stm->bindValue(9, $company_name, PDO::PARAM_STR);
                    $stm->bindValue(10, $company_city, PDO::PARAM_STR);
                    $stm->bindValue(11, $company_street, PDO::PARAM_STR);
                    $stm->bindValue(12, $company_number, PDO::PARAM_STR);
                    $stm->bindValue(13, $company_mol, PDO::PARAM_STR);
                    $stm->bindValue(14, $company_eik, PDO::PARAM_STR);
                    $stm->bindValue(15, $company_vat, PDO::PARAM_STR);
                    $stm->bindValue(16, $company_phone, PDO::PARAM_STR);
                    $stm->bindValue(17, $_POST['account_type'], PDO::PARAM_STR);
                    $stm->bindValue(18, $__user->getId(), PDO::PARAM_INT);

                    $stm->execute();
                    require '__members.php';
                }
            }

            if($user_edit_error===NULL && $order_address_data_error==null){
                ?>
                <script type="text/javascript">
                    window.location = "<?php echo url; ?>index.php?user";
                </script>
                <?php
            }
        }
    }
}
?>
<style type="text/css">
    #cont {
        width: 1000px;
        margin: 5px auto;
        text-align: left;
        padding-bottom: 200px;
        font-size: 14px;
    }
    #nav-bar {
        height: 20px;
        font-size: 12px;
        padding-bottom: 20px;
        padding-left: 10px;
        width: 100%;
    }
    #nav-bar a{
        font-size: 12px;
    }
    #userOrders {
        margin-top: 30px;
    }
    #tabs-bar {
        height: 20px;
        width: 100%;
        padding-bottom: 2px;
        border-bottom: 1px solid #f05a24;
        margin-bottom: 20px;
    }
    #tabs-bar a{
        margin-left: 5px;
        margin-right: 20px;
        font-size: 14px;
    }
    .userDetails {
        margin-top: 20px;
        width: 930px;
        margin-left: 20px;
    }
    .userDetails table {
        border: none;
    }
    .userDetails table tr td{
        font-size: 9pt;
    }
    .userDetails input{
        width: calc(100% - 10px);
        margin-bottom: 15px;
        margin-right: 15px;
        padding: 5px;
        border: 1px solid #CCC;
    }
    .userDetails input:hover,.userDetails input:focus {
        border: 1px solid #f05a24;
    }

    label {
        display: inline-block;
        font-size: 13px;
        margin-bottom: 3px;
    }

    .userDetails select {
        border: 1px solid #CCC;
        background-color: #F1F1F1;
        padding: 5px;
        width: 100%;
    }
    .submit {
        display: inline-block;
        border: 1px solid #FF671F !important;
        background: none repeat scroll 0 0 #FF671F;
        border: medium none;
        color: #fff;
        cursor: pointer;
        float: right;
        margin: 34px 0 0;
        padding: 0 8px 2px;
        text-decoration: none;
    }
    .back {
        display: inline-block;
        background: none repeat scroll 0 0 #333;
        border: medium none;
        color: #fff;
        cursor: pointer;
        float: right;
        margin: 34px 10px 0px;
        height: 36px;
        width: 150px;
        line-height: 36px;
        text-align: center;
        text-decoration: none;
        float:right;
        font-size: 13px;
    }
    input, textarea {
        font-size: 13px;
    }
    input {
        font-size: 13px!important;
    }
    .need {
        color: red;
    }
    .error {
        margin: 20px 10px 0 10px;
        font-size: 13px;
    }
    #legend {
        margin-top: -3px;
        font-size: 11px;
    }
    .left-side {
        width: 500px;
        padding: 0;
        margin: 0 100px 0 5px;
        float: left;
    }
    .row-half {
        width: 240px;
        display: block;
        float: left;
    }
    .right-side .row-half {
        width: 152px;
    }
    .row-half:first-child {
        margin-right: 20px;
    }
    form label {
        font-size: 13px !important;
        vertical-align: text-bottom;
        font-family: Verdana;
        display: inline-block;
        margin-bottom: 2px;
        margin-right: 10px;
    }
    .right-side {
        width: 325px;
        float: left;
    }
    .row {
        clear: both;
    }
    div.three-fourth {
        width: 225px;
        float: left;
        margin-right: 20px;
    }
    div.one-fourth {
        width: 80px;
        float: left;
    }
    div.one-fifth {
        width: calc(33% - 15px);
        margin-right: 24px;
        float: left;
    }
    div.one-fifth:nth-child(3n) {
        margin-right: 0;
    }
    input[type=radio] {
        margin-right: 0;
        margin-bottom: 0;
        height: 18px;
    }
    .top-row span {
        display: block;
        float: left;
        font-size: 13px;
        line-height: 18px;
    }
    #legal-form {
        display: none;
    }
    #register {
        margin-right: 5px;
    }
    .clear {
        clear: both;
    }
    input, textarea {
        font-size: 13px;
    }
    .userDetails input:hover, .userDetails input:focus, .userDetails select:hover, .userDetails select:focus {
        border: 1px solid #FF671F;
    }
    .top-row {
        margin: 0 0 10px 7px;
    }
    .left-side div.three-fourth {
        width: 370px;
        float: left;
        margin-right: 20px;
    }
    .left-side div.one-fourth {
        width: 110px;
        float: left;
    }
    @media screen and (max-width: 780px) {

        .userDetails {
            width: calc(100% - 20px);
            margin: 0 10px;
        }

        #individual-form .left-side,
        .left-side,
        #individual-form .right-side,
        .userDetails .right-side,
        #legal-form .left-side,
        #legal-form .right-side {
            display: block;
            width: 100%;
            margin: 0 auto;
        }

        .userDetails select,
        #individual-form input,
        #legal-form input {
            width: 100% !important;
        }

        .userDetails input{
            width: calc(100% - 6px) !important;
        }

        #cont div.three-fourth, #cont div.one-fourth {
            width: 100%;
            margin-bottom: 10px;
        }

        .userDetails .row-half,
        #individual-form .row-half,
        #legal-form .row-half {
            width: calc(50% - 5px);
            margin-bottom: 10px !important;
        }
        .userDetails .row, .userDetails .one-fifth {
            margin-bottom: 10px !important;
        }
        .userDetails select {
            margin-bottom: 0 !important;
        }

        .userDetails .right-side, .userDetails .left-side {
            margin-top: 0 !important;
        }

        #cont .submit {
            margin-top: 0;
            margin-bottom: 10px;
            width: calc(100% - 16px);
            float: left;
        }
    }
</style>
<script>
    $(document).ready(function(){
        if($("input[name=account_type]:checked").val() == '0') {
            $("#legal-form").hide();
            $("div.cart-price").css("margin-top", "0px");
            $("#legal-form :input").prop("disabled", true);
            $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
        } else if($("input[name=account_type]:checked").val() == '1') {
            $("#legal-form").show();
            $("div.cart-price").css("margin-top", "-30px");
            $("#legal-form :input").prop("disabled", false);
        }
        $("input[name=account_type]").change(function () {
            if($(this).val() == '0') {
                $("#legal-form").hide();
                $("div.cart-price").css("margin-top", "0px");
                $("#legal-form :input").prop("disabled", true);
                $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
            } else {
                $("#legal-form").show();
                $("div.cart-price").css("margin-top", "-30px");
                $("#legal-form :input").prop("disabled", false);
            }
        });
    });
</script>
<div id="cont">
    <?php
    if(isset($__user)){
    ?>
    <div id="nav-bar">
        <a href="<?php echo url; ?>" style="color: #4a4942;">18gshop</a> <span style="color: #4a4942;">/</span>
        <a href="<?php echo url.'index.php?user'; ?>" style="color: #4a4942;">profile</a> <span style="color: #4a4942;">/</span>
        <a href="<?php echo url.'index.php?user'; ?>" style="color: #4a4942;"><?php echo $__user->getName(); ?></a>
        /
        <?php if($__user->is_partner()) : ?>
            <span style="color: #4a4942;">/</span>
            <span style="color: #FF671F">partner</span> <span style="color: #4a4942;">/</span>
        <?php endif; ?>
        <a href="<?php echo url.'index.php?user&r'; ?>" style="color: #4a4942;">edit</a>
    </div>
    <div id="tabs-bar">
        <a href="<?php echo url.'index.php?user'; ?>" style="color: #FF671F;">profile</a>
        <a href="<?php echo url.'index.php?user&o'; ?>" style="color: #4a4942;">orders
        <?php
        $stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ?");
        $stm->bindValue(1, $__user->getId(), PDO::PARAM_STR);
        $stm->execute();
        echo $stm->rowCount();
        ?>
        </a>
    </div>
    <span style="margin-left:5px;">Edit Profile</span>
    <div class="userDetails">
        <form method="post">
            <div class="top-row">
                <input type="radio" id="individual" style="width: auto;" name="account_type" value="0" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '0') echo "checked"; ?> />
                <label for="individual">Individual person</label>
                <input type="radio" id="legal_entity" style="width: auto;" name="account_type" value="1" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '1') echo "checked" ?> />
                <label for="legal_entity">Legal entity</label>
            </div>
            <div id="individual-form">
                <div class="left-side">
                    <div class="row">
                        <div class="row-half">
                            <label for="name">first name<span class="need">*</span></label>
                            <input type="text" autocomplete="off" class="always-disabled" disabled name="register_name" value="<?php if(isset($_POST['register_name'])) echo $_POST['register_name']; else echo $__user->getName(); ?>" />
                        </div>
                        <div class="row-half">
                            <label for="name">last name<span class="need">*</span></label>
                            <input type="text" autocomplete="off" class="always-disabled" disabled name="register_name_last" value="<?php if(isset($_POST['register_name_last'])) echo $_POST['register_name_last']; else echo $__user->getName_last(); ?>" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">e-mail<span class="need">*</span></label>
                            <input style="margin-right:0px;" type="text" autocomplete="off" name="user_mail" value="<?php if(isset($_POST['edit_mail'])) echo $_POST['edit_mail'];else{echo $__user->getMail();} ?>"/>
                        </div>
                        <div class="row-half">
                            <label for="name">GSM<span class="need">*</span></label>
                            <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm'];else{echo $__user->getPhone_mobile();} ?>" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">password</label>
                            <input type="password" autocomplete="off" name="user_oldPass">
                        </div>
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">new password</label>
                            <input type="password" autocomplete="off" name="user_pass">
                        </div>
                        <div class="row-half">
                            <label for="name">new password again</label>
                            <input type="password" autocomplete="off" name="user_passAgain">
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <div class="row">
                        <label for="country">country<span class="need">*</span></label>
                        <select id="country" name="country" style="margin-bottom: 25px;">
                            <?php
                            $stm = $pdo -> prepare("SELECT * FROM `locations`");
                            $stm -> execute();
                            foreach ($stm -> fetchAll() as $c) {
                                echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="row">
                        <label for="name">city<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; else{echo $__user->getAdress_town();} ?>"/>
                    </div>
                    <div class="row">
                        <div class="three-fourth">
                            <label for="name">street<span class="need">*</span></label>
                            <input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; else{echo $__user->getAdress_street();} ?>" />
                        </div>
                        <div class="one-fourth">
                            <label for="name">number<span class="need">*</span></label>
                            <input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; else{echo $__user->getAdress_number();} ?>" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="one-fifth">
                            <label for="name">en.</label>
                            <input type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" />
                        </div>
                        <div class="one-fifth">
                            <label for="name">fl.</label>
                            <input type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" />
                        </div>
                        <div class="one-fifth">
                            <label for="name">apt.</label>
                            <input type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; else{echo $__user->getAdress_ap();}?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="legal-form">
                <div class="left-side" style="margin-top: 30px;">
                    <div class="row">
                        <label for="name">company name<span class="need">*</span></label>
                        <input style="margin-right:0px;" type="text" autocomplete="off" name="company_name" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name']; else{echo $__user->getCompanyName();} ?>"/>
                    </div>
                    <div class="row">
                        <label for="name">city<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_city" value="<?php if(isset($_POST['company_city'])) echo $_POST['company_city']; else{echo $__user->getCompanyCity();} ?>"/>
                    </div>
                    <div class="row">
                        <div class="three-fourth">
                            <label for="name">street<span class="need">*</span></label>
                            <input type="text" autocomplete="off" name="company_street" value="<?php if(isset($_POST['company_street'])) echo $_POST['company_street']; else{echo $__user->getCompanyStreet();} ?>" />
                        </div>
                        <div class="one-fourth">
                            <label for="name">number</label>
                            <input type="text" autocomplete="off" name="company_number" value="<?php if(isset($_POST['company_number'])) echo $_POST['company_number']; else{echo $__user->getCompanyNumber();} ?>" />
                        </div>
                    </div>
                </div>
                <div class="right-side" style="margin-top:30px;">
                    <div class="row">
                        <label for="name">f.l.p<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_mol" value="<?php if(isset($_POST['company_mol'])) echo $_POST['company_mol']; else{echo $__user->getCompanyMol();} ?>" />
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">bulstat<span class="need">*</span></label>
                            <input type="text" autocomplete="off" name="company_eik" value="<?php if(isset($_POST['company_eik'])) echo $_POST['company_eik']; else{echo $__user->getCompanyEik();} ?>" />
                        </div>
                        <div class="row-half">
                            <label for="name">vat<span class="need">*</span></label>
                            <input type="text" autocomplete="off" name="company_vat" value="<?php if(isset($_POST['company_vat'])) echo $_POST['company_vat']; else{echo $__user->getCompanyVat();} ?>" />
                        </div>
                    </div>
                    <div class="row">
                        <label for="name">phone</label>
                        <input type="text" autocomplete="off" name="company_phone" value="<?php if(isset($_POST['company_phone'])) echo $_POST['company_phone']; else{echo $__user->getCompanyPhone();} ?>" />
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div style="margin-top:30px;">
                <input id="register" style="width:150px;float:right;margin: 34px 0 0;height: 36px; " type="submit" name="submit" value="update profile" class="submit">
                <a href="<?php echo url.'index.php?user'; ?>" class="back">cancel</a>
            </div>
            <div class="clear"></div>
        </form>
        <?php
        $tmp_msg='';
        if((isset($user_edit_error))&&($user_edit_error)) foreach ($user_edit_error as $v) {
            $tmp_msg= $tmp_msg.', '.$v;
        } ?>
        <?php if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
            $tmp_msg= $tmp_msg.', '.$v;
        }
        $tmp_msg=trim($tmp_msg,', ');
        if(!empty($tmp_msg)){
            echo '<div class="error">';
            echo $tmp_msg;
            echo '</div>';
        }
        ?>
    </div>
</div>
<?php
} else {
    echo "<center>Please login into your account</center>";
}
?>
</div>