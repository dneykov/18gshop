<?php
/* @var $artikul artikul */
$page_current = 'products';

if (isset($artikul)) {
    if(!isset($__user) && $artikul->getCenaDostavna() > 0 && $artikul->getCena() == 0) {
        header('Location: ' . url);
    }

    if(isset($__user) && !$__user->is_partner() && $artikul->getCenaDostavna() > 0 && $artikul->getCena() == 0) {
        header('Location: ' . url);
    }
}

if(isset($artikul) && !$artikul->isPreorder()){
    $isAvaliable = $artikul->isAvaliable();
    $checkDate = $artikul->getCheckDate();
    $nowDate = date("Y-m-d");
    if($checkDate != $nowDate){
        $tempAval = false;
        if($artikul->isAvaliable()) $tempAval = true;
        $wstm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ?');
        $wstm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
        $wstm -> execute();
        if($wstm -> rowCount() > 0){
            $here = false;
            $artikul_tag_ids = array();
            foreach ($wstm -> fetchAll() as $v) {
                $tempCode = $v['code'];
                $tempTagId = $v['tag_id'];

                $stmTagCheck = $pdo->prepare('SELECT id FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? LIMIT 1');
                $stmTagCheck -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                $stmTagCheck -> bindValue(2, $tempTagId, PDO::PARAM_INT);
                $stmTagCheck -> execute();

                $type = $v['type'];
                $stm = $pdo->prepare('SELECT id FROM `warehouse` WHERE `code` = ? LIMIT 1');
                $stm -> bindValue(1, (int)$tempCode, PDO::PARAM_INT);
                $stm -> execute();
                if($stm -> rowCount() > 0) {
                    $artikul_tag_ids[] = $tempTagId;
                    $here = true;
                } else if(is_null($tempCode) && $stmTagCheck -> rowCount() > 0) {
                    $artikul_tag_ids[] = $tempTagId;
                    $here = true;
                }
            }

            if($type == 2){
                if($artikul->getTaggroup_dropdown() != null){
                    foreach ($artikul->getTaggroup_dropdown() as $td) {
                        foreach($td->getEtiketi() as $t){
                            $stm = $pdo -> prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? ');
                            $stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                            $stm -> bindValue(2, $t->getId(), PDO::PARAM_INT);
                            $stm -> execute();
                        }
                    }
                }
                foreach ($artikul_tag_ids as $etk) {
                    if (empty($etk)) continue;
                    $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
                    $stm -> bindValue(1, $etk, PDO::PARAM_INT);
                    $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm -> execute();
                }
            }
            if($here) {
                if(count($artikul_tag_ids) > 0) {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd -> bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd -> bindValue(2, 1, PDO::PARAM_INT);
                    $upd -> bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd -> execute();

                    $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                    $stm_enable_group_product -> bindValue(1, 1, PDO::PARAM_STR);
                    $stm_enable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm_enable_group_product -> execute();

                    $isAvaliable = true;
                } else {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd->bindValue(2, 0, PDO::PARAM_INT);
                    $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd->execute();

                    $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                    $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
                    $stm_disable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm_disable_group_product -> execute();

                    $isAvaliable = false;
                }
            } else {
                if(!$artikul->isPreorder()) {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd->bindValue(2, 0, PDO::PARAM_INT);
                    $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd->execute();

                    $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                    $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
                    $stm_disable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm_disable_group_product -> execute();

                    $isAvaliable = false;
                }
            }
        } else {
            $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ? WHERE `id` = ? LIMIT 1');
            $upd -> bindValue(1, $nowDate, PDO::PARAM_STR);
            $upd -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
            $upd -> execute();
        }

        $dartikuli = $artikul->getAddArtikuli();
        //dop artikuli
        if ($dartikuli != "") {
            $d_art_arr = explode(",", $dartikuli);
            foreach($d_art_arr as $dart) {
                $dr = new artikul((int)$dart);
                $isAvaliable = $dr->isAvaliable();
                $checkDate = $dr->getCheckDate();
                $nowDate = date("Y-m-d");
                if($checkDate != $nowDate){
                    $tempAval = false;
                    if($dr->isAvaliable()) $tempAval = true;
                    $wstm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ?');
                    $wstm -> bindValue(1, $dr->getId(), PDO::PARAM_INT);
                    $wstm -> execute();
                    if($wstm -> rowCount() > 0){
                        $here = false;
                        $artikul_tag_ids = array();
                        foreach ($wstm -> fetchAll() as $v) {
                            $tempCode = $v['code'];
                            $tempTagId = $v['tag_id'];

                            $stmTagCheck = $pdo->prepare('SELECT id FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? LIMIT 1');
                            $stmTagCheck -> bindValue(1, $dr->getId(), PDO::PARAM_INT);
                            $stmTagCheck -> bindValue(2, $tempTagId, PDO::PARAM_INT);
                            $stmTagCheck -> execute();

                            $type = $v['type'];
                            $stm = $pdo->prepare('SELECT id FROM `warehouse` WHERE `code` = ? LIMIT 1');
                            $stm -> bindValue(1, (int)$tempCode, PDO::PARAM_INT);
                            $stm -> execute();
                            if($stm -> rowCount() > 0) {
                                $artikul_tag_ids[] = $tempTagId;
                                $here = true;
                            } else if(is_null($tempCode) && $stmTagCheck -> rowCount() > 0) {
                                $artikul_tag_ids[] = $tempTagId;
                                $here = true;
                            }
                        }
                        
                        if($type == 2){
                            if($dr->getTaggroup_dropdown() != null){
                                foreach ($dr->getTaggroup_dropdown() as $td) {
                                    foreach($td->getEtiketi() as $t){
                                        $stm = $pdo -> prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? ');
                                        $stm -> bindValue(1, $dr->getId(), PDO::PARAM_INT);
                                        $stm -> bindValue(2, $t->getId(), PDO::PARAM_INT);
                                        $stm -> execute();
                                    }
                                }
                            }
                            foreach ($artikul_tag_ids as $etk) {
                                if (empty($etk)) continue;
                                $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
                                $stm -> bindValue(1, $etk, PDO::PARAM_INT);
                                $stm -> bindValue(2, $dr->getId(), PDO::PARAM_INT);
                                $stm -> execute();
                            }
                        }
                        if($here) {
                            if(count($artikul_tag_ids) > 0) {
                                $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                                $upd -> bindValue(1, $nowDate, PDO::PARAM_STR);
                                $upd -> bindValue(2, 1, PDO::PARAM_INT);
                                $upd -> bindValue(3, $dr->getId(), PDO::PARAM_INT);
                                $upd -> execute();

                                $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                                $stm_enable_group_product -> bindValue(1, 1, PDO::PARAM_STR);
                                $stm_enable_group_product -> bindValue(2, $dr->getId(), PDO::PARAM_INT);
                                $stm_enable_group_product -> execute();

                                $isAvaliable = true;
                            } else {
                                $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                                $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                                $upd->bindValue(2, 0, PDO::PARAM_INT);
                                $upd->bindValue(3, $dr->getId(), PDO::PARAM_INT);
                                $upd->execute();

                                $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                                $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
                                $stm_disable_group_product -> bindValue(2, $dr->getId(), PDO::PARAM_INT);
                                $stm_disable_group_product -> execute();

                                $isAvaliable = false;
                            }
                        } else {
                            if(!$dr->isPreorder()) {
                                $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                                $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                                $upd->bindValue(2, 0, PDO::PARAM_INT);
                                $upd->bindValue(3, $dr->getId(), PDO::PARAM_INT);
                                $upd->execute();

                                $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                                $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
                                $stm_disable_group_product -> bindValue(2, $dr->getId(), PDO::PARAM_INT);
                                $stm_disable_group_product -> execute();

                                $isAvaliable = false;
                            }
                        }
                    } else {
                        $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ? WHERE `id` = ? LIMIT 1');
                        $upd -> bindValue(1, $nowDate, PDO::PARAM_STR);
                        $upd -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                        $upd -> execute();
                    }
                }
            }
        }
        header('Location: ' . url . "index.php?" . $_SERVER['QUERY_STRING']);
    }
    $artikul->cache_update();
    $artikul->cache_search();
}

require_once dir_root_template.'__top.php';
?>





	<?php 
			
			if(isset($_GET['news'])){
				require dir_root_template.'news.php';
			}else if(isset($_GET['newid'])){
				require dir_root_template.'open_news.php';
			}else if(isset($_GET['help'])){
                require dir_root_template.'help.php';
            }else if(isset($_GET['privacy'])){
                require dir_root_template.'privacy.php';
            }else if(isset($_GET['contacts'])){
				require dir_root_template.'shop_details.php';
			}else if(isset($_GET['user'])){
                if(isset($_GET['r'])){
                    if (! $__user) {
                        ?>
                        <script type="text/javascript">
                            window.location = "<?php echo urlm; ?>index.php";
                        </script>
                        <?php
                    }

                    require dir_root_template.'user_details_edit.php';
                } else if(isset($_GET['o'])){
                    if (! $__user) {
                        ?>
                        <script type="text/javascript">
                            window.location = "<?php echo urlm; ?>index.php";
                        </script>
                        <?php
                    }

                    require dir_root_template.'user_details_orders.php';
                } else {
                    if (! $__user) {
                        ?>
                        <script type="text/javascript">
                            window.location = "<?php echo urlm; ?>index.php";
                        </script>
                        <?php
                    }
                    
                    require dir_root_template.'user_details.php';
                }
            }else if(isset($_GET['gid'])){
                require dir_root_template.'group_details.php';
            }else if(!isset($artikul)&&!isset($kategoriq)&&!isset($_GET['hotOffers'])&&!isset($_GET['bundles'])&&!isset($_GET['branch'])&&!isset($_GET['newid']) && !isset($_GET['gid'])){
				require dir_root_template.'intro.php';
			}else if (isset($artikul)){
				include dir_root_template.'navigation.php';
				require dir_root_template.'product_details.php';
			}else if (isset($_GET['bundles'])){
                include dir_root_template.'navigation.php';
                require dir_root_template.'__menu_left_bundle.php';
                require dir_root_template.'__bundles.php';
            }else{
				if($artikuli){
					include dir_root_template.'navigation.php';
					require dir_root_template.'__menu_left.php';
					require dir_root_template.'__products.php';
				}else{
//					include dir_root_template.'navigation.php';
//					require dir_root_template.'__menu_left.php';
//					echo '<h2>Няма модели</h2>';
                    echo "<script type='text/javascript'>window.location.href = '" . url . "';</script>";
                    exit();
				}
			}
if(!isset($artikul)){

    ?>
    <div style="clear:both;font-size:0;" class="pager">



        <?php
    		$prev=false;
            if(isset($_GET['tag'])) {
                foreach ($_GET['tag'] as $tag) {
                    $__url .= '&tag[]='.$tag;
                }
            }
    		if(($artikul_broi>$broi_artikuli_na_stranica) && (isset($kategoriq)||isset($_GET['id'])||(isset($_GET['hotOffers']))||(isset($_GET['branch'])))){
    			foreach (po_taka_pagination($page,$artikuli_broi , $broi_artikuli_na_stranica, $__url.'&page=') as $v)
        {
    		$purl=$v['url'];
    		if(isset($_GET['hotOffers'])){
    			$purl=$purl."&hotOffers";
    		}else if(isset($_GET['branch'])){
    			$purl=$purl."&branch=".$_GET['branch'];
    		}

            if(isset($_GET['filter'])) {
                $purl=$purl."&filter=".$_GET['filter'];
            }
            if(isset($_GET['s'])) {
                $purl=$purl."&s=".$_GET['s'];
            }
    	?>


        <?php if($v['current']) : ?>

            <a href="<?php echo $purl; ?>" class="bottom_lincs_activ"><?php echo $v['number']; $prev=true;?></a>

        <?php else : ?>

            <a <?php if($prev) {echo 'rel="next"';}else{echo 'rel="prev"';} ?> href="<?php echo $purl; ?>" class="bottom_lincs"><?php echo $v['number']; ?></a>

        <?php endif; ?>


        <?php 	}
    		}?>

    </div>
<?php
}

require dir_root_template.'__footer.php'; // празно е нз дали ще го има ?>
