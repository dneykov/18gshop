
<style type="text/css">

    .registrationDetails table tr{
        margin:10px 0;
    }
    .registrationDetails table tr td{
        font-size: 9pt;
    }
    .left-side input, .right-side input{
        width:100%;
        margin-right: 10px;
        margin-bottom: 5px;
        padding: 5px;
    }
    .need {
        color: red;
    }
    .error {
        margin: 10px 10px 0 10px;
    }
    #legend {
        font-size: 11px;
    }
    .left-side {
        width: 370px;
        padding: 0;
        margin: 0 70px 0 0;
        float: left;
    }
    .row-half {
        width: 180px;
        display: block;
        float: left;
    }
    .right-side .row-half {
        width: 147px;
        display: block;
        float: left;
    }
    .row-half:first-child {
        margin-right: 10px;
    }
    form label {
        font-size: 13px !important;
        vertical-align: text-bottom;
        font-family: Verdana;
        height: 18px;
        display: inline-block;
        margin-bottom: 2px;
        margin-right: 10px;
    }
    .right-side {
        width: 305px;
        float: left;
    }
    .row {
        clear: both;
    }
    div.three-fourth {
        width: 237px;
        margin-right: 10px;
        float: left;
    }
    div.one-fourth {
        width: 58px;
        float: left;
    }
    div.one-fifth {
        width: 69px;
        margin-right: 15px;
        float: left;
    }
    div.one-fifth input {
        width: 100%;
    }
    div.one-fifth:nth-child(3n) {
        margin-right: 0;
    }
    input[type=radio] {
        margin-right: 0;
        margin-bottom: 0;
        height: 18px;
    }
    .top-row {
        margin-bottom: 10px;
    }
    .top-row span {
        display: block;
        float: left;
        font-size: 13px;
        line-height: 18px;
    }
    #legal-form {
        display: none;
    }

    input[name=register_mail],
    input[name=gsm],
    input[name=cumparator],
    input[name=iban],
    input[name=bank],
    input[name=street2],
    input[name=postcode] {
        width:100%;
    }

    .terms {
        margin-top: 0;
        clear: left;
        float: right;
        margin-right: 78px;
    }

    .clear {
        clear: both;
    }

    input:hover, input:focus {
        border: 1px solid #FF671F;
    }

    .left-side div.three-fourth {
        width: 300px;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        if($("input[name=account_type]:checked").val() == '0') {
            $("#individual-form").show();
            $("#legal-form").hide();
            $('#legal-form :input').prop("disabled", true);
            $('#individual-form :input').prop("disabled", false);
        } else if($("input[name=account_type]:checked").val() == '1') {
            $("#individual-form").hide();
            $('#individual-form :input').prop("disabled", true);
            $('#legal-form :input').prop("disabled", false);
            $("#legal-form").show();
        }
        $("input[name=account_type]").change(function () {
            if($(this).val() == '0') {
                $("#individual-form").show();
                $("#legal-form").hide();
                $('#legal-form :input').prop("disabled", true);
                $('#individual-form :input').prop("disabled", false);
            } else {
                $("#individual-form").hide();
                $('#individual-form :input').prop("disabled", true);
                $('#legal-form :input').prop("disabled", false);
                $("#legal-form").show();
            }
        });
    });
</script>
<h3>New User Registration</h3>
<div class="title">If you already have an account please click <a href="<?php echo url; ?>/fancy_login.php?p=login">here</a></div>
<div class="registrationDetails">

    <form action="<?php echo url;?>fancy_login.php?p=registation" method="post" style="margin:20px 10px 0;">
        <div class="top-row">
            <input type="radio" id="individual" style="width: auto;" name="account_type" value="0" checked <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; ?> />
            <label for="individual">Individual person</label>
            <span class="radio-divider"></span>
            <input type="radio" id="legal_entity" style="width: auto;" name="account_type" value="1" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; ?> />
            <label for="legal_entity">Legal entity</label>
        </div>
        <div id="individual-form">
            <div class="left-side">
                <div class="row">
                    <div class="row-half">
                        <label for="name">first name<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="register_name" value="<?php if(isset($_POST['register_name'])) echo $_POST['register_name']; ?>" />
                    </div>
                    <div class="row-half">
                        <label for="name">last name<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="register_name_last" value="<?php if(isset($_POST['register_name_last'])) echo $_POST['register_name_last']; ?>" />
                    </div>
                </div>
                <div class="row">
                    <label for="name">mail<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="register_mail" value="<?php if(isset($_POST['register_mail'])) echo $_POST['register_mail']; ?>"/></td>
                </div>
                <div class="row">
                    <div class="row-half">
                        <label for="name">password<span class="need">*</span></label>
                        <input type="password" autocomplete="new-password" name="register_pass" value="<?php if(isset($_POST['register_pass'])) echo $_POST['register_pass']; ?>">
                    </div>
                    <div class="row-half">
                        <label for="name">password again<span class="need">*</span></label>
                        <input type="password" autocomplete="new-password" name="register_passAgain" value="<?php if(isset($_POST['register_passAgain'])) echo $_POST['register_passAgain']; ?>">
                    </div>
                </div>
                <div class="row">
                    <label for="name">GSM<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm']; ?>" />
                </div>
            </div>
            <div class="right-side">
                <div class="row">
                    <label for="country">country<span class="need">*</span></label>
                    <select id="country" name="country" style=" width: 305px; margin-bottom: 25px;">
                        <?php
                        $stm = $pdo -> prepare("SELECT * FROM `locations`");
                        $stm -> execute();
                        foreach ($stm -> fetchAll() as $c) {
                            echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input style="width:305px;" type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">street<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; ?>" />
                    </div>
                    <div class="one-fourth">
                        <label for="name">number<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; ?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="one-fifth">
                        <label for="name">en.</label>
                        <input type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; ?>" />
                    </div>
                    <div class="one-fifth">
                        <label for="name">fl.</label>
                        <input type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['etaj'])) echo $_POST['etaj']; ?>" />
                    </div>
                    <div class="one-fifth">
                        <label for="name">ap.</label>
                        <input type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; ?>" />
                    </div>
                </div>
            </div>
        </div>
        <div id="legal-form">
            <div class="left-side">
                <div class="row">
                    <div class="row-half">
                        <label for="name">first name<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="register_name" value="<?php if(isset($_POST['register_name'])) echo $_POST['register_name']; ?>" />
                    </div>
                    <div class="row-half">
                        <label for="name">last name<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="register_name_last" value="<?php if(isset($_POST['register_name_last'])) echo $_POST['register_name_last']; ?>" />
                    </div>
                </div>
                <div class="row">
                    <label for="name">mail<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="register_mail" value="<?php if(isset($_POST['register_mail'])) echo $_POST['register_mail']; ?>"/></td>
                </div>
                <div class="row">
                    <div class="row-half">
                        <label for="name">password<span class="need">*</span></label>
                        <input type="password" autocomplete="new-password" name="register_pass" value="<?php if(isset($_POST['register_pass'])) echo $_POST['register_pass']; ?>">
                    </div>
                    <div class="row-half">
                        <label for="name">password again<span class="need">*</span></label>
                        <input type="password" autocomplete="new-password" name="register_passAgain" value="<?php if(isset($_POST['register_passAgain'])) echo $_POST['register_passAgain']; ?>">
                    </div>
                </div>
                <div class="row">
                    <label for="name">GSM<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm']; ?>" />
                </div>
            </div>
            <div class="right-side">
                <div class="row">

                    <label for="country">country<span class="need">*</span></label>
                    <select id="country" name="country" style=" width: 305px; margin-bottom: 25px;">
                        <?php
                        $stm = $pdo -> prepare("SELECT * FROM `locations`");
                        $stm -> execute();
                        foreach ($stm -> fetchAll() as $c) {
                            echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input style="width:305px;" type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">street<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; ?>" />
                    </div>
                    <div class="one-fourth">
                        <label for="name">number<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; ?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="one-fifth">
                        <label for="name">en.</label>
                        <input type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; ?>" />
                    </div>
                    <div class="one-fifth">
                        <label for="name">fl.</label>
                        <input type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['etaj'])) echo $_POST['etaj']; ?>" />
                    </div>
                    <div class="one-fifth">
                        <label for="name">ap.</label>
                        <input type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; ?>" />
                    </div>
                </div>
            </div>
            <div class="left-side" style="margin-top: 40px;">
                <div class="row">
                    <label for="name">company name<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="company_name" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name']; ?>"/>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input type="text" autocomplete="off" name="company_city" value="<?php if(isset($_POST['company_city'])) echo $_POST['company_city']; ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">street<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_street" value="<?php if(isset($_POST['company_street'])) echo $_POST['company_street']; ?>" />
                    </div>
                    <div class="one-fourth">
                        <label for="name">number</label>
                        <input type="text" autocomplete="off" name="company_number" value="<?php if(isset($_POST['company_number'])) echo $_POST['company_number']; ?>" />
                    </div>
                </div>
            </div>
            <div class="right-side" style="margin-top: 40px;">
                <div class="row">
                    <label for="name">f.l.p<span class="need">*</span></label>
                    <input type="text" autocomplete="off" name="company_mol" value="<?php if(isset($_POST['company_mol'])) echo $_POST['company_mol']; ?>" />
                </div>
                <div class="row">
                    <div class="row-half">
                        <label for="name">bulstat<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_eik" value="<?php if(isset($_POST['company_eik'])) echo $_POST['company_eik']; ?>" />
                    </div>
                    <div class="row-half">
                        <label for="name">vat<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_vat" value="<?php if(isset($_POST['company_vat'])) echo $_POST['company_vat']; ?>" />
                    </div>
                </div>
                <div class="row">
                    <label for="name">phone</label>
                    <input type="text" autocomplete="off" name="company_phone" value="<?php if(isset($_POST['company_phone'])) echo $_POST['company_phone']; ?>" />
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="legend">
            <span class="need">*</span> required fields
        </div>
        <div style="margin-top:15px;">
            <input id="register" style="width:206px;float:right;margin: 0 35px 0 0;height: 36px; line-height: 32px; background-color: #FF671F;" type="submit" name="submit" value="register me" class="submit">
            <div style="margin:0 40px 10px 0;">
                <label for="con" style="font-size:9pt;color:#7c7c7c"><input id="con" name="terms" style="-ms-transform: scale(2);margin: 0 10px 0 0;width:13px;margin-right:10px;" type="checkbox" <?php if(isset($_POST['terms'])){echo ' checked="yes" ';} ?> >I accept the <a onclick="javascript:window.open('<?php echo url; ?>terms.php','mywin','left=20,top=20,width=982,height=640,toolbar=0,scrollbars=1');return false;" href="<?php echo url; ?>/terms.html" class="tgr" target="_blank" style="font-weight: bold;color:#333;font-size:9pt;">terms of use</a></label><br/>
                <label for="privacy" style="font-size:9pt;color:#7c7c7c"><input id="privacy" name="privacy" style="-ms-transform: scale(2);margin: 0 10px 0 0;width:13px;margin-right:10px;" type="checkbox" <?php if(isset($_POST['privacy'])){echo ' checked ';} ?> >I accept <a href="<?php echo url; ?>index.php?privacy" class="tgr" target="_blank" style="font-weight: bold;color:#333;font-size:9pt;">processing</a> my privacy data</label><br/>
            </div>
        </div>
</div>
</form>
<?php
$tmp_msg='';
if((isset($user_register_error))&&($user_register_error)) foreach ($user_register_error as $v) {
    //echo '<div class="error">';
    $tmp_msg= $tmp_msg.', '.$v;
    //echo '</div>';
} ?>
<?php if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
    //echo '<div class="error">';
    $tmp_msg= $tmp_msg.', '.$v;
    //echo '</div>';
}
$tmp_msg=trim($tmp_msg,', ');
if(!empty($tmp_msg)){
    echo '<div class="error">';
    echo $tmp_msg;
    echo '</div>';
}
?>
</div>
