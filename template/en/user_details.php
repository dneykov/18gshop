<style type="text/css">
    #cont {
        width: 1000px;
        margin: 5px auto;
        text-align: left;
        padding-bottom: 200px;
        font-size: 14px;
    }
    #nav-bar {
        height: 20px;
        font-size: 12px;
        padding-bottom: 20px;
        padding-left: 10px;
        width: 100%;
    }
    #nav-bar a{
        font-size: 12px;
    }
    #userOrders {
        margin-top: 30px;
    }
    #tabs-bar {
        height: 20px;
        width: 100%;
        padding-bottom: 2px;
        border-bottom: 1px solid #f05a24;
        margin-bottom: 20px;
    }
    #tabs-bar a{
        margin-left: 5px;
        margin-right: 20px;
        font-size: 14px;
    }
    .userDetails {
        margin-top: 20px;
        width: 930px;
        margin-left: 20px;
    }
    .userDetails table {
        border: none;
    }
    .userDetails table tr td{
        font-size: 9pt;
    }
    .userDetails input{
        width: calc(100% - 10px);
        margin-bottom: 15px;
        margin-right: 15px;
        padding: 5px;
        border: 1px solid #CCC;
    }
    .userDetails input:hover,.userDetails input:focus {
        border: 1px solid #f05a24;
    }

    label {
        display: inline-block;
        font-size: 13px;
        margin-bottom: 3px;
    }

    .userDetails select {
        border: 1px solid #CCC;
        background-color: #F1F1F1;
        padding: 5px;
        width: 100%;
    }
    .submit {
        display: block;
        background: none repeat scroll 0 0 #333;
        border: medium none;
        color: #fff;
        cursor: pointer;
        float: right;
        margin: 34px 0 0;
        height: 36px;
        width: 180px;
        line-height: 33px;
        text-align: center;
        text-decoration: none;
        font-size: 13px;
    }
    .submit:hover {
        background: none repeat scroll 0 0 #FF671F;
    }
    input, textarea {
        font-size: 13px;
    }
    input {
        font-size: 13px!important;
    }
    .need {
        color: red;
    }
    .error {
        margin: 70px 10px 0 10px;
    }
    #legend {
        margin-top: -3px;
        font-size: 11px;
    }
    .left-side {
        width: 500px;
        padding: 0;
        margin: 0 100px 0 5px;
        float: left;
    }
    .row-half {
        width: 240px;
        display: block;
        float: left;
    }
    .row-half:first-child {
        margin-right: 20px;
    }
    form label {
        font-size: 13px !important;
        vertical-align: text-bottom;
        font-family: Verdana;
        display: inline-block;
        margin-bottom: 2px;
        margin-right: 10px;
    }
    .right-side {
        width: 325px;
        float: left;
    }
    .row {
        clear: both;
    }
    div.three-fourth {
        width: 225px;
        float: left;
        margin-right: 20px;
    }
    div.one-fourth {
        width: 80px;
        float: left;
    }
    div.one-fifth {
        width: calc(33% - 15px);
        margin-right: 24px;
        float: left;
    }
    div.one-fifth:nth-child(3n) {
        margin-right: 0;
    }
    input[type=radio] {
        margin-right: 0;
        margin-bottom: 0;
        height: 18px;
    }
    .top-row span {
        display: block;
        float: left;
        font-size: 13px;
        line-height: 18px;
    }
    #legal-form {
        display: none;
    }
    #register {
        margin-right: 5px;
    }
    .clear {
        clear: both;
    }
    .right-side .row-half {
        width: 152px;
    }
    .left-side div.three-fourth {
        width: 370px;
        float: left;
        margin-right: 20px;
    }
    .left-side div.one-fourth {
        width: 110px;
        float: left;
    }
    @media screen and (max-width: 780px) {

        .userDetails {
            width: calc(100% - 20px);
            margin: 0 10px;
        }

        #individual-form .left-side,
        .left-side,
        #individual-form .right-side,
        .userDetails .right-side,
        #legal-form .left-side,
        #legal-form .right-side {
            display: block;
            width: 100%;
            margin: 0 auto;
        }

        .userDetails select,
        #individual-form input,
        #legal-form input {
            width: 100% !important;
        }

        .userDetails input{
            width: calc(100% - 6px) !important;
        }

        #cont div.three-fourth, #cont div.one-fourth {
            width: 100%;
            margin-bottom: 10px;
        }

        .userDetails .row-half,
        #individual-form .row-half,
        #legal-form .row-half {
            width: calc(50% - 5px);
            margin-bottom: 10px !important;
        }
        .userDetails .row, .userDetails .one-fifth {
            margin-bottom: 10px !important;
        }
        .userDetails select {
            margin-bottom: 0 !important;
        }

        .userDetails .right-side, .userDetails .left-side {
            margin-top: 0 !important;
        }

        #cont .submit {
            margin-top: 0;
            margin-bottom: 10px;
            width: calc(100% - 16px);
            float: left;
        }
    }
</style>
<div id="cont">
    <div id="nav-bar">
        <a href="<?php echo url; ?>" style="color: #4a4942;">18gshop</a> <span style="color: #4a4942;">/</span>
        <a href="<?php echo url.'index.php?user'; ?>" style="color: #4a4942;">profile</a> <span style="color: #4a4942;">/</span>
        <a href="<?php echo url.'index.php?user'; ?>" style="color: #4a4942;"><?php echo $__user->getName(); ?></a>
        <?php if($__user->is_partner()) : ?>
            <span style="color: #4a4942;">/</span>
            <span style="color: #FF671F">partner</span>
        <?php endif; ?>
    </div>
    <div id="tabs-bar">
        <a href="<?php echo url.'index.php?user'; ?>" style="color: #FF671F;">profile</a>
        <a href="<?php echo url.'index.php?user&o'; ?>" style="color: #4a4942;">orders
            <?php
            $stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ?");
            $stm->bindValue(1, $__user->getId(), PDO::PARAM_STR);
            $stm->execute();
            echo $stm->rowCount();
            ?>
        </a>
    </div>
    <?php
    if(isset($__user)){
        if($__user->getAccountType() == '0') {
            ?>
            <div class="userDetails">
                <div class="left-side">
                    <div class="row">
                        <div class="row-half">
                            <label for="name">first name</label>
                            <input type="text" autocomplete="off" name="register_name" value="<?php echo $__user->getName(); ?>" disabled />
                        </div>
                        <div class="row-half">
                            <label for="name">last name</label>
                            <input type="text" autocomplete="off" name="register_name_last" value="<?php echo $__user->getName_last(); ?>" disabled />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row">
                        <label for="name">e-mail</label>
                        <input style="margin-right:0px;" type="text" autocomplete="off" name="register_mail" value="<?php echo $__user->getMail(); ?>" disabled/>
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">GSM</label>
                            <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm" value="<?php echo $__user->getPhone_mobile(); ?>" disabled />
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <div class="row">
                        <label for="country">country</label>
                        <select id="country" name="country" style="margin-bottom: 25px;" disabled>
                            <?php
                            $stm = $pdo -> prepare("SELECT * FROM `locations`");
                            $stm -> execute();
                            ?>
                            <?php foreach ($stm -> fetchAll() as $c) : ?>
                                <option <?php echo $__user->getCountryId() == $c["id"] ? 'selected' : ''; ?> value="<?php echo $c['id']; ?>"><?php echo $c['country_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="row">
                        <label for="name">city</label>
                        <input type="text" autocomplete="off" name="city" value="<?php echo $__user->getAdress_town(); ?>" disabled />
                    </div>
                    <div class="row">
                        <div class="three-fourth">
                            <label for="name">street</label>
                            <input type="text" autocomplete="off" name="street" value="<?php echo $__user->getAdress_street(); ?>" disabled />
                        </div>
                        <div class="one-fourth">
                            <label for="name">number</label>
                            <input type="text" autocomplete="off" name="number" value="<?php echo $__user->getAdress_number(); ?>" disabled />
                        </div>
                    </div>
                    <div class="row">
                        <div class="one-fifth">
                            <label for="name">en.</label>
                            <input type="text" autocomplete="off" name="vhod" value="<?php echo $__user->getAdress_vhod(); ?>" disabled />
                        </div>
                        <div class="one-fifth">
                            <label for="name">fl.</label>
                            <input type="text" autocomplete="off" name="etaj" value="<?php echo $__user->getAdress_etaj(); ?>" disabled />
                        </div>
                        <div class="one-fifth">
                            <label for="name">apt.</label>
                            <input type="text" autocomplete="off" name="apartament" value="<?php echo $__user->getAdress_ap(); ?>" disabled />
                        </div>
                    </div>
                </div>
                <div class="clear"> </div>
                <div style="margin-top:30px;">
                    <a href="<?php echo url . 'index.php?user&r'; ?>" class="submit">edit</a>
                </div>
            </div>
            <?php
        } else if($__user->getAccountType() == '1') { ?>
            <div class="userDetails">
                <div class="left-side">
                    <div class="row">
                        <div class="row-half">
                            <label for="name">first name</label>
                            <input type="text" autocomplete="off" name="register_name" value="<?php echo $__user->getName(); ?>" disabled />
                        </div>
                        <div class="row-half">
                            <label for="name">last name</label>
                            <input type="text" autocomplete="off" name="register_name_last" value="<?php echo $__user->getName_last(); ?>" disabled />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row">
                        <label for="name">e-mail</label>
                        <input style="margin-right:0px;" type="text" autocomplete="off" name="register_mail" value="<?php echo $__user->getMail(); ?>" disabled/>
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">GSM</label>
                            <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm" value="<?php echo $__user->getPhone_mobile(); ?>" disabled />
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <div class="row">
                        <label for="country">country</label>
                        <select id="country" name="country" style="margin-bottom: 25px;" disabled>
                            <?php
                            $stm = $pdo -> prepare("SELECT * FROM `locations`");
                            $stm -> execute();
                            ?>
                            <?php foreach ($stm -> fetchAll() as $c) : ?>
                                <option <?php echo $__user->getCountryId() == $c["id"] ? 'selected' : ''; ?> value="<?php echo $c['id']; ?>"><?php echo $c['country_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="row">
                        <label for="name">city</label>
                        <input type="text" autocomplete="off" name="city" value="<?php echo $__user->getAdress_town(); ?>" disabled />
                    </div>
                    <div class="row">
                        <div class="three-fourth">
                            <label for="name">street</label>
                            <input type="text" autocomplete="off" name="street" value="<?php echo $__user->getAdress_street(); ?>" disabled />
                        </div>
                        <div class="one-fourth">
                            <label for="name">number</label>
                            <input type="text" autocomplete="off" name="number" value="<?php echo $__user->getAdress_number(); ?>" disabled />
                        </div>
                    </div>
                    <div class="row">
                        <div class="one-fifth">
                            <label for="name">en.</label>
                            <input type="text" autocomplete="off" name="vhod" value="<?php echo $__user->getAdress_vhod(); ?>" disabled />
                        </div>
                        <div class="one-fifth">
                            <label for="name">fl.</label>
                            <input type="text" autocomplete="off" name="etaj" value="<?php echo $__user->getAdress_etaj(); ?>" disabled />
                        </div>
                        <div class="one-fifth">
                            <label for="name">apt.</label>
                            <input type="text" autocomplete="off" name="apartament" value="<?php echo $__user->getAdress_ap(); ?>" disabled />
                        </div>
                    </div>
                </div>
                <div class="left-side" style="margin-top: 30px;">
                    <div class="row">
                        <label for="name">company name</label>
                        <input style="margin-right:0px;" type="text" autocomplete="off" name="company_name" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name']; else{echo $__user->getCompanyName();} ?>" disabled />
                    </div>
                    <div class="row">
                        <label for="name">city</label>
                        <input type="text" autocomplete="off" name="company_city" value="<?php if(isset($_POST['company_city'])) echo $_POST['company_city']; else{echo $__user->getCompanyCity();} ?>" disabled />
                    </div>
                    <div class="row">
                        <div class="three-fourth">
                            <label for="name">street</label>
                            <input type="text" autocomplete="off" name="company_street" value="<?php if(isset($_POST['company_street'])) echo $_POST['company_street']; else{echo $__user->getCompanyStreet();} ?>" disabled />
                        </div>
                        <div class="one-fourth">
                            <label for="name">number</label>
                            <input type="text" autocomplete="off" name="company_number" value="<?php if(isset($_POST['company_number'])) echo $_POST['company_number']; else{echo $__user->getCompanyNumber();} ?>" disabled />
                        </div>
                    </div>
                </div>
                <div class="right-side" style="margin-top:30px;">
                    <div class="row">
                        <label for="name">f.l.p</label>
                        <input type="text" autocomplete="off" name="company_mol" value="<?php if(isset($_POST['company_mol'])) echo $_POST['company_mol']; else{echo $__user->getCompanyMol();} ?>" disabled />
                    </div>
                    <div class="row">
                        <div class="row-half">
                            <label for="name">bulstat</label>
                            <input type="text" autocomplete="off" name="company_eik" value="<?php if(isset($_POST['company_eik'])) echo $_POST['company_eik']; else{echo $__user->getCompanyEik();} ?>" disabled />
                        </div>
                        <div class="row-half">
                            <label for="name">vat</label>
                            <input type="text" autocomplete="off" name="company_vat" value="<?php if(isset($_POST['company_vat'])) echo $_POST['company_vat']; else{echo $__user->getCompanyVat();} ?>" disabled />
                        </div>
                    </div>
                    <div class="row">
                        <label for="name">phone</label>
                        <input type="text" autocomplete="off" name="company_phone" value="<?php if(isset($_POST['company_phone'])) echo $_POST['company_phone']; else{echo $__user->getCompanyPhone();} ?>" disabled />
                    </div>
                </div>
                <div class="clear"></div>
                <div style="margin-top:30px;">
                    <a href="<?php echo url . 'index.php?user&r'; ?>" class="submit">edit</a>
                </div>
            </div>
            <?php
        }
        echo "</div>";
    } else {
        echo "<center>Please login into your account</center>";
    }
    ?>
</div>