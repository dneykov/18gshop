<?php
require dir_root_template."__top.php";
if(isset($_GET['submitpass'])){

	$mail = $_GET['mail'];
	$key = $_GET['key'];

	$done = false;
	if(isset($updatesuccess) && $updatesuccess == true) {
		$done = true;
	}

	?>
	</center>
	<style>
		input, textarea {
			font-size: 13px;
		}
		input:hover, input:focus{
			border: 1px solid #FF671F;
		}
	</style>
	<div style="margin-left: 5px;">
		<?php
		if($done) {
			echo '<div style="display: block; margin-top: 400px; margin-bottom: 350px; color: #000;font-size: 17px; text-align: center;width:100%;">Вие променихте вашата парола успешно. Моля натиснете <strong><a href="'.url.'">тук</a></strong> за да се върнете в началото';
		} else {
		?>
		<div style="margin-left: 185px;">
		<h3>Смяна на парола</h3>
		<div class="title">Моля попълнете в полетата вашата нова парола</div>
		</div>
		<div style="width: 350px; margin: 100px auto; padding-bottom: 100px;">
			<form action="" method="post" style="width: 250px; margin: 0 auto; display: block;">
				<div style="color: rgb(77, 77, 77);">Парола</div>
				<input type="password" name="pass" id="pass" style="width: 250px; margin-bottom: 5px;padding:5px;">
				<div style="color: rgb(77, 77, 77);" >Парола Отново</div>
				<input type="password" name="passagain" id="passagain" style="width: 250px; margin-bottom: 5px;padding:5px;">
				<input type="hidden" name="updatepass" value="updatepass">
				<input type="hidden" name="mail" value="<?php echo $mail; ?>">
				<input type="hidden" name="key" value="<?php echo $key; ?>">
				<div style="clear: both;"></div>
				<input type="submit" name="send" class="submit" value="Изпрати" style="display: block; margin: 10px auto; float: left; border: none; background-color: #FF671F; height: 25px; width: 130px;" >
			</form>
			<div class="error" style="font-size: 13px; width: 250px; margin: 0 auto; display: block;">
				<?php 
				if(isset($errors)){
					foreach($errors as $error) {
						echo $error;
					}
				}
				?>
			</div>
		<?php
		}
		?>
		</div>
	</div>
	<center>
	<?php
}

require dir_root_template."__footer.php";
?>