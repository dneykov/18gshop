<script type="text/javascript">
	$(document).ready(function(){
		$(".branch img").hover(function() {
			var src = $(this).attr("src").replace(".jpg", "_color.jpg");
            $(this).attr("src", src);
		},
		function() {
			var src = $(this).attr("src").replace("_color.jpg", ".jpg");
			
            $(this).attr("src", src);
		});
	});
</script>
<div id="branches" class="branches jThumbnailScroller">
	<div class="jTscrollerContainer">
        <div class="jTscroller">
			<?php
				
				if (isset($branches)){
					$pdo = PDOX::vrazka();
					foreach ($branches as $branch)
					{
						$tmp_branch= new model($branch);
						
						$sql='SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_model` in (SELECT `id` FROM `model` where `ime` = ?) AND IsOnline=1 AND `isDelete`=0 AND `Okazion`=0 ';
						$stm = $pdo->prepare($sql);
						$stm -> bindValue(1, $tmp_branch->getIme(), PDO::PARAM_INT);
						$stm -> execute();

						$data = $stm->fetch();

						if(((int)$data[0])>0 ){
							echo '<a class="branch" href="'.url.'index.php?branch='.$branch['ime'].'"><img style="height:60px;"  src="'.url.$tmp_branch->getBWImage().'" /></a>' ;    
						}
					}
				}
			?>
		</div>
    </div>
    <a href="#" class="jTscrollerPrevButton"></a>
    <a href="#" class="jTscrollerNextButton"></a>
</div>

	<!--<div class="bottom_navigation">
		<div style="margin:auto;width:1000px;">	
			<a href="index.php?contacts">
				<img style="margin-top:-16px;" src="<?php echo url; ?>/images/phone.png" alt="phone" />
				<span style="color: #FFFFFF;font-size: 16px;font-style: italic;font-weight: bolder;line-height: 20px;"><?php echo $phone_for_contact;?></span>			
			</a>
			<ul class="menu" >
				<li><a href="index.php?news">НОВИНИ</a></li>	
				<li style="background:#333;"><a href="index.php?contacts" style="color:#fff;">КОНТАКТИ</a></li>
				<li style="background:#fff;margin-left:-3px;"><a href="http://www.facebook.com/pages/18gshop/348633109873" TARGET="_blank" style="color: #999999;font-size: 13pt;font-weight: bold;line-height: 20px;">facebook</a></li>
			</ul>
		</div>
	</div>-->
	<div id='footerTable'>
	<div id='contacts'>
		<ul>
			<li><a href="index.php?contacts">КОНТАКТИ</a></li>
			<li>how to buy?</li>
			<li>УСЛОВИЯ ЗА ПОЛЗВАНЕ</li>
		</ul>
	</div>
	<table id='footerItems'>
		<tr>
			<?php			
				if(($all_kategorii)&&(sizeof($all_kategorii)>0)){
					
					foreach($all_kategorii as $tmp_kategoriq){				
								echo "<td valign='top'><div class='banner' >";										
									echo '<a href="'.url.'/index.php?category='.$tmp_kategoriq->getId().'">';
										echo '<div>'.$tmp_kategoriq->getIme().'</div>';
									echo '</a>';		
									echo '<div class="vid_menu">';
										foreach($tmp_kategoriq->getVidove() as $tmp_vid){											
												echo '<a href="'.url.'/index.php?category='.$tmp_kategoriq->getId().'&mode='.$tmp_vid->getId().'">'.$tmp_vid->getIme().'</a></br>';
										}			
									echo '</div>';						
								echo "</div></td>";					
					 }

							 
				}
			?>			
		</tr>
	</table>
	
	<hr style='width: 100%; color: #1A1A1A; height: 1px;'>
	<div id='footerLine'>
		<span style='font-family: verdana; font-size: 11px; color: white;'>18gshop&copy; 2013. All Rights Reserved.</span>
		<span style='float: right; margin-right: 40px;'>&nbsp; &nbsp;<a href='https://www.facebook.com/pages/18gshop/348633109873' target="_blank"><img src='template/bg/images/logo_fb.png'></a>&nbsp;&nbsp; <a href="skype:pb.18gshop.pro?chat"><img src='template/bg/images/logo_kype.png'></a></span>&nbsp;&nbsp;<span style='float: right; color: white;'>+359 885 938 601</span>
	</div>
	</div>
	<style>
		#contacts
		{
			color: white;
			text-align: left;
			position: relative;
			float: right;
			margin-left: -190px;
			margin-right: 40px;
			margin-top: -2px;
			
		}
		#contacts li
		{
			margin-top: 5px;
			font-size: 12px;
		}
		#contacts a
		{
			color: white;
			font-size: 12px;
		}
		#contacts a:hover
		{
			color:#fcc900 !important;
		}
		#footerTable
		{
			width: 100%;
			background-color:#333333;
			padding: 20px;
		}
		#footerItems
		{
			background-color:#333333;
			font-size: 14px;
			margin-left: -210px;
		}
		#footerItems a:hover
		{
			color:#fcc900 !important;
		}
		.banner
		{
			font-size: 12px;
		}
		#footerLine
		{
			text-align: left;
			background-color: #1A1A1A;
			height: 28px;
			width: 100%;
			margin-bottom: -20px;
			margin-left: -30px;
			padding: 12px 0px 0px 10px;
			margin-top: 20px;
			
		}
	</style>
	<script type="text/javascript">
		$('table.footer td:last').css('border-right','none');
		$('table.footer td:last .banner').css('margin-right','0px');
		$('table.footer td:first .banner').css('margin-left','0px');
	</script>
	<div style="clear:both;"> &nbsp;</div>
</div>

</center>
</div>
</body>
</html>
