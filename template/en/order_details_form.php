<?php
$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :preorder LIMIT 1");
$stm->bindValue(':preorder', "bank_details_" . lang_prefix, PDO::PARAM_STR);
$stm->execute();
$bankDetailsDb = $stm->fetch();
$bankDetails = $bankDetailsDb['value'];

$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :iban LIMIT 1");
$stm->bindValue(':iban', 'iban', PDO::PARAM_STR);
$stm->execute();
$ibanDb = $stm->fetch();
$iban = $ibanDb['value'];
?>
<script>
    $(document).ready(function () {
        $('#country').change(function () {
            var id = $('#country').val();
            var cost = $('#ajax_cost').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_delivery_en.php",
                data: "id=" + id + "&cost=" + cost,
                cache: false,
                success: function (html) {
                    $('#ajax_delivery').html(" ");
                    $('#ajax_delivery').append(html);
                }
            });
            $.ajax({
                type: "POST",
                url: "ajax/ajax_delivery_en_paypal.php",
                data: "id=" + id + "&cost=" + cost,
                cache: false,
                success: function (html) {
                    $('#shipping_js').html(" ");
                    $('#shipping_js').append(html);
                }
            });
        });

        $('#btn-paypal').click(function () {
            var scode = $('#scode').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_order_paypal.php",
                data: $("#form_order").serialize() + "&scode=" + scode,
                cache: false,
                success: function () {
                    parent.location.reload();
                }
            });
        });

        $('input[name=payment_type]').change(function () {
            if ($(this).val() == "bank") {
                $("#payment #details").show();
            } else {
                $("#payment #details").hide();
            }
        });

        $('input[name=delivery_method]').change(function () {
            if ($(this).val() == "courier") {
                $("#ajax_delivery #allcost").show();
            } else {
                $("#ajax_delivery #allcost").hide();
            }
        });

        $("#register").click(function () {
            $("#register").hide();
            $(".error").hide();
        });

        $('#individual-form input :enabled').prop("disabled", true);
        if ($("input[name=account_type]:checked").val() == '0') {
            $("#legal-form").hide();
            $("div.cart-price").css("margin-top", "0px");
            $("#legal-form :input").prop("disabled", true);
            $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
        } else if ($("input[name=account_type]:checked").val() == '1') {
            $("#legal-form").show();
            $("div.cart-price").css("margin-top", "-30px");
            $("#legal-form :input").prop("disabled", false);
        }
        $("input[name=account_type]").change(function () {
            if ($(this).val() == '0') {
                $("#legal-form").hide();
                $("div.cart-price").css("margin-top", "0px");
                $("#legal-form :input").prop("disabled", true);
                $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
            } else {
                $("#legal-form").show();
                $("div.cart-price").css("margin-top", "-30px");
                $("#legal-form :input").prop("disabled", false);
            }
        });
    });
</script>
<style type="text/css">
    .registrationDetails table tr {
        margin: 10px 0;
    }

    .registrationDetails table tr td {
        font-size: 9pt;
    }

    .left-side input, .right-side input {
        width: 100%;
        margin-right: 10px;
        margin-bottom: 5px;
        padding: 5px;
    }

    .need {
        color: red;
    }

    .error {
        margin: 10px 10px 0 10px;
    }

    #legend {
        font-size: 11px;
    }

    .left-side {
        width: 350px;
        padding: 0;
        margin: 0 50px 0 0;
        float: left;
    }

    .row-half {
        width: 170px;
        display: block;
        float: left;
    }

    .right-side .row-half {
        width: 147px;
        display: block;
        float: left;
    }

    .row-half:first-child {
        margin-right: 10px;
    }

    form label {
        font-size: 13px !important;
        vertical-align: text-bottom;
        font-family: Verdana;
        height: 18px;
        display: inline-block;
        margin-bottom: 2px;
        margin-right: 10px;
    }

    .right-side {
        width: 305px;
        float: left;
    }

    .row {
        clear: both;
    }

    div.three-fourth {
        width: 237px;
        margin-right: 10px;
        float: left;
    }

    div.one-fourth {
        width: 58px;
        float: left;
    }

    div.one-fifth {
        width: 69px;
        margin-right: 15px;
        float: left;
    }

    div.one-fifth input {
        width: 100%;
    }

    div.one-fifth:nth-child(3n) {
        margin-right: 0;
    }

    input[type=radio] {
        margin-right: 0;
        margin-bottom: 0;
        height: 18px;
    }

    .top-row {
        margin-bottom: 10px;
    }

    .top-row span {
        display: block;
        float: left;
        font-size: 13px;
        line-height: 18px;
    }

    #legal-form {
        display: none;
    }

    input[name=edit_mail],
    input[name=gsm],
    input[name=cumparator],
    input[name=iban],
    input[name=bank],
    input[name=street2],
    input[name=postcode] {
        width: 100%;
    }

    .terms {
        margin-top: 0;
        clear: left;
        float: right;
        margin-right: 78px;
    }

    .clear {
        clear: both;
    }

    input:hover, input:focus {
        border: 1px solid #FF671F;
    }

    .left-side div.three-fourth {
        width: 282px;
    }

    .loader {
        background-image: url("images/ajax-loader.gif") !important;
    }

    #payment {
        width: 305px;
        margin-right: 75px;
    }
</style>
<script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
<div class="registrationDetails">
    <form action="fancy_login.php?p=ordrdetails" id="form_order" method="post" style="margin:32px 10px 0;">
        <div class="top-row">
            <input type="radio" id="individual" style="width: auto;" name="account_type"
                   value="0" <?php if (isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '0') echo "checked"; ?> />
            <label for="individual">Individual person</label>
            <input type="radio" id="legal_entity" style="width: auto;" name="account_type"
                   value="1" <?php if (isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '1') echo "checked" ?> />
            <label for="legal_entity">Legal entity</label>
        </div>
        <div id="individual-form">
            <div class="left-side">
                <div class="row">
                    <div class="row-half">
                        <label for="name">first name<span class="need">*</span></label>
                        <input type="text" class="always-disabled" autocomplete="off" disabled name="register_name"
                               value="<?php if (isset($_POST['register_name'])) echo $_POST['register_name']; else echo $__user->getName(); ?>"/>
                    </div>
                    <div class="row-half">
                        <label for="name">last name<span class="need">*</span></label>
                        <input type="text" class="always-disabled" autocomplete="off" disabled name="register_name_last"
                               value="<?php if (isset($_POST['register_name_last'])) echo $_POST['register_name_last']; else echo $__user->getName_last(); ?>"/>
                    </div>
                </div>
                <div class="row">
                    <label for="name">mail<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="edit_mail"
                           value="<?php if (isset($_POST['edit_mail'])) echo $_POST['edit_mail']; else {
                               echo $__user->getMail();
                           } ?>"/></td>
                </div>
                <div class="row">
                    <label for="name">GSM<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="gsm"
                           value="<?php if (isset($_POST['gsm'])) echo $_POST['gsm']; else {
                               echo $__user->getPhone_mobile();
                           } ?>"/>
                </div>
                <div class="row">
                    <label for="name">comment</label>
                    <textarea name="comment"
                              style="width:350px;height:70px; margin-right: 20px;"><?php if (isset($_POST['comment'])) echo $_POST['comment']; ?></textarea>
                </div>
            </div>
            <div class="right-side">
                <div class="row">
                    <label for="country">country<span class="need">*</span></label>
                    <select id="country" name="country" style=" width: 305px; margin-bottom: 15px;">
                        <?php
                        $stm = $pdo->prepare("SELECT * FROM `locations`");
                        $stm->execute();
                        foreach ($stm->fetchAll() as $c) {
                            echo "<option value='" . $c['id'] . "'>" . $c['country_name'] . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input style="width:305px;" type="text" autocomplete="off" name="city"
                           value="<?php if (isset($_POST['city'])) echo $_POST['city']; else {
                               echo $__user->getAdress_town();
                           } ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">street<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="street"
                               value="<?php if (isset($_POST['street'])) echo $_POST['street']; else {
                                   echo $__user->getAdress_street();
                               } ?>"/>
                    </div>
                    <div class="one-fourth">
                        <label for="name">number<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="number"
                               value="<?php if (isset($_POST['number'])) echo $_POST['number']; else {
                                   echo $__user->getAdress_number();
                               } ?>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="one-fifth">
                        <label for="name">en.</label>
                        <input type="text" autocomplete="off" name="vhod"
                               value="<?php if (isset($_POST['vhod'])) echo $_POST['vhod']; else {
                                   echo $__user->getAdress_vhod();
                               } ?>"/>
                    </div>
                    <div class="one-fifth">
                        <label for="name">fl.</label>
                        <input type="text" autocomplete="off" name="etaj"
                               value="<?php if (isset($_POST['vhod'])) echo $_POST['vhod']; else {
                                   echo $__user->getAdress_vhod();
                               } ?>"/>
                    </div>
                    <div class="one-fifth">
                        <label for="name">ap.</label>
                        <input type="text" autocomplete="off" name="apartament"
                               value="<?php if (isset($_POST['apartament'])) echo $_POST['apartament']; else {
                                   echo $__user->getAdress_ap();
                               } ?>"/>
                    </div>
                </div>
            </div>
        </div>
        <div id="legal-form">
            <div class="left-side" style="margin-top: 20px;">
                <div class="row">
                    <label for="name">company name<span class="need">*</span></label>
                    <input style="margin-right:0px;" type="text" autocomplete="off" name="company_name"
                           value="<?php if (isset($_POST['company_name'])) echo $_POST['company_name']; else {
                               echo $__user->getCompanyName();
                           } ?>"/>
                </div>
                <div class="row">
                    <label for="name">city<span class="need">*</span></label>
                    <input type="text" autocomplete="off" name="company_city"
                           value="<?php if (isset($_POST['company_city'])) echo $_POST['company_city']; else {
                               echo $__user->getCompanyCity();
                           } ?>"/>
                </div>
                <div class="row">
                    <div class="three-fourth">
                        <label for="name">street<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_street"
                               value="<?php if (isset($_POST['company_street'])) echo $_POST['company_street']; else {
                                   echo $__user->getCompanyStreet();
                               } ?>"/>
                    </div>
                    <div class="one-fourth">
                        <label for="name">number</label>
                        <input type="text" autocomplete="off" name="company_number"
                               value="<?php if (isset($_POST['company_number'])) echo $_POST['company_number']; else {
                                   echo $__user->getCompanyNumber();
                               } ?>"/>
                    </div>
                </div>
            </div>
            <div class="right-side" style="margin-top:20px;">
                <div class="row">
                    <label for="name">f.l.p<span class="need">*</span></label>
                    <input type="text" autocomplete="off" name="company_mol"
                           value="<?php if (isset($_POST['company_mol'])) echo $_POST['company_mol']; else {
                               echo $__user->getCompanyMol();
                           } ?>"/>
                </div>
                <div class="row">
                    <div class="row-half">
                        <label for="name">bulstat<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_eik"
                               value="<?php if (isset($_POST['company_eik'])) echo $_POST['company_eik']; else {
                                   echo $__user->getCompanyEik();
                               } ?>"/>
                    </div>
                    <div class="row-half">
                        <label for="name">vat<span class="need">*</span></label>
                        <input type="text" autocomplete="off" name="company_vat"
                               value="<?php if (isset($_POST['company_vat'])) echo $_POST['company_vat']; else {
                                   echo $__user->getCompanyVat();
                               } ?>"/>
                    </div>
                </div>
                <div class="row">
                    <label for="name">phone</label>
                    <input type="text" autocomplete="off" name="company_phone"
                           value="<?php if (isset($_POST['company_phone'])) echo $_POST['company_phone']; else {
                               echo $__user->getCompanyPhone();
                           } ?>"/>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <?php
        $sint = rand(2000, 9999);
        $scode = sha1("18g" . $sint . $__user->getName());
        ?>
        <input type="hidden" id="scode" value="<?php echo $scode; ?>">
        <input type="hidden" name="buy" value="buy"/>
        <div id="CostsBox">
            <?php
            $allcost = 0;
            $allPreorder = 0;
            if (isset($_SESSION['cart'])) {
                foreach ($_SESSION['cart'] as $key => $val) {
                    if ($_SESSION['cart'][$key]) {
                        $artikul = new artikul((int)$key);
                        if ($artikul->isPreorder()) {
                            $allPreorder += $_SESSION['cart'][$key]['preorder']['deposit_price'] * $_SESSION['cart'][$key]['count'];
                        } else {
                            $allPreorder += $_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count'];
                        }

                        $allcost += $_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count'];
                    } else {
                        $group = new paket((int)$key);
                        $allcost += $group->getPrice() * $_SESSION['cart'][$key]['count'];
                        $allPreorder += $group->getPrice() * $_SESSION['cart'][$key]['count'];
                    }
                }
            }
            $total = number_format($allcost / CURRENCY_RATE, 2, '.', '');
            $total = explode('.', $total);
            ?>
            <input type="hidden" id="ajax_cost" value="<?php echo $allPreorder / CURRENCY_RATE; ?>">
            <span id="allcost"
                  style="color: #424242; margin-top:0px;font-size:13px;width:185px;float:right; margin-right: 30px; text-align: center;">total € <span
                        style="font-style:italic; font-size: 18px;"><?php echo $total[0]; ?></span> <sup
                        style="font-style:italic;font-size:8pt;"><?php echo $total[1]; ?></sup></span><br/>
            <div style="clear: both;"></div>
            <?php
            $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
            $ds->execute();
            $d = $ds->fetch();
            $dostavka_cena = $d['delivery_price'];
            $dostavka_free = $d['free_delivery'];

            ?>
            <div id="ajax_delivery">
                <?php
                if ($dostavka_free > $allcost) {
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $dostavka_cena += $_SESSION['cart']['total_additional_delivery'];
                    }

                    $tmp = number_format($dostavka_cena / CURRENCY_RATE, 2, '.', '');
                    $tmp = explode('.', $tmp);
                    ?>
                    <span id="allcost"
                          style="margin-top:0px;font-size:10pt;width:185px;float:right; margin-bottom: 5px; color: #808080; margin-right: 30px; text-align: center;">delivery € <span
                                style="font-style:italic;"><?php echo $tmp[0]; ?></span><sup
                                style="font-style:italic;font-size:8pt;"><?php echo $tmp[1]; ?></sup></span>
                    <?php
                } else {
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $tmp = number_format($_SESSION['cart']['total_additional_delivery'] / CURRENCY_RATE, 2, '.', '');
                        $tmp = explode('.', $tmp);
                        $delivery_text = '<span id="allcost" style="margin-top:0px;font-size:11pt;width:185px;float:right; margin-bottom: 5px; color: #808080; margin-right: 30px; text-align: center;">delivery € <span style="font-style:italic;">' . $tmp[0] . '</span><sup style="font-style:italic;font-size:8pt;">' . $tmp[1] . '</sup></span>';
                    } else {
                        $delivery_text = '<span id="allcost" style="margin-top:0px;font-size:11px;width:185px;float:right; margin-bottom: 5px; color: #808080; margin-right: 30px; text-align: center;">free delivery</span>';
                    }

                    echo $delivery_text;
                }
                ?>
            </div>
        </div>
        <div style="margin-top:30px;">
            <div id="form-loader"
                 style="width: 180px; float: right; margin: 0 35px 0 0; height: 36px; line-height: 32px; background: #FF671F url(images/ajax-loader.gif) no-repeat center center; position: absolute; right: 10px; z-index: -1;"></div>
        </div>
    </form>
    <div id="paypal-method">
        <form action="https://www.paypal.com/us/cgi-bin/webscr" method="post" target="_blank">
            <input type="hidden" name="cmd" value="_cart">
            <input type="hidden" name="business" value="todor@18gshop.com">
            <input type="hidden" name="upload" value="1">
            <input type="hidden" name="first_name" value="<?php echo $__user->getName(); ?>">
            <input type="hidden" name="last_name" value="<?php echo $__user->getName_last(); ?>">
            <input type="hidden" name="currency_code" value="EUR">
            <input type="hidden" name="cancel_return" value="<?php echo url; ?>">
            <input type="hidden" name="return" value="<?php echo url . 'order_details_paypal.php?s=' . $scode; ?>">
            <div id="shipping_js">
                <?php
                if ($dostavka_free > $allcost) {
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $dostavka_cena += $_SESSION['cart']['total_additional_delivery'];
                    }
                    ?>
                    <input type="hidden" name="shipping_1"
                           value="<?php echo number_format($dostavka_cena / CURRENCY_RATE, 2, '.', ''); ?>">
                    <?php
                } else {
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $delivery = $_SESSION['cart']['total_additional_delivery'];
                        ?>
                        <input type="hidden" name="shipping_1"
                               value="<?php echo number_format($delivery / CURRENCY_RATE, 2, '.', ''); ?>">
                        <?php
                    }
                }
                ?>
            </div>
            <?php
            echo '<input type="hidden" name="upload" value="1">';

            if (isset($_SESSION['cart'])) {
                $n = 1;
                foreach ($_SESSION['cart'] as $key => $val) {
                    if ($key == "total_additional_delivery") continue;

                    if ($_SESSION['cart'][$key]['type'] == "product") {
                        $artikul = new artikul((int)$key);
                        $option = '';
                        if (isset($_SESSION['cart'][$key]['options'])) {
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0) {
                                        $artikulid = (int)$vall;
                                    }
                                    if ($keyy == 1 && $artikulid == $key) {
                                        $option = $vall;
                                    }
                                }
                            }
                        }
                        echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($artikul->getIme_marka()) . ' ' . stripslashes($artikul->getIme()) . ' size ' . $option . '">';
                        echo '<input type="hidden" name="item_number_' . $n . '" value="' . $artikul->getId() . '">';
                        echo '<input type="hidden" name="quantity_' . $n . '" value="' . (int)$_SESSION['cart'][$key]['count'] . '">';
                        echo '<input type="hidden" name="amount_' . $n . '" value="' . number_format((($artikul->getCena_promo() == 0) ? $artikul->getCena() / CURRENCY_RATE : $artikul->getCena_promo() / CURRENCY_RATE) * ((int)$_SESSION['cart'][$key]['count']), 2, '.', '') . '">';

                        $n++;

                        $dop_artikuli = $artikul->getAddArtikuli();
                        if ($dop_artikuli != "") {
                            $dop_art_arr = explode(",", $dop_artikuli);
                            foreach ($dop_art_arr as $dart) {
                                $dr = new artikul((int)$dart);
                                $option = '';
                                $a = 0;
                                foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                    $expl_arr = explode(",", $optionVal);
                                    foreach ($expl_arr as $keyy => $vall) {
                                        if ($keyy == 0 && $vall == $dr->getId()) {
                                            $artikulid = (int)$vall;
                                        }
                                        if ($keyy == 1 && $artikulid == $dr->getId() && $a == 0) {
                                            $option = $vall;
                                            $a = 1;
                                        }
                                    }
                                }
                                echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($dr->getIme_marka()) . ' ' . stripslashes($dr->getIme()) . ' size ' . $option . '">';
                                echo '<input type="hidden" name="item_number_' . $n . '" value="' . $dr->getId() . '">';
                                echo '<input type="hidden" name="amount_' . $n . '" value="0">';
                                $n++;
                            }
                        }
                    } else {
                        $group = new paket((int)$key);

                        echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($group->getName()) . '">';
                        echo '<input type="hidden" name="item_number_' . $n . '" value="' . $group->getId() . '">';
                        echo '<input type="hidden" name="quantity_' . $n . '" value="' . (int)$_SESSION['cart'][$key]['count'] . '">';
                        echo '<input type="hidden" name="amount_' . $n . '" value="' . number_format(($group->getPrice() / CURRENCY_RATE) * ((int)$_SESSION['cart'][$key]['count']), 2, '.', '') . '">';

                        $n++;
                        $artikuli = $group->getProducts();
                        foreach ($artikuli as $dart) {
                            $dr = new artikul((int)$dart);
                            $option = '';
                            $a = 0;
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0 && $vall == $dr->getId()) {
                                        $artikulid = (int)$vall;
                                    }
                                    if ($keyy == 1 && $artikulid == $dr->getId() && $a == 0) {
                                        $option = $vall;
                                        $a = 1;
                                    }
                                }
                            }
                            echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($dr->getIme_marka()) . ' ' . stripslashes($dr->getIme()) . ' size ' . $option . '">';
                            echo '<input type="hidden" name="item_number_' . $n . '" value="' . $dr->getId() . '">';
                            echo '<input type="hidden" name="amount_' . $n . '" value="0">';
                            $n++;
                        }
                    }
                }
            }
            ?>
            <input type="submit" class="submit"
                   style="width:180px;float:right;margin: 0 45px 0 0; height: 36px; line-height: 25px;" name="submit"
                   value="check out" id="btn-paypal">
        </form>
    </div>
    <?php
    $tmp_msg = '';
    if ((isset($user_edit_error)) && ($user_edit_error)) foreach ($user_edit_error as $v) {
        $tmp_msg = $tmp_msg . ', ' . $v;
    } ?>
    <?php if ((isset($order_address_data_error)) && ($order_address_data_error)) foreach ($order_address_data_error as $v) {
        $tmp_msg = $tmp_msg . ', ' . $v;
    }
    $tmp_msg = trim($tmp_msg, ', ');
    if (!empty($tmp_msg)) {
        echo '<div class="error">';
        echo $tmp_msg;
        echo '</div>';
    }
    ?>
</div>
