<?php
$group = new paket((int)$_GET['gid']);
$online = true;

foreach ($group->getProducts() as $product) {
    $artikul = new artikul((int)$product);

    if ($artikul->isAvaliable() === false) $online = false;
}

$upd = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` = ?');
$upd -> bindValue(1, (int)$online, PDO::PARAM_STR);
$upd -> bindValue(2, $group->getId(), PDO::PARAM_INT);
$upd -> execute();

$group = new paket((int)$_GET['gid']);
$isAvaliable = (bool)$group->getOnline();
?>
<div class="product_navigation">
    <?php
    echo '<a style="color: #333;" href="' . url . '">' . $pageTitle . '</a> ';
    echo ' <span style="color: #333;">/</span> <a style="color: #333;" href="' . url . 'index.php?bundles">bundles </a>';
    echo ' <span style="color: #333;">/</span> <a style="color: #333;" href="' . url . 'index.php?gid=' . $group->getId() . '">' . mb_strtolower($group->getName(), 'UTF-8') . '</a>';
    ?>
</div>
<div class="clear"></div>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.serialScroll-1.2.1.js"></script>

<div style="clear:both; margin-top:10px;">
    <div id="proddivdetail">
        <script type="text/javascript">
            var addToCartRules = [];

            function hideModal() {
                $(".modal").hide();
            }

            function validate(rules) {
                for (x in rules) {
                    if ($("#" + rules[x][0]).val() == null || $("#" + rules[x][0]).val() == "") {
                        $(".modal-text-inside").text('Select ' + rules[x][2] + ' for ' + rules[x][1]);
                        $(".modal").show();
                        return false;
                    }
                }
                return true;
            }

            function updatePriceBySize(el) {
                var gID = $("input[name=addToCard]").val();
                $.ajax({
                    type: "POST",
                    url: "ajax/get_price_by_size_group_en.php",
                    data: "gid=" + gID + "&" + $('.price_tag').serialize(),
                    cache: false,
                    success: function (html) {
                        var result = jQuery.parseJSON(html);
                        $("#item_price").html(result.price);
                        $(".delivery-box").html(result.delivery);
                    }
                });
            }

            $(document).ready(function () {
                $('.product_a_image').fancybox({
                    'type': 'iframe',
                    'margin': 37,
                    'width': 1920,
                    'height': 1200
                });
                $('.questionsBar div a').fancybox({
                    'type': 'iframe',
                    'padding': 0,
                    'width': 800,
                    'height': 600
                });
                $('#addtocart').ajaxForm({
                    success: function (responseText) {
                        $.fancybox({
                            'type': 'iframe',
                            'href': '<?php echo url ?>/fancy_login.php?p=cart',
                            'padding': 0,
                            'width': 800,
                            'height': 800
                        });
                        $('#main_menu a.cart').load('<?php echo url ?>/template/bg/top_menu_shopping_cart.php');
                    }
                });
                $('.questionsBar div:last').css('border', 'none');
                var discriptionh = $('.product_description').height();
                if (discriptionh > 170) {
                    $('.product_description').css({
                        position: 'relative',
                        height: '170px',
                        overflow: 'hidden'
                    });
                    $('.showmoredis').css({
                        display: 'block'
                    });
                }
                $('.showmoredis').click(function normalhdis() {
                    $('.product_description').css({
                        height: discriptionh + 25 + 'px',
                    });
                    $('.hidemoredis').css({
                        display: 'block'
                    });
                    $('.showoredis').css({
                        display: 'none'
                    });
                })
                $('.hidemoredis').click(function normalhdis() {
                    $('.product_description').css({
                        height: '170px',
                    });
                    $('.hidemoredis').css({
                        display: 'none'
                    });
                    $('.showoredis').css({
                        display: 'block'
                    });
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                })
            });

            function movetocenter(url, img, n) {
                image = new Image();
                image.src = img;
                image.onload = function () {
                    var width = image.width;
                    var nowImg = $('#nowcenterimg').val();
                    var nowy = $('#img' + nowImg).height();
                    var nowx = $('#img' + nowImg).width();
                    var ny = $('#img' + n).height();
                    var nx = $('#img' + n).width();
                    if (width > 320) {
                        var ImgClass = "class='item_img'";
                    }
                    else {
                        var ImgClass = "class='item_img_small'";
                    }
                    var text = '<a id="proddetailscenterimg" class="product_a_image" href="' + url + '"><img ' + ImgClass + ' style="max-width: ' + width + 'px; min-width: 50px;" src="' + img + '" /></a>';
                    $("#proddetailscenterimg").replaceWith(text);
                    $('#img' + n).css({
                        border: '1px solid #FF671F'
                    });
                    $('#img' + nowImg).css({
                        border: '1px solid #FFFFFF'
                    });
                    $('#nowcenterimg').val(n);
                };
            }
        </script>
        <style>
            #proddetailscenterimg a {
                cursor: url(<?php echo url; ?>images/spyglass_z.cur);
            }
        </style>
        <input type="hidden" name="nowcenterimg" id="nowcenterimg" value="big">
        <div class="proddetailimage">
            <?php
            $filename = $group->getKatinka();
            $width = 0;
            if (file_exists($filename)) {
                $size = getimagesize($group->getKatinka());
                $width = $size[0];
            }

            $uwsidth = $width;

            $artikuli = $group->getProducts();
            // foreach($artikuli as $a){
            //     $artikul = new artikul((int)$a);
            //     $art_images = $artikul->getKatinka_dopalnitelni(false);
            //     if(isset($art_images)){
            //         for( $i=1;$i<=12;$i++){
            //             $image=$art_images[$i];
            //             if(!is_null ($image)&&file_exists($image->getKatinka())){
            //                 $size = getimagesize($image->getKatinka());
            //                 $width = $size[0];
            //                 $uwsidth+=$width +10;
            //             }
            //         }
            //     }
            // }

            $img = $group->getKatinka();
            list($width, $height, $type, $attr) = getimagesize($img);
            if ($width > 320) {
                $ImgClass = "class='item_img'";
            } else {
                $ImgClass = "class='item_img_small'";
            }

            echo '<div id="proddetaildopimgbox" style="margin-right: 40px;">';
            echo '<div style="display:none;border: 1px solid #FF671F; margin-bottom: 20px;" id="imgbig"><a onclick="movetocenter(\'' . url_template_folder . 'group_look_image.php?id=' . $group->getId() . '&image=' . $group->getKartinka_b() . '&img=big\', \'' . $group->getKatinka() . '\', \'big\');return false;" href="#"><img class="prodimgtumb" style="max-width: 95px; margin-bottom: 0;" src="' . $group->getKartinka_t() . '"/></a></div>';
            $n = 1;

            foreach ($artikuli as $a) {
                $artikul = new artikul((int)$a);
                echo '<div style="border: 1px solid #FFFFFF; margin-bottom: 20px;" id="img' . $n . '"><a onclick="movetocenter(\'' . url_template_folder . 'group_look_image.php?id=' . $group->getId() . '&image=' . $artikul->getKartinka_b() . '&img=' . $n . '\', \'' . $artikul->getKatinka() . '\', \'' . $n . '\');return false;" href="#"><img class="prodimgtumb" style="max-width: 95px; margin-bottom: 0;" src="' . $artikul->getKartinka_t() . '"/></a></div>';
                $n++;
            }
            // foreach($art_images as $image){
            //     if(!is_null ($image)){
            //         echo '<div style="border: 1px solid #FFFFFF; margin-bottom: 20px;" id="img'.$n.'"><a onclick="movetocenter(\''.url_template_folder.'look_image.php?id='.$artikul->getId().'&image='.$image->getKartinka_b().'&img='.$n.'\', \''.$image->getKatinka().'\', \''.$n.'\');return false;" href="#"><img class="prodimgtumb" style="max-width: 95px; margin-bottom: 0;" src="'.$image->getKartinka_t().'"/></a></div>';
            //         $n++;
            //     }
            // }
            echo '</div>';
            echo '<div id="proddetailbigimgbox" style="min-width: 50px;">';
            ?>
            <a id="proddetailscenterimg" class="product_a_image"
               href="<?php echo url_template_folder . 'group_look_image.php?id=' . $group->getId() . '&image=' . $group->getKartinka_b(); ?>&img=big"><img <?php echo $ImgClass; ?>
                        style="max-width: <?php echo $width; ?>px; min-width: 50px;"
                        src="<?php echo $group->getKatinka(); ?>"/></a>
            <div style="clear: both; height: 10px;"></div>
        </div>
        <?php if ($group->getDescription()) : ?>
            <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-bottom: 15px;"></div>
        <?php endif; ?>
        <div class="product_description">
            <?php echo nl2br($group->getDescription()); ?>
            <a href="javascript: void(0);" class="showmoredis" style="color: rgb(128, 128, 128);">read more <img
                        src="<?php echo url . "/images//nav/showmore.png"; ?>"></a>
            <a href="javascript: void(0);" class="hidemoredis" style="color: rgb(128, 128, 128);">hide</a>
        </div>
    </div>
    <div class="proddetailinfo">
        <h3 class="prodinfoname">
            <div class="prodcat"><?php echo $group->getName(); ?></div>
            <div style="color:#999;font-size:10px;font-weight: normal">#B<?php echo $group->getId(); ?></div>
            <style>
                .balloon {
                    display: inline-block;
                    border: 1px solid rgb(207, 207, 207);
                    margin-right: 4px;
                    width: 84px;
                    height: 63px;
                    background-color: #FFF;
                }

                .balloon:hover {
                    border: 1px solid #FF671F;
                }
            </style>
            <?php
            foreach ($artikuli as $dart) {
                $dr = new artikul((int)$dart);
                echo "<div class='balloon'><a style='font-size: 13px;' href='" . url . "index.php?id=" . $dr->getId() . "' target='_blank'><table style='border-collapse:collapse;'><tr><td style='width:84px;height:63px;text-align:center;'><img src='" . $dr->getKartinka_t() . "' style='margin:0 auto; display: block; max-width: 84px; max-height: 63px;' alt='" . $dr->getIme_marka() . " " . $dr->getIme() . "' title='" . $dr->getIme_marka() . " " . $dr->getIme() . "'/></td></tr></table></a></div>";
            }
            echo '<div style="clear: both;"></div>';
            ?>
        </h3>
        <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-top: 15px; margin-bottom: 15px;"></div>
        <?php
        echo '<div class="questionsBar">';
        echo '<div style="padding-left:0px;"><a style="font-size: 13px;" class="thickbox" href="' . url . 'fancy_qbs_group.php?p=howtobuy&id=' . $group->getId() . '">how<br />to buy?</a></div>';
        echo '<div><a style="font-size: 13px;" class="thickbox" href="' . url . 'fancy_qbs_group.php' . '?id=' . $group->getId() . '&p=sendquestion">question<br />about product</a></div>';
        echo '</div>';
        $t = $group->getPrice();
        ?>
        <div style="clear: both; height: 1px; background-color: #cfcfcf; margin-top: 15px; margin-bottom: 10px;"></div>
        <form id="addtocart" action="<?php echo url . 'cart.php'; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="addToCard" value="<?php echo $group->getId(); ?>"/>
            <input type="hidden" name="type" value="group"/>
            <input name="counts" value="1" type="hidden"/>
            <div class="prodinfotext">
                <?php
                $nn = 1;
                foreach ($artikuli as $dart) {
                    $dr = new artikul((int)$dart);
                    if ($dr->isAvaliable()) {
                        $add_artikul_etiket_grup = NULL;
                        $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ? ORDER BY `id` ');
                        $stm->bindValue(1, $dr->getId(), PDO::PARAM_INT);
                        $stm->execute();
                        foreach ($stm->fetchAll() as $v) {
                            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ORDER BY `id` ');
                            $stm->bindValue(1, $v['id_etiket'], PDO::PARAM_INT);
                            $stm->execute();
                            $temp = new etiket($stm->fetch());
                            $temp_grupa = new etiket_grupa((int)$temp->getId_etiketi_grupa());
                            $add_artikul_etiket_grup[$temp->getId_etiketi_grupa()] = $temp_grupa;
                            unset($temp);
                            unset($temp_grupa);
                        }
                        if (isset($add_artikul_etiket_grup) && (sizeof($add_artikul_etiket_grup) > 0)) {
                            foreach ($add_artikul_etiket_grup as $v) {
                                if ($v->getSelect_menu()) {
                                    if ($nn == 1) {
                                        echo '<div style="font-size: 13px; margin-top: 15px; margin-bottom: 10px; color: #808080;">choose size <br /><br /><span style="color: #444;">' . $dr->getIme_marka() . ' / ' . $dr->getIme() . '</span></div>';
                                    } else {
                                        echo '<div style="font-size: 13px; margin-top: 10px; margin-bottom: 10px;"><span style="color: #444;">' . $dr->getIme_marka() . ' / ' . $dr->getIme() . '</span></div>';
                                    }
                                    echo '<div class="prodinfotext">';
                                    echo '<input type="hidden" name="option' . $dr->getId() . '" id="option' . $dr->getId() . '"/>';
                                    echo '<input type="hidden" name="price_tag[' . $dr->getId() . ']" class="price_tag" id="price_tag' . $dr->getId() . '"/>';
                                    $etiketii = null;
                                    $pdo = PDOX::vrazka();
                                    $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id_etiketi_grupa` = :idgroupa ORDER BY `ordr`');
                                    $stm->bindValue(":idgroupa", $v->getId(), PDO::PARAM_INT);
                                    $stm->execute();
                                    if ($stm->rowCount() > 0)

                                        foreach ($stm->fetchAll() as $d) {
                                            $temp_etiket = new etiket((int)$d['id']);
                                            $etiketii[] = $temp_etiket;
                                            unset($temp_etiket);
                                        }

                                    if ($etiketii) {
                                        foreach ($etiketii as $e) {
                                            if ($dr->getEtiketizapisi()) if (in_array($e->getId(), $dr->getEtiketi_array_ids())) {
                                                echo '<a id="option_' . $dr->getId() . '_' . $v->getId() . '_' . $e->getId() . '" onclick="javascript:$(\'#option' . $dr->getId() . '\').val(\'' . $e->getIme() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);$(\'#price_tag' . $dr->getId() . '\').val(\'' . $e->getId() . '\');updatePriceBySize(this);return false;" href="#">' . $e->getIme() . '</a>';
                                            }
                                        }
                                    }
                                    echo '</div>';
                                    echo '
                                                        <script type="text/javascript">
                                                                addToCartRules.push(["option' . $dr->getId() . '","' . $dr->getIme() . '","' . $v->getIme() . '"]);
                                                        </script>
                                                        ';

                                }
                            }
                        }
                        unset($add_artikul_etiket_grup);
                    }
                    unset($dr);
                    $nn++;
                }
                ?>
            </div>
            <div style="clear:both; height: 15px;"></div>
            <div id="item_price" style="float: left; margin-top: 12px; width: 180px;" class="price-box">
                <span class="prodpricenormal" style="float:left;margin-right:10px;"> <span
                            style="color:#424242;font-size:12pt;font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($group->getPrice_normal() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup></span> <?php echo lang_currency_append; ?></span>
                <span class="prodpricepromo"
                      style="float:left !important; margin-left: 3px; font-size:20px;font-style: italic; color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($group->getPrice() / CURRENCY_RATE, 2, '.', ''));
                    echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> <?php echo lang_currency_append; ?></span>
                <?php if (isset($__user) && $__user->is_partner()) : ?>
                    <div style="position: absolute;bottom: 10px;margin-top: 5px; margin-right: 25px; font-size:8pt; color: #0071b9;">
                        End
                        user: <?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($group->getPrice_reference() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup><?php echo $tmp_cen[1]; ?></sup> <?php echo lang_currency_append; ?></div>
                <?php endif; ?>
                <?php
                $tmpraz = (($group->getPrice_normal() / CURRENCY_RATE) - ($group->getPrice() / CURRENCY_RATE));
                $tmp_razf = explode(".", number_format($tmpraz, 2, '.', ''));
                echo '<div class="saving-box">';
                echo '<span style="float: left;margin-top: 2px;margin-left: 5px;font-size: 8pt;color: #FF671F;">you save ' . lang_currency_prepend . '' . $tmp_razf[0] . '<sup>' . $tmp_razf[1] . '</sup> ' . lang_currency_append . ' (-' . round((($tmpraz / ($group->getPrice_normal() / CURRENCY_RATE)) * 100), 0) . '%)</span>';
                echo '</div>';
                ?>
            </div>
            <?php
            if ($isAvaliable) {
                echo '<input onclick="return validate(addToCartRules);" style="float:right; margin-top: 2px; height: 36px; line-height: 30px; width: 175px;" class="btnaddprod" type="submit" value="add to cart"/>';
            } else {
                echo '<div style="float:right; margin-top: 2px; height: 30px; line-height: 30px; background-color: #333; cursor: default; width: 150px; text-align: center;" class="btnaddprod" >sold out</div>';
            }

            $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
            $ds->execute();
            $d = $ds->fetch();
            $dostavka_cena = $d['delivery_price'];
            $dostavka_free = $d['free_delivery'];
            $additional_delivery = 0;

            foreach ($group->getProducts() as $d) {
                $group_article = new artikul((int)$d);
                if ($group_article->isAvaliable()) {
                    $additional_delivery += $group_article->getAdditionalDelivery();
                }
            }

            $ac = 0;
            $ac = $group->getPrice();

            echo '<div class="delivery-box">';

//            if ($dostavka_free < $ac) {
//                if ($additional_delivery != '' && $additional_delivery > 0) {
//                    echo '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">delivery: ' . $additional_delivery . ' bgn.</div>';
//                } else {
//                    echo '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">free delivery</div>';
//                }
//            } else {
//                if ($additional_delivery != '' && $additional_delivery > 0) {
//                    $total_delivery = $dostavka_cena + $additional_delivery;
//                    echo '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">delivery: ' . $total_delivery . ' bgn.</div>';
//                } else {
//                    echo '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">delivery: ' . $dostavka_cena . ' bgn.</div>';
//                }
//            }
            echo '</div>';
            ?>
            <div style="clear:both;"></div>
        </form>
    </div>
    <div style="float: right;text-align: left;width:100px;clear: right;margin-top:10px; margin-right: 310px;">
        <?php
        echo '<span><iframe src="//www.facebook.com/plugins/like.php?href=' . curPageURL() . '&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;colorscheme=dark" scrolling="no" frameborder="0" style="border:none; overflow:hidden;height:20px; " allowTransparency="true"></iframe></span>';
        ?>
    </div>
</div>
<div style="clear:both; height:20px"></div>
</div>
<div style="clear:both;"></div>
<div style="clear:both; height: 10px;"></div>
<style>
    #p-details-left-col {
        text-align: left;
        display: inline-block;
        vertical-align: top;
        margin-left: 20px;
        width: 160px;
    }

    #p-details-right-col {
        text-align: left;
        width: 1260px;
        margin: 0 auto;
        vertical-align: top;
        padding-bottom: 30px;
    }

    .lastseen {
        width: 1260px;
        margin: 0 auto;
    }
</style>
</center>
<div id="p-details-left-col">
    <?php
    // if((isset($banners))&&(sizeof($banners)>0)&&(!isset($_GET['hotOffers']))&&(!isset($_GET['branch']))){
    //      foreach($banners as $tmp_banner){
    //          echo '<a class="banner_item" ';
    //              if($tmp_banner->getDistinationURL()!='') echo ' href="'.$tmp_banner->getDistinationURL();
    //              echo '">';
    //              echo '<img style="width:160px;height:400px;" src="'.url.$tmp_banner->getImage_b().'" />';
    //          echo '</a>';
    //       }
    //  }
    ?>
</div>
<script>
    $(document).ready(function () {
        var width = $("#p-details-right-col").width(),
            broi = width / 270,
            group = <?php echo (int)$_GET['gid'] ?>;
        $.ajax({
            type: "POST",
            url: "ajax/ajax_product_group_products_group_en.php",
            data: "broi=" + broi + "&group=" + group,
            cache: false,
            success: function (html) {
                $('#group-products').html(html);
            }
        });
    });
</script>
<div id="p-details-right-col">
    <?php if (isset($_COOKIE['CatSeen'])) { ?>
        <script>
            $(document).ready(function () {
                var width = $("#p-details-right-col").width(),
                    broi = width / 270;
                $.ajax({
                    type: "POST",
                    url: "ajax/ajax_product_category_seen_en.php",
                    data: "broi=" + broi,
                    cache: false,
                    success: function (html) {
                        $('#categories-seen').html(html);
                    }
                });
            });
        </script>
        <div style="padding-bottom:5px;padding-left:22px;color:#808080;display:block;position:relative;z-index: 1;">
            suggested
        </div>
        <div id="categories-seen"></div>
        <div style="clear:both"></div>
    <?php } ?>
    <div style="padding-bottom:5px;padding-left:22px;color:#808080;display:block;position:relative;z-index: 1;">
        bundles
    </div>
    <div id="group-products"></div>
    <div style="clear:both"></div>
</div>
<div style="clear:both; height: 30px;"></div>
<?php
$lastSeen = array();

if (isset($_COOKIE["LastSeen"])) {
    $lastSeen = unserialize(stripslashes(rawurldecode($_COOKIE["LastSeen"])));
}

foreach ($lastSeen as $id) {
    if (isProductAvailable((int)$id)) {
        $lastSeenProducts[] = new artikul((int)$id);
    }
}

if (isset($lastSeenProducts) && (sizeof($lastSeenProducts) > 0)) {
    ?>
    <div class="lastseen">
        <div class="title">
            <div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">last
                seen
            </div>
            <div style="border-bottom: 1px dashed #CCC;display: block;height: 5px;margin: -14px 0 14px 0px;position: relative;"></div>
        </div>
        <?php
        if (isset($lastSeenProducts) && (sizeof($lastSeenProducts) > 0)) {
            foreach ($lastSeenProducts as $product) {
                echo '<a href="index.php?id=' . $product->getId() . '"><table class="imageCenter"><tr><td><img src="' . $product->getKartinka_t() . '" title="' . $product->getIme_marka() . ' ' . $product->getIme() . '"></td></tr></table></a>';
            }
        }
        ?>
    </div>
    <?php
}
?>
<div style="clear:both; height: 20px; border-bottom: 1px dashed #CCC;"></div>

<div class="modal">
    <div class="modal-content">
        <div class="modal-body" style="text-align: left !important;">
            <div style="width: 100%; height: 40px;" class="modal-text-inside"></div>
            <button onclick="hideModal();"
                    style="width: 100px;background-color: #FF671F; color: white;position: relative;top: 45px;display: block; text-align: center; line-height: 36px;height:36px; font-size: 13px; float: right;cursor: pointer;margin-left: 10px; margin-right: 10px;">
                OK
            </button>
        </div>
    </div>
</div>

<center>
