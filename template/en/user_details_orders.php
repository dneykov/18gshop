<style type="text/css">
    #cont {
        width: 1000px;
        margin: 5px auto;
        text-align: left;
        padding-bottom: 200px;
        font-size: 14px;
    }

    #nav-bar {
        height: 20px;
        font-size: 12px;
        padding-bottom: 20px;
        padding-left: 10px;
        width: 100%;
    }

    #nav-bar a {
        font-size: 12px;
    }

    #tabs-bar {
        height: 20px;
        width: 100%;
        padding-bottom: 2px;
        border-bottom: 1px solid #FB671F;
        margin-bottom: 20px;
    }

    #tabs-bar a {
        margin-left: 5px;
        margin-right: 20px;
        font-size: 14px;
    }

    #userOrders {
        margin-top: 20px;
    }

    .submit {
        display: block;
        background: none repeat scroll 0 0 #333;
        border: medium none;
        color: #fff;
        cursor: pointer;
        float: right;
        margin: 34px 0 0;
        height: 36px;
        width: 180px;
        line-height: 33px;
        text-align: center;
        text-decoration: none;
        font-size: 13px;
    }

    textarea {
        font-size: 13px;
        border: 1px solid #CCC;
    }

    .orderLink {
        width: 100%;
        height: 23px;
        color: #fff;
        cursor: pointer;
        background-color: #FB671F;
        margin-bottom: 10px;
        line-height: 23px;
        font-size: 12px;
    }

    .orderLink span {
        margin-left: 10px;
        margin-right: 10px;
    }

    .orderBox {
        padding: 5px;
        margin-bottom: 20px;
    }

    .orderCostBox {
        padding-bottom: 30px;
    }

    .orderDelivery {
        font-size: 13px;
        color: #333;
        margin-bottom: 5px;
        text-transform: uppercase;
    }

    .payment {
        font-size: 13px;
        color: #333;
        margin-bottom: 5px;
        text-transform: uppercase;
    }

    .orderCost {
        font-size: 13px;
        color: #333;
        margin-bottom: 5px;
        text-transform: uppercase;
    }

    .orderCommentBox {
        margin-bottom: 20px;
    }

    .orderComment {
        height: 130px;
        width: 470px;
        display: inline-block;
    }

    .orderComment:first-child {
        margin-right: 30px;
    }

    .orderCommentLabel {
        font-size: 12px;
        margin-bottom: 5px;
    }

    .orderTextarea {
        width: 470px;
        height: 100px;
    }

    .orderProductImg {
        width: 230px;
        height: 135px;
        text-align: center;
        margin-bottom: 10px;
    }

    .centerImg {
        width: 180px;
        height: 135px;
    }

    .orderProductInfo {
        height: auto;
        margin-bottom: 5px;
        overflow: hidden;
        width: 140px;
        float: left;
        display: inline-block;
        font-size: 11px;
        line-height: 18px;
        text-align: left;
    }

    .orderProduct {
        display: inline-block;
        vertical-align: top;
        margin-bottom: 30px;
        margin-right: 20px;
        font-size: 12px;
        width: 230px;
        position: relative;
    }

    .orderProductPrice {
        float: right;
        width: 85px;
        display: inline-block;
    }

    .o-product-price {
        color: #424242;
        float: right;
        margin-right: 5px;
        clear: right;
    }

    .orderProductAdditional {
        position: absolute;
        left: 0px;
        top: 70px;
        font-style: bold;
        font-size: 18px;
        color: #333333;
    }

    .preorder-img {
        position: absolute;
        top: 90px;
        right: 10px;
        margin-top: 5px;
    }
</style>
<div id="cont">
    <div id="nav-bar">
        <a href="<?php echo url; ?>" style="color: #4a4942;">18gshop</a> <span style="color: #4a4942;">/</span>
        <a href="<?php echo url . 'index.php?user'; ?>" style="color: #4a4942;">profile</a> <span
                style="color: #4a4942;">/</span>
        <a href="<?php echo url . 'index.php?user'; ?>" style="color: #4a4942;"><?php echo $__user->getName(); ?></a>
        <span style="color: #4a4942;">/</span>
        <?php if ($__user->is_partner()) : ?>
            <span style="color: #FB671F">партньор</span> <span style="color: #4a4942;">/</span>
        <?php endif; ?>
        <a href="<?php echo url . 'index.php?user&o'; ?>" style="color: #4a4942;">orders</a>
    </div>
    <div id="tabs-bar">
        <a href="<?php echo url . 'index.php?user'; ?>" style="color: #4a4942;">profile</a>
        <a href="<?php echo url . 'index.php?user&o'; ?>" style="color: #FB671F;">orders
            <?php
            $stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ? ORDER BY `id` DESC");
            $stm->bindValue(1, $__user->getId(), PDO::PARAM_STR);
            $stm->execute();
            echo $stm->rowCount();
            ?>
        </a>
    </div>
    <?php
    if (isset($__user)) {
        echo "<div id='userOrders'>";
        if ($stm->rowCount() > 0) {
            foreach ($stm->fetchAll() as $a) {
                $order = new order((int)$a['id']);
                $contactInformation = $order->getContactInformation();
                $orderProducts = $order->getProducts();
                $deletedItems = $order->getDeletedItems();
                $orderCost = $order->getCost();
                ?>
                <div class="orderLink">
							<span style="font-weight: bold;">
								<?php
                                if ($order->getStatus() == 3) {
                                    echo "Processed order / Preorder delivered";
                                } else if ($order->getStatus() == 2) {
                                    echo "Processed order";
                                } else if ($order->getStatus() == 1) {
                                    echo 'Approved order';
                                } else if ($order->getStatus() == 4) {
                                    echo 'Canceled order';
                                } else if ($order->getStatus() == 5) {
                                    echo 'Returned order';
                                } else {
                                    echo 'New order';
                                }
                                ?>
							</span>
                    <span><?php echo $order->getNomer() . " "; ?></span>
                    <span><?php echo $order->getDate(); ?></span>
                </div>
                <div class="orderBox" id="order-<?php echo $order->getId(); ?>">
                    <div class="orderProducts">
                        <?php
                        if (isset($orderProducts)) {
                            foreach ($orderProducts as $key => $val) {
                                if (in_array((int)$key, $deletedItems)) {
                                    $orderCost = $orderCost - $orderProducts[$key]['price'] * $orderProducts[$key]['count'];
                                    continue;
                                }
                                if ($orderProducts[$key]['type'] == "product") {
                                    $artikul = new artikul((int)$key);
                                    $option = '';
                                    if (isset($orderProducts[$key]['options'])) {
                                        foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                            $expl_arr = explode(",", $optionVal);
                                            foreach ($expl_arr as $keyy => $vall) {
                                                if ($keyy == 0) {
                                                    $artikulid = (int)$vall;
                                                }
                                                if ($artikul->getTaggroup_dropdown()) {
                                                    foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                                        $option .= " / " . $gr->getIme() . " " . $vall;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (isset($orderProducts[$key]['upgrade_option'])) {
                                        foreach ($orderProducts[$key]['upgrade_option'] as $upgrade_option) {
                                            $option .= " / " . $upgrade_option['upgrade_name'] . " " . $upgrade_option['name'];
                                        }
                                    }
                                    echo "<a href=" . url . "index.php?id=" . $artikul->getId() . " target='_blank'>";
                                    echo "<div class='orderProduct'>";
                                    if ($artikul->isPreorder()) {
                                        echo '<div class="preorder-img"><div style="background-color: #29AC92; width: 16px; height: 40px; color: #fff;">
                                                                <span class="rotate-90">PRE</span>
                                                                </div></div>';
                                    }
                                    echo "<div class='orderProductImg'>";
                                    echo "<table class='centerImg'><tr><td><img style='max-width:180px;max-height:135px;' src='" . url . $artikul->getKartinka_t() . "' /></td></tr></table>";
                                    echo "</div>";
                                    echo "<div class='orderProductInfo'>" . $artikul->getIme_marka() . " / " . $artikul->getIme() . $option . " / " . $orderProducts[$key]['count'] . "pcs.</div>";
                                    echo "<div class='orderProductPrice'>";
                                    ?>
                                    <span class="o-product-price"
                                          style="font-size:18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format((float)$orderProducts[$key]['price'] / CURRENCY_RATE, 2, '.', ''));
                                        echo $tmp_cen[0]; ?> <sup style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                                    <?php
                                    echo "</div>";
                                    if ($artikul->isPreorder()) {
                                        $tmp_cen = explode(".", number_format($orderProducts[$key]['preorder']['deposit_price'] / CURRENCY_RATE, 2, '.', ''));
                                        $toPayAtDelivery = explode(".", number_format(($orderProducts[$key]['price'] - $orderProducts[$key]['preorder']['deposit_price']) / CURRENCY_RATE, 2, '.', ''));
                                        echo "<div style='clear: both; color: #FB671F;'>with pre-order<br/>
																advance " . $orderProducts[$key]['preorder']['deposit_percent'] . "% <span style='font-style: italic;'>" . lang_currency_prepend . " " . $tmp_cen[0] . " <sup style='font-size:8pt'>" . $tmp_cen[1] . "</sup> " . lang_currency_append . "</span> " . ($order->getStatus() == 2 || $order->getStatus() == 3 ? '<strong>paid</strong>' : '') . "<br/>
																left <span style='font-style: italic;'>" . lang_currency_prepend . " " . $toPayAtDelivery[0] . " <sup style='font-size:8pt'>" . $toPayAtDelivery[1] . "</sup> " . lang_currency_append . "</span> " . ($order->getStatus() == 3 ? '<strong>paid</strong>' : 'at delivery') . "</div>";
                                    }
                                    echo "</div></a>";
                                    $dop_artikuli = $artikul->getAddArtikuli();
                                    if ($dop_artikuli != "") {
                                        $dop_art_arr = explode(",", $dop_artikuli);
                                        foreach ($dop_art_arr as $dart) {
                                            $dr = new artikul((int)$dart);
                                            $option = '';
                                            $n = 0;
                                            if (isset($orderProducts[$key]['options'])) {
                                                foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                                    $expl_arr = explode(",", $optionVal);
                                                    foreach ($expl_arr as $keyy => $vall) {
                                                        if ($keyy == 0 && $vall == $dr->getId()) {
                                                            $artikulid = (int)$vall;
                                                        }
                                                        if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                                            $option = " / size " . $vall;
                                                            $n = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            echo "<a href=" . url . "index.php?id=" . $dr->getId() . " target='_blank'>";
                                            echo "<div class='orderProduct' style='width: 180px;padding-left:40px;'>";
                                            echo "<div class='orderProductAdditional'>+</div>";
                                            echo "<div class='orderProductImg' style='width: 180px;'>";
                                            echo "<table class='centerImg'><tr><td><img style='max-width:180px;max-height:135px;' src='" . url . $dr->getKartinka_t() . "' /></td></tr></table>";
                                            echo "</div>";
                                            echo "<div class='orderProductInfo' style='width: 180px;'>" . $dr->getIme_marka() . " / " . $dr->getIme() . $option . " / 1pcs.</div>";
                                            echo "</div></a>";
                                        }
                                    }
                                } else {
                                    $group = new paket((int)$key);
                                    $dop_art_arr = $group->getProducts();

                                    echo "<a href=" . url . "index.php?gid=" . $group->getId() . " target='_blank'>";
                                    echo "<div class='orderProduct'>";
                                    echo "<div class='orderProductImg'>";
                                    echo "<table class='centerImg'><tr><td><img style='max-width:180px;max-height:135px;' src='" . url . $group->getKartinka_t() . "' /></td></tr></table>";
                                    echo "</div>";
                                    echo "<div class='orderProductInfo'>" . $group->getName() . " / " . $orderProducts[$key]['count'] . "pcs.</div>";
                                    echo "<div class='orderProductPrice'>";
                                    ?>
                                    <span class="o-product-price"
                                          style="font-size:18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format((float)$orderProducts[$key]['price'] / CURRENCY_RATE, 2, '.', ''));
                                        echo $tmp_cen[0]; ?> <sup style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                                    <?php
                                    echo "</div>";
                                    echo "</div></a>";
                                    foreach ($dop_art_arr as $dart) {
                                        $dr = new artikul((int)$dart);
                                        $option = '';
                                        $n = 0;
                                        if (isset($orderProducts[$key]['options'])) {
                                            foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                                $expl_arr = explode(",", $optionVal);
                                                foreach ($expl_arr as $keyy => $vall) {
                                                    if ($keyy == 0 && $vall == $dr->getId()) {
                                                        $artikulid = (int)$vall;
                                                    }
                                                    if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                                        $option = " / size " . $vall;
                                                        $n = 1;
                                                    }
                                                }
                                            }
                                        }
                                        echo "<a href=" . url . "index.php?id=" . $dr->getId() . " target='_blank'>";
                                        echo "<div class='orderProduct' style='width: 180px;padding-left:40px;'>";
                                        echo "<div class='orderProductAdditional'>+</div>";
                                        echo "<div class='orderProductImg' style='width: 180px;'>";
                                        echo "<table class='centerImg'><tr><td><img style='max-width:180px;max-height:135px;' src='" . url . $dr->getKartinka_t() . "' /></td></tr></table>";
                                        echo "</div>";
                                        echo "<div class='orderProductInfo' style='width: 180px;'>" . $dr->getIme_marka() . " / " . $dr->getIme() . $option . " / " . $orderProducts[$key]['count'] . "pcs.</div>";
                                        echo "</div></a>";
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                    <div class="orderCommentBox">
                        <?php
                        if ($order->getComment() != "") {
                            echo '<div class="orderComment">';
                            echo '<div class="orderCommentLabel">comment</div>';
                            echo '<textarea class="orderTextarea" disabled>' . $order->getComment() . '</textarea>';
                            echo '</div>';
                        }
                        if ($order->getAdminComment() != "") {
                            echo '<div class="orderComment">';
                            echo '<div class="orderCommentLabel" style="color: #FB671F;">comment from us</div>';
                            echo '<textarea class="orderTextarea" disabled>' . $order->getAdminComment() . '</textarea>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                    <div class="orderCostBox">
                        <div class="payment">payment type
                            <span style="font-size:14px;color:#FB671F; display: inline-block; text-transform: none;">
                                    <?php
                                    switch ($order->getPaymentType()) {
                                        case "cash":
                                            echo "Pay at delivery";
                                            break;
                                        case "bank":
                                            echo "Bank transfer";
                                            break;
                                        default:
                                            echo "Pay at delivery";
                                            break;
                                    }
                                    ?>
                                    </span>
                        </div>
                        <?php if ($order->getPaymentType() == "bank") : ?>
                            <div class="payment">IBAN
                                <span style="font-size:14px;font-style: italic; display: inline-block;text-transform: none;"><?php echo $order->getIban(); ?></span>
                            </div>
                        <?php endif; ?>
                        <?php if ($order->getPaymentType() == "bank") : ?>
                            <div class="payment">bank details
                                <br>
                                <span style="font-size:14px;font-style: italic; display: inline-block;text-transform: none;"><?php echo nl2br($order->getBankDetails()); ?></span>
                            </div>
                        <?php endif; ?>
                        <div class="orderDelivery">delivery
                            <?php if ($contactInformation['delivery'] === 'pickup'): ?>
                                <span style="color: #FB671F;font-size:14px; display: inline-block;text-transform: none;">Pickup from us</span>
                            <?php else: ?>
                                <span style="font-size:14px;font-style: italic; display: inline-block;text-transform: none;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format((float)$contactInformation['delivery'] / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                            <?php endif; ?>
                        </div>
                        <div class="orderCost">total
                            <?php if ($order->isPreorder()) : ?>
                                <span style="font-size: 14px;font-style: italic; display: inline-block;text-transform: none;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($order->getPreorderCost() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                            <?php else : ?>
                                <span style="font-size: 14px;font-style: italic; display: inline-block;text-transform: none;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($orderCost / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                            <?php endif; ?>
                            <?php
                            if ($order->getPaypal() == 1) {
                                echo "<br />Paypal";
                                if ($order->getPaypalPaid() == 1) {
                                    echo ": paid";
                                }
                            }
                            ?>
                        </div>
                        <?php if ($order->isPreorder()) : ?>
                            <div class="orderCost">need to pay
                                <span style="font-size: 18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format(($orderCost - $order->getPreorderCost()) / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                                <?php if ($order->getStatus() == 3) : ?>
                                    <span style="color: #FB671F; font-size: 12px;"><strong>paid</strong></span>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
            }
        } else {
            echo "<div class='serctionTitle'>No orders yet.</div>";
        }
        echo "</div>";
    } else {
        echo "<center>Please login into your account</center>";
    }
    ?>
</div>
</div>