<?php
	require dir_root_template.'__top.php';
?>
<style type="text/css">
	.title{
		color:#4d4d4d;
		font-size:9pt;
		margin:0 10px;
		text-align:left;
	}
	.title a{
		color:#4d4d4d;
		font-weight: bold;
		font-size:12pt;
	}
	.submit {
		background: none repeat scroll 0 0 #FF671F;
		border: medium none;
		color: #fff;
		cursor: pointer;
		float: right;
		margin: 34px 0 0;
		padding: 0 8px 2px;
		text-decoration: none;
	}
	.error{
		color: #FE0000;
		font-size:9pt;
		margin:10px;
		clear:both;
	}
</style>
<style type="text/css">
	.registrationDetails {
		width: 100%;
		margin-left: -20px;
	}
	.registrationDetails form{
		width: 100%;
	}
	.registrationDetails table{
		border: none;
	}
	.registrationDetails table tr{
		margin:10px 0;
	}
	.registrationDetails table tr td{
		font-size: 9pt;
	}
	.registrationDetails table tr td input{
		margin-bottom: 15px;
	}
	.register {
		background: none repeat scroll 0 0 #FF671F;
		border: medium none;
		color: #fff;
		cursor: pointer;
		float: right;
		text-decoration: none;
		height: 26px;
	}
	select {
		border: 1px solid #999;
	}
	input, textarea {
		font-size: 13px;
	}
	input:hover, input:focus, select:hover, select:focus, textarea:hover, textarea:focus {
		border: 1px solid #FF671F;
	}
	input, select {
		 padding: 5px;
	}
</style>
<script type="text/javascript" src="<?php echo url; ?>js/jquery-1.7.1.min.js"></script>
<div style="width: 1000px; margin: 100px auto; text-align: left; padding-bottom: 100px;">
	<?php
		if(isset($sucessSend) && $sucessSend == true){
			echo "<div style='margin-bottom: 20px;'>Your order was send successfuly!</div>";
		}
	?>
	<h3>Order details</h3>

	<div class="registrationDetails">	
		<form action="" method="post">
			<table>
				<tr>
					<td>first name</td>
					<td>last name</td>
					<td colspan="4">GSM</td>
				</tr>
				<tr>
					<td><?php echo $__user->getName(); ?></td>
					<td><?php echo $__user->getName_last(); ?></td>
					<td colspan="4"><input  style="width:468px;" type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm'];else{echo $__user->getPhone_mobile();} ?>" /></td>
				</tr>
				<tr>
					<td colspan="2">mail</td>	
					<td colspan="4" >country</td>
				</tr>
				<tr>
					<td colspan="2"><input style="width:450px;margin-right:0px;" type="text" autocomplete="off" name="edit_mail" value="<?php if(isset($_POST['edit_mail'])) echo $_POST['edit_mail'];else{echo $__user->getMail();} ?>"/></td>	
					<td colspan="4" valign="top">
						<select id="country" name="country" style="width: 480px;">
						<?php
							$stm = $pdo -> prepare("SELECT * FROM `locations`");
							$stm -> execute();
							foreach ($stm -> fetchAll() as $c) {
								echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">comment or details about the order</td>
					<td colspan="4" >city</td>
				</tr>
				<tr>
	            	<td colspan="2" rowspan="5" valign="top"><textarea name="comment" style="width:457px;height:127px; margin-right: 20px;"><?php if(isset($_POST['comment'])) echo $_POST['comment']; else echo "comment"; ?></textarea>
	            	<td colspan="4"><input style="width:468px;" type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; else{echo $__user->getAdress_town();} ?>"/></td>
	            </tr>

				<tr>
					<td colspan="3">street</td>
					<td>number</td>
				</tr>
				<tr>					
					<input type="hidden" autocomplete="off" name="register_pass" value="<?php if(isset($_POST['register_pass'])) echo $_POST['register_pass']; ?>">
	                <input type="hidden" autocomplete="off" name="register_passAgain" value="<?php if(isset($_POST['register_passAgain'])) echo $_POST['register_passAgain']; ?>"></td>
	                <td colspan="3"><input type="text" autocomplete="off" name="street" style="width: 375px;"value="<?php if(isset($_POST['street'])) echo $_POST['street']; else{echo $__user->getAdress_street();} ?>" /></td>
					<td><input style="width:55px" type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; else{echo $__user->getAdress_number();} ?>" /></td>
				</tr>

				<tr>
					<td>en.</td>
					<td>fl.</td>
					<td colspan="2">ap.</td>
				</tr>
				<tr>			
					<td><input style="width:100px" type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" /></td>
					<td><input style="width:100px" type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['etaj'])) echo $_POST['etaj']; else{echo $__user->getAdress_etaj();} ?>" /></td>
					<td><input style="width:100px" type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; else{echo $__user->getAdress_ap();}?>" /></td>
				</tr>
			</table>
			<input type="hidden" autocomplete="off" name="telephone" value="<?php if(isset($_POST['telephone'])) echo $_POST['telephone']; else{echo $__user->getPhone_home();} ?>" /> 
			<input type="hidden" name="buy" value="buy"/>
			<input  class="register" style="width:180px;float:right; height: 36px; line-height: 25px;" type="submit" name="submit" value="finish" class="submit">
		</form>
		<?php
			$tmp_msg='';
			if((isset($user_register_error))&&($user_register_error)) foreach ($user_register_error as $v) {
			//echo '<div class="error">';
				$tmp_msg= $tmp_msg.', '.$v;
			//echo '</div>';
			} ?>
		<?php if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
			//echo '<div class="error">';
				$tmp_msg= $tmp_msg.', '.$v;
			//echo '</div>';
			} 
			$tmp_msg=trim($tmp_msg,', ');
			if(!empty($tmp_msg)){
				echo '<div class="error">';
					echo $tmp_msg;
				echo '</div>';
			}
		?>				
	</div>
</div>
<?php
	require dir_root_template.'__footer.php';
?>
