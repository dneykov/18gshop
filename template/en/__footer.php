<style>
    .cc-btn {
        background-color: #FF671F !important;
        border-radius: 0px !important;
        color: #fff !important;
        height: 19px !important;
        padding-bottom: 8px !important;
        padding-top: 4px !important;
    }

    .cc-window {
        border-radius: 0px !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".branch img").hover(function () {
                var src = $(this).attr("src").replace(".jpg", "_color.jpg");
                $(this).attr("src", src);
            },
            function () {
                var src = $(this).attr("src").replace("_color.jpg", ".jpg");

                $(this).attr("src", src);
            });
    });
    $(document).ready(function () {

        $('#footer-newsletter #submit').click(function (e) {
            e.preventDefault();
            var email = $('#email').val();
            var err = false;
            if (!validateEmail(email) || email == '') {
                $('.response').html('<span class="error">Please enter a valid email address.</span>');
                err = true;
            }
            if (err == false) {
                $.ajax({
                    type: "POST",
                    url: "ajax/subscribe_en.php",
                    data: "email=" + email,
                    cache: false,
                    success: function (data) {
                        if (data != '') {
                            $('.response').html(data);
                            $('#email').val('');
                        }
                    }
                });
            }
        });

        $('.footer a.howtobuy').fancybox({
            'type': 'iframe',
            'padding': 0,
            'width': 800,
            'height': 600
        });
        $('.footer a.terms').fancybox({
            'type': 'iframe',
            'padding': 0,
            'width': 800,
            'height': 600
        });
        $(window).scroll(function () {

            if ($(window).scrollTop() > 66) {
                $('#main_menu').css({
                    'position': 'fixed',
                    'top': '0px',
                    'width': '100%',
                    'box-shadow': '0 1px 1px 0 rgba(0,0,0,0.25)',
                    'z-index': 9,
                    'min-width': '1260px'
                });
                $('#logoc').css('display', 'block');
                $('.menu-inner .change-lang').css('display', 'block');
                $('#main_menu ul').css({'margin-left': '17px', 'left': '-8px'});
            }
            else {
                $('#main_menu').css({
                    'position': '',
                    'top': '',
                    'width': '',
                    'box-shadow': '0 1px 1px 0 rgba(0,0,0,0.25)'
                });
                $('#logoc').css('display', 'none');
                $('.menu-inner .change-lang').css('display', 'none');
                $('#main_menu ul').css({'margin-left': '80px', 'left': '0px'});
            }
        }); //scroll
    });

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }

    function SendFooterMail() {
        var mail = $('#footer-email').val();
        var msg = $('#footer-message').val();
        var err = "";
        var en = 0;
        if (mail == "") {
            err += "e-mail";
            en++;
        }
        if (msg == "") {
            if (en == 1) err += ", ";
            err += " съобщение";
        }
        if (err == "") {
            $.ajax({
                type: "POST",
                url: "ajax/ajax_send_mail.php",
                data: "mail=" + mail + "&msg=" + msg,
                cache: false,
                success: function (html) {
                    $('#footer-email').val("");
                    $('#footer-message').val("");
                    $('#footer-form-msgs').html(" ");
                    $('#footer-form-msgs').html("<span style='font-size: 11px; color: #FF671F;'>The Message was send successfuly.<span>");
                }
            });
        } else {
            $('#footer-form-msgs').html(" ");
            $('#footer-form-msgs').html("<span style='font-size: 11px; color: #FF671F;'>Please insert " + err + ".</span>");
        }
    }
</script>
<div id="branches" class="branches jThumbnailScroller" style="margin-bottom: 40px;">
    <div class="jTscrollerContainer">
        <div class="jTscroller">
            <?php
            if (isset($branches)) {
                $pdo = PDOX::vrazka();
                foreach ($branches as $branch) {
                    $tmp_branch = new model($branch);

                    $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_model` in (SELECT `id` FROM `model` where `ime` = ?) AND IsOnline=1 AND `isDelete`=0 AND `Okazion`=0';
                    $stm = $pdo->prepare($sql);
                    $stm->bindValue(1, $tmp_branch->getIme(), PDO::PARAM_STR);
                    $stm->execute();
                    $data = $stm->fetch();

                    if (((int)$data[0]) > 0) {
                        echo '<a class="branch" href="' . url . 'index.php?branch=' . $branch['ime'] . '"><img style="height:60px;"  src="' . url . $tmp_branch->getBWImage() . '" /></a>';
                    }
                }
            }
            ?>
        </div>
    </div>
    <a href="#" class="jTscrollerPrevButton" style="margin: -14px 10px 0 10px !important;"></a>
    <a href="#" class="jTscrollerNextButton" style="margin: -14px 10px 0 10px !important;"></a>

</div>
<div id="h-footer">
    <div id="h-footer-box">
        <div id="footer-cats">
            <div class="f-label" style="padding-top: 2px;"><a href="index.php?help"
                                                              style="display: block; font-size: 18px;color: #c9c9c9;text-transform: uppercase;">information</a>
            </div>
            <div style="clear: both; height: 5px;"></div>
            <?php
            $stm = $pdo->prepare("SELECT * FROM `help_groups` ORDER BY `id` ASC");
            $stm->execute();
            $n = 0;
            foreach ($stm->fetchAll() as $v) {
                $temp = $pdo->prepare("SELECT * FROM `help_articles` WHERE `cat_id` = ?");
                $temp->bindValue(1, (int)$v['id'], PDO::PARAM_INT);
                $temp->execute();
                if ($temp->rowCount() > 0) {
                    $n++;
                    if ($n == 1) {
                        echo "<div class='f-cats-col'>";
                        echo "<ul>";
                    }
                    echo "<li><a href='" . url . "index.php?help&c=" . $v['id'] . "'>" . $v['name_en'] . "</a></li>";
                    if ($n == 5 || $n == 10) {
                        echo "</ul></div><div class='f-cats-col'><ul>";
                    }

                }
            }
            if ($n != 0) {
                echo "</ul>";
                echo "</div>";
            }
            ?>
            <div style="clear: both;"></div>
        </div>
        <div id="footer-contacts">
            <div class="f-label"><a href="index.php?contacts"
                                    style="display: block; font-size: 18px; padding-top: 3px;">contacts</a></div>
            <div style="clear: both; height: 6px;"></div>
            <?php
            $contact = $pdo->prepare('SELECT * FROM `zz_regioni_magazini` LIMIT 1');
            $contact->execute();
            $c = $contact->fetch();
            echo nl2br($c['info_' . lang_prefix]);
            ?>
        </div>
        <div id="footer-newsletter">
            <div class="f-label"><a href="index.php?contacts"
                                    style="display: block; font-size: 18px; padding-top: 3px;">newsletter</a></div>
            <div style="clear: both; height: 6px;"></div>
            <p style="width:235px;">First to know about our new products and promotions. Subscribe to our
                newsletter!</p>

            <form action="" method="post">
                <input type="text" name="email" placeholder="e-mail" id="email"/>
                <input type="submit" name="submit" value="subscribe me" id="submit"/>
            </form>
            <div class="response">

            </div>
        </div>
        <div id="footer-social">
            <div class="f-label"><a href="index.php?contacts"
                                    style="display: block; font-size: 18px; padding-top: 3px;">LIKE US</a></div>
            <div style="clear: both; height: 10px;"></div>
            <div class="fb-page" data-href="https://www.facebook.com/pages/18GSHOP/348633109873" data-width="280"
                 data-height="218" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
        </div>
        <!-- <div id="footer-likes">
        	<div class="fb-like-box" data-href="https://www.facebook.com/pages/18GSHOP/348633109873" data-width="240" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
    	</div> -->
        <div style="clear:both;"></div>
    </div>
</div>
<div id="footer-wrapper">
    <table class="footer" style="text-align: left;">
        <tr>
            <?php
            if (($all_kategorii) && (sizeof($all_kategorii) > 0)) {

                foreach ($all_kategorii as $tmp_kategoriq) {
                    echo "<td valign='top'><div class='banner' >";
                    echo '<a class="footer_menu_item" href="' . url . '/index.php?category=' . $tmp_kategoriq->getId() . '">';
                    echo '<div><i>' . $tmp_kategoriq->getIme() . '</i></div>';
                    echo '</a>';
                    echo '<div class="vid_menu" style="margin-top: -10px;">';
                    foreach ($tmp_kategoriq->getVidove() as $tmp_vid) {
                        echo '<a href="' . url . '/index.php?category=' . $tmp_kategoriq->getId() . '&mode=' . $tmp_vid->getId() . '">' . $tmp_vid->getIme() . '</a></br>';
                    }
                    echo '</div>';
                    echo "</div></td>";
                }
            }
            ?>
            <td valign='top'>
            </td>
        </tr>
    </table>
</div>

<div id="footer-nav">
    <div id="inner">
        <p class="copy"><?php echo $pageTitle;?> &copy; <?php echo date("Y"); ?>. All Rights Reserved. </p>
        <h1 style="display: inline;line-height: 40px;font-size: 11px;font-weight: normal;">&nbsp;<?php echo $h1heading; ?></h1><span style="margin-left: 10px;"><a href="http://bigcoin.com/dev/"
                                                  style="color: rgb(128, 128, 128); text-decoration: none; font-size: 11px;"
                                                  target="_blank">Created by WebMaster</a></span>
        <div id="contact">
            <a href="http://www.facebook.com/pages/18GSHOP/348633109873" target="_blank">
                <img src="images/logo_fb.png" width="21" height="21" alt=""/>
            </a>
            <a href="skype:pb.18gshop.pro?call">
                <img src="images/logo_skype.png" width="21" height="21" alt=""/>
            </a>
            <p class="phone">+ 359 885 938 601</p>
        </div>
    </div>
</div>
</div>
</center>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.1/cookieconsent.min.js"></script>
<script>window.cookieconsent.initialise({
        "palette": {
            "popup": {"background": "#333333"},
            "button": {"background": "#FF671F"}
        },
        "showLink": false,
        "theme": "classic",
        "content": {
            "message": "We are using cookies to give you the best experience on our site. <a href='index.php?privacy' target='_blank' style='color: #FF671F;'>Read more</a>",
            "dismiss": "Accept!"
        }
    });</script>
</body>
</html>
