<?php


if($_POST){
	$err=NULL;
	if((!isset($_POST['email']))||(trim($_POST['email'])=='')||(!check_email_address($_POST['email']))||($_POST['email']=='e-mail or phone')){
		$err[]="valid email or phone";
	}
	if((!isset($_POST['name']))||(trim($_POST['name'])=='')||($_POST['name']=='name')){
		$err[]="name";
	}
	if((!isset($_POST['content']))||(trim($_POST['content'])=='')||($_POST['content']=='message')){
		$err[]="message";
	}
}

?>

		<style type="text/css">
			*{
				margin:0;
				padding:0;
			}

			.title{
				font-family: Verdana;
				color:#000;
				margin-top:10px;
				padding-top: 10px;
			}
			.title .productName{
				font-size:13pt;
				color:#000;
			}
			body{
				font-family: Verdana;
				color:#000;
				background: #fff;
			}
			#btnSend{
				display:inline-block;
				color:#fff;
				border:none;
				font-weight:400;
				height: 35px;
				padding-bottom: 5px;
				float:right;
				font-family: Verdana;
				font-size:15px;
				cursor: pointer;
				width:200px;
				margin-top: 85px;
			}
			#btnCancle{
				font-family: Verdana;
				display:inline-block;
				margin:11px 0px 0px;
				font-weight:900;
				display:inline-block;
				color:#000;
				text-decoration: none;
				float:right;
				font-size:13pt;
			}
			#success{
				clear: both;
				color: #FE0000;
				display: none;
				float: left;
				font-size: 9pt;
				margin-left: 13px;
				margin-top: 0;
			}
			input,textarea{
				font-family: Verdana;
				font-size: 11pt;
				color:#808080;
			}
			.product_description{
				float: left;
				height: 200px;
				margin-bottom: 10px;
				overflow: hidden;
				width: 265px;
				word-wrap: break-word;
				font-family: Verdana;
				font-size: 11pt;
				color:#808080;
				line-height: 18px;
			}
			.product_description ul{

				 margin-left: 13px;
			}
			.product_description ol{

				 margin-left: 19px;
			}
		</style>

<?php if(isset($artikul)) { ?>
		<div style="margin-left:10px;" class="title"><span class="productName" style="color: rgb(51, 51, 51); font-size: 16px;"><?php echo  '<span style="font-weight: bold;">'.$artikul->getIme_marka().'</span> '.$artikul->getIme().''; ?></span></div>
		<?php
				echo '<div style="height:200px;margin:10px;margin-right:20px;" >';
					echo '<div style="float:left;width:420px;"><img style="display: block; max-height:200px;max-width:400px; margin: 0 auto;" src="'.url.$artikul->getKatinka().'"/></div>';
					$brand=$artikul->getBrand();
					echo '<div style="margin:0px 0;max-height:80px;margin-right:20px;float:right;clear:right; width: 300px; text-align: right;"><img src="'.url.$brand->getImage().'"/></div>';
					echo '<div class="product_description" style="width: 260px;">'.$artikul->getDescription().'</div>';
				echo '</div>';
		?>

		<script type="text/javascript">
			$(document).ready(function() {
				   $('#clname').click(function() {
						$(this).val('');
					});
					$('#email').click(function() {
						$(this).val('');
					});
					$('#question').click(function() {
						$(this).val('');
					});
			});
		</script>
		<form style="margin-left:10px;" name="mailform" action="?id=<?php echo $artikul->getID(); ?>&p=<?php echo $_GET['p']; ?>" method="post">
			<div style="float:right;width:320px; margin-right: 40px;">
				<input id="clname" style="width:320px;margin-bottom:15px;height:25px;line-height:25px;" name="name" type="text" <?php if(isset($_POST['name'])){ echo 'value="'.$_POST['name'].'"';} else {if(isset($__user)){ echo 'value="'.$__user->getName().' '.$__user->getName_last().'"';}else{ echo 'value="name"';}} ?> />
				<br/><input id="email" style="width:320px;margin-bottom:14px;height:25px;line-height:25px;" name="email" type="text" <?php if(isset($_POST['email'])){ echo 'value="'.$_POST['email'].'"';} else{ if(isset($__user)){ echo 'value="'.$__user->getMail().'"';}else{ echo 'value="e-mail or phone"'; }}?> />
				<br/><input id="btnSend" value="send" type="submit" style="background-color: #FF671F;"/>
			</div>
			<textarea id="question" style="resize: none;float:left;width:400px;height:200px" name="content"><?php if(isset($_POST['content'])) { echo $_POST['content'];}else { echo 'message';}?></textarea>
			<input name="id" value="<?php echo $artikul->getID(); ?>" type="hidden"/>
		</form>
<?php } else { ?>
    <form style="margin-top:50px; padding-bottom:20px;" name="mailform" action="?p=<?php echo $_GET['p']; ?>" method="post">
        <textarea id="question" style="resize: none;width:300px;height:250px;margin-bottom: 5px;" name="content" placeholder="your question here" ><?php if(isset($_POST['content'])) { echo $_POST['content'];}?></textarea> <span style="display:inline-block;vertical-align:top;"></span>
        <div style="height: 8px;width:100%;"></div>
        <input id="clname" style="width:300px;margin-bottom:5px;padding: 5px;height: 28px;" name="name" type="text" <?php if(isset($_POST['name'])){ echo 'value="'.$_POST['name'].'"';} else {if(isset($__user)){ echo 'value="'.$__user->getName().' '.$__user->getName_last().'"';}} ?> placeholder="name"  /><span></span>
        <div style="height: 10px;width:100%;"></div>
        <input id="email" style="width:300px;margin-bottom:5px;padding: 5px;height: 28px;" name="email" type="text" <?php if(isset($_POST['email'])){ echo 'value="'.$_POST['email'].'"';} else{ if(isset($__user)){ echo 'value="'.$__user->getMail().'"';}}?>  placeholder="e-mail" /><span></span>
        <div style="height: 10px;width:100%;"></div>
        <input id="phone" style="width:300px;margin-bottom:0px;padding: 5px;height: 28px;" name="phone" type="text" <?php if(isset($_POST['phone'])){ echo 'value="'.$_POST['phone'].'"';} else{ if(isset($__user)){ echo 'value="'.$__user->getPhone_mobile().'"';}}?>  placeholder="phone" />
        <input id="btnSend" value="send" type="submit" style="width: 180px;
float: right;
margin: 10px 25px 0 0;
height: 36px;
line-height: 32px;
background: #FF671F;"/>
        <div style="clear:both"></div>
    </form>
<?php } ?>
		<div id="success">
			<?php
				if($_POST){
					?>
						<script type="text/javascript">
							$('#success').css('display','block');
						</script>
					<?php
					if($err!==NULL){

						$tmp= 'Please insert ';
						foreach($err as $tmp_err){
							$tmp.= $tmp_err.', ';
						}
						echo trim($tmp, ", ");
					}else{
						echo '<span style="color:#00A7EC">Your question was sent successfuly</span>';
					}
				}
			?>
		</div>
<?php
if($_POST) {
if ($err === NULL) {

    if (isset($_POST['id']) && (trim($_POST['id']) != '')) {
		$artikul = new artikul((int)$_POST['id']);
		$email=htmlspecialchars($_POST['email']);
		$name=htmlspecialchars($_POST['name']);
		$product="";
		$product=$product.$artikul->getIme_marka().' '.$artikul->getIme().' ';
		$product=$product.$artikul->getKod().' ';

		$subject="Запитване за: ";
		$subject=$subject.$product;
		$subject=$subject.' от '.$name." ";

		$email=htmlspecialchars($_POST['email']);
		$name=htmlspecialchars($_POST['name']);

		$msg="Запитване за: ".$product." <a href=\"".url."index.php?id=".$artikul->getId()."\">".url."index.php?id=".$artikul->getId()."</a><br/>";//"Име: ".$name."<br/>";
		$msg=$msg."от ".$name." ".$email."<br/>";
		$msg=$msg."Въпрос: ".htmlspecialchars($_POST['content']);

		if(mail(mail_office, $subject, $msg, "Content-type: text/html; charset=UTF-8\r\nFrom: ".'vupros@18gshop.com'." \nReply-To: ".$email)){
		?>

			<script type="text/javascript">
				setTimeout(function(){
					parent.$.fancybox.close();
				}, 3000);

			</script>
		<?php
		}

	} else {
        $email = htmlspecialchars($_POST['email']);
        $name = htmlspecialchars($_POST['name']);

        $subject = "Въпрос от " . $name;

        $msg = "Въпрос от " . $name . " " . $email . "<br/>";
        $msg .= "Въпрос: " . htmlspecialchars($_POST['content']);

        if (mail(mail_office, $subject, $msg, "Content-type: text/html; charset=UTF-8\r\nFrom: " . 'vupros@18gshop.com' . " \nReply-To: " . $email)) {
            ?>
            <script type="text/javascript">
                setTimeout(function () {
                    parent.$.fancybox.close();
                }, 3000);
            </script>
        <?php
        }
    }
}
}
?>
