<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="<?php echo url_template_folder; ?>css/css3.css" type="text/css" >
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
    <style type="text/css">
        #main_menu li {
            display: block;
            list-style: none outside none;
            height: 25px;
        }
        #main_menu li a {
            width: 100%;
        }
        h3{
            color:#4d4d4d;
            font-size:16pt;
            margin:10px 10px 0;
            font-weight: normal;
            text-align:left;
        }
        .title{
            color:#4d4d4d;
            font-size:9pt;
            margin:0 10px;
            text-align:left;
        }
        .title a{
            color:#4d4d4d;
            font-weight: bold;
            font-size:12pt;
        }
        .submit {
            background: none repeat scroll 0 0 #FF671F;
            border: medium none;
            color: #fff;
            cursor: pointer;
            float: right;
            margin: 34px 0 0;
            padding: 0 8px 2px;
            text-decoration: none;
        }
        .error{
            color: #FE0000;
            font-size:9pt;
            margin:10px;
            clear:both;
        }
        input, textarea {
            font-size: 13px;
        }
        input:hover, input:focus {
            border: 1px solid #FF671F;
        }
        a.branch {
            display: block;
        }
        div.branch-item {
            float: left;
            margin: 0 25px 40px 25px;
            text-align: center;
        }
        .branch-item .name {
            display: inline-block;
            margin-top: 10px;
        }
        .branch-item .name a {
            color: #333;
        }
        .branch-item .name a:hover {
            color: #FF671F;
        }
        .main span {
            display: block;
        }
        .main span a {
            color: #0071b9;
        }
    </style>
</head>
<body>
<ul id="main_menu">
    <li><a href="<?php echo url.'fancy_brands.php';?>" class="active">Price lists</a></li>
    <div style="height:3px;width: 100%;background: #FF671F;"></div>
</ul>
<div style="height:3px;color:#fcc900;background:#fcc900;"></div>
<div class="main" style="margin-top: 40px;">
    <?php if($storage_files) foreach ($storage_files as $file) { ?>
        <span style="padding-bottom: 5px; padding-left:10px;max-width: 500px; padding-right:10px;"><a href="<?php echo url . "storage/" . $file; ?>" target="_blank"><?php echo $file; ?></a></span>
    <?php } ?>
</div>
<div class="clear"></div>
</body>
</html>

