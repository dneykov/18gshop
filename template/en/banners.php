<script type="text/javascript">
$(document).ready(function() {
    $('#banners').cycle({ 
		    fx:     'scrollDown', 
			easing: 'easeOutBounce', 
			delay:  -2000 
	});
});
</script>
<?php
	
	require dir_root_template.'__category_list.php';
	
	echo '<div style="clear:both;"> </div>';
	
	if(($banners)&&(sizeof($banners)>3)){
		echo '<div id="banners" style="margin-top:20px;border-top:1px solid #424143;">';
			for($i=4;$i<=5;$i++){
				if(sizeof($banners)>=$i){
					if($banners[$i-1]){		
						echo '<a class="banner_item" style="margin-left:0px;"';
							if($banners[$i-1]->getDistinationURL()!='') echo ' href="'.$banners[$i-1]->getDistinationURL();
							echo '">';
							echo '<img style="width:1000px;height:300px;" src="'.url.$banners[$i-1]->getImage().'" />';						
						echo '</a>';	
					}
				}
			 }				 
		echo '</div>';	
				 
	}
?>
