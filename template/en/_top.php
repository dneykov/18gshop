<!DOCTYPE html>

<html>

<head>

	<meta charset="UTF-8">

	<meta name="format-detection" content="telephone=no">

	<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

	<link rel="stylesheet" type="text/css" href="<?php echo url_template_folder; ?>css/main.css">

	<script type="text/javascript" src="<?php echo url_template_folder; ?>js/jquery.min.js"></script>

	<script type="text/javascript" src="<?php echo url_template_folder; ?>js/jquery.watermark.js"></script>

	<script type="text/javascript" src="<?php echo url_template_folder; ?>js/script.js"></script>

	<script type="text/javascript" src="<?php echo url; ?>js/functions.js"></script>

	<?php

		if(isset($artikul)){

			echo '<link rel="image_src" href="'.url.$artikul->getKartinka_t().'" />';

		}

	?>

	<title>18gshop<?php if(isset($page_title)) echo $page_title; ?></title>

</head>

<body>

<header>

	<!--<a href="#" onclick="language_set(37);" id="lang">EN</a>-->
	<a href="#" id="lang">EN</a>

	<h1 id="logo"><a href="index.php"><img src="<?php echo url; ?>images/logo_m.png" alt="#"></a></h1>

	<div class="login_panel">

			<?php

				if($__user){

					echo ' | <a  href="login.php?logout=1">'.lang_login_logout.'</a>';

                 }

			?>

	</div>

	<h1 id="card"><a class="cart" href="fancy_login_m.php?p=cart"><img src="<?php echo url; ?>images//icons/card.png" alt="#"></a></h1>

	<span id="in_card">

			<?php				

				if(isset($_SESSION['cart'])){

					$count=0;

					foreach($_SESSION['cart'] as $key => $val){

						$count+=$_SESSION['cart'][$key]['count'];

					}

					echo $count ? $count : '';

				}else{ 

					echo '';

				}

			?>

	</span>

</header>

<div class="searchdiv">

	<input id="text" name="text" onClick="this.value='';" class="search" onkeyup="mini_search_ajax(this.value);" value=""></input>				

</div>

<div style="padding-left: 5px; padding-right: 5px;">

	<div class="autocomplate" id="suggestions" ></div>

</div>

<table id="mainmenu">

    <tr>

        <td><a <?php if((isset($_GET['products']) || isset($kategoriq)) && !isset($_GET['hotOffers'])){ echo 'class="is_active"';} ?> href="index.php?products">продукти</a></td>

        <td><a <?php if(isset($_GET['hotOffers']) || isset($_GET['hotProducts'])) echo 'class="is_active"'; ?> href="index.php?hotOffers">намаления</a></td>

		<td><a <?php if(isset($_GET['contacts'])) echo 'class="is_active"'; ?> href="index.php?contacts">контакти</a></td>

    </tr>

</table>





