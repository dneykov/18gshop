<?php
$browser = new Browser();
require_once('klasove/Mobile_Detect.php');
$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    $lok = $_SERVER['REQUEST_URI'];
    if($lok == "/") {
        header('Location: ' . urlm);
    } else {
        header('Location: ' . urlm . substr($lok, 1));
    }

}
include('template/bg/seo.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="description" content="<?php echo $pageDescription; ?>">
    <meta charset="UTF-8">
    <link rel="icon" href="<?php echo url; ?>images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo url; ?>images/favicon.ico" type="image/x-icon">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo url_template_folder; ?>css/css2.css" type="text/css" >
    <link rel="stylesheet" href="<?php echo url_template_folder; ?>css/css.css" type="text/css" >
    <link rel="stylesheet" href="<?php echo url_template_folder; ?>css/jquery.bxslider.css" type="text/css" >
    <link rel="stylesheet" href="<?php echo url; ?>css/jquery.lightbox-0.5.css" type="text/css" >
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/functions.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery.lightbox-0.5.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-ui-1.8.5.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery.cycle.all.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery.endless-pager.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/func_en.js"></script>

    <link type="text/css" href="<?php echo url; ?>css/jquery-ui-1.8.5.custom.css" rel="stylesheet" />
    <link href="<?php echo url; ?>css/jquery.thumbnailScroller.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo url; ?>css/anythingslider.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo url_template_folder; ?>js/fancyBox/jquery.fancybox.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo url_template_folder; ?>js/fancyBox/jquery.fancybox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-buttons.css?v=2.0.3" />
    <script type="text/javascript" src="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-buttons.js?v=2.0.3"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-thumbs.css?v=2.0.3" />
    <script type="text/javascript" src="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-thumbs.js?v=2.0.3"></script>


    <title>
        <?php
        $dn = false;
        if(isset($page_title) && $page_title != ""){
            if($page_title[1] == '-'){
                echo $pageTitle;
            }
            echo $page_title;
        }
        if(isset($_GET['help'])) {
            echo $pageTitle . " - INFORMATION";
            if(isset($_GET['c'])){
                $stm = $pdo->prepare("SELECT * FROM `help_groups` WHERE `id` = ? LIMIT 1");
                $stm -> bindValue(1, (int)$_GET['c'], PDO::PARAM_INT);
                $stm -> execute();
                $cat = $stm->fetch();
                echo " - ".$cat['name_en'];
            }
            if(isset($_GET['t'])){
                $stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `id` = ? LIMIT 1");
                $stm -> bindValue(1, (int)$_GET['t'], PDO::PARAM_INT);
                $stm -> execute();
                $t = $stm->fetch();
                echo " - ".$t['name_en'];
            }
            $dn = true;
        }
        else if(isset($_GET['products'])) {
            echo $pageTitle . " - products";
            $dn = true;
        }
        else if(isset($_GET['termsofuse'])) {
            echo $pageTitle . " - terms of use";
            $dn = true;
        }
        else if(isset($_GET['howtobuy'])) {
            echo " - how to buy";
            $dn = true;
        }
        else if(isset($_GET['gid'])) {
            $group = new paket((int)$_GET['gid']);
            $page_title = " - " . $group->getName();
            echo $page_title;
        }
        if(isset($page_title) && $page_title == "" && $dn == false) {
            echo $pageTitle . " - " . $websiteDescription;
        }
        ?>
    </title>
    <?php
    if(isset($artikul)){
        echo '<link rel="image_src" href="'.url.$artikul->getKartinka_t().'" />';
    }
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2221202-12"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-2221202-12');
    </script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.1/cookieconsent.min.css" />
</head>
<body class="<?php echo $browser->getBrowser(); ?>">
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1089169884433425',
            xfbml      : true,
            version    : 'v2.3'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    (function($){
        window.onload=function(){
            $(".jThumbnailScroller").thumbnailScroller({
                scrollerType:"clickButtons",
                scrollerOrientation:"horizontal",
                scrollSpeed:2,
                scrollEasing:"easeOutCirc",
                scrollEasingAmount:600,
                acceleration:4,
                scrollSpeed:800,
                noScrollCenterSpace:10,
                autoScrolling:0,
                autoScrollingSpeed:2000,
                autoScrollingEasing:"easeInOutQuad",
                autoScrollingDelay:500
            });
        }
    })(jQuery);
</script>
<script src="<?php echo url; ?>js/jquery.thumbnailScroller.js"></script>
<script type="text/javascript">
    $(document).bind('click', function(e) {
        var $clicked = $(e.target);

        if (!$clicked.parents().hasClass("searchdiv"))
            search_ajax_close('#suggestions');
    });
</script>
<?php
if(isset($_SESSION['displayMassage'])){
    ?>
    <div id="statusbar_info" style="display: none;position: absolute;left: 50%;margin-top: 350px;color: #000; background-color: #FFF; border: 1px dotted black; padding: 10px;
             "><?php echo $_SESSION['displayMassage']; ?><a href="#" style="color: red;" onclick="$('#statusbar_info').slideUp();">x</a></div>
    <script type="text/javascript">
        $('#statusbar_info').slideDown();
    </script>

    <?php
    unset($_SESSION['displayMassage']);
}
?>
<div class="wrapper">
    <div class="header-wrapper">
        <div class="header">
            <div class="lang-container">
                <a class="change-lang" href="javascript:void(0)" onclick="language_set(32);">BG</a>
                <a class="change-lang" href="javascript:void(0)" onclick="language_set(38);">RO</a>
            </div>
            <a href="<?php echo url;?>" class="logo" ><img src="../../images/logo/Logo.png" alt="<?php echo $pageTitle; ?>" /></a>
            <div class="header-right">
                <div class="top_panel">
                    <div class="searchdiv">
                        <input autocomplete="off" id="text" name="text" onClick="this.value='';" class="search" onkeyup="search_ajax(this.value);" value="">
                        <div><div class="autocomplate" id="suggestions" ></div></div>
                    </div>
                    <?php
                    if($__user) {
                        echo '<span style="color: #FF671F;">'.$__user->getName().'</span>';
                        echo ' | <a href="'.url.'index.php?user" style="color: #FF671F;">account</a>';
                        echo ' | <a  href="login.php?logout=1" style="color: #FF671F;">'.lang_login_logout.'</a> | ';
                    } else {
                        echo '<a href="fancy_login.php?p=login" class="hreg" title="вход">login</a> | ';
                        echo '<a href="fancy_login.php?p=registation" class="hreg" title="регистрация">registration</a> | ';
                    }
                    ?>
                    <a href="<?php echo url."index.php?help"; ?>" style="<?php if('http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'] == url."index.php?help") echo "color: #fd559";?>" title="информация">information</a> |
                    <a href="<?php echo url."index.php?contacts"; ?>" title="contacts">contacts</a>

                    <?php if($__user && $__user->is_partner()) : ?>
                        | <a href="pricelist.php" class="pricelist" title="ценови листи">price lists</a>
                    <?php endif; ?>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.login_panel a.aloggin').fancybox({
                                'type'			: 'iframe',
                                'padding'		: 0,
                                'width'			: 800,
                                'height'		: 600
                            });
                            $('.top_panel a.hreg').fancybox({
                                'type'			: 'iframe',
                                'padding'		: 0,
                                'width'			: 800,
                                'height'		: 790
                            });
                            $('#main_menu a.brands-popup').fancybox({
                                'type'			: 'iframe',
                                'padding'		: 0,
                                'width'			: 800,
                                'height'		: 600
                            });
                            $('.help a').fancybox({
                                'type'          : 'iframe',
                                'padding'       : 0,
                                'width'         : 350,
                                'height'        : 530
                            });
                            $('.pricelist').fancybox({
                                'type'          : 'iframe',
                                'padding'       : 0,
                                'width'         : 480,
                                'height'        : 500
                            });

                        });
                    </script>
                </div>
                <div class="help">
                    <a href="<?php echo url.'fancy_qbs.php'.'?p=sendquestion'; ?>"><img src="images/icons/help.png" alt="help"/></a>
                </div>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
    <?php
    include 'header_menu.php';
    ?>
    <center>
        <div class="main">
