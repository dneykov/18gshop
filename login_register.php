<?php
require_once 'main.php';


try{

    $user_register_error = NULL;
    $order_address_data_error = NULL;

    if (isset($_POST['submit'], $_POST['register_mail'], $_POST['register_pass'], $_POST['register_name'], $_POST['register_name_last'], $_POST['city'], $_POST['street'], $_POST['gsm'], $_POST['number']) ) {
        if ($_POST['account_type'] == '0') {
            $order_addr_gsm = $_POST['gsm'];
            $order_addr_city = $_POST['city'];
            $order_addr_street = $_POST['street'];
            $order_addr_number = $_POST['number'];
            $order_addr_vhod = $_POST['vhod'];
            $order_addr_etaj = $_POST['etaj'];
            $order_addr_apartament = $_POST['apartament'];

            $username_reg_name = $_POST['register_name'];
            $username_reg_name_last = $_POST['register_name_last'];

            $username_reg_mail = $_POST['register_mail'];
            $username_reg_mail = trim($username_reg_mail);


            if (mb_strlen($order_addr_gsm) < 9) $order_address_data_error[] = lang_order_address_data_error_gsm;
            if (mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

            if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
            if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

            if (mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
            if (mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

            if (mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
            if (mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;
            if (mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;

            if (mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;

            if (mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;


            if (isset($_POST['newsletter'])) {
                $newsletter = 1;
            } else {
                $newsletter = 0;
            }

            if ((!isset($_POST['terms']))) $user_register_error[] = lang_login_register_error_terms;

            if ((!isset($_POST['privacy']))) $user_register_error[] = lang_login_register_error_privacy;

            if (mb_strlen($username_reg_name) < 2) $user_register_error[] = lang_login_register_error_name_first_lenght_short;
            if (mb_strlen($username_reg_name) > 200) $user_register_error[] = lang_login_register_error_name_first_lenght_long;

            if (mb_strlen($username_reg_name_last) < 2) $user_register_error[] = lang_login_register_error_name_last_lenght_short;
            if (mb_strlen($username_reg_name_last) > 200) $user_register_error[] = lang_login_register_error_name_last_lenght_long;


            if (mb_strlen($username_reg_mail) > 200) $user_register_error[] = lang_login_register_error_mail_lenght_long;


            $username_reg_pass = $_POST['register_pass'];
            if (mb_strlen($username_reg_pass) < 4) $user_register_error[] = lang_login_register_error_pass_lenght_short;
            if (mb_strlen($username_reg_pass) > 200) $user_register_error[] = lang_login_register_error_pass_lenght_long;

            if (!filter_var($username_reg_mail, FILTER_VALIDATE_EMAIL)) $user_register_error[] = lang_login_register_error_mail_invalid;


            $stm_check_mail = $pdo->prepare("SELECT `mail` FROM `members` WHERE LOWER(`mail`) = LOWER(?) LIMIT 1");
            $stm_check_mail->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
            $stm_check_mail->execute();

            //ако името вече го има показва грешка
            if ($stm_check_mail->rowCount() > 0) {
                $user_register_error[] = str_replace("mail@mail.com", $username_reg_mail, lang_login_register_error_mail_already_used);
            }

            $username_reg_pass = sha1($username_reg_pass);
            if (isset($_POST['newsletter'])) {
                $newsletter = 1;
                $stm_select = $pdo->prepare("SELECT * FROM `newsletter_members` WHERE `email` = ?");
                $stm_select->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
                $stm_select->execute();
                if ($stm_select->rowCount() == 0) {
                    $insert = $pdo->prepare("INSERT INTO `newsletter_members`(`email`, `created_at`) VALUES (?,?)");
                    $insert->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
                    $insert->bindValue(2, time(), PDO::PARAM_INT);
                    $insert->execute();
                }
            } else {
                $newsletter = 0;
            }
            if ($user_register_error === NULL && $order_address_data_error === NULL) {


                $pdo->beginTransaction();

                // сега въвеждаме данните в базата данни
                $stm = $pdo->prepare("INSERT INTO `members` (`mail`, `password`, `name`, `name_last`,`subscribe`) VALUES (?, ?, ?, ?, ?)");
                $stm->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
                $stm->bindValue(2, $username_reg_pass, PDO::PARAM_STR);
                $stm->bindValue(3, $username_reg_name);
                $stm->bindValue(4, $username_reg_name_last);
                $stm->bindValue(5, $newsletter);
                $stm->execute();

                $member_id = $pdo->lastInsertId();

                $member_code = sha1('G&*IO' . uniqid() . 'yjml');


                $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=?, `country`=? WHERE id=?");
                $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
                $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
                $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
                $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
                $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
                $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
                $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
                $stm->bindValue(8, $_POST['country'], PDO::PARAM_STR);
                $stm->bindValue(9, $member_id, PDO::PARAM_INT);

                $stm->execute();
                $pdo->commit();


                $login_pass_cookie = sha1($username_reg_pass . uniqid());

                $stm = $pdo->prepare('UPDATE `members` SET `password_coockie` = ?, `ip` = ? WHERE `id` = ? LIMIT 1');
                $stm->bindValue(1, $login_pass_cookie, PDO::PARAM_STR);
                $stm->bindValue(2, $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stm->bindvalue(3, $member_id, PDO::PARAM_INT);
                $stm->execute();

                setcookie('username_id', $member_id);
                setcookie('username_pass', $login_pass_cookie);

                if (isset($_SESSION['cart']) && sizeof($_SESSION['cart']) > 0) {
                    header("Location: " . url . "fancy_login.php?p=ordrdetails");
                } else {
                    echo '<script>parent.location=parent.location;</script>';
                }
                ?>

                <?php
                die();
            }
        } else if($_POST['account_type'] == '1') {
            $order_addr_gsm = $_POST['gsm'];
            $order_addr_city = $_POST['city'];
            $order_addr_street = $_POST['street'];
            $order_addr_number = $_POST['number'];
            $order_addr_vhod = $_POST['vhod'];
            $order_addr_etaj = $_POST['etaj'];
            $order_addr_apartament = $_POST['apartament'];

            $company_name = $_POST['company_name'];
            $company_city = $_POST['company_city'];
            $company_street = $_POST['company_street'];
            $company_number = $_POST['company_number'];
            $company_mol = $_POST['company_mol'];
            $company_eik = $_POST['company_eik'];
            $company_vat = $_POST['company_vat'];
            $company_phone = $_POST['company_phone'];

            $username_reg_name = $_POST['register_name'];
            $username_reg_name_last = $_POST['register_name_last'];

            $username_reg_mail = $_POST['register_mail'];
            $username_reg_mail = trim($username_reg_mail);


            if (mb_strlen($order_addr_gsm) < 9) $order_address_data_error[] = lang_order_address_data_error_gsm;
            if (mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

            if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
            if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

            if (mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
            if (mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

            if (mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
            if (mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;

            if (mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;

            if (mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;

            if (mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;

            if (isset($_POST['newsletter'])) {
                $newsletter = 1;
            } else {
                $newsletter = 0;
            }

            if ((!isset($_POST['terms']))) $user_register_error[] = lang_login_register_error_terms;

            if ((!isset($_POST['privacy']))) $user_register_error[] = lang_login_register_error_privacy;

            if (mb_strlen($username_reg_name) < 2) $user_register_error[] = lang_login_register_error_name_first_lenght_short;
            if (mb_strlen($username_reg_name) > 200) $user_register_error[] = lang_login_register_error_name_first_lenght_long;

            if (mb_strlen($username_reg_name_last) < 2) $user_register_error[] = lang_login_register_error_name_last_lenght_short;
            if (mb_strlen($username_reg_name_last) > 200) $user_register_error[] = lang_login_register_error_name_last_lenght_long;


            if (mb_strlen($username_reg_mail) > 200) $user_register_error[] = lang_login_register_error_mail_lenght_long;


            $username_reg_pass = $_POST['register_pass'];
            if (mb_strlen($username_reg_pass) < 4) $user_register_error[] = lang_login_register_error_pass_lenght_short;
            if (mb_strlen($username_reg_pass) > 200) $user_register_error[] = lang_login_register_error_pass_lenght_long;

            if (!filter_var($username_reg_mail, FILTER_VALIDATE_EMAIL)) $user_register_error[] = lang_login_register_error_mail_invalid;

            $stm_check_mail = $pdo->prepare("SELECT `mail` FROM `members` WHERE LOWER(`mail`) = LOWER(?) LIMIT 1");
            $stm_check_mail->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
            $stm_check_mail->execute();

            //ако името вече го има показва грешка
            if ($stm_check_mail->rowCount() > 0) {
                $user_register_error[] = str_replace("mail@mail.com", $username_reg_mail, lang_login_register_error_mail_already_used);
            }

            if (mb_strlen($company_name) < 3) $order_address_data_error[] = lang_order_company_name_short;
            if (mb_strlen($company_name) > 200) $order_address_data_error[] = lang_order_company_name_long;

            if (mb_strlen($company_city) < 2) $order_address_data_error[] = lang_order_company_city_error;
            if (mb_strlen($company_city) > 200) $order_address_data_error[] = lang_order_company_city_error;

            if (mb_strlen($company_street) < 2) $order_address_data_error[] = lang_order_company_street_error;
            if (mb_strlen($company_street) > 200) $order_address_data_error[] = lang_order_company_street_error;

            if (mb_strlen($company_mol) < 2) $order_address_data_error[] = lang_order_company_mol_error;
            if (mb_strlen($company_mol) > 200) $order_address_data_error[] = lang_order_company_mol_error;

            if (mb_strlen($company_eik) < 2 && mb_strlen($company_vat) < 2) $order_address_data_error[] = lang_order_company_eik_vat_error;
            if (mb_strlen($company_eik) > 200 && mb_strlen($company_vat) > 200) $order_address_data_error[] = lang_order_company_eik_vat_error;

            $username_reg_pass = sha1($username_reg_pass);
            if (isset($_POST['newsletter'])) {
                $newsletter = 1;
                $stm_select = $pdo->prepare("SELECT * FROM `newsletter_members` WHERE `email` = ?");
                $stm_select->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
                $stm_select->execute();
                if ($stm_select->rowCount() == 0) {
                    $insert = $pdo->prepare("INSERT INTO `newsletter_members`(`email`, `created_at`) VALUES (?,?)");
                    $insert->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
                    $insert->bindValue(2, time(), PDO::PARAM_INT);
                    $insert->execute();
                }
            } else {
                $newsletter = 0;
            }
            if ($user_register_error === NULL && $order_address_data_error === NULL) {


                $pdo->beginTransaction();

                $stm = $pdo->prepare("INSERT INTO `members` (`mail`, `password`, `name`, `name_last`,`subscribe`) VALUES (?, ?, ?, ?, ?)");
                $stm->bindValue(1, $username_reg_mail, PDO::PARAM_STR);
                $stm->bindValue(2, $username_reg_pass, PDO::PARAM_STR);
                $stm->bindValue(3, $username_reg_name);
                $stm->bindValue(4, $username_reg_name_last);
                $stm->bindValue(5, $newsletter);
                $stm->execute();

                $member_id = $pdo->lastInsertId();

                $member_code = sha1('G&*IO' . uniqid() . 'yjml');


                $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=?,`country`=?,`company_name`=?,`company_city`=?,`company_street`=?,`company_number`=?,`company_mol`=?,`company_eik`=?,`company_vat`=?,`company_phone`=?,`account_type`=? WHERE id=?");
                $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
                $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
                $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
                $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
                $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
                $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
                $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
                $stm->bindValue(8, $_POST['country'], PDO::PARAM_STR);
                $stm->bindValue(9, $company_name, PDO::PARAM_STR);
                $stm->bindValue(10, $company_city, PDO::PARAM_STR);
                $stm->bindValue(11, $company_street, PDO::PARAM_STR);
                $stm->bindValue(12, $company_number, PDO::PARAM_STR);
                $stm->bindValue(13, $company_mol, PDO::PARAM_STR);
                $stm->bindValue(14, $company_eik, PDO::PARAM_STR);
                $stm->bindValue(15, $company_vat, PDO::PARAM_STR);
                $stm->bindValue(16, $company_phone, PDO::PARAM_STR);
                $stm->bindValue(17, $_POST['account_type'], PDO::PARAM_STR);
                $stm->bindValue(18, $member_id, PDO::PARAM_INT);

                $stm->execute();

                $pdo->commit();

                $login_pass_cookie = sha1($username_reg_pass . uniqid());

                $stm = $pdo->prepare('UPDATE `members` SET `password_coockie` = ?, `ip` = ? WHERE `id` = ? LIMIT 1');
                $stm->bindValue(1, $login_pass_cookie, PDO::PARAM_STR);
                $stm->bindValue(2, $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stm->bindvalue(3, $member_id, PDO::PARAM_INT);
                $stm->execute();

                setcookie('username_id', $member_id);
                setcookie('username_pass', $login_pass_cookie);

                if (isset($_SESSION['cart']) && sizeof($_SESSION['cart']) > 0) {
                    header("Location: " . url . "fancy_login.php?p=ordrdetails");
                } else {
                    echo '<script>parent.location=parent.location;</script>';
                }
                ?>

                <?php
                die();
            }
        }
    }
} catch (Exception $e){
    greshka($e);
}

 




$__navigation_link=Null;
// template

	//require dir_root_template.'order_details.php';

?>
