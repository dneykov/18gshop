<?php

require '__top.php';

$tmp_promp = false;
if (isset($_GET['hotOffers'])) {
    $tmp_promp = true;
}

$__navigation_link[] = array('url' => url . "index.php", 'text' => '18gshop');
$broi_artikuli_na_stranica = 40;

if (isset ($_GET['page'])) $page = (int)$_GET['page'];
else $page = 0;

/* @var $pdo pdo */
/* @var $stm PDOStatement */


$modeli = NULL;


if (isset ($_GET['search'])) {

    require 'index_search.php';


} ################### search


if (isset ($_GET['id']) && isProductAvailable($_GET['id'])) {

    require 'index_id.php';


    /*$__navigation_link[] = array('url' => po_taka_url_append($url, 'category='.$kategoriq->getId()), 'text' => $kategoriq->getIme());


    $__navigation_link[] = array('url' => po_taka_url_append($url, 'mode='.$vid->getId()), 'text' => $vid->getIme());


    $__navigation_link[] = array('url' => po_taka_url_append($url, 'model='.$model->getId()), 'text' => $model->getIme());
    */
} ############################## ako e po ID


$url = url;
//po_taka_url_append($url, 'products.php', false);

if (!isset($kategoriq)) {
    if (isset ($_GET['category'])) {
        $kategoriq = new kategoriq((int)$_GET['category']);
        $__navigation_link[] = array('url' => po_taka_url_append($url, 'category=' . $kategoriq->getId()), 'text' => $kategoriq->getIme());
    } else $kategoriq = NULL;
}

if (!isset($vid)) {
    if (isset ($_GET['mode'])) {
        $vid = new vid((int)$_GET['mode']);
        $__navigation_link[] = array('url' => po_taka_url_append($url, 'mode=' . $vid->getId()), 'text' => $vid->getIme());

    } else $vid = NULL;
}
if (!isset($model)) {
    if (isset ($_GET['model'])) {
        $model = new model((int)$_GET['model']);
        $__navigation_link[] = array('url' => po_taka_url_append($url, 'model=' . $model->getId()), 'text' => $model->getIme());
    } else $model = NULL;
}

if (isset ($_GET['tag']) && is_array($_GET['tag'])) {
    $etiket = $_GET['tag'];

    foreach ($etiket as $e) {
        $temp = new etiket((int)$e);
        $__navigation_link[] = array('url' => po_taka_url_append($url, 'tag[]=' . $temp->getId()), 'text' => $temp->getIme());


    }


} else $etiket = NULL;

$branch = NULL;
if (isset ($_GET['branch'])) {
    $branch = $_GET['branch'];
    $__navigation_link[] = array('url' => po_taka_url_append($url, 'branch=' . $branch), 'text' => $branch);
    $sql = 'SELECT * FROM `model` WHERE ( `ime` like :marka  ) ORDER BY `id`;';
    $stm = $pdo->prepare($sql);
    $stm->bindValue(':marka', $branch, PDO::PARAM_STR);
    $stm->execute();

    if ($stm->rowCount() == 0) {
        $branch_image = NULL;
    } else {
        foreach ($stm->fetchAll() as $v) {
            if (file_exists($v['image'])) {
                $branch_image = $v['image'];
            }
            break;
        }
    }
}

if (isset ($_GET['hotOffers'])) {
    $__navigation_link[] = array('url' => po_taka_url_append($url, 'hotOffers'), 'text' => 'Промоции');
}

if ($kategoriq) {
    $kategorii = NULL;
    $sql = 'SELECT * FROM `vid` WHERE `id_kategoriq` = :id ';

    $sql .= ' ORDER BY `ordr` ';
    $stm = $pdo->prepare($sql);
    $stm->bindValue(':id', $kategoriq->getId(), PDO::PARAM_INT);

    $stm->execute();

    $vidove = NULL;
    foreach ($stm->fetchAll() as $v) {

        $temp_vid = new vid($v);

        if (isset ($branch)) {
            $tmp_broi = $temp_vid->getBroi_artikuli($branch, $tmp_promp);
        } else {
            $tmp_broi = $temp_vid->getBroi_artikuli(null, $tmp_promp);
        }

        if ($tmp_broi == 0) continue;

        $vidove[] = $temp_vid;
    }


    if (isset($vid) && $vid->isVirtual()) {
        $sql = 'SELECT * FROM `model`';
    } else {
        $sql = 'SELECT * FROM `model` WHERE (`id_kategoriq` = :id ';
    }

    if (isset ($branch)) {
        if (isset($vid) && $vid->isVirtual()) {
            $sql .= ' WHERE `ime` like :marka ';
        } else {
            $sql .= ' AND ( `ime` like :marka  ) ';
        }

    }

    if (isset($vid) && $vid->isVirtual()) {
        $sql .= ' ORDER BY `ime` ';
    } else {
        $sql .= ' ) ORDER BY `ime` ';
    }

    $stm = $pdo->prepare($sql);

    $stm->bindValue(':id', $kategoriq->getId(), PDO::PARAM_INT);
    if (isset($vid) && !$vid->isVirtual()) {
        $stm->bindValue(':id', $kategoriq->getId(), PDO::PARAM_INT);
    }
    if (isset ($branch)) {
        $stm->bindValue(':marka', $branch, PDO::PARAM_STR);
    }
    $stm->execute();


    if ($stm->rowCount() == 0) $modeli = NULL;

    else {
        foreach ($stm->fetchAll() as $v) {

            if ($vid) $temp_model = new model($v, $vid->getId());
            else $temp_model = new model($v);

            if ($temp_model->getBroi_artikuli($tmp_promp) == 0) continue;

            $modeli[] = $temp_model;

            unset($temp_model);
        }
        unset($v);
    }


    $etiketi_grupi = NULL;
    if ($vid) {
        $etiket_izpolzvani = NULL;

        if ($vid->isVirtual() && $vid->vidoveFromSameCategory()) {
            $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` IN (?) ORDER BY `ime_' . lang_prefix . '`');
            $stm->bindValue(1, implode(",", $vid->getChildVidove()), PDO::PARAM_STR);
        } else {
            $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ORDER BY `ime_' . lang_prefix . '`');
            $stm->bindValue(1, $vid->getId(), PDO::PARAM_INT);
        }
        $stm->execute();

        if ($stm->rowCount() > 0) {
            foreach ($stm->fetchAll() as $v) {
                if ($model) $temp_etiket_model_id = $model->getId();
                else $temp_etiket_model_id = 0;

                $temp_taggrupa = new etiket_grupa($v, $temp_etiket_model_id, $etiket_izpolzvani, true);

                if ($temp_taggrupa->getEtiketi(null, $tmp_promp) == NULL) continue;

                $etiketi_grupi[] = $temp_taggrupa;

                if ($etiket && $temp_taggrupa->getEtiketi(null, $tmp_promp)) {
                    foreach ($temp_taggrupa->getEtiketi(null, $tmp_promp) as $temp_etiket_zaeti) {
                        if (in_array($temp_etiket_zaeti->getId(), $etiket)) {
                            $etiket_izpolzvani[] = $temp_etiket_zaeti->getId();
                            break;
                        }
                    }

                }

                unset($temp_etiket_model_id, $temp_taggrupa);
            }
        } else {
            $etiketi_grupi = NULL;
        }
    } else {
        $etiketi_grupi = NULL;
    }
} // if kategoriq


$kategorii = NULL;
$stm = $pdo->prepare('SELECT * FROM `kategorii` ORDER BY `ordr` ');
$stm->execute();

if ($stm->rowCount()) foreach ($stm->fetchAll() as $k_fe) {
    $temp = new kategoriq($k_fe);
    if (isset ($branch)) {
        $tmp_broi = $temp->getBroi_artikuli($branch, $tmp_promp);
    } else {
        $tmp_broi = $temp->getBroi_artikuli(null, $tmp_promp);
    }
    if ($tmp_broi > 0)
        $kategorii[] = new kategoriq($k_fe);
    unset($temp);
}


//load all kategorii

$all_kategorii = NULL;
$stm = $pdo->prepare('SELECT * FROM `kategorii` ORDER BY `ordr` ');
$stm->execute();

if ($stm->rowCount()) foreach ($stm->fetchAll() as $k_fe) {
    $temp = new kategoriq($k_fe);
    $tmp_broi = $temp->getBroi_artikuli();
    if ($tmp_broi > 0)
        $all_kategorii[] = new kategoriq($k_fe);
    unset($temp);
}


//end load all kategorii


if (isset ($artikul)) {


}
if (isset($_GET['filter']) && $_GET['filter'] == 'last') {
    $filter = ' `creat_date` DESC ';
} else if (isset($_GET['filter']) && $_GET['filter'] == 'low-price') {
    $filter = ' IF(cena_promo>0,cena_promo,cena) ASC ';
} else if (isset($_GET['filter']) && $_GET['filter'] == 'high-price') {
    $filter = ' IF(cena_promo>0,cena_promo,cena) DESC ';
} else {
    $filter = ' `creat_date` DESC ';
}

$sql = 'SELECT * FROM `artikuli` WHERE (isAvaliable=1 AND IsOnline=1 ';

if ($kategoriq) {
    if ($vid) {
        if (!$vid->isVirtual()) {
            $sql .= 'AND `id_kategoriq` = :cat ';
        }
    } else {
        $sql .= 'AND `id_kategoriq` = :cat ';
    }
}

if (isset($_GET['s']) && strlen($_GET['s']) >= 3) {
    $search = preg_replace('/[  |\t]/', ' ', $_GET['s']);
    $search = explode(' ', $search);
    $where = '';

    foreach ($search as $k => $v) {
        $sql .= ' AND LOWER(`cache_etiketi`) LIKE(LOWER( :text' . $k . ' )) ';
    }
}

if ($vid) {
    if ($vid->isVirtual()) {
        $sql .= 'AND `id` IN (SELECT `artikul_id` FROM `product_vid` WHERE `vid_id` = :vid)';
    } else {
        $sql .= ' AND `id_vid` = :vid ';
    }
}

if (isset ($_GET['hotOffers'])) {
    $sql .= ' AND `cena_promo`>0 ';
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------

if (isset ($_GET['branch'])) {
    $sql .= ' AND `id_model` IN (Select id From `model` where ( `ime` like :marka  )) ';

}


if ($model) $sql .= ' AND `id_model` = :model ';

//var_dump($etiket);

if ($etiket) foreach ($etiket as $e_k => $e_v) {
    $sql .= ' AND `cache_etiketi` LIKE (:etiket' . $e_k . ') ';
}


$sql_broi = $sql . ' ) ';
$sql_broi = preg_replace('|SELECT \*|', 'SELECT COUNT(`id`) AS `broi`', $sql_broi);


$sql .= ') ORDER BY ' . $filter . ' LIMIT :page , ' . $broi_artikuli_na_stranica;

$stm = $pdo->prepare($sql);

$stm_broi = $pdo->prepare($sql_broi);

if ($kategoriq) {
    if ($vid) {
        if (!$vid->isVirtual()) {
            $stm->bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
            $stm_broi->bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
        }
    } else {
        $stm->bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
        $stm_broi->bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
    }
}

if (isset($_GET['s']) && strlen($_GET['s']) >= 3) {
    $search = preg_replace('/[  |\t]/', ' ', $_GET['s']);
    $search = explode(' ', $search);

    foreach ($search as $k => $v) {
        $stm->bindValue(':text' . $k, '%' . $v . '%');
        $stm_broi->bindValue(':text' . $k, '%' . $v . '%');
    }
}

if ($vid) {
    $stm->bindValue(':vid', $vid->getId(), PDO::PARAM_INT);
    $stm_broi->bindValue(':vid', $vid->getId(), PDO::PARAM_INT);
}
//---------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------


if ($model) {
    $stm_broi->bindValue(':model', $model->getId(), PDO::PARAM_INT);
    $stm->bindValue(':model', $model->getId(), PDO::PARAM_INT);
}


if ($etiket) {
    foreach ($etiket as $e_k => $e_v) {
        $stm->bindValue(':etiket' . $e_k, '%<etiket>' . $e_v . '</etiket>%', PDO::PARAM_STR);
        $stm_broi->bindValue(':etiket' . $e_k, '%<etiket>' . $e_v . '</etiket>%', PDO::PARAM_STR);
    }

}

if (isset ($_GET['branch'])) {
    $stm_broi->bindValue(':marka', $_GET['branch'], PDO::PARAM_STR);
    $stm->bindValue(':marka', $_GET['branch'], PDO::PARAM_STR);
}

$stm->bindValue(':page', $page * $broi_artikuli_na_stranica, PDO::PARAM_INT);


$stm_broi->execute();
$artikuli_broi = $stm_broi->fetch();

$artikul_broi = $artikuli_broi[0];

unset($stm_broi);

$stm->execute();

$artikuli = NULL;
if (isset($artikul)) {
    $artikuli_idta = $artikul->getId() . ', ';
} else {
    $artikuli_idta = NULL;
}
if ($stm->rowCount()) foreach ($stm->fetchAll() as $v) {
    $temp = new artikul($v);
    if ($temp->getEtiketizapisi()) {
        foreach ($temp->getEtiketizapisi() as $t) {
            $t->getEtiket()->getIme_etiket_grupa();
        }
    }
    $artikuli[] = $temp;
    $artikuli_idta = $artikuli_idta . $temp->getId() . ', ';
    unset($temp);
}
$artikuli_idta = trim($artikuli_idta, ', ');

$min_prod_per_page = 32;
$last_artikuli = NULL;

if ($artikul_broi < $min_prod_per_page) {
    $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE isAvaliable = 1 AND IsOnline=1 ORDER BY `id` DESC LIMIT 0 , ' . ($min_prod_per_page - $artikul_broi) . ' ;');
    $stm->execute();

    if ($stm->rowCount()) foreach ($stm->fetchAll() as $v) {
        $temp = new artikul($v);
        if ($temp->getEtiketizapisi()) {
            foreach ($temp->getEtiketizapisi() as $t) {
                $t->getEtiket()->getIme_etiket_grupa();
            }
        }
        $last_artikuli[] = $temp;
        unset($temp);
    }
}


//Load advertises
$advertises = NULL;
$stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=1 order by `id` DESC');
$stm->execute();

foreach ($stm->fetchAll() as $adv) {
    $temp_advertise = new advertise((int)$adv['id']);
    $banners[] = $temp_advertise;
    unset($temp_advertise);
    unset($adv);
}

$stm = $pdo->prepare('SELECT * FROM `model` GROUP by ime ORDER BY `ime` ');
$stm->execute();

if ($stm->rowCount() == 0) {
    $branches = NULL;
} else {
    foreach ($stm->fetchAll() as $v) {
        $branches[] = $v;
    }
    unset($v);
}

function getArticuli($art_kategoriq, $art_vid, $art_model, $broi, $disable_ids, $art_etiketi = null, $modelType = 'id', $promo = false)
{
    global $pdo;

    $filter_title = '';
    if (isset($art_kategoriq)) {
        $filter_title .= ' ' . mb_strtolower($art_kategoriq->getIme(), 'UTF-8');
    }
    if (isset($art_vid)) {
        $filter_title .= ' ' . mb_strtolower($art_vid->getIme(), 'UTF-8');
    }
    if (isset($art_model)) {
        $filter_title .= ' ' . mb_strtolower($art_model->getIme(), 'UTF-8');
    }
    if (isset($art_etiketi)) {


        $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ORDER BY `id` ');
        $stm->bindValue(1, $art_vid->getId(), PDO::PARAM_INT);
        $stm->execute();

        if ($stm->rowCount() == 0) $all_etiketi_grupi = NULL;
        else foreach ($stm->fetchAll() as $v) {
            if ($art_model) $temp_etiket_model_id = $art_model->getId();
            else $temp_etiket_model_id = 0;

            $temp_taggrupa = new etiket_grupa($v, $temp_etiket_model_id);


            $all_etiketi_grupi[] = $temp_taggrupa;
        }

        if (isset($all_etiketi_grupi)) {
            foreach ($all_etiketi_grupi as $v) {
                if (($v->getEtiketi())) {
                    foreach ($v->getEtiketi() as $e) {
                        if (in_array($e->getId(), $art_etiketi)) {
                            $filter_title .= ' ' . mb_strtolower($v->getIme(), 'UTF-8') . ' ' . mb_strtolower($e->getIme(), 'UTF-8');
                        }
                    }
                }
            }
        }
    }

    $sql = 'SELECT * FROM `artikuli` WHERE ( isAvaliable = 1 AND IsOnline=1 ';
    if (isset($disable_ids)) $sql .= 'AND `id` NOT IN ( ' . $disable_ids . ' ) ';
    if ($art_kategoriq) $sql .= 'AND `id_kategoriq` = :cat ';
    if ($art_vid) $sql .= ' AND `id_vid` = :vid ';
    if ($art_model) {
        if ($modelType == 'id') {
            $sql .= ' AND `id_model` = :model ';
        } else {
            $sql .= ' AND (`id_model` in ( Select `id` FROM `model` WHERE `ime` LIKE :model ))';
        }

    }
    if ($promo) {
        $sql .= ' AND `cena_promo`>0 ';
    }
    if (isset($art_etiketi) && is_array($art_etiketi) && (sizeof($art_etiketi) > 0)) {
        foreach ($art_etiketi as $e_k => $e_v) {
            $sql .= ' AND `cache_etiketi` LIKE (:etiket' . $e_k . ') ';
        }
    }
    $sql .= ') ORDER BY RAND() DESC LIMIT ' . $broi;

    $stm = $pdo->prepare($sql);

    if (isset($art_etiketi) && is_array($art_etiketi) && (sizeof($art_etiketi) > 0)) {
        foreach ($art_etiketi as $e_k => $e_v) {
            $stm->bindValue(':etiket' . $e_k, '%<etiket>' . $e_v . '</etiket>%', PDO::PARAM_STR);
        }
    }


    if ($art_kategoriq) $stm->bindValue(':cat', $art_kategoriq->getId(), PDO::PARAM_INT);
    if ($art_vid) $stm->bindValue(':vid', $art_vid->getId(), PDO::PARAM_INT);

    if ($art_model) {
        if (is_int($art_model)) {
            if ($art_model) $stm->bindValue(':model', $art_model->getId(), PDO::PARAM_INT);
        } else {
            if ($art_model) $stm->bindValue(':model', $art_model->getIme(), PDO::PARAM_STR);
        }

    }
    $stm->execute();

    $tmp_artikuli_broi = $stm->rowCount();
    $art['title'] = $filter_title;
    $art['count'] = $tmp_artikuli_broi;
    $art['itams_id'] = '';
    if (isset($tmp_artikuli_broi) && ($tmp_artikuli_broi > 0)) {
        foreach ($stm->fetchAll() as $v) {
            $temp = new artikul($v);
            $art['itams'][] = $temp;
            $art['itams_id'] = $art['itams_id'] . $temp->getId() . ', ';
            unset($temp);
        }
        $art['itams_id'] = trim($art['itams_id'], ', ');
    }
    return $art;
}

//------------load telefon za vryzka----------
$stm = $pdo->prepare('SELECT text_' . lang_prefix . ' FROM `zz_about` where `id`=1');
$stm->execute();
$phone_for_contact = $stm->fetchall();
$phone_for_contact = $phone_for_contact[0][0];
//------------end load telefon za vryzka----------

//------------Load Day Offers----------
if (isset($kategoriq)) {
    $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE (IsOnline=1 AND `id_kategoriq` = :cat and ADDTIME(`DayOffer` , "0 23:59:59.999999") > CURRENT_TIMESTAMP) ORDER BY RAND()');
    $stm->bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
    $stm->execute();
    $dayOfferProducts = Null;
    foreach ($stm->fetchAll() as $v) {
        $dayOfferProducts[] = new artikul($v);
    }
}

//------------ Promo products count ----------
$promoProductsCount = 0;

//------------ End Promo products count ----------
$stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND isAvaliable=1 AND IsOnline=1');
$stm->execute();
$promoProductsCount = $stm->rowCount();
//------------End Load Day Offers----------


$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "deliveryCost"');
$stm->execute();
$deliveryCost = $stm->fetch();
$deliveryCost = floatval($deliveryCost['value']);


$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "freeDelivery"');
$stm->execute();
$freeDelivery = $stm->fetch();
$freeDelivery = floatval($freeDelivery['value']);

/**
 * Getting all bundle products
 */
if (isset($_GET['bundles'])) {
    $sql = 'SELECT * FROM `group_products`';

    if (isset($kategoriq)) {
        $sql .= ' WHERE `group_products`.`online` = 1 AND `group_products`.`id` IN (SELECT DISTINCT `group_id` FROM `group_products_zapisi` WHERE `cat_id` = :cat_id)';
    } else {
        $sql .= ' WHERE `group_products`.`online` = 1';
    }

    $sql .= ' ORDER BY `id` DESC';

    $stm = $pdo->prepare($sql);
    if (isset($kategoriq)) $stm->bindValue(':cat_id', $kategoriq->getId(), PDO::PARAM_INT);

    $stm->execute();
    $bundleProducts = Null;
    foreach ($stm->fetchAll() as $v) {
        $bundleProducts[] = new paket($v);
    }

}
