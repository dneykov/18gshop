<?php
	require 'main.php';
	
	$page_title="";
	if (isset($artikul)){
		$page_title .= " - ".$artikul->getTitle()." ".$artikul->getIme_marka()." ".$artikul->getIme().' '.$artikul->getKod();
	}else{ 
		if (isset($kategoriq)){
			$page_title .=" - ".$kategoriq->getIme();
		}
		if (isset($vid)){
			$page_title .=	" - ".$vid->getIme();				
		}
		if (isset($model)){
			$page_title .= " - ".$model->getIme();
		}
	}
	

	if(isset($vid)){
		// ------------------------------Start Cat Seen----------------------------------------
	    $catSeen = array(); 

	    //unset($_COOKIE["CatSeen"]);
	    
	    if(isset($_COOKIE["CatSeen"])){
			$catSeen=unserialize(stripslashes(rawurldecode($_COOKIE["CatSeen"])));
		}
		
		if(!isset($catSeen[$vid->getId()])) $catSeen[$vid->getId()] = 1;
		else $catSeen[$vid->getId()] = $catSeen[$vid->getId()] + 1;

		setcookie("CatSeen", serialize($catSeen), strtotime( '+360 days' ), '/');
		// ------------------------------End Cat Seen-------------------------------------------
	}

	if(isset($_GET['news'])){
		$stm = $pdo->prepare('SELECT * FROM `news`');
		$stm -> execute();
		$news_per_page=9;
		$news_count=$stm->rowCount(); 
		
		
		$stm = $pdo->prepare('SELECT * FROM `news` ORDER BY `news_date` DESC LIMIT :page , :perpage');
		$stm -> bindValue(':page', $page*$news_per_page, PDO::PARAM_INT);
		$stm -> bindValue(':perpage',$news_per_page, PDO::PARAM_INT);
		$stm -> execute();
		$rez = $stm -> fetchAll();
		$news=null;
		foreach ($rez as $n)
		{
			$news[]= new news($n);
		}
		$page_title=" - новини";
	}
	if(isset($_GET['contacts'])){		
		$page_title=" - контакти";
		
		$stm = $pdo->prepare('SELECT * FROM `shop_images` where `shop_id`=:shopid ORDER BY `id`');
		$stm -> bindValue(':shopid', 1, PDO::PARAM_INT);
		$stm -> execute();
		$rez = $stm -> fetchAll();
		$images=null;
		foreach ($rez as $img)
		{
			$images[]= $img['image'];
		}
		
	}
	if(isset($_GET['hotOffers'])){
		$page_title=" - намаления";
	}

	if(isset($_GET['bundles'])){
		$page_title=" - пакетни оферти";
	}

	//Load advertises intro banners
		$intro_banners = NULL;
		if (isset($__user) && $__user->is_partner()) {
			$stm = $pdo -> prepare('SELECT * FROM `zz_banners` where `podredba`=10 and `partner`=1 order by `id` DESC');
		} else {
			$stm = $pdo -> prepare('SELECT * FROM `zz_banners` where `podredba`=10 and `partner`=0 order by `id` DESC');
		}

		$stm -> execute();
		
		foreach ($stm->fetchAll() as $adv) {			
			$temp_advertise = new advertise((int)$adv['id']);		  
			$intro_banners[] = $temp_advertise ;
			unset($temp_advertise);
			unset($adv);
		}
	//End Load advertises intro banners
	
	require dir_root_template.'index.php';
?>
