var cover = $('.cover'),
    body = $('body'),
  	coverClick = 0,
    startClickPos = null;

$(function(){
  $(window).mousemove(function(e){
    curPos = e.pageX;
    if(coverClick === 1){
      curDif = Math.round(curPos - startClickPos);
      cover.css('width', body.width() - curDif + 'px');
      if(startClickPos === null){
        startClickPos = curPos;
      }
    }
    

  });
  
  cover.mousedown(function(){
    coverClick = 1;
    startClickPos = curPos;
  });
  
  body.mouseup(function(){
    coverClick = 0;
    startClickPos = null;
    if(cover.width() <= Math.round(body.width() / 1.5)){
      cover.stop().animate({ width: '0' }, 300); 
    } else {
      cover.stop().animate({ width: '100%' }, 300); 
    }
  });
  
  $('.lock').click(function(){
    cover.stop().animate({ width: '100%' }, 600);
  });
})