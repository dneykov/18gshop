$(document).ready(function() {
    
	$('#slider').bxSlider({
        pager: true,
        speed: 500,
        auto: false,
        autoHover: true,
        adaptiveHeight: true
    });
    $('.banners-slider').css('visibility', 'visible');
    //$('#slider').imagesLoaded(function(){
    //    $('#slider').reloadSlider();
    //});
    
	var width = $("#theBoxRightCol").width(),
		broi = width/270;
    broi = broi*1;
    if(broi == 0) broi = 5;
	$.ajax({
        type: "POST",
        url: "ajax/ajax_intro_new_artikuli.php",
        data: "broi="+broi,
        cache: false,
        success: function(html){
        	$('#theBoxRightCol-new').html(html);
        }
    });
    
    $.ajax({
        type: "POST",
        url: "ajax/ajax_intro_promo_artikuli.php",
        data: "broi="+broi,
        cache: false,
        success: function(html){
        	$('#theBoxRightCol-promo').html(html);
        }
    });
});

