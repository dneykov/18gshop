;(function($){
    $.fn.extend({
        'endlessPager': function(pager, itemSelector, threshold, container){
            var threshold = threshold || 200;
            return $(this).each(function(i, e){
                $(pager, this).hide();
                
                var loading = false, endreached = false;
                
                $(window).scroll(function(){
                    if(loading || endreached)
                        return;
                    if($(document).height() - $(window).height() <= $(window).scrollTop() + threshold) {
                        loading = true;
                        var next = $(pager).find('a[rel=next]').first();
                        if(next.length) {
                            $.get(
                                next.attr('href'),
                                function(page) {
                                    var dom = $(page);
                                    $(pager).replaceWith(dom.find(pager).hide());
                                    if (typeof container === "undefined") {
                                        $(e).append(dom.find(itemSelector))
                                    } else {
                                        $(e).append(dom.find(container + " " + itemSelector));
                                    }
                                    loading = false;
                                }
                            );
                        } else {
                            endreached = true;
                        }
                    }
                });
            });
        }
    });
})(jQuery);

