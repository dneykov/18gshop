$(document).ready(function() {

    $('#slider').bxSlider({
        pager: true,
        speed: 500,
        auto: false,
        autoHover: true
    });

    var width = $("#theBox").width(),
    broi = width/270;
    broi = broi*1;

    $.ajax({
        type: "POST",
        url: "ajax/ajax_intro_new_artikuli_ro.php",
        data: "broi="+broi,
        cache: false,
        success: function(html){
            $('#theBox-new').html(html);
        }
    });

    $.ajax({
        type: "POST",
        url: "ajax/ajax_intro_promo_artikuli_ro.php",
        data: "broi="+broi,
        cache: false,
        success: function(html){
            $('#theBox-promo').html(html);
        }
    });

    setTimeout(function(){
        $('.banners-slider').css('visibility', 'visible');
    }, 1000);

});
