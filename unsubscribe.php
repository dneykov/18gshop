<?php
require '__top.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>18gShop - Отписване бюлетин</title>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
    <meta http-equiv="refresh" content="3;url=<?php echo url; ?>">
</head>

<body>
<?php

$user_id = (int)$_GET['user_id'];
$expected = md5($user_id . hash);
if( $_GET['validation_hash'] == $expected ) {
    $stm = $pdo->prepare('DELETE FROM `newsletter_members` WHERE `id` = ?');
    $stm -> bindValue(1, (int)$user_id, PDO::PARAM_INT);
    $stm -> execute();
    echo '<span style="font-family: Verdana; font-size: 23px; color: #333; position: absolute; margin-left: auto; margin-right: auto; left: 0; right: 0; width: 360px; height: 20px; top: 0; bottom: 0; vertical-align: middle; margin: auto;">Вие се отписахте от бюлетина на 18gshop.</span>';
} else {
    echo '<span>Възникна грешка, моля опитайте по-късно</span>';
}

?>
</body>

</html>
