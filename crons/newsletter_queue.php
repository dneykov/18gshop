<?php
if (!is_cli()) return;

require_once '../__top.php';
const EMAILS_PER_RUN = 70;

$stm = $pdo->prepare('SELECT * FROM newsletter_queue LIMIT 0,' . EMAILS_PER_RUN);
$stm->execute();
$newsletter_emails = $stm->fetchAll();

if (count($newsletter_emails) == 0) return;

$ids_to_remove = array();
$news = null;

$mail = new PHPMailer();
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPKeepAlive = true;
$mail->SMTPSecure = 'tls';
$mail->Host = NEWSLETTER_MAIL_HOST;
$mail->Port = 587;
$mail->Username = NEWSLETTER_MAIL;
$mail->Password = NEWSLETTER_MAIL_PASSWORD;
$mail->CharSet = "utf-8";
$mail->From = NEWSLETTER_FROM;
$mail->FromName = NEWSLETTER_FROM_NAME;
$mail->AddAddress("newsletter@18gshop.com");
$mail->WordWrap = 50;
$mail->IsHTML(TRUE);

foreach ($newsletter_emails as $email) {
    if (!PHPMailer::ValidateAddress($email['email'])) {
        array_push($ids_to_remove, $email['id']);
        continue;
    }

    if (is_null($news) || $news['id'] != $email['news_id']) {
        $stm = $pdo->prepare('SELECT * FROM `news` WHERE `id`=?');
        $stm->bindValue(1, $email['news_id'], PDO::PARAM_INT);
        $stm->execute();

        $news = $stm->fetch();
    }

    $unsubscribe_link = '<br/><br/><br/>Не желаете да получавате нашият бюлетин? Натиснете <a href="' . url . 'unsubscribe.php?user_id=' . $email['subscriber_id'] . '&validation_hash=' . md5($email['subscriber_id'] . hash) . '">тук</a>, за да се отпишете от бюлетина.';
    $msg = '<a href="' . $news['url'] . '" ><img src="' . url . str_replace('.jpg', '_b.jpg', $news['image']) . '" style="max-width: 1024px;"/></a><br/><br/>
            	<a href="' . $news['url'] . '" ><img src="' . url . 'images/open_nl.png" width="120" /></a>';
    $msg .= $news['text_4c9a99e1a510a'] . '<br/>';
    $msg .= $unsubscribe_link;
    $mail->Body = $msg;
    $mail->AddBCC($email['email']);
    $msg = '';

    array_push($ids_to_remove, $email['id']);
}

$mail->Subject = $news['title_4c9a99e1a510a'];
$mail->Send();

$stm = $pdo->prepare('DELETE FROM newsletter_queue WHERE id IN(' . implode(", ", $ids_to_remove) . ')');
$stm->execute();

function is_cli() {
    return !http_response_code();
}