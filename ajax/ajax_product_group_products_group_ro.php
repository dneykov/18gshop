<?php
require '../__top.php';

$broi = (int)$_POST['broi'];
$group = (int)$_POST['group'];
$stm = $pdo->prepare('SELECT `id` FROM `group_products` WHERE `online` = 1 ORDER BY id DESC LIMIT 36');
$stm->execute();
if ($stm->rowCount() > 0) {
    $rez = $stm->fetchAll();
    shuffle($rez);
    if ($broi < $stm->rowCount()) $rez = array_slice($rez, 0, $broi);
    foreach ($rez as $n) {
        $style = 'margin-right: 55px;';
        if ($n === end($rez)) {
            $style = '';
        }
        if ($group != (int)$n['id']) {
            $v = new paket((int)$n['id']);
            ?>
            <div class="proddiv" style="border-bottom: none; <?php echo $style; ?>">
                <a href="<?php echo url; ?>index.php?gid=<?php echo $v->getId(); ?>">
                    <div class="promo">
                        <div style="background-color: #FF671F; width: 16px; height: 40px; color: #fff;">
                            <span class="rotate-90"><?php echo "-".round($v->getPercent(),0)."%"; ?></span>
                        </div>
                    </div>
                    <div class="prodimgdiv">
                        <table class='imageCenter'>
                            <tr>
                                <td><img alt="" src="<?php echo url . $v->getKartinka_t(); ?>" border="0"/></td>
                            </tr>
                        </table>
                    </div>
                    <div class="prod1" style="padding-left:15px;">
                        <div class="prodnamebox" style="width: 130px;">
                            <span class="prodcat"><?php echo $v->getName(); ?></span>
                        </div>
                        <div class="prodpricebox" style="width: 110px;">
                    <span class="prodpricenormal"> <span
                                style="color:#808080;font-size:12pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getPrice_normal() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup
                                    style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup> Lei</span></span>
                            <span class="prodpricepromo"
                                  style="font-size:15pt;font-style: italic;color:#ff671f;"><?php $tmp_cen = explode(".", number_format($v->getPrice() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                        </div>
                    </div>
                </a>
            </div>
            <?php
        }
    }
}
?>
<div style="clear:both"></div>