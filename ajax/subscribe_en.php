<?php
require '../__top.php';
if (isset($_POST['email'])) {
    $email = $search_url = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);;
    $stm = $pdo->prepare("SELECT * FROM `newsletter_members` WHERE `email` = ?");
    $stm->bindValue(1, $email, PDO::PARAM_STR);
    $stm->execute();
    if ($stm->rowCount() == 0) {
        $insert = $pdo->prepare("INSERT INTO `newsletter_members`(`email`, `created_at`) VALUES (?,?)");
        $insert->bindValue(1, $email, PDO::PARAM_STR);
        $insert->bindValue(2, time(), PDO::PARAM_INT);
        if ($insert->execute()) {
            echo '<span class="success">You are subscribed, Thank you!</span>';
        }
    } else {
        echo '<span class="error">This e-mail already exists.</span>';
    }
}
