<?php
require_once '../main.php';

$user_edit_error = NULL;
$order_address_data_error = NULL;

if (isset($_POST['comment']) && ($_POST['comment'] != lang_order_comment_text)) {
    $_SESSION['orderComment'] = $_POST['comment'];
}

if (isset($__user)) {

    $username_edit_mail = $_POST['edit_mail'];

    if (!preg_match('|^[a-zA-Z\.\-\_0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]+$|is', $username_edit_mail)) $user_edit_error[] = lang_login_register_error_mail_invalid;
    if (mb_strlen($username_edit_mail) > 200) $user_edit_error[] = lang_login_register_error_mail_lenght_long;

    $stm_check_mail = $pdo->prepare("SELECT `mail` FROM `members` WHERE LOWER(`mail`) = LOWER(?) and id != ? LIMIT 1");
    $stm_check_mail->bindValue(1, $username_edit_mail, PDO::PARAM_STR);
    $stm_check_mail->bindValue(2, $__user->getId(), PDO::PARAM_INT);
    $stm_check_mail->execute();

    //ако името вече го има показва грешка
    if ($stm_check_mail->rowCount() > 0) {
        $user_edit_error[] = str_replace("mail@mail.com", $username_edit_mail, lang_login_register_error_mail_already_used);
    }

    $changepass = false;

    if ($user_edit_error === NULL) {
        $stm = $pdo->prepare("UPDATE `members` SET `mail`=? WHERE id=?");
        $stm->bindValue(1, $username_edit_mail, PDO::PARAM_STR);
        $stm->bindValue(2, $__user->getId(), PDO::PARAM_INT);

        $stm->execute();
    }

    $order_addr_gsm = $_POST['gsm'];
    $order_addr_city = $_POST['city'];
    $order_addr_street = $_POST['street'];
    $order_addr_number = $_POST['number'];
    $order_addr_vhod = $_POST['vhod'];
    $order_addr_etaj = $_POST['etaj'];
    $order_addr_apartament = $_POST['apartament'];

    if (mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

    if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
    if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

    if (mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
    if (mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

    if (mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
    if (mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;

    //if(mb_strlen($order_addr_vhod) < 1) $order_address_data_error[] = lang_order_address_data_error_vhod;
    if (mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;

    //if(mb_strlen($order_addr_etaj) < 1) $order_address_data_error[] = lang_order_address_data_error_etaj;
    if (mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;

    //if(mb_strlen($order_addr_apartament) < 1) $order_address_data_error[] = lang_order_address_data_error_apartament;
    if (mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;

    if ($order_address_data_error == null) {
        $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=? WHERE id=?");
        $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
        //$stm->bindValue(2, $order_addr_telephone, PDO::PARAM_STR);
        $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
        $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
        $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
        $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
        $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
        $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
        $stm->bindValue(8, $__user->getId(), PDO::PARAM_INT);

        $stm->execute();

        require '../__members.php';
    }
    //}
    //pory4ka
    if ($order_address_data_error == null) {
        $order = null;
        $ordHeader = null;
        if (isset($_SESSION['orderComment'])) {
            $comment = $_SESSION['orderComment'];
        } else {
            $comment = "";
        }
        $ordHeader['gsm'] = $__user->getPhone_mobile();
        //$ordHeader['telephone']=$__user->getPhone_home();
        $ordHeader['city'] = $__user->getAdress_town();
        $ordHeader['street'] = $__user->getAdress_street();
        $ordHeader['number'] = $__user->getAdress_number();
        $ordHeader['vhod'] = $__user->getAdress_vhod();
        $ordHeader['etaj'] = $__user->getAdress_etaj();
        $ordHeader['apartament'] = $__user->getAdress_ap();
        $ordHeader['email'] = $__user->getMail();

        $order['details'] = $_SESSION['cart'];
        $cost = 0;
        foreach ($order['details'] as $key => $val) {
            if ($_SESSION['cart'][$key]['type'] == "product") {
                $artikul = new artikul((int)$key);
                $tmp_cena = $artikul->getCena_promo() * $order['details'][$key]['count'];
                if (!isset($tmp_cena) || ($tmp_cena == 0)) {
                    $tmp_cena = $artikul->getCena() * $order['details'][$key]['count'];
                }
            } else {
                $group = new paket((int)$key);
                $tmp_cena = $group->getPrice() * $order['details'][$key]['count'];
            }
            $order['details'][$key]['price'] = $tmp_cena;
            $cost += $tmp_cena;
        }

        $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = ? LIMIT 1");
        $ds->bindValue(1, (int)$_POST['country'], PDO::PARAM_INT);
        $ds->execute();
        $d = $ds->fetch();
        $dostavka_cena = $d['delivery_price'];
        $dostavka_free = $d['free_delivery'];
        if ($dostavka_free <= $cost) {
            $delivery = 0;
        } else {
            $delivery = $dostavka_cena;
        }

        $ordHeader['delivery'] = $delivery;

        $order['header'] = $ordHeader;
        $cost += $delivery;
        $scode = $_POST['scode'];

        $stm = $pdo->prepare("INSERT INTO `orders` (`user`,`comment`,`details`,`nomer`, `country`, `paypal`, `scode`, `cost`) VALUES (?,?,?,?,?,?,?,?)");

        $stm->bindValue(1, $__user->getId(), PDO::PARAM_INT);
        $stm->bindValue(2, $comment, PDO::PARAM_STR);
        $stm->bindValue(3, serialize($order), PDO::PARAM_STR);
        $last_id = time();
        $stm->bindValue(4, $last_id, PDO::PARAM_STR);
        $stm->bindValue(5, $_POST['country'], PDO::PARAM_STR);
        $stm->bindValue(6, 1, PDO::PARAM_INT);
        $stm->bindValue(7, $scode, PDO::PARAM_STR);
        $stm->bindValue(8, $cost, PDO::PARAM_STR);
        $stm->execute();

        $subject = lang_order_subject . $_SERVER['SERVER_NAME'];

        $msg = lang_order_msg1 . '<a href="' . url . '" target="_blank">' . $_SERVER['SERVER_NAME'] . '</a>.<br /><br />' . lang_order_msg_nom;
        $msg = $msg . $last_id . " " . lang_from . " " . date("d.m.Y") . "<br />";
        $msg = $msg . '<br/ ><br /><table style="min-width: 770px; font-family:verdana;font-size: 10pt;border-collapse: collapse;" cellpadding="10">
					<tr>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_kod . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_art . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;"></th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_opt . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;width: 66px;">' . lang_order_msg_br . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_pr . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_deposit . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_preorder . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_total . '</th>
					</tr>';

        $all = 0;
        if (isset($_SESSION['cart']))
            foreach ($_SESSION['cart'] as $key => $val) {

                if ($_SESSION['cart'][$key]['type'] == "product") {
                    $artikul = new artikul((int)$key);
                    $option = '';
                    $option = '<table>';
                    if (isset($_SESSION['cart'][$key]['options'])) {
                        foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                            $expl_arr = explode(",", $optionVal);

                            foreach ($expl_arr as $keyy => $vall) {
                                if ($keyy == 0) {
                                    $artikulid = (int)$vall;
                                }
                                if ($keyy == 1 && $artikulid == $key) {
                                    $option .= '<tr>';
                                    $option .= '<td>';
                                    if ($artikul->getTaggroup_dropdown()) {
                                        foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                            $option .= $gr->getIme() . ":";
                                        }
                                    }
                                    $option .= '</td>';
                                    $option .= '<td>';
                                    $option .= $vall;
                                    $option .= '</td>';
                                    $option .= '</tr>';
                                }
                            }
                        }
                    }

                    if (isset($_SESSION['cart'][$key]['upgrade_option'])) {
                        foreach ($_SESSION['cart'][$key]['upgrade_option'] as $upgrade_option) {
                            $option .= '<tr>';
                            $option .= '<td>';
                            $option .= $upgrade_option['upgrade_name'] . ":";
                            $option .= '</td>';
                            $option .= '<td>';
                            $option .= $upgrade_option['name'];
                            $option .= '</td>';
                            $option .= '</tr>';
                        }
                    }
                    $option .= '</table>';
                    $msg = $msg . '
								<tr ' . (!$artikul->isPreorder() ? 'style="border-bottom: 1px solid #e6e6e6;"' : "") . '>
									<td >
										' . $artikul->getKod() . '
									</td>
									<td>
										<div id="thumbs1">
											<div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
													<a href="' . url . 'index.php?id=' . $artikul->getId() . '"><img src="' . url . $artikul->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
											</div>	
										</div></td>
									
									<td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $artikul->getId() . '">' . stripslashes($artikul->getIme_marka()) . '<br/>' . stripslashes($artikul->getIme()) . '</a></td>

									<td >' . $option . '</td>

									<td >
										' . $_SESSION['cart'][$key]['count'] . '												
									</td>
									<td><span id="price' . $artikul->getId() . '" >' . lang_currency_prepend . ' ' . number_format(($_SESSION['cart'][$key]['price'] / CURRENCY_RATE), 2, '.', '') . '</span> ' . lang_currency_append . '</td>
									<td>' . (isset($_SESSION['cart'][$key]['preorder']) ? lang_currency_prepend . ' ' . number_format(($_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count']) / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append : "") . '</td>
									<td></td>
									<td ><span id="pricetotal' . $artikul->getId() . '" >' . lang_currency_prepend . ' ' . number_format((($artikul->isPreorder() ? $_SESSION['cart'][$key]['price'] : $_SESSION['cart'][$key]['price']) * (int)$_SESSION['cart'][$key]['count']) / CURRENCY_RATE, 2, '.', '') . ' </span> ' . lang_currency_append . '</td>											
								</tr>
								';

                    if ($artikul->isPreorder()) {
                        $msg = $msg . '<tr style="border-bottom: 1px solid #e6e6e6;"><td colspan="9"><span style="color: #FB671F;margin-top: -20px;margin-left: 60px;max-width: 690px;width: 100%;display: inline-block;">' . $artikul->getPreorderDescription() . '</span></td></tr>';
                    }

                    $all += $_SESSION['cart'][$key]['price'] * (int)$_SESSION['cart'][$key]['count'];

                    $dop_artikuli = $artikul->getAddArtikuli();
                    if ($dop_artikuli != "") {
                        $dop_art_arr = explode(",", $dop_artikuli);
                        foreach ($dop_art_arr as $dart) {
                            $dr = new artikul((int)$dart);

                            $option = '<table>';
                            $n = 0;
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                $artikulid = 0;
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0 && $vall == $dr->getId()) {
                                        $artikulid = (int)$vall;
                                        echo '<pre>';
                                        print_r("match");
                                        echo '</pre>';
                                    }
                                    if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                        $option .= '<tr>';
                                        $option .= '<td>';
                                        if ($dr->getTaggroup_dropdown()) {
                                            foreach ($dr->getTaggroup_dropdown() as $gr) {
                                                $option .= $gr->getIme() . ":";
                                            }
                                        }
                                        $option .= '</td>';
                                        $option .= '<td>';
                                        $option .= $vall;
                                        $option .= '</td>';
                                        $option .= '</tr>';
                                        $n = 1;
                                    }
                                }
                            }
                            $option .= '</table>';

                            $msg = $msg . '
                                    <tr>
                                           <td >
                                                ' . $dr->getKod() . '												
                                            </td>
                                            <td>
                                            <div id="thumbs1">
                                                <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
                                                    <a href="' . url . 'index.php?id=' . $dr->getId() . '"><img src="' . url . $dr->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
                                                </div>		
                                            </div>
                                            </td>
                                            <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $dr->getId() . '">' . stripslashes($dr->getIme_marka()) . '<br/>' . stripslashes($dr->getIme()) . '</a></td>
                                            <td >' . $option . '</td>
                                            <td >1</td>
                                            <td></td>
                                            <td></td>											
                                    </tr>
                                    ';
                        }
                    }

                } else if ($_SESSION['cart'][$key]['type'] == "group") {
                    $group = new paket((int)$key);
                    $msg = $msg . '
								<tr>
									<td ></td>
									<td>
									<div id="thumbs1">
										<div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
												<a href="' . url . 'index.php?gid=' . $group->getId() . '"><img src="' . url . $group->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
										</div>		
									</div>
									</td>
												
									<td ><a style="font-size: 10pt;" href="' . url . 'index.php?gid=' . $group->getId() . '">' . stripslashes($group->getName()) . '</a></td>

									<td ></td>

									<td >
										' . $_SESSION['cart'][$key]['count'] . '												
									</td>
									<td><span id="price' . $group->getId() . '" >' . lang_currency_prepend . ' ' . number_format($group->getPrice() / CURRENCY_RATE, 2, '.', '') . '</span> ' . lang_currency_append . '</td>
									<td></td>
									<td></td>
									<td ><span id="pricetotal' . $group->getId() . '" >' . lang_currency_prepend . ' ' . number_format(($group->getPrice() * (int)$_SESSION['cart'][$key]['count']) / CURRENCY_RATE, 2, '.', '') . ' </span> ' . lang_currency_append . '
									</td>											
								</tr>
							';
                    $option = '';
                    $all += ($group->getPrice() * ((int)$_SESSION['cart'][$key]['count']));

                    $dop_artikuli = $group->getProducts();
                    foreach ($dop_artikuli as $dart) {
                        $dr = new artikul((int)$dart);
                        $option = '<table>';
                        $artikulid = 0;
                        $n = 0;
                        foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                            $expl_arr = explode(",", $optionVal);
                            foreach ($expl_arr as $keyy => $vall) {
                                if ($keyy == 0 && $vall == $dr->getId()) {
                                    $artikulid = (int)$vall;
                                }
                                if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                    $option .= '<tr>';
                                    $option .= '<td>';
                                    if ($dr->getTaggroup_dropdown()) {
                                        foreach ($dr->getTaggroup_dropdown() as $gr) {
                                            $option .= $gr->getIme() . ":";
                                        }
                                    }
                                    $option .= '</td>';
                                    $option .= '<td>';
                                    $option .= $vall;
                                    $option .= '</td>';
                                    $option .= '</tr>';
                                    $n = 1;
                                }
                            }
                        }

                        $option .= '</table>';

                        $msg = $msg . '
	                            <tr>
                                   <td >
                                        ' . $dr->getKod() . '												
                                    </td>
                                    <td>
                                    <div id="thumbs1">
                                        <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
                                            <a href="' . url . 'index.php?id=' . $dr->getId() . '"><img src="' . url . $dr->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
                                        </div>		
                                    </div>
                                    </td>
                                    <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $dr->getId() . '">' . stripslashes($dr->getIme_marka()) . '<br/>' . stripslashes($dr->getIme()) . '</a></td>
                                    <td >' . $option . '</td>
                                    <td >' . $_SESSION['cart'][$key]['count'] . '</td>
                                    <td></td>
                                    <td></td>											
	                            </tr>
	                            ';
                    }

                }
            }

        $msg = $msg . '</table><br><br><br>';

        $msg = $msg . '<span style=" padding-left: 10px;">' . payment_type . ' <span style="color: #FB671F;">Paypal</span></span><br />';
        $msg = $msg . '<br />';

        $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = ? LIMIT 1");
        $ds->bindValue(1, (int)$_POST['country'], PDO::PARAM_INT);
        $ds->execute();
        $d = $ds->fetch();
        $dostavka_cena = $d['delivery_price'];
        $dostavka_free = $d['free_delivery'];

        $msg .= '<span style=" padding-left: 10px;">';
        if ($delivery === 'pickup') {
            $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . pickup_delivery . '</span> ';
        } elseif ($dostavka_free > $all) {
            if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                $dostavka_cena += $_SESSION['cart']['total_additional_delivery'];
            }

            $all += $dostavka_cena;
            $tmp = number_format($dostavka_cena / CURRENCY_RATE, 2, '.', '');

            $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . lang_currency_append . ' ' . $tmp . ' ' . lang_currency_append . '</span> ';

        } else {
            if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                $delivery = $_SESSION['cart']['total_additional_delivery'];
                $tmp = number_format($delivery / CURRENCY_RATE, 2, '.', '');

                $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . $tmp . ' ' . lang_currency_append . '</span> ';
            } else {
                $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . lang_currency_append . ' ' . lang_free_shipping . '.</span> ';
            }
        }

        switch ($_POST['payment_type']) {
            case "cash":
                $msg = $msg . '</span><br /><div style="background: #FB671F; min-height: 20px; max-height: 20px; display: inline-block; line-height: 20px; color: #fff; padding-left: 10px; padding-right: 10px; margin-top: 10px; min-width: 300px; width: 300px;">' . lang_order_msg_total2 . ' <span style="font-weight: bold;">' . lang_currency_prepend . ' ' . number_format($all / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></div><br><br>' . lang_order_tmp_cash . '<br/><br/>' . lang_order_msg3_cash . '<br/><br/>' . lang_order_preorde_desc;
                break;
            case "bank":
                $msg = $msg . '</span><br /><div style="background: #FB671F; min-height: 20px; max-height: 20px; display: inline-block; line-height: 20px; color: #fff; padding-left: 10px; padding-right: 10px; margin-top: 10px; min-width: 300px; width: 300px;">' . lang_order_msg_total2 . ' <span style="font-weight: bold;">' . lang_currency_prepend . ' ' . number_format($all / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></div><br><br>' . lang_order_tmp_bank . '<br/><br/>' . lang_order_tmp_bank_details . '<br/>';
                $msg = $msg . iban . " " . $_POST['iban'] . '<br />';
                $msg = $msg . nl2br($_POST['bank_details']) . '<br /><br />';
                $msg = $msg . lang_order_msg3_bank . '<br/><br/>' . lang_order_preorde_desc;
                break;
            default:
                $msg = $msg . '</span><br /><div style="background: #FB671F; min-height: 20px; max-height: 20px; display: inline-block; line-height: 20px; color: #fff; padding-left: 10px; padding-right: 10px; margin-top: 10px; min-width: 300px; width: 300px;">' . lang_order_msg_total2 . ' <span style="font-weight: bold;">' . lang_currency_prepend . ' ' . number_format($all / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></div><br><br>' . lang_order_tmp_cash . '<br/><br/>' . lang_order_msg3_cash . '<br/><br/>' . lang_order_preorde_desc;
                break;
        }


        $mail = new PHPMailer();
        $mail->Host = "localhost";  // specify main and backup server
        $mail->CharSet = "UTF-8";

        //$mail->From = mail_office;
        $mail->From = mail_office;
        $mail->FromName = $_SERVER['SERVER_NAME'];
        $mail->AddAddress($ordHeader['email']);
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        $mail->IsHTML(TRUE);                                  // set email format to HTML

        $mail->Subject = $subject;
        $mail->Body = $msg;
        $mail->AltBody = htmlspecialchars($msg);
        $mail->Send();

//------------------------------------------------------------------


        $subject = 'нова поръчка на ' . $_SERVER['SERVER_NAME'];

        $msg = 'Имате нова поръчка: ';
        $msg = $msg . $last_id . " от " . date("d.m.Y") . "<br />";
        $msg = $msg . "<br /> от <br /><br />";
        $msg .= $__user->getName() . ' ' . $__user->getName_last() . "<br />";
        $msg .= $ordHeader['email'] . "<br />";
        $msg .= "<br />град " . $ordHeader['city'];
        $msg .= "<br />улица " . $ordHeader['street'] . " №" . $ordHeader['number'];
        $msg .= "<br />вход " . $ordHeader['vhod'] . " етаж " . $ordHeader['etaj'] . " апартамент " . $ordHeader['apartament'] . "<br />";
        $msg .= "тел.: " . $ordHeader['gsm'] . "<br />";

        if ($_POST['account_type'] == '1') {
            $msg .= "<br />";

            $msg .= "Име на фирма: " . $company_name . "<br />";
            $msg .= "М.О.Л: " . $company_mol . "<br />";
            $msg .= "ЕИК: " . $company_eik . "<br />";
            $msg .= "ЗДДС №: " . $company_vat . "<br />";
            $msg .= "Адрес: " . $company_city . ", " . $company_street . " " . $company_number . "<br />";
            $msg .= "Телефон: " . $company_phone . "<br />";
        }

        $msg = $msg . '<br/ ><br /><table style="min-width: 770px; font-family:verdana;font-size: 10pt; " cellpadding="10">
				
					<tr>
						<th style="text-align: left;font-weight:normal;padding:10px;">Код</th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Артикул</th>
						<th style="text-align: left;font-weight:normal;padding:10px;"></th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Опции</th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Брой</th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Цена</th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Аванс</th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Остатък</th>
						<th style="text-align: left;font-weight:normal;padding:10px;">Общо</th>
					</tr>';

        $all = 0;
        if (isset($_SESSION['cart']))
            foreach ($_SESSION['cart'] as $key => $val) {
                if ($_SESSION['cart'][$key]['type'] == "product") {
                    $artikul = new artikul((int)$key);
                    $option = '';
                    $option = '<table>';
                    if (isset($_SESSION['cart'][$key]['options'])) {
                        foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                            $expl_arr = explode(",", $optionVal);
                            foreach ($expl_arr as $keyy => $vall) {
                                if ($keyy == 0) {
                                    $artikulid = (int)$vall;
                                }
                                if ($keyy == 1 && $artikulid == $key) {
                                    $option .= '<tr>';
                                    $option .= '<td>';
                                    if ($artikul->getTaggroup_dropdown()) {
                                        foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                            $option .= $gr->getIme() . ":";
                                        }
                                    }
                                    $option .= '</td>';
                                    $option .= '<td>';
                                    $option .= $vall;
                                    $option .= '</td>';
                                    $option .= '</tr>';
                                }
                            }
                        }
                    }

                    if (isset($_SESSION['cart'][$key]['upgrade_option'])) {
                        foreach ($_SESSION['cart'][$key]['upgrade_option'] as $upgrade_option) {
                            $option .= '<tr>';
                            $option .= '<td>';
                            $option .= $upgrade_option['upgrade_name'] . ":";
                            $option .= '</td>';
                            $option .= '<td>';
                            $option .= $upgrade_option['name'];
                            $option .= '</td>';
                            $option .= '</tr>';
                        }
                    }
                    $option .= '</table>';
                    $msg = $msg . '
									<tr>
												<td >
													' . $artikul->getKod() . '												
												</td>
												<td>
									<div id="thumbs1">
										<div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
												<a href="' . url . 'index.php?id=' . $artikul->getId() . '"><img src="' . url . $artikul->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
										</div>		
									</div>
												</td>
												
												<td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $artikul->getId() . '">' . stripslashes($artikul->getIme_marka()) . '<br/>' . stripslashes($artikul->getIme()) . '</a></td>

												<td >' . $option . '</td>

												<td >
													' . $_SESSION['cart'][$key]['count'] . '												
												</td>
												<td><span id="price' . $artikul->getId() . '" >' . number_format($_SESSION['cart'][$key]['price'], 2, '.', '') . '</span> лв.</td>
												<td>' . (isset($_SESSION['cart'][$key]['preorder']) ? number_format(($_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count']), 2, '.', '') . ' лв.' : "") . '</td>
												<td></td>
												<td ><span id="pricetotal' . $artikul->getId() . '" >' . number_format((($artikul->isPreorder() ? $_SESSION['cart'][$key]['price'] : $_SESSION['cart'][$key]['price']) * (int)$_SESSION['cart'][$key]['count']), 2, '.', '') . ' </span> лв.</td>											
											</tr>
									';

                    $all += (($artikul->getCena_promo() == 0) ? $artikul->getCena() : $artikul->getCena_promo()) * ((int)$_SESSION['cart'][$key]['count']);

                    $dop_artikuli = $artikul->getAddArtikuli();
                    if ($dop_artikuli != "") {
                        $dop_art_arr = explode(",", $dop_artikuli);
                        foreach ($dop_art_arr as $dart) {
                            $dr = new artikul((int)$dart);
                            $option = '<table>';
                            $artikulid = 0;
                            $n = 0;
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0 && $vall == $dr->getId()) {
                                        $artikulid = (int)$vall;
                                    }
                                    if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                        $option .= '<tr>';
                                        $option .= '<td>';
                                        if ($dr->getTaggroup_dropdown()) {
                                            foreach ($dr->getTaggroup_dropdown() as $gr) {
                                                $option .= $gr->getIme() . ":";
                                            }
                                        }
                                        $option .= '</td>';
                                        $option .= '<td>';
                                        $option .= $vall;
                                        $option .= '</td>';
                                        $option .= '</tr>';
                                        $n = 1;

                                        $lang_prefix = lang_prefix;
                                        $tgstm = $pdo->prepare("SELECT E.id FROM etiketi E, etiketi_zapisi Z WHERE E.ime_$lang_prefix = ? AND Z.id_artikul = ? LIMIT 1");
                                        $tgstm->bindValue(1, trim($vall), PDO::PARAM_STR);
                                        $tgstm->bindValue(2, (int)$dr->getId(), PDO::PARAM_INT);
                                        $tgstm->execute();
                                        $et = $tgstm->fetch();

                                        $stmt = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `tag_id` = :tagid AND `product_id` = :id LIMIT 1");
                                        $stmt->bindValue(':tagid', (int)$et['id'], PDO::PARAM_INT);
                                        $stmt->bindValue(':id', (int)$dr->getId(), PDO::PARAM_INT);
                                        $stmt->execute();
                                        if ($stmt->rowCount() > 0) {
                                            $t = $stmt->fetch();
                                            $option .= '<tr>';
                                            $option .= '<td>';
                                            $option .= 'код:';
                                            $option .= '</td>';
                                            $option .= '<td>';
                                            $option .= $t['code'];
                                            $option .= '</td>';
                                            $option .= '</tr>';
                                        } else {
                                            $stma = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = :id AND `type` = 1 LIMIT 1");
                                            $stma->bindValue(':id', (int)$dr->getId(), PDO::PARAM_STR);
                                            $stma->execute();
                                            if ($stma->rowCount() > 0) {
                                                $t = $stma->fetch();
                                                $option .= '<tr>';
                                                $option .= '<td>';
                                                $option .= 'код:';
                                                $option .= '</td>';
                                                $option .= '<td>';
                                                $option .= $t['code'];
                                                $option .= '</td>';
                                                $option .= '</tr>';
                                            }
                                        }
                                    }
                                }
                            }

                            $option .= '</table>';

                            $msg = $msg . '
			                            <tr>
			                                   <td >
			                                        ' . $dr->getKod() . '												
			                                    </td>
			                                    <td>
			                                    <div id="thumbs1">
			                                        <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
			                                            <a href="' . url . 'index.php?id=' . $dr->getId() . '"><img src="' . url . $dr->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
			                                        </div>		
			                                    </div>
			                                    </td>
			                                    <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $dr->getId() . '">' . stripslashes($dr->getIme_marka()) . '<br/>' . stripslashes($dr->getIme()) . '</a></td>
			                                    <td >' . $option . '</td>
			                                    <td >1</td>
			                                    <td></td>
			                                    <td></td>											
			                            </tr>
			                            ';
                        }
                    }
                } else if ($_SESSION['cart'][$key]['type'] == "group") {
                    $group = new paket((int)$key);

                    $msg = $msg . '
									<tr>
										<td ></td>
										<td>
										<div id="thumbs1">
											<div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
													<a href="' . url . 'index.php?gid=' . $group->getId() . '"><img src="' . url . $group->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
											</div>		
										</div>
										</td>
													
										<td ><a style="font-size: 10pt;" href="' . url . 'index.php?gid=' . $group->getId() . '">' . stripslashes($group->getName()) . '</a></td>

										<td ></td>

										<td >
											' . $_SESSION['cart'][$key]['count'] . '												
										</td>
										<td><span id="price' . $group->getId() . '" >' . number_format($group->getPrice(), 2, '.', '') . '</span> лв.</td>
										<td></td>
										<td></td>
										<td ><span id="pricetotal' . $group->getId() . '" >' . number_format(($group->getPrice()) * ((int)$_SESSION['cart'][$key]['count']), 2, '.', '') . ' </span> лв.
										</td>											
									</tr>
								';

                    $option = '';
                    $all += ($group->getPrice() * ((int)$_SESSION['cart'][$key]['count']));
                    $dop_artikuli = $group->getProducts();
                    foreach ($dop_artikuli as $dart) {
                        $dr = new artikul((int)$dart);
                        $option = '<table>';
                        $artikulid = 0;
                        $n = 0;
                        foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                            $expl_arr = explode(",", $optionVal);
                            foreach ($expl_arr as $keyy => $vall) {
                                if ($keyy == 0 && $vall == $dr->getId()) {
                                    $artikulid = (int)$vall;
                                }
                                if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                    $option .= '<tr>';
                                    $option .= '<td>';
                                    if ($dr->getTaggroup_dropdown()) {
                                        foreach ($dr->getTaggroup_dropdown() as $gr) {
                                            $option .= $gr->getIme() . ":";
                                        }
                                    }
                                    $option .= '</td>';
                                    $option .= '<td>';
                                    $option .= $vall;
                                    $option .= '</td>';
                                    $option .= '</tr>';
                                    $n = 1;

                                    $lang_prefix = lang_prefix;
                                    $tgstm = $pdo->prepare("SELECT E.id FROM etiketi E, etiketi_zapisi Z WHERE E.ime_$lang_prefix = ? AND Z.id_artikul = ? LIMIT 1");
                                    $tgstm->bindValue(1, trim($vall), PDO::PARAM_STR);
                                    $tgstm->bindValue(2, (int)$dr->getId(), PDO::PARAM_INT);
                                    $tgstm->execute();
                                    $et = $tgstm->fetch();

                                    $stmt = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `tag_id` = :tagid AND `product_id` = :id LIMIT 1");
                                    $stmt->bindValue(':tagid', (int)$et['id'], PDO::PARAM_INT);
                                    $stmt->bindValue(':id', (int)$dr->getId(), PDO::PARAM_INT);
                                    $stmt->execute();
                                    if ($stmt->rowCount() > 0) {
                                        $t = $stmt->fetch();
                                        $option .= '<tr>';
                                        $option .= '<td>';
                                        $option .= 'код:';
                                        $option .= '</td>';
                                        $option .= '<td>';
                                        $option .= $t['code'];
                                        $option .= '</td>';
                                        $option .= '</tr>';
                                    } else {
                                        $stma = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = :id AND `type` = 1 LIMIT 1");
                                        $stma->bindValue(':id', (int)$dr->getId(), PDO::PARAM_STR);
                                        $stma->execute();
                                        if ($stma->rowCount() > 0) {
                                            $t = $stma->fetch();
                                            $option .= '<tr>';
                                            $option .= '<td>';
                                            $option .= 'код:';
                                            $option .= '</td>';
                                            $option .= '<td>';
                                            $option .= $t['code'];
                                            $option .= '</td>';
                                            $option .= '</tr>';
                                        }
                                    }
                                }
                            }
                        }

                        $option .= '</table>';

                        $msg = $msg . '
		                            <tr>
	                                   <td >
	                                        ' . $dr->getKod() . '												
	                                    </td>
	                                    <td>
	                                    <div id="thumbs1">
	                                        <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
	                                            <a href="' . url . 'index.php?id=' . $dr->getId() . '"><img src="' . url . $dr->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
	                                        </div>		
	                                    </div>
	                                    </td>
	                                    <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $dr->getId() . '">' . stripslashes($dr->getIme_marka()) . '<br/>' . stripslashes($dr->getIme()) . '</a></td>
	                                    <td >' . $option . '</td>
	                                    <td >' . $_SESSION['cart'][$key]['count'] . '</td>
	                                    <td></td>
	                                    <td></td>											
		                            </tr>
		                            ';
                    }
                }
            }

        $msg = $msg . '</table><br><br><br>';
        $msg = $msg . 'коментар: <br>';
        $msg = $msg . ' ' . $_SESSION['orderComment'] . ' <br><br>';

        $msg = $msg . '<span>начин на плащане: <span>Paypal</span></span><br />';

        if ($_POST['payment_type'] == "bank") {
            $msg = $msg . '<span>IBAN: <span>' . $_POST['iban'] . '</span></span><br />';
            $msg = $msg . '<span>банкови детайли: </span><br /><div>' . nl2br($_POST['bank_details']) . '</div><br />';
        } else {
            $msg = $msg . '<br />';
        }

        $dss = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = ? LIMIT 1");
        $dss->bindValue(1, (int)$_POST['country'], PDO::PARAM_INT);
        $dss->execute();
        $dd = $dss->fetch();
        $dostavka_cena = $dd['delivery_price'];
        $dostavka_free = $dd['free_delivery'];

        if ($delivery === 'pickup') {
            $msg = $msg . 'Доставка: Вземете на място от нас. ';
        } elseif ($dostavka_free > $all) {
            if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                $dostavka_cena += $_SESSION['cart']['total_additional_delivery'];
            }

            $all += $dostavka_cena;
            $tmp = number_format($dostavka_cena, 2, '.', '');

            $msg = $msg . 'Доставка ' . $tmp . ' лв. ';

        } else {
            if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                $delivery = $_SESSION['cart']['total_additional_delivery'];
                $tmp = number_format($delivery / CURRENCY_RATE, 2, '.', '');

                $msg = $msg . 'Доставка ' . $tmp . ' лв. ';
            } else {
                $msg = $msg . 'Безплатна доставка. ';
            }
        }
        $msg = $msg . 'Обща стойност на поръчката: <span style="font-weight: bold;">' . number_format($all, 2, '.', '') . ' лв</span>.<br><br>Може да влезете в <a href="' . url . '/admin/" style="font-weight: bold;">административния панел</a> за да обработите поръчката.';


        $mail = new PHPMailer();
        $mail->Host = "localhost";  // specify main and backup server
        $mail->CharSet = "UTF-8";

        //$mail->From = mail_office;
        $mail->From = mail_office;
        $mail->FromName = $_SERVER['SERVER_NAME'];
        $mail->AddAddress(mail_office);
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        $mail->IsHTML(TRUE);                                  // set email format to HTML

        $mail->Subject = $subject;
        $mail->Body = $msg;
        $mail->AltBody = htmlspecialchars($msg);

        $mail->Send();

        unset($_SESSION['orderComment']);
        unset($_SESSION['cart']);
        $sucessSend = true;
    }
}

$page_title = " - поръчка";