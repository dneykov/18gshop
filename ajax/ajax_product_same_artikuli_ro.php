<?php
require '../__top.php';

$cat = (int)$_POST['cat'];
$vid = (int)$_POST['vid'];
$marka = (int)$_POST['marka'];
$broi = (int)$_POST['broi'];
$except = (int)$_POST['except'];
$stm = $pdo->prepare('SELECT `id` FROM `artikuli` WHERE `id_vid` = ? AND `id_model` = ? AND `id_kategoriq`= ? AND `id` != ? AND `IsOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 36');
$stm->bindValue(1, $vid, PDO::PARAM_INT);
$stm->bindValue(2, $marka, PDO::PARAM_INT);
$stm->bindValue(3, $cat, PDO::PARAM_INT);
$stm->bindValue(4, $except, PDO::PARAM_INT);
$stm->execute();
if ($stm->rowCount() > 0) {
    $rez = $stm->fetchAll();
    shuffle($rez);
    if (in_array($except, $rez)) {
        $position = array_search($except, $rez);
        unset($rez[$position]);
    }
    if ($broi < $stm->rowCount()) $rez = array_slice($rez, 0, $broi);
    foreach ($rez as $n) {
        $v = new artikul((int)$n['id']);

        if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

        $style = 'margin-right: 55px;';
        if ($n === end($rez)) {
            $style = '';
        }
        ?>
        <div class="proddiv" style="border-bottom: none; <?php echo $style; ?>">
            <a href="<?php echo url; ?>index.php?id=<?php echo $v->getId() . "&category=" . $v->getId_kategoriq() . "&mode=" . $v->getId_vid(); ?>">
                <?php
                $brnd = $v->getBrand();
                if ($brnd->getBWImage() != "") {
                    ?>
                    <div class="brand_image"><img src="<?php echo $brnd->getBWImage(); ?>"
                                                  alt="<?php echo $v->getIme(); ?>" class="brand_img"/></div>
                    <?php
                }
                ?>
                <div class="promo">
                    <?php
                    if($v->getCena_promo()){
                        $tmpraz=($v->getCena() - $v->getCena_promo() );
                        if ($v->IsNew()) {
                            echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff;position:absolute; right: 20px; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                        } else {
                            echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                        }
                    }

                    if($v->getAddArtikuli() != ""){
                        echo '<div style="float: left; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <img src="'.url.'images/icons/gift.png" alt="promo" />
                                    </div>';
                    }

                    if ($v->isPreorder() != "") {
                        echo '<div style="float: left; background-color: #29AC92; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90">PRE</span>
                                    </div>';
                    }

                    if ($v->IsNew()) {
                        echo '<div style="float: left; background-color: #01A0E2; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90" style="font-size: 11px; padding-top: 17px;">NEW</span>
                                    </div>';
                    }
                    ?>
                </div>
                <div class="prodimgdiv">
                    <table class='imageCenter'>
                        <tr>
                            <td><img alt="" src="<?php echo url . $v->getKartinka_t(); ?>" border="0"/>
            </a></td></tr></table>

        </div>

        <div class="prod1" style="padding-left:15px;">
            <div class="prodnamebox" style="width: 130px;">
                <span class="prodname"><?php echo $v->getIme_marka(); ?></span>
                /
                <span class="prodcat"><?php echo $v->getIme(); ?></span>
            </div>

            <!--<div ><font class="prodcode"><?php echo $v->getKod(); ?></font></div>-->
            <div class="prodpricebox" style="width: 110px;">
                <?php if (isset($__user) && $__user->is_partner()) : ?>
                    <?php if ($v->getCenaDostavna() > 0) : ?>
                        <span class="prodpricepromo"
                              style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCenaDostavna() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                    <?php else : ?>
                        <?php if ($v->getCena_promo()) : ?>
                            <span class="prodpricenormal"> <span
                                        style="color:#808080;font-size:12pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup> Lei</span></span>
                            <span class="prodpricepromo"
                                  style="font-size:15pt;font-style: italic;color:#FF671F;"><?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                        <?php else : ?>
                            <span class="prodpricepromo"
                                  style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if ($v->getCena_promo()) : ?>
                        <span class="prodpricenormal"> <span
                                    style="color:#808080;font-size:12pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup> Lei</span></span>
                        <span class="prodpricepromo"
                              style="font-size:15pt;font-style: italic;color:#FF671F;"><?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                    <?php else : ?>
                        <span class="prodpricepromo"
                              style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup> Lei</span>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        </a>
        </div>
        <?php
    }
    echo '<div style="clear:both"></div>';
}