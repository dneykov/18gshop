<?php
require_once '../main.php';
if (isset($_POST['gid']) && is_array($_POST['price_tag'])) {

    if (isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
        $user = new user((int)$_COOKIE['username_id']);
    } else {
        $user = NULL;
    }


    $group = new paket((int)$_POST['gid']);
    $price_tags = $_POST['price_tag'];

    if (is_null($group)) return;

    $normal_price = explode(".", number_format($group->getPrice_normal($price_tags, $user) / CURRENCY_RATE, 2, '.', ''));
    $promo_price = explode(".", number_format($group->getPrice($price_tags, $user) / CURRENCY_RATE, 2, '.', ''));

    $price = '<span class="prodpricenormal" style="float:left;margin-right:10px;"> <span style="color:#424242;font-size:12pt;font-style: italic;">' . lang_currency_prepend . ' ' . $normal_price[0] . ' <sup style="font-size:10pt">' . $normal_price[1] . '</sup></span> </span>
		  <span class="prodpricepromo" style="float:none !important; margin-left: 3px; font-size:20px;font-style: italic; color: #ff671f;">' . lang_currency_prepend . ' ' . $promo_price[0] . ' <sup style="font-size:12px">' . $promo_price[1] . '</sup></span>';

    if (isset($user) && $user->is_partner()) {
        $tmp_cen = explode(".", number_format($group->getPrice_reference($price_tags, $user) / CURRENCY_RATE, 2, '.', ''));
        $price .= '<div style="position: absolute;bottom: 10px;margin-top: 5px; margin-right: 25px; font-size:8pt; color: #424242;">' . lang_enduser_ref_price . lang_currency_prepend . ' ' . $tmp_cen[0] . ' <sup>' . $tmp_cen[1] . '</sup></div>';
    }

    $tmpraz = (($group->getPrice_normal($price_tags, $user)) - ($group->getPrice($price_tags, $user)));
    $tmp_razf = explode(".", number_format($tmpraz, 2, '.', ''));
    $price .= '<div class="saving-box">';
    $price .= '<span style="float: left;margin-top: 2px;margin-left: 5px;font-size: 8pt;color: #FF671F;">you save ' . lang_currency_prepend . '' . $tmp_razf[0] . '<sup>' . $tmp_razf[1] . '</sup> ' . lang_currency_append . ' (-' . round((($tmpraz / ($group->getPrice_normal($price_tags, $user))) * 100), 0) . '%)</span>';
    $price .= '</div>';

    $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
    $ds->execute();
    $d = $ds->fetch();
    $dostavka_cena = $d['delivery_price'];
    $dostavka_free = $d['free_delivery'];
    $additional_delivery = 0;

    foreach ($group->getProducts() as $d) {
        $group_article = new artikul((int)$d);
        if ($group_article->isAvaliable()) {
            $additional_delivery += $group_article->getAdditionalDelivery();
        }
    }

    $delivery = 0;
    $ac = 0;
    $ac = $group->getPrice($price_tags, $user);
    if ($dostavka_free < $ac) {
        if ($additional_delivery != '' && $additional_delivery > 0) {
            $delivery = '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">' . lang_currency_prepend . ' ' . lang_order_delivery . ' ' . $additional_delivery . ' </div>';
        } else {
            $delivery = '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">' . with_free_delivery . '</div>';
        }
    } else {
        if ($additional_delivery != '' && $additional_delivery > 0) {
            $total_delivery = $dostavka_cena + $additional_delivery;
            $delivery = '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">' . lang_currency_prepend . ' ' . lang_order_delivery . ' ' . $total_delivery . '</div>';
        } else {
            $delivery = '<div style="float: right; margin-top: 5px; width: 175px; text-align: center; font-size:8pt; color: #808080;">' . lang_currency_prepend . ' ' . lang_order_delivery . ' ' . $dostavka_cena . '</div>';
        }
    }

    // Remove delivery
    $delivery = '';

    $result = [
        'price' => $price,
        'delivery' => $delivery,
    ];

    echo json_encode($result);
}