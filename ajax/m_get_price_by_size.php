<?php
require_once '../main.php';
if (isset($_POST['product_id']) && isset($_POST['tag_id'])) {
    if (isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
        $user = new user((int)$_COOKIE['username_id']);
    }
    $product_id = $_POST['product_id'];
    $tag_id = $_POST['tag_id'];
    $additional_price = $_POST['additional_price'];

    $stm = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id`=?");
    $stm->bindValue(1, $product_id, PDO::PARAM_INT);
    $stm->bindValue(2, $tag_id, PDO::PARAM_INT);
    $stm->execute();
    $warehouse = $stm->fetch();

    $promo_price = (double)$warehouse['promo_price'];
    $price_regular = (double)$warehouse['price'];
    $big_price = (double)$warehouse['big_price'];
    $code = $warehouse['code'];
    

    $stm = $pdo->prepare("SELECT * FROM `artikuli` WHERE `id` = ?");
    $stm->bindValue(1, $product_id, PDO::PARAM_INT);
    $stm->execute();
    $artikul = new artikul($stm->fetch());


    if (isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
        if ($user->is_partner()) {
            if ($big_price != "" && $big_price > 0) {
                $result = [
                    'price' => outputPrice(number_format(($big_price + $additional_price), 2, '.', '')) . outputEndUserPrice($artikul),
                    'delivery' => outputDelivery($artikul, ($big_price + $additional_price)),
                    'quantity' => $artikul->quantityByCode($code),
                    'code' => $code,
                ];

                echo json_encode($result);
            } else if ($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                $result = [
                    'price' => outputPrice(number_format(($artikul->getCenaDostavna() + $additional_price), 2, '.', '')) . outputEndUserPrice($artikul),
                    'delivery' => outputDelivery($artikul, ($artikul->getCenaDostavna() + $additional_price)),
                    'quantity' => $artikul->quantityByCode($code),
                    'code' => $code,
                ];

                echo json_encode($result);
            } else {
                if (empty($price_regular) && empty($promo_price)) {
                    if ($artikul->getCena_promo() > 0) {
                        $result = [
                            'price' => outputPrice(number_format(($artikul->getCena() + $additional_price), 2, '.', ''), number_format(($artikul->getCena_promo() + $additional_price), 2, '.', '')),
                            'delivery' => outputDelivery($artikul, ($artikul->getCena() + $additional_price), ($artikul->getCena_promo() + $additional_price)),
                            'quantity' => $artikul->quantityByCode($code),
                            'code' => $code,
                        ];

                        echo json_encode($result);
                    } else {
                        $result = [
                            'price' => outputPrice(number_format(($artikul->getCena() + $additional_price), 2, '.', '')),
                            'delivery' => outputDelivery($artikul, ($artikul->getCena() + $additional_price)),
                            'quantity' => $artikul->quantityByCode($code),
                            'code' => $code,
                        ];

                        echo json_encode($result);
                    }
                } else {
                    if (!empty($promo_price)) {
                        $result = [
                            'price' => outputPrice(number_format(($price_regular + $additional_price), 2, '.', ''), number_format(($promo_price + $additional_price), 2, '.', '')),
                            'delivery' => outputDelivery($artikul, ($price_regular + $additional_price), ($promo_price + $additional_price)),
                            'quantity' => $artikul->quantityByCode($code),
                            'code' => $code,
                        ];

                        echo json_encode($result);
                    } else {
                        $result = [
                            'price' => outputPrice(number_format(($price_regular + $additional_price), 2, '.', '')),
                            'delivery' => outputDelivery($artikul, ($price_regular + $additional_price)),
                            'quantity' => $artikul->quantityByCode($code),
                            'code' => $code,
                        ];

                        echo json_encode($result);
                    }
                }
            }
        } else {
            if (empty($price_regular) && empty($promo_price)) {
                if ($artikul->getCena_promo() > 0) {
                    $result = [
                        'price' => outputPrice(number_format(($artikul->getCena() + $additional_price), 2, '.', ''), number_format(($artikul->getCena_promo() + $additional_price), 2, '.', '')),
                        'delivery' => outputDelivery($artikul, ($artikul->getCena() + $additional_price), ($artikul->getCena_promo() + $additional_price)),
                        'quantity' => $artikul->quantityByCode($code),
                        'code' => $code,
                    ];

                    echo json_encode($result);
                } else {
                    $result = [
                        'price' => outputPrice(number_format(($artikul->getCena() + $additional_price), 2, '.', '')),
                        'delivery' => outputDelivery($artikul, ($artikul->getCena() + $additional_price)),
                        'quantity' => $artikul->quantityByCode($code),
                        'code' => $code,
                    ];

                    echo json_encode($result);
                }
            } else {
                if (!empty($promo_price)) {
                    $result = [
                        'price' => outputPrice(number_format(($price_regular + $additional_price), 2, '.', ''), number_format(($promo_price + $additional_price), 2, '.', '')),
                        'delivery' => outputDelivery($artikul, ($price_regular + $additional_price), ($promo_price + $additional_price)),
                        'quantity' => $artikul->quantityByCode($code),
                        'code' => $code,
                    ];

                    echo json_encode($result);
                } else {
                    $result = [
                        'price' => outputPrice(number_format(($price_regular + $additional_price), 2, '.', '')),
                        'delivery' => outputDelivery($artikul, ($price_regular + $additional_price)),
                        'quantity' => $artikul->quantityByCode($code),
                        'code' => $code,
                    ];

                    echo json_encode($result);
                }
            }
        }
    } else {
        if (empty($price_regular) && empty($promo_price)) {
            if ($artikul->getCena_promo() > 0) {
                $result = [
                    'price' => outputPrice(number_format(($artikul->getCena() + $additional_price), 2, '.', ''), number_format(($artikul->getCena_promo() + $additional_price), 2, '.', '')),
                    'delivery' => outputDelivery($artikul, ($artikul->getCena() + $additional_price), ($artikul->getCena_promo() + $additional_price)),
                    'quantity' => $artikul->quantityByCode($code),
                    'code' => $code,
                ];

                echo json_encode($result);
            } else {
                $result = [
                    'price' => outputPrice(number_format(($artikul->getCena() + $additional_price), 2, '.', '')),
                    'delivery' => outputDelivery($artikul, ($artikul->getCena() + $additional_price)),
                    'quantity' => $artikul->quantityByCode($code),
                    'code' => $code,
                ];

                echo json_encode($result);
            }
        } else {
            if (!empty($promo_price)) {
                $result = [
                    'price' => outputPrice(number_format(($price_regular + $additional_price), 2, '.', ''), number_format(($promo_price + $additional_price), 2, '.', '')),
                    'delivery' => outputDelivery($artikul, ($price_regular + $additional_price), ($promo_price + $additional_price)),
                    'quantity' => $artikul->quantityByCode($code),
                    'code' => $code,
                ];

                echo json_encode($result);
            } else {
                $result = [
                    'price' => outputPrice(number_format(($price_regular + $additional_price), 2, '.', '')),
                    'delivery' => outputDelivery($artikul, ($price_regular + $additional_price)),
                    'quantity' => $artikul->quantityByCode($code),
                    'code' => $code,
                ];

                echo json_encode($result);
            }
        }
    }
}

function outputEndUserPrice($artikul)
{
    global $pdo;
    $product_id = $_POST['product_id'];
    $tag_id = $_POST['tag_id'];
    $additional_price = $_POST['additional_price'];

    $stm = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id`=?");
    $stm->bindValue(1, $product_id, PDO::PARAM_INT);
    $stm->bindValue(2, $tag_id, PDO::PARAM_INT);
    $stm->execute();

    foreach ($stm->fetchAll() as $price) {
        $promo_price = (double)$price['promo_price'];
        $price_regular = (double)$price['price'];
    }

    if (empty($price_regular) && empty($promo_price)) {
        if ($artikul->getCena_promo() > 0) {
            $price = number_format(($artikul->getCena_promo() + $additional_price), 2, '.', '');
            $tmp_price = explode(".", $price);
        } else {
            $price = number_format(($artikul->getCena() + $additional_price), 2, '.', '');
            $tmp_price = explode(".", $price);
        }
    } else {
        if (!empty($promo_price)) {
            $price = number_format(($promo_price + $additional_price), 2, '.', '');
            $tmp_price = explode(".", $price);
        } else {
            $price = number_format(($price_regular + $additional_price), 2, '.', '');
            $tmp_price = explode(".", $price);
        }
    }

    if (isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
        $user = new user((int)$_COOKIE['username_id']);

        if ($user->is_partner()) {
            return '<div style="width: 175px;margin-top: 5px;margin-bottom: 5px; margin-right: 25px; font-size:8pt; color: #424242;">Краен клиент: ' . $tmp_price[0] . ' <sup>' . $tmp_price[1] . '</sup> <span style="font-size:10px;">лв.</span></div>';
        }
    }

    return '';
}

function outputPrice($price, $promo = '')
{
    $tmp_price = explode(".", $price);
    if ($promo != "") {
        $tmp_promo = explode(".", $promo);
        $html = '<span class="prodpricenormal" style="float:none;color:#424242;font-size:12pt;font-style: italic;text-align: center;"> ' . $tmp_price[0] . ' <sup style="font-size:10pt">' . $tmp_price[1] . '</sup><span style="font-size:10px;">лв.</span></span>
              <span class="prodpricepromo" style="font-size:20px;font-style: italic; color: #FB671F;text-align: center;">' . $tmp_promo[0] . ' <sup style="font-size:12px">' . $tmp_promo[1] . '</sup><span style="font-size:10px;">лв.</span></span>';
    } else {
        if (isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
            $user = new user((int)$_COOKIE['username_id']);

            if ($user->is_partner()) {
                $html = '<span class="prodpricepromo" style="font-size:20px;font-style: italic; color: #424242;text-align: center;">' . $tmp_price[0] . ' <sup style="font-size:12px">' . $tmp_price[1] . '</sup><span style="font-size:10px;">лв.</span></span>';
            } else {
                $html = '<span class="prodpricepromo" style="font-size:20px;font-style: italic; color: #424242;text-align: center;">' . $tmp_price[0] . ' <sup style="font-size:12px">' . $tmp_price[1] . '</sup><span style="font-size:10px;">лв.</span></span>';
            }
        } else {
            $html = '<span class="prodpricepromo" style="font-size:20px;font-style: italic; color: #424242;text-align: center;">' . $tmp_price[0] . ' <sup style="font-size:12px">' . $tmp_price[1] . '</sup><span style="font-size:10px;">лв.</span></span>';
        }
    }

    if ($promo != "") {
        $tmpraz = (($price) - ($promo));
        $tmp_razf = explode(".", number_format($tmpraz, 2, '.', ''));

        $html .= '<div style="clear:both;"></div>';
        $html .= '<div style="width: 175px;text-align: center;">';
        $html .= '<span style="font-style: normal;font-size: 8pt;color: #FF671F;">спестявате ' . lang_currency_prepend . '' . $tmp_razf[0] . '<sup>' . $tmp_razf[1] . '</sup> ' . lang_currency_append . ' (-' . round((($tmpraz / ($price)) * 100), 0) . '%)</span>';
        $html .= '</div>';
    }

    return $html;
}

function outputDelivery($artikul, $price, $promo = '')
{
    global $pdo;
    $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
    $ds->execute();
    $d = $ds->fetch();
    $dostavka_cena = $d['delivery_price'];
    $dostavka_free = $d['free_delivery'];
    $additional_delivery = $artikul->getAdditionalDelivery();
    $dop_artikuli = $artikul->getAddArtikuli();

    if ($dop_artikuli != "") {
        $dop_art_arr = explode(",", $dop_artikuli);
        foreach ($dop_art_arr as $dart) {
            $dr = new artikul((int)$dart);
            if ($dr->isAvaliable()) {
                $additional_delivery += $dr->getAdditionalDelivery();
            }
        }
    }

    if ($promo != "") {
        if ($additional_delivery != '' && $additional_delivery > 0) {
            if ($dostavka_free < $promo) {
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">доставка: ' . $additional_delivery . ' лв.</div>';
            } else {
                $total_delivery = $dostavka_cena + $additional_delivery;
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">доставка: ' . $total_delivery . ' лв.</div>';
            }
        } else {
            if ($dostavka_free < $promo) {
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">с безплатна доставка</div>';
            } else {
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">доставка: ' . $dostavka_cena . ' лв.</div>';
            }
        }
    } else {
        if ($additional_delivery != '' && $additional_delivery > 0) {
            if ($dostavka_free < $price) {
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">доставка: ' . $additional_delivery . ' лв.</div>';
            } else {
                $total_delivery = $dostavka_cena + $additional_delivery;
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">доставка: ' . $total_delivery . ' лв.</div>';
            }
        } else {
            if ($dostavka_free < $price) {
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">с безплатна доставка</div>';
            } else {
                return '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080; width: 175px; text-align: center;">доставка: ' . $dostavka_cena . ' лв.</div>';
            }
        }
    }
}