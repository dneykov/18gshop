<?php
	require '../__top.php';
	$broi = (int)$_POST['broi'];
	$vid = (int)$_POST['vid'];

	$rezultati = 0;
	$paketi = array();
	$stm = $pdo->prepare('SELECT `group_id` FROM `group_products_zapisi` WHERE `vid_id` = ? ORDER BY id DESC');
	$stm->bindValue(1, $vid, PDO::PARAM_INT);
	$stm -> execute();
	if($stm->rowCount() > 0){
		$rez = $stm -> fetchAll();
		shuffle($rez);
		foreach ($rez as $g) {
			if(!in_array($g['group_id'], $paketi)&&(count($paketi) < $broi)){
				$paketi[] = $g['group_id'];
				if(count($paketi) == $broi){
					break;
				}
			}
		}
		if($paketi[0] != null){ ?>
			<div style="padding-left:10px;padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;color:#29a8df">пакетни оферти</div>
			<div style="clear:both"></div>
		<?php }
		foreach ($paketi as $n) {
			$v = new paket((int)$n);
			?>
			<div class="proddiv" style="border-bottom: none;">
				<a href="<?php echo url; ?>index.php?gid=<?php echo $v->getId(); ?>">
					<div class="promo">
                        <div style="background-color: #FF671F; width: 16px; height: 40px; color: #fff;">
                            <span class="rotate-90"><?php echo "-".round($v->getPercent(),0)."%"; ?></span>
                        </div>
					</div>
			        <div class="prodimgdiv">
						<table class='imageCenter'><tr><td><img alt="" src="<?php echo url.$v->getKartinka_t(); ?>" border="0" /></a></td></tr></table>

					</div>
					<div class="prod1" style="padding-left:15px;">
		                <div class="prodnamebox">
							 <span class="prodcat"><?php echo $v->getName(); ?></span>
		                </div>
						<div class="prodpricebox">
							<span class="prodpricenormal"> <span style="color:#808080;font-size:12pt;font-style: italic;"> <?php $tmp_cen=explode(".",number_format($v->getPrice_normal(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span> </span>
							<span class="prodpricepromo" style="font-size:15pt;font-style: italic;color:#ff671f;"><?php $tmp_cen=explode(".",number_format($v->getPrice(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
						</div>
					</div>
				</a>
			</div>
		<?php
		$rezultati++;
		}
	}
?>
<div style="clear:both"></div>
