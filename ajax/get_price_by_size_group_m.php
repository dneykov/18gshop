<?php
require_once '../main.php';
if(isset($_POST['gid']) && is_array($_POST['price_tag'] )) {
    if (isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
        $user = new user((int)$_COOKIE['username_id']);
    } else {
        $user = NULL;
    }


    $group = new paket((int) $_POST['gid']);
    $price_tags = $_POST['price_tag'];

    if (is_null($group)) return;

    $normal_price = explode(".", number_format($group->getPrice_normal($price_tags, $user), 2, '.', ''));
    $promo_price = explode(".", number_format($group->getPrice($price_tags, $user), 2, '.', ''));

    $price = '<span class="prodpricenormal" style="float:none;margin-right:10px;"><span style="color:#424242;font-size:12pt;font-style: italic;">' . lang_currency_prepend . ' ' . $normal_price[0] . ' <sup style="font-size:10pt">' . $normal_price[1] . '</sup><span style="font-size:10px;">' . lang_currency_append . '</span></span></span>
          <span class="prodpricepromo" style="font-style: italic; color: #FF671F;">' . lang_currency_prepend . ' ' . $promo_price[0] . ' <sup style="font-size:12px">' . $promo_price[1] . '</sup><span style="font-size:10px;">' . lang_currency_append . '</span></span>';

    if(isset($user) && $user->is_partner()) {
        $tmp_cen=explode(".",number_format($group->getPrice_reference($price_tags, $user), 2, '.', ''));
        $price .= '<div style="margin-top: 5px; margin-bottom: 5px; width: 175px; text-align: center; margin-right: 25px; font-size:8pt; color: #424242;">' . lang_enduser_ref_price . ' ' . $tmp_cen[0] . ' <sup>' .  $tmp_cen[1] .'</sup> <span style="font-size:10px;">' . lang_currency_append . '</span></div>';
    }

    $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
    $ds->execute();
    $d = $ds->fetch();
    $dostavka_cena = $d['delivery_price'];
    $dostavka_free = $d['free_delivery'];
    $additional_delivery = 0;

    foreach( $group->getProducts() as $d) {
        $group_article = new artikul((int)$d);
        if ($group_article->isAvaliable()) {
            $additional_delivery += $group_article->getAdditionalDelivery();
        }
    }

    $delivery = 0;
    $ac = 0;
    $ac = $group->getPrice($price_tags, $user);
    if($dostavka_free<$ac){
        if ($additional_delivery != '' && $additional_delivery > 0) {
            $delivery=  '<div style="float: right; text-align: center;width: 175px; margin-top: 5px; font-size:8pt; color: #808080;">' . lang_order_delivery . ': ' . lang_currency_prepend . $additional_delivery.' ' . lang_currency_append . '</div>';
        } else {
            $delivery = '<div style="float: right; text-align: center;width: 175px; margin-top: 5px; font-size:8pt; color: #808080;">' . with_free_delivery . '</div>';
        }
    } else {
        if ($additional_delivery != '' && $additional_delivery > 0) {
            $total_delivery = $dostavka_cena + $additional_delivery;
            $delivery = '<div style="float: right; text-align: center;width: 175px; margin-top: 5px; font-size:8pt; color: #808080;">' . lang_order_delivery . ': ' . lang_currency_prepend . $total_delivery.' ' . lang_currency_append . '</div>';
        } else {
            $delivery = '<div style="float: right; text-align: center;width: 175px; margin-top: 5px; font-size:8pt; color: #808080;">' . lang_order_delivery . ': ' . lang_currency_prepend . $dostavka_cena.' ' . lang_currency_append . '</div>';
        }
    }

    $result = [
        'price'    => $price,
        'delivery' => lang_directory == 'bg' ? $delivery : '',
    ];

    echo json_encode($result);
}