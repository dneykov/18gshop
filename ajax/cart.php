<?php

require '../__top.php';

if ($__user == NULL) {
    greshka('nelognat cart', '', true);
    die();
}


$artikul = new artikul((int)$_GET['cart_id']);

if (!isset ($_GET['action'])) {
    greshka('cart action not set', '', true);
    die();
}

$action = (int)$_GET['action'];


if ($action == 1) {


    if ($_GET['cart_tag']) {
        $tags = $_GET['cart_tag'];
    } else $tags = NULL;

    $temp_dropdown_text = '';

    if ($artikul->getTaggroup_dropdown()) foreach ($artikul->getTaggroup_dropdown() as $v) {

        $temp_ima_li_selectiran_dropdown = false;

        foreach ($v->getEtiketi() as $e) {

            if (in_array($e->getId(), $tags)) {

                //echo $e->getIme().'<br>';

                $temp_dropdown_text .= $v->getIme() . ' = ' . $e->getIme() . PHP_EOL;


                $temp_ima_li_selectiran_dropdown = true;
                break;

            }


        }


        if ($temp_ima_li_selectiran_dropdown == false) {

            die('nqma select be ');


        }


    }


    $stm = $pdo->prepare('INSERT INTO `koshnica`(`id_user`, `id_produkt`, `text_info`)
        VALUES (:id_user, :id_produkt, :text_info)');

    $stm->bindValue(':id_user', $__user->getId(), PDO::PARAM_INT);
    $stm->bindValue(':id_produkt', $artikul->getId(), PDO::PARAM_INT);
    $stm->bindValue(':text_info', $temp_dropdown_text);
    $stm->execute();


    echo $temp_dropdown_text;

} // action 1


else if ($action == 2) { // какво има в торбата


    $stm = $pdo->prepare('SELECT * FROM `koshnica` WHERE `id_user` = :id_user ORDER BY `id` ');
    $stm->bindValue(':id_user', $__user->getId(), PDO::PARAM_INT);
    $stm->execute();

    if ($stm->rowCount() == 0) {

        echo 'nqma';

    } else {

        header("content-type: text/xml");

        echo '<?xml version="1.0" encoding="utf-8" ?>
            <artikuli>';

        foreach ($stm->fetchAll() as $v) {

            $temp_artikul = new artikul((int)$v['id_produkt']);
            echo '<artikul

            id_artikul = "' . $temp_artikul->getId() . '"
            name = "' . htmlspecialchars($temp_artikul->getIme()) . '"
            url  = "' . htmlspecialchars(url . '?id=' . $temp_artikul->getId()) . '"
            url_delete = "' . addslashes(htmlspecialchars(url . 'ajax/cart.php?action=3&cart_id=' . $artikul->getId() . '&cart_delete=' . $v['id'])) . '"';


            echo '></artikul>';


            //echo $v['text_info'];

            //echo '<hr>';

        }

        echo '</artikuli>';

    } // else na rowcount == 0


} // action 2

else if ($action == 3) {

    $za_iztirvane = (int)$_GET['cart_delete'];

    $stm = $pdo->prepare('DELETE FROM `koshnica` WHERE `id_user` = :id_user AND `id` = :id ');
    $stm->bindValue(':id_user', $__user->getId(), PDO::PARAM_INT);
    $stm->bindValue(':id', $za_iztirvane, PDO::PARAM_INT);

    $stm->execute();

    if ($stm->rowCount() == 0) {
        greshka('iztrivan na iztit produkt', '', true);
        die('1');

    }
}
