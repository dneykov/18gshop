<?php
	require '../__top.php';
	$broi = (int)$_POST['broi'];

	$catSeen = array();
	if(isset($_COOKIE["CatSeen"])){
		$catSeen=unserialize(stripslashes(rawurldecode($_COOKIE["CatSeen"])));
	}

	$n = count($catSeen);
	arsort($catSeen);
	$loops = 0;
	$kat_broi = 0;
	switch ($n) {
		case 1: $loops=1;
				$kat_broi=$broi;
				break;
		case 2: $loops=2;
				$kat_broi=(int)($broi/2);
				break;
		case 3: $loops=3;
				$kat_broi=(int)($broi/3);
				break;
		default:$loops=3;
				$kat_broi=(int)($broi/3);
				break;
	}
	$fn = 1;
	$minati = 1;
	$done = false;
	foreach ($catSeen as $key => $value) {
		$stm = $pdo->prepare('SELECT `id` FROM `artikuli` WHERE `id_vid` = ? AND `isOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 36');
		$stm->bindValue(1, (int)$key, PDO::PARAM_INT);
		$stm -> execute();
		if($stm->rowCount() > 0){
			$rez = $stm -> fetchAll();
			shuffle($rez);
			if($fn == $loops && $fn > 1){
				if($loops*$kat_broi != $broi){
					$rez = array_slice($rez, 0, $broi+1);
				} else {
					$rez = array_slice($rez, 0, $kat_broi);
				}
			} else {
				$rez = array_slice($rez, 0, $kat_broi);
			}
			foreach ($rez as $n)
			{
				$v = new artikul((int)$n['id']);

                if(!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                $style2 = 'margin-right: 55px;';
                if($minati >= 4) {
                    $style2 = 'margin-right: 0;';
                }
				?>
				<div class="proddiv" style="border-bottom: none; <?php echo $style2; ?>">
					<a href="<?php echo url; ?>index.php?id=<?php echo $v->getId()."&category=".$v->getId_kategoriq()."&mode=".$v->getId_vid(); ?>">
						<?php
							$brnd = $v->getBrand();
							if($brnd->getBWImage() != ""){
								?>
								<div class="brand_image"><img src="<?php echo $brnd->getBWImage();?>" alt="<?php echo $v->getIme();?>" class="brand_img"/></div>
								<?php
							}
						?>
					<div class="promo">
						<?php
                        if($v->getCena_promo()){
                            $tmpraz=($v->getCena() - $v->getCena_promo() );
                            if($v->IsNew()){
                                echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff;position:absolute;right: 20px; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                            } else {
                                echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                            }

                        }

                        if($v->getAddArtikuli() != ""){
                            echo '<div style="float: left; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <img src="'.url.'images/icons/gift.png" alt="promo" />
                                    </div>';
                        }

						if($v->isPreorder() != ""){
                            echo '<div style="float: left; background-color: #29AC92; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90">PRE</span>
                                    </div>';
						}

						if($v->IsNew()){
                            echo '<div style="float: left; background-color: #01A0E2; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90" style="font-size: 11px; padding-top: 17px;">НОВО</span>
                                    </div>';
						}
						?>
					</div>
			        <div class="prodimgdiv">
						<table class='imageCenter'><tr><td><img alt="" src="<?php echo url.$v->getKartinka_t(); ?>" border="0" /></td></tr></table>

					</div>

						<div class="prod1" style="padding-left:15px;">
			                <div class="prodnamebox">
								 <span class="prodname"><?php echo $v->getIme_marka(); ?></span>
								 /
								 <span class="prodcat"><?php echo $v->getIme(); ?></span>
			                </div>

			                     <!--<div ><font class="prodcode"><?php echo $v->getKod(); ?></font></div>-->
                            <div class="prodpricebox">
                                <?php if(isset($__user) && $__user->is_partner()) : ?>
                                    <?php if($v->getCenaDostavna() > 0) : ?>
                                        <span class="prodpricepromo" style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen=explode(".",number_format($v->getCenaDostavna(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                    <?php else : ?>
                                        <?php if($v->getCena_promo()) : ?>
                                            <span class="prodpricenormal"> <span style="color:#808080;font-size:12pt;font-style: italic;"> <?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span> </span>
                                            <span class="prodpricepromo" style="font-size:15pt;font-style: italic;color:#ff671f;"><?php $tmp_cen=explode(".",number_format($v->getCena_promo(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                        <?php else : ?>
                                            <span class="prodpricepromo" style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <?php if($v->getCena_promo()) : ?>
                                        <span class="prodpricenormal"> <span style="color:#808080;font-size:12pt;font-style: italic;"> <?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span> </span>
                                        <span class="prodpricepromo" style="font-size:15pt;font-style: italic;color:#ff671f;"><?php $tmp_cen=explode(".",number_format($v->getCena_promo(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                    <?php else : ?>
                                        <span class="prodpricepromo" style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
						</div>
					</a>
				</div>
				<?php
				if($minati==$broi){
					$done = true;
					break;
				}
				$minati++;
			}
		}
		if($done) break;
		if($fn == $loops) break;
		$fn++;
	}
?>
<div style="clear:both"></div>
