<?php
	require '../__top.php';
	$broi = (int)$_POST['broi'];
    $products = null;

	if(isset($_COOKIE["CatSeen"])){
		$catSeen = array();
		$catSeen = unserialize(stripslashes(rawurldecode($_COOKIE["CatSeen"])));
		arsort($catSeen);
		$n = count($catSeen);
		$loops = 0;
		$kat_broi = 0;
		switch ($n) {
			case 1: $loops=1;
					$kat_broi=$broi;
					break;
			case 2: $loops=2;
					$kat_broi=(int)($broi/2);
					break;
			case 3: $loops=3;
					$kat_broi=(int)($broi/3);
					break;
			default:$loops=3;
					$kat_broi=(int)($broi/3);
					break;
		}
	}

	if(isset($catSeen)){
		$fn = 0;
		$rezultati = 0;
		$done = false;
		$products=null;
		$ids = array();
		foreach ($catSeen as $key => $value) {
			$stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND `id_vid` = ? AND `isOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 20');
			$stm->bindValue(1, (int)$key, PDO::PARAM_INT);
			$stm -> execute();
			$rez = $stm -> fetchAll();
			shuffle ($rez);
			if($fn == 0){
				if($loops*$kat_broi != $broi){
					$rezane = $broi-($loops*$kat_broi);
					$rez = array_slice($rez, 0, $kat_broi+$rezane);
				} else {
					$rez = array_slice($rez, 0, $kat_broi);
				}
			} else {
				$rez = array_slice($rez, 0, $kat_broi);
			}
			foreach ($rez as $n)
			{
                $artk = new artikul($n);
                if(!isset($__user) && $artk->getCenaDostavna() > 0 && $artk->getCena() == 0) continue;

				$products[]= new artikul($n);
				$rezultati++;
				$ids[] = $n;
				if($rezultati==$broi){
					$done = true;
					break;
				}
			}
			$fn++;
			if($done) break;
			if($fn == $loops) break;
		}
		if($rezultati != $broi){
			$stmm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND `isOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 50');
			$stmm -> execute();
			$rez = $stmm -> fetchAll();
			shuffle ($rez);
			foreach ($rez as $n)
			{
                $artk = new artikul($n);
                if(!isset($__user) && $artk->getCenaDostavna() > 0 && $artk->getCena() == 0) continue;

				if(!in_array($n, $ids)){
					$products[]= new artikul($n);
					$rezultati++;
				}
				if($rezultati==$broi){
					break;
				}
			}
		}
        if(is_null($products) && count($products) == 0) return;
		shuffle($products);
	}else{
		$stmpromo = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND `isOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 36');
		$stmpromo -> execute();
		$rezpromo = $stmpromo -> fetchAll();
		shuffle ($rezpromo);
        foreach ($rezpromo as $n)
        {
            $artk = new artikul($n);
            if(!isset($__user) && $artk->getCenaDostavna() > 0 && $artk->getCena() == 0) continue;
            $products[]= new artikul($n);
        }

        if(is_null($products) && count($products) == 0) return;
        $products = array_slice($products, 0, $broi);
	}
	if($products != null) {
		foreach($products as $v)
		{
			?>
			<div class="proddiv" style="border-bottom: none;">
			<a href="<?php echo url; ?>index.php?id=<?php echo $v->getId()."&category=".$v->getId_kategoriq()."&mode=".$v->getId_vid(); ?>">
				<?php
					$brnd = $v->getBrand();
					if($brnd->getBWImage() != ""){
						?>
						<div class="brand_image"><img src="<?php echo $brnd->getBWImage();?>" alt="<?php echo $v->getIme();?>" class="brand_img"/></div>
						<?php
					}
				?>
				<div class="promo">
					<?php

						if($v->isPreorder() != ""){
                            echo '<div style="background-color: #29AC92; width: 16px; height: 40px; color: #fff;">
                                    <span class="rotate-90">PRE</span>
                                    </div>';
						}
                    $tmpraz=($v->getCena() - $v->getCena_promo() );

					?>
                    <div style="background-color: #FF671F; width: 16px; height: 40px; color: #fff;">
                        <span class="rotate-90"><?php echo "-".round((($tmpraz/$v->getCena())*100),0)."%"; ?></span>
                    </div>
				</div>
		        <div class="prodimgdiv">
					<table class='imageCenter'><tr><td><img alt="" src="<?php echo url.$v->getKartinka_t(); ?>" border="0" /></a></td></tr></table>

				</div>

					<div class="prod1" style="padding-left:15px;">
		                <div class="prodnamebox">
							 <span class="prodname"><?php echo $v->getIme_marka(); ?></span>
							 /
							 <span class="prodcat"><?php echo $v->getIme(); ?></span>
		                </div>

		                     <!--<div ><font class="prodcode"><?php echo $v->getKod(); ?></font></div>-->
                        <div class="prodpricebox">
                            <?php if(isset($__user) && $__user->is_partner()) : ?>
                                <?php if($v->getCenaDostavna() > 0) : ?>
                                    <span class="prodpricepromo" style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen=explode(".",number_format($v->getCenaDostavna(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                <?php else : ?>
                                    <?php if($v->getCena_promo()) : ?>
                                        <span class="prodpricenormal"> <span style="color:#808080;font-size:12pt;font-style: italic;"> <?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span> </span>
                                        <span class="prodpricepromo" style="font-size:15pt;font-style: italic;color:#FF671F;"><?php $tmp_cen=explode(".",number_format($v->getCena_promo(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                    <?php else : ?>
                                        <span class="prodpricepromo" style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else : ?>
                                <?php if($v->getCena_promo()) : ?>
                                    <span class="prodpricenormal"> <span style="color:#808080;font-size:12pt;font-style: italic;"> <?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span> </span>
                                    <span class="prodpricepromo" style="font-size:15pt;font-style: italic;color:#FF671F;"><?php $tmp_cen=explode(".",number_format($v->getCena_promo(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                <?php else : ?>
                                    <span class="prodpricepromo" style="margin-top:8px;font-size:15pt;font-style: italic;"><?php $tmp_cen=explode(".",number_format($v->getCena(), 2, '.', '')); echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span style="font-size:10px;">лв.</span></span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
					</div>
				</a>
			</div>
			<?php
		}
	}
?>
