<?php
/* @var $pdo pdo */
/* @var $stm PDOStatement */

mb_internal_encoding("UTF-8");


function get_pat()
{

    $pdo = PDOX::vrazka();

    function url(&$url, $a)
    {
        if ($url) $url = $url . '&' . $a;
        else {
            if (defined('__current_url_without_query')) $url = __current_url_without_query . '?' . $a;
            else $url = '?' . $a;
        }
    }// funct


    $data = NULL;
    $containsVirtualVid = false;

    $url = NULL;


    if (isset ($_GET['category'])) {


        $kategoriq = (int)$_GET['category'];

        $stm = $pdo->prepare('SELECT * FROM `kategorii` WHERE `id` = ? LIMIT 1 ');
        $stm->bindValue(1, $kategoriq, PDO::PARAM_INT);
        $stm->execute();

        if ($stm->rowCount() == 0) greshka('Пат грешка категория');

        $temp = new kategoriq($stm->fetch());
        url($url, 'category=' . $temp->getId());

        $data[] = array('var' => 'category', 'ime' => $temp->getIme(), 'id' => $temp->getId(), 'url' => $url);

        unset($temp);

    }

    if (isset ($_GET['mode'])) {
        $vid = (int)$_GET['mode'];
        $stm = $pdo->prepare('SELECT * FROM `vid` WHERE `id` = ? AND `id_kategoriq` = ? LIMIT 1 ');
        $stm->bindValue(1, $vid, PDO::PARAM_INT);
        $stm->bindValue(2, $kategoriq, PDO::PARAM_INT);
        $stm->execute();

        if ($stm->rowCount() == 0) greshka('Пат грешка вид');

        $temp = new vid($stm->fetch());
        $containsVirtualVid = $temp->isVirtual();

        url($url, 'mode=' . $temp->getId());
        $data[] = array('var' => 'mode', 'ime' => $temp->getIme(), 'id' => $temp->getId(), 'url' => $url);

        unset($temp);
    }

    if (isset ($_GET['model'])) {
        $model = (int)$_GET['model'];
        if ($containsVirtualVid) {
            $stm = $pdo->prepare('SELECT * FROM `model` WHERE `id` = ? LIMIT 1 ');
        } else {
            $stm = $pdo->prepare('SELECT * FROM `model` WHERE `id` = ? AND `id_kategoriq` = ? LIMIT 1 ');
        }
        $stm->bindValue(1, $model, PDO::PARAM_INT);
        if (!$containsVirtualVid) {
            $stm->bindValue(2, $kategoriq, PDO::PARAM_INT);
        }
        $stm->execute();

        if ($stm->rowCount() == 0) greshka('Пат грешка модел');

        $temp = new model(($stm->fetch()));

        url($url, 'model=' . $temp->getId());
        $data[] = array('var' => 'mode', 'ime' => $temp->getIme(), 'id' => $temp->getId(), 'url' => $url);

        unset($temp);


    }

    if (isset ($_GET['tag']) && is_array($_GET['tag'])) {
        foreach ($_GET['tag'] as $v) {

            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ');
            $stm->bindValue(1, $v, PDO::PARAM_INT);
            $stm->execute();

            if ($stm->rowCount() == 0) greshka('Пат грешка етикет');

            $temp_etiket = new etiket($stm->fetch());
            $temp_grupa = new etiket_grupa((int)$temp_etiket->getId_etiketi_grupa());

            if (isset($vid) && $temp_grupa->getId_vid() != $vid) greshka('Грешан път тагрупа категория');

            url($url, 'tag[]=' . $temp_etiket->getId());
            $data[] = array('var' => 'tag[]', 'ime' => $temp_etiket->getIme(), 'id' => $temp_etiket->getId(), 'url' => $url);

            unset($temp_etiket, $temp_grupa);

        }

    }


    return $data;

}

############ krai pat


function greshka($text, $text_display = 'Възникна грешка, съжаляваме, оптайте пак по-късно', $skrita_gre6ka = true, $zapisvai = true)
{

    $text .= $text_display . '<br><br>';

    $text .= '<br>REQUEST<br>' . nl2br(htmlspecialchars(print_r($_REQUEST, 1)));
    $text .= '<br>GET<br>' . nl2br(htmlspecialchars(print_r($_GET, 1)));
    $text .= '<br>POST<br>' . nl2br(htmlspecialchars(print_r($_POST, 1)));
    $text .= '<br>SERVER<br>' . nl2br(htmlspecialchars(print_r($_SERVER, 1)));
    $text .= '<br>FILES<br>' . nl2br(htmlspecialchars(print_r($_FILES, 1)));

    $text = preg_replace('|  |', '&nbsp;&nbsp;', $text);

    if ($skrita_gre6ka == true) {
        error_log($text);
        return;
    }

    ?>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title></head>
<body>
    <?php
    echo $text_display;
    if ($text == NULL) die(); // ako ne e zadaden tekst za gre6kata => e syob6tenie samo

    ?><br><br>Изникна грешка, всичко е записано, извинете за нудобството;<br>
    <?php
    echo $text;
    die();

}


require 'klasove/artikul.php';
require 'klasove/kategoriq.php';
require 'klasove/vid.php';
require 'klasove/etiket.php';
require 'klasove/etiket_zapis.php';
require 'klasove/model.php';
require 'klasove/etiket_grupa.php';
require 'klasove/kartinka.php';
require 'klasove/user.php';
require 'klasove/laguages.php';
require 'klasove/advertise.php';
require 'klasove/news.php';
require 'klasove/order.php';
require 'klasove/group.php';
require 'klasove/Browser.php';
require 'klasove/artikul_admin.php';
require 'klasove/kategoriq_admin.php';
require 'klasove/vid_admin.php';
require 'klasove/etiket_admin.php';
require 'klasove/model_admin.php';
require 'klasove/etiket_grupa_admin.php';
require 'klasove/kartinka_admin.php';
require 'klasove/UpgradeGroup.php';
require 'klasove/UpgradeOption.php';
//require 'klasove/specification.php';


require("klasove/PHPMailer_v5.1/class.phpmailer.php");


$__languages_all = NULL;
$__languages = NULL;
$stm = $pdo->prepare('SELECT * FROM `languages` ORDER BY `default` DESC, `enabled` DESC, `order` ');
$stm->execute();
if ($stm->rowCount() > 0) foreach ($stm->fetchAll() as $v) {
    if ($v['enabled'] == 1) $__languages[] = new language($v);
    $__languages_all[] = new language($v);

}


if (!defined('dir_root_admin')) require '__members.php';


function po_taka_mail($za, $text, $tema, $ot = mail_office, $is_coded = false)
{

// $text = print_r($text);

    $mail = new PHPMailer();

//$mail->IsSMTP();                                      // set mailer to use SMTP
    $mail->Host = "localhost";  // specify main and backup server
    $mail->CharSet = "utf-8";

    $mail->From = $ot;
    $mail->FromName = "";
    $mail->AddAddress($za);
//$mail->AddAddress("ellen@example.com");                  // name is optional
//$mail->AddReplyTo("info@example.com", "Information");

    $mail->WordWrap = 50;                                 // set word wrap to 50 characters
//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
    $mail->IsHTML(TRUE);                                  // set email format to HTML

    $mail->Subject = $tema;

    if ($is_coded == false) {
        $mail->Body = nl2br(htmlspecialchars($text));
        $mail->AltBody = htmlspecialchars($text);
    } else {

        $mail->Body = $text;
        $mail->AltBody = $text;

    }


    if (!$mail->Send()) {
        greshka('Neizpraten mail<br>' . $mail->ErrorInfo);

    }


}


// pagination

function po_taka_pagination($page, $artikuli_broi, $broi_artikuli_na_stranica, $prefix, $nazad = NULL, $napred = NULL)
{
    $element_koito_ne_se_broqt = 0;
    $temp_next = NULL;
    $temp_previous = NULL;


    if ($prefix[0] == '&') $prefix[0] = '?';


    $pagination = NULL;

    if ($nazad && ($page >= 1)) {
        $element_koito_ne_se_broqt++;
        $temp_previous = array('url' => $prefix . ($page - 1), 'number' => $nazad, 'current' => false);
    }


    if ($napred && (($page + 1) * $broi_artikuli_na_stranica < $artikuli_broi)) {

        $temp_next = array('url' => $prefix . ($page + 1), 'number' => $napred, 'current' => false);
        $element_koito_ne_se_broqt++;
    }


    if (is_array($artikuli_broi)) {
        $temp = $artikuli_broi['broi'] / $broi_artikuli_na_stranica;
    } else {
        $temp = $artikuli_broi / $broi_artikuli_na_stranica;
    }
    $temp = ceil($temp);

    //   echo $artikuli_broi.' -- '.$broi_artikuli_na_stranica.' -- '.$temp.'<br>';


    $temp_start = $page - 4;
    if ($temp_start < 0) $temp_start = 0;


    $temp_br = 0;
    while ($temp > $temp_start) {


        if ($temp_start == $page) {
            $temp_current = 1;
        } else $temp_current = 0;
        $pagination[] = array('url' => $prefix . $temp_start, 'number' => $temp_start + 1, 'current' => $temp_current);

        $temp_br++;

        if ($temp_br == 10) break;

        $temp_start++;


    }


    $temp = 0;
    $pagination_temp = array();
    if ((sizeof($pagination) - $element_koito_ne_se_broqt) <= 9 && $page > 5) {
        // if( (sizeof($pagination) - $element_koito_ne_se_broqt ) <= 5 && $page > 5) {

        $temp_page = $page - 5;

        //echo $temp_br;

        while ($temp_page >= 0 && $temp_br <= 9) {


            //echo $temp_br.'www';

            if ($temp_page == $page) {
                $temp_current = 1;
            } else $temp_current = 0;

            $pagination_temp[] = array('url' => $prefix . $temp_page, 'number' => ($temp_page + 1), 'current' => $temp_current);


            $temp_page--;


            // new one
            $temp_br++;
            if ($temp_br >= 10) break;

            // old one
            //$temp++;
            //if($temp>4) break;
        }


    }


    $pagination_temp = array_reverse($pagination_temp);


    $pagination = array_merge($pagination_temp, $pagination);


//    foreach ($pagination as $v) {
//     // echo $v['number'].'['.$v['url'].'] '.$v['current'].' -- ';
//    }


    if ($temp_next) $pagination[] = $temp_next;
    if ($temp_previous) {
        $pagination = array_reverse($pagination);
        $pagination[] = $temp_previous;
        $pagination = array_reverse($pagination);
    }


    return $pagination;


} // function pagination


// krai pagination


function po_taka_url_append(&$url, $append, $add_paramether_symbil = true)
{

    if ($add_paramether_symbil) {
        if (preg_match("/\?/", $url)) {
            $url .= '&' . $append;
        } else $url .= '?' . $append;
    } else {
        $url .= $append;
    }

    return $url;

}

function virtual_product_suggestion($search, $vid)
{
    $return = '<?xml version="1.0" encoding="utf-8" ?>
<Modeli>';
    $pdo = PDOX::vrazka();
    $search = preg_replace('/[  |\t]/', ' ', $search);
    $search = explode(' ', $search);

    $sql = 'SELECT * FROM `artikuli` WHERE (';
    $where = '';

    foreach ($search as $k => $v) {
        $where .= ' LOWER(`cache_etiketi`) LIKE(LOWER( :text' . $k . ' ))  AND ';
    }

    $where = preg_replace('/^(.*)AND(.*)$/', '$1', $where);

    $sql .= $where;
    $sql .= ') AND `isOnline` = 1  AND `isAvaliable` = 1 AND `id` NOT IN (SELECT `artikul_id` FROM `product_vid` WHERE `vid_id` = :vid) ORDER BY `id` DESC LIMIT 25';
    $stm = $pdo->prepare($sql);

    foreach ($search as $k => $v) {
        $stm->bindValue(':text' . $k, '%' . $v . '%');
    }

    $stm->bindValue(':vid', $vid);
    $stm->execute();

    foreach ($stm->fetchAll() as $k => $v) {
        $temp = new artikul($v);

        ?><?php if ($k == 31) {


            $return .= '<nextpage code="' . htmlspecialchars(htmlspecialchars('linkzao6te&&')) . '">

    </nextpage>';

            break;
        }

        $return .= '<model
        name="' . htmlspecialchars($temp->getIme()) . '"
        image ="' . htmlspecialchars(url . $temp->getKartinka_t()) . '"
        category ="' . $temp->getId_kategoriq() . '"
        id="' . $temp->getId() . '"
        marka ="' . htmlspecialchars($temp->getIme_marka()) . '"
        >
    </model>';

    }

    $return .= '</Modeli>';

    return $return;

}


function po_taka_search_autocomplete_artikuli_admin($search, $url, $check_pemission = false)
{

    $return = '<?xml version="1.0" encoding="utf-8" ?>
<Modeli>';


    $pdo = PDOX::vrazka();


    $search = preg_replace('/[  |\t]/', ' ', $search);

    $search = explode(' ', $search);


    if ($check_pemission) {
//    $__user = __user_admin::singleton();
//    $__user->permission_check('продукти',$b='r',true, $no_echo = true);
    }

    $sql = 'SELECT * FROM `artikuli` WHERE (';

    $where = '';

    foreach ($search as $k => $v) {

        $where .= ' LOWER(`cache_etiketi`) LIKE(LOWER( :text' . $k . ' ))  AND ';

    }

    $where = preg_replace('/^(.*)AND(.*)$/', '$1', $where);

    $sql .= $where;


    if (isset($_GET['_'])) {
        $sql .= ') AND `isOnline` = 1  AND `isAvaliable` = 1 ORDER BY `id` DESC LIMIT 25';
    } else {
        $sql .= ') AND `isOnline` = 1 ORDER BY `id` DESC LIMIT 25';
    }

    $stm = $pdo->prepare($sql);

    foreach ($search as $k => $v) {

        $stm->bindValue(':text' . $k, '%' . $v . '%');

    }


    $stm->execute();
    foreach ($stm->fetchAll() as $k => $v) {
        $temp = new artikul($v);

        ?><?php if ($k == 31) {


            $return .= '<nextpage code="' . htmlspecialchars(htmlspecialchars('linkzao6te&&')) . '">

    </nextpage>';

            break;
        }

        $return .= '<model
        name="' . htmlspecialchars($temp->getIme()) . '"
        image ="' . htmlspecialchars(url . $temp->getKartinka_t()) . '"
        url="' . htmlspecialchars(htmlspecialchars($url . $temp->getId())) . '"
        id="' . $temp->getId() . '"
        marka ="' . htmlspecialchars($temp->getIme_marka()) . '"
        >
    </model>';

    }


    $return .= '</Modeli>';

    return $return;

}

function po_taka_search_autocomplete_artikuli($search, $url, $check_pemission = false)
{
    $pdo = PDOX::vrazka();
    $search = preg_replace('/[  |\t]/', ' ', $search);
    $search = explode(' ', $search);
    $sql = 'SELECT * FROM `artikuli` WHERE (';
    $sqlCategoriesFromResult = 'SELECT * FROM `artikuli` JOIN `kategorii` ON `artikuli`.`id_kategoriq` = `kategorii`.`id` WHERE (';
    $sqlCategories = 'SELECT * FROM `kategorii` WHERE (';
    $sqlModes = 'SELECT * FROM `vid` WHERE (';
    $where = '';
    $whereCategoriesFromResult = '';
    $whereModes = '';

    foreach ($search as $k => $v) {
        $where .= ' LOWER(`cache_search`) LIKE(LOWER( :text' . $k . ' ))  AND ';
        $whereCategoriesFromResult .= ' LOWER(`ime_' . lang_prefix . '`) LIKE(LOWER( :text' . $k . ' ))  AND ';
        $whereModes .= ' LOWER(`ime_' . lang_prefix . '`) LIKE(LOWER( :text' . $k . ' ))  AND ';
    }

    $where = preg_replace('/^(.*)AND(.*)$/', '$1', $where);
    $whereCategoriesFromResult = preg_replace('/^(.*)AND(.*)$/', '$1', $whereCategoriesFromResult);
    $whereModes = preg_replace('/^(.*)AND(.*)$/', '$1', $whereModes);
    $sql .= $where;
    $sqlCategoriesFromResult .= $where;
    $sqlCategories .= $whereCategoriesFromResult;
    $sqlModes .= $whereModes;
    $sql .= ') AND `isOnline` = 1 AND `isAvaliable` = 1 ORDER BY `id` DESC LIMIT 10';
    $sqlCategoriesFromResult .= ') AND `isOnline` = 1 AND `isAvaliable` = 1 GROUP BY `id_vid` ORDER BY `kategorii`.`ordr`';
    $sqlCategories .= ') ORDER BY `id` DESC';
    $sqlModes .= ') ORDER BY `id` DESC';

    $stmSearch = $pdo->prepare($sql);
    $stmCategoriesFromResult = $pdo->prepare($sqlCategoriesFromResult);
    $stmCategories = $pdo->prepare($sqlCategories);
    $stmModes = $pdo->prepare($sqlModes);

    foreach ($search as $k => $v) {
        $stmSearch->bindValue(':text' . $k, '%' . $v . '%');
        $stmCategoriesFromResult->bindValue(':text' . $k, '%' . $v . '%');
        $stmCategories->bindValue(':text' . $k, '%' . $v . '%');
        $stmModes->bindValue(':text' . $k, '%' . $v . '%');
    }

    $stmSearch->execute();
    $stmCategoriesFromResult->execute();
    $stmCategories->execute();
    $stmModes->execute();


    $return = '<?xml version="1.0" encoding="utf-8" ?><Search>';

    if ($stmCategoriesFromResult->rowCount() == 0 && $stmSearch->rowCount() == 0) {
        $return .= '<Kategorii>';
        foreach ($stmCategories->fetchAll() as $k => $v) {
            $temp = new kategoriq($v);

            $return .= '<kategoriq
        name="' . htmlspecialchars( $temp->getIme() ) . '"
        id="' . htmlspecialchars( $temp->getId() ) . '"
        search_term="' . htmlspecialchars( implode(" ", $search) ) . '"
        ></kategoriq>';
        }
        $return .= '</Kategorii>';

        $return .= '<Vidove>';
        foreach ($stmModes->fetchAll() as $k => $v) {
            $temp = new vid($v);

            $return .= '<vid
        name="' . htmlspecialchars( $temp->getIme() . " " . lang_in . " " . $temp->getIme_kategoriq()) . '"
        id="' . htmlspecialchars( $temp->getId() ) . '"
        category_id="' . htmlspecialchars( $temp->getId_kategoriq() ) . '"
        search_term="' . htmlspecialchars( implode(" ", $search) ) . '"
        ></vid>';
        }
        $return .= '</Vidove>';
    } else {
        $return .= '<Kategorii>';
        foreach ($stmCategoriesFromResult->fetchAll() as $k => $v) {
            $temp = new artikul($v);

            $return .= '<kategoriq
        name="' . htmlspecialchars( implode(" ", $search) . " " . lang_in . " " . $temp->getIme_kategoriq() . " " . $temp->getIme_vid() ) . '"
        id="' . htmlspecialchars( $temp->getId_kategoriq() ) . '"
        vid_id="' . htmlspecialchars( $temp->getId_vid() ) . '"
        search_term="' . htmlspecialchars( implode(" ", $search) ) . '"
        ></kategoriq>';
        }
        $return .= '</Kategorii>';

        $return .= '<Modeli>';

        foreach ($stmSearch->fetchAll() as $k => $v) {
            $temp = new artikul($v);
            $title = empty($temp->getTitle()) ? $temp->getIme_marka() : $temp->getTitle() . " " . $temp->getIme_marka();
            $return .= '<model
        name="' . htmlspecialchars( $title . " " . $temp->getIme()) . '"
        image ="' . htmlspecialchars(url . $temp->getKartinka_t()) . '"
        url="' . htmlspecialchars(htmlspecialchars($url . $temp->getId())) . '"
        id="' . $temp->getId() . '"
		marka ="' . htmlspecialchars($temp->getIme_marka()) . '"
        ></model>';

        }
        $return .= '</Modeli>';
    }

    $return .= '</Search>';

    return $return;
}

function curPageURL()
{
    $pageURL = 'http';
// if ($_SERVER["HTTPS"] == "on") {$pageURL = $pageURL."s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}


function check_email_address($email)
{
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        return false;
    } // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!preg_match("/^(([A-Za-z0-9!#$%&'*+=?^_`{|}~-][A-Za-z0-9!#$%&'*+=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
            return false;
        }
    }
    if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) {
        // Check if domain is IP. If not, it should be valid domain name
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
            return false;
            // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}


function remove_querystring_var($url, $varname)
{
    return preg_replace('/([?&])' . $varname . '=[^&]+(&|$)/', '$1', $url);
}

function isProductAvailable($productId)
{

    $sql = 'SELECT * FROM `artikuli` WHERE (`isDelete`=0 AND `id`=:id);';

    $pdo = PDOX::vrazka();
    $stm = $pdo->prepare($sql);
    $stm->bindValue(':id', $productId, PDO::PARAM_INT);
    $stm->execute();

    return $stm->rowCount();
}

function isProductOnline($productId)
{

    $sql = 'SELECT * FROM `artikuli` WHERE ( `isDelete`=0 AND IsOnline=1 AND `id`=:id);';

    $pdo = PDOX::vrazka();
    $stm = $pdo->prepare($sql);
    $stm->bindValue(':id', $productId, PDO::PARAM_INT);
    $stm->execute();

    return $stm->rowCount();
}


function geoCheckIP($ip)
{
    //check, if the provided ip is valid
    if (!filter_var($ip, FILTER_VALIDATE_IP)) {
        throw new InvalidArgumentException("IP is not valid");
    }

    //contact ip-server
    $response = @file_get_contents('http://www.netip.de/search?query=' . $ip);
    if (empty($response)) {
        throw new InvalidArgumentException("Error contacting Geo-IP-Server");
    }

    //Array containing all regex-patterns necessary to extract ip-geoinfo from page
    $patterns = array();
    $patterns["domain"] = '#Domain: (.*?)&nbsp;#i';
    $patterns["country"] = '#Country: (.*?)&nbsp;#i';
    $patterns["state"] = '#State/Region: (.*?)<br#i';
    $patterns["town"] = '#City: (.*?)<br#i';

    //Array where results will be stored
    $ipInfo = array();

    //check response from ipserver for above patterns
    foreach ($patterns as $key => $pattern) {
        //store the result in array
        $ipInfo[$key] = preg_match($pattern, $response, $value) && !empty($value[1]) ? $value[1] : 'not found';
    }

    return $ipInfo;
}


function show_add_dop_producti($id)
{

    $return = '<?xml version="1.0" encoding="utf-8" ?>
<Modeli>';


    $pdo = PDOX::vrazka();

    $id = (int)$id;


    $sql = 'SELECT * FROM `artikuli` WHERE `id` = ' . $id . ' LIMIT 1';
    $stm = $pdo->prepare($sql);
    $stm->execute();
    foreach ($stm->fetchAll() as $k => $v) {
        $temp = new artikul($v);
        $return .= '<model
        name="' . htmlspecialchars($temp->getIme()) . '"
        image ="' . htmlspecialchars(url . $temp->getKartinka_t()) . '"
        url="' . htmlspecialchars(htmlspecialchars(url . $temp->getId())) . '"
        id="' . $temp->getId() . '"
		marka ="' . htmlspecialchars($temp->getIme_marka()) . '"
        >
    </model>';
    }
    $return .= '</Modeli>';
    return $return;

}

function remove_tag($url, $tagId, $id = null)
{
    $to_return = null;
    $to_return = str_replace('&tag[]=' . $tagId, '', $url);
    $to_return = str_replace(url, '', $to_return);
    if (isset($id)) {
        $to_return = str_replace('&id=' . $id, '', $to_return);
    }
    return $to_return;
}

function parse_tags_url($etiket, $etiketi_used, $remove = false)
{
    $url_to_return = '';
    foreach ($etiketi_used as $e) {
        if ($etiket->getId_etiketi_grupa() <= $e->getId_etiketi_grupa()) {
            break;
        } else {
            $url_to_return .= '&tag[]=' . $e->getId();
        }
    }
    if (!$remove)
        $url_to_return .= '&tag[]=' . $etiket->getId();
    return $url_to_return;
}

function ip_address()
{
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                    return $ip;
                }
            }
        }
    }
}

function listDirectory($dir, $prefix = '')
{
    $dir = rtrim($dir, '\\/');
    $result = array();

    foreach (scandir($dir) as $f) {
        if ($f !== '.' and $f !== '..') {
            if (is_dir("$dir/$f")) {
                $result = array_merge($result, ListIn("$dir/$f", "$prefix$f/"));
            } else {
                $result[] = $prefix . $f;
            }
        }
    }

    return $result;
}
