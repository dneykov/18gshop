<?php
require_once 'main.php';

if (isset ($_GET['logout'])) {

    setcookie('username_id');
    setcookie('username_pass');

    header("Location: " . urlm . "/");

    exit;
}

$login_errors = NULL;
if (isset ($_POST['submit'])) {

    if (!isset ($_POST['login_mail'], $_POST['login_pass'])) $login_errors[] = lang_login_error_pass_or_mail;


    if ($login_errors == NULL) {

        $login_mail = $_POST['login_mail'];
        $login_mail = trim($login_mail);
        $login_pass = $_POST['login_pass'];
        $login_pass = trim($login_pass);

        $login_pass = sha1($login_pass);

        $stm = $pdo->prepare('SELECT * FROM `members` WHERE `mail` = ? AND `password` = ? LIMIT 1');
        $stm->bindValue(1, $login_mail, PDO::PARAM_STR);
        $stm->bindValue(2, $login_pass, PDO::PARAM_STR);
        $stm->execute();

        if ($stm->rowCount() == 0) $login_errors[] = lang_login_error_pass_or_mail;

        else {
            $row_member = $stm->fetch();

            $login_pass_cookie = sha1($login_pass . uniqid());

            $stm = $pdo->prepare('UPDATE `members` SET `password_coockie` = ?, `ip` = ? WHERE `id` = ? LIMIT 1');
            $stm->bindValue(1, $login_pass_cookie, PDO::PARAM_STR);
            $stm->bindValue(2, $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
            $stm->bindvalue(3, $row_member['id'], PDO::PARAM_INT);
            $stm->execute();

            setcookie('username_id', $row_member['id']);
            setcookie('username_pass', $login_pass_cookie);

            if (isset($_SESSION['cart']) && sizeof($_SESSION['cart']) > 0) {
                header("Location: fancy_login_m.php?p=cart");
            } else if (isset($_POST['toRedirect'])) {
                header("Location: " . $_POST['toRedirect']);
            } else {
                echo '<script>parent.location=parent.location;</script>';
            }

            exit;
        }
    }
}
$page_title = " - вход";
