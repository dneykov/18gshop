<?php
require_once 'main.php';
if(isset($_GET['del'])){
	unset($_SESSION['cart'][$_GET['del']]);

    if(isset($_SESSION['cart'])){
        $total_additional_delivery = 0;
        foreach ($_SESSION['cart'] as $key => $tmp_product){

            $total_additional_delivery += $tmp_product['additional_delivery'] * $tmp_product['count'];
        }

        $_SESSION['cart']['total_additional_delivery'] = $total_additional_delivery;

        if ($_SESSION['cart']['total_additional_delivery'] == 0) unset($_SESSION['cart']['total_additional_delivery']);
    }
}
if(isset($_POST['updateNumber'])){
    if(((int)$_POST['updateNumber'])<1){
        unset($_SESSION['cart'][$_POST['podID']]);
    }else{
        $_SESSION['cart'][$_POST['podID']]['count']=$_POST['updateNumber'];

        if(isset($_SESSION['cart'])){
            $total_additional_delivery = 0;
            foreach ($_SESSION['cart'] as $key => $tmp_product){

                $total_additional_delivery += $tmp_product['additional_delivery'] * $tmp_product['count'];
            }

            $_SESSION['cart']['total_additional_delivery'] = $total_additional_delivery;
        }
    }
    exit;
}

if(isset($_POST['addToCard'])){
    $options = array();	
    $products_info = array();
    $price_tag = 0;
    $products = "";
    $add_artikuli = "";
    $id = $_POST['addToCard'];

    if($_POST['type']=="product"){

        $product['additional_delivery'] = 0;

        $artikul = new artikul((int)$id);
        if(isset($_POST['option'.$id])){
            $options[]= $id.", ".$_POST['option'.$id];
        }

        if (isset($_POST['upgrade_option'])) {
            $product['upgrade_option'] = $_POST['upgrade_option'];
        }

        $products = $id;
        $products_info[$artikul->getId()]['marka'] = $artikul->getIme_marka();
        $products_info[$artikul->getId()]['model'] = $artikul->getIme();

        if(isset($_POST['price_tag']) && $_POST['price_tag'] != "") {
            $price_tag = $_POST['price_tag'];

            $stm = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id`=?");
            $stm -> bindValue(1, $id, PDO::PARAM_INT);
            $stm -> bindValue(2, $_POST['price_tag'], PDO::PARAM_INT);
            $stm->execute();

            foreach($stm->fetchAll() as $price) {
                $promo_price = (double)$price['promo_price'];
                $price_regular = (double)$price['price'];
                $big_price = (double)$price['big_price'];
            }

            if(isset($__user)){
                if($__user->is_partner()) {
                    if($big_price != "") {
                        $product['price'] = $big_price;
                    } else if($artikul->getCenaDostavna() != "") {
                        $product['price'] = $artikul->getCenaDostavna();
                    } else {
                        if(empty($price_regular) && empty($promo_price))
                        {
                            if($artikul->getCena_promo() > 0) {
                                $product['price'] = $artikul->getCena_promo();
                            } else {
                                $product['price'] = $artikul->getCena();
                            }
                        } else {
                            if(!empty($promo_price)) {
                                $product['price'] = $promo_price;
                            } else {
                                $product['price'] = $price_regular;
                            }
                        }
                    }
                } else {
                    if(empty($price_regular) && empty($promo_price))
                    {
                        if($artikul->getCena_promo() > 0) {
                            $product['price'] = $artikul->getCena_promo();
                        } else {
                            $product['price'] = $artikul->getCena();
                        }
                    } else {
                        if(!empty($promo_price)) {
                            $product['price'] = $promo_price;
                        } else {
                            $product['price'] = $price_regular;
                        }
                    }
                }
            } else {
                if(empty($price_regular) && empty($promo_price))
                {
                    if($artikul->getCena_promo() > 0) {
                        $product['price'] = $artikul->getCena_promo();
                    } else {
                        $product['price'] = $artikul->getCena();
                    }
                } else {
                    if(!empty($promo_price)) {
                        $product['price'] = $promo_price;
                    } else {
                        $product['price'] = $price_regular;
                    }
                }
            }
        } else {
            if(isset($__user)){
                if($__user->is_partner()) {
                    if($artikul->getCenaDostavna() > 0) {
                        $product['price'] = $artikul->getCenaDostavna();
                    } else {
                        $product['price'] = ($artikul->getCena_promo() > 0) ? $artikul->getCena_promo() : $artikul->getCena();
                    }
                } else {
                    $product['price'] = ($artikul->getCena_promo() > 0) ? $artikul->getCena_promo() : $artikul->getCena();
                }
            } else {
                $product['price'] = ($artikul->getCena_promo() > 0) ? $artikul->getCena_promo() : $artikul->getCena();
            }
        }

        if (isset($product['upgrade_option'])) {
            foreach ($product['upgrade_option'] as $option) {
                $product['price'] += $option['additional_price'];
            }
        }

        if($artikul->isPreorder()) {

            $stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :deposit LIMIT 1");
            $stm->bindValue(':deposit', "deposit", PDO::PARAM_STR);
            $stm->execute();
            $d = $stm->fetch();
            $depositPercent= $d['value'];

            $product['preorder']['deposit_price'] = $product['price'] * ($depositPercent / 100);
            $product['preorder']['deposit_percent'] = $depositPercent;
        }
        
        $dop_artikuli = $artikul->getAddArtikuli();
        if ($dop_artikuli != "") {
            $dop_art_arr = explode(",", $dop_artikuli);
            foreach($dop_art_arr as $dart) {
                $dr = new artikul((int)$dart);
                if(isset($_POST['option'.$dr->getId()])){
                    $options[] = $dr->getId().", ".$_POST['option'.$dr->getId()];
                }
                $products .= ", ".$dr->getId();
                $products_info[$dr->getId()]['marka'] = $dr->getIme_marka();
                $products_info[$dr->getId()]['model'] = $dr->getIme();
                $product['additional_delivery'] += $dr->getAdditionalDelivery() != '' ? $dr->getAdditionalDelivery() : 0;
                $add_artikuli .= ", ".$dr->getId();
                unset($dr);
            }
        }

        $product['type']="product";
        $product['add_artikuli'] = $add_artikuli;
        $product['additional_delivery'] += $artikul->getAdditionalDelivery() != '' ? $artikul->getAdditionalDelivery() : 0;
    } else {


        $group = new paket((int)$id);
        $artikuli = $group->getProducts();
        $n = 0;
        foreach($artikuli as $d) {
            $artikul = new artikul((int)$d);
            if(isset($_POST['option'.$artikul->getId()])){
                $options[] = $artikul->getId().", ".$_POST['option'.$artikul->getId()];
            }
            if($n > 0) $products .= ", ";
            $products .= $artikul->getId();
            $products_info[$artikul->getId()]['marka'] = $artikul->getIme_marka();
            $products_info[$artikul->getId()]['model'] = $artikul->getIme();

            $product['additional_delivery'] += $artikul->getAdditionalDelivery() != '' ? $artikul->getAdditionalDelivery() : 0;

            unset($artikul);
            $n++;
        }

        $product['type']="group";
        if(isset($_POST['price_tag']) && is_array($_POST['price_tag'])) {
            $product['price']=$group->getPrice($_POST['price_tag'], isset($__user) ? $__user : NULL);
        } else {
            $product['price']=$group->getPrice();
        }
    }

    $product['id']=$_POST['addToCard'];
    $product['ids']=$products;
    $product['info']=$products_info;
    $product['price_tag']=$price_tag;
    $product['options']=$options;
    $product['count']=$_POST['counts'];

    $tmp_rez='add';
    if(isset($_SESSION['cart'])){
        $total_additional_delivery = 0;
        foreach ($_SESSION['cart'] as $key => $tmp_product){
            
            $total_additional_delivery += $tmp_product['additional_delivery'];

            if($product['id']==$tmp_product['id']&&$product['type']==$tmp_product['type']&&$product['options']==$tmp_product['options']&&$product['upgrade_option']==$tmp_product['upgrade_option']){
                $_SESSION['cart'][$key]['count']+=(int)$_POST['counts'];
                $tmp_rez='noadd';
            }
        }

        $_SESSION['cart']['total_additional_delivery'] = $total_additional_delivery;
    }

    if($tmp_rez=='add'){
        $_SESSION['cart'][$_POST['addToCard'].'_'.rand()]=$product;
    }

    if(isset($_SESSION['cart'])){
        $total_additional_delivery = 0;
        foreach ($_SESSION['cart'] as $key => $tmp_product){

            $total_additional_delivery += $tmp_product['additional_delivery'] * $tmp_product['count'];
        }

        $_SESSION['cart']['total_additional_delivery'] = $total_additional_delivery;
    }
}
$page_title=" - количка";
?>
