<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class vid
{

    private $id;
    private $ime;
    private $id_kategoriq;
    private $ime_kategoriq;
    private $is_virtual;

    function __construct($a)
    {
        $pdo = PDOX::vrazka();

        if (is_int($a)) {

            $stm = $pdo->prepare('SELECT * FROM `vid` WHERE `id` = ? ');
            $stm->bindValue(1, $a, PDO::PARAM_INT);
            $stm->execute();

            $a = $stm->fetch();

        }


        if (is_array($a)) {
            $this->id = $a['id'];
            $this->ime = $a['ime_' . lang_prefix];
            if (!$this->ime) $this->ime = $a['ime_' . lang_default_prefix];

            $this->id_kategoriq = $a['id_kategoriq'];
            $this->is_virtual = $a['is_virtual'];

            $temp_kategoriq = new kategoriq((int)$this->getId_kategoriq());

            $this->ime_kategoriq = $temp_kategoriq->getIme(1);

            unset($temp_kategoriq);
        } else greshka('KONSTUKTUR GRe6KA VID');

    }


    public function getId()
    {
        return $this->id;
    }

    public function getIme($bez_html_special = FALSE)
    {
        if ($bez_html_special == FALSE) return htmlspecialchars($this->ime);
        else return $this->ime;
    }

    public function getId_kategoriq()
    {
        return $this->id_kategoriq;
    }

    public function getIme_kategoriq()
    {
        return $this->ime_kategoriq;
    }

    public function isVirtual()
    {
        return (bool) $this->is_virtual;
    }

    public function getBroi_artikuli($branch = Null, $promo = false)
    {
        $pdo = PDOX::vrazka();
        if ($this->isVirtual()) {
            $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id` IN (SELECT `artikul_id` FROM `product_vid` WHERE `vid_id` = :vid) AND isAvaliable=1 AND IsOnline=1';
        } else {
            $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_vid` = :vid AND isAvaliable=1 AND IsOnline=1';
        }

        if (isset ($branch)) {
            $sql .= ' AND `id_model` IN (Select id From `model` where ( `ime` like :marka  )) ';
        }
        if ($promo) {
            $sql .= ' AND `cena_promo`>0 ';
        }

        $stm = $pdo->prepare($sql);

        $stm->bindValue(':vid', $this->getId(), PDO::PARAM_INT);
        if (isset ($branch)) {
            $stm->bindValue(':marka', $branch, PDO::PARAM_STR);
        }
        $stm->execute();
        $data = $stm->fetch();

        return $data[0];
    }

    public function getChildVidove() {
        if (!$this->isVirtual()) return;

        $pdo = PDOX::vrazka();
        $sql = 'SELECT DISTINCT id_vid FROM artikuli WHERE id IN (SELECT artikul_id FROM product_vid WHERE vid_id = :vid_id)';
        $stm = $pdo->prepare($sql);
        $stm->bindValue(':vid_id', $this->getId(), PDO::PARAM_INT);
        $stm->execute();

        $ids = array_map(function($v){
            return $v['id_vid'];
        }, $stm->fetchAll());

        return $ids;
    }

    public function vidoveFromSameCategory() {
        if (!$this->isVirtual()) return false;

        $pdo = PDOX::vrazka();
        $vid = "";
        foreach ($this->getChildVidove() as $childVid) {
            $vid .= ":vid" . $childVid . ",";
        }
        $vid = substr($vid, 0, -1);
        $sql = 'SELECT DISTINCT id_kategoriq FROM vid WHERE id IN (' . $vid . ')';
        $stm = $pdo->prepare($sql);
        foreach ($this->getChildVidove() as $childVid) {
            $stm->bindValue(':vid' . $childVid, $childVid, PDO::PARAM_STR);
        }
        $stm->execute();
        
        return count($stm->fetchAll()) == 1;
    }

    public function __get($name)
    {
        greshka('get');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }


}

?>
