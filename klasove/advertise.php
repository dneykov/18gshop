<?php

class advertise
{
    private $id;
    private $image;
    private $text;
    private $texts;
    private $distination_url;
    private $order;
    private $partner;

    public function __construct($adv_id)
    {
        $pdo = PDOX::vrazka();

        if (is_int($adv_id)) {

            $stm = $pdo->prepare('SELECT * FROM `zz_banners` WHERE `id` = ? ');
            $stm->bindValue(1, $adv_id, PDO::PARAM_INT);
            $stm->execute();

            $result = $stm->fetch();
        }

        if (is_array($result)) {
            $this->id = $result['id'];
            $this->image = $result['image'];
            $this->text = $result['adv_text_' . lang_prefix];
            $this->texts = $result['adv_texts_' . lang_prefix];
            $this->distination_url = $result['url'];
            $this->order = $result['podredba'];
            $this->partner = $result['partner'];

        } else {
            greshka('Konstruktyr advertise');
        }
    }


    public function getID()
    {
        return $this->id;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getImage_b()
    {
        return preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $this->getImage());
    }

    public function getImage_t()
    {
        return preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $this->getImage());
    }

    public function getText()
    {
        return $this->text;
    }

    public function getDistinationURL()
    {
        return $this->distination_url;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getTexts()
    {
        return $this->texts;
    }

    public function isOnlyPartners()
    {
        return $this->partner;
    }

    public function __get($name)
    {
        greshka('get');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }
}
