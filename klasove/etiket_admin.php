<?php

class etiket_admin extends etiket {

private $ime_etiket_grupa_all;
private $ime_all;


function __construct($a) {

    parent::__construct($a);

    try{

    $pdo = PDOX::vrazka();

    $stm = $pdo -> prepare('SELECT * FROM `etiketi` WHERE `id` = ? LIMIT 1');
    $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
    $stm -> execute();

    $data = $stm -> fetch();

    global $__languages;

    foreach ($__languages as $v) {

        $this->ime_all[] = array(
            'name' => $data['ime_'.$v->getPrefix()],
            'lang_name' => $v->getName(),
            'lang_prefix' => $v->getPrefix()
        );

    }






    } catch (Exception $e){
        greshka($e);
    }

    




}



public function getIme_etiket_grupa_all($bez_html_spec = FALSE) {

    if(!isset ($this->ime_etiket_grupa_all)) {

    $pdo = PDOX::vrazka();
    $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id` = ? ');
    $stm -> bindValue(1, $this->getId_etiketi_grupa(), PDO::PARAM_INT);
    $stm -> execute();

    $data = $stm -> fetch();


    global $__languages;

    foreach ($__languages as $v) {
        $this->ime_etiket_grupa_all[] = array(
            'name' => $data['ime_'.$v->getPrefix()],
            'id_grupa' => $data['id'],
            'prefix' => $v->getPrefix()
                );
    }

    }

    if($bez_html_spec == FALSE ) {
        $temp = NULL;
        foreach ($this->ime_etiket_grupa_all as $v) {

            $temp[] = htmlspecialchars($v);
        }

        return $temp;



    }
    else return $this->ime_etiket_grupa_all;

}





public function getIme_all($bez_html_spec = false) {
   if($bez_html_spec == true ) return $this->ime_all;

   else greshka('nedovar6ena funcq etiket admin');
}






public function __get($name) {
    greshka('get');
}

public function __set($name, $value) {
    greshka('set');
}


} // class



?>
