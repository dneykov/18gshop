<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class etiket{
    private $id;
    private $id_etiketi_grupa;
    private $ime;

    private $ime_etiket_grupa;


    public function  __construct($a, $marka = NULL, $etiketi_predishni = NULL) {

       $pdo = PDOX::vrazka();

        if(is_int($a)){

            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ');
            $stm -> bindValue(1, $a, PDO::PARAM_INT);
            $stm -> execute();
            $a = $stm->fetch();

        }



        if(is_array($a)){

            $this->id = $a['id'];
            $this->id_etiketi_grupa = $a['id_etiketi_grupa'];


            $this->ime = $a['ime_'.lang_prefix];
            if(!$this->ime) $this->ime = $a['ime_'.lang_default_prefix];


            /*if($marka || $etiketi_predishni) {
            $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE (
                    `cache_etiketi` LIKE (:id_etiket) ';

            if($marka) $sql .= ' AND `id_model` = :id_model ';

            if($etiketi_predishni) {


                foreach ($etiketi_predishni as $k => $v) {
                $sql .= ' AND `cache_etiketi` LIKE (:etiket'.$k.')';
                }
            }

            $sql .= ' ) ';


            $stm = $pdo->prepare($sql);
            $stm -> bindValue(':id_etiket', '%<etiket>'.$this->getId().'</etiket>%', PDO::PARAM_STR);
            if($marka)  $stm -> bindValue(':id_model', $marka, PDO::PARAM_INT);
            if($etiketi_predishni) foreach ($etiketi_predishni as $k => $v) {
                $stm->bindValue(':etiket'.$k, '%<etiket>'.$v.'</etiket>%', PDO::PARAM_STR);
                }


            }


            else
                {*/
				


        } //else greshka('konstruktur etiket');
    }


    public function getId() {
        return $this->id;
    }

public function etiketi_grupaFix($id){
    $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = $id ');
    $stm -> execute();
    $a = $stm->fetch();
    return $a;
}

public function getId_etiketi_grupa() {
    return htmlspecialchars($this->id_etiketi_grupa);
}




    public function getIme($bez_html_spec = false) {
     if($bez_html_spec == FALSE )   return htmlspecialchars($this->ime);

     else return $this->ime;

    }


public function getBroi_artikuli($branch=Null,$promo=false) {
        global $vid; 
        global $model; 
	$pdo = PDOX::vrazka();
	$sql='SELECT COUNT(`id`) AS `broi` FROM `etiketi_zapisi` WHERE `id_etiket` = :id';
	
	if(isset($model)){
		$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE id_model=:model AND isAvaliable=1 AND IsOnline=1)';
	}
	if(isset($vid)){
		if ($vid->isVirtual()) {
			$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE id IN (SELECT artikul_id FROM product_vid WHERE vid_id = :vid) AND isAvaliable=1 AND IsOnline=1)';
		} else {
			$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE id_vid=:vid AND isAvaliable=1 AND IsOnline=1)';
		}
	}
	if($promo){
		$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE `cena_promo`>0 AND isAvaliable=1 AND IsOnline=1) ';
	}
	if(isset ($branch)){	
		$sql .= ' AND  id_artikul in (SELECT id FROM artikuli WHERE `id_model` IN (Select id From `model` where ( `ime` like :marka  )) AND isAvaliable=1 AND IsOnline=1) ';
	}
	
	$stm = $pdo->prepare($sql);
	$stm -> bindValue(':id', $this->getId(), PDO::PARAM_INT);
	if(isset($model)){
		$stm -> bindValue(':model', $model->getId(), PDO::PARAM_INT);
	}

	if(isset($vid)){
		$stm -> bindValue(':vid', $vid->getId(), PDO::PARAM_INT);
	}
	if(isset ($branch)){	
		$stm -> bindValue(':marka', $branch, PDO::PARAM_STR);
	}
	$stm -> execute();

	$data = $stm -> fetch();

	return $data[0];
}

public function getBroi_artikuli_artikul($id, $branch=Null,$promo=false) {
        global $vid; 
        global $model;   
	$pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id` = ?');
        $stm -> bindValue(1, $id, PDO::PARAM_INT);
        $stm -> execute();
        $ss = $stm->fetch();
        $vid = $ss['id_vid']; 
        $model= $ss['id_model'];
	$sql='SELECT COUNT(`id`) AS `broi` FROM `etiketi_zapisi` WHERE `id_etiket` = :id';
	
	if(isset($model)){
		$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE id_model=:model AND isAvaliable=1 AND IsOnline=1)';
	}
	if(isset($vid)){
		$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE id_vid=:vid AND isAvaliable=1 AND IsOnline=1)';
	}
	if($promo){
		$sql.=' AND id_artikul in (SELECT id FROM artikuli WHERE `cena_promo`>0 AND isAvaliable=1 AND IsOnline=1) ';
	}
	if(isset ($branch)){	
		$sql .= ' AND  id_artikul in (SELECT id FROM artikuli WHERE `id_model` IN (Select id From `model` where ( `ime` like :marka  ))  AND isAvaliable=1 AND IsOnline=1) ';
	}
	
	$stm = $pdo->prepare($sql);
	$stm -> bindValue(':id', $this->getId(), PDO::PARAM_INT);
	if(isset($model)){
		$stm -> bindValue(':model', $model, PDO::PARAM_INT);
	}
	
	if(isset($vid)){
		$stm -> bindValue(':vid', $vid, PDO::PARAM_INT);
	}
	if(isset ($branch)){	
		$stm -> bindValue(':marka', $branch, PDO::PARAM_STR);
	}
	$stm -> execute();

	$data = $stm -> fetch();

	return $data[0];
}

public function getIme_etiket_grupa($bez_html_spec = FALSE) {

    if(isset ($this->ime_etiket_grupa)) {
        if($bez_html_spec == TRUE) return $this->ime_etiket_grupa;
        return htmlspecialchars($this->ime_etiket_grupa);
    }

    $temp = new etiket_grupa((int)$this->getId_etiketi_grupa());

    $this->ime_etiket_grupa = $temp->getIme(false);

    if($bez_html_spec == FALSE ) return htmlspecialchars($this->ime_etiket_grupa);
    else return $this->ime_etiket_grupa;

}






public function __get($name) {
    greshka('get');
}

public function __set($name, $value) {
    greshka('set');
}



} // class



?>
