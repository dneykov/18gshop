<?php


class language{

    private $id;
    private $prefix;
    private $name;
    private $default;
    private $template_folder;
    private $enabled;

    public function  __construct($a) {

        $pdo = PDOX::vrazka();

        if(is_int($a)){

        $stm = $pdo->prepare('SELECT * FROM `languages` WHERE `id` = ? ');
        $stm -> bindValue(1, $a, PDO::PARAM_INT);
        $stm -> execute();

        if($stm -> rowCount() == 0 ) greshka('Нямаме език с ИД'.$а);

        $a = $stm -> fetch();

            
        }



        if(is_array($a)){

            $this->id = $a['id'];
            $this->prefix = $a['prefix'];
            $this->name = $a['name'];
            $this->default = $a['default'];
            $this->template_folder = $a['template_folder'];
            $this->enabled = $a['enabled'];

        }
    }

    

public function getId() {
    return $this->id;
}

public function getPrefix() {
    return $this->prefix;
}

public function getName() {
    return htmlspecialchars($this->name);
}

public function getDefault() {
    return $this->default;
}


public function getTemplate_folder() {
    return $this->template_folder;
}


public function getEnabled() {
    return $this->enabled;
}


public function __get($name) {
    greshka('get');
}

public function __set($name, $value) {
    greshka('set');
}


    
}


?>
