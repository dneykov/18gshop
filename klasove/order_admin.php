<?php

class order_admin
{

    private $id;
    private $userID;
    private $status;
    private $comment;
    private $admin_comment;
    private $date;
    private $products;
    private $contactInformation;
    private $dateAccepted;
    private $dateDone;
    private $dateReturned;
    private $dateCanceled;
    private $nomer;
    private $country;
    private $paypal;
    private $paypal_paid;
    private $cost;
    private $preorderCost;
    private $deleted_items;
    private $paymentType;
    private $confirmationSent;
    private $productOffered;

    function __construct($a)
    {

        try {
            $pdo = PDOX::vrazka();

            if (is_int($a)) {
                $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `id` = ? ');
                $stm->bindValue(1, $a, PDO::PARAM_INT);
                $stm->execute();

                if ($stm->rowCount() == 0) greshka('order RC=0');

                $a = $stm->fetch();
            }


            if (is_array($a)) {
                $this->id = $a['id'];
                $this->userID = $a['user'];
                $this->status = $a['status'];
                $this->comment = $a['comment'];
                $this->admin_comment = $a['admin_comment'];
                $this->date = $a['date'];
                $this->nomer = $a['nomer'];
                $this->country = $a['country'];
                $this->paypal = $a['paypal'];
                $this->paypal_paid = $a['paypal_paid'];
                $this->cost = $a['cost'];
                $this->preorderCost = $a['preorder_cost'];
                $this->deleted_items = $a['deleted_items'];
                $this->paymentType = $a['payment_type'];
                $this->confirmationSent = $a['confirmation_sent'];
                $this->productOffered = $a['products_offered'];

                $orderDetails = unserialize($a['details']);
                $this->products = $orderDetails['details'];
                $this->contactInformation = $orderDetails['header'];

                $this->dateAccepted = $a['date_accepted'];
                $this->dateDone = $a['date_done'];
                $this->dateCanceled = $a['date_canceled'];
                $this->dateReturned = $a['date_returned'];
            } else {
                greshka('contruktyr users_admin');
            }

        } catch (Exception $e) {
            greshka($e);
        }

    } // construct


    public function getId()
    {
        return $this->id;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getComment($bezhtmlspec = false)
    {
        return htmlspecialchars($this->comment);
    }

    public function getAdminComment($bezhtmlspec = false)
    {
        return htmlspecialchars($this->admin_comment);
    }

    public function getProductsOffered()
    {
        return $this->productOffered;
    }

    public function isConfirmationSent()
    {
        return (bool) $this->confirmationSent;
    }

    public function getDate()
    {
        $date = new DateTime($this->date);
        return date_format($date, 'd-m-Y H:i:s');
    }

    public function getNomer()
    {
        return $this->nomer;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function isPreorder()
    {
        foreach ($this->products as $product) {
            if (isset($product['preorder'])) return true;
        }

        return false;
    }

    public function getContactInformation()
    {
        return $this->contactInformation;
    }

    public function getDateAccepted()
    {
        if (empty($this->dateAccepted)) return null;
        $date = new DateTime($this->dateAccepted);
        return date_format($date, 'd-m-Y H:i:s');
    }

    public function getDateDone()
    {
        if (empty($this->dateDone)) return null;
        $date = new DateTime($this->dateDone);
        return date_format($date, 'd-m-Y H:i:s');
    }

    public function getDateReturned()
    {
        if (empty($this->dateReturned)) return null;
        $date = new DateTime($this->dateReturned);
        return date_format($date, 'd-m-Y H:i:s');
    }

    public function getDateCanceled()
    {
        if (empty($this->dateCanceled)) return null;
        $date = new DateTime($this->dateCanceled);
        return date_format($date, 'd-m-Y H:i:s');
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getPreorderCost()
    {
        return $this->preorderCost;
    }

    public function getDeletedItems()
    {
        $array = explode(",", $this->deleted_items);
        $items = array();
        foreach ($array as $k => $v) {
            $items[] = trim($v);
        }
        return $items;
    }

    public static function getCountForAll()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT COUNT(*) FROM `orders` Where `status`=0');
        $stm->execute();
        $rez = $stm->fetch();
        return $rez[0];
    }

    public static function getCountForDone()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT COUNT(*) FROM `orders` Where `status`=2');
        $stm->execute();
        $rez = $stm->fetch();
        return $rez[0];
    }

    public static function getCountForApproved()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT COUNT(*) FROM `orders` Where `status`=1');
        $stm->execute();
        $rez = $stm->fetch();
        return $rez[0];
    }

    public static function getCountForCanceled()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT COUNT(*) FROM `orders` Where `status`=4');
        $stm->execute();
        $rez = $stm->fetch();
        return $rez[0];
    }

    public static function getCountForReturned()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT COUNT(*) FROM `orders` Where `status`=5');
        $stm->execute();
        $rez = $stm->fetch();
        return $rez[0];
    }

    public static function getCountForWaiting()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT COUNT(*) FROM `orders` Where `status`=0');
        $stm->execute();
        $rez = $stm->fetch();
        return $rez[0];
    }

    public function getCountryId()
    {
        return $this->country;
    }

    public function getCountryName()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `locations` WHERE `id`= ? LIMIT 1');
        $stm->bindValue(1, $this->getCountryId(), PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $rez = $stm->fetch();
            return $rez['country_name'];
        } else {
            return false;
        }
    }

    public function getPaypal()
    {
        return $this->paypal;
    }

    public function getPaypalPaid()
    {
        return $this->paypal_paid;
    }

    public function getPaymentType()
    {
        return $this->paymentType;
    }

    public function isBankPayment()
    {
        if ($this->paymentType == "bank") return true;

        return false;
    }
} // class

