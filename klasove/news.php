<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class news{
    private $id;
    private $text;
    private $image;
    private $url;
	private $title;
	private $date;
	private $type;

    public function  __construct($a) {

       $pdo = PDOX::vrazka();

        if(is_int($a)){

            $stm = $pdo->prepare('SELECT * FROM `news` WHERE `id` = ? ');
            $stm -> bindValue(1, $a, PDO::PARAM_INT);
            $stm -> execute();
            $a = $stm->fetch();

        }
			

        if(is_array($a)){

            $this->id = $a['id'];
            $this->text = $a['text_'.lang_prefix];
			$this->image = $a['image'];
            $this->url = $a['url'];
			$this->title = $a['title_'.lang_prefix];
			$this->date = $a['news_date'];
			$this->type = $a['Type'];
			/*
            $this->ime = $a['ime_'.lang_prefix];
            if(!$this->ime) $this->ime = $a['ime_'.lang_default_prefix];
			*/

        } else greshka('konstruktur news');
    }


    public function getId() {
        return $this->id;
    }

public function getURL() {
	return $this->url;
    //return htmlspecialchars($this->id_etiketi_grupa);
}

public function getImage() {
	return $this->image;
}
public function getImage_Original() {
    $kartinka = str_replace('thumbnail_', 'resize_', $this->getImage());
    return $kartinka;
}
public function getImage_t() {
    $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $this->getImage());
    return $kartinka;
}
public function getImage_b() {
    $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $this->getImage());
    return $kartinka;
}
public function getText() {
	return $this->text;
}
public function getTitle() {
	return $this->title;
}

public function getDate() {
	return $this->date;
}

public function getType() {
	return $this->type;
}

public function __get($name) {
    greshka('get');
}

public function __set($name, $value) {
    greshka('set');
}



} // class



?>
