<?php

class user{

    private $id;
    private $mail;
    private $name;
    private $name_last;
    private $admin_comment;
    private $phone_mobile;
    private $phone_home;
    private $adress_town;
    private $adress_street;
    private $adress_number;
    private $adress_vhod;
    private $adress_etaj;
    private $adress_ap;
    private $country;
    private $clientNumber;
    private $company_name;
    private $company_city;
    private $company_street;
    private $company_number;
    private $company_mol;
    private $company_eik;
    private $company_vat;
    private $company_phone;
    private $account_type;
    private $created_at;



    function __construct($a) {

        if(is_int($a)){
			$pdo = PDOX::vrazka();
            

            $stm = $pdo -> prepare('SELECT * FROM `members` WHERE `id` = ? ');
            $stm -> bindValue(1, $a, PDO::PARAM_INT);
            $stm -> execute();

            if($stm->rowCount() == 0 ) greshka('user RC=0');

            $a = $stm -> fetch();

        }
	
	
        if(is_array($a)){


            $this->mail = $a['mail'];
            $this->id = $a['id'];
            $this->name = $a['name'];
            $this->name_last = $a['name_last'];
            $this->admin_comment = $a['admin_comment'];
            $this->country = $a['country'];
            $this->phone_home = $a['phone_home'];
            $this->phone_mobile = $a['phone_mobile'];
            $this->adress_town = $a['adress_town'];
            $this->adress_street = $a['adress_street'];
            $this->adress_number = $a['adress_number'];
            $this->adress_vhod = $a['adress_vhod'];
            $this->adress_etaj = $a['adress_etaj'];
            $this->adress_ap = $a['adress_ap'];
            $this->clientNumber = $a['client_no'];
            $this->company_name = $a['company_name'];
            $this->company_city = $a['company_city'];
            $this->company_street = $a['company_street'];
            $this->company_number = $a['company_number'];
            $this->company_mol = $a['company_mol'];
            $this->company_eik = $a['company_eik'];
            $this->company_vat = $a['company_vat'];
            $this->company_phone = $a['company_phone'];
            $this->account_type = $a['account_type'];
            $this->created_at = $a['created_at'];
        }

        else greshka('construct error');

    }



public function getId() {
    return $this->id;
}

public function getMail() {
    return $this->mail;
}



public function getName() {
    return htmlspecialchars($this->name);
}

public function getName_last() {
    return htmlspecialchars($this->name_last);
}





public function getPhone_mobile() {
    return htmlspecialchars($this->phone_mobile);
}

public function getPhone_home() {
    return htmlspecialchars($this->phone_home);
}

public function getAdress_town() {
    return htmlspecialchars($this->adress_town);
}

public function getAdress_street() {
    return htmlspecialchars($this->adress_street);
}

public function getAdress_number() {
    return htmlspecialchars($this->adress_number);
}

public function getAdress_vhod() {
    return htmlspecialchars($this->adress_vhod);
}

public function getAdress_etaj() {
    return htmlspecialchars($this->adress_etaj);
}

public function getAdress_ap() {
    return htmlspecialchars($this->adress_ap);
}
public function getAdminComment() {
    return htmlspecialchars($this->admin_comment);
}
public function getCountryId(){
         return $this->country;
}
public function getCountryName(){
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare("SELECT `country_name` FROM `locations` WHERE `id`=?");
        $stm -> bindValue(1, (int)$this->country, PDO::PARAM_INT);
        $stm -> execute();
        $c = $stm -> fetch();
        return $c['country_name'];
}


public function __get($name) {
    greshka('get');
}

public function __set($name, $value) {
    greshka('set');
}

public function delete(){

    try{


        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('DELETE FROM `members` WHERE `id` = ? ');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();





    } catch (Exception $e){
        greshka($e);
    }



}

    public function updatePartner($status) {
        try{


            $pdo = PDOX::vrazka();

            $stm = $pdo->prepare('UPDATE `members` SET partner = ? WHERE `id` = ? ');
            $stm -> bindValue(1, $status, PDO::PARAM_INT);
            $stm -> bindValue(2, $this->getId(), PDO::PARAM_INT);
            $stm -> execute();





        } catch (Exception $e){
            greshka($e);
        }
    }

    public function is_partner() {
        try{


            $pdo = PDOX::vrazka();

            $stm = $pdo -> prepare('SELECT partner FROM `members` WHERE `id` = ? ');
            $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
            $stm -> execute();





        } catch (Exception $e){
            greshka($e);
        }
        $u = $stm -> fetch();
        return $u['partner'];
    }

    public function getClientNumber() {
        return $this->clientNumber;
    }

    public function getAccountType() {
        return $this->account_type;
    }

    public function getCompanyName() {
        return $this->company_name;
    }

    public function getCompanyCity() {
        return $this->company_city;
    }

    public function getCompanyStreet() {
        return $this->company_street;
    }

    public function getCompanyNumber() {
        return $this->company_number;
    }

    public function getCompanyMol() {
        return $this->company_mol;
    }

    public function getCompanyEik() {
        return $this->company_eik;
    }

    public function getCompanyVat() {
        return $this->company_vat;
    }

    public function getCompanyPhone() {
        return $this->company_phone;
    }

    public function createdAt() {
        return $this->created_at;
    }
}

?>
