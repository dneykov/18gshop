<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class etiket_grupa
{
    private $id;
    private $id_vid;
    private $ime;
    private $etiketi;

    private $select_menu;

    public function __construct($a, $model = NULL, $etiketi_izpolzvani = NULL, $propusniNuleviteEtiketi = false)
    {

        $pdo = PDOX::vrazka();

        if (is_int($a)) {

            $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id` = ?');
            $stm->bindValue(1, $a, PDO::PARAM_INT);
            $stm->execute();

            $a = $stm->fetch();
        }

        if (is_array($a)) {

            $this->id = $a['id'];
            $this->id_vid = $a['id_vid'];
            $this->select_menu = $a['select_menu'];
            $this->ime = $a['ime_' . lang_prefix];
            if (!$this->ime) $this->ime = $a['ime_' . lang_default_prefix];

            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id_etiketi_grupa` = ? ORDER BY `ordr`,`ime_' . lang_prefix . '` ASC');
            $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
            $stm->execute();

            if ($stm->rowCount() == 0) $this->etiketi = NULL;
            else foreach ($stm->fetchAll() as $v) {

                $temp_etiket = new etiket($v, $model, $etiketi_izpolzvani);
                
                if ($propusniNuleviteEtiketi) if ($temp_etiket->getBroi_artikuli() == 0) continue;

                $this->etiketi[] = $temp_etiket;

                unset($temp_etiket);
            }
            unset($v);

        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getId_vid()
    {
        return $this->id_vid;
    }


    public function getIme($html_coded = true)
    {
        if ($html_coded == true) return htmlspecialchars($this->ime);

        else return $this->ime;

    }


    public function getEtiketi()
    {
        return $this->etiketi;
    }


    public function getSelect_menu()
    {
        return $this->select_menu;
    }


    public function setEtiket_null()
    {
        $this->etiketi = array();
    }

    public function setEtiket_push($a)
    {
        array_push($this->etiketi, $a);
    }

    public function __get($name)
    {
        greshka('get');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }

}