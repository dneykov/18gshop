<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class artikul
{

    private $id;
    private $kod;
    private $id_vid;
    private $id_model;
    private $id_kategoriq;
    private $add_artikuli;
    private $related_products;
    private $ime;
    private $cena;

    private $cena_promo;
    private $cena_dostavka;
    private $cena_dostavna;


    private $etiketi_zapisi;
    private $etiketi_array_ids;

    private $etiketi_zapisi_all;

    private $katinka;
    protected $katinka_dopalnitelni;
    protected $katinka_dopalnitelni_text;

    private $ime_kategoriq;
    private $ime_marka;
    private $ime_vid;

    private $manual;

    private $taggroups;

    private $taggroup_dropdown;
    private $description;
    private $preorderDescription;

    private $brand;
    private $isonline;
    private $isavaliable;
    private $check_date;
    private $creat_date;
    private $dayoffer;
    private $dop_info;
    private $video;
    private $ispreorder;
    private $collection_id;
    private $collection_name;
    private $title;
    private $specification;
    private $collectionProducts;
    private $additionalDelivery;

    private $isgone;

    function __construct($a)
    {
        $pdo = PDOX::vrazka();


        if (is_int($a)) {

            $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id` = ? LIMIT 1 ');
            $stm->bindValue(1, $a, PDO::PARAM_INT);
            $stm->execute();

            $a = $stm->fetch();

        }


        if (is_array($a)) {

            $this->kod = $a['kod'];
            $this->id = $a['id'];
            $this->id_vid = $a['id_vid'];
            $this->id_model = $a['id_model'];
            $this->id_kategoriq = $a['id_kategoriq'];
            $this->add_artikuli = $a['add_artikuli'];
            $this->related_products = $a['related_products'];
            $this->ime = $a['ime'];
            $this->cena = $a['cena'];
            $this->cena_dostavka = $a['cena_dostavka'];
            $this->cena_promo = $a['cena_promo'];
            $this->cena_dostavna = $a['cena_dostavna'];
            $this->description = $a['dop_info_' . lang_prefix];
            $this->preorderDescription = $a['preorder_' . lang_prefix];
            $this->katinka = $a['kartinka'];
            $this->isonline = $a['IsOnline'];
            $this->isavaliable = $a['isAvaliable'];
            $this->check_date = $a['check_date'];
            $this->manual = $a['Manual'];
            $this->creat_date = $a['creat_date'];
            $this->dayoffer = $a['DayOffer'];
            $this->video = $a['video'];
            $this->isgone = false;
            $this->ispreorder = $a['preorder'];
            $this->collection_id = $a['collection_id'];
            $this->title = $a['title_' . lang_prefix];
            $this->specification = $a['spec_' . lang_prefix];
            $this->additionalDelivery = $a['additional_delivery'];

            $stm = $pdo->prepare('SELECT * FROM `collections` WHERE `id` = ?  ');
            $stm->bindValue(1, $this->getCollectionId(), PDO::PARAM_INT);
            $stm->execute();
            $collection = $stm->fetch();
            $this->collection_name = $collection['name'];

            if ($this->getCollectionId() > 0) {
                $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `collection_id` = ? ORDER BY id DESC');
                $stm->bindValue(1, $this->getCollectionId(), PDO::PARAM_INT);
                $stm->execute();

                if ($stm->rowCount() == 0) {
                    $this->collectionProducts = NULL;
                } else {
                    foreach ($stm->fetchAll() as $v) {
                        $this->collectionProducts[] = $v['id'];
                    }
                }
            }

            $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ?  ');
            $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
            $stm->execute();

            if ($stm->rowCount() == 0) $this->etiketi_zapisi = NULL;
            else {

                foreach ($stm->fetchAll() as $v) {

                    $temp = new etiket_zapis($v);


                    $this->etiketi_array_ids[] = $v['id_etiket'];


                    $temp_etiket = new etiket((int)$temp->getId_etiket());

                    $temp_grupa = new etiket_grupa((int)$temp_etiket->getId_etiketi_grupa());


                    $this->etiketi_zapisi_all[] = $temp_etiket;

                    if ($temp_grupa->getSelect_menu()) {


                        if (!isset ($this->taggroup_dropdown[$temp_grupa->getId()])) {
                            $this->taggroup_dropdown[$temp_grupa->getId()] = $temp_grupa;
                            //$this->taggroup_dropdown[$temp_grupa->getId()] ->setEtiket_null();
                        }

                        // $this->taggroup_dropdown[$temp_grupa->getId()] -> setEtiket_push($temp_etiket);


                        $this->etiketi_zapisi[] = $temp;


                    } // ako e dropdown
                    else {


                        if (!isset ($this->taggroups[$temp_grupa->getId()])) {
                            $this->taggroups[$temp_grupa->getId()] = $temp_grupa;
                            //$this->taggroups[$temp_grupa->getId()] ->setEtiket_null();
                        }

                        //$this->taggroups[$temp_grupa->getId()] -> setEtiket_push($temp_etiket);


                        $this->etiketi_zapisi[] = $temp;
                    }


                }
            }


            $temp_category = new kategoriq((int)$this->getId_kategoriq());
            $this->ime_kategoriq = $temp_category->getIme(1);

            unset($temp_category);


            $temp_vid = new vid((int)$this->getId_vid());


            $this->ime_vid = $temp_vid->getIme(1);

            unset($temp_vid);


            $temp_model = new model((int)$this->getId_model());

            $this->ime_marka = $temp_model->getIme(1);

            unset($temp_model);


            for ($q = 1; $q <= 12; $q++) {

                if (mb_strlen($a['img' . $q]) > 3) {
                    $this->katinka_dopalnitelni[$q] = new kartinka($a['img' . $q]);


                }

//                else $this->katinka_dopalnitelni[$q] = NULL;

            }


//            $stm = $pdo -> prepare('SELECT * FROM `artikuli_images` WHERE `id_artikul` = ? ');
//            $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
//            $stm -> execute();
//
//            if($stm->rowCount() == 0 ) $this->katinka_dopalnitelni = NULL;
//            else foreach ($stm->fetchAll() as $v) {
//                $this->katinka_dopalnitelni[] = new kartinka($v);
//                }


        } // if array

        else {
            $this->isgone = true;
        }
        return $a;
    }


    public function isGone()
    {
        return $this->isgone;
    }

    public function __toString()
    {
        return 'object to sting artikul';
    }


    public function getId()
    {
        return $this->id;
    }

    public function getId_vid()
    {
        return $this->id_vid;
    }

    public function getId_model()
    {
        return $this->id_model;
    }

    public function getId_kategoriq()
    {
        return $this->id_kategoriq;
    }

    public function getAddArtikuli()
    {
        return $this->add_artikuli;
    }

    public function getRelatedProducts()
    {
        return $this->related_products;
    }

    public function getIme($bez_html_spec = false)
    {

        if ($bez_html_spec == false) return htmlspecialchars($this->ime);
        else return $this->ime;
    }


    public function getCena()
    {
        return $this->cena;
    }

    public function getEtiketi_array_ids()
    {
        return $this->etiketi_array_ids;
    }


    public function getCena_promo()
    {
        return $this->cena_promo;
    }

    public function getCena_dostavka()
    {
        return $this->cena_dostavka;
    }

    public function getCenaDostavna()
    {
        return $this->cena_dostavna;
    }

    public function getEtiketizapisi()
    {
        return $this->etiketi_zapisi;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function getKatinka()
    {
        return $this->katinka;
    }


    public function getKartinka_t()
    {


        $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $this->getKatinka());
        return $kartinka;
    }

    public function getKartinka_b()
    {
        $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $this->getKatinka());
        return $kartinka;
    }

    public function getKartinka_org()
    {
        $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_org.$2', $this->getKatinka());
        return $kartinka;
    }

    public function getIme_kategoriq($bez_htmlspecials = false)
    {

        if ($bez_htmlspecials == false) return htmlspecialchars($this->ime_kategoriq);
        else return $this->ime_kategoriq;

    }

    public function getIme_marka($bez_html_specials = false)
    {
        if ($bez_html_specials == false) return htmlspecialchars($this->ime_marka);
        else return $this->ime_marka;


    }


    public function getCena_otstapka()
    {

        if ($this->getCena_promo()) return $this->getCena() - $this->getCena_promo();
        else return 0;

    }

    public function getIme_vid($bez_html_specials = false)
    {
        if ($bez_html_specials == false) return htmlspecialchars($this->ime_vid);
        else return $this->ime_vid;
    }


    public function getKatinka_dopalnitelni($return_empty_blocksa = true)
    {
        if ($return_empty_blocksa) return $this->katinka_dopalnitelni;
        else {

            if (count($this->katinka_dopalnitelni) > 0) {
                return $this->katinka_dopalnitelni;
            } else {
                return NULL;
            }


        }
    }

    public function getTaggroup_dropdown()
    {
        return $this->taggroup_dropdown;
    }

    public function getTaggroups()
    {
        return $this->taggroups;
    }

    public function getCreatDate()
    {
        return strtotime($this->creat_date);
    }

    public function IsNew()
    {
        if (($this->getCreatDate() + (120 * 24 * 60 * 60)) > time()) return true;
        return false;
    }

    public function IsDayOffer()
    {
        if ((strtotime($this->dayoffer) + (1 * 24 * 60 * 60)) > time()) return true;
        return false;
    }

    public function getEtiketi_zapisi_all()
    {
        return $this->etiketi_zapisi_all;
    }


    public function __get($name)
    {
        greshka('get magic method');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }


    public function getKod($html_specials = true)
    {
        if ($html_specials) return htmlspecialchars($this->kod);
        else return $this->kod;

    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPreorderDescription()
    {
        return $this->preorderDescription;
    }

    public function getBrand()
    {

        if (!isset($this->brand)) {
            $this->brand = new model((int)$this->getId_model());

        }
        return $this->brand;

    }

    public function isOnline()
    {
        return (bool)$this->isonline;
    }

    public function isAvaliable()
    {
        return (bool)$this->isavaliable;
    }

    public function getManual()
    {
        return $this->manual;
    }

    public function getDopInfo()
    {
        return $this->dop_info;
    }

    public function getKatinka_dopalnitelni_text()
    {
        return $this->katinka_dopalnitelni_text;
    }

    public function totalQuantity()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare("SELECT SUM(kolichestvo) as total_quantity FROM warehouse WHERE `code` IN (SELECT `code` FROM warehouse_products WHERE product_id = ?)");
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['total_quantity'];
        }

        return 0;
    }

    public function quantityByCode($code)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare("SELECT SUM(kolichestvo) as quantity FROM warehouse WHERE `code` = ?");
        $stm->bindValue(1, $code, PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['quantity'];
        }

        return 0;
    }

    public function getWarehouseCode()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `type` = ? LIMIT 1');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->bindValue(2, 1, PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['code'];
        } else {
            return "";
        }
    }

    public function getWarehouseTagCode($tag_id)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id` = ? AND `type` = ? LIMIT 1');
        $stm->bindValue(1, $this->id, PDO::PARAM_INT);
        $stm->bindValue(2, $tag_id, PDO::PARAM_INT);
        $stm->bindValue(3, 2, PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['code'];
        } else {
            return "";
        }
    }

    public function getWarehouseBigPrice($tag_id)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id` = ? AND `type` = ? LIMIT 1');
        $stm->bindValue(1, $this->id, PDO::PARAM_INT);
        $stm->bindValue(2, $tag_id, PDO::PARAM_INT);
        $stm->bindValue(3, 2, PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['big_price'];
        } else {
            return "";
        }
    }

    public function getWarehousePrice($tag_id)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id` = ? AND `type` = ? LIMIT 1');
        $stm->bindValue(1, $this->id, PDO::PARAM_INT);
        $stm->bindValue(2, $tag_id, PDO::PARAM_INT);
        $stm->bindValue(3, 2, PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['price'];
        } else {
            return "";
        }
    }

    public function getWarehousePromoPrice($tag_id)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id` = ? AND `type` = ? LIMIT 1');
        $stm->bindValue(1, $this->id, PDO::PARAM_INT);
        $stm->bindValue(2, $tag_id, PDO::PARAM_INT);
        $stm->bindValue(3, 2, PDO::PARAM_INT);
        $stm->execute();
        if ($stm->rowCount() > 0) {
            $a = $stm->fetch();
            return $a['promo_price'];
        } else {
            return "";
        }
    }

    public function getCheckDate()
    {
        return $this->check_date;
    }

    public function getCollectionId()
    {
        return $this->collection_id;
    }

    public function getCollectionName()
    {
        return $this->collection_name;
    }

    public function isPreorder()
    {
        return (bool)$this->ispreorder;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSpecification()
    {
        return $this->specification;
    }

    public function getCollectionProducts()
    {
        return $this->collectionProducts;
    }

    public function getAdditionalDelivery()
    {
        return $this->additionalDelivery;
    }

    public function getUpgradeGroups()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `mode_group_option` WHERE `mode_id` = ? ORDER BY `id` ');
        $stm->bindValue(1, $this->getId_vid(), PDO::PARAM_INT);
        $stm->execute();

        if ($stm->rowCount() > 0) {
            foreach ($stm->fetchAll() as $v) {
                $temp_upgrade_groups = new UpgradeGroup($v);
                $upgrade_groups[] = $temp_upgrade_groups;
                unset ($temp_upgrade_groups);
            }
        } else {
            $upgrade_groups = [];
        }

        return $upgrade_groups;
    }

    public function getUpgradeOptions($group_id)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `group_option_values` WHERE `group_option_id` = ? AND `artikul_id` = ? AND `default` = ? ORDER BY `id` ');
        $stm->bindValue(1, $group_id, PDO::PARAM_INT);
        $stm->bindValue(2, $this->getId(), PDO::PARAM_INT);
        $stm->bindValue(3, 0, PDO::PARAM_INT);
        $stm->execute();

        if ($stm->rowCount() > 0) {
            foreach ($stm->fetchAll() as $v) {
                $temp_options = new UpgradeOption($v);
                $upgrade_options[] = $temp_options;
                unset ($temp_options);
            }
        } else {
            $upgrade_options = [];
        }

        return $upgrade_options;
    }

    public function getDefaultUpgradeOptions($group_id)
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('SELECT * FROM `group_option_values` WHERE `group_option_id` = ? AND `artikul_id` = ? AND `default` = ? ORDER BY `id` LIMIT 1');
        $stm->bindValue(1, $group_id, PDO::PARAM_INT);
        $stm->bindValue(2, $this->getId(), PDO::PARAM_INT);
        $stm->bindValue(3, 1, PDO::PARAM_INT);
        $stm->execute();

        if ($stm->rowCount() > 0) {
            return new UpgradeOption($stm->fetch());
        } else {
            return NULL;
        }
    }

}
