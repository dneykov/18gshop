<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class model
{


    private $id;
    private $ime;
    private $image;
    private $id_kategoriq;

    public function __construct($a, $vid = NULL)
    {
        $pdo = PDOX::vrazka();

        if (is_int($a)) {

            $stm = $pdo->prepare('SELECT * FROM `model` WHERE `id` = ? ');
            $stm->bindValue(1, $a, PDO::PARAM_INT);
            $stm->execute();

            $a = $stm->fetch();
        }

        if (is_array($a)) {
            $this->id = $a['id'];
            $this->ime = $a['ime'];
            $this->id_kategoriq = $a['id_kategoriq'];
            $this->image = $a['image'];
        } else greshka('Konstruktyr model');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIme($bez_html_spec = FALSE)
    {
        if ($bez_html_spec == FALSE) return htmlspecialchars($this->ime);
        else return $this->ime;
    }

    public function getId_kategoriq()
    {
        return $this->id_kategoriq;
    }

    public function getBroi_artikuli($promo = false)
    {
        global $kategoriq, $vid, $__user;
        $pdo = PDOX::vrazka();
        if (isset($kategoriq)) {
            if (isset($vid)) {
                if ($vid->isVirtual()) {
                    $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_model` = ? AND id IN (SELECT `artikul_id` FROM `product_vid` WHERE `vid_id` = ?) AND isAvaliable=1 AND IsOnline=1 AND `isDelete`=0  AND `Okazion`=0';
                } else {
                    $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_kategoriq`=? AND `id_model` = ? AND `id_vid` = ? AND isAvaliable=1 AND IsOnline=1 AND `isDelete`=0  AND `Okazion`=0';
                }

                if (!isset($__user)) {
                    $sql .= ' AND `cena`>0 ';
                }

                if ($__user instanceof user) {
                    if (isset($__user) && !$__user->is_partner()) {
                        $sql .= ' AND `cena`>0 ';
                    }
                }

                if ($promo) {
                    $sql .= ' AND `cena_promo`>0 ';
                }
                
                $stm = $pdo->prepare($sql);


                if ($vid->isVirtual()) {
                    $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
                    $stm->bindValue(2, $vid->getId(), PDO::PARAM_INT);
                } else {
                    $stm->bindValue(1, $kategoriq->getId(), PDO::PARAM_INT);
                    $stm->bindValue(2, $this->getId(), PDO::PARAM_INT);
                    $stm->bindValue(3, $vid->getId(), PDO::PARAM_INT);
                }
            } else {
                $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_kategoriq`=? AND `id_model` = ? AND IsOnline=1 AND `isDelete`=0 AND `Okazion`=0 ';

                if (!isset($__user)) {
                    $sql .= ' AND `cena`>0 ';
                }

                if ($__user instanceof user) {
                    if (isset($__user) && !$__user->is_partner()) {
                        $sql .= ' AND `cena`>0 ';
                    }
                }

                if ($promo) {
                    $sql .= ' AND `cena_promo`>0 ';
                }
                $stm = $pdo->prepare($sql);
                $stm->bindValue(1, $kategoriq->getId(), PDO::PARAM_INT);
                $stm->bindValue(2, $this->getId(), PDO::PARAM_INT);
            }
        } else {
            $sql = 'SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_model` = ? AND IsOnline=1 AND `isDelete`=0 AND `Okazion`=0 ';

            if (!isset($__user)) {
                $sql .= ' AND `cena`>0 ';
            }

            if ($__user instanceof user) {
                if (isset($__user) && !$__user->is_partner()) {
                    $sql .= ' AND `cena`>0 ';
                }
            }

            if ($promo) {
                $sql .= ' AND `cena_promo`>0 ';
            }
            $stm = $pdo->prepare($sql);
            $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        }
        $stm->execute();

        $data = $stm->fetch();

        return $data[0];
    }

    public function getImage($bez_html_spec = false)
    {
        if ($bez_html_spec == false) return htmlspecialchars($this->image);
        else return $this->image;

    }

    public function getBWImage($bez_html_spec = false)
    {
        if ($bez_html_spec == false) return htmlspecialchars(str_replace('_color', '', $this->image));
        else return str_replace('_color', '', $this->image);

    }


    public function __get($name)
    {
        greshka('get');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }

}
