<?php

class paket{

    private $id;
    private $name;
    private $percent;
    private $price;
    private $price_normal;
    private $online;
    private $image;
    private $description;
    private $dop_info_all;
    private $name_all;
    private $products;
	
    function __construct($a) {
        $pdo = PDOX::vrazka();

        if(is_int($a)){

            $stm = $pdo->prepare('SELECT * FROM `group_products` WHERE `id` = ? LIMIT 1 ');
            $stm -> bindValue(1, $a, PDO::PARAM_INT);
            $stm -> execute();
            $a = $stm -> fetch();

        }

        if(is_array($a)){

            $this->id = $a['id'];
            $this->name = $a['name_'.lang_prefix];
            $this->percent = $a['percent'];
            $this->price = $a['price'];
            $this->price_normal = $a['price_normal'];
            $this->online = $a['online'];
            $this->image = $a['image'];
			$this->description = $a['dop_info_'.lang_prefix];

        }

        global $__languages;

        foreach ($__languages as $v) {

            $this->dop_info_all[] = array(
                'name' => $a['dop_info_'.$v->getPrefix()],
                'lang_name' => $v->getName(),
                'lang_prefix' => $v->getPrefix()
            );

            $this->name_all[] = array(
                'name' => $a['name_'.$v->getPrefix()],
                'lang_name' => $v->getName(),
                'lang_prefix' => $v->getPrefix()
            );
        }

        return $a;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getPercent() {
        return $this->percent;
    }

    public function getPrice($price_tags = NULL, $user = NULL) {
        global $pdo;
        $price = 0;

        if (is_array($price_tags) && is_array($this->getProducts())) {
            foreach ($this->getProducts() as $id) {
                $artikul = new artikul((int)$id);

                if (array_key_exists($artikul->getId(), $price_tags) && !empty($price_tags[$artikul->getId()])) {
                    $stm = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id`=?");
                    $stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                    $stm -> bindValue(2, $price_tags[$artikul->getId()], PDO::PARAM_INT);
                    $stm->execute();

                    foreach($stm->fetchAll() as $price_tag) {
                        $price_regular = (double)$price_tag['price'];
                        $big_price = (double)$price_tag['big_price'];
                    }


                    if(!is_null($user)) {
                        if ($user->is_partner()) {
                            if($big_price != "" && $big_price > 0) {
                                $price += $big_price;
                            } else if($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                                $price += $artikul->getCenaDostavna();
                            } else {
                                if(empty($price_regular))
                                {
                                    $price += $artikul->getCena();
                                } else {
                                    $price += $price_regular;
                                }
                            }
                        } else {
                            if(empty($price_regular))
                            {
                                $price += $artikul->getCena();
                            } else {
                                $price += $price_regular;
                            }
                        }
                    } else {
                        if(empty($price_regular))
                        {
                            $price += $artikul->getCena();
                        } else {
                            $price += $price_regular;
                        }
                    }
                } else {
                    if(!is_null($user)) {
                        if ($user->is_partner()) {
                            if($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                                $price += $artikul->getCenaDostavna();
                            } else {
                                $price += $artikul->getCena();
                            }
                        }
                    } else {
                        $price += $artikul->getCena();
                    }
                }

                unset($artikul);
            }
        } else {
            if(isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
                $user = new user((int)$_COOKIE['username_id']);
            }
            if (is_array($this->getProducts())) {
                foreach ($this->getProducts() as $id) {
                    $artikul = new artikul((int)$id);

                    if(isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
                        if ($user->is_partner()) {
                            if($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                                $price += $artikul->getCenaDostavna();
                            } else {
                                $price += $artikul->getCena();
                            }
                        } else {
                            $price += $artikul->getCena();
                        }
                    } else {
                        $price += $artikul->getCena();
                    }

                    unset($artikul);
                }
            }
        }

        return $price - ($price*($this->getPercent()/100));
    }

    public function getPrice_normal($price_tags = NULL, $user = NULL) {
        global $pdo;
        $price = 0;

        if (is_array($price_tags)) {
            foreach ($this->getProducts() as $id) {
                $artikul = new artikul((int)$id);

                if (array_key_exists($artikul->getId(), $price_tags) && !empty($price_tags[$artikul->getId()])) {
                    $stm = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id`=?");
                    $stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                    $stm -> bindValue(2, $price_tags[$artikul->getId()], PDO::PARAM_INT);
                    $stm->execute();

                    foreach($stm->fetchAll() as $price_tag) {
                        $price_regular = (double)$price_tag['price'];
                        $big_price = (double)$price_tag['big_price'];
                    }


                    if(!is_null($user)) {
                        if ($user->is_partner()) {
                            if($big_price != "" && $big_price > 0) {
                                $price += $big_price;
                            } else if($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                                $price += $artikul->getCenaDostavna();
                            } else {
                                if(empty($price_regular))
                                {
                                    $price += $artikul->getCena();
                                } else {
                                    $price += $price_regular;
                                }
                            }
                        } else {
                            if(empty($price_regular))
                            {
                                $price += $artikul->getCena();
                            } else {
                                $price += $price_regular;
                            }
                        }
                    } else {
                        if(empty($price_regular))
                        {
                            $price += $artikul->getCena();
                        } else {
                            $price += $price_regular;
                        }
                    }
                } else {
                    if(!is_null($user)) {
                        if ($user->is_partner()) {
                            if($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                                $price += $artikul->getCenaDostavna();
                            } else {
                                $price += $artikul->getCena();
                            }
                        }
                    } else {
                        $price += $artikul->getCena();
                    }
                }

                unset($artikul);
            }
        } else {
            if(isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
                $user = new user((int)$_COOKIE['username_id']);
            }

            foreach ($this->getProducts() as $id) {
                $artikul = new artikul((int)$id);

                if(isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
                    if ($user->is_partner()) {
                        if($artikul->getCenaDostavna() != "" && $artikul->getCenaDostavna() > 0) {
                            $price += $artikul->getCenaDostavna();
                        } else {
                            $price += $artikul->getCena();
                        }
                    } else {
                        $price += $artikul->getCena();
                    }
                } else {
                    $price += $artikul->getCena();
                }

                unset($artikul);
            }
        }

        return $price;
    }

    public function getPrice_reference($price_tags = NULL, $user = NULL) {
        global $pdo;
        $price = 0;

        if (is_array($price_tags)) {
            foreach ($this->getProducts() as $id) {
                $artikul = new artikul((int)$id);

                if (array_key_exists($artikul->getId(), $price_tags) && !empty($price_tags[$artikul->getId()])) {
                    $stm = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `tag_id`=?");
                    $stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                    $stm -> bindValue(2, $price_tags[$artikul->getId()], PDO::PARAM_INT);
                    $stm->execute();

                    foreach($stm->fetchAll() as $price_tag) {
                        $promo_price = (double)$price_tag['promo_price'];
                        $price_regular = (double)$price_tag['price'];
                    }


                    if(!is_null($user)) {
                        if ($user->is_partner()) {
                            if(empty($price_regular) && empty($promo_price)) {
                                if($artikul->getCena_promo() > 0) {
                                    $price += $artikul->getCena_promo();
                                } else {
                                    $price += $artikul->getCena();
                                }
                            } else {
                                if(!empty($promo_price)) {
                                    $price += $promo_price;
                                } else {
                                    $price += $price_regular;
                                }
                            }
                        }
                    }
                } else {
                    if(!is_null($user)) {
                        if ($user->is_partner()) {
                            $price += $artikul->getCena();
                        }
                    }
                }

                unset($artikul);
            }
        } else {
            if(isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
                $user = new user((int)$_COOKIE['username_id']);
            }

            foreach ($this->getProducts() as $id) {
                $artikul = new artikul((int)$id);

                if(isset($_COOKIE['username_id']) && $_COOKIE['username_id'] != '') {
                    if ($user->is_partner()) {
                        $price += $artikul->getCena();
                    }
                }

                unset($artikul);
            }
        }

        return $price - ($price*($this->getPercent()/100));
    }

    public function getOnline() {
        return $this->online;
    }

    public function getKatinka() {
        return $this->image;
    }

    public function getKartinka_t() {
        $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $this->getKatinka() );
        return $kartinka;
    }
        
    public function getKartinka_b() {
        $kartinka = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $this->getKatinka() );
        return $kartinka;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getProducts() {
        $artikuli = null;
        $pdo = PDOX::vrazka(); 
        $stm = $pdo->prepare('SELECT `artikul_id` FROM `group_products_zapisi` WHERE `group_id` = ?;');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();
        foreach ($stm->fetchAll() as $a) {
            $artikuli[] = $a['artikul_id'];
        }
        return $artikuli;
    }
    public function getProducts_string() {
        $artikuli = "";
        $pdo = PDOX::vrazka(); 
        $stm = $pdo->prepare('SELECT `artikul_id` FROM `group_products_zapisi` WHERE `group_id` = ?;');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();
        $n = 0;
        foreach ($stm->fetchAll() as $a) {
            if($n > 0) $artikuli .= ", ";
            $artikuli .= $a['artikul_id'];
            $n++;
        }
        return $artikuli;
    }

    public function delete(){
        delete_file(dir_root.$this->getKatinka());
        delete_file(dir_root.$this->getKartinka_b());
        delete_file(dir_root.$this->getKartinka_t());

        if(isset($tmp_ddopImg)&& (sizeof($tmp_ddopImg)>0)) foreach ($tmp_ddopImg as $v) {
            if(empty ($v)) continue;
            delete_file(dir_root.$v->getKatinka());
            delete_file(dir_root.$v->getKartinka_t());
            delete_file(dir_root.$v->getKartinka_b());
        }

        $pdo = PDOX::vrazka(); 
        $stm = $pdo->prepare('DELETE FROM `group_products` WHERE `id` = ?');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();
        
        $stm = $pdo->prepare('DELETE FROM `group_products_zapisi` WHERE `group_id` = ?');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }
    public function offline(){
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('UPDATE `group_products` SET online=0 WHERE `id` = ? LIMIT 1 ');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }
    public function online(){
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('UPDATE `group_products` SET online=1 WHERE `id` = ? LIMIT 1 ');
        $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }

    public function getDop_info_all($bez_html_spec = false ) {
       if($bez_html_spec == FALSE ) return $this->dop_info_all;
    }
    public function getName_all($bez_html_spec = false ) {
        if($bez_html_spec == FALSE ) return $this->name_all;
    }

    public function calculatePrice() {
        $price = 0;

        foreach ($this->getProducts() as $id) {
            $artikul = new artikul((int)$id);
            $price += $artikul->getCena();
            unset($artikul);
        }

        return $price - ($price*($this->getPercent()/100));
    }

    public function calculatePriceNormal() {

    }
}
?>
