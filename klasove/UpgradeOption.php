<?php

class UpgradeOption
{
    private $id;
    private $group_option_id;
    private $artikul_id;
    private $name;
    private $partner_price;
    private $price;
    private $default;

    function __construct($a)
    {
        $pdo = PDOX::vrazka();

        if (is_int($a)) {
            $stm = $pdo->prepare('SELECT * FROM `group_option_values` WHERE `id` = ? ');
            $stm -> bindValue(1, $a, PDO::PARAM_INT);
            $stm -> execute();

            if ($stm->rowCount() == 0 ) {
                greshka('Stm row count = 0 ');
            } else {
                $a = $stm -> fetch();
            }
        }

        if (is_array($a)) {
            $this->id = $a['id'];
            $this->artikul_id = $a['artikul_id'];
            $this->group_option_id = $a['group_option_id'];
            $this->name = $a['name'];
            $this->price = $a['price'];
            $this->partner_price = $a['partner_price'];
            $this->default = $a['default'];
        } else {
            greshka('категория конструктур грешка');
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGroupId()
    {
        return $this->group_option_id;
    }

    public function getArtikulId()
    {
        return $this->artikul_id;
    }

    public function getIme($bez_html_special=FALSE)
    {
        if($bez_html_special == FALSE )return htmlspecialchars($this->name);
        else return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPricePartner()
    {
        return $this->partner_price;
    }

    public function getPriceByUser()
    {
        global $__user;

        if (isset($__user) && $__user->is_partner()) {
            return $this->partner_price;
        }

        return $this->price;
    }

    public  function isDefault(){
        return (bool) $this->default;
    }
}
