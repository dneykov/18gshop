<?php

class order{

    private $id;
    private $userID;
    private $status;
    private $comment;
    private $admin_comment;
	private $date;
    private $products;
	private $contactInformation;
    private $dateAccepted;
	private $dateDone;
	private $nomer;
	private $country;
	private $paypal;
	private $paypal_paid;
	private $cost;
	private $preorderCost;
	private $deleted_items;
	private $paymentType;

    function __construct($a) {

        try{
			$pdo = PDOX::vrazka();

			if(is_int($a)){
				$stm = $pdo -> prepare('SELECT * FROM `orders` WHERE `id` = ? ');
				$stm -> bindValue(1, $a, PDO::PARAM_INT);
				$stm -> execute();

				if($stm->rowCount() == 0 ) greshka('order RC=0');

				$a = $stm -> fetch();
			}


			if(is_array($a)){
				$this->id = $a['id'];
				$this->userID = $a['user'];
				$this->status = $a['status'];
				$this->comment = $a['comment'];
				$this->admin_comment = $a['admin_comment'];
				$this->date = $a['date'];
				$this->nomer = $a['nomer'];
				$this->country = $a['country'];
				$this->paypal = $a['paypal'];
				$this->paypal_paid = $a['paypal_paid'];
				$this->cost = $a['cost'];
				$this->preorderCost = $a['preorder_cost'];
				$this->deleted_items = $a['deleted_items'];
				$this->paymentType = $a['payment_type'];

				$orderDetails=unserialize($a['details']);				
				$this->products = $orderDetails['details'];
				$this->contactInformation = $orderDetails['header'];
				
				$this->dateAccepted = $a['date_accepted'];
				$this->dateDone = $a['date_done'];

			}else{
				greshka('contruktyr users_admin');
			}

        } catch (Exception $e){
            greshka($e);
        }

    } // construct



public function getId() {
    return $this->id;
}

public function getUserID() {
    return $this->userID;
}

public function getStatus() {
    return $this->status;
}

public function getComment($bezhtmlspec = false) {
    return htmlspecialchars($this->comment);
}
public function getAdminComment($bezhtmlspec = false) {
    return htmlspecialchars($this->admin_comment);
}

public function getDate() {
    $date = new DateTime($this->date);
    return date_format($date, 'd-m-Y H:i:s');
}

public function getNomer() {
	return $this->nomer;
}
public function getProducts() {
    return $this->products;
}
public function isPreorder() {
	foreach ($this->products as $product) {
		if(isset($product['preorder'])) return true;
	}

	return false;
}
public function getContactInformation() {
    return $this->contactInformation;
}

public function getDateAccepted() {
    $date = new DateTime($this->dateAccepted);
    return date_format($date, 'd-m-Y H:i:s');
}
public function getDateDone() {
    $date = new DateTime($this->dateDone);
    return date_format($date, 'd-m-Y H:i:s');
}
public function getCost() {
    return $this->cost;
}
public function getPreorderCost() {
	return $this->preorderCost;
}
public function getDeletedItems(){
	$array = explode(",", $this->deleted_items);
	$items = array();
	foreach($array as $k => $v){
		$items[] = trim($v);
	}
	return $items;
}
public function getCountryId(){
	return $this->country;
}
public function getCountryName() {
	$pdo = PDOX::vrazka();
	$stm = $pdo -> prepare('SELECT * FROM `locations` WHERE `id`= ? LIMIT 1');
	$stm -> bindValue(1, $this->getCountryId(), PDO::PARAM_INT);
	$stm -> execute();
	if($stm->rowCount() > 0){
		$rez = $stm -> fetch();
		return $rez['country_name'];
	}
	else {
		return false;
	}
}
public function getPaypal(){
	return $this->paypal;
}

public function getPaypalPaid(){
	return $this->paypal_paid;
}

public function getPaymentType() {
    return $this->paymentType;
}

public function getIban() {
    $pdo = PDOX::vrazka();
    $stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :iban LIMIT 1");
    $stm->bindValue(':iban', 'iban', PDO::PARAM_STR);
    $stm -> execute();
    if($stm->rowCount() > 0){
        $rez = $stm -> fetch();
        return $rez['value'];
    }
    else {
        return false;
    }
}

public function getBankDetails() {
    $pdo = PDOX::vrazka();
    $stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :preorder LIMIT 1");
    $stm->bindValue(':preorder', "bank_details_" . lang_prefix, PDO::PARAM_STR);
    $stm -> execute();
    if($stm->rowCount() > 0){
        $rez = $stm -> fetch();
        return $rez['value'];
    }
    else {
        return false;
    }
}

public function isBankPayment() {
	if ($this->paymentType == "bank") return true;

	return false;
}

} // class
