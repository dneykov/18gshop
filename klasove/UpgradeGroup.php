<?php

class UpgradeGroup
{
    private $id;
    private $name;
    private $ime_all;
    private $vid_id;

    function __construct($a)
    {
        $pdo = PDOX::vrazka();

        if (is_int($a)) {
            $stm = $pdo->prepare('SELECT * FROM `mode_group_option` WHERE `id` = ? ');
            $stm -> bindValue(1, $a, PDO::PARAM_INT);
            $stm -> execute();

            if ($stm->rowCount() == 0 ) {
                greshka('Stm row count = 0 ');
            } else {
                $a = $stm -> fetch();
            }
        }

        if (is_array($a)) {
            $this->id = $a['id'];
            $this->name = $a['ime_'.lang_prefix];
            if(!$this->name) $this->name = $a['ime_'.lang_default_prefix];
            $this->vid_id = $a['mode_id'];

            global $__languages;

            foreach ($__languages as $v) {
                $this->ime_all[] = array(
                    'name' => $a['ime_' . $v->getPrefix()],
                    'lang_name' => $v->getName(),
                    'lang_prefix' => $v->getPrefix()
                );
            }
        } else {
            greshka('категория конструктур грешка');
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIme($bez_html_special=FALSE)
    {
        if($bez_html_special == FALSE )return htmlspecialchars($this->name);
        else return $this->name;
    }

    public function getIme_all($bez_html_spec = false )
    {
        if($bez_html_spec == true ) return $this->ime_all;

        $temp = $this->ime_all;
        foreach ($temp as &$v) {
            $v['name'] = htmlspecialchars($v['name']);

        }
        unset($v);

        return $temp;
    }


    public function __get($name)
    {
        greshka('get');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }

    public function delete()
    {
        $pdo = PDOX::vrazka();
        $stm = $pdo->prepare('DELETE FROM `group_option_values` WHERE `group_option_id` = ?');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();

        $stm = $pdo->prepare('DELETE FROM `mode_group_option` WHERE `id` = ?');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();
    }
}
