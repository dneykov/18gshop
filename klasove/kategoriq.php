<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class kategoriq{
    private $ime;
    private $id;
	private $image;
	private $vidove;


    function __construct($a) {
        $pdo = PDOX::vrazka();


        if(is_int($a)){
        $stm = $pdo->prepare('SELECT * FROM `kategorii` WHERE `id` = ? ');
        $stm -> bindValue(1, $a, PDO::PARAM_INT);
        $stm -> execute();

        if($stm->rowCount() == 0 ) greshka('Stm row count = 0 ');

        else $a = $stm -> fetch();


        }

        if(is_array($a)){


            $this->id = $a['id'];

            
            $this->ime = $a['ime_'.lang_prefix];
            if(!$this->ime) $this->ime = $a['ime_'.lang_default_prefix];



           





        }
        else greshka('категория конструктур грешка');
    }





    public function getIme($bez_html_specials = false) {
        if($bez_html_specials == false ) return htmlspecialchars($this->ime);
        else return $this->ime;
    }

    public function getId() {
        return $this->id;
    }

    public function getBroi_artikuli($branch=Null,$promo=false) {		
		$pdo = PDOX::vrazka();
        $sql='SELECT COUNT(`id`) AS `broi` FROM `artikuli` WHERE `id_kategoriq` = :id AND isAvaliable=1 AND IsOnline=1';
		if(isset ($branch)){	
			$sql .= ' AND `id_model` IN (Select id From `model` where ( `ime` like :marka  )) ';
		}
		if($promo){
			$sql .= ' AND `cena_promo`>0 ';
		}
		$stm = $pdo->prepare($sql);
		$stm -> bindValue(':id', $this->getId(), PDO::PARAM_INT);
		if(isset ($branch)){	
			$stm -> bindValue(':marka', $branch, PDO::PARAM_STR);
		}
		
		$stm -> execute();

		$data = $stm->fetch();
		return $data[0];
		
    }

    public function getBundleCount() {
        $pdo = PDOX::vrazka();
        $sql='SELECT * FROM `group_products_zapisi` WHERE `cat_id` = :id GROUP BY `group_id`';
//        if(isset ($branch)){
//            $sql .= ' AND `id_model` IN (Select id From `model` where ( `ime` like :marka  )) ';
//        }
//        if($promo){
//            $sql .= ' AND `cena_promo`>0 ';
//        }
        $stm = $pdo->prepare($sql);
        $stm -> bindValue(':id', $this->getId(), PDO::PARAM_INT);
//        if(isset ($branch)){
//            $stm -> bindValue(':marka', $branch, PDO::PARAM_STR);
//        }

        $stm -> execute();
        return $stm->rowCount();

    }

	public function getImage() {
		if (!isset($this->image)){
			$pdo = PDOX::vrazka();
			
			$stm = $pdo->prepare('SELECT `kartinka`  FROM `artikuli` WHERE `id_kategoriq` = ? AND IsOnline=1 limit 1');
            $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
            $stm -> execute();

            $data = $stm->fetch();
            $this->image = $data[0];
		}
        return $this->image;
    }

	public function getVidove(){

		if(!isset($this->vidove)){
			$pdo = PDOX::vrazka();
			$stm = $pdo -> prepare('SELECT * FROM `vid` WHERE `id_kategoriq` = ? ORDER BY `ordr` ');
			$stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
			$stm -> execute();
   
			foreach ($stm->fetchAll() as $v) {			
				$temp_vid = new vid($v);
				if($temp_vid->getBroi_artikuli() == 0 ) continue;
				$this->vidove[] =  $temp_vid;
            } 
		}
		return $this->vidove;
	}



    public function __get($name) {
    greshka('get');
}

public function __set($name, $value) {
    greshka('set');
}


}

?>
