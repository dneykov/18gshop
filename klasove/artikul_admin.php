<?php

class artikul_admin extends artikul
{
    private $cena_dostavna;
    private $dop_info_all;
    private $preorder_info_all;
    private $dop_info2_all;
    private $title_all;
    private $specification_all;


    public function __construct($a)
    {
        try {
            parent::__construct($a);
            $pdo = PDOX::vrazka();

            $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id` = ? ');
            $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
            $stm->execute();

            if ($stm->rowCount() == 0) greshka('admin produkt gre6ka RC = 0 ');

            $temp_data = $stm->fetch();

            global $__languages;

            foreach ($__languages as $v) {

                $this->dop_info_all[] = array(
                    'name' => $temp_data['dop_info_' . $v->getPrefix()],
                    'lang_name' => $v->getName(),
                    'lang_prefix' => $v->getPrefix()
                );
                $this->preorder_info_all[] = array(
                    'name' => $temp_data['preorder_' . $v->getPrefix()],
                    'lang_name' => $v->getName(),
                    'lang_prefix' => $v->getPrefix()
                );
                $this->title_all[] = array(
                    'name' => $temp_data['title_' . $v->getPrefix()],
                    'lang_name' => $v->getName(),
                    'lang_prefix' => $v->getPrefix()
                );
                $this->specification_all[] = array(
                    'name' => $temp_data['spec_' . $v->getPrefix()],
                    'lang_name' => $v->getName(),
                    'lang_prefix' => $v->getPrefix()
                );
            }

            $this->cena_dostavna = $temp_data['cena_dostavna'];

            if ($this->getKatinka_dopalnitelni()) {

                foreach ($this->katinka_dopalnitelni as &$v) {
                    if ($v) $v = new kartinka_admin($v->getKatinka());
                    else $v = NULL;

                }
                unset($v);
            }
            for ($q = 1; $q <= 12; $q++) {

                if (isset($this->katinka_dopalnitelni[$q])) {
                    $this->katinka_dopalnitelni[$q] = new kartinka_admin($this->katinka_dopalnitelni[$q]->getKatinka());
                }
            }

        } catch (Exception $e) {
            greshka($e);
        }

    }

    public function delete()
    {

        delete_file(dir_root . $this->getKatinka());
        delete_file(dir_root . str_replace("thumbnail", "resize", $this->getKatinka()));
        delete_file(dir_root . str_replace("thumbnail", "resize", $this->getKartinka_org()));
        delete_file(dir_root . $this->getKartinka_b());
        delete_file(dir_root . $this->getKartinka_t());

        $tmp_ddopImg = $this->getKatinka_dopalnitelni();
        if (isset($tmp_ddopImg) && (sizeof($tmp_ddopImg) > 0)) foreach ($tmp_ddopImg as $v) {

            if (empty ($v)) continue;

            delete_file(dir_root . $v->getKatinka());
            delete_file(dir_root . str_replace("thumbnail", "resize", $this->getKatinka()));
            delete_file(dir_root . str_replace("thumbnail", "resize", $this->getKartinka_org()));
            delete_file(dir_root . $v->getKartinka_t());
            delete_file(dir_root . $v->getKartinka_b());
        }


        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('DELETE FROM `artikuli` WHERE `id` = ?;');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();
    }

    public function virtualDelete($vid)
    {
        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('DELETE FROM `product_vid` WHERE `artikul_id` = ? AND vid_id = ?;');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->bindValue(2, $vid, PDO::PARAM_INT);
        $stm->execute();
    }

    public function offline()
    {
        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('UPDATE `artikuli` SET IsOnline=0 WHERE `id` = ? LIMIT 1 ');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();
    }

    public function online()
    {
        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('UPDATE `artikuli` SET IsOnline=1 WHERE `id` = ? LIMIT 1 ');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();
    }

    public function notavaliable()
    {
        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('UPDATE `artikuli` SET isAvaliable=0 WHERE `id` = ? LIMIT 1 ');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();

        $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
        $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
        $stm_disable_group_product -> bindValue(2, $this->getId(), PDO::PARAM_INT);
        $stm_disable_group_product -> execute();
    }

    public function avaliable()
    {
        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('UPDATE `artikuli` SET isAvaliable=1 WHERE `id` = ? LIMIT 1 ');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();

        $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
        $stm_enable_group_product -> bindValue(1, 1, PDO::PARAM_STR);
        $stm_enable_group_product -> bindValue(2, $this->getId(), PDO::PARAM_INT);
        $stm_enable_group_product -> execute();
    }

    public function makeNew() {
        $pdo = PDOX::vrazka();

        $stm = $pdo->prepare('UPDATE `artikuli` SET creat_date = NOW() WHERE `id` = ?');
        $stm->bindValue(1, $this->getId(), PDO::PARAM_INT);
        $stm->execute();
    }

    public function delete_kartinka()
    {
        delete_file(dir_root . $this->getKatinka());
        delete_file(dir_root . $this->getKartinka_b());
        delete_file(dir_root . $this->getKartinka_t());
    }


    public function getCena_pechalba()
    {
        if ($this->cena_dostavna) {

            if ($this->getCena_promo()) return $this->getCena_promo() - $this->getCena_dostavna();
            if ($this->getCena()) return $this->getCena() - $this->getCena_dostavna();
        } else return 0;
    }

    public function getCena_dostavna()
    {
        return $this->cena_dostavna;
    }


    public function image_sort_empty()
    {
        /*
        $pdo = PDOX::vrazka();

        $image_extra = $this->getKatinka_dopalnitelni();

        $q_empty_count_images = 0;
        $q_real_count_images = 1;
        $temp_images = NULL;

        for($q=1;$q<=12;$q++){

        if($image_extra[$q]) {
            $temp_images[$q_real_count_images] = $image_extra[$q];
            $q_real_count_images++;

        }
        else {
            $temp_images[12-$q_empty_count_images] = '';
            $q_empty_count_images++;

        }
    }




        for($q=1;$q<=12;$q++){

        if($temp_images[$q]) {


            $image_ime_temp = $temp_images[$q]-> getKartinka_t();
            $image_ime_temp_renamed = preg_replace('|^(.*)extra\-[0-9]+\_(.*)$|is', '$1extra-'.$q.'_$2', $image_ime_temp);

            rename(dir_root.$image_ime_temp, dir_root.$image_ime_temp_renamed);

            $image_ime_temp = $temp_images[$q]-> getKartinka_b();
            $image_ime_temp_renamed = preg_replace('|^(.*)extra\-[0-9]+\_(.*)$|is', '$1extra-'.$q.'_$2', $image_ime_temp);
            rename(dir_root.$image_ime_temp, dir_root.$image_ime_temp_renamed);

            $image_ime_temp = $temp_images[$q]-> getKatinka();
            $image_ime_temp_renamed = preg_replace('|^(.*)extra\-[0-9]+\.(.*)$|is', '$1extra-'.$q.'.$2', $image_ime_temp);
            rename(dir_root.$image_ime_temp, dir_root.$image_ime_temp_renamed);


            $stm = $pdo->prepare('UPDATE `artikuli` SET `img'.$q.'` = ? WHERE `id` = ? ');
            $stm -> bindValue(1, $image_ime_temp_renamed, PDO::PARAM_STR);
            $stm -> bindValue(2, $this->getId(), PDO::PARAM_INT);
            $stm -> execute();

        }

        else {
             $stm = $pdo->prepare('UPDATE `artikuli` SET `img'.$q.'` = "" WHERE `id` = ? ');
            $stm -> bindValue(1, $this->getId(), PDO::PARAM_INT);
            $stm -> execute();



        }



        }
        */
    }


    public function getTitleAll($bez_html_spec = false)
    {
        if ($bez_html_spec == FALSE) return $this->title_all;

        greshka('ned. f-q title_all admin produkt');
    }

    public function getDop_info_all($bez_html_spec = false)
    {
        if ($bez_html_spec == FALSE) return $this->dop_info_all;

        greshka('ned. f-q dop_info_all admin produkt');
    }

    public function getPreorder_info_all($bez_html_spec = false)
    {
        if ($bez_html_spec == FALSE) return $this->preorder_info_all;

        greshka('ned. f-q preorder_info_all admin produkt');
    }

    public function getDop_info2_all($bez_html_spec = false)
    {
        if ($bez_html_spec == FALSE) return $this->dop_info2_all;

        greshka('ned. f-q dop_info2_all admin produkt');
    }

    public function getSpecification_all($bez_html_spec = false)
    {
        if ($bez_html_spec == FALSE) return $this->specification_all;

        greshka('ned. f-q dop_info2_all admin produkt');
    }

    public function cache_search()
    {
        try {
            $pdo = PDOX::vrazka();

            $search_cache = NULL;
            $search_cache .= '<id>' . $this->getId() . '</id>' . PHP_EOL;
            $search_cache .= '<kod>' . $this->getKod() . '</kod>' . PHP_EOL;
            $search_cache .= '<ime>' . htmlspecialchars($this->getTitle() . " " . $this->getIme(1)) . '</ime>' . PHP_EOL;
            $search_cache .= '<marka>' . $this->getIme_marka(false) . '</marka>' . PHP_EOL;
            if (!empty($this->getWarehouseCode())) {
                $search_cache .= '<warehouse_code>' . $this->getWarehouseCode() . '</warehouse_code>' . PHP_EOL; // cache
            } else if (is_array($this->getTaggroup_dropdown())) {
                foreach ($this->getTaggroup_dropdown() as $taggroup) {
                    foreach ($taggroup->getEtiketi() as $e) {
                        if (!empty($this->getWarehouseTagCode($e->getId()))) {
                            $search_cache .= '<warehouse_code>' . $this->getWarehouseTagCode($e->getId()) . '</warehouse_code>' . PHP_EOL; // cache
                        }
                    }
                }
            }

            $stm = $pdo->prepare('UPDATE `artikuli` SET `cache_search` = ? WHERE `id` = ? LIMIT 1');
            $stm->bindValue(1, $search_cache, PDO::PARAM_STR);
            $stm->bindValue(2, $this->getId(), PDO::PARAM_INT);
            $stm->execute();

        } catch (Exception $e) {
            greshka($e);
        }
    }

    public function cache_update()
    {

        try {

            $pdo = PDOX::vrazka();
            global $__languages;

            $etiketi_cache = NULL; // look for 'cache'
            $etiketi_cache .= '<id>' . $this->getId() . '</id>' . PHP_EOL; // cache
            $etiketi_cache .= '<kod>' . $this->getKod() . '</kod>' . PHP_EOL;
            $etiketi_cache .= '<ime>' . htmlspecialchars($this->getIme(1)) . '</ime>' . PHP_EOL; // cache


            $kategoriq = new kategoriq_admin((int)$this->getId_kategoriq());
            $etiketi_cache .= '<kategoriq>' . $kategoriq->getId() . '</kategoriq>' . PHP_EOL;
            foreach ($kategoriq->getIme_all(1) as $v) {
                $etiketi_cache .= '<kategoriq_ime_' . $v['lang_prefix'] . '>' . htmlspecialchars($v['name']) . '</kategoriq_ime_' . $v['lang_prefix'] . '>' . PHP_EOL; // cache
            }

            $vid = new vid_admin((int)$this->getId_vid());
            $etiketi_cache .= '<vid>' . $vid->getId() . '</vid>' . PHP_EOL;
            foreach ($vid->getIme_all(1) as $v) {
                $etiketi_cache .= '<vid_ime_' . $v['lang_prefix'] . '>' . htmlspecialchars($v['name']) . '</vid_ime_' . $v['lang_prefix'] . '>' . PHP_EOL; // cache
            }

            $etiketi_cache .= '<model>' . $this->getId_model() . '</model>' . PHP_EOL;
            $etiketi_cache .= '<model_ime>' . $this->getIme_marka(false) . '</model_ime>' . PHP_EOL;
            $etiketi_cache .= '<warehouse_code>' . $this->getWarehouseCode() . '</warehouse_code>' . PHP_EOL;

            if ($this->getEtiketizapisi()) foreach ($this->getEtiketizapisi() as $v) {
                /* @var $v etiket */

                $temp = new etiket_admin((int)$v->getEtiket()->getId());
                $etiketi_cache .= '<warehouse_code>' . $this->getWarehouseTagCode($temp->getId()) . '</warehouse_code>' . PHP_EOL;

                foreach ($temp->getIme_etiket_grupa_all(true) as $g) {
                    if (preg_match('|\<etiket\_grupa\>' . $g['id_grupa'] . '\<\/etiket\_grupa\>|is', $etiketi_cache)) {
                        $etiketi_cache .= '<etiket_grupa_ime_' . $g['prefix'] . '>' . htmlspecialchars($g['name']) . '</etiket_grupa_ime_' . $g['prefix'] . '>' . PHP_EOL;
                        continue;
                    }

                    $etiketi_cache .= '<etiket_grupa>' . $g['id_grupa'] . '</etiket_grupa>' . PHP_EOL;
                    $etiketi_cache .= '<etiket_grupa_ime_' . $g['prefix'] . '>' . htmlspecialchars($g['name']) . '</etiket_grupa_ime_' . $g['prefix'] . '>' . PHP_EOL;
                }

                $etiketi_cache .= '<etiket>' . $temp->getId() . '</etiket>' . PHP_EOL;
                foreach ($temp->getIme_all(true) as $e) {
                    $etiketi_cache .= '<etiket_ime_' . $e['lang_prefix'] . '>' . htmlspecialchars($e['name']) . '</etiket_ime_' . $e['lang_prefix'] . '>' . PHP_EOL;
                }
            }

            foreach ($this->getDop_info_all() as $v) {
                $etiketi_cache .= '<desctiption_' . $v['lang_prefix'] . '>' . htmlspecialchars($v['name']) . '</desctiption_' . $v['lang_prefix'] . '>' . PHP_EOL; // cache
            }
            $etiketi_cache .= '<desctiption2>' . htmlspecialchars($this->getDopInfo()) . '</desctiption2>' . PHP_EOL; // cache


            $stm = $pdo->prepare('UPDATE `artikuli` SET `cache_etiketi` = ? WHERE `id` = ? LIMIT 1');
            $stm->bindValue(1, $etiketi_cache, PDO::PARAM_STR);
            $stm->bindValue(2, $this->getId(), PDO::PARAM_INT);
            $stm->execute();

        } catch (Exception $e) {
            greshka($e);
        }
    }


    public function __get($name)
    {
        greshka('get');
    }

    public function __set($name, $value)
    {
        greshka('set');
    }

}
