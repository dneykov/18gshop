<?php
require_once 'main.php';

$stm = $pdo->prepare("UPDATE `orders` SET `paypal_paid`=? WHERE scode=?");
$stm->bindValue(1, 1, PDO::PARAM_INT);
$stm->bindValue(2, $_GET['s'], PDO::PARAM_INT);
$stm->execute();
header("Location: " . url);
