<?php

/* @var $pdo pdo */
/* @var $stm PDOStatement */

require '__top.php';

if (isset ($_GET['page'])) {
    $page = (int)$_GET['page'];
} else {
    $page = 0;
}

if(!isset ($_GET['category'])) greshka('Не е посочено ид ГЕТ', 'Не е посочена атегория');

$kategoriq = new kategoriq((int)$_GET['category']);

if(isset ($_GET['mode'])) {
    $vid = new vid((int)$_GET['mode']);
} else {
    $vid = NULL;
}

$stm = $pdo -> prepare('SELECT * FROM `vid` WHERE `id_kategoriq` = ? ORDER BY `ordr` ');
$stm -> bindValue(1, $kategoriq->getId(), PDO::PARAM_INT);
$stm -> execute();

$vidove = NULL;
foreach ($stm->fetchAll() as $v) {
    $vidove[] = new vid($v);
}

$stm = $pdo->prepare('SELECT * FROM `model` WHERE `id_kategoriq` = ? ORDER BY `ime` ');
$stm -> bindValue(1, $kategoriq->getId(), PDO::PARAM_INT);
$stm -> execute();

if ($stm -> rowCount() == 0) {
    $modeli = NULL;
} else {
    foreach ($stm->fetchAll() as $v) {
        if($vid) {
            $modeli[] = new model($v,$vid->getId());
        } else {
            $modeli[] = new model($v);
        }
    }
    unset($v);
}


if (isset($_GET['model'])) {
    $model = new model((int)$_GET['model']);
} else {
    $model = NULL;
}


if (isset($_GET['tag'])) {
    $etiket = $_GET['tag'];
} else {
    $etiket = NULL;
}

if($vid) {
    $etiket_izpolzvani = NULL;

    $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ORDER BY `ime_' . lang_prefix . '`');
    $stm -> bindValue(1, $vid->getId(), PDO::PARAM_INT);
    $stm -> execute();

    if($stm->rowCount() == 0 ) {
        $etiketi_grupi = NULL;
    } else {
        foreach ($stm->fetchAll() as $v) {
            if ($model) {
                $temp_etiket_model_id = $model->getId();
            } else {
                $temp_etiket_model_id = 0;
            }

            $temp_taggrupa = new etiket_grupa_admin($v, $temp_etiket_model_id,$etiket_izpolzvani);

            $etiketi_grupi[] = $temp_taggrupa;

            if ($etiket && $temp_taggrupa->getEtiketi()) {
                foreach ($temp_taggrupa->getEtiketi() as $temp_etiket_zaeti) {
                    if (in_array($temp_etiket_zaeti->getId(), $etiket)) {
                        $etiket_izpolzvani[] = $temp_etiket_zaeti->getId();
                        break;
                    }
                }
            }

            unset($temp_etiket_model_id, $temp_taggrupa);

        }
    }
} else {
    $etiketi_grupi = NULL;
}

if(isset($_GET['filter']) && $_GET['filter'] == 'last') {
    $filter = '`creat_date` DESC';
} else if(isset($_GET['filter']) && $_GET['filter'] == 'low-price') {
    $filter = '`cena` ASC';
}  else if(isset($_GET['filter']) && $_GET['filter'] == 'high-price') {
    $filter = '`cena` DESC';
} else {
    $filter = '`creat_date` DESC';
}

if($vid) {
    if ($vid->isVirtual()) {
        $sql = 'SELECT * FROM `artikuli` WHERE ( ';
    } else {
        $sql = 'SELECT * FROM `artikuli` WHERE ( `id_kategoriq` = :cat ';
    }
} else {
    $sql = 'SELECT * FROM `artikuli` WHERE ( `id_kategoriq` = :cat ';
}

if($vid) {
    if ($vid->isVirtual()) {
        $sql .= '`id` IN (SELECT `artikul_id` FROM `product_vid` WHERE `vid_id` = :vid)';
    } else {
        $sql .= ' AND `id_vid` = :vid ';
    }
}

if (isset($_GET['availability']) && $_GET['availability'] == 'all') {
    $sql .= '';
} elseif (isset($_GET['availability']) && $_GET['availability'] == 'online') {
    $sql .= ' AND `IsOnline` = 1 AND `isAvaliable` = 1 ';
} elseif (isset($_GET['availability']) && $_GET['availability'] == 'offline') {
    $sql .= ' AND `IsOnline` = 0 ';
} elseif (isset($_GET['availability']) && $_GET['availability'] == 'na') {
    $sql .= 'AND `isAvaliable` = 0 ';
} else {
    $sql .= '';
}

if($model) $sql .= ' AND `id_model` = :model ';
if(isset($_GET['preorder'])) $sql .= ' AND `preorder` = :preorder ';

if($etiket) foreach ($etiket as $e_k => $e_v) {
    $sql .= ' AND `cache_etiketi` LIKE (:etiket'.$e_k.') ';
}


$sql .= ') ORDER BY '.$filter.' LIMIT :page,50';

$stm = $pdo -> prepare($sql);

$stm -> bindValue(':page', $page*50, PDO::PARAM_INT);

if($vid) {
    if (!$vid->isVirtual()) {
        $stm -> bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
    }
} else {
    $stm -> bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
}

if($vid) {
    $stm -> bindValue(':vid', $vid->getId(), PDO::PARAM_INT);
}
if($model) $stm -> bindValue(':model', $model->getId(), PDO::PARAM_INT);
if(isset($_GET['preorder'])) $stm -> bindValue(':preorder', 1, PDO::PARAM_INT);

if($etiket) foreach ($etiket as $e_k => $e_v) {
    $stm->bindValue(':etiket'.$e_k, '%<etiket>'.$e_v.'</etiket>%', PDO::PARAM_STR);
}

$stm -> execute();

$artikuli = NULL;
if ( $stm->rowCount() ) foreach ($stm->fetchAll() as $v) {
    $temp = new artikul_admin($v);
    if($temp->getEtiketizapisi()) {
        foreach ($temp->getEtiketizapisi() as $t) {
            $t->getEtiket()->getIme_etiket_grupa();
        }
    }

    $artikuli[] = $temp;

    unset($temp);
}
$broi_pokazani = $stm->rowCount();

$sql_broi=str_replace ('SELECT *','SELECT count(*) as `broi`',$sql);
$sql_broi=str_replace (['DESC LIMIT :page,50', 'ASC LIMIT :page,50'],';',$sql_broi);


$stm = $pdo -> prepare($sql_broi);

if($vid) {
    if (!$vid->isVirtual()) {
        $stm -> bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
    }
} else {
    $stm -> bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);
}

if($vid)  $stm -> bindValue(':vid', $vid->getId(), PDO::PARAM_INT);
if($model) $stm -> bindValue(':model', $model->getId(), PDO::PARAM_INT);
if(isset($_GET['preorder'])) $stm -> bindValue(':preorder', 1, PDO::PARAM_INT);

if($etiket) foreach ($etiket as $e_k => $e_v) {
    $stm->bindValue(':etiket'.$e_k, '%<etiket>'.$e_v.'</etiket>%', PDO::PARAM_STR);
}

$stm -> execute();

$rez=$stm->fetchAll() ;
$all_artikuli_broi=$rez[0]['broi'];

$sql_preorder = 'SELECT COUNT(*) as `broi` FROM `artikuli` WHERE ( `id_kategoriq` = :cat ';
if($vid) $sql_preorder .= ' AND `id_vid` = :vid ';
if($model) $sql_preorder .= ' AND `id_model` = :model ';
$sql_preorder .= ' AND `preorder` = :preorder ';

if($etiket) foreach ($etiket as $e_k => $e_v) {
    $sql_preorder .= ' AND `cache_etiketi` LIKE (:etiket'.$e_k.') ';
}
$sql_preorder .= ') ORDER BY `id`';
$stm_preorder = $pdo->prepare($sql_preorder);

$stm_preorder->bindValue(':cat', $kategoriq->getId(), PDO::PARAM_INT);

if($vid)  $stm_preorder->bindValue(':vid', $vid->getId(), PDO::PARAM_INT);
if($model) $stm_preorder->bindValue(':model', $model->getId(), PDO::PARAM_INT);
$stm_preorder->bindValue(':preorder', 1, PDO::PARAM_INT);


if($etiket) foreach ($etiket as $e_k => $e_v) {
    $stm_preorder->bindValue(':etiket'.$e_k, '%<etiket>'.$e_v.'</etiket>%', PDO::PARAM_STR);
}

$countPreorder = $stm_preorder->execute();
$countPreorder = $stm_preorder->fetchAll();
$countPreorder = $countPreorder[0]['broi'];

if(isset($_GET['preorder'])) {
    $_SESSION['preorder'] = true;
} else {
    $_SESSION['preorder'] = false;
}

require dir_root_admin.'template/browse.php';
