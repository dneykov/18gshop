<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../__top.php';
$maxfilesize = 32 * 1024 * 1024;
$fileTypes = array('xlsx', 'doc', 'docx', 'xls', 'xlsx'); // Allowed file extensions
if($_POST['image_form_submit'] == 1)
{
    $images_arr = array();
    foreach($_FILES['images']['name'] as $key=>$val){
        $image_name = $_FILES['images']['name'][$key];
        $tmp_name 	= $_FILES['images']['tmp_name'][$key];
        $size 		= $_FILES['images']['size'][$key];
        $type 		= $_FILES['images']['type'][$key];
        $error 		= $_FILES['images']['error'][$key];
        $fileParts = pathinfo($_FILES['images']['name'][$key]);

        ############ Remove comments if you want to upload and stored images into the "uploads/" folder #############

        $target_dir = dir_root . "storage/";
        $url_file = url . "storage/";
        $target_file = $target_dir.$_FILES['images']['name'][$key];
        $target_url = $url_file.$_FILES['images']['name'][$key];
        if($size < $maxfilesize) {
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                if(move_uploaded_file($_FILES['images']['tmp_name'][$key],$target_file)){
                    $images_arr[] = $_FILES['images']['name'][$key];
                }
            } else {
                $_SESSION['file_errors'][] = $_FILES['images']['name'][$key] . " не е качен, ." . $fileParts['extension'] . " файл не е позволен.";
            }
        } else {
            $_SESSION['file_errors'][] = $_FILES['images']['name'][$key] . " не е качен. Максималният размер на файла е 32MB.";
        }
    }

    //Generate images view
    if(!empty($images_arr)){ $count=0;
        foreach($images_arr as $image_src){ $count++?>
            <tr>
                <td style="padding-left:5px;max-width: 500px; padding-right:10px;"><a href="<?php echo url . 'storage/' . $image_src; ?>" target="_blank"><?php echo $image_src; ?></a></td>
                <td style="padding-left:5px;padding-right:10px"><a href="#" onclick=""><?php lang_delete_x(); ?></a></td>
            </tr>
        <?php }
    }
}
?>