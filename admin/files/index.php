<?php

/* @var $pdo pdo */
/* @var $stm PDOStatement */

require dir_root_admin.'template/__top.php';

?>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.fileuploadmulti.min.js"></script>
<link type="text/css" href="<?php echo url; ?>css/uploadfilemulti.css" rel="stylesheet" />
<style>
    #navigation {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #navigation a {
        margin-left: 10px;
        margin-right: 30px;
        font-size: 13px;
        color: #333;
    }
    ul, ol, li {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .gallery{ width:100%; float:left; margin-top:30px;}
    .gallery ul{ margin:0; padding:0; list-style-type:none;}
    .gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
    .gallery img{ width:250px;}
    .none{ display:none;}
    .upload_div{ margin-bottom:15px;}
    .uploading{ margin-top:15px;}
    .storage_files td{
        font-size: 9pt;
        white-space: nowrap;
        overflow: hidden;
        o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        height: 20px;
    }
    .storage_files {
        margin-top: 10px;
        margin-left: 10px;
        max-width: 1000px;
        border: none;
        margin-bottom: 50px;
    }
    .storage_files tr {
        height: 20px;
    }
    .storage_files a {
        color: #333;
    }
    .storage_files tr td a{
        width: 100%;
        display: block;
    }
    .storage_files tr:hover{
        background-color: #F7931E !important;
    }
    .storage_files tr:hover td > a{
        color: #FFF !important;
    }
    #tablehead {
        color: #ccc;
    }
    #tablehead:hover {
        background-color: #999 !important;
    }
    .productLink:hover{
        text-decoration:none;
    }
    .storage_files{
        border-collapse: collapse;
    }
    .delete:hover {
        color: #ff0000!important;
    }
    .storage_files tr:nth-child(even) {
        background-color: #fff;
        color: #333;
    }
    .storage_files tr:nth-child(even) {
        background-color: #f2f2f2;
        color: #333;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('#images').change(function() {
            $('#multiple_upload_form').ajaxForm({
                beforeSubmit:function(e){
                    $('.uploading').show();
                },
                success:function(data){
                    $('.uploading').hide();
                    setTimeout(function () {
                        location.reload();
                    }, 100);
                },
                error:function(e){
                }
            }).submit();
        });

        $(".delete").click(function () {
            var file = $(this).attr("data-file");
            delete_file(file);
        });
    });

    function delete_file(file){
        $(".modal-top-lenta-text").text("Are you sure you want to remove this?");
        $(".modal-body").html('<div style="width: 100%; height: 40px;">' + file + '</div> <button class="button-cancel" onclick="hideModal();" style="position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="delete_file_Ajax(\'' + file + '\');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
        $(".modal").show();
    }

    function delete_file_Ajax(file) {
        $.ajax({
            type: "POST",
            url: "files/delete_file.php",
            data: "file=" + file,
            cache: false,
            success: function(data){
                if(data == 1) {
                    location.reload();
                }
            }
        });
    }
</script>
<div id="navigation">
    <a href="<?php echo url."admin/help.php"?>">Oсновна информация</a>
    <a href="<?php echo url."admin/moduls/about.php"?>">Kак да поръчам</a>
    <a href="<?php echo url."admin/files.php"?>" style="color: #F8931F">Ценови листи</a>
    <a href="<?php echo url."admin/moduls/privacy.php"?>">Лични данни</a>
</div>
<div style="margin-left: 5px;margin-top: 15px;">
    <div style="margin:0px 0px 20px 10px;font-size:13pt;color:#fb921d;">Ценови листи</div>
    <div style="margin-left:10px;">
        <div class="upload_div">
            <form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="files/upload.php">
                <input type="hidden" name="image_form_submit" value="1"/>
                <input type="file" name="images[]" id="images" multiple><br>
                <span style="font-size: 11px; padding-top: 4px; display: inline-block">Позволени файлове .xlsx, .doc</span>
                <div class="uploading none">
                    <img src="images/uploading.gif"/>
                </div>
            </form>
        </div>
    </div>
</div>
<div style="margin-left: 10px; font-size: 14px; color: #ff0000;">
    <?php
    if(isset($_SESSION['file_errors'])) {
        echo '<ul>';
        foreach ($_SESSION['file_errors'] as $file_error) {
            echo "<li>$file_error</li>";
        }
        echo '</ul>';
        unset($_SESSION['file_errors']);
    }
    ?>
</div>
<table class="storage_files" cellpadding="3">
    <tr id="tablehead" style="background-color: #666666; color: #fff;">
        <td style="padding-left:5px;width: 800px; padding-right:10px;"><?php echo _('файл');?></td>
        <td></td>
    </tr>
    <?php if($storage_files) foreach ($storage_files as $file) { ?>
        <tr>
            <td style="padding-left:5px;max-width: 500px; padding-right:10px;"><a href="<?php echo url . "storage/" . $file; ?>" target="_blank"><?php echo $file; ?></a></td>
            <td style="padding-left:5px;padding-right:10px"><a href="#" onclick=""><span data-file="<?php echo $file; ?>" class="delete" style="color: #333; font-size: 8pt;">X</span></a></td>
        </tr>
    <?php } ?>
</table>
<div class="modal">

    <div class="modal-content">
        <div class="modal-top-lenta">
            <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
            <div class="modal-top-close" onclick="hideModal();">×</div>
        </div>

        <div class="modal-body">

        </div>
    </div>
</div>
