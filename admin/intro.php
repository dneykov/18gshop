<?php
require '../__top.php';
$languages = array();
foreach ($__languages as $language) {
    $languages['all'][] = array(
        'name'   => $language->getName(),
        'prefix' => $language->getPrefix(),
    );
}

$languages['current'] = lang_prefix;
unset($_SESSION['crop_thumb_image_location']);
unset($_SESSION['crop_large_image_location']);
unset($_SESSION['crop_html_img']);
$_SESSION['crop_upload_dir']='../../../images/banners/';

$_SESSION['crop_width']=1260;
$_SESSION['crop_height']=500;

try{
    if(isset ($_POST['adv_new'])) {

            $url = $_POST['adv_url_new'];
            $url = trim($url);

           if(isset($_POST['adv_text_new'])){
               $adv_text = $_POST['adv_text_new'];
           }else{
                $adv_text = '';
           }

           $adv_text = stripslashes($adv_text);
           $adv_text = trim($adv_text);

            $stm = $pdo->prepare('INSERT INTO `zz_banners` (podredba, image, url) VALUES(10, ?, ?)');

            $stm -> bindValue(1, $_POST['adv_file_new'], PDO::PARAM_STR);

            $stm -> bindValue(2, $url, PDO::PARAM_STR);

            $stm -> execute();
    }

    if(isset ($_POST['adv_submit'])) {
        $stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=10 ORDER BY `id` DESC');
        $stm -> execute();

        foreach ($stm->fetchAll() as $v)
        {
            if(isset($_POST['adv_delete_'.$v['id']])) {
                $thumbnailFilePath = pathinfo(dir_root . $v['image']);
                $resizeFilePath = pathinfo(dir_root . str_replace("thumbnail", "resize", $v['image']));

                delete_file(dir_root . $v['image']);
                delete_file(dir_root . str_replace("thumbnail", "resize", $v['image']));
                delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_tt." . $thumbnailFilePath['extension']);
                delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_t." . $thumbnailFilePath['extension']);
                delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_b." . $thumbnailFilePath['extension']);
                delete_file($resizeFilePath['dirname'] . "/" . $resizeFilePath['filename'] . "_org." . $resizeFilePath['extension']);

                 $stm = $pdo->prepare('DELETE FROM `zz_banners` WHERE `id` = ? ');
                 $stm -> bindValue(1, $v['id'], PDO::PARAM_INT);
                 $stm -> execute();

            }
        }
    }

    if(isset($_POST['adv_update'])) {

        if(isset($_POST['adv_delete_'.$_GET['id']])) {
            delete_file(dir_root.$_POST['adv_file']);
            $stm = $pdo->prepare('DELETE FROM `zz_banners` WHERE `id` = ? ');
            $stm -> bindValue(1, $_GET['id'], PDO::PARAM_INT);
            $stm -> execute();

            header("Location: ".url."admin/moduls/adv.php");
        } else {
            $stm = $pdo->prepare('SELECT * FROM `zz_banners` WHERE id = ? LIMIT 1');
            $stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
            $stm -> execute();
            $adv = $stm -> fetch();

            foreach ($__languages as $key => $v)  {
                $texts = unserialize($adv['adv_texts_temp_' . $v->getPrefix()]);

                $stm = $pdo->prepare('UPDATE `zz_banners` SET `image`= ?, `url`=?, `adv_texts_' . $v->getPrefix() . '`=?, `adv_texts_temp_' . $v->getPrefix() . '`=? WHERE id = ? ');
                $stm->bindValue(1, $_POST['adv_file'], PDO::PARAM_STR);
                $stm->bindValue(2, $_POST['adv_url'], PDO::PARAM_STR);
                $stm->bindValue(3, serialize($texts), PDO::PARAM_STR);
                $stm->bindValue(4, serialize($texts), PDO::PARAM_STR);
                $stm->bindValue(5, (int)$_GET['id'], PDO::PARAM_INT);
                $stm->execute();
            }
            header("Location: ".url."admin/moduls/adv.php");
        }
    }

    if(!isset($_GET['id'])){
        $stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba` ="10" order by `id` DESC ');
        $stm -> execute();
        $banners = $stm -> fetchAll();
    }
} catch (Exception $e){
    greshka($e);
}

require dir_root_admin_template.'__top.php';
require 'adv_menu.php';
?>
<script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
<script>
    var languages = <?php echo json_encode($languages); ?>;
    tinyMCE.init({
        // General options
        mode : "specific_textareas",
        editor_selector : "texts-class",
        theme : "advanced",
        width:"1260",
        height:"250",
        plugins : "template,paste,iespell,advlink",
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px,26px,30px,36px,40px,46px,50px,60px,72px",
        font_size_style_values: "12px,13px,14px,16px,18px,20px,26px,30px,36px,40px,46px,50px,60px,72px",
        entity_encoding : "raw",
        element_format : "html",
        entities : "160,nbsp,162,cent,8364,euro,163,pound",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,fontsizeselect,|,cut,copy,paste,pastetext,pasteword,|,undo,redo,|,bullist,numlist,|,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        language : "bg",

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo url_template_folder; ?>images/banners.css",

    });

	function adv_delete(id){

        is_checkbox = $('[name=adv_delete_'+id+']').attr('checked');

        if(is_checkbox) {
            $('[name=adv_url_'+id+']').val('');
            $('[name=adv_url_'+id+']').attr('disabled', 'true');

            $('[name=adv_file_'+id+']').val('');
            $('[name=adv_file_'+id+']').attr('disabled', 'true');
        }
        else {
            $('[name=adv_url_'+id+']').removeAttr('disabled');
            $('[name=adv_file_'+id+']').removeAttr('disabled');
        }
	}
</script>
<?php if(!isset($_GET['id'])) { ?>
	<div id="admin_body">
		<div style="font-size: 14px;display:block;margin-top: 10px;margin-left: 10px;"><a href="#" onclick="$('#new_banner_form').slideToggle();" style="color: #8CC43F;">+ нов банер</a></div>
		<form id="new_banner_form" style="margin:10px;width:1260px;display:none; font-size: 14px;" action="" method="post" enctype="multipart/form-data">
				<div>
                    <a  class="thickbox" href='crop/crop_intro.php?crop_html_img=adv_file_new&crop_width=1260&crop_height=500&crop_html_img_input_hidden=adv_file_new&keepThis=true&TB_iframe=true&height=650&width=1260'>+ снимка</a>
                    <img id="adv_file_new" src="<?php echo url?>/images/white1x1.png" style="width:1260px;height:500px;display:block;"/> <br/>
					<input type="hidden" name="adv_file_new" value="/images/white1x1.png">
				</div>
				<p>url</p>
				<input type="text" name="adv_url_new" value="" style="width: 500px;" /><br />
<!--				<p>текст</p>-->
<!--				<textarea name="adv_text_new" value="" style="width: 1000px; height: 50px;"></textarea><br /><br />-->
				<input style="float:right; margin-left: 5px;" type="button" value="<?php echo lang_cancel; ?>" onclick="window.location='<?php echo url.'admin/moduls/intro.php'; ?>'">
				<input style=" margin-top:0;float:right;" type="submit" name="adv_new" value="добави">
				<br /><br />
		</form>

		<form style="margin:20px 10px;"action="" method="post" enctype="multipart/form-data">
		<?php foreach ($banners as $v) { ?>
		    <div style="margin:0px 30px 30px 0px;width: 1260px; font-size: 14px;">
                <span style="color: #F7931E">1260x500</span><div style="float:right;">изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"></div><div style="float:right;margin-right: 20px;margin-left: 5px;"><a style="color: #8CC43F" href="<?php echo url.'admin/moduls/intro.php?id='.$v['id']; ?>">E</a></div>
				<br />
				<a href="<?php echo url.'admin/moduls/intro.php?id='.$v['id']; ?>"><img style="width: 1260px;height: 500px;" id="adv_file_<?php echo $v['id']; ?>" src="<?php echo url.htmlspecialchars($v['image']); ?>"/></a><br>
				<table border="0" cellspacing="3" cellpadding="3" style="width:1000px;">
			        <tr>
						<td><?php echo $v['url']; ?></td>
					</tr>
			        <tr>
			        	<td><div style="height:30px;"></div></td>
			        </tr>
		        </table>
			</div>

		<?php } ?>
		<div style="clear:both;"> </div>
		<input type="submit" name="adv_submit" value="<?php echo lang_save; ?>">
		<input type="button" value="<?php echo lang_cancel; ?>" onclick="window.location='<?php echo addslashes(url_admin); ?>'">
		</form>
	</div>
<?php } else {
		$stm = $pdo->prepare('SELECT * FROM `zz_banners` WHERE id = ? LIMIT 1');
		$stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
		$stm -> execute();
		$adv = $stm -> fetch();

        foreach ($__languages as $key => $v)  {
            $adv_texts[$v->getPrefix()] = unserialize($adv['adv_texts_' . $v->getPrefix()]);

            $stm = $pdo->prepare('UPDATE `zz_banners` SET `adv_texts_temp_' . $v->getPrefix() . '`=? WHERE id = ? ');
            $stm->bindValue(1, serialize($adv_texts[$v->getPrefix()]), PDO::PARAM_STR);
            $stm->bindValue(2, (int)$_GET['id'], PDO::PARAM_INT);
            $stm->execute();
        }
?>
	<div id="admin_body">
		<style type="text/css">
            .mce-reset label {
                max-width: none !important;
            }
			.articles_attached a{
				color:#FFF;
			}
            .addbutton {
                font-size: 14px;
                margin: 0;
            }
			.undertext {
				position:absolute;
				border:1px solid #f9f9f9;
				background:#FFF;
				color:#333333;
				visibility: hidden;
				width:177px;
			}
			.undertext li {
				visibility: visible;
				display:block;
				border-bottom:1px solid #f9f9f9;
				padding:2px 4px;
				background: #fff;
			}

			.undertext li.hovered {
				background:#CCC;
				color:#000;
				text-shadow:0px 1px #DDD;
				cursor:pointer;
			}
			#baner-show {
				position: relative;
			}
			#baner-show p {
				margin: 0;
				padding: 0;
			}
			#bnr-container {
				position: relative;
				width: 1000px;
				display: block;
			}
			.adv-text {
				position: absolute;
				padding: 10px;
			}
			.adv-place-1 {
				top: 20px;
				left: 0px;
				text-align: left;
			}
			.adv-place-2 {
				top: 20px;
				left: 50%;
                transform:translate(-50%, 0);
				text-align: center;
			}
			.adv-place-3 {
				top: 20px;
				right: 0px;
				text-align: right;
			}

            .adv-place-4 {
                top: calc(50% - 25px);
                left: 0px;
                text-align: left;
            }
            .adv-place-5 {
                top: calc(50% - 25px);
                left: 50%;
                transform:translate(-50%, 0);
                text-align: center;
            }
            .adv-place-6 {
                top: calc(50% - 25px);
                right: 0px;
                text-align: right;
            }

            .adv-place-7 {
                bottom: 20px;
                left: 0;
                text-align: left;
            }
            .adv-place-8 {
                bottom: 20px;
                left: 50%;
                transform:translate(-50%, 0);
                text-align: center;
            }
            .adv-place-9 {
                bottom: 20px;
                right: 0;
                text-align: right;
            }
            .adv-text a, .texts-links a {
                color: inherit;
                font-size: inherit;
            }
		</style>

		<script type="text/javascript">
			var globalTimeout = null;

			$(document).bind('click', function(e) {

				var $clicked = $(e.target);

				if ( !$clicked.parents().hasClass("underText") )
				{
					$('.underText').remove();
				}
			});
		</script>
		<form style="margin:10px 10px;" action="" method="post" enctype="multipart/form-data">
			<div style="margin:0px 30px 0px 0px; width:1260px; height: 420px;">
				<span style="display:inline-block; width: 1090px; font-size: 13px;">Url <input type="text" style="width:500px;" name="adv_url" value="<?php echo $adv['url']; ?>"></span> <span style="font-size: 13px;">1260x500</span> <a style='font-size: 13px;' class="thickbox" href='crop/crop_intro_details.php?crop_html_img_b=adv_file&crop_width=1260&crop_height=500&crop_html_img_input_hidden=adv_file_input&crop_large_image_location=../../../<?php echo str_replace("thumbnail","resize",$adv['image']); ?>&keepThis=true&TB_iframe=true&height=650&width=1260'>Редактиране</a>
				<div style="height:3px;"></div>
				<div id="baner-show">
					<img id="adv_file" style="width: 1260px; height: 500px;" src="<?php echo url.htmlspecialchars($adv['image']); ?>" />
					<?php
						if($adv_texts[lang_prefix] != ''){
							foreach($adv_texts[lang_prefix] as $text){
								list($r, $g, $b) = sscanf($text['bgcolor'], "#%02x%02x%02x");
								echo '<div class="adv-text adv-place-'.$text['place'].'" style="background-color: rgba('.$r.', '.$g.', '.$b.', ';
									if((int)$text['opacity'] == 100) echo '1';
									else echo '0.'.$text['opacity'];
								echo '); color: '.$text['color'].'">';
								echo  $text['content'];
								echo '</div>';
							}
						}
					?>
				</div><br>
				<input type="hidden" name="adv_file" id="adv_file_input" value="<?php echo $adv['image']; ?>" >
		        <div class="clear: both"></div>
			</div>

			<style>
				#addNewText {
					display: none;
				}
				.baner-texts {
					display: none;
					padding-bottom: 20px;
					padding-top: 10px;
				}
				.texts-links {
					margin-top:3px;
					min-height: 20px;
					overflow: hidden;
					border-bottom: 1px solid #ccc;
					width: 1260px;
					cursor: pointer;
				}
			</style>
			<script type="text/javascript">
				function ShowAddNewtext(){
					if ($("#addNewText").is(":hidden")) {
			            $("#addNewText").fadeIn(300);
			        } else {
			            $("#addNewText").hide(300);
			        }
				}
				function addNewText(){
                    tinyMCE.triggerSave();
                    var place = $('input[name=new-text-place]:checked', '#addNewText').val(),
                    id = '<?php echo $adv['id']; ?>';

					if($('input[name=new-text-place]:checked', '#addNewText').val()) {
						$.ajax({
				            type: "POST",
				            url: "ajax/ajax_add_new_text.php",
				            data: $("#addNewText").find("select, textarea, input").serialize() + "&id="+id+"&place="+place,
				            cache: false,
				            success: function(res){
				            	$('#banner-texts').append(res);
                                jQuery.each(languages.all, function(index, value) {
                                    tinyMCE.get('new-text-content_' + value.prefix).setContent('');
                                });
				            	$('#new-text-color').val('#FFFFFF');
				            	$('#new-text-bgcolor').val('#333333');
				            	var tmp = $('input[name=new-text-place]:checked', '#addNewText');
				            	tmp.attr("checked", false);
				            	tmp.attr("disabled", true);
				            	ShowAddNewtext();
				            	$(".texts-class").each(function(){
                                    jQuery.each(languages.all, function(index, value) {
                                        tinyMCE.execCommand("mceAddControl",false, "text-content-"+place + "_" + value.prefix);
                                    });
								});
								$.ajax({
						            type: "POST",
						            url: "ajax/preview_banner.php",
						            data: "id="+id,
						            cache: false,
						            success: function(res){
						            	$('#baner-show').html(res);
						            }
						        });
				            }
				        });
					} else {
						alert('Моля изберете позиция!');
					}
				}
				function EditText(place){
                    tinyMCE.triggerSave();
					var	content = $("#text-content-" + place + "_" + languages.current).val(),
                        place = place,
						id = '<?php echo $adv['id']; ?>';

					$.ajax({
			            type: "POST",
			            url: "ajax/ajax_add_new_text.php",
			            data: $("#text-" + place).find("select, textarea, input").serialize() + "&id="+id+"&place="+place,
			            cache: false,
			            success: function(res){
			            	$('#text-link-'+place).html(strip_tags(content, '<div><p><strong><em><span>'));
			            	toggleTexts(place);
			            	$.ajax({
					            type: "POST",
					            url: "ajax/preview_banner.php",
					            data: "id="+id,
					            cache: false,
					            success: function(res){
					            	$('#baner-show').html(res);
					            }
					        });
			            }
			        });
				}
                function strip_tags (input, allowed) {
                    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
                    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
                    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
                        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
                    });
                }
				function DeleteText(place){
					if(!confirm('Сигурни ли сте че искате да го изтриете?')) return false;

					var place = place,
						id = '<?php echo $adv['id']; ?>';

					$.ajax({
			            type: "POST",
			            url: "ajax/ajax_delete_text.php",
			            data: "id="+id+"&place="+place,
			            cache: false,
			            success: function(res){
			            	$('#text-link-'+place).remove();
			            	$('#text-'+place).remove();
			            	$('#new-text-place-'+place).attr('disabled', false);
			            	$.ajax({
					            type: "POST",
					            url: "ajax/preview_banner.php",
					            data: "id="+id,
					            cache: false,
					            success: function(res){
					            	$('#baner-show').html(res);
					            }
					        });
			            }
			        });
				}
				function toggleTexts(x) {
					if($('.baner-texts').is(':visible')) {
						$('.baner-texts').slideUp("fast");
					}
				    if($('#text-'+x).is(":hidden")) {
				        $('#text-'+x).slideDown("fast");
				    } else {
				        $('#text-'+x).hide();
				    }
				};
				function showPrecentageChange(id) {
					$('.text-show-opacity-'+id).html($('#text-opacity-'+id).val());
				}
				function showNewPrecentageChange() {
					$('.text-show-opacity-new').html($('#new-text-opacity').val());
				}
			</script>
			<div style="height: 10px;"></div>
			<?php if(count($adv_texts[lang_prefix]) < 9) { ?>
			<a class="addbutton" href="#" onclick="ShowAddNewtext(); return false;">+ текст</a><br />
			<?php } ?>

			<div id="addNewText">
				<table>
					<tr>
						<td style="padding-right: 30px; font-size: 13px">
							Позиция
						</td>
						<td  style="padding-right: 30px; font-size: 13px">
							Текст
						</td>
						<td  style="padding-right: 30px; font-size: 13px">
							Контейнер
						</td>
						<td  style="padding-right: 30px; font-size: 13px">
							Плътност <span class="text-show-opacity-new">80</span> %
						</td>
					</tr>
					<tr>
						<td>
							<table>
								<?php if($adv_texts[lang_prefix] != ''){
										for($i = 1; $i <= 9; $i++){
											if($i == 1){
												echo "<tr>";
											}
											echo '<td><input type="radio" name="new-text-place" id="new-text-place-'.$i.'" value="'.$i.'"';
												if(isset($adv_texts[lang_prefix]['text-'.$i]) && (int)$adv_texts[lang_prefix]['text-'.$i]['place'] == $i){
													echo " disabled";
												}
											echo '></td>';


											if($i%3 == 0){
												echo "</tr><tr>";
											}
											if($i == 9){
												echo "</tr>";
											}
										}
									} else { ?>
								<tr>
									<td><input type="radio" name="new-text-place" id="new-text-place-1" value="1"></td>
									<td><input type="radio" name="new-text-place" id="new-text-place-2" value="2"></td>
									<td><input type="radio" name="new-text-place" id="new-text-place-3" value="3"></td>
								</tr>
								<tr>
									<td><input type="radio" name="new-text-place" id="new-text-place-4" value="4"></td>
									<td><input type="radio" name="new-text-place" id="new-text-place-5" value="5"></td>
									<td><input type="radio" name="new-text-place" id="new-text-place-6" value="6"></td>
								</tr>
								<tr>
									<td><input type="radio" name="new-text-place" id="new-text-place-7" value="7"></td>
									<td><input type="radio" name="new-text-place" id="new-text-place-8" value="8"></td>
									<td><input type="radio" name="new-text-place" id="new-text-place-9" value="9"></td>
								</tr>
								<?php } ?>
							</table>
						</td>
						<td>
							<input type="color" name="new-text-color" id="new-text-color" value="#FFFFFF" style="cursor:pointer">
						</td>
						<td>
							<input type="color" name="new-text-bgcolor" id="new-text-bgcolor" value="#333333" style="cursor:pointer">
						</td>
						<td style="font-size: 13px; cursor: pointer;">
							0 <input type="range" name="new-text-opacity" id="new-text-opacity" value="80" min="00" max="100" step="10" onchange="showNewPrecentageChange();"> 100
						</td>
					</tr>
				</table>
                <?php foreach ($__languages as $key => $v)  { ?>
                    <div><?php echo _($v->getName()); ?></div>
                    <textarea style="width:1260px;height:250px;" id="new-text-content_<?php echo $v->getPrefix(); ?>" class="texts-class" name="new-text-content_<?php echo $v->getPrefix(); ?>"></textarea><br />
                <?php } ?>
				<input type="submit" value="add" id="new-text-add" style="margin-top: 10px;" onclick="addNewText(); return false;">
				<input type="submit" value="cancel" style="margin-top: 10px;" onclick="ShowAddNewtext(); return false;">
			</div>
			<div style="height:20px;"></div>

			<div id="banner-texts">
			<?php
				if($adv_texts[lang_prefix] != ''){
					foreach($adv_texts[lang_prefix] as $k => $t){
						?>
						<div id="text-link-<?php echo $t['place']; ?>" class="texts-links" onclick="toggleTexts('<?php echo $t['place']; ?>')"><?php echo strip_tags($t['content'], '<div><p><strong><em><span>'); ?></div>
						<div id="text-<?php echo $t['place']; ?>" class="baner-texts">
							<table>
								<tr>
									<td style="padding-right: 30px; font-size: 13px">
										Позиция
									</td>
									<td  style="padding-right: 30px; font-size: 13px">
										Текст
									</td>
									<td  style="padding-right: 30px; font-size: 13px">
										Контейнер
									</td>
									<td  style="padding-right: 30px; font-size: 13px">
										Плътност <span class="text-show-opacity-<?php echo $t['place']; ?>"><?php echo $t['opacity']; ?></span> %
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<?php
											for($i = 1; $i <= 9; $i++){
												if($i == 1){
													echo "<tr>";
												}
												echo '<td><input type="radio" value="1"';
													if($i == (int)$t['place']) echo " checked";
													else echo " disabled";
												echo '></td>';
												if($i%3 == 0){
													echo "</tr><tr>";
												}
												if($i == 9){
													echo "</tr>";
												}
											}
											?>
										</table>
									</td>
									<td>
										<input type="color" name="text-color" id="text-color-<?php echo $t['place']; ?>" value="<?php echo $t['color']; ?>" style="cursor:pointer">
									</td>
									<td>
										<input type="color" name="text-bgcolor" id="text-bgcolor-<?php echo $t['place']; ?>" value="<?php echo $t['bgcolor']; ?>" style="cursor:pointer">
									</td>
									<td style="font-size: 13px; cursor: pointer">
										0 <input type="range" name="text-opacity" id="text-opacity-<?php echo $t['place']; ?>" value="<?php echo $t['opacity']; ?>" min="00" max="100" step="10" onchange="showPrecentageChange('<?php echo $t['place']; ?>');"> 100
									</td>
								</tr>
							</table>
                            <?php foreach ($__languages as $key => $v)  { ?>
                                <div><?php echo _($v->getName()); ?></div>
                                <textarea style="width:1000px;height:50px;" class="texts-class" id="text-content-<?php echo $t['place']; ?>_<?php echo $v->getPrefix(); ?>" name="text-content_<?php echo $v->getPrefix(); ?>"><?php echo $adv_texts[$v->getPrefix()][$k]['content']; ?></textarea><br />
                            <?php } ?>
							<input type="submit" value="apply" id="text-update" style="margin-top: 10px;" onclick="EditText('<?php echo $t['place']; ?>'); return false;">
							<input type="submit" value="delete" style="margin-top: 10px;" onclick="DeleteText('<?php echo $t['place']; ?>'); return false;">
						</div>
						<?php
					}
				}
			?>
			</div>
			<div style="height:30px;"></div>
			<!-- <a class="thickbox" href="ajax/preview_banner.php?id=<?php //echo $v['id']; ?>">Преглед</a> -->
			<div style="height:30px;"></div>

			<input type="submit" name="adv_update" value="<?php echo lang_save; ?>">
			<input type="button" value="<?php echo lang_cancel; ?>" onclick="window.location='<?php echo url.'admin/moduls/intro.php'; ?>'">
		</form>
	</div>
<?php } ?>
<div style="height: 60px;"></div>
