<html>
<head>
    <title><?php echo $_SERVER['SERVER_NAME'];?> admin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/slide-login/prefixfree.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            setTimeout( "jQuery('form').show();",800 );
        });
    </script>
    <?php
    if(isset($_POST['submit'])) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                $('.cover').remove();
                jQuery(document).ready(function () {
                    setTimeout( "jQuery('form').show();",10 );
                });
            });
        </script>
    <?php
    }
    ?>
    <style type="text/css">
        *{
            font-family: verdana, arial;
            font-size: 16px;
            color: #FFFFFF;
        }
        html, body {
            width: 100%;
            height: 100%;
            user-select: none;
            margin: 0;
            background-color: #999;
        }
        .error {
            height: 20px;
            text-align: center;
            margin-right: 10px;
            margin-top: 10px;
            font-size: 13px;
            color: #E31318;
        }
        input {
            color: #333;
            background-color:#fff;
            border:1px solid #fff;
            float:right;
            width:300px;
            height:30px;
            padding-left: 10px;
            margin-top: -4px;
        }
        input[type=password] {
            width: 234px;
        }
        .cover{
            background: url('<?php echo url; ?>//admin/images/admin_background_hd.jpg');
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            right: 0;
            bottom: 0;
            z-index: 9999;
        }
        form{
            background-color: #333;
            display: none;
            width:640px;
            height:360px;
            -webkit-box-shadow: 15px 15px 20px 0px rgba(0,0,0,0.5);
            -moz-box-shadow: 15px 15px 20px 0px rgba(0,0,0,0.5);
            box-shadow: 15px 15px 20px 0px rgba(0,0,0,0.5);
        }
        table{
            width:100%;
            height:100%;
            position: relative;
            z-index: 1;
        }
        #submit{
            background: #F7931E;
            border: medium none;
            color: #FFFFFF;
            cursor: pointer;
            font-size: 13px;
            margin-top: -4px;
            height: 30px;
            padding: 0;
            width: 66px;
            margin-right: 65px;
        }
        .footer {
            background-color: #4d4d4d;
            height: 30px;
            width: 100%;
            margin-top: 145px;
        }
        input:hover,input:focus {
            border: 1px solid #F7931E;
        }
        footer {
            display: none;
        }
        .footer a {
            text-decoration: none;
            line-height: 27px;
            color: #ccc;
        }
        .footer a:hover {
            color: #F7931E;
        }
        .forgot {
            margin-top: 5px;
        }
        .forgot a {
            font-size: 13px;
            color: #666;
            text-decoration: none;
            margin-left: 137px;
        }
        .forgot a:hover {
            color: #F7931E;
        }
    </style>
</head>
<body >
<div class="cover"></div>
<table>
    <tr>
        <td align="center" valign="center">
            <form id="form" method="post" action="" >
                <div style="   left: -34px;
    position: relative;
    text-align: left;
    top: 60px;
    width: 490px;">
                    <div style="color: #F7931E;margin-left:125px; margin-bottom: 30px;padding-left: 10px;">Sign In to iStore</div>
                    <div style="width:425px; height: 30px;margin-bottom:25px;">
                        <input type="text" name="login_mail" id="login_mail" placeholder="iStore ID" value="" tabindex="1"/>
                    </div>
                    <input id="submit" type="submit" name="submit" value="Sign In" tabindex="3" />
                    <div style="width:360px;height: 30px;">
                        <input type="password" name="login_pass" id="login_pass" placeholder="Password" value="" tabindex="2" />
                    </div>
                    <div class="forgot">
                        <a href="#">forgot ID or Password</a>
                    </div>
                    <div class="error">
                        <?php
                        if($login_errors) {
                            echo 'Incorrect iStore ID or password';
                        }
                        ?>

                    </div>

                </div>
                <div class="footer">
                    <a href="<?php echo url;?>" target="_blank"><?php echo $_SERVER['SERVER_NAME'];?></a>
                </div>
            </form>

        </td>
    </tr>
</table>
<script type="text/javascript" src="<?php echo url; ?>js/slide-login/index.js"></script>
</body>
</html>
