<?php
require('template/__top.php');

unset($_SESSION['crop_thumb_image_location']);
unset($_SESSION['crop_large_image_location']);
unset($_SESSION['crop_html_img']);
unset($_SESSION['crop_html_img_t']);
unset($_SESSION['crop_html_img_b']);
unset($_SESSION['crop_html_img_input_hidden']);

$_SESSION['crop_upload_dir'] = '../../../images/products';

/* @var $artikul artikul_admin */
?>
<script>
    $( document ).ready(function() {
        $('#additional_delivery').keypress(function(event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && event.which > 31 && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('#additional_delivery').keyup(function() {
            var delivery = parseFloat($('#delivery').val() == '' ? 0 : $('#delivery').val());
            var additional_delivery = parseFloat(($(this).val() == '' || $(this).val() == '.') ? 0 : $(this).val());
            var total = delivery + additional_delivery;

            $('#total_delivery').val(total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'))
        });
    });

    function syncProduct(id){
        var dataString = 'id='+id;
        $.ajax({
            type: "POST",
            url: "<?php echo url; ?>admin/ajax/syncProduct.php",
            data: dataString,
            cache: false,
            success: function() {
                $('input[type=submit]').trigger('click');
                //location.reload();
                //$('#syncProduct').append('<span style="color: #00FF00; font-size: 13px; display: block; margin: 10px;">Успешно синхронизирахте този продукт!</span>')
            }
        });
    }
</script>
<style type="text/css">
    #TB_window {
        background:#333;
    }
    .selectoptions a{
        color:#fff;
        margin-right:5px;
        text-decoration:none;
        font-size:13px;
    }
    .selectoptions a.active{
        color:#fb921d;
    }
    .button-cancel:disabled {
        background: #dddddd;
        cursor: default;
    }
</style>
<div id="syncProduct"></div>
<!--<div style="margin:10px 0 0 10px;font-size:13px;color:#fb921d;">редактиране продукт марка <?php //echo $artikul->getIme_marka(); ?> в <?php //echo $artikul->getIme_kategoriq();?> / <?php //echo $artikul->getIme_vid();?></div>-->
<div style="width: 1280px;">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="proddetails-left-col">
            <div style="min-height: 515px;">
                <table style="font-size: 13px;width: 100%;">
                    <tr>
                        <td style="vertical-align: top;">
                            <div>
                                <div class="extra_images">
                                    <?php
                                    if (isset($_POST['product_images_extra'])) {
                                        foreach ($_POST['product_images_extra'] as $key => $file) {
                                            echo '<div style="margin: 13px;width:95px;height: auto;display: block; " class="additional-image-'.($key+1).'">';
                                            echo '<div style="height:21px;">';
                                            echo '<a class="delete-button" href="#" onclick="deleteExtraImage('.($key+1).');return false;" style="color:red;float:right;font-size:17px;padding-right:7px;" id="product_images_extra_a_dell_'.($key+1).'">x</a>
                                              <a class="thickbox" style="color: #80B543;font-size: 13px;padding-top: 4px;padding-right: 10px;float:right;" href="moduls/crop/new_crop_products.php?crop_html_img_t=1&crop_width=600&crop_height=450&crop_html_img=product_images_extra_'.($key+1).'&crop_html_img_input_hidden=product_images_extra_'.($key+1).' input&crop_html_a=product_images_extra_a'.($key+1).'&crop_large_image_location=../../../'.str_replace("thumbnail","resize",$file).'&crop_upload_dir=../../../upload_images/&keepThis=true&TB_iframe=true&height=640&width=1240">E</a>';
                                            echo '</div>';
                                            echo '<a rel="fancybox-thumb" href="'.url.preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $file ).'"><img style="max-width: 95px;" src="'.url.preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $file ).'" id="product_images_extra_'.($key+1).'"/></a>';
                                            echo '<div id="product_images_extra_'.($key+1).'">';
                                            echo '<input type="hidden" name="product_images_extra[]" value="'.$file.'" />';
                                            echo '</div>';
                                            echo '<div style="clear:both;height:5px;" > &nbsp;</div>';
                                            echo '</div>';
                                        }
                                    }

                                    $counter = 1;
                                    if (is_array($product_images_extra)) {
                                        foreach ($product_images_extra as $key=>$image) {
                                            if (isset($image) && !empty($image)) {
                                                $tmp_image_url = url.$image->getKartinka_t();
                                                $tmp_image_crop = $image->getKatinka();
                                                $tmp_image_input = $image->getKatinka();
                                                $tmp_image_url_b = url.$image->getKartinka_b();
                                                $tmp_rel = 'rel="fancybox-thumb"';

                                                echo '<div style="margin: 13px;width:95px;height: auto;">';
                                                echo '<div style="height:21px;">';
                                                if(!empty($tmp_image_input)){
                                                    echo ' <a id="product_images_extra_a_dell_'.$key.'" href="#" onclick="deleteExtraImage('.$key.',\''.url.'images/white1x1.png\');return false;" style="color:red;float:right;font-size:17px;padding-right:7px;">x</a>
                                                <a class="thickbox" style="color: #80B543;font-size: 13px;padding-top: 4px;padding-right: 10px;float:right;" href="moduls/crop/new_crop_products.php?crop_html_img_t=1&crop_width=600&crop_height=450&crop_html_img=product_images_extra_'.$key.'&crop_html_img_input_hidden=product_images_extra_'.$key.' input&crop_html_a=product_images_extra_a'.$key.'&crop_large_image_location=../../../'.str_replace("thumbnail","resize",$tmp_image_crop).'&crop_upload_dir=../../../upload_images/&keepThis=true&TB_iframe=true&height=640&width=1240">E</a>';
                                                }else{
                                                    echo '<a id="product_images_extra_a_dell_'.$key.'" href="#" onclick="return false;" style="display:none;color:red;float:right;font-size:17px;padding-right:7px;">x</a>
                                                <a class="thickbox" style="color: #80B543;font-size: 13px;padding-top: 4px;padding-right: 10px;float:right;" href="moduls/crop/new_crop_products.php?crop_html_img_t=1&crop_width=600&crop_height=450&crop_html_img=product_images_extra_'.$key.'&crop_html_img_input_hidden=product_images_extra_'.$key.' input&crop_html_a=product_images_extra_a'.$key.'&crop_large_image_location=../../../'.str_replace("thumbnail","resize",$tmp_image_crop).'&crop_upload_dir=../../../upload_images/&keepThis=true&TB_iframe=true&height=640&width=1240">E</a>';

                                                }
                                                echo '</div>';
                                                echo '<div>';
                                                echo '<a rel="fancybox-thumb" id="product_images_extra_a'.$key.'" href="'.$tmp_image_url_b.'"><img id="product_images_extra_'.$key.'" src="'.$tmp_image_url.'" style=" max-width: 95px;" /></a>';
                                                echo '</div>';
                                                echo '<div id="product_images_extra_'.$key.'"><input type="hidden" name="product_images_extra[]" value="'.$tmp_image_input.'" /></div>';
                                                echo '<div style="clear:both;height:5px;" > &nbsp;</div>';
                                                echo '</div>';

                                                $counter++;
                                            }
                                        }
                                    }
                                    echo '</div>';

                                    $tmp_image_url = url.'/admin/images/blank_135.jpg';
                                    $tmp_image_crop = '/images/deff.png';
                                    $tmp_image_input = '';
                                    $tmp_image_url_b = '';
                                    $tmp_rel = '';

                                    echo '<div style="display: none;margin: 13px;width:95px;height: auto;" class="image-reference">';
                                    echo '<div style="height:21px;">';
                                    echo '<a class="delete-button" href="#" style="color:red;float:right;font-size:17px;padding-right:7px;">x</a>
                              <a class="thickbox" style="color: #80B543;font-size: 13px;padding-top: 4px;padding-right: 10px;float:right;" href="moduls/crop/new_crop_products.php?crop_html_img_t=1&crop_width=600&crop_height=450&crop_html_img=|image|&crop_html_img_input_hidden=|hidden|&crop_html_a=|image_href|&crop_large_image_location=|image_location|&crop_upload_dir=../../../upload_images/&keepThis=true&TB_iframe=true&height=640&width=1240">E</a>';
                                    echo '</div>';
                                    echo '<a rel="fancybox-thumb"><img style=" max-width: 95px;" /></a>';
                                    echo '<div>';
                                    echo '<input type="hidden" disabled name="product_images_extra[]" value="'.$tmp_image_input.'" />';
                                    echo '</div>';
                                    echo '<div style="clear:both;height:5px;" > &nbsp;</div>';
                                    echo '</div>';

                                    echo '<div class="add-extra-image" style="margin: 13px;width:95px;border-radius: 10px;border: 2px dashed #333;height: 95px; position: relative;z-index: 9999;">';
                                    echo '<a class="thickbox redaktirai-image-trigger-new" style="text-align: center;font-size: 13px;color: #333;cursor:pointer;line-height: 95px;position: absolute;display: block;padding-left: 0;left:0;right:0;z-index: -1;" href="moduls/crop/new_crop_products.php?crop_html_img_t=1&crop_width=600&crop_height=450&crop_html_img=product_images_extra_' . (count($product_images_extra) + 1) . '&crop_html_img_input_hidden=new&crop_html_a=product_images_extra_a_' . (count($product_images_extra) + 1) . '&crop_large_image_location=../../../'.str_replace("thumbnail","resize",$tmp_image_crop).'&crop_upload_dir=../../../upload_images/&keepThis=true&TB_iframe=true&height=640&width=1240">+снимка</a>';
                                    echo '</div>';
                                    echo '<div style="clear:both;height:5px;" > &nbsp;</div>';
                                    ?>
                                </div>
                        </td>
                        <td colspan="6" style="text-align: center;">
                            <a id="product_main_image" rel="fancybox-thumb" href="../<?php echo $artikul->getKartinka_b(); ?>"><img style="border:none;margin-top: 13px; max-height: 450px;" src="<?php echo url.$artikul->getKatinka(); ?>" ALT="image"></a>
                            <script type="text/javascript" src="<?php echo url; ?>js/jquery.lightbox-0.5.min.js"></script>
                            <link rel="stylesheet" href="<?php echo url; ?>css/jquery.lightbox-0.5.css" type="text/css" />
                            <script type="text/javascript">

                                $(document).ready(function() {
                                    $("a[rel=fancybox-thumb][href!='']").fancybox({
                                        prevEffect : 'fade',
                                        nextEffect : 'fade',
                                        nextSpeed: 700,
                                        prevSpeed: 700,
                                        helpers : {
                                            title : {
                                                type: 'outside'
                                            },
                                            overlay : {
                                                opacity : 0.8,
                                                css : {
                                                    'background-color' : '#000'
                                                }
                                            },
                                            thumbs : {
                                                width : 80,
                                                height : 45
                                            }
                                        }
                                    });
                                });
                            </script>
                            <div id="product_main_image"><input name="product_main_image" type="hidden" value="<?php echo $artikul->getKatinka(); ?>" /></div>
                            <a class="thickbox" style="color:#fff;background-color:#666666;width:100px;height:25px;display:block;text-align:center;margin-left: auto;margin-right: auto;font-size:13px;line-height:22px;margin-top:10px;" href="moduls/crop/new_crop_products.php?crop_html_a=product_main_image&crop_html_img_input_hidden=product_main_image input&crop_width=600&crop_height=450&crop_html_img=product_main_image img&crop_large_image_location=../../../<?php echo str_replace("thumbnail","resize",$artikul->getKatinka());?>&crop_upload_dir=../../../upload_images/&keepThis=true&TB_iframe=true&height=640&width=1000">редактирай</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div colspan="6" style="font-size: 13px;width: 100%;">Youtube/Vimeo видео <br> <input name="product_video" type="text" style="width: 100%;margin-top: 3px;" value="<?php echo $artikul->getVideo(); ?>"></div>
            <div style="clear: both"></div>
            <div class="input-tab-holder" style="margin-top: 10px;width:100%;float: left;height: auto;overflow: hidden;">
                <div class="input-tab-title">информация</div>
                <?php
                $inputs1 = "";
                $dop_info_all = array_reverse($artikul->getDop_info_all());
                foreach (array_reverse($__languages) as $key => $v)  {
                    echo '<div class="input-tab-trigger-' . $v->getName() . '-product-info-edit">' . $v->getName() . '</div>';
                    $inputs1 = $inputs1 . '<textarea rows="25" class="input-tab-' . $v->getName() . '-product-info-edit" name="product_info_' . $v->getPrefix() .'" style="height: 200px;color: #333;border: 1px solid #666666;font-size: 13px;width: 100%;">' . $dop_info_all[$key]['name'] . '</textarea>';
                }
                ?>
                <?php echo $inputs1; ?>
            </div>
            <div style="clear: both;"></div>

            <div style="margin:10px 0px; font-size: 10pt; width: 830px;">
                <?php
                if(($artikul->getId_kategoriq()==27)||($artikul->getId_kategoriq()==29)){
                    echo '<div style="background-color: #f2f2f2; width: 100%; height: auto; padding-bottom: 3px;">';
                    if (isset($product_vidove)) foreach ($product_vidove as $v) {
                        echo '<div style="margin-right:30px;display:inline-block;font-size:10pt"><input type="checkbox" name="product_used[]" ';
                        if(in_array($v['id'],$artikul->getIzpolzvaniProduktiIUslugi())){
                            echo 'checked="yes" ';
                        }

                        echo ' value="'.$v['id'].'"> ';
                        echo $v['ime_'.lang_prefix].'</div>';
                    }
                    echo '<br /><br /><hr /><br />';
                    if (isset($deinosti_vidove)) foreach ($deinosti_vidove as $v) {
                        echo '<div style="margin-right:30px;display:inline-block;font-size:10pt"><input type="checkbox" name="product_used[]" ';
                        if(in_array($v['id'],$artikul->getIzpolzvaniProduktiIUslugi())){
                            echo 'checked="yes" ';
                        }


                        echo ' value="'.$v['id'].'"> ';
                        echo $v['ime_'.lang_prefix].'</div>';
                    }
                    echo '</div>';
                }else{
                if($etiket_grupi) foreach ($etiket_grupi as $v) {
                /* @var $v etiket_grupa */
                ?>
                <div style="background-color: #f2f2f2; width: 100%; height: auto;"><div class="menu_zaglavie2" style="clear:both;margin:15px 0px 5px;font-size:13px;height: auto;"><?php  echo "<div style='"; if ($v->getSelect_menu() == 1) echo "min-width: 320px;"; else echo "width: 100%;"; echo "vertical-align: top;float:left;'>".$v->getIme()."</div>"; if ($v->getSelect_menu() == 1) { echo "<div style='display:inline-block;width: 70px;margin-left:2px;vertical-align: top;float:left;'>код</div><span style='display:inline-block;width:70px;margin-left: 32px;vertical-align: top;'>дилър</span><span style='display:inline-block;margin-left: 29px;width: 70px;vertical-align: top;'>цена</span><span style='display:inline-block;margin-left: 30px;width: 70px;vertical-align: top;'>промо</span><span style='display:inline-block;margin-left: 29px;width: 70px;vertical-align: top;'>отстъпка</span>"; }?></div>
                    <div style="width: 100%; height: auto; overflow: hidden; background-color: #cccccc; padding-top: 7px; padding-bottom:10px;">
                        <?php
                        if ($v->getSelect_menu()) {
                            echo "<ul style='display:inline-block;vertical-align:top; margin-right: 5px; padding: 10px; list-style: none;background: #CCC; color: #333;width:100%;'>";
                        }
                        $n = 1;
                        foreach ($v->getEtiketi() as $e) {
                        if ($v->getSelect_menu() == 1) { ?>
                        <li style="margin-bottom: 3px;"><input type="checkbox" name="product_tags[]" <?php if($artikul->getEtiketizapisi()) if(in_array($e->getId(), $artikul->getEtiketi_array_ids() ) ) { ?> checked <?php } ?> value="<?php echo $e->getId(); ?>">
                            <div style="width: 300px;display:inline-block;"><?php echo $e->getIme(); ?></div>
                            <?php
                            echo "<input type='text' style='width: 70px;' name='warehouse_etiket_".$e->getId()."' value='".$artikul->getWarehouseTagCode($e->getId())."'>";
                            echo "<input type='text' style='margin-left: 30px; width: 70px;' name='warehouse_price_big_".$e->getId()."' value='".$artikul->getWarehouseBigPrice($e->getId())."'>";
                            echo "<input type='text' style='margin-left: 30px; width: 70px;' name='warehouse_price_".$e->getId()."' value='".$artikul->getWarehousePrice($e->getId())."'>";
                            echo "<input type='text' style='margin-left: 30px; width: 70px;' name='warehouse_promo_price_".$e->getId()."' value='".$artikul->getWarehousePromoPrice($e->getId())."'>";
                            echo "<span style='margin-left: 30px; width: 70px;'>0 лв. 0 %</span>";
                            echo "</li><li style='height: 1px;margin: 6px 0;background: #f2f2f2;width: calc(100% - 20px);'></li>";
//                            if($n%20==0) {
//                                echo "</ul><ul style='display:inline-block;vertical-align:top; margin-right: 5px; padding: 10px; list-style: none; color: #333;background: #CCC;width:100%;'>";
//                                $n=0;
//                            }
//                            $n++;
                            } else {
                                ?>

                                <div style="float: left; width: 33%; padding-right: 10px; padding-left: 10px; box-sizing: border-box; color: #333333;">
                                    <input type="checkbox" name="product_tags[]" <?php if($artikul->getEtiketizapisi()) if(in_array($e->getId(), $artikul->getEtiketi_array_ids() ) ) { ?> checked <?php } ?> style="margin-top: 5px;" value="<?php echo $e->getId(); ?>">
                                    <?php echo $e->getIme(); ?>
                                </div>
                                <?php
                            }
                            }
                            echo "</div>";
                            if ($v->getSelect_menu()) {
                                echo "</ul>";
                                echo "<div style='color:#f7931e;font-size:13px;background: white;padding-left: 10px;padding-top: 5px;font-size:10pt'>* Aко нямa въведен поне един таг за ".$v->getIme().", продукта не може да се добави в количката</div>";
                            }
                            echo "</div>";
                            }
                            }
                            ?>
                    </div>
                    <div style="clear: both; height: 60px;"></div>
                </div>

                <div class="proddetails-right-col" style="margin-top: 13px;">
                    <div class="proddetails-right-col-top-lenta">
                        <?php
                        if($__user->permission_check('продукти','rw',true,true)) {
                            ?>
                            <div style="color: #FF0000 ;text-align:right;margin-right: 7px;float: right;cursor: pointer;margin-top:-3px; font-size: 17px;" onclick="product_delete_inside(<?php echo $artikul->getId(); ?>,'<?php echo $artikul->getId_kategoriq(); ?>', '<?php echo $artikul->getId_vid(); ?>');" >x</div>
                            <?php
                        }
                        ?>
                        <?php
                        if($__user->permission_check('продукти','rw',true,true)) {
                            if ($artikul->isOnline()){
                                ?>
                                <div style="color: #fff; background-color: #F7931E; height:20px; line-height: 20px; width: 60px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">online</div>
                                <div style="color: #fff; background-color: #666; line-height: 20px; height:20px; width: 60px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;" onclick="product_offline(<?php echo $artikul->getId(); ?>);">offline</div>
                                <?php
                            }else{
                                ?>
                                <div style="/* color: #00FF00;*/ color: #fff; line-height: 20px; height:20px; background-color: #666; width: 60px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;" onclick="product_online(<?php echo $artikul->getId(); ?>);">online</div>
                                <div style="color: #fff; background-color: #F7931E; width: 60px; line-height: 20px; height:20px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">offline</div>
                                <?php
                            }
                            if(!$artikul->isAvaliable()){
                                ?>
                                <div style="margin-left: 10px;color: #333; width: auto;cursor: pointer; line-height: 20px;float: left; display: inline-block; font-size: 13px;">n/a</div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <input type="hidden" name="ref" value="<?php /*if(isset($_SERVER['HTTP_REFERER'])) echo $_SERVER['HTTP_REFERER']; */ ?>browse.php?category=<?php echo $artikul->getId_kategoriq(); ?>&mode=<?php echo $artikul->getId_vid(); ?>&model=<?php echo $artikul->getId_model(); ?>" />
                    <input type="hidden" name="product_kod" value="<?php echo $artikul->getKod(); ?>" />
                    <table style="margin: 10px 0px;font-size: 10pt;table-layout: fixed;width: 380px;">
                        <tr><td style="font-size: 16px;"><?php echo $artikul->getIme_marka(); ?></td></tr>
                        <tr><td style="height: 10px;"></td></tr>
                        <tr>
                            <td colspan="7">файл <?php
                                $manual=$artikul->getManual();
                                echo "<input type='hidden' name='manual_link' value='{$manual}' />";
                                if ((isset($manual))&&(!empty($manual))){
                                    echo '<a id="file_'.$artikul->getId().'" href="'.url.$manual.'" TARGET="_blank">'.basename($manual).'</a> <a href="#" onclick="deleteManual('.$artikul->getId().');return false;" style="color:#ff0000;">x</a>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <input type="file" name="Manual" style="width:300px;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7"><div style="clear:both; height: 10px"></div></td>
                        </tr>
                        <tr>
                            <?php
                            $solo = true;
                            if($etiket_grupi) foreach ($etiket_grupi as $v) {
                                if ($v->getSelect_menu()) {
                                    $solo = false;
                                }
                            }
                            ?>
                            <td colspan="5">
                                модел *
                                <!--			<span><a href="#" onclick="syncProduct('<?php //echo $artikul->getId(); ?>'); return false;" style="color: #00FF00; font-size: 13px;">Sync</a></span>-->
                            </td>
                            <?php
                            if($solo) {
                                ?>
                                <td colspan="2" style="padding-left: 18px;">код</td>
                            <?php } else { ?>
                            <?php } ?>
                        </tr>
                        <?php
                        if(!$artikul->isAvaliable()){
                            ?>
                            <script>
                                $(document).ready(function(){
                                    $('#productNameText').css({
                                        'color': '#ff0000'
                                    });
                                });
                            </script>
                            <?php
                        }
                        ?>
                        <tr style="margin-top: -10px;">
                            <td colspan="5">
                                <input style="width:280px;" name="product_name" type="text" class="ime_na_product2_<?php echo $artikul->getId(); ?>" value="<?php echo $artikul->getIme(); ?>" id="productNameText">
                                <?php //echo ' ID'.$artikul->getId(); ?>
                            </td>
                            <?php
                            if($solo) {
                                ?>
                                <td colspan="2"><input type="text" style="width:90px; margin-left: 18px;" name="warehouse_code" value="<?php echo $artikul->getWarehouseCode(); ?>"></td>
                            <?php } else { ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="7"><div style="clear:both; height: 10px"></div></td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <div class="input-tab-holder" style="width:100%;float: left;height: auto;overflow: hidden;">
                                    <div class="input-tab-title">заглавие</div>
                                    <?php
                                    $inputs = "";
                                    $title_all=array_reverse($artikul->getTitleAll());
                                    if($__languages) foreach (array_reverse($__languages) as $key => $v){
                                        echo '<div class="input-tab-trigger-' . $v->getName() . '-product-edit">' . $v->getName() . '</div>';
                                        $inputs = $inputs . '<input style="width:100%;" class="input-tab-' . $v->getName() . '-product-edit" value="' . $title_all[$key]['name'] . '" type="text" name="product_title_' . $v->getPrefix() . '">';
                                    }
                                    ?>
                                    <?php echo $inputs; ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7"><div style="clear:both; height: 10px"></div></td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <div class="input-tab-holder" style="width:100%;float: left;height: auto;overflow: hidden;">
                                    <div class="input-tab-title">описание</div>
                                    <?php
                                    $inputs1 = "";
                                    $specification_all=array_reverse($artikul->getSpecification_all());
                                    foreach (array_reverse($__languages) as $key => $v)  {
                                        echo '<div class="input-tab-trigger-' . $v->getName() . '-product-info2-edit">' . $v->getName() . '</div>';
                                        $inputs1 = $inputs1 . '<textarea rows="25" class="input-tab-' . $v->getName() . '-product-info2-edit" name="product_info2_' . $v->getPrefix() .'" style="height: 282px;color: #333;border: 1px solid #666666;font-size: 13px;padding:7px;width: 100%;">' . $specification_all[$key]["name"] .'</textarea>';
                                    }
                                    ?>
                                    <?php echo $inputs1; ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7"><div style="clear:both; height: 10px"></div></td>
                        </tr>
                        <tr>
                            <td><div style="width: 100px;">дилър</div></td>
                            <td><div style="margin-left: 40px;">цена</div></td>
                            <td><div style="width: 100px;margin-left:80px;">промо</div></td>
                            <td>&nbsp;</td>
                            <td colspan="3" style="text-align:right;">отстъпка</td>
                        </tr>
                        <tr>
                            <td><input name="product_cena_dostavna" style="width:65px; margin-right: 10px;" type="text" value="<?php echo $artikul->getCena_dostavna(); ?>"></td>
                            <td><input name="product_cena" style="width:65px; margin-right: 10px;margin-left:40px;" type="text" value="<?php echo $artikul->getCena(); ?>"></td>
                            <td><input name="product_cena_promo" style="width:65px; margin-right: 10px;margin-left:80px;" type="text" value="<?php echo $artikul->getCena() > 0 ? $artikul->getCena_promo() : 0; ?>"></td>
                            <td>&nbsp;</td>
                            <td colspan="3" style="color:#fb921d;text-align:right;">
                                <?php
                                $cenapromo=$artikul->getCena_promo();
                                if (isset($cenapromo) && $cenapromo>0 && $artikul->getCena() > 0){
                                    $tmpraz=($artikul->getCena() - $artikul->getCena_promo() );
                                    echo $tmpraz." лв. ".round((($tmpraz/$artikul->getCena())*100),2)." %";
                                }else{
                                    echo "0 лв. 0 %";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: bottom;"><div style="width: 100px;">доставка</div></td>
                            <td><div style="margin-left: 40px;"></div></td>
                            <td style="vertical-align: bottom;"><div style="width: 100px;margin-left:80px;vertical-align: bottom;">общо</div></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <?php
                            $ac = 0;
                            if($artikul->getCena_promo() > 0){
                                $ac = $artikul->getCena_promo();
                            }else {
                                $ac = $artikul->getCena();
                            }

                            if ($dostavka_free < $ac) {
                                $dostavka_cena = 0;
                            }
                            ?>
                            <td><input disabled id="delivery" style="width:65px; margin-right: 10px;" type="text" value="<?php echo number_format($dostavka_cena, 2); ?>"></td>
                            <td style="position: relative;"><span style="position: absolute; top: 3px; right: 20px; font-size: 15px;font-weight: bold;">+</span><input id="additional_delivery" name="additional_delivery" style="width:65px; margin-right: 10px;margin-left:40px;" type="text" value="<?php echo number_format($artikul->getAdditionalDelivery(), 2); ?>"></td>
                            <td><input disabled id="total_delivery" style="width:65px; margin-right: 10px;margin-left:80px;" type="text" value="<?php echo number_format($dostavka_cena + $artikul->getAdditionalDelivery(), 2); ?>"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="7"><div style="clear:both; height: 10px"></div></td>
                        </tr>
                        <tr id="preorder_row" <?php echo !$artikul->isAvaliable() ? "style='display: none;'" : ""; ?>>
                            <td colspan="2"><input onchange="$('#preorder-wrapper').toggle();" <?php echo $artikul->isPreorder() ? "checked='checked'" : ""; ?> id="preorder" name="preorder" style="-ms-transform: scale(2);margin: 0 5px 0 0;vertical-align: middle;width:13px;" value="1" type="checkbox" />
                                <label for="preorder">Pre-order</label>
                            </td>
                            <td colspan="3"><input id="con" name="dayoffer" style="-ms-transform: scale(2);margin: 0 10px 0 0;vertical-align: middle;width:13px;margin-right:10px;" type="checkbox" <?php if($artikul->IsDayOffer()){echo ' checked="yes" ';} ?> > Оферта на деня</td>
                        </tr>
                        <tr>
                            <td colspan="7">

                                <div id="preorder-wrapper" <?php echo !$artikul->isPreorder() ? "style='display: none;'" : ""; ?>>
                                    <div class="input-tab-holder" style="margin-top: 10px;width:100%;float: left;height: auto;overflow: hidden;">
                                        <div class="input-tab-title">Pre-order описание</div>
                                        <?php
                                        $inputs5 = "";
                                        $preorder_info_all=array_reverse($artikul->getPreorder_info_all());
                                        foreach (array_reverse($__languages) as $key => $v){
                                            echo '<div class="input-tab-trigger-' . $v->getName() . '-product-pre-order">' . $v->getName() . '</div>';
                                            $inputs5 = $inputs5 . '<textarea rows="3" name="preorder_info_' . $v->getPrefix() . '" cols="45" id="pre-order" class="pre-order input-tab-' . $v->getName() . '-product-pre-order" style="line-height: 23px;width:100%;font-size: 13px;color: #333;font-family: Verdana, sans-serif;border: 1px solid #666666;">' . $preorder_info_all[$key]["name"] .'</textarea>';
                                        }
                                        ?>
                                        <?php echo $inputs5; ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div style="display:inline-block; width: 380px; font-size: 13px; position: relative;">
                        <span>колекция</span>
                        <div style="clear: both;"></div>
                        <?php if($artikul->getCollectionId()) : ?>
                            <input class="search" disabled="disabled" id="product_collection" autocomplete="off" style="background-image: none; display: block; float: left; width: 100%;margin-top: 3px;" type="text" onClick="this.value='';" onkeyup="add_collection_suggestion();" name="add_collection_search" value="<?php echo $artikul->getCollectionName(); ?>">
                            <span onclick="clearCollection();" id="remove_collection" style="color: #FF0000;font-size: 13px; cursor: pointer; position: absolute; right: 15px; top: 21px;">X</span>

                        <?php else : ?>
                            <input class="search" id="product_collection" autocomplete="off" style="display: block; float: left; width: 100%;margin-top: 3px;" type="text" onClick="this.value='';" onkeyup="add_collection_suggestion();" name="add_collection_search" value="търсене">
                            <span onclick="clearCollection();" id="remove_collection" style="display: none;color: #FF0000;font-size: 13px; cursor: pointer; position: absolute; right: 15px; top: 21px;">X</span>

                        <?php endif; ?>
                        <div style="clear: both;"></div>
                        <div id="add_prod_collection_suggestion" style="display: none; float: left; background-color: #f2f2f2; width: 100%; z-index:2; margin-top: -4px;"></div>
                        <div style="clear: both;"></div>
                        <input type="hidden" name="product_collection" id="product_collection_hidden" value="<?php echo $artikul->getCollectionId(); ?>">
                        <input type="hidden" name="product_collection_name" id="product_collection_name_hidden" value="<?php echo $artikul->getCollectionName(); ?>">
                    </div>
                    <div style="width: 380px; margin-top: 10px; font-size: 13px;">
                        <span>свързани продукти</span>
                        <div style="clear: both;"></div>
                        <input class="search" style="display: block; float: left; width: 100%;margin-top: 3px;" type="text" onClick="this.value='';" onkeyup="related_prod_search_suggestion();" name="related_prod_search" value="търсене" autocomplete="off">
                        <div style="clear: both;"></div>
                        <div id="related_prod_search_suggestion" style="display: block; float: left; background-color: #fff; width: 100%; z-index:2; margin-top: 1px;margin-bottom: 10px;border-bottom: 1px solid #333;"></div>
                        <div style="clear: both;"></div>
                        <input type="hidden" name="related_products_ids" id="related_products_ids" value="<?php echo $artikul->getRelatedProducts(); ?>">
                        <div id="related_products">
                            <?php
                            $strRelatedProducts = $artikul->getRelatedProducts();
                            if ($strRelatedProducts != "") {
                                $ArtikuliArray = explode(",", $strRelatedProducts);
                                foreach($ArtikuliArray as $ar) {
                                    $ar = (int)$ar;
                                    $adda = new artikul($ar);

                                    if (empty($adda->getId())) {
                                        echo '<div id="related_prod_'.$ar.'" style="width: 100%; position: relative; margin-bottom: 20px;padding-left: 10px;box-sizing: border-box;">';
                                        echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_related_product(\'-1\'); return false;">x</a></div>';
                                        echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">';
                                        echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:180px;text-align: left;">';
                                        echo '<span style="color:#fb921d;" class="ime_na_related_prod_'.$adda->getId().'">изтрит продукт</span>';
                                        echo '</span></div> </div></div>';
                                        echo '<div style="clear: both;"></div>';
                                    } else {
                                        echo '<div id="related_prod_'.$adda->getId().'" style="width: 100%; position: relative; margin-bottom: 20px;padding-left: 10px;box-sizing: border-box;">';
                                        echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_related_product(\''.$adda->getId().'\'); return false;">x</a></div>';
                                        echo '<a href="'.url_admin.'product.php?id='.$adda->getId().'">';
                                        echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">  <img src="'.url.$adda->getKartinka_t().'" style="max-width: 60px;max-height:60px; border:0;vertical-align:middle;" />';
                                        echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:180px;text-align: left;">';
                                        echo $adda->getIme_marka().'<br><span class="ime_na_related_prod_' . $adda->getId() . '">';
                                        echo $adda->getIme();
                                        echo '</span></div> </div> </a></div>';
                                        echo '<div style="clear: both;"></div>';
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div style="width: 380px; margin-top: 10px; font-size: 13px;">
                        <span>допълнителни продукти</span>
                        <div style="clear: both;"></div>
                        <input class="search" style="display: block; float: left; width: 100%;margin-top: 3px;" type="text" onClick="this.value='';" onkeyup="add_prod_search_suggestion();" name="add_prod_search" value="търсене" autocomplete="off">
                        <div style="clear: both;"></div>
                        <div id="add_prod_search_suggestion" style="display: block; float: left; background-color: #fff; width: 100%; z-index:2; margin-top: 1px;margin-bottom: 10px;border-bottom: 1px solid #333;"></div>
                        <div style="clear: both;"></div>
                        <input type="hidden" name="add_products_ids" id="add_products_ids" value="<?php echo $artikul->getAddArtikuli(); ?>">
                        <div style="margin-top: 20px;" id="add_products">
                            <?php
                            $strAddArtikuli = $artikul->getAddArtikuli();
                            if ($strAddArtikuli != "") {
                                $ArtikuliArray = explode(",", $strAddArtikuli);
                                foreach($ArtikuliArray as $ar) {
                                    $ar = (int)$ar;
                                    $adda = new artikul($ar);

                                    if (empty($adda->getId())) {
                                        echo '<div id="dop_prod_'.$ar.'" style="width: 100%; position: relative; margin-bottom: 20px;padding-left: 10px;box-sizing: border-box;">';
                                        echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_dop_product(\'-1\'); return false;">x</a></div>';
                                        echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">';
                                        echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:180px;text-align: left;">';
                                        echo '<span style="color:#fb921d;" class="ime_na_dop_prod_'.$adda->getId().'">изтрит продукт</span>';
                                        echo '</span></div> </div></div>';
                                        echo '<div style="clear: both;"></div>';
                                    } else {
                                        echo '<div id="dop_prod_'.$adda->getId().'" style="width: 100%; position: relative; margin-bottom: 20px;padding-left: 10px;box-sizing: border-box;">';
                                        echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_dop_product(\''.$adda->getId().'\'); return false;">x</a></div>';
                                        echo '<a href="'.url_admin.'product.php?id='.$adda->getId().'">';
                                        echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">  <img src="'.url.$adda->getKartinka_t().'" style="max-width: 60px;max-height:60px; border:0;vertical-align:middle;" />';
                                        echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:180px;text-align: left;">';
                                        echo $adda->getIme_marka().'<br><span class="ime_na_dop_prod_' . $adda->getId() . '">';
                                        echo $adda->getIme();
                                        echo '</span></div> </div> </a></div>';
                                        echo '<div style="clear: both;"></div>';
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div style="clear:both;height: 29px;"></div>
                    <?php foreach ($artikul->getUpgradeGroups() as $upgrade_group) : ?>
                        <div id="upgrade_group_<?php echo $upgrade_group->getId(); ?>" style="margin-bottom: 30px;">
                            <div style="background-color: #f2f2f2;width: 100%;vertical-align: middle;font-size: 13px; height: 25px;line-height: 25px;padding-left: 10px;"><?php echo $upgrade_group->getIme(); ?></div>
                            <span style="display: inline-block; font-size: 13px; margin-top: 5px; padding-left: 10px;">Серийно</span>
                            <?php if(!is_null($artikul->getDefaultUpgradeOptions($upgrade_group->getId()))) : ?>
                                <input id="upgrade_option_default_<?php echo $upgrade_group->getId(); ?>" name="upgrade_options[<?php echo $upgrade_group->getId(); ?>][default]" autocomplete="off" style="display: block; width: 100%;margin-top: 3px;margin-bottom: 5px;" type="text" value="<?php echo $artikul->getDefaultUpgradeOptions($upgrade_group->getId())->getIme(); ?>">
                            <?php else : ?>
                                <?php if(isset($_POST["upgrade_options"][$upgrade_group->getId()]["default"])) : ?>
                                    <input id="upgrade_option_default_<?php echo $upgrade_group->getId(); ?>" name="upgrade_options[<?php echo $upgrade_group->getId(); ?>][default]" autocomplete="off" style="display: block; width: 100%;margin-top: 3px;margin-bottom: 5px;" type="text" value="<?php echo $_POST["upgrade_options"][$upgrade_group->getId()]["default"];    ?>">
                                <?php else : ?>
                                    <input id="upgrade_option_default_<?php echo $upgrade_group->getId(); ?>" name="upgrade_options[<?php echo $upgrade_group->getId(); ?>][default]" autocomplete="off" style="display: block; width: 100%;margin-top: 3px;margin-bottom: 5px;" type="text">
                                <?php endif; ?>
                            <?php endif; ?>
                            <div class="option_labels" style="<?php echo count($artikul->getUpgradeOptions($upgrade_group->getId())) == 0 ? 'display: none;' : ''; ?>">
                                <span style="display: inline-block; font-size: 13px; margin-top: 5px; width: calc(100% - 125px);padding-left: 10px;">Опции</span>
                                <span style="display: inline-block; font-size: 13px; margin-top: 5px; margin-right: 15px;">дилър</span>
                                <span style="display: inline-block; font-size: 13px; margin-top: 5px;">цена</span>
                            </div>
                            <div class="upgrade_options">
                                <?php foreach ($artikul->getUpgradeOptions($upgrade_group->getId()) as $option) : ?>
                                    <div class="option">
                                        <input name='upgrade_options[<?php echo $upgrade_group->getId(); ?>][name][]' type='text' style='display: inline-block; width: calc(100% - 140px);margin-top: 3px;margin-bottom: 5px;' value='<?php echo $option->getIme(); ?>'><!--
                                        --><a href="#" onclick="remove_upgrade_option(this, '<?php echo $upgrade_group->getId(); ?>');return false;" class="plus plus-vid" style="display:inline-block; margin-top: 5px; position: relative;font-weight: normal; left; left: -15px;margin-right: -8px;top: -1px;font-size: 8pt;color: #f7931e;">X</a><!--
                                        --><span style='display: inline-block; width: 30px; text-align: center; line-height: 25px;'>+</span><!--
                                        --><input name='upgrade_options[<?php echo $upgrade_group->getId(); ?>][partner_price][]' type='text' style='display: inline-block; width: 50px;margin-top: 3px;margin-right: 10px;' value='<?php echo $option->getPricePartner(); ?>'><!--
                                        --><input name='upgrade_options[<?php echo $upgrade_group->getId(); ?>][price][]' type='text' style='display: inline-block; width: 50px;margin-top: 3px;' value='<?php echo $option->getPrice(); ?>'>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="error">Моля въведете серийна опция</div>
                            <a href="#" onclick="add_upgrade_option(<?php echo $upgrade_group->getId(); ?>);return false;" class="plus plus-vid" style="display:inline-block; padding-left: 10px;margin-top: 5px;"> + <span style="font-size: 13px; font-weight: normal;"><?php echo $upgrade_group->getIme(); ?></span></a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div style="clear: both; height: 45px;"></div>
                <div style="width: 100%; padding-top: 10px; height: 35px; position: fixed; bottom: 0px; background-color: #f2f2f2; border-top: 1px solid #f7931e;">
                    <div style="clear:both;font-size: 10pt">
                        <?php if($__user->permission_check('продукти','rw',true,true)) { ?>
                            <button type="submit" class="button-save" style="margin-left:10px;" name="submit"><?php echo lang_save ?></button>
                            <button type="button" class="button-cancel"
                                    onclick="location.href='browse.php?category=<?php echo $artikul->getId_kategoriq(); ?>&mode=<?php echo $artikul->getId_vid(); ?>&model=<?php echo $artikul->getId_model(); ?><?php echo isset($_SESSION['preorder']) && $_SESSION['preorder'] ? "&preorder" : ""; ?>'"
                            ><?php echo lang_cancel ?></button>
                            <button style="margin-left: 100px;" class="button-cancel" type="submit" name="template">clone product</button>
                            <?php if(!$artikul->IsNew()) : ?>
                                <button style="margin-left: 20px;" class="button-cancel" type="button" name="make-new"
                                        onclick="makeProductNew('<?php echo $artikul->getId(); ?>')">make new
                                </button>
                            <?php else: ?>
                                <button style="margin-left: 20px;" class="button-cancel" type="submit" disabled="disabled">make new</button>
                            <?php endif; ?>
                        <?php } else { ?>
                            няма права за едит <?php } ?>


                    </div>
                </div>
        </form>
    </div>
    <div class="modal">
        <div class="modal-content">
            <div class="modal-top-lenta">
                <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                <div class="modal-top-close" onclick="hideModal();">×</div>
            </div>

            <div class="modal-body">

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $(".input-tab-trigger-en-product-edit").click(function(){
                $(".input-tab-bg-product-edit").hide();
                $(".input-tab-ro-product-edit").hide();
                $(".input-tab-en-product-edit").show();
                $(".input-tab-trigger-en-product-edit").css("background-color","#666666");
                $(".input-tab-trigger-en-product-edit").css("color","#fff");
                $(".input-tab-trigger-bg-product-edit").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-edit").css("color","#333");
                $(".input-tab-trigger-ro-product-edit").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-edit").css("color","#333");
            });

            $(".input-tab-trigger-ro-product-edit").click(function(){
                $(".input-tab-bg-product-edit").hide();
                $(".input-tab-en-product-edit").hide();
                $(".input-tab-ro-product-edit").show();
                $(".input-tab-trigger-ro-product-edit").css("background-color","#666666");
                $(".input-tab-trigger-ro-product-edit").css("color","#fff");
                $(".input-tab-trigger-bg-product-edit").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-edit").css("color","#333");
                $(".input-tab-trigger-en-product-edit").css("background-color","#fff");
                $(".input-tab-trigger-en-product-edit").css("color","#333");
            });

            $(".input-tab-trigger-bg-product-edit").click(function(){
                $(".input-tab-bg-product-edit").show();
                $(".input-tab-en-product-edit").hide();
                $(".input-tab-ro-product-edit").hide();
                $(".input-tab-trigger-en-product-edit").css("background-color","#fff");
                $(".input-tab-trigger-en-product-edit").css("color","#333");
                $(".input-tab-trigger-ro-product-edit").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-edit").css("color","#333");
                $(".input-tab-trigger-bg-product-edit").css("background-color","#666666");
                $(".input-tab-trigger-bg-product-edit").css("color","#fff");
            });

            $(".input-tab-trigger-en-product-info-edit").click(function(){
                $(".input-tab-bg-product-info-edit").hide();
                $(".input-tab-ro-product-info-edit").hide();
                $(".input-tab-en-product-info-edit").show();
                $(".input-tab-trigger-en-product-info-edit").css("background-color","#666666");
                $(".input-tab-trigger-en-product-info-edit").css("color","#fff");
                $(".input-tab-trigger-bg-product-info-edit").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-info-edit").css("color","#333");
                $(".input-tab-trigger-ro-product-info-edit").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-info-edit").css("color","#333");
            });

            $(".input-tab-trigger-ro-product-info-edit").click(function(){
                $(".input-tab-bg-product-info-edit").hide();
                $(".input-tab-en-product-info-edit").hide();
                $(".input-tab-ro-product-info-edit").show();
                $(".input-tab-trigger-ro-product-info-edit").css("background-color","#666666");
                $(".input-tab-trigger-ro-product-info-edit").css("color","#fff");
                $(".input-tab-trigger-bg-product-info-edit").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-info-edit").css("color","#333");
                $(".input-tab-trigger-en-product-info-edit").css("background-color","#fff");
                $(".input-tab-trigger-en-product-info-edit").css("color","#333");
            });

            $(".input-tab-trigger-bg-product-info-edit").click(function(){
                $(".input-tab-bg-product-info-edit").show();
                $(".input-tab-en-product-info-edit").hide();
                $(".input-tab-ro-product-info-edit").hide();
                $(".input-tab-trigger-en-product-info-edit").css("background-color","#fff");
                $(".input-tab-trigger-en-product-info-edit").css("color","#333");
                $(".input-tab-trigger-ro-product-info-edit").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-info-edit").css("color","#333");
                $(".input-tab-trigger-bg-product-info-edit").css("background-color","#666666");
                $(".input-tab-trigger-bg-product-info-edit").css("color","#fff");
            });

            $(".input-tab-trigger-en-product-info2-edit").click(function(){
                $(".input-tab-bg-product-info2-edit").hide();
                $(".input-tab-ro-product-info2-edit").hide();
                $(".input-tab-en-product-info2-edit").show();
                $(".input-tab-trigger-en-product-info2-edit").css("background-color","#666666");
                $(".input-tab-trigger-en-product-info2-edit").css("color","#fff");
                $(".input-tab-trigger-bg-product-info2-edit").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-info2-edit").css("color","#333");
                $(".input-tab-trigger-ro-product-info2-edit").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-info2-edit").css("color","#333");
            });

            $(".input-tab-trigger-ro-product-info2-edit").click(function(){
                $(".input-tab-bg-product-info2-edit").hide();
                $(".input-tab-en-product-info2-edit").hide();
                $(".input-tab-ro-product-info2-edit").show();
                $(".input-tab-trigger-ro-product-info2-edit").css("background-color","#666666");
                $(".input-tab-trigger-ro-product-info2-edit").css("color","#fff");
                $(".input-tab-trigger-bg-product-info2-edit").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-info2-edit").css("color","#333");
                $(".input-tab-trigger-en-product-info2-edit").css("background-color","#fff");
                $(".input-tab-trigger-en-product-info2-edit").css("color","#333");
            });

            $(".input-tab-trigger-bg-product-info2-edit").click(function(){
                $(".input-tab-bg-product-info2-edit").show();
                $(".input-tab-en-product-info2-edit").hide();
                $(".input-tab-ro-product-info2-edit").hide();
                $(".input-tab-trigger-en-product-info2-edit").css("background-color","#fff");
                $(".input-tab-trigger-en-product-info2-edit").css("color","#333");
                $(".input-tab-trigger-ro-product-info2-edit").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-info2-edit").css("color","#333");
                $(".input-tab-trigger-bg-product-info2-edit").css("background-color","#666666");
                $(".input-tab-trigger-bg-product-info2-edit").css("color","#fff");
            });

            $(".input-tab-trigger-en-product-pre-order").click(function(){
                $(".input-tab-bg-product-pre-order").hide();
                $(".input-tab-ro-product-pre-order").hide();
                $(".input-tab-en-product-pre-order").show();
                $(".input-tab-trigger-en-product-pre-order").css("background-color","#666666");
                $(".input-tab-trigger-en-product-pre-order").css("color","#fff");
                $(".input-tab-trigger-bg-product-pre-order").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-pre-order").css("color","#333");
                $(".input-tab-trigger-ro-product-pre-order").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-pre-order").css("color","#333");
            });

            $(".input-tab-trigger-ro-product-pre-order").click(function(){
                $(".input-tab-bg-product-pre-order").hide();
                $(".input-tab-en-product-pre-order").hide();
                $(".input-tab-ro-product-pre-order").show();
                $(".input-tab-trigger-ro-product-pre-order").css("background-color","#666666");
                $(".input-tab-trigger-ro-product-pre-order").css("color","#fff");
                $(".input-tab-trigger-bg-product-pre-order").css("background-color","#fff");
                $(".input-tab-trigger-bg-product-pre-order").css("color","#333");
                $(".input-tab-trigger-en-product-pre-order").css("background-color","#fff");
                $(".input-tab-trigger-en-product-pre-order").css("color","#333");
            });

            $(".input-tab-trigger-bg-product-pre-order").click(function(){
                $(".input-tab-bg-product-pre-order").show();
                $(".input-tab-en-product-pre-order").hide();
                $(".input-tab-ro-product-pre-order").hide();
                $(".input-tab-trigger-en-product-pre-order").css("background-color","#fff");
                $(".input-tab-trigger-en-product-pre-order").css("color","#333");
                $(".input-tab-trigger-ro-product-pre-order").css("background-color","#fff");
                $(".input-tab-trigger-ro-product-pre-order").css("color","#333");
                $(".input-tab-trigger-bg-product-pre-order").css("background-color","#666666");
                $(".input-tab-trigger-bg-product-pre-order").css("color","#fff");
            });

            $(".redaktirai-image-trigger-0").click(function(){
                $(".redaktirai-image-1").show();
                $(".redaktirai-image-trigger-1").css("display","block");
                $(".redaktirai-image-e-0").show();
                $(".redaktirai-image-trigger-0").hide();
                $(".redaktirai-image-0").css("border","none");
            });
            $(".redaktirai-image-trigger-1").click(function(){
                $(".redaktirai-image-2").show();
                $(".redaktirai-image-trigger-2").css("display","block");
                $(".redaktirai-image-e-1").show();
                $(".redaktirai-image-trigger-1").hide();
                $(".redaktirai-image-1").css("border","none");
            });
            $(".redaktirai-image-trigger-2").click(function(){
                $(".redaktirai-image-3").show();
                $(".redaktirai-image-trigger-3").css("display","block");
                $(".redaktirai-image-e-2").show();
                $(".redaktirai-image-trigger-2").hide();
                $(".redaktirai-image-2").css("border","none");
            });
            $(".redaktirai-image-trigger-3").click(function(){
                $(".redaktirai-image-4").show();
                $(".redaktirai-image-trigger-4").css("display","block");
                $(".redaktirai-image-e-3").show();
                $(".redaktirai-image-trigger-3").hide();
                $(".redaktirai-image-3").css("border","none");
            });
            $(".redaktirai-image-trigger-4").click(function(){
                $(".redaktirai-image-5").show();
                $(".redaktirai-image-trigger-5").css("display","block");
                $(".redaktirai-image-e-4").show();
                $(".redaktirai-image-trigger-4").hide();
                $(".redaktirai-image-4").css("border","none");
            });
            $(".redaktirai-image-trigger-5").click(function(){
                $(".redaktirai-image-6").show();
                $(".redaktirai-image-trigger-6").css("display","block");
                $(".redaktirai-image-e-5").show();
                $(".redaktirai-image-trigger-5").hide();
                $(".redaktirai-image-5").css("border","none");
            });
            $(".redaktirai-image-trigger-6").click(function(){
                $(".redaktirai-image-7").show();
                $(".redaktirai-image-trigger-7").css("display","block");
                $(".redaktirai-image-e-6").show();
                $(".redaktirai-image-trigger-6").hide();
                $(".redaktirai-image-6").css("border","none");
            });
            $(".redaktirai-image-trigger-7").click(function(){
                $(".redaktirai-image-8").show();
                $(".redaktirai-image-trigger-8").css("display","block");
                $(".redaktirai-image-e-7").show();
                $(".redaktirai-image-trigger-7").hide();
                $(".redaktirai-image-7").css("border","none");
            });
            $(".redaktirai-image-trigger-8").click(function(){
                $(".redaktirai-image-9").show();
                $(".redaktirai-image-trigger-9").css("display","block");
                $(".redaktirai-image-e-8").show();
                $(".redaktirai-image-trigger-8").hide();
                $(".redaktirai-image-8").css("border","none");
            });
        });
    </script>
