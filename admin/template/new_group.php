<?php
unset($_SESSION['crop_thumb_image_location']);
unset($_SESSION['crop_large_image_location']);
unset($_SESSION['crop_html_img']);
unset($_SESSION['crop_html_img_t']);
unset($_SESSION['crop_html_img_b']);
unset($_SESSION['crop_html_img_input_hidden']);
$_SESSION['crop_upload_dir'] = '../../../images/bundle';
?>
<style>
    .addgroup {
        margin: 0px 10px 30px;
        display: block;
        width: 100px;
        font-size: 13px;
        font-family: 'Verdana', sans-serif;
        color: #8CC540;
    }

    .subMenu {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }

    .subMenu a {
        color: #333;
        display: inline-block;
        margin: 0 15px;
        font-size: 13px;
    }

    .subMenu a:hover, .subMenu a.active {
        color: #F7931E;
    }

    #cont {
        margin-left: 10px;
        font-size: 13px;
        padding-bottom: 60px;
    }

    #cont table {
        margin-top: 20px;
    }

    #cont table tr td {
        font-size: 14px;
    }

    #cont table tr td {
        padding: 0px 10px 5px 0px;
    }

    #cont input[type=text] {
        width: 300px;
        height: 26px;
        border: none;
        padding-left: 10px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        border: 1px solid #666666;
    }

    textarea {
        padding-left: 10px;
        width: 300px;
        height: 130px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        border: 1px solid #666666;
    }

    .input-tab-trigger-bg-new-group-ime {
        float: right;
        color: #fff;
        font-size: 13px;
        background-color: #666666;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }

    .input-tab-trigger-bg-new-group-ime:hover {
        color: #fff !important;
        background-color: #ff9207 !important;
    }

    .input-tab-trigger-en-new-group-ime {
        float: right;
        color: #333;
        font-size: 13px;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }

    .input-tab-trigger-en-new-group-ime:hover {
        color: #fff !important;
        background-color: #ff9207 !important;
    }

    .input-tab-trigger-ro-new-group-ime {
        float: right;
        color: #333;
        font-size: 13px;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }

    .input-tab-trigger-ro-new-group-ime:hover {
        color: #fff !important;
        background-color: #ff9207 !important;
    }

    .input-tab-bg-new-group-ime {
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        border: 1px solid #666666;
        height: 26px;
    }

    .input-tab-en-new-group-ime {
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        display: none;
        border: 1px solid #666666;
        height: 26px;
    }

    .input-tab-ro-new-group-ime {
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        display: none;
        border: 1px solid #666666;
        height: 26px;
    }

    .input-tab-trigger-bg-new-group-info {
        float: right;
        color: #fff;
        font-size: 13px;
        background-color: #666666;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }

    .input-tab-trigger-bg-new-group-info:hover {
        color: #fff !important;
        background-color: #ff9207 !important;
    }

    .input-tab-trigger-en-new-group-info {
        float: right;
        color: #333;
        font-size: 13px;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }

    .input-tab-trigger-en-new-group-info:hover {
        color: #fff !important;
        background-color: #ff9207 !important;
    }

    .input-tab-trigger-ro-new-group-info {
        float: right;
        color: #333;
        font-size: 13px;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }

    .input-tab-trigger-ro-new-group-info:hover {
        color: #fff !important;
        background-color: #ff9207 !important;
    }

    .input-tab-bg-new-group-info {
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        border: 1px solid #666666;
    }

    .input-tab-en-new-group-info {
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        display: none;
        border: 1px solid #666666;
    }

    .input-tab-ro-new-group-info {
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        display: none;
        border: 1px solid #666666;
    }
</style>
<div class="subMenu">
    <a href="<?php echo url_admin; ?>moduls/promo.php">Групови промоции</a>
    <a class="active" href="<?php echo url_admin; ?>groups.php">Пакети</a>
</div>
<?php if (isset ($errors)) { ?>
    <div style="margin-left: 10px; color: red; font-size: 13px;">
        <?php foreach ($errors as $v) {
            echo $v . '<br>';
        } ?>
    </div>
<?php } ?>
<div id="cont">
    <div style="color:#fb921d;">добавяне на нов пакет</div>
    <form action="" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <td>
                    <div class="input-tab-holder" style="margin-top: 10px; width: 300px;">
                        <div class="input-tab-title">име</div>
                        <?php
                        $inputs = "";
                        foreach (array_reverse($__languages) as $key => $v) {
                            echo '<div class="input-tab-trigger-' . $v->getName() . '-new-group-ime">' . $v->getName() . '</div>';
                            $inputs = $inputs . '<input class="input-tab-' . $v->getName() . '-new-group-ime" name="new_name_' . $v->getPrefix() . '" value="';
                            if (isset($_POST['new_name_' . $v->getPrefix()])) $inputs = $inputs . $_POST['new_name_' . $v->getPrefix()];
                            $inputs = $inputs . '" style="width: 300px;" type="text">';
                        }
                        ?>
                    </div>
                    <?php echo $inputs; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    снимка*
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a id="product_main_image" rel="fancybox-thumb" href="#"><img style="border:none;max-height:180px;"
                                                                                  src="<?php if (isset($_POST['product_main_image']) && $_POST['product_main_image'] != "") {
                                                                                      echo url . $_POST['product_main_image'];
                                                                                  } else {
                                                                                      echo url . '/images/blank_400X300.png';
                                                                                  } ?>" ALT="image"></a>
                    <script type="text/javascript" src="<?php echo url; ?>js/jquery.lightbox-0.5.min.js"></script>
                    <link rel="stylesheet" href="<?php echo url; ?>css/jquery.lightbox-0.5.css" type="text/css"/>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("a[rel=fancybox-thumb][href!='']").fancybox({
                                prevEffect: 'fade',
                                nextEffect: 'fade',
                                nextSpeed: 700,
                                prevSpeed: 700,
                                helpers: {
                                    title: {
                                        type: 'outside'
                                    },
                                    overlay: {
                                        opacity: 0.8,
                                        css: {
                                            'background-color': '#000'
                                        }
                                    },
                                    thumbs: {
                                        width: 80,
                                        height: 45
                                    }
                                }
                            });
                        });
                    </script>
                    <div id="product_main_image"><input name="product_main_image" type="hidden"
                                                        value="<?php if (isset($_POST['product_main_image'])) echo $_POST['product_main_image']; ?>"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><a class="thickbox"
                                   style="color: #fff;background-color: #666666;margin-top: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;width:100px;font-size: 13px;line-height: 25px;"
                                   href="moduls/crop/new_crop_products.php?crop_html_a=product_main_image&crop_html_img_input_hidden=product_main_image input&crop_width=600&crop_height=450&crop_html_img_t=product_main_image img&crop_upload_dir=../../../images/bundle/&keepThis=true&TB_iframe=true&height=650&width=1280">редактирай</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="margin-top: 20px;">
                        <div>добавяне на продукт към пакета</div>
                        <input class="search" style="display: block; float: left; width: 300px; margin-top: 5px;"
                               type="text" onclick="this.value=''" onkeyup="add_prod_search_suggestion();"
                               name="add_prod_search" placeholder="търсене">
                        <div style="clear: both;"></div>
                        <div id="add_prod_search_suggestion"
                             style="display: block; float: left; background-color: #FFFFFF; width: 300px; z-index:2; margin-top: 1px;"></div>
                        <input type="hidden" name="add_products_ids" id="add_products_ids"
                               value="<?php if (isset($_POST['add_products_ids'])) echo $_POST['add_products_ids']; ?>">
                        <div style="margin-top: 20px;" id="add_products">
                            <?php
                            if (isset($_POST['add_products_ids'])) {
                                $productite = $_POST['add_products_ids'];
                                if ($productite != "") {
                                    $producti = explode(",", $productite);
                                    foreach ($producti as $ar) {
                                        $ar = (int)$ar;
                                        $adda = new artikul($ar);
                                        echo '<div id="dop_prod_' . $adda->getId() . '" style="width: 300px; position: relative; margin-bottom: 20px;">';
                                        echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_dop_product(\'' . $adda->getId() . '\'); return false;">x</a></div>';
                                        echo '<a href="' . url_admin . 'product.php?id=' . $adda->getId() . '">';
                                        echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">  <img src="' . url . $adda->getKartinka_t() . '" style="max-width: 60px;max-height:60px; border:0;vertical-align:middle;" />';
                                        echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:180px;text-align: left;"> ';
                                        echo $adda->getIme_marka() . '<br>';
                                        echo $adda->getIme();
                                        echo '</div> </div> </a></div>';
                                        echo '<div style="clear: both;"></div>';
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <div style="text-align:left;margin:-10px 0 5px 0">намаление</div>
                    <input type="text" name="percent" style="width: 60px;"
                           value="<?php if (isset($_POST['percent'])) echo $_POST['percent']; ?>"> %
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="input-tab-holder" style="margin-top: 10px; width: 300px;">
                        <div class="input-tab-title">информация</div>
                        <?php
                        $inputs = "";
                        foreach (array_reverse($__languages) as $key => $v) {
                            echo '<div class="input-tab-trigger-' . $v->getName() . '-new-group-info">' . $v->getName() . '</div>';
                            $inputs = $inputs . '<textarea class="input-tab-' . $v->getName() . '-new-group-info" name="new_artikul_info_' . $v->getPrefix() . '" value="';
                            if (isset($_POST['new_artikul_info_' . $v->getPrefix() . ''])) $inputs = $inputs . $_POST['new_artikul_info_' . $v->getPrefix() . ''];
                            $inputs = $inputs . '" style="width: 300px;"></textarea>';
                        }
                        ?>
                    </div>
                    <?php echo $inputs; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" value="save" class="button-save"
                           style="margin-top: 10px; float: left;">
                    <a type="submit" class="button-cancel" href="groups.php" href="groups.php"
                       style="display: inline-block;line-height: 25px; text-align:center;float:left;font-size:13px;margin-top: 10px;margin-left: 10px;padding-bottom:3px;height: 22px;">cancel</a>
                </td>
            </tr>
        </table>
    </form>
</div>

<script>
    $(document).ready(function () {
        $(".input-tab-trigger-ro-new-group-ime").click(function () {
            $(".input-tab-bg-new-group-ime").hide();
            $(".input-tab-en-new-group-ime").hide();
            $(".input-tab-ro-new-group-ime").show();
            $(".input-tab-trigger-ro-new-group-ime").css("background-color", "#666666");
            $(".input-tab-trigger-ro-new-group-ime").css("color", "#fff");
            $(".input-tab-trigger-bg-new-group-ime").css("background-color", "#fff");
            $(".input-tab-trigger-bg-new-group-ime").css("color", "#333");
            $(".input-tab-trigger-en-new-group-ime").css("background-color", "#fff");
            $(".input-tab-trigger-en-new-group-ime").css("color", "#333");
        });

        $(".input-tab-trigger-en-new-group-ime").click(function () {
            $(".input-tab-bg-new-group-ime").hide();
            $(".input-tab-ro-new-group-ime").hide();
            $(".input-tab-en-new-group-ime").show();
            $(".input-tab-trigger-en-new-group-ime").css("background-color", "#666666");
            $(".input-tab-trigger-en-new-group-ime").css("color", "#fff");
            $(".input-tab-trigger-bg-new-group-ime").css("background-color", "#fff");
            $(".input-tab-trigger-bg-new-group-ime").css("color", "#333");
            $(".input-tab-trigger-ro-new-group-ime").css("background-color", "#fff");
            $(".input-tab-trigger-ro-new-group-ime").css("color", "#333");
        });

        $(".input-tab-trigger-bg-new-group-ime").click(function () {
            $(".input-tab-bg-new-group-ime").show();
            $(".input-tab-en-new-group-ime").hide();
            $(".input-tab-ro-new-group-ime").hide();
            $(".input-tab-trigger-en-new-group-ime").css("background-color", "#fff");
            $(".input-tab-trigger-en-new-group-ime").css("color", "#333");
            $(".input-tab-trigger-ro-new-group-ime").css("background-color", "#fff");
            $(".input-tab-trigger-ro-new-group-ime").css("color", "#333");
            $(".input-tab-trigger-bg-new-group-ime").css("background-color", "#666666");
            $(".input-tab-trigger-bg-new-group-ime").css("color", "#fff");
        });

        $(".input-tab-trigger-ro-new-group-info").click(function () {
            $(".input-tab-bg-new-group-info").hide();
            $(".input-tab-en-new-group-info").hide();
            $(".input-tab-ro-new-group-info").show();
            $(".input-tab-trigger-ro-new-group-info").css("background-color", "#666666");
            $(".input-tab-trigger-ro-new-group-info").css("color", "#fff");
            $(".input-tab-trigger-bg-new-group-info").css("background-color", "#fff");
            $(".input-tab-trigger-bg-new-group-info").css("color", "#333");
            $(".input-tab-trigger-en-new-group-info").css("background-color", "#fff");
            $(".input-tab-trigger-en-new-group-info").css("color", "#333");
        });

        $(".input-tab-trigger-en-new-group-info").click(function () {
            $(".input-tab-bg-new-group-info").hide();
            $(".input-tab-ro-new-group-info").hide();
            $(".input-tab-en-new-group-info").show();
            $(".input-tab-trigger-en-new-group-info").css("background-color", "#666666");
            $(".input-tab-trigger-en-new-group-info").css("color", "#fff");
            $(".input-tab-trigger-bg-new-group-info").css("background-color", "#fff");
            $(".input-tab-trigger-bg-new-group-info").css("color", "#333");
            $(".input-tab-trigger-ro-new-group-info").css("background-color", "#fff");
            $(".input-tab-trigger-ro-new-group-info").css("color", "#333");
        });

        $(".input-tab-trigger-bg-new-group-info").click(function () {
            $(".input-tab-bg-new-group-info").show();
            $(".input-tab-en-new-group-info").hide();
            $(".input-tab-ro-new-group-info").hide();
            $(".input-tab-trigger-en-new-group-info").css("background-color", "#fff");
            $(".input-tab-trigger-en-new-group-info").css("color", "#333");
            $(".input-tab-trigger-ro-new-group-info").css("background-color", "#fff");
            $(".input-tab-trigger-ro-new-group-info").css("color", "#333");
            $(".input-tab-trigger-bg-new-group-info").css("background-color", "#666666");
            $(".input-tab-trigger-bg-new-group-info").css("color", "#fff");
        });
    });
</script>