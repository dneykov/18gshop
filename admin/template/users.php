<?php

$page_current = 'users';

require('template/__top.php');
/* @var $artikul artikul_admin */
?>

<style>
    #tpmenu{
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #tpmenu a{
        color: #333;
        font-size: 13px;
        margin:0 15px;
    }

    #tpmenu a:hover {
        color: rgb(248, 147, 29);
        text-decoration: none;
    }
    .users td{
        font-size: 9pt;
        white-space: nowrap;
        overflow: hidden;
        o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        height: 20px;
    }
    .users {
        margin-top: 0px;
        margin-left: 10px;
        width: 1000px;
        border: none;
        margin-bottom: 50px;
    }
    .users tr {
        height: 20px;
    }
    .users a {
        color: #333;
    }
    .users tr td a{
        width: 100%;
        display: block;
    }
    .users tr:nth-child(even){
        background-color: #fff;
        color: #333;
    }
    .users tr:nth-child(odd){
        background-color: #f2f2f2;
        color: #333;
    }
    .users tr:hover{
        background-color: #F7931E !important;
    }
    .users tr:hover td > a{
        color: #FFF !important;
    }
    #tablehead:hover {
        background-color: #999 !important;
    }
    .productLink:hover{
        text-decoration:none;
    }
    .users{
        border-collapse: collapse;
    }
</style>
<div id="tpmenu">
    <?php if($__user->permission_check('администратори', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users_admin.php" <?php  ?>>администратори</a>
    <?php } ?>
    <?php if($__user->permission_check('потребители', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users.php" style="color: orange;">клиенти</a>
<!--        <a href="--><?php //echo url_admin; ?><!--partners.php">партньори</a>-->
        <a href="<?php echo url_admin; ?>newsletter.php">абонирани за новини</a>
    <?php } ?>
</div>
<table class="users" cellpadding="3">
    <tr id="tablehead" style="background-color: #666666; color: #fff;">
        <td style="padding-left:5px;width: 335px; padding-right:10px;">И-мейл</td>
        <td style="padding-left:5px;width: 300px; padding-right:10px;">Име</td>
        <td style="padding-left:5px;width: 220px; padding-right:10px;">Телефон</td>
        <td style="padding-left:5px;width: 50px; padding-right:0px;;">Поръчки</td>
        <td style="width: 10px;"></td>
    </tr>
<?php if($users) foreach ($users as $v) { ?>
    <?php
        $style = "";
        $stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ?");
        $stm->bindValue(1, $v->getId(), PDO::PARAM_STR);
        $stm->execute();
        $ordersCount = $stm->rowCount();
        if($ordersCount == 0) $style="style='color: #666666'";
    ?>
    <tr>
        <td style="padding-left:5px;max-width: 335px; padding-right:10px;"><a <?php echo $style; ?> href="<?php echo url.'admin/users.php?usr='.$v->getId(); ?>"><?php echo $v->getMail(); ?></a></td>
        <td style="padding-left:5px;max-width: 300px; padding-right:10px;"><a <?php echo $style; ?> href="<?php echo url.'admin/users.php?usr='.$v->getId(); ?>" class="ime_na_user_<?php echo $v->getId(); ?>"><?php echo $v->getName().' '.$v->getName_last(); ?></a></td>
        <td style="padding-left:5px;max-width: 220px; padding-right:10px;"><a <?php echo $style; ?> href="<?php echo url.'admin/users.php?usr='.$v->getId(); ?>"><?php echo $v->getPhone_home(); if($v->getPhone_home() != "") echo ", "; ?><?php echo $v->getPhone_mobile(); ?></a></td>
        <td style="padding-left:5px;max-width: 50px; padding-right:0px;text-align: center;"><a <?php echo $style; ?> href="<?php echo url.'admin/users.php?usr='.$v->getId(); ?>"><?php echo $ordersCount; ?></a></td>
        <td style="padding-left:5px;padding-right:10px;text-align: right;"><a href="#" onclick="users_delete(<?php echo $v->getId(); ?>);return false;"><?php lang_delete_x(); ?></a></td>
    </tr>
<?php } ?>
</table>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>