<?php
$page_current = 'orders';

$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "TotalТurnover"');
$stm -> execute();
$totalTurnover=$stm->fetch();
$totalTurnover=$totalTurnover['value'];


$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "TotalProfit"');
$stm -> execute();
$totalProfit=$stm->fetch();
$totalProfit=$totalProfit['value'];

require('template/__top.php');

?>
<style type="text/css">
	table td{
		font-size: 10pt;
	}
	.subMenu{
		width: 100%;
		height: 23px;
		margin-top: -10px;
		background-color: #808080;
	}
	.subMenu a{
		color:#F7931E;
		font-size: 13px;
		margin:0 15px;
	}
	.subMenu a:hover{
		color:#F7931E !important;
	}
	.productLink:hover{
		text-decoration:none;
	}
	.orders{
		border-collapse: collapse;
	}
</style>
<div class="subMenu">

    <a href="<?php echo url_admin; ?>orders.php" <?php if((isset($_GET['approved']))||(isset($_GET['done'])||(isset($_GET['statistik'])))) echo 'style="color: #FFFFFF;"' ?>>Нови <?php echo order_admin::getCountForWaiting(); ?></a>   
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo url_admin; ?>orders.php?approved" <?php if(!isset($_GET['approved'])) echo 'style="color: #FFFFFF;"' ?>>Одобрени <?php echo order_admin::getCountForApproved(); ?></a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo url_admin; ?>orders.php?done" <?php if(!isset($_GET['done'])) echo 'style="color: #FFFFFF;"' ?>>Изпълнени <?php echo order_admin::getCountForDone(); ?></a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo url_admin; ?>orders.php?statistik">Отчет</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo url_admin; ?>orders.php?delivery" style="color: #FFFFFF;">Доставка</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo url_admin; ?>orders.php?valute" style="color: #FFFFFF;">Валутен курс</a>

</div>
<div style="margin-left: 5px">
<?php
	echo '<br />Оборот: '.$totalTurnover.' лв.';
	echo '<br />Печалба: '.$totalProfit.' лв.';
?>
</div>
