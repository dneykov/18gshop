<?php
$page_current = 'users';
require('template/__top.php');
$user = new user((int)$_GET['usr']);
?>
<style type="text/css">
    .partnership {
        margin: 10px 0!important;
    }
    .partnership a {
        background: #F7931E;
        color: #fff;
        padding: 2px 10px 4px 10px;
        line-height: 20px;
    }
    .partnership a:hover {
        color: #fff!important;
    }
	.userBox {
		margin-left: 10px;
		width: 1000px;
		font-size: 10pt;
		margin-top: 10px;
		padding-bottom: 50px;
	}
	.userOrders {
		margin-top: 10px;
	}
	button {
		margin-right: 10px;
		margin-top: 20px;
	}
	.orders td{
		font-size: 9pt;
		white-space: nowrap;
		overflow: hidden;
		o-text-overflow: ellipsis;
		text-overflow: ellipsis;
		height: 20px;
	}
	.orders {
		margin-top: 5px;
		width: 550px;
		border: none;
		margin-bottom: 20px;
	}
	.orders tr {
		height: 20px;
	}
	.orders a {
		color: #333;
	}
	.orders tr:nth-child(even){
        background-color: #fff;
        color: #333;
    }
    .orders tr:nth-child(odd){
        background-color: #f2f2f2;
        color: #333;
    }
	.orders tr:hover{
		background-color: #F7931E !important;
		color: #fff !important;
	}
	.orders tr:hover td > a{
		color: #FFF !important;
	}
	#tablehead:hover {
		background-color: #999 !important;
	}
	.productLink:hover{
		text-decoration:none;
	}
	.orders{
		border-collapse: collapse;
	}
	.userOrders {
		margin-top: 10px;
	}
	.userAdminComment {
		margin-bottom: 10px;
	}
	#tpmenu{
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #tpmenu a{
        color: #333;
        font-size: 13px;
        margin:0 15px;
    }

    #tpmenu a:hover {
        color: rgb(248, 147, 29);
        text-decoration: none;
    }
    .userNames {
		position: relative;
		min-width: 180px;
		padding-right: 20px;
		display: inline-block;
		height: 15px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
        $('#save').click(function(){
            var id = $('#user_id').val();
            var comment = $('#admin_comment').val();
            var clientNumber = $('#cnumber').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_user_comment.php",
                data: "id="+id+"&comment="+comment+"&clientNumber="+clientNumber,
                cache: false,
                success: function(html){
                    $('#jsResponse').html('<div style="margin-top:5px; color: #b3b3b3;">Профила беше обновен!</div>');
                }
            });
        });
		$('#cancel').click(function(){
			location.reload();
		});
	});
</script>
<div id="tpmenu">
    <?php if($__user->permission_check('администратори', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users_admin.php">администратори</a>
    <?php } ?>
    <?php if($__user->permission_check('потребители', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users.php" <?php if(!$user->is_partner()) { echo 'style="color: orange;"';}?>>клиенти</a>
<!--        <a href="--><?php //echo url_admin; ?><!--partners.php" --><?php //if($user->is_partner()) { echo 'style="color: orange;"';}?><!--партньори</a>-->
        <a href="<?php echo url_admin; ?>newsletter.php">бюлетин</a>
    <?php } ?>
</div>
<div class="userBox">
	<div class="userNames"><span style="color: #b3b3b3;">Име</span> <span style='color: #F7931E;' class="ime_na_user_<?php echo $user->getId(); ?>"><?php echo $user->getName().' '.$user->getName_last(); ?></span> <a href="#" onclick="users_delete_inside(<?php echo $user->getId(); ?>);return false;" style="position: absolute; top:3px; right: 0px;padding-left: 2px; padding-right: 2px;background: #F7931E;color:#FFF;font-size: 10px;">X</a></div>
	<div class="userEmail"><span style="color: #b3b3b3;">И-мейл</span> <?php echo $user->getMail(); ?></div>
	<div class="userPhones"><span style="color: #b3b3b3;">Телефон</span> <?php echo $user->getPhone_home(); if($user->getPhone_home() != "") echo ", "; ?><?php echo $user->getPhone_mobile(); ?></div>
	<div class="userAdress"><span style="color: #b3b3b3;">Адрес</span> <?php echo $user->getAdress_town(); ?>, <?php echo $user->getAdress_street().' '.$user->getAdress_number(); ?>
	<?php 
	if($user->getAdress_vhod() != "") echo " вх. ".$user->getAdress_vhod();
	if($user->getAdress_etaj() != "") echo " ет. ".$user->getAdress_etaj();
	if($user->getAdress_ap() != "") echo " ап. ".$user->getAdress_ap();
	?>
	</div>
    <?php if($user->createdAt() != "") : ?>
    <div class="userNames"><span style="color: #b3b3b3;">Регистриран на</span> <?php echo date("d-m-Y H:i:s", strtotime($user->createdAt())); ?></div>
    <?php endif; ?>
    <?php if($user->getAccountType() == 1) : ?>
        <br/>
        <div class="userNames"><span style="color: #b3b3b3;">Име на фирма</span> <?php echo $user->getCompanyName(); ?></div>
        <div class="userEmail"><span style="color: #b3b3b3;">М.О.Л</span> <?php echo $user->getCompanyMol(); ?></div>
        <div class="userPhones"><span style="color: #b3b3b3;">ЕИК</span> <?php echo $user->getCompanyEik(); ?></div>
        <div class="userAdress"><span style="color: #b3b3b3;">ЗДДС №:</span> <?php echo $user->getCompanyVat(); ?></div>
        <div class="userAdress"><span style="color: #b3b3b3;">Адрес</span> <?php echo $user->getCompanyCity(); ?>, <?php echo $user->getCompanyStreet().' '.$user->getCompanyNumber(); ?></div>
        <div class="userAdress"><span style="color: #b3b3b3;">Телефон</span> <?php echo $user->getCompanyPhone(); ?></div>
    <?php endif; ?>
    <?php if($user->is_partner()) {?>
        <div class="ClientNumber"><span style="color: #b3b3b3;">Клиентски No</span> <input type="text" name="cnumber" id="cnumber" value="<?php echo $user->getClientNumber(); ?>"/></div>
    <?php } ?>
    <div class="partnership">

<!--        --><?php //if(!$user->is_partner()) {
//            echo '<a href="#" data-value="1" title="partner">Партньорство</a>';
//        } else {
//            echo '<a href="#" data-value="0" title="partner">Прекрати партньорство</a>';
//        }
//        ?>
    </div>
	<div class="userOrders"><span style="color: #b3b3b3;">Поръчки</span>
		<?php
            $stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ? ORDER BY `id` DESC");
            $stm->bindValue(1, $user->getId(), PDO::PARAM_STR);
            $stm->execute();
            echo '<table class="orders" cellpadding="3">
					<tr id="tablehead" style="background-color: #666; color: #fff;">
						<td style="padding-left:5px;">Номер</td>
						<td style="padding-left:5px;">Дата</td>
						<td style="padding-left:5px;">Коментар</td>
						<td style="padding-left:5px;">Продукти</td>
						<td style="padding-left:5px;">Общо</td>
						<td style="padding-left:5px;padding-right:5px;">Статус</td>
						<td></td>
					</tr>';
			if($stm->rowCount() > 0){
	            foreach($stm->fetchAll() as $a){
	            	$order = new order_admin((int)$a['id']);
					$contactInformation=$order->getContactInformation();
					$orderProducts=$order->getProducts();
					$deletedItems=$order->getDeletedItems();
					$orderCost = $order->getCost();
					?>
						<tr>
							<td style="padding-left:5px; width: 85px; padding-right:5px;"><a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>" class="ime_na_order_<?php echo $order->getId(); ?>"><?php echo $order->getNomer()." "; ?></a></td>
							<td style="padding-left:5px; width: 150px; padding-right:10px;">
								<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
									<?php 
										if(isset($_GET['done'])){
											$dateDone=$order->getDateDone();
											if(isset($dateDone)){
												echo $order->getDateAccepted();
											}
										} else if(isset($_GET['approved'])){
											$dateAccepted=$order->getDateAccepted();
											if(isset($dateAccepted)){
												echo $order->getDateAccepted();
											}
										} else {
											echo $order->getDate();
										}
									?>
								</a>
							</td>
							<td style="padding-left:5px; width: 100px;padding-right:10px;"><a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>"><?php echo mb_substr($order->getComment(), 0, 10);?>&nbsp;</td>
							<td style="padding-left:5px; width: 70px;padding-right:10px;text-align: center;">
								<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
									<?php
										$brAll=0;
										if(isset($orderProducts)){
											foreach($orderProducts as $key => $val){
												if(in_array((int)$key, $deletedItems)){
													$orderCost = $orderCost - $orderProducts[$key]['price']*$orderProducts[$key]['count'];
													continue;
												}									
												$brAll += (int)$orderProducts[$key]['count'];
											}
											echo $brAll;
										}
									?>
								</a>
							</td>
							<td style="padding-left:5px;padding-right:10px;">
								<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
									<?php 
										echo $orderCost." лв";
										if($order->getPaypal() == 1){
											echo "<br />Paypal";
											if($order->getPaypalPaid() == 1){
												echo ": платено";
											}
										}
									?>
								</a>
							</td>
							<td style="padding-left:5px;padding-right:5px;">
								<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
									<?php
										if($order->getStatus() == 2){
											echo "Изпълнена";
										}else if ($order->getStatus() == 1){									
											echo 'Одобрена' ;
										}else{
											echo 'Нова' ;
										}
									?>
								</a>
							</td>
							<td style="padding-left:5px;padding-right:10px;"><a href="#" onclick="javascript: return deleteOrder(<?php echo $order->getID(); ?>);" style="color:#FF0000;">X</a></td>
						</tr>
					<?php
	            }
	       	} else {
	       		echo "<tr><td colspan='7' style='padding-left:5px;'>Потребителя няма поръчки</td></tr>";
	       	}
            echo "</table>";
        ?>
	</div>
	<div class="userAdminComment">
		<span style="color: #b3b3b3;">Коментар от Администратор</span><br />
		<input type="hidden" id="user_id" value="<?php echo $_GET['usr']; ?>">
		<textarea id="admin_comment" style="width: 400px; height: 150px;font-family: Verdana; font-size: 10pt;margin-top:5px;"><?php echo $user->getAdminComment(); ?></textarea>
	</div>
	<div id="jsResponse"></div>
	<button id="save" class="button-save">save</button>
	<button id="cancel" class="button-cancel">cancel</button>
</div>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>