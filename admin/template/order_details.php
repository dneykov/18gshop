<?php
$page_current = 'orders';
require('template/__top.php');
?>
<style type="text/css">
    .subMenu {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }

    .subMenu a {
        color: #F7931E;
        font-size: 13px;
        margin: 0 15px;
    }

    .subMenu a:hover {
        color: #F7931E !important;
    }

    .orderBox {
        margin-left: 10px;
        width: 1000px;
        font-size: 10pt;
        margin-top: 10px;
        padding-bottom: 50px;
    }

    .orderNumber {
        position: relative;
        min-width: 200px;
        display: inline-block;
        margin-bottom: 5px;
        height: 20px;
    }

    .orderDates {
        display: inline-block;
        margin-right: 30px;
        margin-bottom: 20px;
    }

    .orderPaymentType {
        margin-bottom: 20px;
    }

    .orderComment {
        margin-bottom: 20px;
    }

    .orderProducts {
        margin-top: 30px;
        margin-bottom: 20px;
    }

    .orderProduct {
        min-width: 200px;
        display: inline-block;
        margin-right: 10px;
    }

    .orderProduct:nth-child(even) {
        border-top: 1px bulet #ccc;
        border-bottom: 1px bulet #ccc;
    }

    .orderPrice {
        margin-bottom: 10px;
    }

    .orderStatus {
        margin-bottom: 10px;
    }

    .orderAdminComment {
        margin-bottom: 10px;
    }

    .button-save:focus {
        outline: none;
    }

    .button-save:disabled {
        background: #ccc;
        cursor: default;
    }

    button {
        margin-right: 10px;
        margin-top: 20px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#save').click(function () {
            var id = $('#order_id').val();
            var comment = $('#admin_comment').val();
            var product_ids = $('#offer_products_ids').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_order_admin_comment.php",
                data: "id=" + id + "&comment=" + comment + "&product_ids=" + product_ids,
                cache: false,
                success: function (html) {
                    $('#jsResponse').html('<div style="margin-top:5px; color: #b3b3b3;">Поръчката беше обновена!</div>');
                }
            });
        });
        $('#saveAndSend').click(function () {
            var id = $('#order_id').val();
            var comment = $('#admin_comment').val();
            var product_ids = $('#offer_products_ids').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_order_product_offers.php",
                data: "id=" + id + "&comment=" + comment + "&product_ids=" + product_ids,
                cache: false,
                success: function () {
                    $('#jsResponse').html('<div style="margin-top:5px; color: #b3b3b3;">Мейла е изпратен успешно и поръчката беше обновена!</div>');
                    $('#saveAndSend').attr("disabled", "disabled");
                    $('#saveAndSend').unbind("click");
                    $('#saveAndSend').removeAttr("id");

                }
            });
        });
        $('#cancel').click(function () {
            location.reload();
        });
    });

    function deleteOrderProduct(id) {
        ime_na_kol = $(".ime_na_product_poruchka_" + id).text();
        $(".modal-body").html('<div style="width: 100%; height: 40px;">' + ime_na_kol + '</div> <button class="button-cancel" onclick="hideModal();" style="margin-top:0px !important;position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deleteOrderProduct_Ajax(\'' + id + '\');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right;margin-top:0px !important; padding-bottom: 0px;">Yes</button>');
        $(".modal").show();
    }

    function deleteOrderProduct_Ajax(id) {
        var orderid = $('#order_id').val();
        $.ajax({
            type: "POST",
            url: "ajax/ajax_delete_order_item.php",
            data: "id=" + id + "&orderid=" + orderid,
            cache: false,
            success: function (html) {
                location.reload();
            }
        });
    }

</script>
<?php
$order = new order_admin((int)$_GET['ord']);
?>
<div class="subMenu">

    <a href="<?php echo url_admin; ?>orders.php" <?php if ($order->getStatus() == 0) echo "style='color: #f8931d;'"; else echo 'style="color: #333;"'; ?>>Нови <?php echo order_admin::getCountForWaiting(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?approved" <?php if ($order->getStatus() == 1) echo "style='color: #f8931d;'"; else echo 'style="color: #333;"'; ?>>Одобрени <?php echo order_admin::getCountForApproved(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?done" <?php if ($order->getStatus() == 2) echo "style='color: #f8931d;'"; else echo 'style="color: #333;"'; ?>>Изпълнени <?php echo order_admin::getCountForDone(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?cancel" <?php if ($order->getStatus() == 4) echo "style='color: #f8931d;'"; else echo 'style="color: #333;"'; ?>>Отказани <?php echo order_admin::getCountForCanceled(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?return" <?php if ($order->getStatus() == 5) echo "style='color: #f8931d;'"; else echo 'style="color: #333;"'; ?>>Върнати <?php echo order_admin::getCountForReturned(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?statistik" style="color: #333;">Отчет</a>
    <a href="<?php echo url_admin; ?>orders.php?delivery" style="color: #333;">Доставка</a>
    <a href="<?php echo url_admin; ?>orders.php?valute" style="color: #333;">Настройки</a>
</div>
<div class="orderBox">
    <div class='orderNumber'><span style="color: #b3b3b3;">Поръчка</span> <span style='color: #f8931d;'
                                                                                class="ime_na_order_<?php echo $order->getID(); ?> ime_na_order_approved_<?php echo $order->getID(); ?>"><?php echo $order->getNomer(); ?></span>
        <?php if ($order->getStatus() < 2 || $order->getStatus() == 4 || $order->getStatus() == 5) : ?>
            <a href="#" onclick="javascript: return deleteOrder(<?php echo $order->getID(); ?>);"
               style="position: absolute; top:3px; right: 0px;padding-left: 2px; padding-right: 2px;background: #F7931E;color:#FFF;font-size: 10px;">X</a>
        <?php endif; ?>
    </div>
    <div class="orderStatus">
        <?php if ($order->getStatus() == 4 || $order->getStatus() == 5) : ?>
        <span style='color: red;'>
        <?php else : ?>
            <span style='color: #f8931d;'>
        <?php endif; ?>
        <?php
        if ($order->getStatus() == 3) {
            echo "Изпълнена | Доставен preorder";
            echo '<a href="#" onmousedown="javascript: returnOrder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 red;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;margin-left: 20px;  ">Връщане</a>';
        } else if ($order->getStatus() == 2) {
            echo "Изпълнена";
            if ($order->isPreorder()) {
                echo '<a href="#" onmousedown="javascript: finishPreorder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 #ff671f;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;margin-top: 5px;">Доставен Preorder</a>';
            }
            echo '<a href="#" onmousedown="javascript: returnOrder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 red;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;margin-left: 20px;  ">Връщане</a>';
        } else if ($order->getStatus() == 1) {
            echo '<a href="#" onmousedown="javascript: doneOrder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 #ff671f;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;margin-right: 20px;">Изпълни</a>';
            echo '<a href="#" onmousedown="javascript: cancelOrder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 red;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;">Откажи</a>';
        } else if ($order->getStatus() == 4) {
            echo "Отказана";
        } else if ($order->getStatus() == 5) {
            echo "Върната";
        } else {
            echo '<a href="#" onmousedown="javascript: approvedOrder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 #ff671f;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;;margin-right: 20px;">Одобри</a>';
            echo '<a href="#" onmousedown="javascript: cancelOrder(' . $order->getID() . ');" style="display:inline-block;background: none repeat scroll 0 0 red;border: none;color: #FFFFFF;font-size: 11pt;font-weight: normal;padding: 6px 10px;width: 180px;text-align:center;">Откажи</a>';
        }

        ?>
		</span>
    </div>
    <div class='orderDates'><span style="color: #F7931E;">Приета</span><br/><?php echo $order->getDate(); ?></div>
    <?php
    $dateAccepted = $order->getDateAccepted();
    if (isset($dateAccepted)) {
        echo "<div class='orderDates'><span style='color: #F7931E;'>Одобрена</span><br />";
        echo $order->getDateAccepted() . "</div>";
    }
    $dateDone = $order->getDateDone();
    if (isset($dateDone)) {
        echo "<div class='orderDates'><span style='color: #F7931E;'>Изпълнена</span><br />";
        echo $order->getDateDone() . "</div>";
    }
    $dateCanceled = $order->getDateCanceled();
    if (isset($dateCanceled)) {
        echo "<div class='orderDates'><span style='color: #F7931E;'>Отказана</span><br />";
        echo $order->getDateCanceled() . "</div>";
    }
    $dateReturned = $order->getDateReturned();
    if (isset($dateReturned)) {
        echo "<div class='orderDates'><span style='color: #F7931E;'>Върната</span><br />";
        echo $order->getDateReturned() . "</div>";
    }
    $stm = $pdo->prepare("SELECT * FROM `members` WHERE `id` = :id LIMIT 1");
    $stm->bindValue(':id', $order->getUserID(), PDO::PARAM_STR);
    $stm->execute();
    $usrr = $stm->fetch();
    $usrrr = $stm->rowCount();

    if ($usrrr > 0) {
        $usr = new user($usrr);
    }

    $contactInformation = $order->getContactInformation();
    $orderProducts = $order->getProducts();
    $deletedItems = $order->getDeletedItems();
    $orderCost = $order->getCost();


    if (isset($usr)) {
        if ($usr->is_partner()) {
            echo "<div class='orderPerson'><span style='color: #b3b3b3;'>Клиент</span> <span style='color: #f8931d;'><a href='" . url . "admin/partners.php?usr=" . $usr->getId() . "' style='color: #f8931d;'>" . $usr->getName() . " " . $usr->getName_last() . "</a></span></div>";
        } else {
            echo "<div class='orderPerson'><span style='color: #b3b3b3;'>Клиент</span> <span style='color: #f8931d;'><a href='" . url . "admin/users.php?usr=" . $usr->getId() . "' style='color: #f8931d;'>" . $usr->getName() . " " . $usr->getName_last() . "</a></span></div>";
        }
    } else {
        echo "<div class='orderPerson'>Изтрит потребител</div>";
    }
    ?>
    <div class="orderPhone"><span style="color: #b3b3b3;">Тел.</span> <?php echo $contactInformation['gsm']; ?></td>
        <div class="orderAddress"><span style="color: #b3b3b3;">Адрес</span>
            <?php
            if ($order->getCountryName() !== false) {
                echo $order->getCountryName() . ", ";
            }
            echo $contactInformation['city'] . ", " . $contactInformation['street'] . " №" . $contactInformation['number'];
            if ($contactInformation['vhod'] != "") echo " вх. " . $contactInformation['vhod'];
            if ($contactInformation['etaj'] != "") echo " ет. " . $contactInformation['etaj'];
            if ($contactInformation['apartament'] != "") echo " ап. " . $contactInformation['apartament'];
            ?>
        </div>
        <?php if (isset($contactInformation['company_name']) && isset($contactInformation['company_city']) && isset($contactInformation['company_street']) && isset($contactInformation['company_number']) &&
            isset($contactInformation['company_mol']) && isset($contactInformation['company_eik']) && isset($contactInformation['company_vat']) && isset($contactInformation['company_phone'])) : ?>
            <br/>
            <div class="userNames"><span
                        style="color: #b3b3b3;">Име на фирма</span> <?php echo $contactInformation['company_name']; ?>
            </div>
            <div class="userEmail"><span
                        style="color: #b3b3b3;">М.О.Л</span> <?php echo $contactInformation['company_mol']; ?></div>
            <div class="userPhones"><span
                        style="color: #b3b3b3;">ЕИК</span> <?php echo $contactInformation['company_eik']; ?></div>
            <div class="userAdress"><span
                        style="color: #b3b3b3;">ЗДДС №:</span> <?php echo $contactInformation['company_vat']; ?></div>
            <div class="userAdress"><span
                        style="color: #b3b3b3;">Адрес</span> <?php echo $contactInformation['company_city']; ?>
                , <?php echo $contactInformation['company_street'] . ' ' . $contactInformation['company_number']; ?>
            </div>
            <div class="userAdress"><span
                        style="color: #b3b3b3;">Телефон</span> <?php echo $contactInformation['company_phone']; ?></div>
            <br/>
        <?php endif; ?>
        <div class="orderPaymentType"><span style="color: #b3b3b3;">Начин на плащане</span>
            <?php
            switch ($order->getPaymentType()) {
                case "cash":
                    echo "Наложен платеж";
                    break;
                case "bank":
                    echo "Паричен превод";
                    break;
                default:
                    echo "Наложен платеж";
                    break;
            }
            ?>
        </div>
        <div class="orderComment"><span style="color: #b3b3b3;">Коментар</span><br> <?php echo $order->getComment(); ?>
        </div>
        <div class="orderAdminComment">
            <span style="color: #b3b3b3;">Коментар от Администратор</span><br/>
            <input type="hidden" id="order_id" value="<?php echo $_GET['ord']; ?>">
            <textarea id="admin_comment"
                      style="width: 400px; height: 150px;font-family: Verdana; font-size: 10pt;margin-top:5px;"><?php echo $order->getAdminComment(); ?></textarea>
        </div>
        <div class="orderProducts">
            <span style="color: #b3b3b3;">Продукти</span> <br>
            <?php

            if (isset($orderProducts)) {
                foreach ($orderProducts as $key => $val) {
                    if ($key == "total_additional_delivery") continue;

                    if (in_array((int)$key, $deletedItems)) {
                        $orderCost = $orderCost - $orderProducts[$key]['price'];
                        continue;
                    }

                    if ($orderProducts[$key]['type'] == "product") {
                        $artikul = new artikul((int)$key);

                        if (!$artikul->isGone()) {
                            $option = '';
                            if (isset($orderProducts[$key]['options'])) {
                                foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                    $expl_arr = explode(",", $optionVal);
                                    foreach ($expl_arr as $keyy => $vall) {
                                        if ($keyy == 0) {
                                            $artikulid = (int)$vall;
                                        }
                                        if ($keyy == 1 && $artikulid == $key) {
                                            $option .= '<tr>';
                                            $option .= '<td style="padding-left:5px;">';
                                            if ($artikul->getTaggroup_dropdown()) {
                                                foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                                    $option .= $gr->getIme() . ": " . $vall;
                                                }
                                            }
                                            $option .= '</td>';
                                            $option .= '</tr>';

                                            $tgstm = $pdo->prepare("SELECT E.`id` FROM `etiketi` E, `etiketi_zapisi` Z WHERE (E.`ime_4c9a99e1a510a` = :ime AND E.`id` = Z.`id_etiket` AND Z.`id_artikul` = :artikul) OR (E.`ime_4c9bca8e36494` = :ime AND E.`id` = Z.`id_etiket` AND Z.`id_artikul` = :artikul) LIMIT 1");
                                            $tgstm->bindValue(':ime', trim($vall), PDO::PARAM_STR);
                                            $tgstm->bindValue(':artikul', $artikul->getId(), PDO::PARAM_INT);
                                            $tgstm->execute();
                                            $et = $tgstm->fetch();

                                            if ($artikul->getWarehouseTagCode((int)$et['id']) != "") {
                                                $option .= '<tr>';
                                                $option .= '<td style="padding-left:5px;">код: ';
                                                $option .= $artikul->getWarehouseTagCode((int)$et['id']);
                                                $option .= '</td>';
                                                $option .= '</tr>';;
                                            } else if ($artikul->getWarehouseCode() != "") {
                                                $option .= '<tr>';
                                                $option .= '<td style="padding-left:5px;">код: ';
                                                $option .= $artikul->getWarehouseCode();
                                                $option .= '</td>';
                                                $option .= '</tr>';;
                                            } else {
                                                $option .= '<tr><td>&nbsp;</td></tr>';
                                            }

                                            unset($a);
                                        }
                                    }
                                }
                            }

                            if ($artikul->getWarehouseCode() != "") {
                                $option .= '<tr>';
                                $option .= '<td valign="top" style="padding-left:5px;">код: ';
                                $option .= $artikul->getWarehouseCode();
                                $option .= '</td>';
                                $option .= '</tr>';
                            }

                            $rowspan = 9;
                            if (isset($orderProducts[$key]['upgrade_option'])) {
                                $rowspan += count($orderProducts[$key]['upgrade_option']);
                                foreach ($orderProducts[$key]['upgrade_option'] as $upgradeOption) {
                                    $option .= '<tr>';
                                    $option .= '<td style="padding-left:5px;">';
                                    $option .= $upgradeOption['upgrade_name'] . ": " . $upgradeOption['name'];
                                    $option .= '</td>';
                                    $option .= '</tr>';
                                }
                            }

                            if ($artikul->isPreorder() && $order->isPreorder()) {
                                $rowspan += 2;
                            }

                            echo "<a href=" . url_admin . "product.php?id=" . $artikul->getId() . " target='_blank' class='orderProduct'><table style='margin-top: 5px; margin-bottom: 5px;font-size: 10pt;'>";
                            echo "<tr>";
                            echo "<td rowspan='" . $rowspan . "' style='background: #FFF; height: 135px;width: 180px;text-align:center;'>";
                            echo "<img style='max-width:180px;max-height:135px;' src='../" . $artikul->getKartinka_t() . "' />";
                            echo "</td>";
                            echo "<td valign='top' style='padding-left:5px;'>" . $artikul->getIme_marka() . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;' class='ime_na_product_poruchka_" . $artikul->getId() . "'>" . $artikul->getIme() . "</td>";
                            echo "</tr>";
                            echo $option;
                            echo "<tr><td>&nbsp;</td></tr>";
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;'>бр." . $orderProducts[$key]['count'] . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Цена:" . $orderProducts[$key]['price'] . " лв.</span></td>";
                            echo "</tr>";
                            if ($artikul->isPreorder() && $order->isPreorder()) {
                                if ($order->getPaypal() == 1) {
                                    echo "<tr>";
                                    echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Аванс:" . $orderProducts[$key]['price'] . " лв.</span></td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Остатък:0 лв.</span></td>";
                                    echo "</tr>";
                                } else {
                                    echo "<tr>";
                                    echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Аванс:" . $orderProducts[$key]['preorder']['deposit_price'] . " лв.</span></td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Остатък:" . ($orderProducts[$key]['price'] - $orderProducts[$key]['preorder']['deposit_price']) . " лв.</span></td>";
                                    echo "</tr>";
                                }
                            }
                            if ($order->getStatus() == 0) {
                                echo "<tr><td valign='top' style='padding-left:5px;'><a href='#' onclick='deleteOrderProduct(\"" . $artikul->getId() . "\")' style='color: red;'>изтрий</a></td></tr>";
                            }
                            echo "</table></a>";
                            $dop_artikuli = $artikul->getAddArtikuli();
                            if ($dop_artikuli != "") {
                                $dop_art_arr = explode(",", $dop_artikuli);
                                foreach ($dop_art_arr as $dart) {
                                    $dr = new artikul((int)$dart);
                                    $option = '';
                                    $n = 0;
                                    if (isset($orderProducts[$key]['options'])) {
                                        foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                            $expl_arr = explode(",", $optionVal);
                                            foreach ($expl_arr as $keyy => $vall) {
                                                if ($keyy == 0 && $vall == $dr->getId()) {
                                                    $artikulid = (int)$vall;
                                                }
                                                if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                                    $option .= '<tr>';
                                                    $option .= '<td style="padding-left:5px;">';
                                                    if ($artikul->getTaggroup_dropdown()) {
                                                        foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                                            $option .= $gr->getIme() . ":";
                                                        }
                                                    }
                                                    $option .= '</td>';
                                                    $option .= '<td>';
                                                    $option .= $vall;
                                                    $option .= '</td>';
                                                    $option .= '</tr>';
                                                    $n = 1;

                                                    $tgstm = $pdo->prepare("SELECT E.id FROM etiketi E, etiketi_zapisi Z WHERE (E.ime_4c9a99e1a510a = :ime AND Z.id_artikul = :artikul) OR (E.ime_4c9bca8e36494 = :ime AND Z.id_artikul = :artikul) LIMIT 1");
                                                    $tgstm->bindValue(':ime', trim($vall), PDO::PARAM_STR);
                                                    $tgstm->bindValue(':artikul', $artikulid, PDO::PARAM_INT);
                                                    $tgstm->execute();
                                                    $et = $tgstm->fetch();

                                                    $stmt = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `tag_id` = :tagid AND `product_id` = :id LIMIT 1");
                                                    $stmt->bindValue(':tagid', (int)$et['id'], PDO::PARAM_INT);
                                                    $stmt->bindValue(':id', $artikulid, PDO::PARAM_INT);
                                                    $stmt->execute();
                                                    if ($stmt->rowCount() > 0) {
                                                        $t = $stmt->fetch();
                                                        $option .= '<tr>';
                                                        $option .= '<td style="padding-left:5px;">код: ';
                                                        $option .= $t['code'];
                                                        $option .= '</td>';
                                                        $option .= '</tr>';;
                                                    } else {
                                                        $stma = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = :id AND `type` = 1 LIMIT 1");
                                                        $stma->bindValue(':id', $artikulid, PDO::PARAM_INT);
                                                        $stma->execute();
                                                        if ($stma->rowCount() > 0) {
                                                            $t = $stma->fetch();
                                                            $option .= '<tr>';
                                                            $option .= '<td>код: ';
                                                            $option .= $t['code'];
                                                            $option .= '</td>';
                                                            $option .= '</tr>';
                                                        } else {
                                                            $option .= '<tr><td>&nbsp;</td></tr>';
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    echo "<a href=" . url_admin . "product.php?id=" . $dr->getId() . " target='_blank' class='orderProduct'><table style='margin-top: 5px; margin-bottom: 5px;font-size: 10pt;'>";
                                    echo "<tr>";
                                    echo "<td rowspan='7' style='color: #CCC; font-size: 18px; paddgin-left: 5px;'>+</td>";


                                    echo "<td rowspan='7' style='background: #FFF; height: 135px;width:180px;text-align:center;'>";
                                    echo "<img style='max-width:90px;max-height:90px;' src='../" . $dr->getKartinka_t() . "' />";
                                    echo "<td valign='top' style='padding-left:5px;'>" . $dr->getIme_marka() . "</td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td valign='top' style='padding-left:5px;'>" . $dr->getIme() . "</td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td valign='top' style='padding-left:5px;'>бр. 1</td>";
                                    echo "</tr>";
                                    echo $option;
                                    echo "<tr><td>&nbsp;</td></tr>";
                                    echo "<tr><td>&nbsp;</td></tr>";
                                    echo "<tr><td>&nbsp;</td></tr>";
                                    echo "<tr><td>&nbsp;</td></tr>";
                                    echo "</table></a>";
                                }
                            }
                        } else {
                            $option = '';
                            if (isset($orderProducts[$key]['options'])) {
                                foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                    $expl_arr = explode(",", $optionVal);
                                    foreach ($expl_arr as $keyy => $vall) {
                                        if ($keyy == 0) {
                                            $artikulid = (int)$vall;
                                        }
                                        if ($keyy == 1 && $artikulid == $key) {
                                            $option .= '<tr>';
                                            $option .= '<td style="padding-left:5px;">размер: ';
                                            $option .= $vall;
                                            $option .= '</td>';
                                            $option .= '</tr>';
                                            $option .= '<tr><td style="padding-left:5px;">Изтрит продукт</td></tr>';
                                            unset($a);
                                        }
                                    }
                                }
                            }
                            echo "<table style='margin-top: 5px; margin-bottom: 5px;font-size: 10pt;'>";
                            echo "<tr>";
                            echo "<td rowspan='7' style='background: #FFF; height: 135px;width: 180px;text-align:center;'>";
                            echo "<img style='max-width:180px;max-height:135px;' src='../images/blank_400X300.png' />";
                            echo "</td>";
                            foreach ($val['info'] as $info) {
                                echo "<td valign='top' style='padding-left:5px;'>" . $info['marka'] . "</td>";
                            }
                            echo "</tr>";
                            echo "<tr>";
                            foreach ($val['info'] as $info) {
                                echo "<td valign='top' style='padding-left:5px;'>" . $info['model'] . "</td>";
                            }
                            echo "</tr>";
                            echo $option;
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;'>бр." . $orderProducts[$key]['count'] . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Цена:" . $orderProducts[$key]['price'] . " лв.</span></td>";
                            echo "</tr>";
                            if ($order->getStatus() == 0) {
                                echo "<tr><td valign='top' style='padding-left:5px;'><a href='#' onclick='deleteOrderProduct(\"" . $key . "\")' style='color: red;'>изтрий</a></td></tr>";
                            }
                            echo "</table>";
                        }
                    } else {
                        $group = new paket((int)$key);

                        echo "<a href=" . url_admin . "group_details.php?id=" . $group->getId() . " target='_blank' class='orderProduct'><table style='margin-top: 5px; margin-bottom: 5px;font-size: 10pt;'>";
                        echo "<tr>";
                        echo "<td rowspan='7' style='background: #FFF; height: 135px;width: 180px;text-align:center;'>";
                        echo "<img style='max-width:180px;max-height:135px;' src='../" . $group->getKartinka_t() . "' />";
                        echo "</td>";
                        echo "<td valign='top' style='padding-left:5px;'>" . $group->getName() . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td valign='top' style='padding-left:5px;'></td>";
                        echo "</tr>";
                        echo "<td></td>";
                        echo "<tr>";
                        echo "<td valign='top' style='padding-left:5px;'>бр." . $orderProducts[$key]['count'] . "</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td valign='top' style='padding-left:5px;'><span style='color: #f8931d;'>Цена:" . $orderProducts[$key]['price'] . " лв.</span></td>";
                        echo "</tr>";
                        if ($order->getStatus() == 0) {
                            echo "<tr><td valign='top' style='padding-left:5px;'><a href='#' onclick='deleteOrderProduct(\"" . $group->getId() . "\")' style='color: red;'>изтрий</a></td></tr>";
                        }
                        echo "</table></a>";
                        $dop_art_arr = $group->getProducts();
                        foreach ($dop_art_arr as $dart) {
                            $dr = new artikul((int)$dart);
                            $option = '';
                            $n = 0;
                            if (isset($orderProducts[$key]['options'])) {
                                foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                    $expl_arr = explode(",", $optionVal);
                                    foreach ($expl_arr as $keyy => $vall) {
                                        if ($keyy == 0 && $vall == $dr->getId()) {
                                            $artikulid = (int)$vall;
                                        }
                                        if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                            $option .= '<tr>';
                                            $option .= '<td>размер: ';
                                            $option .= $vall;
                                            $option .= '</td>';
                                            $option .= '</tr>';
                                            $n = 1;

                                            $tgstm = $pdo->prepare("SELECT E.id FROM etiketi E, etiketi_zapisi Z WHERE (E.ime_4c9a99e1a510a = :ime AND Z.id_artikul = :artikul) OR (E.ime_4c9bca8e36494 = :ime AND Z.id_artikul = :artikul) LIMIT 1");
                                            $tgstm->bindValue(':ime', trim($vall), PDO::PARAM_STR);
                                            $tgstm->bindValue(':artikul', $artikulid, PDO::PARAM_INT);
                                            $tgstm->execute();
                                            $et = $tgstm->fetch();

                                            $stmt = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `tag_id` = :tagid AND `product_id` = :id LIMIT 1");
                                            $stmt->bindValue(':tagid', (int)$et['id'], PDO::PARAM_INT);
                                            $stmt->bindValue(':id', $artikulid, PDO::PARAM_INT);
                                            $stmt->execute();
                                            if ($stmt->rowCount() > 0) {
                                                $t = $stmt->fetch();
                                                $option .= '<tr>';
                                                $option .= '<td style="padding-left:5px;">код: ';
                                                $option .= $t['code'];
                                                $option .= '</td>';
                                                $option .= '</tr>';;
                                            } else {
                                                $stma = $pdo->prepare("SELECT * FROM `warehouse_products` WHERE `product_id` = :id AND `type` = 1 LIMIT 1");
                                                $stma->bindValue(':id', $artikulid, PDO::PARAM_INT);
                                                $stma->execute();
                                                if ($stma->rowCount() > 0) {
                                                    $t = $stma->fetch();
                                                    $option .= '<tr>';
                                                    $option .= '<td style="padding-left:5px;">код: ';
                                                    $option .= $t['code'];
                                                    $option .= '</td>';
                                                    $option .= '</tr>';
                                                } else {
                                                    $option .= '<tr><td>&nbsp;</td></tr>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            echo "<a href=" . url_admin . "product.php?id=" . $dr->getId() . " target='_blank' class='orderProduct'><table style='margin-top: 5px; margin-bottom: 5px;font-size: 10pt;'>";
                            echo "<tr>";
                            echo "<td rowspan='7' style='color: #CCC; font-size: 18px; paddgin-left: 5px;'>+</td>";


                            echo "<td rowspan='7' style='background: #FFF; height: 135px;width:180px;text-align:center;'>";
                            echo "<img style='max-width:90px;max-height:90px;' src='../" . $dr->getKartinka_t() . "' />";
                            echo "<td valign='top' style='padding-left:5px;'>" . $dr->getIme_marka() . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;'>" . $dr->getIme() . "</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td valign='top' style='padding-left:5px;'>бр. " . $orderProducts[$key]['count'] . "</td>";
                            echo "</tr>";
                            echo $option;
                            echo "<tr><td>&nbsp;</td></tr>";
                            echo "<tr><td>&nbsp;</td></tr>";
                            echo "</table></a>";
                        }
                    }
                }
            }
            ?>
        </div>
        <?php if ($order->getStatus() == 1): ?>
            <div class="offeredProducts" style="margin-bottom: 30px;">
                <span style="color: #b3b3b3; display: inline-block; margin-bottom: 5px;">Предложени продукти</span> <br>
                <?php if (!$order->isConfirmationSent()) : ?>
                    <div style="clear: both;"></div>
                    <input class="search"
                           style="display: block;float: left;width: 400px;margin-top: 3px;background-position: 370px center;"
                           type="text" onClick="this.value='';" onkeyup="order_search_suggestion();"
                           name="related_prod_search" value="търсене" autocomplete="off">
                    <div style="clear: both;"></div>
                    <div id="order_search_suggestion"
                         style="display: block; float: left; background-color: #fff; width: 400px; z-index:2; margin-top: 1px;margin-bottom: 10px;border-bottom: 1px solid #333;"></div>
                    <div style="clear: both;"></div>
                <?php endif; ?>
                <input type="hidden" name="offer_products_ids" id="offer_products_ids"
                       value="<?php echo $order->getProductsOffered(); ?>">
                <div id="offer_products">
                    <?php
                    $strProductsOffered = $order->getProductsOffered();
                    if ($strProductsOffered != "") {
                        $ArtikuliArray = explode(",", $strProductsOffered);
                        foreach ($ArtikuliArray as $ar) {
                            $ar = (int)$ar;
                            $adda = new artikul($ar);

                            if (empty($adda->getId())) {
                                echo '<div id="offer_prod_' . $ar . '" style="width: 400px; position: relative; margin-bottom: 20px;padding-left: 10px;box-sizing: border-box;">';
                                if (!$order->isConfirmationSent()) {
                                    echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_offer_product(\'-1\'); return false;">x</a></div>';
                                }
                                echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">';
                                echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:300px;text-align: left;">';
                                echo '<span style="color:#fb921d;" class="ime_na_offer_prod_' . $adda->getId() . '">изтрит продукт</span>';
                                echo '</span></div> </div></div>';
                                echo '<div style="clear: both;"></div>';
                            } else {
                                echo '<div id="offer_prod_' . $adda->getId() . '" style="width: 400px; position: relative; margin-bottom: 20px;padding-left: 10px;box-sizing: border-box;">';
                                if (!$order->isConfirmationSent()) {
                                    echo '<div style="position: absolute; top: 0; right: 0;"><a href="#" style="color: red;" onclick="javascript: delete_offer_product(\'' . $adda->getId() . '\'); return false;">x</a></div>';
                                }
                                echo '<a href="' . url_admin . 'product.php?id=' . $adda->getId() . '">';
                                echo '<div style="height: 60px;">  <div style="width: 60px;height: 60px;width: 70px;float: left;">  <img src="' . url . $adda->getKartinka_t() . '" style="max-width: 60px;max-height:60px; border:0;vertical-align:middle;" />';
                                echo '</div>  <div style="font-size:10pt;float: left;height: 60px; width:300px;text-align: left;">';
                                echo $adda->getIme_marka() . '<br><span class="ime_na_offer_prod_' . $adda->getId() . '">';
                                echo $adda->getIme();
                                echo '</span></div> </div> </a></div>';
                                echo '<div style="clear: both;"></div>';
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="orderDelivery">
            <span style="color: #b3b3b3;">Доставка </span>
            <?php if ($contactInformation['delivery'] === 'pickup'): ?>
                <span style="color: #FB671F;font-size:14px; display: inline-block;text-transform: none;">Вземете на място от нас</span>
            <?php else: ?>
                <span style='color: #f8931d;'><?php echo $contactInformation['delivery']; ?> лв.</span>
            <?php endif; ?>
        </div>
        <div class="orderPrice">
            <?php
            if ($order->isPreorder()) {
                if ($order->getPaypal() == 1) {
                    echo "<span style='color: #b3b3b3;'>Всичко </span><span style='color: #f8931d;'>" . $orderCost . " лв.</span><br/>";
                    echo "<span style='color: #b3b3b3;'>Остатък </span><span style='color: #f8931d;'>0 лв.</span>";
                } else {
                    echo "<span style='color: #b3b3b3;'>Всичко </span><span style='color: #f8931d;'>" . $order->getPreorderCost() . " лв.</span><br/>";
                    echo "<span style='color: #b3b3b3;'>Остатък </span><span style='color: #f8931d;'>" . ($orderCost - $order->getPreorderCost()) . " лв.</span>";
                }
            } else {
                echo "<span style='color: #b3b3b3;'>Всичко </span><span style='color: #f8931d;'>" . $orderCost . " лв.</span>";
            }
            if ($order->getPaypal() == 1) {
                echo "<br />Paypal";
                if ($order->getPaypalPaid() == 1) {
                    echo ": платено";
                }
            }
            ?>
        </div>
        <div id="jsResponse"></div>
        <button id="save" class="button-save">save</button>
        <?php if ($order->getStatus() == 1): ?>
            <button <?php echo !$order->isConfirmationSent() ? 'id="saveAndSend"' : ''; ?>
                    style="width: 150px;" <?php echo $order->isConfirmationSent() ? 'disabled="disabled"' : ''; ?>
                    class="button-save">save & send e-mail
            </button>
        <?php endif; ?>
        <button id="cancel" class="button-cancel">cancel</button>
    </div>

    <div class="modal">

        <div class="modal-content">
            <div class="modal-top-lenta">
                <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                <div class="modal-top-close" onclick="hideModal();">×</div>
            </div>

            <div class="modal-body">

            </div>
        </div>
    </div>
