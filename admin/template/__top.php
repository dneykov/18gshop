<html>
    <head>
        <title>18gshop admin</title>
        <meta charset="UTF-8">
        <link rel="icon" href="<?php echo url; ?>/admin/images/favicon_a.ico" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo url; ?>/admin/images/favicon_a.ico" type="image/x-icon">
        <script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
        <script type="text/javascript" src="<?php echo url; ?>js/admin_functions.js?id=<?php echo uniqid(); ?>"></script>
        <script type="text/javascript" src="<?php echo url; ?>js/admin_browse_edit.js?id=<?php echo uniqid(); ?>"></script>
        <script type="text/javascript" src="<?php echo url; ?>js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo url; ?>js/thickbox.js"></script>
        <link type="text/css" href="<?php echo url; ?>css/thickbox.css" rel="stylesheet" />
        <link type="text/css" href="<?php echo url; ?>css/jquery-ui-1.8.5.custom.css" rel="stylesheet" />

        <script type="text/javascript" src="<?php echo url; ?>admin/js/fancyBox/jquery.fancybox.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo url; ?>admin/js/fancyBox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo url; ?>admin/js/fancyBox/helpers/jquery.fancybox-buttons.css?v=2.0.3" />
        <script type="text/javascript" src="<?php echo url; ?>admin/js/fancyBox/helpers/jquery.fancybox-buttons.js?v=2.0.3"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo url; ?>admin/js/fancyBox/helpers/jquery.fancybox-thumbs.css?v=2.0.3" />
        <script type="text/javascript" src="<?php echo url; ?>admin/js/fancyBox/helpers/jquery.fancybox-thumbs.js?v=2.0.3"></script>
        <script src="//malsup.github.io/jquery.form.js"></script>
        <script>
            var base_url = "<?php echo url; ?>";
        </script>

        <style type="text/css">
            body{
                background-color: #fff;
                color: #000;
                font-family: Verdana, sans-serif;
                font-size: 12pt;
                min-width: 1000px;
                padding: 0;
                margin: 0;
                overflow-x: hidden;
            }

            a {
                color: #333;
                text-decoration: none;
            }

            input[type="text"]{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }

            .nov{
                color: #fff;
                background-color: #666666;
                width: 100px;
                height: 25px;
                display: block;
                text-align: center;
                float: left;
                font-size: 13px;
                line-height: 22px;
            }

            .nov:hover{
                background-color: #ff9207;
            }

            .product-edit-image-btn{
                color: #fff;
                background-color: #666666;
                width: 100px;
                height: 25px;
                display: block;
                text-align: center;
                float: left;
                font-size: 13px;
                cursor: pointer;
                line-height: 22px;
            }

            .product-edit-image-btn:hover{
                background-color: #ff9207;
            }

            .nov-product{
                color: #fff;
                background-color: #666666;
                width: auto;
                padding-left: 10px;
                padding-right: 10px;
                height: 25px;
                display: block;
                text-align: center;
                float: left;
                font-size: 13px;
                line-height: 22px;
            }

            .nov-product:hover{
                background-color: #ff9207;
            }

            .button-s{
                display: block;
                height: 25px;
                width: 25px;
                text-align: center;
                float: left;
                background-color: #85c63d;
                color: #fff;
                margin-left: 10px;
                line-height: 22px;
            }

            .button-s:hover{
                background-color: #ff9207;
            }

            .menu_zaglavie{
                color: #333333;
                width: 100%;
                display: block;
                font-size: 13px;
                line-height: 25px;
                box-sizing: border-box;
                padding-left: 10px;
                height: 25px;
                background: #f2f2f2;
                font-style: normal !important;
            }

            .menu_zaglavie2 .tagSpec {
                
                font-size: 11px;
            }

            .menu_zaglavie:hover{
                background-color: #ff9207;
                color: #fff;
            }

            .menu_zaglavie2{
                color: #333333;
                width: 100%;
                display: block;
                font-size: 13px;
                line-height: 25px;
                box-sizing: border-box;
                padding-left: 10px;
                height: 25px;
                background: #f2f2f2;
                font-style: normal !important;
            }

            .podvid:hover{
                color: #ff9207 !important;
            }

            .admin_glavna_tablica{
                margin-top: -15px;
                border: 0px;
                border-color: white;
                border-style: dashed;

            }

            .admin_glavna_tablica td{
                border-collapse: collapse;
                vertical-align: top;
                min-width: 150px;
                max-width: 205px;
                word-wrap: break-word;
            }
            .admin_glavna_tablica td a.offline{
                opacity: 0.5;
            }
            .admin_glavna_tablica td a.offline div .product_details_right_thumb{
                opacity: 0.15;
            }
            .new_tag{

            }

            .product_details_left{
                line-height: 12px;
                font-size: 14px;
                color: #B3B3B3;
            }
            .product_details_right{
                color: #333;
                font-size: 13px;
                line-height: 18px;
            }





            .product_details_right_thumb {
                display: table-cell;
                text-align: center;
                vertical-align: middle;
                width: 200px;
                height: 150px;
                background-color: #FFF;
            }
            .product_details_right_thumb * {
                vertical-align: middle;
            }
            .product_details_right_thumb img {
                max-width: 180px;
            }


            .menu_top{
                color: #FFF;
                background-color: #666666;
                width: 100%;
                height: 25px;
                z-index: 99999;
            }

            .menu_top_inside{
                width: 1280px;
            }

            .menu_top a{
                color: #FFF;
                font-size: 13px;
                font-weight: bold;
                line-height: 23px;
            }

            .menu_top a:hover {
                color: rgb(248, 147, 29);
                text-decoration: none;
            }
            .menu_top_current{

                background-color: #808080;
            }


            .menu_top_menu_link{
                float: left;
                height: 25px;
                padding: 0 15px;
                text-align: center;
            }


            .product_edit_clickable :hover{

            }


            .product_edit_table td{
                vertical-align: top;
                padding: 0;
                padding-left: 0;
            }



            .product_taggrupa_select{


            color: fuchsia;

            }


            .admin_users_table{
                border-collapse: collapse;
            }

            .admin_users_table td{
                vertical-align: top;
                padding: 10px;
                border-width: 1px 1px 1px 1px;
                border-color: white;
                border-style: solid;

            }


            .disabled_by_permission{
                color: #111;
            }
            .topControlPanel{
                background-color: #fff;
                color:#666;

            }
            .topControlPanel a{
                font-size:13px;
                color:#666;
            }
            .newform{
                display:none;
            }
            .addbutton{
                margin-left:10px;
                margin-top: 13px;
                color: #fff;
            }
            .search {
                height: 30px;
                border: 1px solid #999;
                margin-bottom: 5px;
                padding-left: 5px;
                background-image: url(/admin/images/icons/lens.png);
                background-position: 350px center;
                background-repeat: no-repeat;
                padding-right: 30px;
            }
            .search:focus {
                outline: none;
            }
            textarea{
                resize: none;
            }
            #top_menu_search_suggestion {
                display: block;
                position: absolute;
                background-color: #FFFFFF;
                width: 850px;
                right:5px;
                z-index:2;
                top:40px;
                box-shadow: 4px 6px 6px #333;
            }

            #virtual_products_suggestion{
                display: block;
                position: absolute;
                background-color: #FFFFFF;
                width: 400px;
                z-index:2;
                box-shadow: 2px 3px 3px #ccc    ;
            }

            .suggestion-virtual-item {
                width: calc(100% - 10px);
                min-height: 60px;
                padding-bottom: 10px;
                padding-top: 10px;
                padding-left: 10px;
                cursor: pointer;
            }

            .suggestion-virtual-item:hover {
                background: #f2f2f2;
            }

            input[type=submit]{
                margin-top: 0px;
            }

            .p_preorder{
               font-size: 13px;
               padding-bottom: 10px;
               color: #b3b3b3;
               text-align: right;
               float: right;
            }

            .p_preorder a {
                display: inline-block;
                color: white;
                font-size: 13px;
                height: 25px;
                line-height: 23px;
                width: 78px;
                text-align: center;
                background-color: #666;
                float: right;
            }

            .p_preorder .active{
                background-color: #F7931E;
            }
            .error {
                font-size: 12px;
                color: #ff0000;
                padding: 4px 0;
                display: none;
            }
            .top-25 {
                margin-top: 26px!important;
            }
            .button-save{
                width: 100px;
                height: 25px;
                background-color: #666666;
                padding-bottom: 3px;
                color: #fff;
                border: none;
                cursor: pointer;
            }
            .button-save:hover{
                background-color: #ff9207;
            }
            .button-cancel{
                width: 100px;
                height: 25px;
                background-color: #666666;
                padding-bottom: 3px;
                color: #fff;
                border: none;
                cursor: pointer;
            }
            .button-cancel:hover{
                background-color: #ff9207;
            }
            .new_category_names{
                border: 1px solid #666666;
                width: 240px;
                height: 26px;
                color: #333;
            }
            .plus{
                color: #333;
                font-weight: bold;
            }
            .plus:hover{
                color: #ff9207;
            }
            .vidove a{
                color: #333;
                font-size: 13px;
                padding-left: 10px;
                margin-top: 5px;
                display: inline-block;
            }
            .vidove a:hover{
                color: #ff9207;
            }
            .marki a{
                color: #333;
                font-size: 13px;
                padding-left: 10px;
                display: inline-block;
                float: left;
                margin-top: 5px;
            }
            .marki a:hover{
                color: #ff9207;
            }
            .input-tab-holder{
                width: 240px;
            }
            .input-tab-title{
                font-size: 13px;
                color: #333;
                float: left;
                display: block;
            }
            .input-tab-trigger-bg{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
            }
            .input-tab-en{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
            }
            .input-tab-ro {
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
            }
            .input-tab-trigger-bg-vid{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-vid:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-vid{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-vid:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-vid{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-vid:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-vid{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-vid{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-vid{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-tag{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-tag{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-tag{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-edit-tag{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-edit-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-edit-tag{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-edit-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-edit-tag{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-edit-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-edit-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-edit-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-edit-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-edit-taggroup{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-edit-taggroup:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-edit-taggroup{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-edit-taggroup:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-edit-taggroup{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-edit-taggroup:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-edit-taggroup{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-edit-taggroup{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-edit-taggroup{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-edit-mode{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-edit-mode:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-edit-mode{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-edit-mode:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-edit-mode{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-edit-mode:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-edit-mode{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-edit-mode{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-edit-mode{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-edit-upgrade-group{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-edit-upgrade-group:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-edit-upgrade-group{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-edit-upgrade-group:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-edit-upgrade-group{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-edit-upgrade-group:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-edit-upgrade-group{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-edit-upgrade-group{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-edit-upgrade-group{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-map-ime{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-map-ime:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-map-ime{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-map-ime:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-map-ime{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-map-ime:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-map-ime{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-map-ime{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-map-ime{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-product-edit{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-product-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-product-edit{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-product-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-product-edit{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-product-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-product-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-product-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-product-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-product-pre-order{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-product-pre-order:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-product-pre-order{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-product-pre-order:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-product-pre-order{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-product-pre-order:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-product-pre-order{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
            }
            .input-tab-en-product-pre-order{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
            }
            .input-tab-ro-product-pre-order{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
            }

            .input-tab-trigger-bg-kak-da-porucham{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-kak-da-porucham:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-kak-da-porucham{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-kak-da-porucham:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-kak-da-porucham{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-kak-da-porucham:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-kak-da-porucham{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
            }
            .input-tab-en-kak-da-porucham{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
            }
            .input-tab-ro-kak-da-porucham{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
            }

            .input-tab-trigger-bg-kak-da-porucham-info{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-kak-da-porucham-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-kak-da-porucham-info{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-kak-da-porucham-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-kak-da-porucham-info{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-kak-da-porucham-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-kak-da-porucham-info{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
            }
            .input-tab-en-kak-da-porucham-info{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
            }
            .input-tab-ro-kak-da-porucham-info{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
            }

            .input-tab-trigger-bg-product-info-edit{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-product-info-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-product-info-edit{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-product-info-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-product-info-edit{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-product-info-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-product-info-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-product-info-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-product-info-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-product-info2-edit{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-product-info2-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-product-info2-edit{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-product-info2-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-product-info2-edit{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-product-info2-edit:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-product-info2-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-product-info2-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-product-info2-edit{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-map-adres{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-map-adres:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-map-adres{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-map-adres:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-map-adres{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-map-adres:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-map-adres{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-map-adres{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-map-adres{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-map-info{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-map-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-map-info{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-map-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-map-info{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-map-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-en-map-info{
                display: none;
            }
            .input-tab-ro-map-info{
                display: none;
            }

            .input-tab-trigger-bg-etiket{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-etiket:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-etiket{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-etiket:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-etiket{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-etiket:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-etiket{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-etiket{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-etiket{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .categories{
                margin-top: 25px;
                margin-left: 5px;
            }
            .categories a{
                font-size: 13px;
                font-family: 'Verdana', sans-serif;
                margin-left: 15px;
            }
            .categories2{
                margin-left: 5px;
            }
            .categories2 a{
                font-size: 13px;
                font-family: 'Verdana', sans-serif;
                margin-left: 15px;
            }
            .active{
                color: #ff9207;
            }
            .product-top-lenta{
                height: 25px;
                color: #333;
                font-size: 13px;
                font-family: 'Verdana', sans-serif;
                background: #f2f2f2;
                padding-right: 10px;
                line-height: 25px;
            }
            .modal{
                display: none;
                position: fixed;
                z-index: 100000;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background: rgba(0, 0, 0, 0.25);
            }
            .modal-content{
                background-color: #f2f2f2;
                margin: 100px auto;
                padding-top: 0px;
                box-sizing: border-box;
                border-radius: 4px;
                width: 500px;
                height: 190px;
                overflow: auto;
                border-radius: 0px;
            }
            .modal-top-lenta{
                height: 40px;
                width: 100%;
                background-color: #f8931f;
                padding: 10px;
                box-sizing: border-box;
            }
            .modal-top-lenta-text{
                font-size: 13px;
                font-family: 'Verdana', sans-serif;
                color: #fff;
                display: inline-block;
                float: left;
            }
            .modal-top-close{
                font-size: 25px;
                font-family: 'Verdana', sans-serif;
                color: #fff;
                display: inline-block;
                float: right;
                cursor: pointer;
                margin-top: -9px;
            }
            .modal-body{
                width: 100%;
                margin-top: 5px;
                height: 135px;
                font-size: 13px;
                padding-left: 12px;
                padding-right: 12px;
                padding-top: 10px;
                box-sizing: border-box;
                overflow: hidden;
            }
            .modal-img{
                width: 180px;
                height: 135px;
                float: left;
            }
            .proddetails-left-col{
                width: 830px;
                height: auto;
                overflow: hidden;
                float: left;
                margin-left: 10px;
            }
            .proddetails-right-col{
                width: 380px;
                height: auto;
                overflow: hidden;
                float: left;
                margin-left: 30px;
            }
            .proddetails-right-col-top-lenta{
                width: 100%;
                background-color: #f2f2f2;
                height: 20px;
            }

            .redaktirai-image-1{
                display: none;
            }
            .redaktirai-image-2{
                display: none;
            }
            .redaktirai-image-3{
                display: none;
            }
            .redaktirai-image-4{
                display: none;
            }
            .redaktirai-image-5{
                display: none;
            }
            .redaktirai-image-6{
                display: none;
            }
            .redaktirai-image-7{
                display: none;
            }
            .redaktirai-image-8{
                display: none;
            }
             .redaktirai-image-9{
                display: none;
            }

            .redaktirai-image-e-0{
                display: none;
            }
            .redaktirai-image-e-1{
                display: none;
            }
            .redaktirai-image-e-2{
                display: none;
            }
            .redaktirai-image-e-3{
                display: none;
            }
            .redaktirai-image-e-4{
                display: none;
            }
            .redaktirai-image-e-5{
                display: none;
            }
            .redaktirai-image-e-6{
                display: none;
            }
            .redaktirai-image-e-7{
                display: none;
            }
            .redaktirai-image-e-8{
                display: none;
            }

            #remove_collection{
                color: #FF0000 !important;
                font-size: 14px !important;
                top: 23px !important;
                font-family: 'Verdana',sans-serif !important;
                right: 7px !important;
            }

            .add-to-collection:hover{
                background-color: #F7931E;
                color: white;
            }

            .add-to-collection2:hover{
                color: #F7931E;
            }

            .filters a:hover, .availability a:hover {
                color: #F7931E;
            }

            .filters a, .availability a {
                padding: 0 5px;
                height: 25px;
                display: inline-block;
            }

            .filters a.current, .availability a.current {
                background: #F7931E;
                color: #fff !important;
            }

            @media screen and (max-width: 1150px) {
                body{
                    overflow-x: auto;
                }

                .menu-top{
                    width: 1280px;
                }
            }
</style>
    <script type="text/javascript">
        function partnership_Ajax(userID,status){
            $.ajax({
                type: "GET",
                url: "ajax/update_partner.php",
                data: "id="+userID+"&partner="+status,
                cache: false,
                success: function(data){
                    if(data == '1') {
                        window.location.href = base_url + 'admin/partners.php?usr='+userID;
                    } else {
                         window.location.href = base_url + 'admin/users.php?usr='+userID;
                    }
                }
            });
        }
        $(document).ready(function() {
            $('.partnership a').click(function (e) {
                e.preventDefault();
                var status = jQuery(this).attr('data-value');
                var userID = jQuery('#user_id').val();
                if(status == '1') {
                    var msg = "Добавяне на "+ jQuery('.userNames span:eq(1)').text()  + " към партньори.";
                } else if(status == '0') {
                    var msg = "Премахване на "+ jQuery('.userNames span:eq(1)').text()  + " от партньори.";
                }
                $(".modal-top-lenta-text").text("Are you sure?");
                var status = jQuery(this).attr('data-value');
                var userID = jQuery('#user_id').val();
                $(".modal-body").html('<div style="width: 100%; height: 20px;">' + msg + '</div> <button class="button-cancel" onclick="hideModal();" style="position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="partnership_Ajax(\'' + userID + '\',\'' + status + '\');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
                $(".modal").show();
            });

            $('.addbutton').click(function() {
              $('.newform').slideToggle('slow', function() {

              });
              return false;
            });

            $('li input[name="product_tags[]"]').change(function() {
                var atLeastOneIsChecked = $('li input[name="product_tags[]"]:checked').length > 0;

                if(atLeastOneIsChecked) {
                    $("#preorder_row").show();
                } else {
                    $("#preorder_row").hide();
                }
            });

            $('#sort_vidove').fancybox({
                padding: 0,
                width: 0,
                height: 0,
                type: "iframe",
                fitToView: false, // we need this
                autoSize: false, // and this
                maxWidth: "95%", // and this too
                afterShow: function () {
                    // find the inner height of the iframe contents
                    this.height = $(".fancybox-iframe").contents().find("#main").innerHeight() + 45;
                }
            });

            $('#sort_category').fancybox({
                padding: 0,
                width: 0,
                height: 0,
                type: "iframe",
                fitToView: false, // we need this
                autoSize: false, // and this
                maxWidth: "95%", // and this too
                afterShow: function () {
                    // find the inner height of the iframe contents
                    this.height = $(".fancybox-iframe").contents().find("#main").innerHeight() + 45;
                }
            });

            $('.sort_etiketi').fancybox({
                padding: 0,
                width: 0,
                height: 0,
                type: "iframe",
                fitToView: false, // we need this
                autoSize: false, // and this
                maxWidth: "95%", // and this too
                afterShow: function () {
                    // find the inner height of the iframe contents
                    this.height = $(".fancybox-iframe").contents().find("#main").innerHeight() + 45;
                }
            });
        });

    </script>
    <script type="text/javascript">
    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("search"))
            search_ajax_close('#top_menu_search_suggestion');
    });
    </script>

    </head>
    <body>

        <div id="zarejdane" style="background-color: transparent;display: none;width: 100%; height: 100%;position: fixed;"><div style="width: 150px; float: right; background-color: #FFF; color: #000">зареждам...</div></div>


        <div class="topControlPanel">
                <div style="float:right;margin-right:10px;font-size:13px;line-height: 35px;">
                    <?php
                        if($__user->logged_in() ) {
                            echo '<b>'.$__user->get_class_admin()->getUsername().'</b>';
                            ?>
                                | <!--<a href="<?php //echo url_admin; ?>statistics.php">статистика</a> |--> <a href="<?php echo url_admin; ?>settings.php">настройки</a> |
                            <a href="<?php echo url_admin; ?>login.php?logout=1"><?php echo lang_login_logout; ?></a>
                            <?php
                        }
                    ?>
                </div>
                <div style="float:right;margin-right:30px;margin-top:5px;">
                    <input class="search" style="width: 300px;background-position: 275px center;" type="text" onkeyup="search_suggestion(this.value);" name="top_menu_search">
                    <div id="top_menu_search_suggestion"></div>
                </div>
                <div style="margin-left:10px;color:#666;line-height: 35px;"><span style="font-weight: bold; color: #ff9207;"><a style="font-size: 12pt; font-weight: bold; color: #ff9207;" href="<?php echo url_admin; ?>">Istore</a></span> 18gshop</span></div>
                <div style="clear:both;"></div>
        </div>

<div class="menu_top">

    <div class="menu_top_inside">
    <?php $path1 = $_SERVER['SCRIPT_NAME']; $file1 = basename($path1, ".php"); if( $__user->permission_check('продукти','r',true,true) ) { ?>
    <div class="menu_top_menu_link <?php if($_SERVER['SCRIPT_NAME'] == '/admin/browse.php' || $_SERVER['SCRIPT_NAME'] == '/admin/product.php' || $file1 == 'product' || $file1 == 'new_product' || $file1 == 'browse') echo "menu_top_current"; ?>">
        <a href="<?php echo url.'admin/index.php'; ?>">продукти <?php echo $__products_count; ?></a>
    </div>
    <div class="menu_top_menu_link"><a href="<?php echo url.'admin/moduls/collections.php'; ?>">колекции</a></div>

    <?php } ?>

    <div class="menu_top_menu_link <?php if($_SERVER['SCRIPT_NAME'] == '/admin/groups.php' || $_SERVER['SCRIPT_NAME'] == '/admin/group_details.php' || $_SERVER['SCRIPT_NAME'] == '/admin/new_group.php' || $_SERVER['SCRIPT_NAME'] == '/admin/group_details.php') echo "menu_top_current"; ?>"><a href="<?php echo url; ?>admin/moduls/promo.php">промоции</a></div>

    <?php if( $__user->permission_check('банери','r',true,true) ) { ?>
    <div class="menu_top_menu_link <?php if($_SERVER['SCRIPT_NAME'] == '/admin/moduls/header_links.php' || $_SERVER['SCRIPT_NAME'] == '/admin/moduls/intro.php' || $_SERVER['SCRIPT_NAME'] == '/admin/moduls/header_links.php') echo "menu_top_current"; ?>"><a href="<?php echo url; ?>admin/moduls/adv.php">банери</a></div>
    <?php } ?>

    <?php if( $__user->permission_check('поръчки','r',true,true) ) { ?>
    <div class="menu_top_menu_link"><a href="<?php echo url; ?>admin/orders.php">поръчки <?php echo order_admin::getCountForAll(); ?></a></div>
    <?php } ?>

    <?php if($__user->permission_check('потребители','r',true, true) || $__user->permission_check('администратори','r',true, true) ) { ?>

    <div class="menu_top_menu_link <?php if($_SERVER['SCRIPT_NAME'] == '/admin/users.php' || $_SERVER['SCRIPT_NAME'] == '/admin/users_admin.php' || $_SERVER['SCRIPT_NAME'] == '/admin/partners.php' || $_SERVER['SCRIPT_NAME'] == '/admin/newsletter.php') echo "menu_top_current"; ?>"><a
             href="<?php echo url_admin; ?><?php
             if($__user->permission_check('потребители','r',true, true)) { ?>users.php<?php }
             else { ?>users_admin.php<?php } ?>">потребители <?php echo $__user_count; ?> </a></div>
    <?php } ?>

    <?php if( $__user->permission_check('контакти','r',true,true) ) { ?>
     <div class="menu_top_menu_link"><a href="<?php echo url; ?>admin/moduls/map.php">контакти</a></div>
    <?php } ?>

    <?php if( $__user->permission_check('новини','r',true,true) ) { ?>
     <div class="menu_top_menu_link"><a href="<?php echo url; ?>admin/moduls/news.php">новини</a></div>
        <div class="menu_top_menu_link <?php if($_SERVER['SCRIPT_NAME'] == '/admin/moduls/create-newsletter.php' || $_SERVER['SCRIPT_NAME'] == '/admin/moduls/newsletter-history.php' || $_SERVER['SCRIPT_NAME'] == '/admin/moduls/newsletter.php') echo "menu_top_current"; ?>"><a href="<?php echo url; ?>admin/moduls/create-newsletter.php">бюлетин</a></div>
    <?php } ?>

    <div class="menu_top_menu_link" style="width: 50px;"><a href="<?php echo url; ?>admin/warehouse.php">склад</a></div>
    <div class="menu_top_menu_link <?php if($_SERVER['SCRIPT_NAME'] == '/admin/moduls/about.php') echo "menu_top_current"; ?>" style="width: 100px;">
        <a href="<?php echo url; ?>admin/help.php">информация</a>
    </div>
    </div>
</div>
<div class="pod-menu" style="height: 13px; background-color: #f2f2f2; font-size: 13px; padding: 6px; padding-bottom: 7px; padding-top: 3px; box-shadow: 1px 0px 1px #ababab; border-bottom: 1px solid #767676;">
    <div class="categories2">
        <?php if (isset($_GET['category']) && basename(parse_url($_SERVER["SCRIPT_NAME"], PHP_URL_PATH)) == "browse.php") : ?>
            <?php foreach ($kategorii as $cat) : ?>
                <?php if (isset($_GET['filter']) && isset($_GET['availability'])) : ?>
                        <a class="<?php echo $_GET['category'] == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>&filter=<?php echo $_GET['filter']; ?>&availability=<?php echo $_GET['availability']; ?>"><?php echo $cat->getIme(); ?></a>
                    <?php elseif(isset($_GET['filter'])) : ?>
                        <a class="<?php echo $_GET['category'] == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>&filter=<?php echo $_GET['filter']; ?>"><?php echo $cat->getIme(); ?></a>
                    <?php elseif(isset($_GET['availability'])) : ?>
                        <a class="<?php echo $_GET['category'] == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>&availability=<?php echo $_GET['availability']; ?>"><?php echo $cat->getIme(); ?></a>
                    <?php else : ?>
                        <a class="<?php echo $_GET['category'] == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>"><?php echo $cat->getIme(); ?></a>
                    <?php endif; ?>
                <?php endforeach; ?>
        <?php elseif (isset($artikul) && !is_null($artikul)) : ?>
            <?php foreach($kategorii as $cat) : ?>
                <?php if (isset($_GET['filter']) && isset($_GET['availability'])) : ?>
                    <a class="<?php echo $artikul->getId_kategoriq() == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>&filter=<?php echo $_GET['filter']; ?>&availability=<?php echo $_GET['availability']; ?>"><?php echo $cat->getIme(); ?></a>
                <?php elseif(isset($_GET['filter'])) : ?>
                    <a class="<?php echo $artikul->getId_kategoriq() == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>&filter=<?php echo $_GET['filter']; ?>"><?php echo $cat->getIme(); ?></a>
                <?php elseif(isset($_GET['availability'])) : ?>
                    <a class="<?php echo $artikul->getId_kategoriq() == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>&availability=<?php echo $_GET['availability']; ?>"><?php echo $cat->getIme(); ?></a>
                <?php else : ?>
                    <a class="<?php echo $artikul->getId_kategoriq() == $cat->getId() ? 'active' : '' ?>" href="browse.php?category=<?php echo $cat->getId(); ?>"><?php echo $cat->getIme(); ?></a>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<script>
    $( document ).ready(function() {
        $(document).unbind("contextmenu");
    });
</script>
