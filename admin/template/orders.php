<?php

$page_current = 'orders';

require('template/__top.php');
/* @var $artikul artikul_admin */
?>
<style type="text/css">
	.orders td{
		font-size: 13px;
		white-space: nowrap;
		overflow: hidden;
		o-text-overflow: ellipsis;
		text-overflow: ellipsis;
		min-height: 20px;
		width: 85px;
		display: inline-block;
	    white-space: nowrap;
	}
	.orders {
		margin-top: 15px;
		margin-left: 10px;
		width: 1100px;
		border: none;
		margin-bottom: 50px;
	}
	.orders tr td a{
		width: 100%;
		display: inline-block;
	}
	.orders tr {
		height: 20px;
	}
	.orders a {
		color: #333;
	}
	.orders tr:nth-child(even){
		background-color: #fff;
		color: #333;
	}
	.orders tr:nth-child(odd){
		background-color: #f2f2f2;
		color: #333;
	}
	.orders tr:hover{
		background-color: #F7931E !important;
	}
	.orders tr:hover td > a{
		color: #FFF !important;
	}
	.deleteorderx{
		visibility: hidden !important;
	}
	.orders tr:hover td > .deleteorderx{
		color: #FFF !important;
		visibility: visible !important;
	}
	#tablehead:hover {
		background-color: #999 !important;
	}
	.subMenu{
		width: 100%;
	    height: 13px;
	    margin-top: -11px;
	    position: relative;
	    top: -13px;
	    padding-bottom: 10px;
	    padding-top: 0px;
	    box-shadow: 1px 0px 1px #ababab;
	    border-bottom: 1px solid #767676;
	    background-color: #f2f2f2;
	}
	.subMenu a{
		color:#F7931E;
		font-size: 13px;
		margin:0 15px;
	}
	.subMenu a:hover{
		color:#F7931E !important;
	}
	.productLink:hover{
		text-decoration:none;
	}
	.orders{
		border-collapse: collapse;
	}
	tr.preorder {
		background-color: #F7931E !important;
	}
	.orders tr.preorder td > a {
		color: #FFF !important;
	}
	.orders tr.preorder-completed td > a, a.active {
		background-color: #F7931E !important;
	}
	div > a {
		color: #333;
	}
</style>

<div class="subMenu">

    <a href="<?php echo url_admin; ?>orders.php" <?php if((isset($_GET['approved']))||(isset($_GET['done']))||(isset($_GET['cancel']))||(isset($_GET['return']))) echo 'style="color: #333;"' ?>>Нови <?php echo order_admin::getCountForWaiting(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?approved" <?php if(!isset($_GET['approved'])) echo 'style="color: #333;"' ?>>Одобрени <?php echo order_admin::getCountForApproved(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?done" <?php if(!isset($_GET['done'])) echo 'style="color: #333;"' ?>>Изпълнени <?php echo order_admin::getCountForDone(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?cancel" <?php if(!isset($_GET['cancel'])) echo 'style="color: #333;"' ?>>Отказани <?php echo order_admin::getCountForCanceled(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?return" <?php if(!isset($_GET['return'])) echo 'style="color: #333;"' ?>>Върнати <?php echo order_admin::getCountForReturned(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?statistik" style="color: #333;">Отчет</a>
    <a href="<?php echo url_admin; ?>orders.php?delivery" style="color: #333;">Доставка</a>
    <a href="<?php echo url_admin; ?>orders.php?valute" style="color: #333;">Настройки</a>
</div>
<?php if(isset($_GET['done'])) : ?>
	<div style="font-size: 13px; padding: 	0px 10px 0 10px; color: #b3b3b3;"><a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?done" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a> <a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?done&preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a></div>
<?php elseif(isset($_GET['approved'])) : ?>
	<div style="font-size: 13px; padding: 0px 10px 0 10px; color: #b3b3b3;"><a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?approved" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a> <a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?approved&preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a></div>
<?php elseif(isset($_GET['cancel'])) : ?>
    <div style="font-size: 13px; padding: 0px 10px 0 10px; color: #b3b3b3;"><a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?cancel" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a> <a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?cancel&preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a></div>
<?php elseif(isset($_GET['return'])) : ?>
    <div style="font-size: 13px; padding: 0px 10px 0 10px; color: #b3b3b3;"><a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?return" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a> <a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?return&preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a></div>
<?php else : ?>
	<div style="font-size: 13px; padding: 0px 10px 0 10px; color: #b3b3b3;"><a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a> <a style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;" href="<?php echo url_admin; ?>orders.php?preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a></div>
<?php endif; ?>
<div style="clear: both;"></div>
<table class="orders" cellpadding="3">
	<tr id="tablehead" style="background-color: #666666; color: #fff;">
		<td style="padding-left:5px; width: 90px; padding-right: 10px;">Номер</td><!-- 
		--><td style="padding-left:5px; width: 185px; padding-right: 10px;">Дата</td><!--
		--><td style="padding-left:5px; width: 145px; padding-right: 5px;">Потребител</td><!--
		--><td style="width: 100px; padding-left: 5px; padding-right: 5px;">Телефон</td><!--
		--><td style="width: 160px; padding-left: 5px; padding-right: 5px;">Адрес</td><!-- 
		--><td style="width: 165px; padding-left: 5px; padding-right: 5px;">Коментар</td><!-- 
		--><td style="padding-left: 5px; width: 85px;">Продукти</td><!-- 
		--><td style="padding-left: 5px; padding-right: 10px; width: 65px;">Общо</td><!-- 
		--><td style="width: 15px;"></td><!--
		-->
	</tr>
    <?php		
		if (isset($orders)){
			foreach($orders as $order){

				$stm = $pdo->prepare("SELECT * FROM `members` WHERE `id` = :id LIMIT 1");
				$stm->bindValue(':id', $order->getUserID(), PDO::PARAM_STR);        
				$stm->execute();
				$usrr = $stm->fetch();
	            $usrrr = $stm->rowCount();
	            if($usrrr > 0){
					$usr = new user($usrr);
					$contactInformation=$order->getContactInformation();
					$orderProducts=$order->getProducts();
					$deletedItems=$order->getDeletedItems();
					$orderCost = $order->getCost();
					?>
					<tr class='<?php echo $order->isPreorder() && $order->getStatus() != 3 ? "preorder" : ""; ?> <?php echo $order->isPreorder() && $order->getStatus() == 3 ? "preorder-completed" : ""; ?>'>
						<td style="padding-left:5px; width: 90px; padding-right:5px;"><a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>" class="ime_na_order_<?php echo $order->getId();?>"><?php echo $order->getNomer()." "; ?></a></td>
						<td style="padding-left:5px; width: 185px; padding-right:5px;">
							<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
								<?php 
									if(isset($_GET['done'])){
										$dateDone=$order->getDateDone();
										if(isset($dateDone)){
											echo $order->getDateAccepted();
										}
									} else if(isset($_GET['approved'])){
										$dateAccepted=$order->getDateAccepted();
										if(isset($dateAccepted)){
											echo $order->getDateAccepted();
										}
									} else {
										echo $order->getDate();
									}
								?>
							</a>
						</td>
						<td style="padding-left:5px; width: 145px; padding-right:5px;"><a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>"><?php echo $usr->getName().' '.$usr->getName_last();?></a></td><!--
						--><td style="padding-left:5px; width: 100px; padding-right:5px;"><a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>"><?php echo mb_substr($contactInformation['gsm'], 0, 10);?></a></td><!--
						--><td style="padding-left:5px; width: 160px; padding-right:5px;">
							<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
								<?php 
								if($order->getCountryName() !== false){
									echo $order->getCountryName().", ";
								}
								echo mb_substr($contactInformation['city'], 0, 15);
								?>
							</a>
						</td><!--
						--><td style="padding-left:5px; width: 165px; font-size: 10px;padding-right:5px;"><a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>"><?php echo mb_substr($order->getComment(), 0, 30);?>&nbsp;</td><!--
						--><td style="padding-left:5px; padding-right:5px;text-align: center;">
							<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
								<?php
									$brAll=0;
									if(isset($orderProducts)){
										foreach($orderProducts as $key => $val){	
											if(in_array((int)$key, $deletedItems)){
												$orderCost = $orderCost - $orderProducts[$key]['price'];
												continue;
											}									
											$brAll += (int)$orderProducts[$key]['count'];
										}
										echo $brAll;
									}
								?>
							</a>
						</td><!--
						--><td style="  padding-left:5px; width: 65px;">
							<a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
								<?php 
									echo $orderCost." лв";
									if($order->getPaypal() == 1){
										echo "<br /><span style=\"font-size: 10px;\">Paypal</span>";
										if($order->getPaypalPaid() == 1){
											echo "<span style=\"font-size: 10px;\">: платено</span>";
										}
										echo "<br><span style='display: inline-block; height: 3px; width: 1px;'></span>";
									} else if($order->isBankPayment()) {
                                        echo "<br /><span style=\"font-size: 10px;\">с паричен превод</span>";
                                        echo "<br><span style='display: inline-block; height: 3px; width: 1px;'></span>";
                                    }
								?>
							</a>
						</td><!--
						--><td style="width: 18px;">
                            <?php if ($order->getStatus() < 2) : ?>
                                <a href="#" class="deleteorderx" onclick="javascript: return deleteOrder(<?php echo $order->getID(); ?>);" style="color:#FF0000;">X</a>
                            <?php endif; ?>
                        </td><!--
						-->
					</tr>
				<?php
                } else {
                    $contactInformation=$order->getContactInformation();
                    $orderProducts=$order->getProducts();
                    ?>
                    <a href="<?php echo url.'admin/orders.php?ord='.$order->getId(); ?>">
						<tr class="<?php echo $order->isPreorder() ? "preorder" : ""; ?>">
							<td style="padding-left:5px; width: 85px;padding-right:10px;"><?php echo $order->getNomer()." "; ?></td><!--
							--><td style="padding-left:5px; width: 185px; padding-right:10px;">
								<?php 
									if(isset($_GET['done'])){
										$dateDone=$order->getDateDone();
										if(isset($dateDone)){
											echo $order->getDateAccepted();
										}
									} else if(isset($_GET['approved'])){
										$dateAccepted=$order->getDateAccepted();
										if(isset($dateAccepted)){
											echo $order->getDateAccepted();
										}
									} else {
										echo $order->getDate();
									}
								?>
							</td><!--
							--><td style="padding-left:5px; width: 150px;padding-right:10px;"><?php echo 'изтрит потребител';?></td><!--
							--><td style="padding-left:5px; padding-right:padding-right:10px;"><?php echo mb_substr($contactInformation['gsm'], 0, 10);?></td><!--
							--><td style="padding-left:5px; width: 200px;padding-right:10px;">
							<?php 
								if($order->getCountryName() !== false){
									echo $order->getCountryName().", ";
								}
								echo mb_substr($contactInformation['city'], 0, 20);
							?>
							</td><!--
							--><td style="padding-left:5px; width: 100px;padding-right:10px;"><?php echo mb_substr($order->getComment(), 0, 15);?>&nbsp;</td><!--
							--><td style="padding-left:5px; width: 70px;padding-right:10px; text-align: center;">
								<?php
									$brAll=0;
									if(isset($orderProducts)){
										if(!isProductAvailable((int)$key)){
											continue;
										}	
										foreach($orderProducts as $key => $val){
											$brAll += (int)$orderProducts[$key]['count'];
										}
										echo $brAll;
									}
								?>
							</td><!--
							--><td style="padding-left:5px; width: 50px;padding-right:10px;"><?php echo $order->getCost()." лв";?></td><!--
							--><td style="padding-left:5px;padding-right:10px;"><a href="#" class="deleteorderx" onclick="javascript: return deleteOrder(<?php echo $order->getID(); ?>);" style="color:#FF0000;">X</a></td><!--
							-->
						</tr>
					</a>
				<?php
                }
			}
		}
	?>
</table>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>