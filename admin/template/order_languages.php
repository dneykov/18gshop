<?php
require dir_root_admin_template.'__top.php';

?>
<ul id="sortable">
<?php
foreach ($__languages as $k => $v) { ?>

<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <?php echo $v->getName();?>
</li>


<?php } ?>

</ul>



<style type="text/css">
	#sortable { list-style-type: none; margin: 0; padding: 0; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
	#sortable li span { position: absolute; margin-left: -1.3em; }
	</style>
        
	<script type="text/javascript">
	$(function() {
		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});
	</script>