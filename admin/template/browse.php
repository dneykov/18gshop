<?php

/* @var $pdo pdo */
/* @var $stm PDOStatement */
/* @var $v kategoriq */

require dir_root_admin . 'template/__top.php';

$url = '?category=' . $kategoriq->getId();
$url_preorder = $url;

if (isset($_GET['availability'])) {
    $url .= '&availability=' . $_GET['availability'];
}

if (isset($_GET['filter'])) {
    $url .= '&filter=' . $_GET['filter'];
}

if (isset($_GET['mode'])) $url_preorder .= "&mode=" . $_GET['mode'];
if (isset($_GET['model'])) $url_preorder .= "&model=" . $_GET['model'];
if (isset($_GET['tag'])) {
    foreach ($_GET['tag'] as $key => $value) {
        $url_preorder .= "&tag[]=" . $value;
    }
}

$__user->permission_check('продукти', 'r');

?>
<div style="display:inline-block; margin-left:10px; float: left; margin-right: 20px;width: 240px;margin-top: 13px;">
    <div class="menu_zaglavie2" style="margin-top: 0px;">
        <a href="<?php echo $url; ?>" class="menu_zaglavie_a ime_na_kategoriq_<?php echo $kategoriq->getId(); ?>">
            <?php echo $kategoriq->getIme(); ?> </a>
        <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
        <a href="#" style="padding-left: 10px; padding-top: 1px;"
           onclick="edit_kateogriq(<?php echo $kategoriq->getId(); ?>);return false;"><?php lang_edit_e(); ?></a>
        <a href="#" style="padding-top: 1px;"
           onclick="delete_kategoriq(<?php echo $kategoriq->getId(); ?>)"><?php lang_delete_x(); ?></a>
        <a id="sort_vidove" href="sort_vidove.php?cat=<?php echo $kategoriq->getId(); ?>"
           style="padding-top: 1px;">s</a>
    </div>
    <div id="category_edit"></div>
    <?php } ?>
    <span class="vidove">
        <?php if ($vidove) foreach ($vidove as $v) {
            $controlsPadding = "padding-left: 10px;";
            ?><a href="<?php echo $url; ?>&mode=<?php echo $v->getId(); ?>"
            <?php if ($vid && ($v->getId() == $vid->getId())) { ?> style="color: #ff9207;"<?php } ?>
            >
            <span class="ime_na_vid_<?php echo $v->getId(); ?>"><?php echo $v->getIme(); ?></span> <?php echo $v->isVirtual() ? '<span></span>' : ''; ?> <span
                    style="color: #ff9207;"
                    class="broi_na_vid_<?php echo $v->getId(); ?>"><?php if ($v->getBroi_artikuli() > 0) echo $v->getBroi_artikuli(); ?></span>
            </a>

            <?php if ($v->isVirtual()) : ?>
                <span style="<?php echo $controlsPadding; $controlsPadding = "padding-left: 0"; ?> color: #ff9207;font-size: 8pt;">V</span>
            <?php endif; ?>

            <?php if ($vid && ($v->getId() == $vid->getId())) { ?>

                <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
                    <a href="#" style="<?php echo $controlsPadding; ?>"
                       onclick="edit_vid(<?php echo $v->getId(); ?>); return false;"><?php lang_edit_e(); ?></a>
                    <a href="#" onclick="delete_vid(<?php echo $v->getId(); ?>)"
                       style="padding-left: 0;"><?php lang_delete_x(); ?></a>
                    <a href="#" onclick="show_upgrade(<?php echo $v->getId(); ?>); return false;"
                       style="padding-left: 0;"><span style="color: #ff9207;font-size: 8pt;">U</span></a>
                <?php } ?>
            <?php } ?>


            <div id="vid_edit_<?php echo $v->getId(); ?>"></div>
            <div id="upgrade_<?php echo $v->getId(); ?>"></div>
        <?php } /* foreach ($stm->fetchAll() */
        if ($vid) $url .= '&mode=' . $vid->getId(); ?>
         </span>

    <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
        <a href="#" style="padding-left: 10px;" onclick="new_vid();return false;" class="plus plus-vid"
           style="font-size: 13px;"> + </a>
        <div id="new_vid"></div>
        <br>
    <?php } ?>
    <?php if (isset($vid) && !$vid->isVirtual()) : ?>
        <span class="menu_zaglavie2" style="display: block; margin-bottom:5px;">Марки</span>
        <div class="marki">

            <?php $brmodel = 0;
            if ($modeli) foreach ($modeli as $v) { ?>

                <?php $brmodel++;
                if ($v->getImage()) { ?>
                    <img src="<?php echo url . $v->getImage(); ?>"
                         style="max-height: 20px; max-width: 20px; float: right; margin-right: 4px; <?php if ($brmodel != 1) echo 'margin-top: 5px;'; ?>">
                <?php } ?>


                <a href="<?php echo $url; ?>&model=<?php echo $v->getId(); ?>"
                    <?php if ($model && ($v->getId() == $model->getId())) { ?> style="color: #ff9207;" <?php } ?>>
                    <span class="ime_na_model_<?php echo $v->getId(); ?>"><?php echo $v->getIme(); ?></span> <span
                            style="color: #ff9207;"><?php if ($v->getBroi_artikuli() > 0) echo $v->getBroi_artikuli(); ?>
                </a>


                <?php if ($model && ($v->getId() == $model->getId())) { ?>

                    <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
                        <a href="#" style="padding-left: 10px; float: left; padding-top: 1px;"
                           onclick="edit_model(<?php echo $v->getId(); ?>); return false;"><?php lang_edit_e(); ?></a>

                        <a href="#" style="padding-left: 4px; float: left; padding-top: 1px;"
                           onclick="delete_model(<?php echo $v->getId(); ?>); return false;"><?php lang_delete_x(); ?></a>
                    <?php } ?>

                <?php } ?>

                <div id="model_edit_<?php echo $v->getId(); ?>" style="clear: both;"></div>

            <?php }
            if ($model) $url .= '&model=' . $model->getId(); ?>
        </div>

        <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
            <a href="#" style="padding-left: 10px;" onclick="new_model();return false;" class="plus"
               style="font-size: 14px;">+</a>
            <div id="new_model"></div>
            <br>

        <?php } ?>

        <?php
        if ($etiketi_grupi) foreach ($etiketi_grupi as $v) {
            $etiketi_grupi_vzeto_get = false;
            $url_dobavqne = NULL; ?>
            <div class="menu_zaglavie2" style="margin-bottom: 5px;">
                <span class="menu_zaglavie_a ime_na_tag_grupa_<?php echo $v->getId(); ?>" <?php if ($v->getSelect_menu() == 1) {
                    echo "style=\"color: #ff9207 !important;\"";
                } ?>><?php echo $v->getIme(); ?> <span
                            class="tagSpec"><?php if ($v->getSelect_menu() == 2) echo "(спецификация)"; ?></span></span>


                <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
                    <a href="#" style="padding-left: 10px; "
                       onclick="edit_taggroup(<?php echo $v->getId(); ?>);return false;"><?php lang_edit_e(); ?></a>


                    <a href="#" onclick="edit_tag_group_delete(<?php echo $v->getId(); ?>, '<?php
                    $temp_echo = NULL;
                    if ($v->getEtiketi()) foreach ($v->getEtiketi() as $e) {

                        $temp_echo .= '&tag[]=' . $e->getId() . ',';

                    }

                    echo $temp_echo;


                    ?>')"><?php lang_delete_x(); ?></a>


                <?php } ?>
                <a class="sort_etiketi" href="sort_etiketi.php?etgr=<?php echo $v->getId(); ?>">s</a>

            </div>


            <div id="taggroup_edit_<?php echo $v->getId(); ?>"></div>
            <?php
            // HERE
            if ($v->getEtiketi()) foreach ($v->getEtiketi() as $e) {

                ?>
                <a href="<?php echo $url; ?>&tag[]=<?php echo $e->getId(); ?>"
                   style="font-size: 13px; padding-left:10px; height: 21px; display: inline-block; margin-top: 5px;
                   <?php if ($etiket && in_array($e->getId(), $etiket) && ($etiketi_grupi_vzeto_get == FALSE)) {
                       $etiketi_grupi_vzeto_get = true;
                       $url_dobavqne = '&tag[]=' . $e->getId(); ?> color: #ff9207;<?php }
                   ?> "><span class="ime_na_tag_<?php echo $e->getId(); ?>"><?php echo $e->getIme(); ?></span> <span
                            style="color: #ff9207;"><?php if ($e->getBroi_artikuli() > 0) echo $e->getBroi_artikuli(); ?></span></a>


                <?php if ($etiket && in_array($e->getId(), $etiket)) { ?>


                    <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
                        <a href="#" style="padding-left: 10px;"
                           onclick="edit_tag(<?php echo $e->getId(); ?>); return false;"><?php lang_edit_e(); ?></a>

                        <a href="#" onclick="delete_tag(<?php echo $e->getId(); ?>)"><?php lang_delete_x(); ?></a>
                    <?php } ?>

                    <div style="display:none;" id="tag_edit_<?php echo $e->getId(); ?>"></div>
                <?php } ?>
                <br>
            <?php }
            $url .= $url_dobavqne;
            ?>


            <?php if ($vid) { ?>

                <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
                    <a href="#" style="padding-left: 10px;"
                       onclick="new_etiket(<?php echo $v->getId(); ?>);return false;" class="plus"
                       style="font-size: 14px;">+</a>

                    <div id="new_etiket<?php echo $v->getId(); ?>" rel="new_etiket_rel_js_hide"></div>
                <?php } ?>
                <br>
            <?php } ?>


            <?php
            $etiketi_js[] = $v->getId();
        } ?>

        <?php if ($vid) { ?>
            <?php if ($__user->permission_check('продукти', 'rw', true, true)) { ?>
                <a href="#" onclick="new_taggrupa();return false;" class="nov">+ таг група</a>
                <div id="new_tag_grupa" class="new_tag"></div>
                <div style="clear: both; height: 10px;"></div>
            <?php } ?>
        <?php } ?>
    <?php endif; ?>
</div>
<div class="all_products" style="float:left;display:inline-block; width: calc(100% - 290px); margin-top: 13px;">
    <div style="width: 100%; height: 25px; margin-bottom: 10px; background-color: #f2f2f2;">
        <?php if ($vid && $vid->isVirtual()) : ?>
            <input class="search" style="width: 400px;background-position: 375px center; height: 25px;" type="text" name="add_virtual_product" onkeyup="virtual_products_suggestion(this.value, '<?php echo $_GET['mode']; ?>');">
            <div id="virtual_products_suggestion" style="display: none;"></div>
        <?php endif; ?>
        <?php if ($vid && $model) : ?>
            <?php if ($__user->permission_check('продукти', 'rw', true, true)) : ?>
                <?php if ($countPreorder > 0) { ?>
                    <div class="p_preorder">
                        <a href="<?php echo $url_preorder; ?>&preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a>
                        <a href="<?php echo $url; ?>" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a>
                    </div>
                <?php } ?>
                <a href="new_product.php?category=<?php echo $kategoriq->getId(); ?>&mode=<?php echo $vid->getId(); ?>&model=<?php echo $model->getId();
                if ($etiket) foreach ($etiket as $v) {
                    ?>&tag[]=<?php echo (int)$v;
                } ?>"
                   class="nov-product">+ <?php echo $vid->getIme() ?> <?php echo $model->getIme(); ?>
                    в <?php echo $kategoriq->getIme(); ?></a>
            <?php endif; ?>
        <?php elseif ($countPreorder > 0 && $__user->permission_check('продукти', 'rw', true, true)) : ?>
            <div class="p_preorder">
                <a href="<?php echo $url; ?>&preorder" <?php echo isset($_GET['preorder']) ? "class='active'" : ""; ?>>preorder</a>
                <a href="<?php echo $url; ?>" <?php echo !isset($_GET['preorder']) ? "class='active'" : ""; ?>>всички</a>
            </div>
        <?php endif; ?>
        <div class="filters" style="float: right; font-size: 13px; line-height: 22px;">
            <?php
            parse_str($url, $filter_url);

            $temp_filter_url = '';
            foreach ($filter_url as $key => $f_url) {
                if ($key == 'filter') continue;

                $temp_filter_url .= $key . "=" . $f_url . "&";
            }
            ?>
            <a href="<?php echo $temp_filter_url; ?>filter=last" style="margin-right: 5px;"
               class="<?php if (isset($_GET['filter']) && $_GET['filter'] == 'last' || !isset($_GET['filter'])) {
                   echo 'current';
               } ?>">Нови</a>
            <a href="<?php echo $temp_filter_url; ?>filter=low-price" style="margin-right: 5px;"
               class="<?php if (isset($_GET['filter']) && $_GET['filter'] == 'low-price') {
                   echo 'current';
               } ?>">Цена възходящо</a>
            <a href="<?php echo $temp_filter_url; ?>filter=high-price" style="margin-right: 5px;"
               class="<?php if (isset($_GET['filter']) && $_GET['filter'] == 'high-price') {
                   echo 'current';
               } ?>">Цена низходящо</a>
        </div>

        <div class="availability" style="float: right; font-size: 13px; line-height: 22px; margin-right: 50px;">
            <?php
            parse_str($url, $availability_url);

            $temp_availability_url = '';
            foreach ($availability_url as $key => $a_url) {
                if ($key == 'availability') continue;

                $temp_availability_url .= $key . "=" . $a_url . "&";
            }
            ?>
            <a href="<?php echo $temp_availability_url; ?>availability=all" style="margin-right: 5px;"
               class="<?php if (isset($_GET['availability']) && $_GET['availability'] == 'all' || !isset($_GET['availability'])) {
                   echo 'current';
               } ?>">All</a>
            <a href="<?php echo $temp_availability_url; ?>availability=online" style="margin-right: 5px;"
               class="<?php if (isset($_GET['availability']) && $_GET['availability'] == 'online') {
                   echo 'current';
               } ?>">Online</a>
            <a href="<?php echo $temp_availability_url; ?>availability=offline" style="margin-right: 5px;"
               class="<?php if (isset($_GET['availability']) && $_GET['availability'] == 'offline') {
                   echo 'current';
               } ?>">Offline</a>
            <a href="<?php echo $temp_availability_url; ?>availability=na" style="margin-right: 5px;"
               class="<?php if (isset($_GET['availability']) && $_GET['availability'] == 'na') {
                   echo 'current';
               } ?>">N/A</a>
        </div>
    </div>
    <?php
    $temp_broi = 0;
    if (isset($artikuli)) {
        foreach ($artikuli as $v) { ?>
            <div class="product"
                 style="width:180px;display:inline-block;padding-bottom:30px;padding-right:10px;vertical-align: top;">
                <div class="product-top-lenta">
                    <?php
                    if ($__user->permission_check('продукти', 'rw', true, true)) {
                        if (isset($vid) && $vid->isVirtual()) : ?>
                            <div style="color: #FF0000 ;text-align:right; float: right;cursor: pointer;margin-top:-3px; font-size: 17px;"
                                 onclick="virtual_product_delete('<?php echo $v->getId(); ?>', '<?php echo $vid->getId(); ?>');">x
                            </div>
                        <?php else: ?>
                            <div style="color: #FF0000 ;text-align:right; float: right;cursor: pointer;margin-top:-3px; font-size: 17px;"
                                 onclick="product_delete(<?php echo $v->getId(); ?>);">x
                            </div>
                        <?php endif; ?>
                        <?php
                    }
                    ?>
                    <?php
                    if ($__user->permission_check('продукти', 'rw', true, true)) {
                        if ($v->isOnline()) {
                            ?>
                            <div style="color: #fff; background-color: #F7931E; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">
                                on
                            </div>
                            <div style="color: #fff; background-color: #666; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;"
                                 onclick="product_offline_browse(<?php echo $v->getId(); ?>);">off
                            </div>
                            <?php
                        } else {
                            ?>
                            <div style="/* color: #00FF00;*/ color: #fff; background-color: #666; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;"
                                 onclick="product_online_browse(<?php echo $v->getId(); ?>);">on
                            </div>
                            <div style="color: #fff; background-color: #F7931E; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">
                                off
                            </div>
                            <?php
                        }
                        if (!$v->isAvaliable()) {
                            ?>
                            <div style="margin-left: 10px;color: #333; width: auto;cursor: pointer; float: left; display: inline-block; font-size: 13px;">
                                n/a
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div style="clear: both;"></div>
                <a href="product.php?id=<?php echo $v->getId(); ?>" <?php if (!$v->isOnline()) {
                    echo 'class="offline"';
                } ?> >
                    <div>
                        <div class="product_details_right_thumb" style="width: 180px; height: 180px;">
                            <img src="<?php echo url . $v->getKartinka_t(); ?>" alt="image">
                        </div>
                    </div>
                    <span class="product_details_right"><?php echo $v->getIme_marka(); ?></span><br>
                    <div class="product_details_right ime_na_product_<?php echo $v->getId(); ?>"
                         style="height: 35px;"><?php echo $v->getIme(); ?></div>
                    <div style="float: left;font-family: 'Verdana', sans-serif;font-size: 10px;text-align: left;overflow: hidden;height: 73px;word-wrap: break-word;width: 100%;box-sizing: border-box;margin-bottom: 10px; margin-top: 10px;">
                        <?php
                        $lines = explode(PHP_EOL, $v->getSpecification());
                        $newLines = implode(PHP_EOL, array_slice($lines, 0, 5)) . PHP_EOL;
                        echo nl2br($newLines);
                        ?>
                    </div>
                    <br>
                    <div class="product_details_right"
                         style="float: right;font-size: 15px; <?php if ($v->getCena_promo() != 0) {
                             echo 'text-decoration: line-through;';
                         } ?>"><?php echo $v->getCena(); ?> лв.
                    </div>
                    <?php if ($v->getCena_promo() != 0) echo '<div class="product_details_right" style="color: #f7931e; font-size: 15px; float:right; margin-right: 15px;">' . $v->getCena_promo() . ' лв.</div>'; ?>
                </a>
            </div>
            <?php
            $temp_broi++;
            if ($temp_broi == 50) {
                break;
            }
        }
    }
    $num_page = (int)ceil($all_artikuli_broi / 50);
    //var_dump($num_page);
    if ($num_page > 1) {
        echo '<div style="text-align:center;clear:both;margin-top: 20px;margin-bottom: 20px;">';
        for ($i = 0; $i < $num_page; $i++) {
            if ($i == $page) {
                echo '<a style="display: inline-block; padding:1px 2px;margin:0 8px;background-color:#fff; color: rgb(102, 102, 102); padding-left:5px; padding-right: 5px;border:solid 1px;" class="active" href="' . $url . '&page=' . ($i) . '">' . ($i + 1) . '</a>';
            } else {
                echo '<a style="padding:1px 2px;margin:0 8px;color:#ff9207;" href="' . $url . '&page=' . ($i) . '">' . ($i + 1) . '</a>';
            }

        }
        echo '<div>';
    }
    ?>
</div>


<script type="text/javascript">

    var new_vid_cancel_old_html = $('#new_vid').html();
    var new_model_cancel_old_html = $('#new_model').html();

    var new_new_etiket_cancel_old_html = [];



    <?php $q = NULL; if (isset ($etiketi_js)) foreach ($etiketi_js as $v) {
        echo 'new_new_etiket_cancel_old_html[' . $v . '] = ' . $v . ';' . PHP_EOL;
    }
    ?>

    for (var v in new_new_etiket_cancel_old_html) {
        new_new_etiket_cancel_old_html[v] = $('#new_etiket' + v).html();
    }

    var new_taggrupa_old_html = $('#new_tag_grupa').html();

    var kategoriq = <?php echo $kategoriq->getId(); ?>;

    var vid = <?php if ($vid) echo $vid->getId(); else echo '0'; ?>


</script>
<div style="clear:both;height: 60px;"></div>

<div class="modal">

    <div class="modal-content">
        <div class="modal-top-lenta">
            <?php if (isset($vid) && $vid->isVirtual()) : ?>
                <div class="modal-top-lenta-text">Are you sure you want to remove this virtual product?</div>
            <?php else: ?>
                <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
            <?php endif; ?>
            <div class="modal-top-close" onclick="hideModal();">×</div>
        </div>

        <div class="modal-body" style="text-align: left !important;">

        </div>
    </div>
</div>
