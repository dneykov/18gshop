<?php

$page_current = 'users';

require('template/__top.php');
/* @var $artikul artikul_admin */
?>

<style>
    #tpmenu{
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #tpmenu a{
        color: #333;
        font-size: 13px;
        margin:0 15px;
    }

    #tpmenu a:hover {
        color: rgb(248, 147, 29);
        text-decoration: none;
    }
    table tr td {
        font-size: 13px;
    }
</style>
<div id="tpmenu">
    <?php if($__user->permission_check('администратори', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users_admin.php" <?php  ?> style="color: orange;">aдминистратори</a>
    <?php } ?>
    <?php if($__user->permission_check('потребители', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users.php">клиенти</a>
<!--        <a href="--><?php //echo url_admin; ?><!--partners.php">партньори</a>-->
        <a href="<?php echo url_admin; ?>newsletter.php">абонирани за новини</a>
    <?php } ?>
</div>

<table style="margin-top: -2px;">

    <tr>

        <td colspan="2">

            <?php if($__user->permission_check('администратори', 'rw', true,true)) { ?>
            <a href="#" style="color: #fff;background-color: #666666;width: auto;margin-left: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;float: left;font-size: 13px;line-height: 25px;" onclick="users_admin_new();return false;" >+ администратор</a>

        <div id="users_admin_new">
            

        </div>

        <div id="users_admin_new_errors">


        </div>
        <?php } ?>

        </td>

    </tr>

    <tr><td colspan="3">



<table style="width: 600px;">
<?php foreach ($users as $v) { /* @var $v user_admin  */ ?>

    <tr>

    <td style="padding: 10px;font-weight: bold; color: #333; " class="ime_na_administrator_<?php echo $v->getId(); ?>">
        <?php echo $v->getUsername(); ?>
    </td>



    <td>
        <?php if ( ( $__user->getId() != $v->getId() ) && $__user->permission_check('администратори', 'rw', true,true)  ) { ?>
        <a href="#" onclick="user_admin_edit(<?php echo $v->getId(); ?>)"><?php lang_edit_e(); ?></a>

        <a href="#" onclick="users_admin_delete(<?php echo $v->getId(); ?>);return false;"><?php lang_delete_x(); ?></a>
        <?php } ?>

    </td>

    <td width="400">


    </td>

    </tr>

    <tr>

        <td colspan="3">


            <div id="admin_edit_<?php echo $v->getId(); ?>" class="admin_edit_ajax_za_skrivane">

            </div>






        </td>




    </tr>


 

<?php } ?>

                    

</table>

        </td></tr>
    
</table>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>