<style>
	.addgroup {
		color: #fff;
	    background-color: #666666;
	    width: 100px;
	    height: 25px;
	    display: block;
	    text-align: center;
	    float: left;
	    font-size: 13px;
	    line-height: 22px;
	    margin-left: 10px;
	}
	.subMenu{
		width: 100%;
	    height: 13px;
	    margin-top: -11px;
	    position: relative;
	    top: -13px;
	    padding-bottom: 10px;
	    padding-top: 0px;
	    box-shadow: 1px 0px 1px #ababab;
	    border-bottom: 1px solid #767676;
	    background-color: #f2f2f2;
	}
	.subMenu a{
		color:#333;
		display:inline-block;
		margin:0 15px;
		font-size: 13px;
	}
	.subMenu a:hover, .subMenu a.active{
		color:#F7931E;
	}
	#cont {
		margin-top: 40px;
		margin-left: 10px;
		font-size: 13px;
		padding-bottom: 60px;
	}
</style>
<div class="subMenu">
    <a href="<?php echo url_admin; ?>moduls/promo.php">Групови промоции</a>
    <a class="active" href="<?php echo url_admin; ?>groups.php">Пакети</a>
</div>
<a class="addgroup" href="<?php echo url_admin; ?>new_group.php">+ пакет</a>
<div id="cont">
    <?php
        if(isset($grupi)){
        	foreach ($grupi as $g) { 
        		$v = new paket((int)$g['id']);
        		?>
	            <div class="product" style="width:180px;display:inline-block;padding-bottom:30px;padding-right:10px;vertical-align: top;">
			       	<div style="color: #FF0000 ;text-align:right; padding-right:7px;float: right;cursor: pointer; font-size: 17px;" onclick="group_delete(<?php echo $v->getId(); ?>);">x</div>
			       	<div class="product-top-lenta">
	              	<?php if ($v->getOnline()){ ?>				
				        <div style="color: #fff; background-color: #F7931E; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">on</div>
						<div style="color: #fff; background-color: #666; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;" onclick="group_offline(<?php echo $v->getId(); ?>);">off</div>
	              	<?php }else{ ?>
					    <div style="/* color: #00FF00;*/ color: #fff; background-color: #666; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;" onclick="group_online(<?php echo $v->getId(); ?>);">on</div>
                        <div style="color: #fff; background-color: #F7931E; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">off</div>
					<?php } ?>		
					</div>
		            <div style="clear: both;"></div>		
	              	<a href="group_details.php?id=<?php echo $v->getId(); ?>" <?php if (!$v->getOnline()){echo 'class="offline"';} ?> >
		              	<div>
			                <div class="product_details_right_thumb" style="width: 180px; height: 180px;">
			                  <img src="<?php echo url.$v->getKartinka_t(); ?>" alt="image">
			                </div>
		              	</div>
		              	<span class="product_details_right ime_na_group_paket_<?php echo $v->getId(); ?>"><?php echo $v->getName(); ?></span><br>
		              	<span class="product_details_right"><?php echo $v->getPrice(); ?> лв</span><br>
	             	 </a>
	            </div>
          	<?php
          	}
        } 
    ?>
</div>

<div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>