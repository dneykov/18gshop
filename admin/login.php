<?php

define('safe_page', '1');

require '__top.php';

if(isset ($_GET['logout'])){

        setcookie('username_admin_id');
        setcookie('username_admin_pass');
        setcookie('language');


        if($__user) {

            $stm = $pdo->prepare('UPDATE `members_admin` SET `password_cookie` = ? WHERE `id` = ? ');
            $stm -> bindValue(1, md5(uniqid()));
            $stm -> bindValue(2, $__user -> getId(), PDO::PARAM_INT);
            $stm -> execute();



        }


        ?>
        <meta http-equiv="Refresh" content="0;URL=<?php echo url.'admin'; ?>" />
        <?php


}



$login_errors = NULL;
if(isset ($_POST['submit'])) {

if(!isset ($_POST['login_mail'], $_POST['login_pass'])) $login_errors[] = lang_login_error_pass_or_mail;


if($login_errors == NULL) {

    $login_mail = $_POST['login_mail'];
    $login_mail = trim($login_mail);
    $login_pass = $_POST['login_pass'];
    $login_pass = trim($login_pass);

    $login_pass = sha1($login_pass);

    $stm = $pdo->prepare('SELECT * FROM `members_admin` WHERE `username` = ? AND `password` = ? LIMIT 1');
    $stm -> bindValue(1, $login_mail, PDO::PARAM_STR);
    $stm -> bindValue(2, $login_pass, PDO::PARAM_STR);
    $stm -> execute();

    if($stm->rowCount() == 0 ) $login_errors[] = lang_login_error_pass_or_mail;

    else {

        $row_member = $stm->fetch();

        $login_pass_cookie = sha1($login_pass.uniqid());

        $stm = $pdo->prepare('UPDATE `members_admin` SET `password_cookie` = ?, `ip` = ? WHERE `id` = ? LIMIT 1');
        $stm -> bindValue(1, $login_pass_cookie, PDO::PARAM_STR);
        $stm -> bindValue(2, $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
        $stm -> bindvalue(3, $row_member['id'], PDO::PARAM_INT);
        $stm -> execute();

        setcookie('username_admin_id', $row_member['id']);
        setcookie('username_admin_pass', $login_pass_cookie);
        setcookie('language', lang_default_id);

        ?>
        <meta http-equiv="Refresh" content="0;URL=<?php echo url_admin; ?>" />
        <?php
        exit;

    }


} }






// template
require(dir_root_admin.'template/login.php');

?>
