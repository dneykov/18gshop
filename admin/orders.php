<?php
require '__top.php';

//$__user->permission_check('поръчки', 'r');


try{
    if(isset($_POST['finishPreorderID'])){
        $__user->permission_check('поръчки','rw');
        $stm = $pdo->prepare('UPDATE `orders` SET `status`=3 WHERE `id`=?;');
        $stm -> bindValue(1, (int)$_POST['finishPreorderID'], PDO::PARAM_INT);
        $stm -> execute();
    }

	if(isset($_POST['doneID'])){
		$__user->permission_check('поръчки','rw');
		$stm = $pdo->prepare('UPDATE `orders` SET `status`=2, `date_done`=CURRENT_TIMESTAMP() WHERE `id`=?;');
		$stm -> bindValue(1, (int)$_POST['doneID'], PDO::PARAM_INT);
		$stm -> execute();
		
		$order = new order_admin((int)$_POST['doneID']);
		$orderProducts=$order->getProducts();
		$priceAll=0;
		$profit=0;
		if(isset($orderProducts)){
			foreach($orderProducts as $key => $val){										
				$artikul = new artikul_admin((int)$key);
				$promocena=$artikul->getCena_promo();
				if($promocena==0){
					$price = ($artikul->getCena())*$orderProducts[$key]['count'];
				}else{
					$price = ($artikul->getCena_promo())*$orderProducts[$key]['count'];
				}
				$priceAll=$priceAll + $price;
				$tmp=$artikul->getCena_pechalba();
				$profit=$profit +  (int)$tmp;
				unset($tmp);
			}
		}

		$stm = $pdo->prepare('UPDATE `dict` SET `value`= `value` + ? WHERE `key`="TotalТurnover";');
		$stm -> bindValue(1, (int)$priceAll, PDO::PARAM_INT);
		$stm -> execute();
		
		$stm = $pdo->prepare('UPDATE `dict` SET `value`= `value` + ? WHERE `key`="TotalProfit";');
		$stm -> bindValue(1, (int)$profit, PDO::PARAM_INT);
		$stm -> execute();
		
		exit;
	}	
	if(isset($_POST['approvedID'])){
		$__user->permission_check('поръчки','rw');
		$stm = $pdo->prepare('UPDATE `orders` SET `status`=1, `date_accepted`=CURRENT_TIMESTAMP() WHERE `id`=?;');
		$stm -> bindValue(1, (int)$_POST['approvedID'], PDO::PARAM_INT);
		$stm -> execute();
		//exit;
	}	
	if(isset($_POST['deleteID'])){	
		//echo '1111111111111111';
		$__user->permission_check('поръчки','rw');
		//echo '22222222222222222';
		$stm = $pdo->prepare('DELETE FROM `orders` WHERE `id`=?;');
		$stm -> bindValue(1, (int)$_POST['deleteID'], PDO::PARAM_INT);
		$stm -> execute();		
		exit;
	}
    if(isset($_POST['canceledID'])){
        $__user->permission_check('поръчки','rw');
        $stm = $pdo->prepare('UPDATE `orders` SET `status`=4, `date_canceled`=CURRENT_TIMESTAMP() WHERE `id`=?;');
        $stm -> bindValue(1, (int)$_POST['canceledID'], PDO::PARAM_INT);
        $stm -> execute();
//        exit;
    }
    if(isset($_POST['returnedID'])){
        $__user->permission_check('поръчки','rw');
        $stm = $pdo->prepare('UPDATE `orders` SET `status`=5, `date_returned`=CURRENT_TIMESTAMP() WHERE `id`=?;');
        $stm -> bindValue(1, (int)$_POST['returnedID'], PDO::PARAM_INT);
        $stm -> execute();
//        exit;
    }
	
	
	
	if(isset($_GET['done'])){
        if(isset($_GET['preorder'])) {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE (`status`=2 OR `status` = 3) AND `details` LIKE \'%deposit%\' ORDER BY `id` DESC');
        } else {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=2 OR `status` = 3 ORDER BY `id` DESC');
        }
	}else if(isset($_GET['approved'])){
        if(isset($_GET['preorder'])) {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=1 AND `details` LIKE \'%deposit%\' ORDER BY `id` DESC');
        } else {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=1 ORDER BY `id` DESC');
        }
	}else if(isset($_GET['cancel'])){
        if(isset($_GET['preorder'])) {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=4 AND `details` LIKE \'%deposit%\' ORDER BY `id` DESC');
        } else {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=4 ORDER BY `id` DESC');
        }
    }else if(isset($_GET['return'])){
        if(isset($_GET['preorder'])) {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=5 AND `details` LIKE \'%deposit%\' ORDER BY `id` DESC');
        } else {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=5 ORDER BY `id` DESC');
        }
    }else{
        if(isset($_GET['preorder'])) {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=0 AND `details` LIKE \'%deposit%\' ORDER BY `id` DESC');
        } else {
            $stm = $pdo->prepare('SELECT * FROM `orders` WHERE `status`=0 ORDER BY `id` DESC');
        }
	}

	$stm -> execute();

	$orders = NULL;

	if($stm->rowCount() > 0) foreach ($stm->fetchAll() as $v) {
		$orders[] = new order_admin($v);
	}

} catch (Exception $e){
    greshka($e);
}
if(isset($_GET['statistik'])){
	require dir_root_admin.'/moduls/orders_statistik.php';
} else if(isset($_GET['delivery'])){
	require dir_root_admin.'/moduls/orders_delivery.php';
} else if(isset($_GET['valute'])){
	require dir_root_admin.'/moduls/valute.php';
} else if(isset($_GET['ord'])){
	$stm = $pdo->prepare("SELECT * FROM `orders` WHERE `id` = ? LIMIT 1");
	$stm -> bindValue(1, (int)$_GET['ord'], PDO::PARAM_INT);
	$stm->execute();
	if($stm->rowCount() > 0){
		require dir_root_admin_template.'order_details.php';
	} else {
		header('Location: '.url.'admin/orders.php');
	}
} else {
	require dir_root_admin_template.'orders.php';
}

