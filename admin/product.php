<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require '__top.php';

if(empty ($_GET['id']) || !isProductAvailable($_GET['id'])){
    //greshka('Ne e poso4eno id GET product.php');
    header('Location: '.url_admin);
    exit;
}

$ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
$ds->execute();
$d = $ds->fetch();
$dostavka_cena = $d['delivery_price'];
$dostavka_free = $d['free_delivery'];


$artikul = new artikul_admin((int)$_GET['id']);

$requiredFields = NULL;
$artikul_greshki = NULL;

$pat = array(

  array('url' => url.'admin', 'ime' => 'Категории'),
  array('url' => url.'admin/browse.php?category='.$artikul->getId_kategoriq(), 'ime'=>$artikul->getIme_kategoriq()),
  array('url' => url.'admin/browse.php?category='.$artikul->getId_kategoriq().'&mode='.$artikul->getId_vid(), 'ime'=>$artikul->getIme_vid()),
  array('url' => url.'admin/browse.php?category='.$artikul->getId_kategoriq().'&mode='.$artikul->getId_vid().'&model='.$artikul->getId_model(), 'ime'=>$artikul->getIme_marka()),
  array('url' => '', 'ime'=>$artikul->getIme())


);





if(isset ($_POST['submit'])){
    if($_POST['product_name'] == "" ) $requiredFields[] = 'модел';
    if($_POST['product_main_image'] == "") $requiredFields[] = 'снимка';
    if($_POST['product_cena_dostavna'] == "" && $_POST['product_cena'] == "") $requiredFields[] = 'цена на едро или крайна цена';

    if($requiredFields != NULL) {
        $stringFields = "";

        foreach ($requiredFields as $field) {
            if ($field === end($requiredFields)) {
                $stringFields .= $field;
            } else {
                $stringFields .= $field . ", ";
            }
        }

        $artikul_greshki = 'Моля въведете ' . $stringFields;
    }

    $pdo->beginTransaction();
    $makeAvailable = false;
    $hasOptionGroup = false;

    $kod = $_POST['product_kod'];

    $ime = $_POST['product_name'];
    if(isset($_POST['preorder'])) {
        $preorder = $_POST['preorder'];
    } else {
        $preorder = 0;
        foreach ($__languages as $key => $v)  {
            $_REQUEST['preorder_info_' . $v->getPrefix()] = "";
        }
    }
	$collection = $_POST['product_collection'];


$temp_img_delete = $artikul->getKatinka_dopalnitelni();
if (isset($_POST['product_images_extra'])) {
    for($q=1;$q<=12;$q++){
        $temp_img = $temp_img_delete[$q];

        if (isset($temp_img) && !in_array($temp_img->getKatinka(), $_POST['product_images_extra'])) {
            $temp_img->delete();
        }

        $stm = $pdo->prepare('UPDATE `artikuli` SET `img'.$q.'` = ? WHERE `id` = ? ');
        $stm -> bindValue(1, "", PDO::PARAM_STR);
        $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }

    foreach ($_POST['product_images_extra'] as $key => $file) {
        $stm = $pdo->prepare('UPDATE `artikuli` SET `img'.($key+1).'` = ? WHERE `id` = ? ');
        $stm -> bindValue(1, $file, PDO::PARAM_STR);
        $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }
} else {
    for($q=1;$q<=12;$q++){
        $temp_img = $temp_img_delete[$q];

        if (isset($temp_img) && !in_array($temp_img->getKatinka(), $_POST['product_images_extra'])) {
            $temp_img->delete();
        }

        $stm = $pdo->prepare('UPDATE `artikuli` SET `img'.$q.'` = ? WHERE `id` = ? ');
        $stm -> bindValue(1, "", PDO::PARAM_STR);
        $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }
}

po_taka_update_multilanguage('product_info_', 'dop_info_', 'artikuli', $artikul->getId());
po_taka_update_multilanguage('product_info2_', 'spec_', 'artikuli', $artikul->getId());
po_taka_update_multilanguage('preorder_info_', 'preorder_', 'artikuli', $artikul->getId());
po_taka_update_multilanguage('product_title_', 'title_', 'artikuli', $artikul->getId());
//po_taka_update_multilanguage('product_dop_info_', 'dop_info2_', 'artikuli', $artikul->getId());



    if(isset($_POST['product_cena'])){
        if($_POST['product_cena'] == "") {
            $product_cena = 0;
        } else {
            $product_cena = po_taka_format_cena_sql($_POST['product_cena']);
        }
    }else{
        $product_cena =0;
    }
    
    if(isset($_POST['product_cena_promo'])){
        if($_POST['product_cena_promo'] == "") {
            $product_cena_promo = 0;
        } else {
            $product_cena_promo = po_taka_format_cena_sql($_POST['product_cena_promo']);
        }
	}else{
		$product_cena_promo =0;
	}

    if(isset($_POST['product_cena_dostavna'])){
        if($_POST['product_cena_dostavna'] == "") {
            $product_cena_dostavna = 0;
        } else {
            $product_cena_dostavna = po_taka_format_cena_sql($_POST['product_cena_dostavna']);
        }
	}else{
		$product_cena_dostavna =0;
	}

	if(isset($_POST['product_cena_dostavka'])){
		$product_cena_dostavka = po_taka_format_cena_sql($_POST['product_cena_dostavka']);
	}else{
		$product_cena_dostavka =0;
	}

    if(isset($_POST['additional_delivery'])){
        $additional_delivery = po_taka_format_cena_sql($_POST['additional_delivery']);
    }else{
        $additional_delivery =0;
    }
	
	if(isset($_POST['product_main_image'])){
		$main_image = $_POST['product_main_image'];
	}else{
		$main_image ='';
	}

	if(isset($_POST['okazion'])){
		if($_POST['okazion']=='on'){
			$isokazion = 1;
		}
	}else{
		$isokazion =0;
	}
	if(isset($_POST['order'])){
		$order =$_POST['order'];
	}else{
		$order =0;
	}

    if( (double)$product_cena_promo && ( (double)$product_cena_promo > (double)$product_cena ) ) {
        setcookie('status_info', 'Промо цена не може да е по-голяма от цената');
        ?>
        <meta http-equiv="Refresh" content="0;URL=" />

        <?php
        exit;
    }

    if( $artikul_greshki != NULL ) {
        setcookie('status_info', $artikul_greshki);
        ?>
        <meta http-equiv="Refresh" content="0;URL=" />

        <?php
        exit;
    }

    if(isset ($_FILES['product_new_image']) && ( $_FILES['product_new_image']['size'] > 0 ) ) {
        if($_FILES['product_new_image']['error'] != 0 ) greshka('ERROR FILES'.print_r($_FILES,1 ));


        $artikul->delete_kartinka();

        $dir = dirname($artikul->getKatinka()).'/';
        $temp = image_upload($_FILES['product_new_image'], dir_root.$dir, $artikul->getId(), FALSE);
        $temp = preg_replace('|^(.*)uploads_images(.*)$|is', 'uploads_images$2', $temp);

        $stm = $pdo->prepare('UPDATE `artikuli` SET `kartinka` = ? WHERE `id` = ? ');
        $stm -> bindValue(1, $temp, PDO::PARAM_STR);
        $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm -> execute();


        unset($_FILES['product_new_image']);
    }





    ######### ПРОВЕРЯВАМ ЗА ГРЕШКИ
    foreach ($_FILES as $v) {
        if(  ($v['error'] != 0 ) && ($v['size'] )   )  greshka('Грешка при качването на картинката #101');
    }
    ##############################



$stm = $pdo->prepare('UPDATE `artikuli` SET
    `kod` = :kod,
    `collection_id` = :collection,
    `additional_delivery` = :additional_delivery,
    `cena` = :cena, `cena_dostavna` = :cena_dostavna, `cena_promo` = :cena_promo, `cena_dostavka` = :cena_dostavka, `add_artikuli` = :add_artikuli, `related_products` = :related_products,`ime`= :ime, `kartinka`=:main_image, `preorder`=:preorder
    WHERE `id` = :id');

$stm -> bindValue(':kod', $kod);
$stm -> bindValue(':ime', $ime);
$stm -> bindValue(':cena', $product_cena,PDO::PARAM_STR);
$stm -> bindValue(':cena_dostavna', $product_cena_dostavna,PDO::PARAM_STR);
$stm -> bindValue(':add_artikuli', $_POST['add_products_ids'],PDO::PARAM_STR);
$stm -> bindValue(':related_products', $_POST['related_products_ids'],PDO::PARAM_STR);
$stm -> bindValue(':cena_promo', $product_cena_promo,PDO::PARAM_STR);
$stm -> bindValue(':cena_dostavka', $product_cena_dostavka,PDO::PARAM_STR);
$stm -> bindValue(':main_image', $main_image,PDO::PARAM_STR);
$stm -> bindValue(':preorder', $preorder,PDO::PARAM_INT);
$stm -> bindValue(':collection', $collection,PDO::PARAM_INT);
$stm -> bindValue(':additional_delivery', $additional_delivery,PDO::PARAM_INT);
$stm -> bindValue(':id', $artikul->getId(),PDO::PARAM_INT);

$stm -> execute();

//WAREHOUSE
if(isset($_POST['warehouse_code'])){
	$warehouse_code = $_POST['warehouse_code'];
	if($warehouse_code != ""){
		$wstm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ? AND `type`= "1"');
		$wstm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
		$wstm -> execute();
		if($wstm -> rowCount() > 0){
			$del = $pdo->prepare('DELETE FROM `warehouse_products` WHERE `product_id` = ? AND `type`= "1"');
			$del -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
			$del -> execute();

			$add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `code`, `type`) VALUES (?,?,?)');
			$add -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
			$add -> bindValue(2, $warehouse_code, PDO::PARAM_INT);
			$add -> bindValue(3, 1, PDO::PARAM_INT);
			$add -> execute();
		} else {
			$add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `code`, `type`) VALUES (?,?,?)');
			$add -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
			$add -> bindValue(2, $warehouse_code, PDO::PARAM_INT);
			$add -> bindValue(3, 1, PDO::PARAM_INT);
			$add -> execute();
		}
	} else {
		$del = $pdo->prepare('DELETE FROM `warehouse_products` WHERE `product_id` = ? AND `type`= "1"');
		$del -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
		$del -> execute();
	}
}

$eg = NULL;

$stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ORDER BY `id` ');
$stm -> bindValue(1, $artikul->getId_vid(), PDO::PARAM_INT);
$stm -> execute();

if($stm->rowCount() > 0 ) foreach ($stm -> fetchAll() as $v) {
    $temp_etiket_grupi = new etiket_grupa_admin($v);
    if($temp_etiket_grupi->getEtiketi() == NULL) {
        unset ($temp_etiket_grupi);
        continue;
    }
    $eg[] = $temp_etiket_grupi;
    unset ($temp_etiket_grupi);
}

    if($eg) foreach ($eg as $v) {
        if ($v->getSelect_menu() == 1) {
            $hasOptionGroup = true;
            $del = $pdo->prepare('DELETE FROM `warehouse_products` WHERE `product_id` = ? AND `type`= "2"');
            $del -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
            $del -> execute();
            foreach ($v->getEtiketi() as $e) {
                $temp_input = $_POST['warehouse_etiket_'.$e->getId().''];
                $temp_price_big = $_POST['warehouse_price_big_'.$e->getId().''];
                $temp_price = $_POST['warehouse_price_'.$e->getId().''];
                $temp_price_promo = $_POST['warehouse_promo_price_'.$e->getId().''];
                if($temp_input != ""){
                    $add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `code`, `tag_id`, `type`,`big_price`,`price`,`promo_price`) VALUES (?,?,?,?,?,?,?)');
                    $add -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                    $add -> bindValue(2, (int)$temp_input, PDO::PARAM_INT);
                    $add -> bindValue(3, $e->getId(), PDO::PARAM_INT);
                    $add -> bindValue(4, 2, PDO::PARAM_INT);
                    $add -> bindValue(5, $temp_price_big, PDO::PARAM_STR);
                    $add -> bindValue(6, $temp_price, PDO::PARAM_STR);
                    $add -> bindValue(7, $temp_price_promo, PDO::PARAM_STR);
                    $add -> execute();
                } else {
                    $add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `tag_id`, `type`,`big_price`,`price`,`promo_price`) VALUES (?,?,?,?,?,?)');
                    $add -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                    $add -> bindValue(2, $e->getId(), PDO::PARAM_INT);
                    $add -> bindValue(3, 2, PDO::PARAM_INT);
                    $add -> bindValue(4, $temp_price_big, PDO::PARAM_STR);
                    $add -> bindValue(5, $temp_price, PDO::PARAM_STR);
                    $add -> bindValue(6, $temp_price_promo, PDO::PARAM_STR);
                    $add -> execute();
                }
            }
        }
    }

//END WAREHOUSE

// iztrivam etiketite
$stm = $pdo -> prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? ');
$stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
$stm -> execute();


// i dobavqme obnovenite
if(isset ( $_POST['product_tags'])) {

    foreach ($_POST['product_tags'] as $v) {

        $temp_etiket = new etiket_admin((int)$v);

        $temp_grupa = new etiket_grupa_admin((int)$temp_etiket->getId_etiketi_grupa());

        if($temp_grupa->getSelect_menu() == 1) $makeAvailable = true;

        if($artikul->getId_vid() != $temp_grupa->getId_vid() ) greshka('etiket koito ne e kam vida pri edit');

        unset($temp_etiket, $temp_grupa);

        $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
        $stm -> bindValue(1, $v, PDO::PARAM_INT);
        $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm -> execute();
    }
}

if($makeAvailable && $hasOptionGroup) {
    $stm_available = $pdo->prepare('UPDATE `artikuli` SET `isAvaliable` = ? WHERE `id` = ? ');
    $stm_available -> bindValue(1, 1, PDO::PARAM_STR);
    $stm_available -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
    $stm_available -> execute();

    $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
    $stm_enable_group_product -> bindValue(1, 1, PDO::PARAM_STR);
    $stm_enable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
    $stm_enable_group_product -> execute();
} else {
    if($hasOptionGroup) {
        $stm_available = $pdo->prepare('UPDATE `artikuli` SET `isAvaliable` = ? WHERE `id` = ? ');
        $stm_available -> bindValue(1, 0, PDO::PARAM_STR);
        $stm_available -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm_available -> execute();

        $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
        $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
        $stm_disable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm_disable_group_product -> execute();
    } else {
        $stm_available = $pdo->prepare('UPDATE `artikuli` SET `isAvaliable` = ? WHERE `id` = ? ');
        $stm_available -> bindValue(1, 1, PDO::PARAM_STR);
        $stm_available -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm_available -> execute();

        $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
        $stm_enable_group_product -> bindValue(1, 1, PDO::PARAM_STR);
        $stm_enable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
        $stm_enable_group_product -> execute();
    }
}

//=============Accessories=================
/*
$stm = $pdo -> prepare('DELETE FROM `artikuli_accessorсies` WHERE `akrtikul_id` = ? ');
$stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
$stm -> execute();


// i dobavqme obnovenite
if(isset ( $_POST['accessories'])) {

        foreach ($_POST['accessories'] as $acc) {
			if(!empty($acc)){
				$acc_id=(int) substr($acc,strpos($acc,'?id=')+4);
				$stm = $pdo->prepare('INSERT INTO `artikuli_accessorсies`(`accessory_id`, `akrtikul_id`) VALUES(?, ?) ');
				$stm -> bindValue(1, $acc_id, PDO::PARAM_INT);
				$stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
				$stm -> execute();
			}
        }
    }
*/
//=============Accessories========================


//============= Manual =======================
	$uploads_dir = dir_root.'/manuals/';
	if ((isset($_FILES["Manual"]))&&($_FILES['Manual']['size'] > 0)) {
		$tmp_name = $_FILES["Manual"]["tmp_name"];
		$name = $_FILES["Manual"]["name"];
		if (move_uploaded_file($tmp_name, $uploads_dir.$artikul->getId()."_".$name)){
			$stm = $pdo->prepare('UPDATE `artikuli` SET `Manual` = ? WHERE `id` = ? ');
			$stm -> bindValue(1, '/manuals/'.$artikul->getId()."_".$name, PDO::PARAM_STR);
			$stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
			$stm -> execute();
		}
	}

//============= End Manual =======================


//============= Day Offer =======================

		if($artikul->IsDayOffer()){
			if(!isset($_POST['dayoffer'])){
				$stm = $pdo->prepare('UPDATE `artikuli` SET `DayOffer` = Null WHERE `id` = ? ');
				$stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
				$stm -> execute();
			}
		}else{
			if(isset($_POST['dayoffer'])){
				$stm = $pdo->prepare('UPDATE `artikuli` SET `DayOffer` = CURRENT_TIMESTAMP WHERE `id` = ? ');
				$stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
				$stm -> execute();
			}


		}


//============= End Day Offer =======================

//============= Video =======================


        if(isset($_POST['product_video'])){
            if($_POST['product_video']!=$artikul->getVideo()){
                $tmp_video_url=$_POST['product_video'];
                if(preg_match('/vbox7.com/',$tmp_video_url)){
                    $fail = explode(':',$_POST['product_video']);
                    $tmp_video_url='http://i48.vbox7.com/player/ext.swf?vid='.$fail[2];
                }
                if(preg_match('/youtube.com/',$tmp_video_url)){
                    $fail = explode('?v=',$_POST['product_video']);
                    $tmp_video_url='https://www.youtube.com/v/'.$fail[1];
                }
                $stm = $pdo->prepare('UPDATE `artikuli` SET `video` = :video WHERE `id` = :id ');
                $stm -> bindValue(':video', $tmp_video_url, PDO::PARAM_STR);
                $stm -> bindValue(':id', $artikul->getId(), PDO::PARAM_INT);
                $stm -> execute();
            }

        }else{
                $stm = $pdo->prepare('UPDATE `artikuli` SET `video` = NULL WHERE `id` = ? ');
                $stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                $stm -> execute();
        }


//============= End video =======================
$artikul = new artikul_admin((int)$artikul->getId());
$artikul->cache_update();
$artikul->cache_search();
$artikul->image_sort_empty();

//============= Upgrade options =======================
    if (isset($_POST['upgrade_options']) && is_array($_POST['upgrade_options'])) {
        $stm = $pdo->prepare('DELETE FROM `group_option_values` WHERE `artikul_id` = ?');
        $stm->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
        $stm->execute();

        foreach ($_POST['upgrade_options'] as $key_group => $upgrade) {
            if (!empty($upgrade['default'])) {
                $defaultOption = $pdo->prepare('INSERT INTO `group_option_values`(`group_option_id`, `artikul_id`, `name`, `price`, `partner_price`, `default`) VALUES (?,?,?,?,?,?)');
                $defaultOption->bindValue(1, $key_group, PDO::PARAM_INT);
                $defaultOption->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                $defaultOption->bindValue(3, $upgrade['default'], PDO::PARAM_STR);
                $defaultOption->bindValue(4, 0, PDO::PARAM_STR);
                $defaultOption->bindValue(5, 0, PDO::PARAM_STR);
                $defaultOption->bindValue(6, 1, PDO::PARAM_INT);
                $defaultOption->execute();

                foreach ($upgrade['name'] as $key => $name) {
                    $option = $pdo->prepare('INSERT INTO `group_option_values`(`group_option_id`, `artikul_id`, `name`, `price`, `partner_price`, `default`) VALUES (?,?,?,?,?,?)');
                    $option->bindValue(1, $key_group, PDO::PARAM_INT);
                    $option->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $option->bindValue(3, $name, PDO::PARAM_STR);
                    $option->bindValue(4, $upgrade['price'][$key], PDO::PARAM_STR);
                    $option->bindValue(5, $upgrade['partner_price'][$key], PDO::PARAM_STR);
                    $option->bindValue(6, 0, PDO::PARAM_INT);
                    $option->execute();
                }
            }
        }
    }
//============= End upgrade options =======================

$pdo->commit();

setcookie('status_info','Обновен продукт '.$artikul->getIme(1));



    ?>
<meta http-equiv="Refresh" content="0;URL=browse.php?category=<?php echo $artikul->getId_kategoriq(); ?>&mode=<?php echo $artikul->getId_vid(); ?>&model=<?php echo $artikul->getId_model(); ?><?php echo isset($_SESSION['preorder']) && $_SESSION['preorder'] ? "&preorder" : ""; ?>" />
<?php

exit;

} else if(isset($_POST['template'])) {
	$_SESSION["saveAs"] = $_POST;

	header("Location: " . url_admin . "new_product.php?category={$artikul->getId_kategoriq()}&mode={$artikul->getId_vid()}&model={$artikul->getId_model()}");
}





$etiket_grupi = NULL;


$stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ORDER BY `id` ');
$stm -> bindValue(1, $artikul->getId_vid(), PDO::PARAM_INT);
$stm -> execute();

if($stm->rowCount() > 0 ) foreach ($stm -> fetchAll() as $v) {

    $temp_etiket_grupi = new etiket_grupa_admin($v);
    if($temp_etiket_grupi->getEtiketi() == NULL) {
        unset ($temp_etiket_grupi);
        continue;
    }
    $etiket_grupi[] = $temp_etiket_grupi;
    unset ($temp_etiket_grupi);

    }

$product_images_extra = $artikul->getKatinka_dopalnitelni();
$product_images_extra_text = $artikul->getKatinka_dopalnitelni_text();

require('template/product.php')


?>
