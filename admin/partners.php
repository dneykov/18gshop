<?php
require '__top.php';

$__user->permission_check('потребители', 'r');

if(isset($_GET['usr'])){
    $stm = $pdo->prepare("SELECT * FROM `members` WHERE `id` = ? AND partner = 1");
    $stm -> bindValue(1, (int)$_GET['usr'], PDO::PARAM_INT);
    $stm->execute();
    if($stm->rowCount() > 0){
        require dir_root_admin_template.'user_details.php';
    } else {
        header('Location: '.url.'admin/partners.php');
    }
} else {
    try {
        $stm = $pdo->prepare('SELECT * FROM `members` WHERE partner = 1 ORDER BY `id` DESC');
        $stm -> execute();

        $users = NULL;

        if($stm->rowCount() > 0) foreach ($stm->fetchAll() as $v) {
            $users[] = new user($v);
        }
    } catch (Exception $e){
        greshka($e);
    }
    require dir_root_admin_template.'partners.php';
}
?>
