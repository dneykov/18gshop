<?php
ini_set('max_execution_time', 300);
require_once 'warehouse/vendor/autoload.php';
require '__top.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

if (isset($_POST['submit'])) {
    if ($_FILES["file"]["error"] > 0) {
        $error = true;
    } else {
        $file = $_FILES["file"]["tmp_name"];
        $spreadsheet = IOFactory::load($file);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $del = $pdo->prepare("TRUNCATE TABLE `warehouse`");
        $del->execute();

        foreach ($sheetData as $row => $data) {
            if (empty($data['C']) || $row == 1) continue;

            $warehouseCode = $data['C'];
            $quantity = empty($data['R']) ? 0 : $data['R'];
            $price = empty($data['Y']) ? 0 : $data['Y'];
            $promoPrice = $price == $data['AA'] ? 0 : $data['AA'];

            $stm = $pdo->prepare("INSERT INTO `warehouse`(`code`, `kolichestvo`, `price`, `promo_price`) VALUES (?,?,?,?)");
            $stm->bindValue(1, $warehouseCode, PDO::PARAM_INT);
            $stm->bindValue(2, $quantity, PDO::PARAM_INT);
            $stm->bindValue(3, $price, PDO::PARAM_STR);
            $stm->bindValue(4, $promoPrice, PDO::PARAM_STR);
            $stm->execute();

            $upd = $pdo->prepare('UPDATE `warehouse_products` SET `price`= ?, `promo_price` = ? WHERE `code` = ?');
            $upd->bindValue(1, $price, PDO::PARAM_STR);
            $upd->bindValue(2, $promoPrice, PDO::PARAM_STR);
            $upd->bindValue(3, $warehouseCode, PDO::PARAM_INT);
            $upd->execute();
        }

        $res = $pdo->prepare("UPDATE `artikuli` SET `check_date`= ?");
        $res->bindValue(1, "0000-00-00", PDO::PARAM_STR);
        $res->execute();

        $stm = $pdo->prepare('SELECT product_id, MAX(CAST(price AS SIGNED)) as price, MAX(CAST(promo_price AS SIGNED)) as promo_price
                              FROM warehouse_products
                              WHERE	price IS NOT NULL AND price != "" AND tag_id IN (SELECT id_etiket FROM etiketi_zapisi WHERE id_artikul = product_id)
							  GROUP BY product_id');
        $stm->execute();

        foreach ($stm->fetchAll() as $wProduct) {
            $upd = $pdo->prepare('UPDATE `artikuli` SET `cena`= ?, `cena_promo` = ? WHERE `id` = ?');
            $upd->bindValue(1, $wProduct['price'], PDO::PARAM_STR);
            $upd->bindValue(2, $wProduct['promo_price'], PDO::PARAM_STR);
            $upd->bindValue(3, $wProduct['product_id'], PDO::PARAM_INT);
            $upd->execute();
        }

        $done = true;
    }
}

require dir_root_admin . 'warehouse/index.php';