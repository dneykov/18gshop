<?php
require '__top.php';

$__user->permission_check('продукти','rw');

if(isset ($_POST['new_kategoriq_ime'])){

    $new_kategoriq_imena = $_POST['new_kategoriq_ime'];

    if( count($new_kategoriq_imena) != ( count($__languages)  ) ) greshka('категория имена масив не е голям колкото броя езици');

    $sql = 'INSERT INTO `kategorii`( ';

    $temp_sql_check = false;
    if($__languages) foreach ($__languages as $v){

        if($temp_sql_check == false) {
            $sql .= ' `ime_'.$v-> getPrefix().'` ';
            $temp_sql_check = true;

        }
        else {
            $sql .= ' , `ime_'.$v-> getPrefix().'` ';
        }
    }

    unset($temp_sql_check);

    $sql .= ' ) VALUES( ';

    $temp_check_sql = false;
    foreach ($__languages as $v) {

        if($temp_check_sql == false) {
            $temp_check_sql = true;
            $sql .= ' ? ';
        }
        else {

            $sql .= ' , ? ';
        }
    }

    $sql .= ' ) ';

    unset($temp_check_sql);


    $stm = $pdo->prepare($sql);
    foreach ($new_kategoriq_imena as $k => $v) {

        $stm -> bindValue($k+1, $v, PDO::PARAM_STR);
    }




    $stm -> execute();
    po_taka_set_status('Добавена нова категория');

    header( 'Location: '.url_admin ) ;
   exit;



}


?>
    <form onsubmit="return new_category_validate();" action="new_kategoriq.php" enctype="multipart/form-data" method="post">
        <div class="input-tab-holder">
            <div class="input-tab-title">категория</div>
            <?php
                $inputs = "";
                if($__languages) foreach (array_reverse($__languages) as $v){
                     echo '<div class="input-tab-trigger-' . $v->getName() . '">' . $v->getName() . '</div>';
                     $inputs = $inputs . '<input class="new_category_names input-tab-' . $v->getName() . '" type="text" name="new_kategoriq_ime[]" id = "new_kategoriq_ime">';
                }
            ?>
        </div>
        <?php echo $inputs; ?>
        <br>
        <div class="error">Моля въведете категория</div>
        <button type="submit" class="button-save" style="margin-top: 10px;"><?php echo lang_save ?></button>
        <button onclick="nova_kategoriq_cancel();" class="button-cancel" style="margin-left: 5px;"><?php echo lang_cancel ?></button>
    </form>

    <script>
        $(document).ready(function(){
            $(".input-tab-bg").keyup(function () {
                var val_bg = $('.input-tab-bg').val();
                $('.input-tab-en').val(val_bg);
                $('.input-tab-ro').val(val_bg);
            });

            $(".input-tab-trigger-ro").click(function(){
                $(".input-tab-bg").hide();
                $(".input-tab-en").hide();
                $(".input-tab-ro").show();
                $(".input-tab-trigger-ro").css("background-color","#666666");
                $(".input-tab-trigger-ro").css("color","#fff");
                $(".input-tab-trigger-bg").css("background-color","#fff");
                $(".input-tab-trigger-bg").css("color","#333");
                $(".input-tab-trigger-en").css("background-color","#fff");
                $(".input-tab-trigger-en").css("color","#333");
            });

            $(".input-tab-trigger-en").click(function(){
                $(".input-tab-bg").hide();
                $(".input-tab-ro").hide();
                $(".input-tab-en").show();
                $(".input-tab-trigger-en").css("background-color","#666666");
                $(".input-tab-trigger-en").css("color","#fff");
                $(".input-tab-trigger-bg").css("background-color","#fff");
                $(".input-tab-trigger-bg").css("color","#333");
                $(".input-tab-trigger-ro").css("background-color","#fff");
                $(".input-tab-trigger-ro").css("color","#333");
            });

            $(".input-tab-trigger-bg").click(function(){
                $(".input-tab-bg").show();
                $(".input-tab-en").hide();
                $(".input-tab-ro").hide();
                $(".input-tab-trigger-en").css("background-color","#fff");
                $(".input-tab-trigger-en").css("color","#333");
                $(".input-tab-trigger-ro").css("background-color","#fff");
                $(".input-tab-trigger-ro").css("color","#333");
                $(".input-tab-trigger-bg").css("background-color","#666666");
                $(".input-tab-trigger-bg").css("color","#fff");
            });
        });
    </script>
