<?php
	require '__top.php';

	if(isset($_POST['submit'])){
		$errors = NULL;
		$price = 0;

		$sql_languages = NULL;
	    $sql_languages_values = NULL;
	    foreach ($__languages as $v) {
	        $sql_languages .= ', `dop_info_'.$v->getPrefix().'` ';
	        $sql_languages .= ', `name_'.$v->getPrefix().'` ';
	        $sql_languages_values .= ', :dopinfo'.$v->getPrefix().' ';
	        $sql_languages_values .= ', :name'.$v->getPrefix().' ';

	        if(isset ($_POST['new_artikul_info_'.lang_prefix])){
	            $temp_info = $_POST['new_artikul_info_'.$v->getPrefix()];
	        } else{
				 $temp_info = '';
			}

            if(isset ($_POST['new_name_'.lang_prefix])){
                $temp_name = $_POST['new_name_'.$v->getPrefix()];
            } else{
                $temp_name = '';
            }

	        $new_prodict_dopinfo[$v->getPrefix()] = $temp_info;
	        unset($temp_info);
            $new_prodict_name[$v->getPrefix()] = $temp_name;
            unset($temp_name);
	    }

        if($new_prodict_name[lang_default_prefix] == "") $errors[] = "невалидно име";

        $percent = (int)$_POST['percent'];

        if($percent == "" || $percent < 0 || $percent > 100) $errors[] = "невалидно намаление";

        if(!isset($_POST['add_products_ids']) || $_POST['add_products_ids'] == "") $errors[] = "не сте добавили продукти";

        if(!isset($_POST['product_main_image']) || $_POST['product_main_image'] == "") $errors[] = "невалидно изображение";

        if($errors == null){

        	$products = $_POST['add_products_ids'];
	        $productsArray = explode(",", $products);
	        foreach ($productsArray as $p) {
	        	$artikul = new artikul((int)$p);
	        	$price += $artikul->getCena();
	        	unset($artikul);
	        }

	        $priceNormal = $price;
	        $priceNew = $price - ($price*($percent/100));

	        $stm = $pdo->prepare('INSERT INTO `group_products` (`percent`, `price`, `price_normal`, `online`, `image`'.$sql_languages.') VALUES (:percent, :price, :price_normal, :online, :image'.$sql_languages_values.')');
		    $stm -> bindValue(':percent', $percent, PDO::PARAM_INT);
		    $stm -> bindValue(':price', $priceNew, PDO::PARAM_INT);
		    $stm -> bindValue(':price_normal', $priceNormal, PDO::PARAM_INT);
		    $stm -> bindValue(':online', 1, PDO::PARAM_INT);
		    $stm -> bindValue(':image', $_POST['product_main_image'], PDO::PARAM_STR);
		    foreach ($__languages as $k => $v) {
				$stm -> bindValue(':dopinfo'.$v->getPrefix(), $new_prodict_dopinfo[$v->getPrefix()], PDO::PARAM_STR);
				$stm -> bindValue(':name'.$v->getPrefix(), $new_prodict_name[$v->getPrefix()], PDO::PARAM_STR);
		    }
   			$stm->execute();

   			$group_id = $pdo->lastInsertId();

   			foreach ($productsArray as $p) {
	        	$artikul = new artikul((int)$p);
	        	
	        	$stm = $pdo->prepare('INSERT INTO `group_products_zapisi` (`artikul_id`, `group_id`, `cat_id`, `vid_id`, `marka_id`) VALUES (:artikul_id, :group_id, :cat_id, :vid_id, :marka_id)');
		        $stm -> bindValue(':artikul_id', (int)$artikul->getId(), PDO::PARAM_INT);
			    $stm -> bindValue(':group_id', (int)$group_id, PDO::PARAM_INT);
			    $stm -> bindValue(':cat_id', (int)$artikul->getId_kategoriq(), PDO::PARAM_INT);
			    $stm -> bindValue(':vid_id', (int)$artikul->getId_vid(), PDO::PARAM_INT);
			    $stm -> bindValue(':marka_id', (int)$artikul->getId_model(), PDO::PARAM_INT);
	   			$stm -> execute();

	        	unset($artikul);
	        }

	        header("Location: ".url_admin."groups.php");
	    }
	}


	require 'template/__top.php';
	require 'template/new_group.php';