<?php

/* @var $pdo pdo */
/* @var $stm PDOStatement */

require dir_root_admin.'template/__top.php';

?>
<script>
	function syncAllProducts(){
		$(".sync-loader").css("display", "table");
		$.ajax({
			type: "POST",
			url: "<?php echo url; ?>admin/ajax/syncAllProduct.php",
			data: {action : 'sync'},
			cache: false,
			success: function() {
				$(".sync-loader").hide();
			}
		});
	}
</script>
<style>
	.sync-loader {
		position: fixed;
		top: 0;
		left: 0;
		display: table;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.4);
		text-align: center;
		display: none;
	}
	.sync-loader p {
		vertical-align: middle;
		display: table-cell;
		text-align: center;
	}
	.sync-loader p img {
		margin-right: 12px;
	}
	.syncAll {
		background-color: #666666;
		width: 100px;
		color: white;
		text-align: center;
		cursor: pointer;
		font-size: 13px;
		line-height: 22px;
		margin-left: 50px;
		display: inline-block;
		float: left;
    	padding-bottom: 3px;
	}
	.syncAll:hover {
		background-color: #ff9207;
	}
</style>
<div style="margin-left: 10px;">
	<div style="margin: 10px 0px 20px 5px;font-size: 13px;color:#333;">Синхронизиране на продуктите от <span style="color: #333; font-size: 13px;">Excel 97-2003 (.xls) <span style="margin-left: 5px;">Колонa A</span></span></div>
	<form action="" method="post" enctype="multipart/form-data" style="margin-left: 5px;display: inline-block; float: left;">
		<input type="file" name="file" id="file">
		<button type="submit" name="submit" class="button-save">Upload</button>
	</form>
	<div class="syncAll" onclick="syncAllProducts();">Sync</div>
	<div class="sync-loader"><p><img src="<?php echo url; ?>images/ajax-loader-2.gif" width="30" alt=""><br>Syncing...</p></div>
	<?php
	if(isset($done)){
		echo "<div style='color: rgb(139, 200, 75); font-size: 13px; margin-top: 15px; box-sizing: border-box; padding-left: 5px;color: red;font-size: 13px; float: left; width: 100%;'>Синхронизацията е завършена !</div>";
	}
	if(isset($error)){
		echo "<div style='color: red; font-size: 13px; margin-top: 15px; box-sizing: border-box; padding-left: 5px;color: red;font-size: 13px; float: left; width: 100%;'>Моля качете вашия Excel (.xls 97-2003) файл</div>";
	}
	?>
</div>