<?php
	require "../__top.php";
	
	$nameBG = $_POST['namebg'];
	$nameEN = $_POST['nameen'];
	$nameRO = $_POST['namero'];
	$id = $_POST['id'];

	$stm = $pdo->prepare("UPDATE `help_tags` SET `name_bg`=?,`name_en`=?,`name_ro`=? WHERE `id`=?");
	$stm -> bindValue(1, $nameBG, PDO::PARAM_STR);
	$stm -> bindValue(2, $nameEN, PDO::PARAM_STR);
	$stm -> bindValue(3, $nameRO, PDO::PARAM_STR);
	$stm -> bindValue(4, (int)$id, PDO::PARAM_INT);
	$stm -> execute();
