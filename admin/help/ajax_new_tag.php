<?php
	require "../__top.php";
	$nameBG = $_POST['namebg'];
	$nameEN = $_POST['nameen'];
	$nameRO = $_POST['namero'];
	$id = $_POST['id'];
	$stm = $pdo->prepare("INSERT INTO `help_tags`(`cat_id`, `name_bg`, `name_en`, `name_ro`) VALUES (?,?,?,?)");
	$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
	$stm -> bindValue(2, $nameBG, PDO::PARAM_STR);
	$stm -> bindValue(3, $nameEN, PDO::PARAM_STR);
	$stm -> bindValue(4, $nameRO, PDO::PARAM_STR);
	$stm -> execute();