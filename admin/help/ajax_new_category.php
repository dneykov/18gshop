<?php
	require "../__top.php";
	$nameBG = $_POST['namebg'];
	$nameEN = $_POST['nameen'];
	$nameRO = $_POST['namero'];
	$stm = $pdo->prepare("INSERT INTO `help_groups`(`name_bg`, `name_en`, `name_ro`) VALUES (?,?)");
	$stm -> bindValue(1, $nameBG, PDO::PARAM_STR);
	$stm -> bindValue(2, $nameEN, PDO::PARAM_STR);
	$stm -> bindValue(3, $nameRO, PDO::PARAM_STR);
	$stm -> execute();