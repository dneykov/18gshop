<?php

require '../__top.php';

$__user->permission_check('езици','rw');

try{

if(isset ($_GET['disable'])){
    $id = (int)$_GET['disable'];
}
else greshka('ne e poso4eno get id');

$lang = new language((int)$id);

if($lang->getDefault() == 1){

    po_taka_set_status('не може да деактивираш главния език');
    exit;
}


$stm = $pdo->prepare('UPDATE `languages` SET `enabled` = 0 WHERE `id` = ? ');
$stm -> bindValue(1, $lang->getId(), PDO::PARAM_INT);
$stm -> execute();

exit;
}
catch (Exception $e){
    greshka($e);
}

?>
