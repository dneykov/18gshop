<?php

require '../__top.php';

$__user->permission_check('продукти','rw');

if(isset ($_REQUEST['id'])){
    $id = (int)$_REQUEST['id'];
    $a = new kategoriq_admin($id);

    if(isset ($_GET['delete'])){
        die('deleten e e gotov');
    }







if(isset ($_POST['delete'])){
    $a->delete();
    exit("1");
}









if(isset ($_POST['category_edit_submit'])){


$pdo->beginTransaction();


po_taka_update_multilanguage('category_edit_name_', 'ime_', 'kategorii', $a->getId());

$stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id_kategoriq` = ? ');
$stm -> bindValue(1, $a->getId(), PDO::PARAM_INT);
$stm -> execute();

if($stm->rowCount() > 0 ) foreach ($stm->fetchAll() as $v) {

    $temp = new artikul_admin($v);
    $temp -> cache_update();
    $temp -> cache_search();

    unset ($temp);

    }



    $pdo->commit();

    po_taka_redirect_to_referrer();
    exit;
}
?>

    <form action="<?php echo url_admin.'ajax/category_edit.php'; ?>" method="post" style="margin-top: 10px;">
        <div class="input-tab-holder">
            <div class="input-tab-title">категория</div>
            <?php 
                $inputs = "";
                foreach (array_reverse($a->getIme_all()) as $v){ 
                     echo '<div class="input-tab-trigger-' . $v['lang_name'] . '">' . $v['lang_name'] . '</div>'; 
                     $inputs = $inputs . '<input class="new_category_names input-tab-' . $v['lang_name'] . '" type="text" name="category_edit_name_' . $v['lang_prefix'] . '" value="' . $v['name'] . '">';
                }
            ?>
        </div>
        <?php echo $inputs; ?>
        <br>
        
        <input type="submit" name="category_edit_submit" style="margin-top: 15px;" class="button-save" value="<?php echo lang_save; ?>">
        <button onclick="edit_kategoriq_cancel();" class="button-cancel" style="margin-left: 5px;"><?php echo lang_cancel ?></button>

        <input type="hidden" name="id" value="<?php echo $a->getId(); ?>">
    </form>

    <script>
        $(document).ready(function(){
            $(".input-tab-bg").keyup(function () {
                var val_bg = $('.input-tab-bg').val();
                $('.input-tab-en').val(val_bg);
                $('.input-tab-ro').val(val_bg);
            });

            $(".input-tab-trigger-ro").click(function(){
                $(".input-tab-bg").hide();
                $(".input-tab-en").hide();
                $(".input-tab-ro").show();
                $(".input-tab-trigger-ro").css("background-color","#666666");
                $(".input-tab-trigger-ro").css("color","#fff");
                $(".input-tab-trigger-bg").css("background-color","#fff");
                $(".input-tab-trigger-bg").css("color","#333");
                $(".input-tab-trigger-en").css("background-color","#fff");
                $(".input-tab-trigger-en").css("color","#333");
            });

            $(".input-tab-trigger-en").click(function(){
                $(".input-tab-bg").hide();
                $(".input-tab-ro").hide();
                $(".input-tab-en").show();
                $(".input-tab-trigger-en").css("background-color","#666666");
                $(".input-tab-trigger-en").css("color","#fff");
                $(".input-tab-trigger-bg").css("background-color","#fff");
                $(".input-tab-trigger-bg").css("color","#333");
                $(".input-tab-trigger-ro").css("background-color","#fff");
                $(".input-tab-trigger-ro").css("color","#333");
            });

            $(".input-tab-trigger-bg").click(function(){
                $(".input-tab-bg").show();
                $(".input-tab-en").hide();
                $(".input-tab-ro").hide();
                $(".input-tab-trigger-en").css("background-color","#fff");
                $(".input-tab-trigger-en").css("color","#333");
                $(".input-tab-trigger-ro").css("background-color","#fff");
                $(".input-tab-trigger-ro").css("color","#333");
                $(".input-tab-trigger-bg").css("background-color","#666666");
                $(".input-tab-trigger-bg").css("color","#fff");
            });
        });
    </script>
    <br>
<?php
} else {
    greshka('не е посочена категория');
}