<?php

require '../__top.php';

$__user->permission_check('продукти','rw');

try{

$a = new model_admin((int)$_REQUEST['id']);


if(isset ($_POST['delete'])){
$a->delete();

exit;
    
}


if(isset ($_POST['model_edit_from_submit'],$_POST['model_edit_name'])){



if(isset ($_FILES['model_edit_image'])){


    $image = $_FILES['model_edit_image'];

    if( ($image['size'] > 0 ) && $image['error'] == 0 ) {

        $a->delete_image();
        
		$image_path = '../../images/models/'.$a->getId().'_60.jpg';
		$image_path_color = '../../images/models/'.$a->getId().'_60_color.jpg';
		
		$image = new Imagick($_FILES['model_edit_image']["tmp_name"]);
		$image->setImageColorSpace(Imagick::COLORSPACE_RGB); 
		$image->thumbnailImage(0,60,false);			
		$image->writeImage($image_path_color);
		
		$image = new Imagick($_FILES['model_edit_image']["tmp_name"]);
		$image->setImageColorSpace(Imagick::COLORSPACE_RGB);
		$image->setimagetype(Imagick::IMGTYPE_TRUECOLOR);
		$image->modulateImage(100,0,100);
		$image->thumbnailImage(0,60,false);			
		$image->writeImage($image_path);
		
		
		
		$image_path = mb_substr($image_path, 6);
		$image_path_color = mb_substr($image_path_color, 6);

		$stm = $pdo->prepare('UPDATE `model` SET `image`= ? WHERE `id` = ? ');
		$stm -> bindValue(1, $image_path_color, PDO::PARAM_STR);
		$stm -> bindValue(2, $a->getId(), PDO::PARAM_INT);
		$stm -> execute();
			
    }


        $stm = $pdo -> prepare('UPDATE `model` SET `ime` = ? WHERE `id` = ? ');
        $stm -> bindValue(1, $_POST['model_edit_name']);
        $stm -> bindValue(2, $a->getId(), PDO::PARAM_INT);
        $stm -> execute();


}


po_taka_redirect_to_referrer();

exit;

}





} catch (Exception $e){
    greshka($e);
}


?>



<form action="<?php echo url_admin.'ajax/model_edit.php'; ?>" method="post" enctype="multipart/form-data">



    <input type="text" name="model_edit_name" value="<?php echo $a->getIme(); ?>">
    <br>
    <input type="file" name="model_edit_image">
    <br>
    <button type="submit" class="button-save" style="margin-top: 15px; margin-bottom: 15px;" name="model_edit_from_submit"><?php echo lang_save; ?></button>
    <button type="button" class="button-cancel" onclick="edit_model_cancel(<?php echo $a->getId(); ?>);"><?php echo lang_cancel; ?></button>


    <input type="hidden" name="id" value="<?php echo $a->getId(); ?>">


</form>
