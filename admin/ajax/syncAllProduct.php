<?php
require '../../__top.php';
$action = $_POST['action'];

switch ($action) {
    case 'sync' :
        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE id IN(SELECT DISTINCT product_id FROM `warehouse_products`) AND `preorder` = 0');
        $stm->execute();
        $wstm = $pdo->prepare('SELECT * FROM warehouse_products INNER JOIN warehouse ON warehouse_products.code = warehouse.code');
        $wstm->execute();
        $warehouse_products = array();
        foreach ($wstm->fetchAll() as $v) {
            $warehouse_products[$v['product_id']][] = [
                'code' => $v['code'],
                'tag_id' => $v['tag_id'],
                'type' => $v['type'],
            ];
        }

        foreach ($stm->fetchAll() as $v) {
            $artikul = new artikul_admin($v);
            $isAvaliable = $artikul->isAvaliable();
            $checkDate = $artikul->getCheckDate();
            $nowDate = date("Y-m-d");
            $tempAval = false;
            if ($artikul->isAvaliable()) $tempAval = true;

            if (array_key_exists($artikul->getId(), $warehouse_products)) {
                $here = false;
                $artikul_tag_ids = array();
                foreach ($warehouse_products[$artikul->getId()] as $v) {
                    $tempCode = $v['code'];
                    $tempTagId = $v['tag_id'];

                    $stmTagCheck = $pdo->prepare('SELECT id FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? LIMIT 1');
                    $stmTagCheck->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                    $stmTagCheck->bindValue(2, $tempTagId, PDO::PARAM_INT);
                    $stmTagCheck->execute();

                    $type = $v['type'];
                    $stm = $pdo->prepare('SELECT id FROM `warehouse` WHERE `code` = ? LIMIT 1');
                    $stm->bindValue(1, (int)$tempCode, PDO::PARAM_INT);
                    $stm->execute();
                    if ($stm->rowCount() > 0) {
                        $artikul_tag_ids[] = $tempTagId;
                        $here = true;
                    } else if (is_null($tempCode) && $stmTagCheck->rowCount() > 0) {
                        $artikul_tag_ids[] = $tempTagId;
                        $here = true;
                    }
                }
                if ($type == 2) {
                    if ($artikul->getTaggroup_dropdown() != null) {
                        foreach ($artikul->getTaggroup_dropdown() as $td) {
                            foreach ($td->getEtiketi() as $t) {
                                $stm = $pdo->prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? ');
                                $stm->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                                $stm->bindValue(2, $t->getId(), PDO::PARAM_INT);
                                $stm->execute();
                            }
                        }
                    }

                    foreach ($artikul_tag_ids as $etk) {
                        if (empty($etk)) continue;
                        $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
                        $stm->bindValue(1, $etk, PDO::PARAM_INT);
                        $stm->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                        $stm->execute();
                    }
                }
                if ($here) {
                    if (count($artikul_tag_ids) > 0) {
                        $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                        $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                        $upd->bindValue(2, 1, PDO::PARAM_INT);
                        $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                        $upd->execute();

                        $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                        $stm_enable_group_product->bindValue(1, 1, PDO::PARAM_STR);
                        $stm_enable_group_product->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                        $stm_enable_group_product->execute();

                        $isAvaliable = true;
                    } else {
                        $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                        $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                        $upd->bindValue(2, 0, PDO::PARAM_INT);
                        $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                        $upd->execute();

                        $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                        $stm_disable_group_product->bindValue(1, 0, PDO::PARAM_STR);
                        $stm_disable_group_product->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                        $stm_disable_group_product->execute();

                        $isAvaliable = false;
                    }
                } else {
                    if (!$artikul->isPreorder()) {
                        $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                        $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                        $upd->bindValue(2, 0, PDO::PARAM_INT);
                        $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                        $upd->execute();

                        $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                        $stm_disable_group_product->bindValue(1, 0, PDO::PARAM_STR);
                        $stm_disable_group_product->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                        $stm_disable_group_product->execute();

                        $isAvaliable = false;
                    }
                }
            } else {
                $removeAvailability = false;
                if ($artikul->getTaggroup_dropdown() != null) {
                    foreach ($artikul->getTaggroup_dropdown() as $td) {
                        if ($td->getSelect_menu()) {
                            foreach ($td->getEtiketi() as $t) {
                                if (!empty($artikul->getWarehouseTagCode($t->getId()))) {
                                    $stm = $pdo->prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? ');
                                    $stm->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                                    $stm->bindValue(2, $t->getId(), PDO::PARAM_INT);
                                    $stm->execute();

                                    $removeAvailability = true;
                                }
                            }
                        }
                    }
                } else if (!empty($artikul->getWarehouseCode())) {
                    $removeAvailability = true;
                }

                if ($removeAvailability) {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd->bindValue(2, 0, PDO::PARAM_STR);
                    $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd->execute();
                } else {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ? WHERE `id` = ? LIMIT 1');
                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $upd->execute();
                }
            }

            $dartikuli = $artikul->getAddArtikuli();
            //dop artikuli
            if ($dartikuli != "") {
                $d_art_arr = explode(",", $dartikuli);
                foreach ($d_art_arr as $dart) {
                    $dr = new artikul((int)$dart);
                    $isAvaliable = $dr->isAvaliable();
                    $checkDate = $dr->getCheckDate();
                    $nowDate = date("Y-m-d");
                    if ($checkDate != $nowDate) {
                        $tempAval = false;
                        if ($dr->isAvaliable()) $tempAval = true;
                        $wstm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ?');
                        $wstm->bindValue(1, $dr->getId(), PDO::PARAM_INT);
                        $wstm->execute();
                        if ($wstm->rowCount() > 0) {
                            $here = false;
                            $artikul_tag_ids = array();
                            foreach ($wstm->fetchAll() as $v) {
                                $tempCode = $v['code'];
                                $tempTagId = $v['tag_id'];

                                $stmTagCheck = $pdo->prepare('SELECT id FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? LIMIT 1');
                                $stmTagCheck->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                                $stmTagCheck->bindValue(2, $tempTagId, PDO::PARAM_INT);
                                $stmTagCheck->execute();

                                $type = $v['type'];
                                $stm = $pdo->prepare('SELECT id FROM `warehouse` WHERE `code` = ? LIMIT 1');
                                $stm->bindValue(1, (int)$tempCode, PDO::PARAM_INT);
                                $stm->execute();
                                if ($stm->rowCount() > 0) {
                                    $artikul_tag_ids[] = $tempTagId;
                                    $here = true;
                                } else if (is_null($tempCode) && $stmTagCheck->rowCount() > 0) {
                                    $artikul_tag_ids[] = $tempTagId;
                                    $here = true;
                                }


                            }
                            if ($type == 2) {
                                if ($dr->getTaggroup_dropdown() != null) {
                                    foreach ($dr->getTaggroup_dropdown() as $td) {
                                        foreach ($td->getEtiketi() as $t) {
                                            $stm = $pdo->prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? ');
                                            $stm->bindValue(1, $dr->getId(), PDO::PARAM_INT);
                                            $stm->bindValue(2, $t->getId(), PDO::PARAM_INT);
                                            $stm->execute();
                                        }
                                    }
                                }
                                foreach ($artikul_tag_ids as $etk) {
                                    $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
                                    $stm->bindValue(1, $etk, PDO::PARAM_INT);
                                    $stm->bindValue(2, $dr->getId(), PDO::PARAM_INT);
                                    $stm->execute();
                                }
                            }
                            if ($here) {
                                if (count($artikul_tag_ids) > 0) {
                                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                                    $upd->bindValue(2, 1, PDO::PARAM_INT);
                                    $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                                    $upd->execute();

                                    $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                                    $stm_enable_group_product->bindValue(1, 1, PDO::PARAM_STR);
                                    $stm_enable_group_product->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                                    $stm_enable_group_product->execute();

                                    $isAvaliable = true;
                                } else {
                                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                                    $upd->bindValue(2, 0, PDO::PARAM_INT);
                                    $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                                    $upd->execute();

                                    $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                                    $stm_disable_group_product->bindValue(1, 0, PDO::PARAM_STR);
                                    $stm_disable_group_product->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                                    $stm_disable_group_product->execute();

                                    $isAvaliable = false;
                                }
                            } else {
                                $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                                $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                                $upd->bindValue(2, 0, PDO::PARAM_INT);
                                $upd->bindValue(3, $dr->getId(), PDO::PARAM_INT);
                                $upd->execute();

                                $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                                $stm_disable_group_product->bindValue(1, 0, PDO::PARAM_STR);
                                $stm_disable_group_product->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                                $stm_disable_group_product->execute();

                                $isAvaliable = false;
                            }
                        } else {
                            $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ? WHERE `id` = ? LIMIT 1');
                            $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                            $upd->bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                            $upd->execute();
                        }
                    }
                }
            }
            $artikul->cache_update();
            $artikul->cache_search();
        }

        $stm = $pdo->prepare('SELECT product_id, MAX(CAST(price AS SIGNED)) as price, MAX(CAST(promo_price AS SIGNED)) as promo_price
                              FROM warehouse_products
                              WHERE	price IS NOT NULL AND price != "" AND tag_id IN (SELECT id_etiket FROM etiketi_zapisi WHERE warehouse_products.product_id = etiketi_zapisi.id_artikul)
							  GROUP BY product_id');
        $stm->execute();

        foreach ($stm->fetchAll() as $wProduct) {
            $upd = $pdo->prepare('UPDATE `artikuli` SET `cena`= ?, `cena_promo` = ? WHERE `id` = ?');
            $upd->bindValue(1, $wProduct['price'], PDO::PARAM_STR);
            $upd->bindValue(2, $wProduct['promo_price'], PDO::PARAM_STR);
            $upd->bindValue(3, $wProduct['product_id'], PDO::PARAM_INT);
            $upd->execute();
        }
        break;

}