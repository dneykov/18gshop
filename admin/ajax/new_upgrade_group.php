<?php
require '../__top.php';


$__user->permission_check('продукти','rw');

try{
    if(isset ($_POST['mode'])){
        $stm = $pdo->prepare('INSERT INTO `mode_group_option` (`mode_id`) VALUES (:mode_id)');
        $stm->bindValue(':mode_id', $_POST['mode'], PDO::PARAM_INT);
        $stm->execute();
        $id = $pdo->lastInsertId();
        po_taka_update_multilanguage('ime_', 'ime_', 'mode_group_option', $id);
    }

} catch (Exception $e){
    greshka($e);
}
?>
<span class="upgrade_group_<?php echo $id; ?>">
    <span class="ime"><?php echo $_POST['ime_' . lang_default_prefix]; ?></span>

    <?php if($__user->permission_check('продукти','rw',true,true)) { ?>
        <a href="#" style="padding-left: 10px;" onclick="edit_upgradee_group(<?php echo $id; ?>); return false;"><?php lang_edit_e(); ?></a>
        <a href="#" onclick="delete_upgrade_group(<?php echo $id; ?>)" style="padding-left: 0px;"><?php lang_delete_x(); ?></a>
    <?php } ?>

    <div id="edit_upgrade_group_<?php echo $id; ?>"></div>
</span>