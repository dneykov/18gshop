<?php 
require '../__top.php';
$artikul = new artikul_admin((int)$_POST["product_id"]);

$stm = $pdo->prepare("INSERT INTO `product_vid`(`artikul_id`, `vid_id`) VALUES (?,?)");
$stm->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
$stm->bindValue(2, (int)$_POST['vid'], PDO::PARAM_INT);
$stm->execute();

?>

<div class="product"
     style="width:180px;display:inline-block;padding-bottom:30px;padding-right:10px;vertical-align: top;">
    <div class="product-top-lenta">
        <?php
        if ($__user->permission_check('продукти', 'rw', true, true)) {
            ?>
            <div style="color: #FF0000 ;text-align:right; float: right;cursor: pointer;margin-top:-3px; font-size: 17px;"
                 onclick="virtual_product_delete('<?php echo $artikul->getId(); ?>', '<?php echo (int)$_POST['vid'];?>');">x
            </div>
            <?php
        }
        ?>
        <?php
        if ($__user->permission_check('продукти', 'rw', true, true)) {
            if ($artikul->isOnline()) {
                ?>
                <div style="color: #fff; background-color: #F7931E; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">
                    on
                </div>
                <div style="color: #fff; background-color: #666; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;"
                     onclick="product_offline_browse(<?php echo $artikul->getId(); ?>);">off
                </div>
                <?php
            } else {
                ?>
                <div style="/* color: #00FF00;*/ color: #fff; background-color: #666; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;"
                     onclick="product_online_browse(<?php echo $artikul->getId(); ?>);">on
                </div>
                <div style="color: #fff; background-color: #F7931E; width: 28px; text-align: center; cursor: pointer; float: left; display: inline-block; font-size: 13px;">
                    off
                </div>
                <?php
            }
            if (!$artikul->isAvaliable()) {
                ?>
                <div style="margin-left: 10px;color: #333; width: auto;cursor: pointer; float: left; display: inline-block; font-size: 13px;">
                    n/a
                </div>
                <?php
            }
        }
        ?>
    </div>
    <div style="clear: both;"></div>
    <a href="product.php?id=<?php echo $artikul->getId(); ?>" <?php if (!$artikul->isOnline()) {
        echo 'class="offline"';
    } ?> >
        <div>
            <div class="product_details_right_thumb" style="width: 180px; height: 180px;">
                <img src="<?php echo url . $artikul->getKartinka_t(); ?>" alt="image">
            </div>
        </div>
        <span class="product_details_right"><?php echo $artikul->getIme_marka(); ?></span><br>
        <div class="product_details_right ime_na_product_<?php echo $artikul->getId(); ?>"
             style="height: 35px;"><?php echo $artikul->getIme(); ?></div>
        <div style="float: left;font-family: 'Verdana', sans-serif;font-size: 10px;text-align: left;overflow: hidden;height: 73px;word-wrap: break-word;width: 100%;box-sizing: border-box;margin-bottom: 10px; margin-top: 10px;">
            <?php
            $lines = explode(PHP_EOL, $artikul->getSpecification());
            $newLines = implode(PHP_EOL, array_slice($lines, 0, 5)) . PHP_EOL;
            echo nl2br($newLines);
            ?>
        </div>
        <br>
        <div class="product_details_right"
             style="float: right;font-size: 15px; <?php if ($artikul->getCena_promo() != 0) {
                 echo 'text-decoration: line-through;';
             } ?>"><?php echo $artikul->getCena(); ?> лв.
        </div>
        <?php if ($artikul->getCena_promo() != 0) echo '<div class="product_details_right" style="color: #f7931e; font-size: 15px; float:right; margin-right: 15px;">' . $artikul->getCena_promo() . ' лв.</div>'; ?>
    </a>
</div>
