<?php
require "../__top.php";

$comment = $_POST['comment'];
$product_ids = $_POST['product_ids'];
$id = $_POST['id'];

$stm = $pdo->prepare("UPDATE `orders` SET `admin_comment`=?,`products_offered`=? WHERE `id`=?");
$stm->bindValue(1, $comment, PDO::PARAM_STR);
$stm->bindValue(2, $product_ids, PDO::PARAM_STR);
$stm->bindValue(3, (int)$id, PDO::PARAM_INT);
$stm->execute();