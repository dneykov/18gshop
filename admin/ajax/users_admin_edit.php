<?php

require '../__top.php';


$__user->permission_check('администратори','rw');


try{

$stm = $pdo->prepare('SELECT * FROM `members_admin_permissions` ORDER BY `id` ');
$stm -> execute();

foreach ($stm->fetchAll() as $v) {

    $prava[] = array(
        'name' => htmlspecialchars($v['ime']),
        'id' => $v['id']
    );

}



if(!isset ($_GET['id'])){
    greshka('nqma get ID');
}

$a = new user_admin((int)$_GET['id']);

if ($__user->getId() == $a->getId() ) greshka('ne moje da se editne6 sam ot admin userite');






if(isset ($_GET['delete'])) {

    $__user->permission_check('администратори','rw');

    $a -> delete();

    echo 1;
    exit;
}



if(isset ($_POST['users_admiN_edit_submit'])){

    $__user->permission_check('администратори','rw');


    $stm = $pdo->prepare('DELETE FROM `members_admin_permissions_zapisi` WHERE `id_members_admin` = ? ');
    $stm -> bindValue(1, $a->getId(), PDO::PARAM_INT);
    $stm -> execute();









    for($q=1;$q<=sizeof($prava);$q++){

    $temp_prava = 0;
    if(isset ($_POST['user_admin_permission_'.$q])) $temp_prava = (int)$_POST['user_admin_permission_'.$q];
    if($temp_prava < 0 || $temp_prava > 2) {
        greshka('prava error');
    }

    if($temp_prava){

    $stm = $pdo->prepare('INSERT INTO `members_admin_permissions_zapisi`(`id_members_admin`, `readwrite`, `id_members_admin_permissions`)
        VALUES (:id, :prava, :id_pravilo) ');


    $stm -> bindValue(':id', $a->getId(), PDO::PARAM_INT);
    $stm -> bindValue(':prava', $temp_prava, PDO::PARAM_INT);
    $stm -> bindValue('id_pravilo', $q, PDO::PARAM_INT);
    $stm -> execute();

    unset ($temp_prava);


    }




    }






// update user info



$username = $_POST['user_admin_edit_username'];

$pass = $_POST['user_admin_edit_pass'];

$mail = $_POST['user_admin_edit_mail'];

$phone = $_POST['user_admin_edit_phone'];





$stm = $pdo->prepare('UPDATE `members_admin` SET
    `username` = :username,
    `mail` = :mail,
    `phone` = :phone
    WHERE `id` = :id ');
$stm -> bindValue(':username', $username);
$stm -> bindValue(':mail',$mail);
$stm -> bindValue(':phone', $phone);
$stm -> bindValue(':id', $a->getId(), PDO::PARAM_INT);

$stm -> execute();


if($pass){

    $stm = $pdo->prepare('UPDATE `members_admin` SET
    `password` = :pass,
    `password_cookie`= ""
    WHERE `id` = :id ');
$stm -> bindValue(':pass', sha1($pass));
$stm -> bindValue(':id', $a->getId(), PDO::PARAM_INT);

$stm -> execute();



}








po_taka_set_status('обновена информация за админ');
po_taka_redirect_to_referrer();

exit;

}








?>

<form action="ajax/users_admin_edit.php?id=<?php echo $a->getId(); ?>" method="post">

<table style="float: left;">

<tr>

        <td>
            username<br>
            <input type="text" name="user_admin_edit_username" style="margin-top:3px;" value="<?php echo $a->getUsername(); ?>">
    </td>
</tr>
<tr>

    <td>
        <div style="padding-top:10px;">password</div>
        <input type="password" name="user_admin_edit_pass" style="padding-left: 10px;width: 240px;font-size: 13px;color: #333;font-family: 'Verdana', sans-serif;border: 1px solid #666666;height: 26px;margin-top: 3px;">
    </td>


</tr>
<tr>
    <td>
        <div style="padding-top:10px;">mail</div>
        <input type="text" value="<?php echo $a->getMail(); ?>" style="margin-top: 3px;" name="user_admin_edit_mail">
</td>

</tr>
<tr>

    <td>
        <div style="padding-top:10px;">phone</div>
        <input type="text" value="<?php echo $a->getPhone(); ?>" style="margin-top: 3px;" name="user_admin_edit_phone">
    </td>



</tr>


</table>

<table style="float: left; margin-left: 30px;">

        <tr>

            <td width="300">
                &nbsp;x&nbsp;&nbsp;&nbsp;r&nbsp;&nbsp;&nbsp;rw
            </td>

        </tr>

    <tr>

<td>




    <?php foreach ($prava as $v) { ?>

    <input type="radio" value="0" name="user_admin_permission_<?php echo $v['id']; ?>" checked>
    <input type="radio" value="1" name="user_admin_permission_<?php echo $v['id']; ?>" <?php if($a->getPermission_read($v['id'])) { ?> CHECKED <?php } ?>>
    <input type="radio" value="2" name="user_admin_permission_<?php echo $v['id']; ?>" <?php if($a->getPermission_write($v['id'])) { ?> CHECKED <?php } ?>>



    <?php echo $v['name']; ?>
            <br>
                   <?php } ?>
        </td></tr>
    </table>

    <div style="clear: both;"></div>

    <button type="submit" style="margin-top: 10px;" class="button-save" name="users_admiN_edit_submit"><?php echo lang_save; ?></button>
    <button type="button" class="button-cancel" onclick="$('#admin_edit_<?php echo $a->getId(); ?>').html('');"><?php echo lang_cancel; ?></button>


</form>





<?php





} // try
catch ( Exception $e ){
    greshka($e);
}

?>
