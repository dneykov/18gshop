<?php header("content-type: text/xml");

require '../__top.php';

try {
    if (isset ($_GET['search'])) {
        $search = $_GET['search'];


        if ($search != '') {
            $temp = virtual_product_suggestion($search, (int)$_GET['vid']);

            echo $temp;
        }
    }

} catch (Exception $e) {
    greshka($e, '', true);
}