<?php
require "../__top.php";

$comment = $_POST['comment'];
$id = $_POST['id'];
$clientNumber = $_POST['clientNumber'];

$stm = $pdo->prepare("UPDATE `members` SET `admin_comment`=?,`client_no`=? WHERE `id`=?");
$stm -> bindValue(1, $comment, PDO::PARAM_STR);
$stm -> bindValue(2, $clientNumber, PDO::PARAM_STR);
$stm -> bindValue(3, (int)$id, PDO::PARAM_INT);
$stm -> execute();
?>