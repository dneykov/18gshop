<?php
require '../__top.php';


$__user->permission_check('продукти','rw');

try{
    if(!isset ($_REQUEST['id'])) greshka('ne e setnato id');

    $upgrade_group = new UpgradeGroup((int)$_REQUEST['id']);

    if(isset ($_POST['id'])){
        po_taka_update_multilanguage('edit_ime_', 'ime_', 'mode_group_option', $_POST['id']);
    }

    if(isset ($_POST['delete'])){
        $upgrade_group->delete();
        exit("1");
    }
} catch (Exception $e){
    greshka($e);
}
?>
<?php if ($_POST['id']) : ?>
    <span class="ime"><?php echo $_POST['edit_ime_' . lang_default_prefix]; ?></span>

    <?php if($__user->permission_check('продукти','rw',true,true)) { ?>
        <a href="#" style="padding-left: 10px;" onclick="edit_upgradee_group(<?php echo $_POST['id']; ?>); return false;"><?php lang_edit_e(); ?></a>
        <a href="#" onclick="delete_upgrade_group(<?php echo $_POST['id']; ?>)" style="padding-left: 0px;"><?php lang_delete_x(); ?></a>
    <?php } ?>

    <div id="edit_upgrade_group_<?php echo $_POST['id']; ?>"></div>
<?php else : ?>
    <form id="edit_form_upgrade_group_<?php echo $upgrade_group->getId(); ?>" method="post">
        <div class="input-tab-holder" style="margin-top: 10px;float: left;height: auto;overflow: hidden; width: 220px;">
            <?php
            $inputs = "";
            foreach (array_reverse($upgrade_group->getIme_all(true)) as $v) {
                echo '<div class="input-tab-trigger-' . $v['lang_name'] . '-edit-upgrade-group">' . $v['lang_name'] . '</div>';
                $inputs = $inputs . '<input class="edit_upgrade_group_ime input-tab-' . $v['lang_name'] . '-edit-upgrade-group" style=" height: 20px;width: 220px; margin-bottom: 10px;" type="text" type="text" name="edit_ime_'.$v['lang_prefix'].'" value="' . $v['name'] . '">';
            }
            ?>
        </div>

        <?php echo $inputs; ?>

        <input type="hidden" name="id" value="<?php echo $upgrade_group->getId(); ?>">
        <div class="error">Моля въведете група</div>
        <button type="button" class="button-save" onclick="update_upgrade_group(<?php echo $upgrade_group->getId(); ?>)" style="margin-bottom: 15px;"><?php echo lang_save; ?></button>
        <button type="button" class="button-cancel" onclick="edit_upgrade_group_cancel(<?php echo $upgrade_group->getId(); ?>)"><?php echo lang_cancel; ?></button>
    </form>

    <script>
        $(document).ready(function(){
            $(".input-tab-trigger-en-edit-upgrade-group").click(function(){
                $(".input-tab-bg-edit-upgrade-group").hide();
                $(".input-tab-en-edit-upgrade-group").show();
                $(".input-tab-trigger-en-edit-upgrade-group").css("background-color","#666666");
                $(".input-tab-trigger-en-edit-upgrade-group").css("color","#fff");
                $(".input-tab-trigger-bg-edit-upgrade-group").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-upgrade-group").css("color","#333");
            });

            $(".input-tab-trigger-bg-edit-upgrade-group").click(function(){
                $(".input-tab-bg-edit-upgrade-group").show();
                $(".input-tab-en-edit-upgrade-group").hide();
                $(".input-tab-trigger-en-edit-upgrade-group").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-upgrade-group").css("color","#333");
                $(".input-tab-trigger-bg-edit-upgrade-group").css("background-color","#666666");
                $(".input-tab-trigger-bg-edit-upgrade-group").css("color","#fff");
            });
        });
    </script>
<?php endif; ?>
