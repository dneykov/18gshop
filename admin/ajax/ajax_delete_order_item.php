<?php
	require "../__top.php";
	
	$item = $_POST['id'];
	$id = $_POST['orderid'];

	$items_std = $pdo->prepare("SELECT `deleted_items` FROM `orders` WHERE `id`=? LIMIT 1");
	$items_std -> bindValue(1, (int)$id, PDO::PARAM_INT);
	$items_std -> execute();
	$old_items = $items_std->fetch();
	$i = $old_items['deleted_items'];

	$items = "";

	if($i != "") $items = $i.", ";
	$items .= $item;

	$artikul = new artikul((int)$item);
	$dop_artikuli = $artikul->getAddArtikuli();
	if ($dop_artikuli != "") $items .= ", ".$dop_artikuli;

	$deletedItemsArray = explode(",", $items);
	$itm = array();
	foreach($deletedItemsArray as $k => $v){
		$itm[] = trim($v);
	}

	$other = false;
	$order = new order_admin((int)$id);
	$orderProducts=$order->getProducts();
	foreach($orderProducts as $key => $val){
		if(in_array((int)$key, $itm)){
			continue;
		}
		if(!isProductAvailable((int)$key)){
			continue;
		}	
		$other = true;	
	}

	if($other){
		$stm = $pdo->prepare("UPDATE `orders` SET `deleted_items`=? WHERE `id`=?");
		$stm -> bindValue(1, $items, PDO::PARAM_STR);
		$stm -> bindValue(2, (int)$id, PDO::PARAM_INT);
		$stm -> execute();
	} else {
		$stm = $pdo->prepare("DELETE FROM `orders` WHERE `id`=?");
		$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
		$stm -> execute();
	}
?>