<?php
require '../../__top.php';
if(isset($_POST['id'])){
	$artikul = new artikul_admin((int)$_POST['id']);
    if ($artikul->isPreorder()) return;
    $artikul->cache_update();
    $artikul->cache_search();
    $isAvaliable = $artikul->isAvaliable();
    $nowDate = date("Y-m-d");
        $tempAval = false;
        if($artikul->isAvaliable()) $tempAval = true;
        $wstm = $pdo->prepare('SELECT * FROM `warehouse_products` WHERE `product_id` = ?');
        $wstm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
        $wstm -> execute();
        if($wstm -> rowCount() > 0){
            $here = false;
            $artikul_tag_ids = array();
            foreach ($wstm -> fetchAll() as $v) {
                $tempCode = $v['code'];
                $tempTagId = $v['tag_id'];

                $stmTagCheck = $pdo->prepare('SELECT id FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? LIMIT 1');
                $stmTagCheck -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                $stmTagCheck -> bindValue(2, $tempTagId, PDO::PARAM_INT);
                $stmTagCheck -> execute();

                $type = $v['type'];
                $stm = $pdo->prepare('SELECT id FROM `warehouse` WHERE `code` = ? LIMIT 1');
                $stm -> bindValue(1, (int)$tempCode, PDO::PARAM_INT);
                $stm -> execute();
                if($stm -> rowCount() > 0) {
                    $artikul_tag_ids[] = $tempTagId;
                    $here = true;
                } else if(is_null($tempCode) && $stmTagCheck -> rowCount() > 0) {
                    $artikul_tag_ids[] = $tempTagId;
                    $here = true;
                }
            }
            if($type == 2){
                if($artikul->getTaggroup_dropdown() != null){
                    foreach ($artikul->getTaggroup_dropdown() as $td) {
                        foreach($td->getEtiketi() as $t){
                            $stm = $pdo -> prepare('DELETE FROM `etiketi_zapisi` WHERE `id_artikul` = ? AND `id_etiket` = ? ');
                            $stm -> bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                            $stm -> bindValue(2, $t->getId(), PDO::PARAM_INT);
                            $stm -> execute();
                        }
                    }
                }
                foreach ($artikul_tag_ids as $etk) {
                    $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
                    $stm -> bindValue(1, $etk, PDO::PARAM_INT);
                    $stm -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm -> execute();
                }
            }
            if($here) {
                if(count($artikul_tag_ids) > 0) {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd -> bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd -> bindValue(2, 1, PDO::PARAM_INT);
                    $upd -> bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd -> execute();

                    $stm_enable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                    $stm_enable_group_product -> bindValue(1, 1, PDO::PARAM_STR);
                    $stm_enable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm_enable_group_product -> execute();

                    $isAvaliable = true;
                } else {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd->bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd->bindValue(2, 0, PDO::PARAM_INT);
                    $upd->bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd->execute();

                    $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                    $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
                    $stm_disable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm_disable_group_product -> execute();

                    $isAvaliable = false;
                }
            } else {
                if(!$artikul->isPreorder()) {
                    $upd = $pdo->prepare('UPDATE `artikuli` SET `check_date`= ?, `isAvaliable`=? WHERE `id` = ? LIMIT 1');
                    $upd -> bindValue(1, $nowDate, PDO::PARAM_STR);
                    $upd -> bindValue(2, 0, PDO::PARAM_INT);
                    $upd -> bindValue(3, $artikul->getId(), PDO::PARAM_INT);
                    $upd -> execute();

                    $stm_disable_group_product = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` IN (SELECT group_id from group_products_zapisi where artikul_id = ?)');
                    $stm_disable_group_product -> bindValue(1, 0, PDO::PARAM_STR);
                    $stm_disable_group_product -> bindValue(2, $artikul->getId(), PDO::PARAM_INT);
                    $stm_disable_group_product -> execute();

                    $isAvaliable = false;
                }
            }
        }
}