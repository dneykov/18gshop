<?php

require '../__top.php';

$__user->permission_check('продукти','rw');


if(!isset ($_REQUEST['id'])) greshka('ne e poso4eno id');

$id = (int)$_REQUEST['id'];

$a = new etiket_grupa_admin($id);

$stm_check = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? AND `select_menu` = ?');
$stm_check -> bindValue(1, $a->getId_vid(), PDO::PARAM_INT);
$stm_check -> bindValue(2, 1, PDO::PARAM_INT);
$stm_check -> execute();
$select_menu_check = $stm_check->fetch();

if(isset ($_POST['delete'])){

    $pdo->beginTransaction();

    $stm = $pdo->prepare('DELETE FROM `etiketi_grupa` WHERE `id` = ? ');
    $stm -> bindValue(1, $a->getId(), PDO::PARAM_INT);
    $stm -> execute();

    $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id_vid` = ? ');
    $stm -> bindValue(1, $a->getId_vid(), PDO::PARAM_INT);
    $stm -> execute();

    if($stm->rowCount() > 0 ) foreach ($stm->fetchAll() as $v) {

        $art = new artikul_admin($v);
        $art->cache_update();
        $art->cache_search();

        unset($art);

        }



    




    $pdo->commit();
    echo 1;
    exit;





}



if(isset ($_POST['edit_names'])){


   po_taka_update_multilanguage('names_', 'ime_', 'etiketi_grupa', $a->getId());

   $stm = $pdo->prepare('UPDATE `etiketi_grupa` SET `select_menu` = ? WHERE `id` = ?');
   
   if(isset ($_POST['model_edit_checkbox'])) $temp = (int)$_POST['model_edit_checkbox'];
   else $temp = 0;

   $stm -> bindValue(1, $temp, PDO::PARAM_INT);
   $stm -> bindValue(2, $a->getId(), PDO::PARAM_INT);

   $stm -> execute();


   ?>

<meta http-equiv="Refresh" content="0;URL=<?php echo $_SERVER['HTTP_REFERER']; ?>" />
    <?php
   

    exit;
}








?>


<form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="formName" method="post" style="margin-bottom:10px;">
    <div class="input-tab-holder" style="margin-top: 10px;float: left;height: auto;overflow: hidden;">
        <?php 
            $inputs = "";
                foreach (array_reverse($a->getIme_all(true)) as $v) {
                     echo '<div class="input-tab-trigger-' . $v['lang_name'] . '-edit-taggroup">' . $v['lang_name'] . '</div>'; 
                     $inputs = $inputs . '<input class="new_taggrupa_ime input-tab-' . $v['lang_name'] . '-edit-taggroup" type="text" name="names_'.$v['lang_prefix'].'" value="' . htmlspecialchars($v['name']) . '">';
                }
        ?>
    </div>

    <?php echo $inputs; ?>

    <?php if((count($select_menu_check) && $select_menu_check['id'] == $id) || $select_menu_check === false) : ?>
        <input type="checkbox" name="model_edit_checkbox" class="tag_option" id="priceOption" onclick="if(this.checked) {document.formName.specOption.checked=false;}" value="1" <?php if($a->getSelect_menu() == 1) { ?> checked <?php } ?> style="line-height:20px;vertical-align:middle;">
        <span style="color:orange;line-height:10px;display:inline-block;"><label style="font-size: 13px;" for="priceOption">опция цена</label></span>
        <br>
        <br>
    <?php else: ?>
        <br>
        <br>
    <?php endif; ?>
    <input type="hidden" name="id" value="<?php echo $a->getId(); ?>">
    <button type="submit" class="button-save" name="edit_names" style="margin-bottom: 15px;"><?php echo lang_save; ?></button>
    <button type="button" class="button-cancel" onclick="edit_taggroup_cancel(<?php echo $a->getId(); ?>);"><?php echo lang_cancel; ?></button>
</form>

     <script>
        $(document).ready(function(){

            $(".input-tab-trigger-ro-edit-taggroup").click(function(){
                $(".input-tab-bg-edit-taggroup").hide();
                $(".input-tab-en-edit-taggroup").hide();
                $(".input-tab-ro-edit-taggroup").show();
                $(".input-tab-trigger-ro-edit-taggroup").css("background-color","#666666");
                $(".input-tab-trigger-ro-edit-taggroup").css("color","#fff");
                $(".input-tab-trigger-bg-edit-taggroup").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-taggroup").css("color","#333");
                $(".input-tab-trigger-en-edit-taggroup").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-taggroup").css("color","#333");
            });

            $(".input-tab-trigger-en-edit-taggroup").click(function(){
                $(".input-tab-bg-edit-taggroup").hide();
                $(".input-tab-ro-edit-taggroup").hide();
                $(".input-tab-en-edit-taggroup").show();
                $(".input-tab-trigger-en-edit-taggroup").css("background-color","#666666");
                $(".input-tab-trigger-en-edit-taggroup").css("color","#fff");
                $(".input-tab-trigger-bg-edit-taggroup").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-taggroup").css("color","#333");
                $(".input-tab-trigger-ro-edit-taggroup").css("background-color","#fff");
                $(".input-tab-trigger-ro-edit-taggroup").css("color","#333");
            });

            $(".input-tab-trigger-bg-edit-taggroup").click(function(){
                $(".input-tab-bg-edit-taggroup").show();
                $(".input-tab-en-edit-taggroup").hide();
                $(".input-tab-ro-edit-taggroup").hide();
                $(".input-tab-trigger-en-edit-taggroup").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-taggroup").css("color","#333");
                $(".input-tab-trigger-ro-edit-taggroup").css("background-color","#fff");
                $(".input-tab-trigger-ro-edit-taggroup").css("color","#333");
                $(".input-tab-trigger-bg-edit-taggroup").css("background-color","#666666");
                $(".input-tab-trigger-bg-edit-taggroup").css("color","#fff");
            });

        });
    </script>