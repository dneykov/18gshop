<?php

require '../__top.php';


$__user->permission_check('продукти','rw');


try{
    
    
    
    if(!isset ($_REQUEST['id'])) {
        greshka('ne e poso4eno id za tag_edit');
    }

    $id = (int)$_REQUEST['id'];
		
    $a = new etiket_admin($id);
    


    if(isset ($_REQUEST['delete'])){


        $pdo-> beginTransaction();

        $stm = $pdo->prepare('DELETE FROM `etiketi` WHERE `id` = ? LIMIT 1 ');

        $stm -> bindValue(1, $a->getId(), PDO::PARAM_INT );

        $stm -> execute();


		$stm = $pdo->prepare('DELETE FROM `etiketi_zapisi` WHERE `id_etiket` = ? ');

        $stm -> bindValue(1, $a->getId(), PDO::PARAM_INT );

        $stm -> execute();



        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cache_etiketi` LIKE ( :like ) ');

        $stm -> bindValue(':like', '%<etiket>'.$a->getId().'</etiket>%' );

        $stm -> execute();

        if($stm->rowCount() > 0 ) foreach ($stm->fetchAll() as $v) {

            $temp = new artikul_admin($v);
            	
            $temp -> cache_update();
            $temp -> cache_search();

            unset($temp);

            }


        












        $pdo->commit();

        exit;



        
    }





    
    
    if(isset ($_POST['id'], $_POST['tag_edit'])){

        $pdo-> beginTransaction();

        po_taka_update_multilanguage('name_', 'ime_', 'etiketi', $a->getId());

        

        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cache_etiketi` LIKE ( :like ) ');

        $stm -> bindValue(':like', '%<etiket>'.$a->getId().'</etiket>%' );

        $stm -> execute();

        if($stm->rowCount() > 0 ) foreach ($stm->fetchAll() as $v) {

            $temp = new artikul_admin($v);
            $temp -> cache_update();
            $temp -> cache_search();

            unset($temp);

            }



        $pdo->commit();

        po_taka_redirect_to_referrer();
        exit;

        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
} catch (Exception $e){
    greshka($e);
}


?>

<form action="<?php echo url_admin.'ajax/tag_edit.php'; ?>" method="post">
    <div class="input-tab-holder" style="margin-top: 10px;float: left;height: auto;overflow: hidden;">
        <?php 
            $inputs = "";
                foreach (array_reverse($a->getIme_all(true)) as $v) {
                     echo '<div class="input-tab-trigger-' . $v['lang_name'] . '-edit-tag">' . $v['lang_name'] . '</div>'; 
                     $inputs = $inputs . '<input class="input-tab-' . $v['lang_name'] . '-edit-tag" type="text" name="name_'.$v['lang_prefix'].'" value="' . htmlspecialchars($v['name']) . '">';
                }
        ?>
    </div>

    <?php echo $inputs; ?>

    <input type="hidden" name="id" value="<?php echo $a->getId(); ?>">
    <button type="submit" name="tag_edit" class="button-save" style="margin-bottom: 15px; margin-top: 15px;"><?php echo lang_save; ?></button>
    <button type="button" class="button-cancel" onclick="cancel_tag_edit(<?php echo $a->getId(); ?>);"><?php echo lang_cancel; ?></button>
</form>

     <script>
        $(document).ready(function(){

            $(".input-tab-trigger-ro-edit-tag").click(function(){
                $(".input-tab-bg-edit-tag").hide();
                $(".input-tab-en-edit-tag").hide();
                $(".input-tab-ro-edit-tag").show();
                $(".input-tab-trigger-ro-edit-tag").css("background-color","#666666");
                $(".input-tab-trigger-ro-edit-tag").css("color","#fff");
                $(".input-tab-trigger-bg-edit-tag").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-tag").css("color","#333");
                $(".input-tab-trigger-en-edit-tag").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-tag").css("color","#333");
            });

            $(".input-tab-trigger-en-edit-tag").click(function(){
                $(".input-tab-bg-edit-tag").hide();
                $(".input-tab-ro-edit-tag").hide();
                $(".input-tab-en-edit-tag").show();
                $(".input-tab-trigger-en-edit-tag").css("background-color","#666666");
                $(".input-tab-trigger-en-edit-tag").css("color","#fff");
                $(".input-tab-trigger-bg-edit-tag").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-tag").css("color","#333");
                $(".input-tab-trigger-ro-edit-tag").css("background-color","#fff");
                $(".input-tab-trigger-ro-edit-tag").css("color","#333");
            });

            $(".input-tab-trigger-bg-edit-tag").click(function(){
                $(".input-tab-bg-edit-tag").show();
                $(".input-tab-en-edit-tag").hide();
                $(".input-tab-ro-edit-tag").hide();
                $(".input-tab-trigger-en-edit-tag").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-tag").css("color","#333");
                $(".input-tab-trigger-ro-edit-tag").css("background-color","#fff");
                $(".input-tab-trigger-ro-edit-tag").css("color","#333");
                $(".input-tab-trigger-bg-edit-tag").css("background-color","#666666");
                $(".input-tab-trigger-bg-edit-tag").css("color","#fff");
            });

        });
    </script>