<?php
require '../__top.php';


$__user->permission_check('продукти','rw');

try{

if(!isset ($_REQUEST['id'])) greshka('ne e setnato id');

$a = new vid_admin((int)$_REQUEST['id']);







if(isset ($_POST['mode_edit_submit'])){
    $pdo->beginTransaction();

    po_taka_update_multilanguage('mode_name_', 'ime_', 'vid', $a->getId());

    $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id_vid` = ? ');
    $stm -> bindValue(1, $a->getId(), PDO::PARAM_INT);
    $stm -> execute();

    if($stm->rowCount() > 0 ) foreach ($stm->fetchAll() as $v) {
        $temp = new artikul_admin($v);
        $temp->cache_update();
        $temp->cache_search();
        unset($temp);
    }

    $pdo->commit();

    po_taka_redirect_to_referrer();
    exit;
}




if(isset ($_POST['delete'])){
    $a->delete();
    exit("1");
}











} catch (Exception $e){
    greshka($e);
}

?>

<form action="<?php echo url_admin.'ajax/mode_edit.php'; ?>" method="post">
    <div class="input-tab-holder" style="margin-top: 10px;float: left;height: auto;overflow: hidden;">
        <?php 
            $inputs = "";
                foreach (array_reverse($a->getIme_all(true)) as $v) {
                     echo '<div class="input-tab-trigger-' . $v['lang_name'] . '-edit-mode">' . $v['lang_name'] . '</div>'; 
                     $inputs = $inputs . '<input class="input-tab-' . $v['lang_name'] . '-edit-mode" type="text" name="mode_name_'.$v['lang_prefix'].'" value="' . $v['name'] . '">';
                }
        ?>
    </div>

    <?php echo $inputs; ?>

    <input type="hidden" name="id" value="<?php echo $a->getId(); ?>">
    <button type="submit" class="button-save" name="mode_edit_submit" style="margin-bottom: 15px; margin-top: 15px;"><?php echo lang_save; ?></button>

    <button type="button" class="button-cancel" onclick="edit_vid_cancel(<?php echo $a->getId(); ?>)"><?php echo lang_cancel; ?></button>
</form>

     <script>
        $(document).ready(function(){

            $(".input-tab-trigger-ro-edit-mode").click(function(){
                $(".input-tab-bg-edit-mode").hide();
                $(".input-tab-en-edit-mode").hide();
                $(".input-tab-ro-edit-mode").show();
                $(".input-tab-trigger-ro-edit-mode").css("background-color","#666666");
                $(".input-tab-trigger-ro-edit-mode").css("color","#fff");
                $(".input-tab-trigger-bg-edit-mode").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-mode").css("color","#333");
                $(".input-tab-trigger-en-edit-mode").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-mode").css("color","#333");
            });

            $(".input-tab-trigger-en-edit-mode").click(function(){
                $(".input-tab-bg-edit-mode").hide();
                $(".input-tab-ro-edit-mode").hide();
                $(".input-tab-en-edit-mode").show();
                $(".input-tab-trigger-en-edit-mode").css("background-color","#666666");
                $(".input-tab-trigger-en-edit-mode").css("color","#fff");
                $(".input-tab-trigger-bg-edit-mode").css("background-color","#fff");
                $(".input-tab-trigger-bg-edit-mode").css("color","#333");
                $(".input-tab-trigger-ro-edit-mode").css("background-color","#fff");
                $(".input-tab-trigger-ro-edit-mode").css("color","#333");
            });

            $(".input-tab-trigger-bg-edit-mode").click(function(){
                $(".input-tab-bg-edit-mode").show();
                $(".input-tab-en-edit-mode").hide();
                $(".input-tab-ro-edit-mode").hide();
                $(".input-tab-trigger-en-edit-mode").css("background-color","#fff");
                $(".input-tab-trigger-en-edit-mode").css("color","#333");
                $(".input-tab-trigger-ro-edit-mode").css("background-color","#fff");
                $(".input-tab-trigger-ro-edit-mode").css("color","#333");
                $(".input-tab-trigger-bg-edit-mode").css("background-color","#666666");
                $(".input-tab-trigger-bg-edit-mode").css("color","#fff");
            });

        });
    </script>