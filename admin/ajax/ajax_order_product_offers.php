<?php
require "../__top.php";

$comment = $_POST['comment'];
$product_ids = $_POST['product_ids'];
$id = $_POST['id'];

$stm = $pdo->prepare("UPDATE `orders` SET `admin_comment`=?,`products_offered`=?,`confirmation_sent`=? WHERE `id`=?");
$stm->bindValue(1, $comment, PDO::PARAM_STR);
$stm->bindValue(2, $product_ids, PDO::PARAM_STR);
$stm->bindValue(3, 1, PDO::PARAM_INT);
$stm->bindValue(4, (int)$id, PDO::PARAM_INT);
$stm->execute();

$order = new order_admin((int)$id);
$delivery = $order->getContactInformation()['delivery'];
$orderProducts = $order->getProducts();
$deletedItems = $order->getDeletedItems();

$subject = lang_order_approved . $_SERVER['SERVER_NAME'];
$msg = lang_order_approved_msg1;
if (!empty($order->getAdminComment())) {
    $msg .= '<br /><br />' . nl2br($order->getAdminComment());
}
$msg = $msg . '<br/ ><br /><table style="min-width: 770px; font-family:Verdana;font-size: 10pt;border-collapse: collapse;" cellpadding="10">
					<tr>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_kod . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_art . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;"></th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_opt . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;width: 66px;">' . lang_order_msg_br . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_pr . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_deposit . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_preorder . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_total . '</th>
					</tr>';

$all = $order->getPreorderCost();
if (isset($orderProducts)) {
    foreach ($orderProducts as $key => $product) {
        if ($key == "total_additional_delivery") continue;

        if (in_array((int)$key, $deletedItems)) {
            $all = $all - $product['price'];
            continue;
        }
        if ($product['type'] == "product") {
            $artikul = new artikul((int)$key);
            $option = '';
            $option = '<table>';
            if (isset($product['options'])) {
                foreach ($product['options'] as $optionkey => $optionVal) {
                    $expl_arr = explode(",", $optionVal);

                    foreach ($expl_arr as $keyy => $vall) {
                        if ($keyy == 0) {
                            $artikulid = (int)$vall;
                        }
                        if ($keyy == 1 && $artikulid == $key) {
                            $option .= '<tr>';
                            $option .= '<td>';
                            if ($artikul->getTaggroup_dropdown()) {
                                foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                    $option .= $gr->getIme() . ":";
                                }
                            }
                            $option .= '</td>';
                            $option .= '<td>';
                            $option .= $vall;
                            $option .= '</td>';
                            $option .= '</tr>';
                        }
                    }
                }
            }

            if (isset($product['upgrade_option'])) {
                foreach ($product['upgrade_option'] as $upgrade_option) {
                    $option .= '<tr>';
                    $option .= '<td>';
                    $option .= $upgrade_option['upgrade_name'] . ":";
                    $option .= '</td>';
                    $option .= '<td>';
                    $option .= $upgrade_option['name'];
                    $option .= '</td>';
                    $option .= '</tr>';
                }
            }
            $option .= '</table>';
            $msg = $msg . '<tr ' . (!$artikul->isPreorder() ? 'style="border-bottom: 1px solid #e6e6e6;"' : "") . '>
                                <td >
                                    ' . $artikul->getKod() . '
                                </td>
                                <td>
                                    <div id="thumbs1">
                                        <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
                                                <a href="' . url . 'index.php?id=' . $artikul->getId() . '"><img src="' . url . $artikul->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
                                        </div>	
                                    </div></td>
                                
                                <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $artikul->getId() . '">' . stripslashes($artikul->getIme_marka()) . '<br/>' . stripslashes($artikul->getIme()) . '</a></td>

                                <td >' . $option . '</td>

                                <td >
                                    ' . $product['count'] . '												
                                </td>
                                <td><span id="price' . $artikul->getId() . '" >' . number_format(($product['price'] / CURRENCY_RATE), 2, '.', '') . '</span> ' . lang_currency_append . '</td>
                                <td>' . (isset($product['preorder']) ? number_format(($product['preorder']['deposit_price'] * $product['count']) / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append : "") . '</td>
                                <td>' . (isset($product['preorder']) ? number_format((($product['price'] - $product['preorder']['deposit_price']) * $product['count']) / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append : "") . '</td>
                                <td ><span id="pricetotal' . $artikul->getId() . '" >' . number_format((($artikul->isPreorder() ? $product['preorder']['deposit_price'] : ($product['price'])) * (int)$product['count']) / CURRENCY_RATE, 2, '.', '') . ' </span> ' . lang_currency_append . '</td>											
                            </tr>';

            if ($artikul->isPreorder()) {
                $msg = $msg . '<tr style="border-bottom: 1px solid #e6e6e6;"><td colspan="9"><span style="color: #FB671F;margin-top: -20px;margin-left: 60px;max-width: 690px;width: 100%;display: inline-block;">' . $artikul->getPreorderDescription() . '</span></td></tr>';
            }

            $dop_artikuli = $artikul->getAddArtikuli();
            if ($dop_artikuli != "") {
                $dop_art_arr = explode(",", $dop_artikuli);
                foreach ($dop_art_arr as $dart) {
                    $dr = new artikul((int)$dart);

                    $option = '<table>';
                    $n = 0;
                    foreach ($product['options'] as $optionkey => $optionVal) {
                        $expl_arr = explode(",", $optionVal);
                        $artikulid = 0;
                        foreach ($expl_arr as $keyy => $vall) {
                            if ($keyy == 0 && $vall == $dr->getId()) {
                                $artikulid = (int)$vall;
                            }
                            if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                $option .= '<tr>';
                                $option .= '<td>';
                                if ($dr->getTaggroup_dropdown()) {
                                    foreach ($dr->getTaggroup_dropdown() as $gr) {
                                        $option .= $gr->getIme() . ":";
                                    }
                                }
                                $option .= '</td>';
                                $option .= '<td>';
                                $option .= $vall;
                                $option .= '</td>';
                                $option .= '</tr>';
                                $n = 1;
                            }
                        }
                    }
                    $option .= '</table>';

                    $msg = $msg . '<tr>
                                       <td >
                                            ' . $dr->getKod() . '												
                                        </td>
                                        <td>
                                        <div id="thumbs1">
                                            <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
                                                <a href="' . url . 'index.php?id=' . $dr->getId() . '"><img src="' . url . $dr->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
                                            </div>		
                                        </div>
                                        </td>
                                        <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $dr->getId() . '">' . stripslashes($dr->getIme_marka()) . '<br/>' . stripslashes($dr->getIme()) . '</a></td>
                                        <td >' . $option . '</td>
                                        <td >1</td>
                                        <td></td>
                                        <td></td>											
                                </tr>';
                }
            }

        } else if ($product['type'] == "group") {
            $group = new paket((int)$key);
            $msg = $msg . '<tr>
                                <td ></td>
                                <td>
                                <div id="thumbs1">
                                    <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
                                            <a href="' . url . 'index.php?gid=' . $group->getId() . '"><img src="' . url . $group->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
                                    </div>		
                                </div>
                                </td>
                                            
                                <td ><a style="font-size: 10pt;" href="' . url . 'index.php?gid=' . $group->getId() . '">' . stripslashes($group->getName()) . '</a></td>

                                <td ></td>

                                <td >
                                    ' . $product['count'] . '												
                                </td>
                                <td><span id="price' . $group->getId() . '" >' . number_format($group->getPrice() / CURRENCY_RATE, 2, '.', '') . '</span> ' . lang_currency_append . '</td>
                                <td></td>
                                <td></td>
                                <td ><span id="pricetotal' . $group->getId() . '" >' . number_format(($group->getPrice() * (int)$product['count']) / CURRENCY_RATE, 2, '.', '') . ' </span> ' . lang_currency_append . '
                                </td>											
                            </tr>';
            $option = '';

            $dop_artikuli = $group->getProducts();
            foreach ($dop_artikuli as $dart) {
                $dr = new artikul((int)$dart);
                $option = '<table>';
                $artikulid = 0;
                $n = 0;
                foreach ($product['options'] as $optionkey => $optionVal) {
                    $expl_arr = explode(",", $optionVal);
                    foreach ($expl_arr as $keyy => $vall) {
                        if ($keyy == 0 && $vall == $dr->getId()) {
                            $artikulid = (int)$vall;
                        }
                        if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                            $option .= '<tr>';
                            $option .= '<td>';
                            if ($dr->getTaggroup_dropdown()) {
                                foreach ($dr->getTaggroup_dropdown() as $gr) {
                                    $option .= $gr->getIme() . ":";
                                }
                            }
                            $option .= '</td>';
                            $option .= '<td>';
                            $option .= $vall;
                            $option .= '</td>';
                            $option .= '</tr>';
                            $n = 1;
                        }
                    }
                }

                $option .= '</table>';

                $msg = $msg . '<tr>
                                   <td >
                                        ' . $dr->getKod() . '												
                                    </td>
                                    <td>
                                    <div id="thumbs1">
                                        <div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
                                            <a href="' . url . 'index.php?id=' . $dr->getId() . '"><img src="' . url . $dr->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
                                        </div>		
                                    </div>
                                    </td>
                                    <td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $dr->getId() . '">' . stripslashes($dr->getIme_marka()) . '<br/>' . stripslashes($dr->getIme()) . '</a></td>
                                    <td >' . $option . '</td>
                                    <td >' . $product['count'] . '</td>
                                    <td></td>
                                    <td></td>											
	                            </tr>';
            }

        }
    }
}

$msg = $msg . '</table><br><br><br>';

switch ($order->getPaymentType()) {
    case "cash":
        $paymentType = cash_on_delivery;
        break;
    case "bank":
        $paymentType = bank_transfer;
        break;
    default:
        $paymentType = cash_on_delivery;
        break;
}

$msg = $msg . '<span style=" padding-left: 10px;">' . payment_type . ' <span style="color: #FB671F;">' . $paymentType . '</span></span><br />';
$msg = $msg . '<br />';

$msg .= '<span style=" padding-left: 10px;">';
if ($delivery === 'pickup') {
    $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . pickup_delivery . '</span> ';
} else if ($delivery == 0) {
    $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . lang_free_shipping . '.</span> ';
} else {
    $tmp = number_format($delivery / CURRENCY_RATE, 2, '.', '');

    $msg = $msg . lang_order_deliv . ': <span style="color: #FB671F;">' . $tmp . ' ' . lang_currency_append . '</span> ';
}

switch ($order->getPaymentType()) {
    case "cash":
        $msg = $msg . '</span><br /><div style="background: #FB671F; min-height: 20px; max-height: 20px; display: inline-block; line-height: 20px; color: #fff; padding-left: 10px; padding-right: 10px; margin-top: 10px; min-width: 300px; width: 300px;">' . lang_order_msg_total2 . ' <span style="font-weight: bold;">' . number_format($all / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></div><br><br>' . lang_order_tmp_cash . '<br/>' . lang_order_msg3_cash;
        break;
    case "bank":
        $msg = $msg . '</span><br /><div style="background: #FB671F; min-height: 20px; max-height: 20px; display: inline-block; line-height: 20px; color: #fff; padding-left: 10px; padding-right: 10px; margin-top: 10px; min-width: 300px; width: 300px;">' . lang_order_msg_total2 . ' <span style="font-weight: bold;">' . number_format($all / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></div><br><br>' . lang_order_tmp_bank . '<br/>' . lang_order_tmp_bank_details . '<br/>';
        $msg = $msg . iban . " " . $_POST['iban'] . '<br />';
        $msg = $msg . nl2br($_POST['bank_details']) . '<br /><br />';
        $msg = $msg . lang_order_msg3_bank;
        break;
    default:
        $msg = $msg . '</span><br /><div style="background: #FB671F; min-height: 20px; max-height: 20px; display: inline-block; line-height: 20px; color: #fff; padding-left: 10px; padding-right: 10px; margin-top: 10px; min-width: 300px; width: 300px;">' . lang_order_msg_total2 . ' <span style="font-weight: bold;">' . number_format($all / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></div><br><br>' . lang_order_tmp_cash . '<br/>' . lang_order_msg3_cash;
        break;
}

$strProductsOffered = $order->getProductsOffered();
if ($strProductsOffered != "") {
    $msg = $msg . '<br/><br/><br/>' . lang_order_approved_msg2;
    $msg = $msg . '<br/ ><br/ ><table style="min-width: 500px; font-family:Verdana,serif;font-size: 10pt;border-collapse: collapse;">
					<tr>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_art . '</th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;"></th>
						<th style="text-align: left;font-weight:normal;padding:10px;background: #e6e6e6;">' . lang_order_msg_pr . '</th>
					</tr>';
    $ArtikuliArray = explode(",", $strProductsOffered);
    foreach ($ArtikuliArray as $ar) {
        $ar = (int)$ar;
        $adda = new artikul($ar);

        if (!empty($adda->getId())) {
            $msg = $msg . '<tr ' . (!$adda->isPreorder() ? 'style="border-bottom: 1px solid #e6e6e6;width: 80px;"' : 'style="width: 80px;"') . '>							
									<td>
										<div id="thumbs1">
											<div class="thumbss1" style="width:78px; height:52px; text-align:center;">				
													<a href="' . url . 'index.php?id=' . $adda->getId() . '"><img src="' . url . $adda->getKartinka_t() . '" border="0" style="margin:2px 2px 0px; max-width:78px; max-height:52px;"/></a>
											</div>	
										</div></td>								
									<td ><a style="font-size: 10pt;" href="' . url . 'index.php?id=' . $adda->getId() . '">' . stripslashes($adda->getIme_marka()) . '<br/>' . stripslashes($adda->getIme()) . '</a></td>
									<td><span id="price' . $artikul->getId() . '" >' . number_format((($adda->getCena_promo() ?: $adda->getCena()) / CURRENCY_RATE), 2, '.', '') . '</span> ' . lang_currency_append . '</td>																	
								</tr>';
        }
    }
    $msg .= '</table>';
}

$msg .= '<br/><br/><br/>' . lang_order_preorde_desc;

$mail = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPDebug = 0;
$mail->SMTPAuth = true;
$mail->SMTPKeepAlive = true;
$mail->SMTPSecure = 'tls';
$mail->Host = MAIL_HOST;
$mail->Port = 587;
$mail->Username = MAIL_USERNAME;
$mail->Password = MAIL_PASSWORD;
$mail->CharSet = "utf-8";
$mail->From = MAIL_FROM;
$mail->FromName = MAIL_FROM_NAME;
$mail->WordWrap = 50;
$mail->IsHTML(TRUE);
$mail->AddAddress($order->getContactInformation()['email']);
$mail->Subject = $subject;
$mail->Body = $msg;
$mail->AltBody = htmlspecialchars($msg);
$mail->Send();