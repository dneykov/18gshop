<?php header("content-type: text/xml");

require '../__top.php';

try {
    if (isset ($_GET['search'])) {
        $search = $_GET['search'];


        if ($search != '') {
            $temp = po_taka_search_autocomplete_artikuli_admin($search, url_admin . 'product.php?id=', true);

            echo $temp;
        }
    }

} catch (Exception $e) {
    greshka($e, '', true);
}