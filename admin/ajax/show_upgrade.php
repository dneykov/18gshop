<?php
require '../__top.php';


$__user->permission_check('продукти','rw');

try{

    if(!isset ($_REQUEST['id'])) greshka('ne e setnato id');

    $a = new vid_admin((int)$_GET['id']);

    $stm = $pdo->prepare('SELECT * FROM `mode_group_option` WHERE `mode_id` = ? ORDER BY `id`');
    $stm -> bindValue(1, $_GET['id'], PDO::PARAM_INT);
    $stm->execute();

    if( $stm->rowCount() == 0 ) {
        $upgrade_groups = [];
    } else {
        foreach ($stm->fetchAll() as $v) {
            $upgrade_groups[] = new UpgradeGroup($v);
        }
        unset($v);
    }
} catch (Exception $e){
    greshka($e);
}

?>
<div style="height: 20px; background: #ff9207; color: #fff; padding-left: 10px; font-size: 13px;">upgrade <span style="cursor: pointer; display:inline-block; color: #fff;float: right; font-size: 8pt;padding-right: 5px;line-height: 20px;" onclick="hide_upgrade('<?php echo $_GET['id']; ?>');">X</span></div>
<div id="upgrade_groups" style=" font-size: 13px; color: #666666;padding: 5px 10px;">
    <?php foreach ($upgrade_groups as $upgrade_group) : ?>
        <span class="upgrade_group_<?php echo $upgrade_group->getId(); ?>">
            <span class="ime"><?php echo $upgrade_group->getIme(); ?></span>

            <?php if($__user->permission_check('продукти','rw',true,true)) { ?>
                <a href="#" style="padding-left: 10px;" onclick="edit_upgrade_group(<?php echo $upgrade_group->getId(); ?>); return false;"><?php lang_edit_e(); ?></a>
                <a href="#" onclick="delete_upgrade_group(<?php echo $upgrade_group->getId(); ?>)" style="padding-left: 0px;"><?php lang_delete_x(); ?></a>
            <?php } ?>

            <div id="edit_upgrade_group_<?php echo $upgrade_group->getId(); ?>"></div>
        </span>
    <?php endforeach; ?>
</div>
<form id="upgrade_form" style="display: none;" action="<?php echo url_admin.'ajax/mode_edit.php'; ?>" method="post">

    <div class="input-tab-holder" style="margin-top: 10px;float: left;height: auto;overflow: hidden; width: 230px;">
        <?php
        $inputs = "";
        foreach (array_reverse($__languages) as $key => $v) {
            echo '<div class="input-tab-trigger-' . $v->getName() . '-edit-mode">' . $v->getName() . '</div>';
            $inputs = $inputs . '<input class="new_upgrade_group_ime input-tab-' . $v->getName() . '-edit-mode" style=" height: 20px;margin-left: 10px;margin-right: 10px;width: 220px; margin-bottom: 10px;" type="text" name="ime_'. $v->getPrefix().'" value="">';
        }
        ?>
    </div>

    <?php echo $inputs; ?>

    <input type="hidden" name="mode" value="<?php echo $a->getId(); ?>">
    <div class="error" style="margin-left: 10px;">Моля въведете група</div>
    <button type="button" class="button-save" onclick="create_upgrade_group();" name="mode_edit_submit" style="margin-bottom: 15px; margin-left: 10px;"><?php echo lang_save; ?></button>

    <button type="button" class="button-cancel" onclick="cancel_upgrade_group();"><?php echo lang_cancel; ?></button>
</form>

<a href="#" id="btn_upgrade_button" onclick="add_upgrade_group();return false;" style="display: inline-block; color: #fff; background: #ff9207; margin-left: 10px; margin-bottom: 10px; margin-top: 10px; height: 20px; padding-right: 10px; line-height: 18px;">+ upgrade group</a>

<script>
    $(document).ready(function(){
        $(".input-tab-bg-edit-mode").keyup(function () {
            var val_bg = $(this).val();
            $('.input-tab-en-edit-mode').val(val_bg);
            $('.input-tab-ro-edit-mode').val(val_bg);
        });

        $(".input-tab-trigger-ro-edit-mode").click(function(){
            $(".input-tab-bg-edit-mode").hide();
            $(".input-tab-en-edit-mode").hide();
            $(".input-tab-ro-edit-mode").show();
            $(".input-tab-trigger-ro-edit-mode").css("background-color","#666666");
            $(".input-tab-trigger-ro-edit-mode").css("color","#fff");
            $(".input-tab-trigger-bg-edit-mode").css("background-color","#fff");
            $(".input-tab-trigger-bg-edit-mode").css("color","#333");
            $(".input-tab-trigger-en-edit-mode").css("background-color","#fff");
            $(".input-tab-trigger-en-edit-mode").css("color","#333");
        });

        $(".input-tab-trigger-en-edit-mode").click(function(){
            $(".input-tab-bg-edit-mode").hide();
            $(".input-tab-ro-edit-mode").hide();
            $(".input-tab-en-edit-mode").show();
            $(".input-tab-trigger-en-edit-mode").css("background-color","#666666");
            $(".input-tab-trigger-en-edit-mode").css("color","#fff");
            $(".input-tab-trigger-bg-edit-mode").css("background-color","#fff");
            $(".input-tab-trigger-bg-edit-mode").css("color","#333");
            $(".input-tab-trigger-ro-edit-mode").css("background-color","#fff");
            $(".input-tab-trigger-ro-edit-mode").css("color","#333");
        });

        $(".input-tab-trigger-bg-edit-mode").click(function(){
            $(".input-tab-bg-edit-mode").show();
            $(".input-tab-en-edit-mode").hide();
            $(".input-tab-ro-edit-mode").hide();
            $(".input-tab-trigger-en-edit-mode").css("background-color","#fff");
            $(".input-tab-trigger-en-edit-mode").css("color","#333");
            $(".input-tab-trigger-ro-edit-mode").css("background-color","#fff");
            $(".input-tab-trigger-ro-edit-mode").css("color","#333");
            $(".input-tab-trigger-bg-edit-mode").css("background-color","#666666");
            $(".input-tab-trigger-bg-edit-mode").css("color","#fff");
        });

    });
</script>