<?php
define('__current_url_without_query', 'browse.php');
require '__top.php';
try{
/* @var $pdo PDO */

    $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
    $ds->execute();
    $d = $ds->fetch();
    $dostavka_cena = $d['delivery_price'];

if(!isset ($_GET['category'], $_GET['mode'], $_GET['model'])) greshka('Не е посочен вид/модел');

    if(isset($_SESSION["saveAs"])) {
        $_POST["ime"] = $_SESSION["saveAs"]["product_name"];
        $_POST["cena_dostavna"] = $_SESSION["saveAs"]["product_cena_dostavna"];
        $_POST["cena"] = $_SESSION["saveAs"]["product_cena"];
        $_POST["cena_promo"] = $_SESSION["saveAs"]["product_cena_promo"];
        $_POST["product_video"] = $_SESSION["saveAs"]["product_video"];
        $_POST["product_collection"] = $_SESSION["saveAs"]["product_collection"];
        $_POST["product_collection_name"] = $_SESSION["saveAs"]["product_collection_name"];
        $_POST["additional_delivery"] = $_SESSION["saveAs"]["additional_delivery"];
        if(isset($_SESSION["saveAs"]["product_tags"])) $_POST["etiket"] = $_SESSION["saveAs"]["product_tags"];
        if(isset($_SESSION["saveAs"]["warehouse_code"])) $_POST["warehouse_code"] = $_SESSION["saveAs"]["warehouse_code"];

        if(isset($_SESSION["saveAs"]["preorder"])) $_POST["preorder"] = $_SESSION["saveAs"]["preorder"];
        if(isset($_SESSION["saveAs"]["add_products_ids"])) $_POST["add_products_ids"] = $_SESSION["saveAs"]["add_products_ids"];

        $_POST["manual_link"] = $_SESSION["saveAs"]["manual_link"];

        foreach ($__languages as $v) {
            if(isset($_SESSION["saveAs"]["preorder_info_{$v->getPrefix()}"])) {
                $_POST["new_preorder_info_{$v->getPrefix()}"] = $_SESSION["saveAs"]["preorder_info_{$v->getPrefix()}"];
            }

            $_POST["new_artikul_info_{$v->getPrefix()}"] = $_SESSION["saveAs"]["product_info_{$v->getPrefix()}"];
            $_POST["product_info2_{$v->getPrefix()}"] = $_SESSION["saveAs"]["product_info2_{$v->getPrefix()}"];
            $_POST["new_product_title_{$v->getPrefix()}"] = $_SESSION["saveAs"]["product_title_{$v->getPrefix()}"];
        }

        foreach ($_SESSION["saveAs"] as $key => $warehouse) {
            if(strpos($key, "warehouse_etiket_") !== false) {
                $_POST[$key] = $warehouse;
            }
        }
    }

    unset($_SESSION['saveAs']);

    $artikul_greshki = NULL;
    $requiredFields = NULL;
    

    $vid = new vid_admin((int)$_GET['mode']);
    $model = new model((int)$_GET['model']);
    $kategoriq = new kategoriq_admin((int)$_GET['category']);

    if(isset ($_GET['tag'])) $etiket = $_GET['tag'];
    else $etiket = NULL;

    if(isset ($_POST['submit'])) {

        if($_POST['ime'] == "" ) $requiredFields[] = 'модел';
//        if($_POST['product_main_image'] == "") $requiredFields[] = 'снимка';
        if($_POST['cena_dostavna'] == "" &&  $_POST['cena'] == "") $requiredFields[] = 'цена на едро или крайна цена';

        if($requiredFields != NULL) {
            $stringFields = "";

            foreach ($requiredFields as $field) {
                if ($field === end($requiredFields)) {
                    $stringFields .= $field;
                } else {
                    $stringFields .= $field . ", ";
                }
            }

            $artikul_greshki[] = 'Моля въведете ' . $stringFields;
        }

        ######### ПРОВЕРЯВАМ ЗА ГРЕШКИ
        foreach ($_FILES as $v) {
            if(  ($v['error'] != 0 ) && ($v['size'] )   )     greshka( 'Грешка при качването на картинката' );
        }
        ##############################

        $pdo-> beginTransaction();

        $sql_languages = NULL;
        $sql_languages_values = NULL;
        foreach ($__languages as $v) {

            $sql_languages .= ', `dop_info_'.$v->getPrefix().'` ';
            $sql_languages .= ', `spec_'.$v->getPrefix().'` ';
            $sql_languages .= ', `preorder_'.$v->getPrefix().'` ';
            $sql_languages .= ', `title_'.$v->getPrefix().'` ';


            $sql_languages_values .= ', :dopinfo'.$v->getPrefix().' ';
            $sql_languages_values .= ', :spec'.$v->getPrefix().' ';
            $sql_languages_values .= ', :preorderinfo'.$v->getPrefix().' ';
            $sql_languages_values .= ', :title'.$v->getPrefix().' ';

            // product name start
            $ime = $_POST['ime'];
            if(isset($_POST['preorder'])) {
                $preorder = $_POST['preorder'];
            } else {
                $preorder = 0;
            }
            $collection = $_POST['product_collection'];

            $ime = trim($ime);

            $new_prodict_ime = $ime;


            unset($ime);
            // product name end

            if(isset ($_POST['new_product_title_'.lang_prefix])){

                $temp_title = $_POST['new_product_title_'.$v->getPrefix()];
            } else{
                $temp_title = '';
            }
            if(isset ($_POST['new_artikul_info_'.lang_prefix])){
                $temp_info = $_POST['new_artikul_info_'.$v->getPrefix()];
                $temp_preorder_info = $_POST['new_preorder_info_'.$v->getPrefix()];
            }
            else{
                $temp_info = '';
                $temp_preorder_info = '';
            }
            if(isset ($_POST['product_info2_'.lang_prefix])){
                $temp_spec = $_POST['product_info2_'.$v->getPrefix()];
            }
            else{
                $temp_spec = '';
            }

            if(isset($_POST['additional_delivery'])){
                $additional_delivery = po_taka_format_cena_sql($_POST['additional_delivery']);
            }else{
                $additional_delivery =0;
            }

            $new_prodict_dopinfo[] = $temp_info;
            $new_product_spec[] = $temp_spec;
            $new_product_preorder_info[] = $temp_preorder_info;
            $new_product_title[] = $temp_title;


            unset($temp_info);
            unset($temp_preorder_info);
            unset($temp_title);
        }

        if((isset($_POST['product_dop_info']))&&($_POST['product_dop_info'])) $dop_info = $_POST['product_dop_info'];
        else $dop_info = '';

        if(isset($_POST['cena'])){
            if($_POST['cena'] == "") {
                $cena = 0;
            } else {
                $cena = po_taka_format_cena_sql($_POST['cena']);
            }
        }else{
            $cena =0;
        }

        if(isset($_POST['cena_dostavna'])){
            if($_POST['cena_dostavna'] == "") {
                $cena_dostavna = 0;
            } else {
                $cena_dostavna = po_taka_format_cena_sql($_POST['cena_dostavna']);
            }
        }else{
            $cena_dostavna =0;
        }

        if(isset($_POST['cena_promo'])){
            if($_POST['cena_promo'] == "") {
                $cena_promo = 0;
            } else {
                $cena_promo = po_taka_format_cena_sql($_POST['cena_promo']);
            }
        }else{
            $cena_promo =0;
        }

        if((isset($_POST['cena_dostavka']))&&($_POST['cena_dostavka'])) $cena_dostavka = po_taka_format_cena_sql($_POST['cena_dostavka']);
        else $cena_dostavka = 0;

        if((isset($_POST["youtube"]))&&($_POST["youtube"])) $youtube = po_taka_format_cena_sql($_POST["youtube"]);
        else $youtube = '';

        if(isset($_POST['okazion'])){
            if($_POST['okazion']=='on'){
                $isokazion = 1;
            }
        }else{
            $isokazion =0;
        }


        if(isset($_POST['order'])){
            $order =$_POST['order'];
        }else{
            $order =0;
        }

//        if($cena_dostavna > $cena ) $artikul_greshki[] = 'Цена доставна е по-голяма от цена';
//        if( $cena_promo && ($cena_dostavna > $cena_promo)) $artikul_greshki[] ='Цена доставна е по-голяма от цена промо';
        if( $cena_promo && ($cena_promo > $cena)) $artikul_greshki[] ='Промо цена е по-голяма от цената';

        $kod = $_POST['kod'];
        $kod = trim($kod);

        if($artikul_greshki == NULL) {

        $stm = $pdo->prepare(' INSERT INTO `artikuli` (
        `kod`,
        `ime`,
        `id_vid` ,
        `id_model` ,
        `id_kategoriq` ,
        `add_artikuli` ,
        `cena`,
        `cena_dostavna` ,
        `cena_promo` ,
        `kartinka` ,
        `cena_dostavka`, 
        `preorder`,
        `collection_id`,
        `additional_delivery`
        '.$sql_languages.'
        )
        VALUES (
        :kod,
        :ime,
        :id_vid, :id_model, :id_kategoriq, :add_artikuli, :cena, :cena_dostavna, :cena_promo, :kartinka, :cena_dostavka, :preorder, :collection_id, :additional_delivery
        '.$sql_languages_values.'
        ) ');

        $stm -> bindValue(':ime', $new_prodict_ime);

        $stm -> bindValue(':kod', $kod);

        $stm -> bindValue('id_vid', $vid->getId(), PDO::PARAM_INT);
        $stm -> bindValue(':id_model', $model->getId(), PDO::PARAM_INT);
        $stm -> bindValue(':id_kategoriq', $kategoriq->getId(), PDO::PARAM_INT);
        $stm -> bindValue(':add_artikuli', $_POST['add_products_ids'], PDO::PARAM_STR);
        $stm -> bindValue(':cena', (float) $cena, PDO::PARAM_STR);
        $stm -> bindValue(':cena_dostavna', (float) $cena_dostavna, PDO::PARAM_STR);
        $stm -> bindValue(':cena_promo', (float) $cena_promo, PDO::PARAM_STR);
        $stm -> bindValue(':kartinka', $_POST['product_main_image'], PDO::PARAM_STR);
        $stm -> bindValue(':cena_dostavka', $cena_dostavka, PDO::PARAM_STR);
        $stm -> bindValue(':preorder', $preorder, PDO::PARAM_STR);
        $stm -> bindValue(':collection_id', $collection, PDO::PARAM_INT);
        $stm -> bindValue(':additional_delivery', $additional_delivery, PDO::PARAM_STR);

        foreach ($__languages as $k => $v) {
            $stm -> bindValue(':dopinfo'.$v->getPrefix(), $new_prodict_dopinfo[$k], PDO::PARAM_STR);
            $stm -> bindValue(':spec'.$v->getPrefix(), $new_product_spec[$k], PDO::PARAM_STR);
            $stm -> bindValue(':preorderinfo'.$v->getPrefix(), $new_product_preorder_info[$k], PDO::PARAM_STR);
            $stm -> bindValue(':title'.$v->getPrefix(), $new_product_title[$k], PDO::PARAM_STR);
        }

        $stm->execute();

        $atikul_id = $pdo->lastInsertId();
        if (isset($_POST['product_images_extra'])) {
            foreach ($_POST['product_images_extra'] as $key => $file) {
                $stm = $pdo->prepare('UPDATE `artikuli` SET `img'.($key+1).'` = ? WHERE `id` = ? ');
                $stm -> bindValue(1, $file, PDO::PARAM_STR);
                $stm -> bindValue(2, $atikul_id, PDO::PARAM_INT);
                $stm -> execute();
            }
        }

        if(($kategoriq->getId()==27)||($kategoriq->getId()==29)){
            if(isset ( $_POST['product_used'])) {
                foreach ($_POST['product_used'] as $v) {
                    $stm = $pdo->prepare('INSERT INTO `artikul_artikul_zapisi`(`id`, `artikul_tag`) VALUES(?, ?) ');
                    $stm -> bindValue(1, $atikul_id, PDO::PARAM_INT);
                    $stm -> bindValue(2, (int)$v, PDO::PARAM_INT);
                    $stm -> execute();
                }
            }
        }

        //WAREHOUSE
        if(isset($_POST['warehouse_code'])){
            $warehouse_code = $_POST['warehouse_code'];
            if($warehouse_code != ""){
                $add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `code`,`type`) VALUES (?,?,?)');
                $add -> bindValue(1, $atikul_id, PDO::PARAM_INT);
                $add -> bindValue(2, $warehouse_code, PDO::PARAM_INT);
                $add -> bindValue(3, 1, PDO::PARAM_INT);
                $add -> execute();
            }
        }

        $eg = NULL;
        $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ORDER BY `id` ');
        $stm -> bindValue(1, $vid->getId(), PDO::PARAM_INT);
        $stm -> execute();
        if($stm->rowCount() > 0 ) foreach ($stm -> fetchAll() as $v) {
            $temp_etiket_grupi = new etiket_grupa_admin($v);
            if($temp_etiket_grupi->getEtiketi() == NULL) {
                unset ($temp_etiket_grupi);
                continue;
            }
            $eg[] = $temp_etiket_grupi;
            unset ($temp_etiket_grupi);
        }
        if($eg) foreach ($eg as $v) {
            if ($v->getSelect_menu() == 1) {
                foreach ($v->getEtiketi() as $e) {
                    $temp_input = $_POST['warehouse_etiket_'.$e->getId().''];
                    $temp_price_big = $_POST['warehouse_price_big_'.$e->getId().''];
                    $temp_price = $_POST['warehouse_price_'.$e->getId().''];
                    $temp_price_promo = $_POST['warehouse_promo_price_'.$e->getId().''];
                    if($temp_input != ""){
                        $add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `code`, `tag_id`, `type`,`big_price`,`price`,`promo_price`) VALUES (?,?,?,?,?,?,?)');
                        $add -> bindValue(1, $atikul_id, PDO::PARAM_INT);
                        $add -> bindValue(2, (int)$temp_input, PDO::PARAM_INT);
                        $add -> bindValue(3, $e->getId(), PDO::PARAM_INT);
                        $add -> bindValue(4, 2, PDO::PARAM_INT);
                        $add -> bindValue(5, $temp_price_big, PDO::PARAM_STR);
                        $add -> bindValue(6, $temp_price, PDO::PARAM_STR);
                        $add -> bindValue(7, $temp_price_promo, PDO::PARAM_STR);
                        $add -> execute();
                    } else {
                        $add = $pdo->prepare('INSERT INTO `warehouse_products`(`product_id`, `tag_id`, `type`,`big_price`,`price`,`promo_price`) VALUES (?,?,?,?,?,?)');
                        $add -> bindValue(1, $atikul_id, PDO::PARAM_INT);
                        $add -> bindValue(2, $e->getId(), PDO::PARAM_INT);
                        $add -> bindValue(3, 2, PDO::PARAM_INT);
                        $add -> bindValue(4, $temp_price_big, PDO::PARAM_STR);
                        $add -> bindValue(5, $temp_price, PDO::PARAM_STR);
                        $add -> bindValue(6, $temp_price_promo, PDO::PARAM_STR);
                        $add -> execute();
                    }
                }
            }
        }

        //END WAREHOUSE

        if(isset ( $_POST['etiket'])) {
            foreach ($_POST['etiket'] as $v) {

            $stm = $pdo->prepare('INSERT INTO `etiketi_zapisi`(`id_etiket`, `id_artikul`) VALUES(?, ?) ');
            $stm -> bindValue(1, $v, PDO::PARAM_INT);
            $stm -> bindValue(2, $atikul_id, PDO::PARAM_INT);
            $stm -> execute();
            }
        }

        //============= Manual =======================
        $uploads_dir = dir_root.'/manuals/';
        if ((isset($_FILES["Manual"]))&&($_FILES['Manual']['size'] > 0)) {
            $tmp_name = $_FILES["Manual"]["tmp_name"];
            $name = $_FILES["Manual"]["name"];
            if (move_uploaded_file($tmp_name, $uploads_dir.$atikul_id."_".$name)){
                $stm = $pdo->prepare('UPDATE `artikuli` SET `Manual` = ? WHERE `id` = ? ');
                $stm -> bindValue(1, '/manuals/'.$atikul_id."_".$name, PDO::PARAM_STR);
                $stm -> bindValue(2, $atikul_id, PDO::PARAM_INT);
                $stm -> execute();
            }
        } else if (isset($_POST["manual_link"]) && $_POST["manual_link"] != "") {
            $stm = $pdo->prepare('UPDATE `artikuli` SET `Manual` = ? WHERE `id` = ? ');
            $stm -> bindValue(1, $_POST["manual_link"], PDO::PARAM_STR);
            $stm -> bindValue(2, $atikul_id, PDO::PARAM_INT);
            $stm -> execute();
        }

        //============= End Manual =======================

        //============= Day Offer =======================


        if(isset($_POST['dayoffer'])){
            $stm = $pdo->prepare('UPDATE `artikuli` SET `DayOffer` = CURRENT_TIMESTAMP WHERE `id` = ? ');
            $stm -> bindValue(1, $atikul_id, PDO::PARAM_INT);
            $stm -> execute();
        }


        //============= End Day Offer =======================

        //============= Video =======================

        if(isset($_POST['product_video'])){

            $tmp_video_url=$_POST['product_video'];
            if(preg_match('/vbox7.com/',$tmp_video_url)){
                $fail = explode(':',$_POST['product_video']);
                $tmp_video_url='http://i48.vbox7.com/player/ext.swf?vid='.$fail[2];
            }
            if(preg_match('/youtube.com/',$tmp_video_url) && strpos($tmp_video_url, 'www.youtube.com/v/') == -1){
                $fail = explode('?v=',$_POST['product_video']);
                $tmp_video_url='https://www.youtube.com/v/'.$fail[1];
            }
            $stm = $pdo->prepare('UPDATE `artikuli` SET `video` = :video WHERE `id` = :id ');
            $stm -> bindValue(':video', $tmp_video_url, PDO::PARAM_STR);
            $stm -> bindValue(':id', $atikul_id, PDO::PARAM_INT);
            $stm -> execute();

        }else{
            $stm = $pdo->prepare('UPDATE `artikuli` SET `video` = NULL WHERE `id` = ? ');
            $stm -> bindValue(1, $atikul_id, PDO::PARAM_INT);
            $stm -> execute();
        }


        //============= End video =======================

        } // ako ima gre6ki da da GOTO

        if($artikul_greshki == NULL ) {
            $artikul = new artikul_admin((int)$atikul_id);
            $artikul->image_sort_empty();
            $artikul->cache_update();
            $artikul->cache_search();



            $pdo->commit();

            setcookie('status_info','Добавен нов продукт');
            unset($_SESSION['atikul_id']);
            ?><meta http-equiv="Refresh" content="0;URL=<?php echo url; ?>admin/browse.php?<?php

            foreach ($_GET as $k => $v) {

                if(is_array($v)){
                    foreach ($v as $kk => $vv) {
                        echo 'tag[]='.urlencode($vv).'&';

                    }
                }
                else {
                echo urlencode($k).'='.$v.'&';
                }    }


            ?>" /><?php

            exit;

        }

        else {

            $pdo->rollBack();

        }

        unset($_SESSION['crop_image']);
    }

    $stm = $pdo->prepare('SELECT * FROM `etiketi_grupa` WHERE `id_vid` = ? ');
    $stm -> bindValue(1, $vid->getId(), PDO::PARAM_INT);
    $stm -> execute();

    if($stm->rowCount() == 0 ) $etiket_grupi = NULL;

    else foreach ($stm->fetchAll() as $v) {

        $etiket_grupi[] = new etiket_grupa_admin($v);

        }

	if(($kategoriq->getId()==27)||($kategoriq->getId()==29)){
	
		$stm = $pdo->prepare('SELECT `id`,`ime_'.lang_prefix.'` FROM `vid` WHERE `id_kategoriq` = ? ORDER BY `id` ');
		$stm -> bindValue(1, 26, PDO::PARAM_INT);
		$stm -> execute();
		if($stm->rowCount() > 0 ) foreach ($stm -> fetchAll() as $v) {   
			$product_vidove[] = $v;
		}
		
		$stm = $pdo->prepare('SELECT `id`,`ime_'.lang_prefix.'` FROM `vid` WHERE `id_kategoriq` = ? ORDER BY `id` ');
		$stm -> bindValue(1, 25, PDO::PARAM_INT);
		$stm -> execute();
		if($stm->rowCount() > 0 ) foreach ($stm -> fetchAll() as $v) {   
			$deinosti_vidove[] = $v;
		}
	}
} catch (Exception $e) { greshka($e); }

require 'template/new_product.php';