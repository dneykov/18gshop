<?php

//require dir_root.'klasove/artikul_admin.php';
//require dir_root.'klasove/kartinka_admin.php';
//require dir_root.'klasove/kategoriq_admin.php';
require dir_root.'klasove/user_admin.php';
//require dir_root.'klasove/vid_admin.php';
//require dir_root.'klasove/etiket_admin.php';
//require dir_root.'klasove/etiket_grupa_admin.php';
//require dir_root.'klasove/model_admin.php';
require dir_root.'klasove/order_admin.php';
//require dir_root.'klasove/shop_admin.php';


   function po_taka_update_multilanguage($request,$table_cloumn, $table, $id){

   global $__languages;
   $pdo = PDOX::vrazka();


   if(!preg_match('|^[0-9\_a-zA-Z]+$|is', $table)) greshka('table preg mach fail');
   if(!preg_match('|^[0-9\_a-zA-Z]+$|is', $table_cloumn)) greshka('table colum preg mach fail');

   $temp_sql = NULL;



   foreach ($__languages as $v) {

       if(isset ($_REQUEST[$request.$v->getPrefix()])){
         $new_names[$v->getPrefix()] =   $_REQUEST[$request.$v->getPrefix()];
       }

       else $new_names[$v->getPrefix()] =   '';


       if($temp_sql) $temp_sql .= ' , ';
       $temp_sql .= ' `'.$table_cloumn.$v->getPrefix().'` = :'.$table_cloumn.$v->getPrefix().' ';
   }


 $stm = $pdo->prepare('UPDATE `'.$table.'` SET '.$temp_sql.' WHERE `id` = :id ');

 $stm -> bindValue(':id', $id, PDO::PARAM_INT);


   foreach ($__languages as $v) {

       $stm -> bindValue(':'.$table_cloumn.$v->getPrefix(), stripslashes($new_names[$v->getPrefix()]));


   }

$stm -> execute();

}



#######################3
# SCALE
#########################

                function image_getHeight($image) {
                    $size = getimagesize($image);
                    $height = $size[1];
                    return $height;
                }
                //You do not need to alter these functions
                function image_getWidth($image) {
                    $size = getimagesize($image);
                    $width = $size[0];
                    return $width;
                }


                function image_resize ($image,$width,$height,$scale) {
                list($imagewidth, $imageheight, $imageType) = getimagesize($image);
                $imageType = image_type_to_mime_type($imageType);
                $newImageWidth = floor($width * $scale);
                $newImageHeight = floor($height * $scale);
                /*var_dump($newImageWidth);
                var_dump($newImageHeight);*/
                $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
                switch($imageType) {
                        case "image/gif":
                                $source=imagecreatefromgif($image);
                                break;
                        case "image/pjpeg":
                        case "image/jpeg":
                        case "image/jpg":
                                $source=imagecreatefromjpeg($image);
                                break;
                        case "image/png":
                        case "image/x-png":
                                $source=imagecreatefrompng($image);
                                break;
                   default:
                       greshka('Грешка в нов артикул, в имаге тайп свич');
                    }

                    imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

                    switch($imageType) {
                            case "image/gif":
                                    imagegif($newImage,$image);
                                    break;
                            case "image/pjpeg":
                            case "image/jpeg":
                            case "image/jpg":
                                    imagejpeg($newImage,$image,90);
                                    break;
                            case "image/png":
                            case "image/x-png":
                                    imagepng($newImage,$image);
                                    break;
                            default:
                                greshka('gre6ka imagetype funciq image resize');
                        }

                    } //function resizeImage

                    function image_resize_scale($image, $maxw, $maxh){
                        $width = image_getWidth($image);
			$height = image_getHeight($image);

                        if ($width > $maxw){
				$scale = $maxw/$width;
				image_resize($image,$width,$height,$scale);
			}

                        $width = image_getWidth($image);
			$height = image_getHeight($image);


                        if ($height > $maxh){
				$scale = $maxh/$height;
				image_resize ($image,$width,$height,$scale);
			}

                    } // functio image resize scale
#######################3
# SCALE          KRAI
#########################





function image_upload($katinka, $uploadfolder, $ime, $sas_pod_dir = true, $width = false, $height = false, $create_thumbnails = true){

    if($katinka['size'] == 0 ) greshka ('image size 0 ');
    if($katinka['error'] != 0 ) greshka (' errors in upload ');




    $godina = date('o');
    $den = date('z');

    $allowedfiletypes = array('jpeg','jpg','gif','png');



    if($sas_pod_dir ) {

    //$uploadfolder = "../uploads_images/" ;
    if(! is_dir($uploadfolder.$godina)) {
        mkdir($uploadfolder.$godina, 0755);
    }



    $uploadfolder .= $godina.'/';


    if(! is_dir($uploadfolder.$den))
        if(!mkdir($uploadfolder.$den, 0755)) greshka('MK dir /godina/den FAIL');
        $uploadfolder .= $den.'/';

    }

        if(empty($katinka['name'])){
            greshka("<strong>Error: File not uploaded!</strong></p>");
            }

            $uploadfilename = $katinka['name'];

            if(!preg_match('|^.+\.[a-zA-Z]+$|is', $uploadfilename)) greshka('Недопустим файл');
            $fileext = mb_strtolower(preg_replace('|^(.+)\.([a-z]+)$|is', '$2', $uploadfilename));


            if (!in_array($fileext,$allowedfiletypes)) { greshka("Недопустим формат ".$fileext); }

                $fulluploadfilename = $uploadfolder.$ime.'.'.$fileext;

                if (! move_uploaded_file($katinka['tmp_name'], $fulluploadfilename) ) {
                    greshka("<strong>Error: Couldn't save file !</strong></p>");                 }




                            if($height || $width) {

                                image_resize_scale($fulluploadfilename, $width, $height);


                            }

                            else {

                            if($create_thumbnails) {

                            $big = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $fulluploadfilename);
                            copy ($fulluploadfilename, $big);
                            image_resize_scale($big, image_size_max_w, image_size_max_h);


                            
                            $thumb = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $fulluploadfilename);
                            copy($fulluploadfilename, $thumb);
                            image_resize_scale($thumb, image_size_thumb_w, image_size_thumb_h);

                            image_resize_scale($fulluploadfilename, image_size_w, image_size_h);
                            
                            }


                            }

            return $fulluploadfilename;
    }






function po_taka_format_cena_sql($a){

$a = preg_replace('|,|', '.', $a);
$a = preg_replace('|[^0-9\.]|', '', $a);

return $a;

if(is_float($a)) return $a;

else {
    $a = (int)$a;
    echo 'ne';

}

return $a;

}



function delete_file($file, $stop_on_fail = false){

    if(is_file($file)) {

        unlink($file);

    }

    else if($stop_on_fail) {
        greshka('delte fial greshka '.$file);
    }


}



function po_taka_set_status($a){
    setcookie('status_info',$a, 0, '/');
}



function po_taka_is_file($patch){

    
    if(is_file($patch)) return true;

    else {

        greshka('nenamerem fajl'.$patch, '',  TRUE);

    }


}





function po_taka_redirect_to_referrer(){




    if(mb_strlen($_SERVER['HTTP_REFERER'] ) < 3 ) {
        greshka('nqma http referer');
    }

?>
<meta http-equiv="Refresh" content="0;URL=<?php echo $_SERVER['HTTP_REFERER']; ?>" />
<?php

exit;

}