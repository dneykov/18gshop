<?php
require '__top.php';
require dir_root_admin.'template/__top.php';

if(isset($_POST['addArticle'])){
	$__user->permission_check('новини','rw');
	$id = $_GET['id'];
	$type = $_POST['type'];
	$titleBG =$_POST['title_BG'];
	$titleEN = $_POST['title_EN'];
	$titleRO = $_POST['title_RO'];
	$textBG = $_POST['text_bg'];
	$textEN = $_POST['text_en'];
	$textRO = $_POST['text_ro'];
	$media = $_POST['media_hidden'];

	$err = "";
	$ernum = 0;
	if($titleBG == ""){
		if($ernum != 0) $err .= ", ";
		$err .= "загалвие на bg";
		$ernum++;
	}
	if($textBG == ""){
		if($ernum != 0) $err .= ", ";
		$err .= "информация на bg";
		$ernum++;
	}

	if(isset($_FILES['upload_img']) && ($_FILES['upload_img']['size'] > 0 )){
        $time=time();
        $max_file_size = 6000000;
        $upload_required = true;
        $upload_dir = __DIR__ . '/../images/info/';
        $dir = 'images/info/';

        $upload_img=$_FILES['upload_img'];
        
        switch($upload_img['error']){
            case UPLOAD_ERR_INI_SIZE:
                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
                break;

            case UPLOAD_ERR_PARTIAL:
                $err .= 'Грешка при качването на файла. Моля, опитайте отново.';
                break;

            case UPLOAD_ERR_NO_FILE:
                $err .='Не сте избрали файл за качване.';
                break;
        }
        
        if($upload_img['size'] > $max_file_size) {
                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
        }
        
        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png');
        $fileName 	= $_FILES['upload_img']['name'];
        $fileExt 	= strtolower(substr($fileName, strpos($fileName,'.'), strlen($fileName)-1));
        if(!in_array($fileExt,$allowed_filetypes)){
            $err .= "Форматът на снимката е неподходящ. Моля, изберете друга снимка";
        }
        
        if($err == "" && $upload_img['error']==0){
            $time=time();
            $rand=rand(1,30000);	

            if(!move_uploaded_file($upload_img['tmp_name'],$upload_dir.$rand."_".$time.$fileExt)){
                echo $upload_img['tmp_name'];
                $err .= "Грешка при преместването на файла!";
                $upload_img['error']++;
            }
            $file_name=$rand."_".$time.$fileExt;

            $image = new Imagick($upload_dir.$file_name);
            $image->resizeImage(720,0,Imagick::FILTER_LANCZOS,1);
            $image->setImageCompression(Imagick::COMPRESSION_JPEG);
            $image->setImageCompressionQuality(90);
            $image->stripImage();

            $upload_dir_thumb=$upload_dir."thumbs_720/";
            $dir_thumb = $dir."thumbs_720/";

            $image->writeImage($upload_dir_thumb."720_".$file_name);
            $media = $dir_thumb."720_".$file_name;
            $image->destroy();
        }
    }


	if($err == ""){
		$stm = $pdo->prepare("INSERT INTO `help_articles`(`cat_id`,`type`, `title_bg`, `title_en`, `title_ro`, `text_bg`, `text_en`, `text_ro`, `media`) VALUES (:id,:type,:titlebg,:titleen,:titlero,:textbg,:texten,:textro,:media)");

		$stm -> bindValue(":id", (int)$id, PDO::PARAM_INT);
		$stm -> bindValue(":type", $type, PDO::PARAM_STR);
		$stm -> bindValue(":titlebg", $titleBG, PDO::PARAM_STR);
		$stm -> bindValue(":titleen", $titleEN, PDO::PARAM_STR);
		$stm -> bindValue(":titlero", $titleRO, PDO::PARAM_STR);
		$stm -> bindValue(":textbg", $textBG, PDO::PARAM_STR);
		$stm -> bindValue(":texten", $textEN, PDO::PARAM_STR);
		$stm -> bindValue(":textro", $textRO, PDO::PARAM_STR);
		$stm -> bindValue(":media", $media, PDO::PARAM_STR);

		$stm -> execute();
		$lid = $pdo->lastInsertId();

		if(isset($_POST['article_tags'])){

		    foreach ($_POST['article_tags'] as $v) {
		    $stm = $pdo->prepare('INSERT INTO `help_tags_zapisi`(`tag_id`, `article_id`) VALUES(?, ?) ');
		    $stm -> bindValue(1, (int)$v, PDO::PARAM_INT);
		    $stm -> bindValue(2, (int)$lid, PDO::PARAM_INT);
		    $stm -> execute();
		    }
		}
	}
}

if(isset($_POST['SaveArticle'])){
	$id = $_POST['articleid'];
	$titleBG =$_POST['title_BG'];
	$titleEN = $_POST['title_EN'];
	$titleRO = $_POST['title_RO'];
	$textBG = $_POST['text_bg'];
	$textEN = $_POST['text_en'];
	$textRO = $_POST['text_ro'];
	$media = $_POST['media_hidden'];

	$err = "";
	$ernum = 0;
	if($titleBG == ""){
		$err .= "загалвие";
		$ernum++;
	}

	if($textBG == ""){
		if($ernum != 0) $err .= ", ";
		$err .= "информация";
	}

	if(isset($_FILES['edit_upload_img']) && ($_FILES['edit_upload_img']['size'] > 0 )){
        $time=time();
        $max_file_size = 6000000;
        $upload_required = true;
        $upload_dir = __DIR__ . '/../images/info/';
        $dir = 'images/info/';

        $upload_img=$_FILES['edit_upload_img'];
        
        switch($upload_img['error']){
            case UPLOAD_ERR_INI_SIZE:
                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
                break;

            case UPLOAD_ERR_PARTIAL:
                $err .= 'Грешка при качването на файла. Моля, опитайте отново.';
                break;

            case UPLOAD_ERR_NO_FILE:
                $err .='Не сте избрали файл за качване.';
                break;
        }
        
        if($upload_img['size'] > $max_file_size) {
                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
        }
        
        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png');
        $fileName 	= $_FILES['edit_upload_img']['name'];
        $fileExt 	= strtolower(substr($fileName, strpos($fileName,'.'), strlen($fileName)-1));
        if(!in_array($fileExt,$allowed_filetypes)){
            $err .= "Форматът на снимката е неподходящ. Моля, изберете друга снимка";
        }
        
        if($err == "" && $upload_img['error']==0){
            $time=time();
            $rand=rand(1,30000);	

            if(!move_uploaded_file($upload_img['tmp_name'],$upload_dir.$rand."_".$time.$fileExt)){
                echo $upload_img['tmp_name'];
                $err .= "Грешка при преместването на файла!";
                $upload_img['error']++;
            }
            $file_name=$rand."_".$time.$fileExt;
//            $image = new Imagick($upload_dir.$file_name);
//            $image->writeImage($upload_dir."original/".$file_name);
//            $image->destroy();

            $image = new Imagick($upload_dir.$file_name);
            $image->resizeImage(720,0,Imagick::FILTER_LANCZOS,1);
            $image->setImageCompression(Imagick::COMPRESSION_JPEG);
            $image->setImageCompressionQuality(90);
            $image->stripImage();

            $upload_dir_thumb=$upload_dir."thumbs_720/";
            $dir_thumb = $dir."thumbs_720/";

            $image->writeImage($upload_dir_thumb."720_".$file_name);
            $media = $dir_thumb."720_".$file_name;
            $image->destroy();
        }
    }

	$stm = $pdo->prepare("UPDATE `help_articles` SET `title_bg`=:titlebg,`title_en`=:titleen,`title_ro`=:titlero,`text_bg`=:textbg,`text_en`=:texten,`text_ro`=:textro,`media`=:media WHERE `id`=:id");

	$stm -> bindValue(":id", (int)$id, PDO::PARAM_INT);
	$stm -> bindValue(":titlebg", $titleBG, PDO::PARAM_STR);
	$stm -> bindValue(":titleen", $titleEN, PDO::PARAM_STR);
	$stm -> bindValue(":titlero", $titleRO, PDO::PARAM_STR);
	$stm -> bindValue(":textbg", $textBG, PDO::PARAM_STR);
	$stm -> bindValue(":texten", $textEN, PDO::PARAM_STR);
	$stm -> bindValue(":textro", $textRO, PDO::PARAM_STR);
	$stm -> bindValue(":media", $media, PDO::PARAM_STR);

	$stm -> execute();

	$stm = $pdo -> prepare('DELETE FROM `help_tags_zapisi` WHERE `article_id` = ? ');
	$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
	$stm -> execute();

	if(isset($_POST['article_tags'])){

	    foreach ($_POST['article_tags'] as $v) {
	    $stm = $pdo->prepare('INSERT INTO `help_tags_zapisi`(`tag_id`, `article_id`) VALUES(?, ?) ');
	    $stm -> bindValue(1, (int)$v, PDO::PARAM_INT);
	    $stm -> bindValue(2, (int)$id, PDO::PARAM_INT);
	    $stm -> execute();
	    }
	}
}
if(isset($_POST['DeleteArticle'])){
	$id = $_POST['articleid'];
    $stmForDelete = $pdo->prepare('SELECT * FROM `help_articles` WHERE `id` = ? LIMIT 1');
    $stmForDelete->bindValue(1, $id, PDO::PARAM_STR);
    $stmForDelete->execute();
    $helpForDeletion = $stmForDelete->fetch();

    $filename = pathinfo(dir_root . $helpForDeletion['media'], PATHINFO_BASENAME);

    delete_file(dir_root . $helpForDeletion['media']);
    delete_file(dir_root  . "images/info/" . mb_substr($filename, 4));

	$stm = $pdo->prepare("DELETE FROM `help_articles` WHERE `id`=:id");
	$stm -> bindValue(":id", (int)$id, PDO::PARAM_INT);
	$stm -> execute();

	$s = $pdo->prepare("DELETE FROM `help_tags_zapisi` WHERE `id`=:id");
	$s -> bindValue(":id", (int)$id, PDO::PARAM_INT);
	$s -> execute();
}

if(isset($_POST['SaveImg'])){
	$err = "";
	$media = "";
	if(isset($_FILES['upload_img']) && ($_FILES['upload_img']['size'] > 0 )){
        $time=time();
        $max_file_size = 6000000;
        $upload_required = true;
        $upload_dir = __DIR__ . '/../images/info/';
        $dir = 'images/info/';

        $upload_img=$_FILES['upload_img'];
        
        switch($upload_img['error']){
            case UPLOAD_ERR_INI_SIZE:
                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
                break;

            case UPLOAD_ERR_PARTIAL:
                $err .= 'Грешка при качването на файла. Моля, опитайте отново.';
                break;

            case UPLOAD_ERR_NO_FILE:
                $err .='Не сте избрали файл за качване.';
                break;
        }
        
        if($upload_img['size'] > $max_file_size) {
                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
        }
        
        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png');
        $fileName 	= $_FILES['upload_img']['name'];
        $fileExt 	= strtolower(substr($fileName, strpos($fileName,'.'), strlen($fileName)-1));
        if(!in_array($fileExt,$allowed_filetypes)){
            $err .= "Форматът на снимката е неподходящ. Моля, изберете друга снимка";
        }
        
        if($err == "" && $upload_img['error']==0){
            $time=time();
            $rand=rand(1,30000);	

            if(!move_uploaded_file($upload_img['tmp_name'],$upload_dir . $rand . "_" . $time . $fileExt)){
                echo $upload_img['tmp_name'];
                $err .= "Грешка при преместването на файла!";
                $upload_img['error']++;
            }
            $file_name=$rand."_".$time.$fileExt;
//            $image = new Imagick($upload_img['tmp_name']);
//            $image->writeImage($upload_dir."original/".$file_name);
//            $image->destroy();

            $image = new Imagick($upload_dir.$file_name);
            $image->resizeImage(720,0,Imagick::FILTER_LANCZOS,1);
            $image->setImageCompression(Imagick::COMPRESSION_JPEG);
            $image->setImageCompressionQuality(90);
            $image->stripImage();

            $upload_dir_thumb = $upload_dir . "thumbs_720/";
            $dir_thumb = $dir . "thumbs_720/";

            $image->writeImage($upload_dir_thumb . "720_" .$file_name);
            $media = $dir_thumb . "720_" . $file_name;
            $image->destroy();
        }
    }
    if($err == ""){
		$stm = $pdo->prepare("UPDATE `dict` SET `value`=:media WHERE `key`='info_img'");
		$stm -> bindValue(":media", $media, PDO::PARAM_STR);
		$stm -> execute();
	}
}
?>
<script type="text/javascript">
	$(document).ready(function(){


		$('#AddCatBtn').click(function() {
			var nameBG = $('#newCategoryBG').val();
			var nameEN = $('#newCategoryEN').val();
			var nameRO = $('#newCategoryRO').val();
			$.ajax({
	            type: "POST",
	            url: "help/ajax_new_category.php",
	            data: "namebg="+nameBG+"&nameen="+nameEN+"&namero="+nameRO,
	            cache: false,
	            success: function(html){
	            	location.reload();
	            }
	        });
		});
		$('#EditCatBtn').click(function() {
			var nameBG = $('#editCategoryBG').val();
			var nameEN = $('#editCategoryEN').val();
			var nameRO = $('#editCategoryRO').val();
			var id = $('#CategoryId').val();
			$.ajax({
	            type: "POST",
	            url: "help/ajax_edit_category.php",
	            data: "namebg="+nameBG+"&nameen="+nameEN+"&namero="+nameRO+"&id="+id,
	            cache: false,
	            success: function(html){
	            	location.reload();
	            }
	        });
		});
		$('#CancelAddCatBtn').click(function() {
			$('#newCategoryBG').val("");
			$('#newCategoryEN').val("");
			ShowAddCategory();
		});
		$('#CancelEditCatBtn').click(function() {
			$('#editCategoryBG').val("");
			$('#editCategoryEN').val("");
			editCategory();
		});


		$('#AddTagBtn').click(function() {
			var nameBG = $('#newTagBG').val();
			var nameEN = $('#newTagEN').val();
			var nameRO = $('#newTagEN').val();
			var id = $('#CategoryId').val();
			$.ajax({
	            type: "POST",
	            url: "help/ajax_new_tag.php",
	            data: "namebg="+nameBG+"&nameen="+nameEN+"&namero="+nameRO+"&id="+id,
	            cache: false,
	            success: function(html){
	            	location.reload();
	            }
	        });
		});
		$('#CancelAddTagBtn').click(function() {
			$('#newTagBG').val("");
			$('#newTagEN').val("");
			ShowAddTag();
		});


		$('#CancelAddArticle').click(function() {
			ShowAddArticle();
		});
	});
	function ShowAddCategory(){
        if ($("#AddCategory").is(":hidden")) {
            $("#AddCategory").show();
        } else {
            $("#AddCategory").hide();
        }
	}
	function ShowAddTag(){
        if ($("#AddTag").is(":hidden")) {
            $("#AddTag").show(300);
        } else {
            $("#AddTag").hide(300);
        }
	}
	function ShowAddArticle(){
		$('.ArticleDiv').slideUp("fast");
        if ($("#AddArticleDiv").is(":hidden")) {
            $("#AddArticleDiv").fadeIn(300);
        } else {
            $("#AddArticleDiv").hide(300);
        }
	}
	function editCategory(){
		if ($("#editCategory").is(":hidden")) {
            $("#editCategory").show();
        } else {
            $("#editCategory").hide();
        }
	}
	function editTag(id){
		if ($("#editTag-"+id).is(":hidden")) {
            $("#editTag-"+id).fadeIn(300);
        } else {
            $("#editTag-"+id).hide(300);
        }
	}
	function CancelTag(id){
		if ($("#editTag-"+id).is(":hidden")) {
            $("#editTag-"+id).fadeIn(300);
        } else {
            $("#editTag-"+id).hide(300);
        }
        $('#editTagBG-'+id).val("");
        $('#editTagEN-'+id).val("");
	}
	function deleteTag(id){
		$.ajax({
            type: "POST",
            url: "help/ajax_delete_tag.php",
            data: "id="+id,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}
	function SaveTag(id) {
		var nameBG = $('#editTagBG-'+id).val();
		var nameEN = $('#editTagEN-'+id).val();
		var nameRO = $('#editTagRO-'+id).val();
		$.ajax({
            type: "POST",
            url: "help/ajax_edit_tag.php",
            data: "namebg="+nameBG+"&nameen="+nameEN+"&namero="+nameRO+"&id="+id,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}
	function deleteTag(id){
		$.ajax({
            type: "POST",
            url: "help/ajax_delete_tag.php",
            data: "id="+id,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}

	function deleteCategory(id){
        var id = $('#CategoryId').val();
        $(".modal-body").html('<div style="width: 100%; height: 40px;">' + $('#cat'+id+' a').html() + '</div> <button class="button-cancel" onclick="hideModal();" style="position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deleteCategory_Ajax(\'' + id + '\');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
        $(".modal").show();
    }
    function deleteCategory_Ajax(id){
        $.ajax({
            type: "POST",
            url: "help/ajax_delete_category.php",
            data: "id="+id,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
    }
	function toggleCont(x) {
		if($('.ArticleDiv').is(':visible')) {
			$('.ArticleDiv').slideUp("fast");
			$('.a-link').css('background', 'url(../images/str_i_f.png) 5px 4px no-repeat');
		}
	    if($('#a-'+x).is(":hidden")) {
	        $('#a-'+x).slideDown("fast");
	       	$('#a-link-'+x).css('background', 'url(../images/str_i_down.png) 5px 4px no-repeat');
	    } else {
	        $('#a-'+x).hide();
	        $('#a-link-'+x).css('background', 'url(../images/str_i_f.png) 5px 4px no-repeat');
	    }
	}; 
</script>
<style>
	#leftCol {
		padding-top: 15px;
		float: left;
		width: 300px;
	}
	#leftCol a{
		margin-left: 5px;
	}
	#leftCol a:hover{
		text-decoration: none;
		color: #f8931d;
	}
	#leftCol button {
		margin-top: 10px;
		margin-bottom: 20px;
	}
	#rightCol {
		float: left;
	}
	#help_cats {
		height: 20px;
		line-height: 10px;
		color: #fff;
		width: 100%;
	}
	#AddCategory {
		padding-left: 5px;
		padding-top: 5px;
		display: none;
	}
	#AddTag {
		padding-left: 5px;
		padding-top: 5px;
		margin-left: 5px;
		display: none;
	}
	.addbutton {
		display: block;
		margin: 5px 0 15px 0;
		font-size: 14px;
	}
	.addbutton:hover {
		text-decoration: none;
		color: #f8931d;
	}
	.addbutton:nth-child(1){
		margin-top: -15px;
	}
	#editCategory {
		margin-left: 10px;
		display: none;
	}
	#AddArticleDiv {
		display: none;
		border: 1px solid #ccc;
		margin-bottom: 100px;
        width: 900px;
	}
	.ArticleDiv {
		border: 1px solid #ccc;
		margin-bottom: 20px;
		display: none;
	}
	.a-link {
		margin-top: 10px;
		cursor: pointer;
		height: 20px;
		padding-bottom: 5px;
		border-bottom: 1px solid #ccc;
		padding-left: 25px;
		width: 875px;
		background: url(../images/str_i_f.png) 5px 4px no-repeat;
	}
	.editTag {
		margin-left: 5px;
		display: none;
	}
	.tags {
		font-size: 13px !important;
		cursor: pointer;
	}
	.tags:hover {
		color: #f8931d;
	}
	.erroradd {
		font-size: 11px;
		color: red;
	}
	#navigation {
		width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
	}
	#navigation a {
		margin-left: 10px;
		margin-right: 30px;
		font-size: 13px;
		color: #333;
	}
	input[type=submit]{
		margin-top: 25px;
	}
            .input-tab-trigger-bg-help{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-help:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-help{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-help:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-help{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-help:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-help{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-help{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-help{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-help-tag{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-help-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-help-tag{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-help-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
    .input-tab-trigger-ro-help-tag{
        float: right;
        color: #333;
        font-size: 13px;
        width: 30px;
        height: 20px;
        line-height: 20px;
        text-align: center;
        cursor: pointer;
    }
    .input-tab-trigger-ro-help-tag:hover{
        color: #fff !important;
        background-color: #ff9207 !important;
    }
            .input-tab-bg-help-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-help-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
    .input-tab-ro-help-tag{
        padding-left: 10px;
        width: 240px;
        font-size: 13px;
        color: #333;
        font-family: 'Verdana', sans-serif;
        display: none;
        border: 1px solid #666666;
        height: 26px;
    }
            .input-tab-trigger-bg-edit-tag{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-edit-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-edit-tag{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-edit-tag:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-edit-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-edit-tag{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-edit-tag-2{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-edit-tag-2:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-edit-tag-2{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-edit-tag-2:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-edit-tag-2{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-edit-tag-2{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }

            .input-tab-trigger-bg-new-info{
                float: right;
                color: #fff;
                font-size: 13px;
                background-color: #666666;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-bg-new-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-en-new-info{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-en-new-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-trigger-ro-new-info{
                float: right;
                color: #333;
                font-size: 13px;
                width: 30px;
                height: 20px;
                line-height: 20px;
                text-align: center;
                cursor: pointer;
            }
            .input-tab-trigger-ro-new-info:hover{
                color: #fff !important;
                background-color: #ff9207 !important;
            }
            .input-tab-bg-new-info{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-en-new-info{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
            .input-tab-ro-new-info{
                padding-left: 10px;
                width: 240px;
                font-size: 13px;
                color: #333;
                font-family: 'Verdana', sans-serif;
                display: none;
                border: 1px solid #666666;
                height: 26px;
            }
</style>
<div id="navigation">
	<a href="<?php echo url."admin/help.php"?>" style="color: #F8931F">Oсновна информация</a>
	<a href="<?php echo url."admin/moduls/about.php"?>">Kак да поръчам</a>
	<a href="<?php echo url."admin/files.php"?>">Ценови листи</a>
    <a href="<?php echo url."admin/moduls/privacy.php"?>">Лични данни</a>
</div>
<div id="errors"></div>
<div style="width: 1280px;">
<div id="leftCol">
	<a class="addbutton" href="#" onclick="ShowAddCategory(); return false;" style="color: #fff;background-color: #666666;width: 100px;margin-left: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;float: left;font-size: 13px;width: 100px;line-height: 23px;">+ категория</a>
	<div id="AddCategory" style="margin-top: 20px;font-size: 13px;">
        <div class="input-tab-holder" style="margin-top: 10px; width: 240px;">
            <div class="input-tab-title">категория</div>
            <div class="input-tab-trigger-ro-help">ro</div>
            <div class="input-tab-trigger-en-help">en</div>
            <div class="input-tab-trigger-bg-help">bg</div>
        </div>
        <input id="newCategoryBG" class="input-tab-bg-help" type="text">
        <input id="newCategoryEN" class="input-tab-en-help" type="text">
        <input id="newCategoryRO" class="input-tab-ro-help" type="text">

		<button id="AddCatBtn" class="button-save">save</button>
		<button id="CancelAddCatBtn" class="button-cancel">cancel</button>
	</div>
    <div style="clear: both;"></div>
	<?php
		$stm = $pdo->prepare("SELECT * FROM `help_groups` ORDER BY `id` ASC");
		$stm -> execute();
		foreach ($stm->fetchAll() as $c) {
			echo "<div class='categories' style='margin-top: 10px;' id='cat".$c['id']."'>";
				echo "<a href='".url."admin/help.php?id=".$c['id']."' ";
					if(isset($_GET['id']) && $_GET['id'] == $c['id']){
						echo "style='color: #f8931d'";
					}
				echo ">".$c['name_bg']."</a>";
				if(isset($_GET['id']) && $_GET['id'] == $c['id']){
					echo "<a href='#' onclick='editCategory()'><span style='color: #80B543;font-size: 8pt;'>E</span></a>";
					echo "<a href='#' onclick='deleteCategory()'><span style='color: red;font-size: 8pt;'>X</span></a>";
				}
			echo "</div>";
			if(isset($_GET['id']) && $_GET['id'] == $c['id']){
				echo "<div id='editCategory' style='margin-top: 10px;'>";
					echo "<div class='input-tab-holder' style='margin-top: 10px; width: 240px;''>
                            <div class='input-tab-title'>таг</div>
                            <div class='input-tab-trigger-ro-edit-tag'>ro</div>
                            <div class='input-tab-trigger-en-edit-tag'>en</div>
                            <div class='input-tab-trigger-bg-edit-tag'>bg</div>
                        </div>
                        <input id='editCategoryBG' class='input-tab-bg-edit-tag' type='text' value='".$c['name_bg']."'>
                        <input id='newCategoryEN' class='input-tab-en-edit-tag' type='text' value='".$c['name_en']."'>
                        <input id='newCategoryRO' class='input-tab-ro-edit-tag' type='text' value='".$c['name_ro']."'>";
					echo "<button id='EditCatBtn' class='button-save'>save</button>";
					echo "<button id='CancelEditCatBtn'class='button-cancel' style='margin-left: 5px;'>cancel</button>";
				echo "</div>";
			}
			if(isset($_GET['id']) && $_GET['id'] == $c['id']){
				$stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `cat_id` = ? ORDER BY `id` ASC");
				$stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
				$stm -> execute();
				foreach ($stm->fetchAll() as $c) {
					echo "<div class='tags' id='tag".$c['id']."'  style='margin-left: 5px;'>";
						echo "<a href='".url."admin/help.php?id=".$_GET['id']."&t=".$c['id']."'";
						if(isset($_GET['t']) && $_GET['t'] == $c['id']){
							echo "style='color: #f8931d'";
						}
						echo ">";
						echo $c['name_bg'];
						echo "</a>";
						echo "<a href='#' onclick='editTag(\"".$c['id']."\")'><span style='color: #80B543;font-size: 8pt;'>E</span></a>";
						echo "<a href='#' onclick='deleteTag(\"".$c['id']."\")'><span style='color: red;font-size: 8pt;'>X</span></a>";
					echo "</div>";
					echo "<div class='editTag' id='editTag-".$c['id']."'>";
                        echo "<div class='input-tab-holder' style='margin-top: 10px; width: 240px;''>
                                <div class='input-tab-title'>таг</div>
                                <div class='input-tab-trigger-ro-edit-tag'>ro</div>
                                <div class='input-tab-trigger-en-edit-tag'>en</div>
                                <div class='input-tab-trigger-bg-edit-tag'>bg</div>
                            </div>
                            <input id='editTagBG-".$c['id']."' class='input-tab-bg-edit-tag' type='text' value='".$c['name_bg']."'>
                            <input id='editTagEN-".$c['id']."' class='input-tab-en-edit-tag' type='text' value='".$c['name_en']."'>
                            <input id='editTagRO-".$c['id']."' class='input-tab-ro-edit-tag' type='text' value='".$c['name_ro']."'>";
						echo "<button onclick='SaveTag(\"".$c['id']."\")' class='button-save'>save</button>";
						echo "<button onclick='CancelTag(\"".$c['id']."\")' class='button-cancel' style='margin-left: 10px;'>cancel</button>";
					echo "</div>";
				}
			?>
			<a class="addbutton" href="#" onclick="ShowAddTag(); return false;" style="color: #fff;background-color: #666666;margin-left: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;width:100px;font-size: 13px;line-height: 23px;">+ таг</a>
			<div id="AddTag" style="font-size: 13px;">
                <div class="input-tab-holder" style="margin-top: 10px; width: 240px;">
                    <div class="input-tab-title">таг</div>
                    <div class="input-tab-trigger-ro-help-tag">ro</div>
                    <div class="input-tab-trigger-en-help-tag">en</div>
                    <div class="input-tab-trigger-bg-help-tag">bg</div>
                </div>
                <input id="newTagBG" class="input-tab-bg-help-tag" type="text">
                <input id="newTagEN" class="input-tab-en-help-tag" type="text">
                <input id="newTagRO" class="input-tab-ro-help-tag" type="text">

				<button id="AddTagBtn" class='button-save'>save</button>
				<button id="CancelAddTagBtn" class='button-cancel'>cancel</button>
			</div>
			<?php
			}
		}
	?>
</div>
<div id="RightCol" style="width: 900px;">
<script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,cut,copy,paste,pastetext,pasteword,|,undo,redo,link,unlink,forecolor,|,bullist,numlist,|,outdent,indent",
		theme_advanced_buttons2 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "<?php echo url_template_folder; ?>images/css3.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],
		
		language : 'bg',
		
		theme_advanced_resizing : false
	});
</script>
<?php
	if(isset($_GET['id']) && !isset($_GET['t'])){
		$id = $_GET['id'];
		echo "<input id='CategoryId' type='hidden' value='".$id."'>";
		if(isset($err) && $err != ""){
			echo "<div class='erroradd'>Моля въведете ".$err."</div>";
			?>
			<style>
			#AddArticleDiv {
				display: block;
			}
			</style>
			<?php
		}
		?>

			<a class="addbutton" href="#" onclick="ShowAddArticle(); return false;" style="color: #fff;background-color: #666666;width: 160px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;font-size: 13px;margin-top:0px;line-height: 23px;">добави информация</a>
			<div id="AddArticleDiv" style="margin-left: 10px;">
				<form enctype="multipart/form-data" action="" method="post">
					<table cellspacing="10">
						<tr>
							<td valign="top">
                                <div class="input-tab-holder" style="margin-top: 10px; width: 300px;">
                                    <div class="input-tab-title">заглавие</div>
                                       <div class="input-tab-trigger-ro-new-info">ro</div>
                                       <div class="input-tab-trigger-en-new-info">en</div>
                                       <div class="input-tab-trigger-bg-new-info">bg</div>
                                </div>
								<input name="title_BG" style="width:300px; height: 26px;" value="<?php if(isset($_POST['title_BG'])) echo $_POST['title_BG'];?>" class="input-tab-bg-new-info"/>	
                                <input name="title_EN" style="width:300px; height: 26px;" value="<?php if(isset($_POST['title_EN'])) echo $_POST['title_EN'];?>" class="input-tab-en-new-info"/>
                                <input name="title_RO" style="width:300px; height: 26px;" value="<?php if(isset($_POST['title_RO'])) echo $_POST['title_RO'];?>" class="input-tab-ro-new-info"/>
                                <br><br>
								<img id="article_image" src="<?php echo url.'/images/blank_400X300.png'; ?>" style="width:720px;" />
								<div id="newinfooptions" style="padding-bottom: 5px;"><br />
									<select name="type" style="height: 26px;" id="article_type">
										<option value="photo">Снимка</option>
										<option value="video">youtube/vimeo видео</option>
									</select>
									<script type="text/javascript">
										$('#article_type').change(function() {
											if($(this).val()=="video"){
												$('#article_image_hidden').val('');
												$('#article_image_hidden').detach().attr('type', 'text').insertAfter("#newinfooptions");
												$('#article_thickbox').css("display","none");
												$('#article_image').css("display","none");
												$('#upload_img').css("display","none");
											}else{
												$('#article_image_hidden').val('');
												$('#article_image_hidden').detach().attr('type', 'hidden').insertAfter("#newinfooptions");
												$('#upload_img').css("display","block");
												$('#article_image').css("display","block");
											}
										});
									</script>
								</div>
								<input type="hidden" style="width:300px; height: 26px;" name="media_hidden" id="article_image_hidden" value="<?php if(isset($_POST['media_hidden'])) echo $_POST['media_hidden'];?>" style="margin-top: 5px;"/>
								<input type="file" name="upload_img" id="upload_img"/>
								<br /><br />
                                <textarea name="text_bg" style="width:800px;height:420px;resize: none;"><?php if(isset($_POST['text_bg'])) echo $_POST['text_bg'];?></textarea><BR />
                                <textarea name="text_en" style="width:800px;height:420px;resize: none;"><?php if(isset($_POST['text_en'])) echo $_POST['text_en'];?></textarea><BR />
                                <textarea name="text_ro" style="width:800px;height:420px;resize: none;"><?php if(isset($_POST['text_ro'])) echo $_POST['text_ro'];?></textarea><BR />
                                <br>
								<div class="a-tags">
									<div style="margin-top: 10px; font-size: 13px;">Тагове</div>
									<?php
									$stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `cat_id` = ? ORDER BY `id` ASC");
									$stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
									$stm -> execute();
									foreach ($stm->fetchAll() as $c) {
										echo '<input type="checkbox" name="article_tags[]" value="'.$c['id'].'" > '.$c['name_bg'].'<br />';
									}
									?>
								</div>			
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<button type="submit" name="addArticle" class="button-save">save</button>								
								<button type="reset" name="CancelAddArticle" class="button-cancel" id="CancelAddArticle">cancel</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		<?php
		$stm = $pdo->prepare("SELECT * FROM `help_articles` WHERE `cat_id` = ? ORDER BY `id` ASC");
		$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
		$stm -> execute();
		foreach ($stm->fetchAll() as $a) {
			?>
			<div class="a-link" style="font-size: 13px;" id="a-link-<?php echo $a['id'];?>" onclick="toggleCont('<?php echo $a['id'];?>')"><?php echo $a['title_bg']; ?></div>
			<div class="ArticleDiv" id="a-<?php echo $a['id'];?>">
				<form enctype="multipart/form-data" action="" method="post">
					<table cellspacing="10">
						<tr>
							<td valign="top">
								<input type="hidden" name="articleid" value="<?php echo $a['id'];?>">		
                                <div class="input-tab-holder" style="margin-top: 10px; width: 300px;">
                                    <div class="input-tab-title">заглавие</div>
                                       <div class="input-tab-trigger-ro-new-info">ro</div>
                                       <div class="input-tab-trigger-en-new-info">en</div>
                                       <div class="input-tab-trigger-bg-new-info">bg</div>
                                </div>
                                <input name="title_BG" style="width:300px; height: 26px;" value="<?php echo $a['title_bg']; ?>" class="input-tab-bg-new-info"/>    
                                <input name="title_EN" style="width:300px; height: 26px;" value="<?php echo $a['title_en']; ?>" class="input-tab-en-new-info"/>
                                <input name="title_RO" style="width:300px; height: 26px;" value="<?php echo $a['title_ro']; ?>" class="input-tab-ro-new-info"/>
                                <br><br>
                                <?php
									if($a['type'] == "photo"){
								?>					
										<?php if($a['media'] != "") {?>
											<img id="article_image<?php echo $a['id'];?>" src="<?php echo url.$a['media'];?>" style="width:720px;"/>
										<?php } ?>
										<input type="hidden" style="width:300px; height: 26px;" name="media_hidden" id="article_image_hidden<?php echo $a['id'];?>" value="<?php echo $a['media']; ?>"/><br />
										<input type="file" name="edit_upload_img" style="margin-top: 5px;"/><br />
								<?php
									}else {
									?>
										<div style="font-size: 13px;">Видео линк</div>
										<input type="text" style="width:300px; height: 26px;" name="media_hidden" value="<?php echo $a['media']; ?>"/><br />
									<?php
									}
								?>
                                <br><br>
                                <textarea name="text_bg" style="width:800px;height:420px;resize: none;"><?php echo $a['text_bg']; ?></textarea><BR />
                                <textarea name="text_en" style="width:800px;height:420px;resize: none;"><?php echo $a['text_en']; ?></textarea><BR />
                                <textarea name="text_ro" style="width:800px;height:420px;resize: none;"><?php echo $a['text_ro']; ?></textarea><BR />
								<div class="a-tags">
									<div style="margin-top: 10px; font-size: 13px;">Тагове</div>
									<?php
									$tags = array();
									$tag = $pdo->prepare("SELECT * FROM `help_tags_zapisi` WHERE `article_id` = ?");
									$tag -> bindValue(1, (int)$a['id'], PDO::PARAM_INT);
									$tag -> execute();
									foreach ($tag ->fetchAll() as $t) {
										$tags[] = $t['tag_id'];
									}

									$stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `cat_id` = ? ORDER BY `id` ASC");
									$stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
									$stm -> execute();
									foreach ($stm->fetchAll() as $c) {
										echo '<input type="checkbox" name="article_tags[]" value="'.$c['id'].'" ';
										if(in_array($c['id'], $tags)) echo "checked";
										echo ' > '.$c['name_bg'].'<br />';
									}
									?>
								</div>			
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<button type="submit" name="SaveArticle" class="button-save">save</button>								
								<button type="submit" name="DeleteArticle" class="button-cancel">delete</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<?php
		}
	} else if(isset($_GET['id']) && isset($_GET['t'])){
		$id = $_GET['id'];
		echo "<input id='CategoryId' type='hidden' value='".$id."'>";
		if(isset($err) && $err != ""){
			echo "<div class='erroradd'>Моля въведете ".$err."</div>";
			?>
			<style>
			#AddArticleDiv {
				display: block;
			}
			</style>
			<?php
		}
		?>

			<a class="addbutton" href="#" onclick="ShowAddArticle(); return false;">добави информация</a>
			<div id="AddArticleDiv" style="margin-left: 10px;">
				<form enctype="multipart/form-data" action="" method="post">
					<table cellspacing="10">
						<tr>
							<td valign="top">
                                <div class="input-tab-holder" style="margin-top: 10px; width: 300px;">
                                    <div class="input-tab-title">заглавие</div>
                                       <div class="input-tab-trigger-ro-new-info">ro</div>
                                       <div class="input-tab-trigger-en-new-info">en</div>
                                       <div class="input-tab-trigger-bg-new-info">bg</div>
                                </div>
                                <input name="title_BG" style="width:300px; height: 26px;" value="<?php if(isset($_POST['title_BG'])) echo $_POST['title_BG'];?>" class="input-tab-bg-new-info"/>    
                                <input name="title_EN" style="width:300px; height: 26px;" value="<?php if(isset($_POST['title_EN'])) echo $_POST['title_EN'];?>" class="input-tab-en-new-info"/>
                                <input name="title_RO" style="width:300px; height: 26px;" value="<?php if(isset($_POST['title_RO'])) echo $_POST['title_RO'];?>" class="input-tab-ro-new-info"/>
                                <br><br>

								<img id="article_image" src="<?php echo url.'/images/blank_400X300.png'; ?>" style="width:720px;" />
								<div id="newinfooptions" style="padding-bottom: 5px;"><br />
									<select name="type" style="height: 26px;" id="article_type">
										<option value="photo">Снимка</option>
										<option value="video">youtube/vimeo видео</option>
									</select>
									<script type="text/javascript">
										$('#article_type').change(function() {
											if($(this).val()=="video"){
												$('#article_image_hidden').val('');
												$('#article_image_hidden').detach().attr('type', 'text').insertAfter("#newinfooptions");
												$('#article_thickbox').css("display","none");
												$('#article_image').css("display","none");
												$('#upload_img').css("display","none");
											}else{
												$('#article_image_hidden').val('');
												$('#article_image_hidden').detach().attr('type', 'hidden').insertAfter("#newinfooptions");
												$('#upload_img').css("display","block");
												$('#article_image').css("display","block");
											}
										});
									</script>
								</div>
								<input type="hidden" style="width:300px; height: 26px;" name="media_hidden" id="article_image_hidden" value="<?php if(isset($_POST['media_hidden'])) echo $_POST['media_hidden'];?>" style="margin-top: 5px;"/>
								<input type="file" name="upload_img" id="upload_img"/>
								<br /><br />
                                <textarea name="text_bg" style="width:800px;height:420px;resize: none;"><?php if(isset($_POST['text_bg'])) echo $_POST['text_bg'];?></textarea><BR />
                                <textarea name="text_en" style="width:800px;height:420px;resize: none;"><?php if(isset($_POST['text_en'])) echo $_POST['text_en'];?></textarea><BR />
                                <textarea name="text_ro" style="width:800px;height:420px;resize: none;"><?php if(isset($_POST['text_ro'])) echo $_POST['text_ro'];?></textarea><BR />
								<div class="a-tags">
									<div style="margin-top: 10px">Тагове</div>
									<?php
									$stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `cat_id` = ? ORDER BY `id` ASC");
									$stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
									$stm -> execute();
									foreach ($stm->fetchAll() as $c) {
										echo '<input type="checkbox" name="article_tags[]" value="'.$c['id'].'" > '.$c['name_bg'].'<br />';
									}
									?>
								</div>		
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<button type="submit" name="addArticle" class="button-save">save</button>								
								<button type="reset" name="CancelAddArticle" class="button-cancel">cancel</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		<?php
		$articles = $pdo->prepare("SELECT * FROM `help_tags_zapisi` WHERE `tag_id`=? ORDER BY `id` ASC");
		$articles->bindValue(1, (int)$_GET['t'], PDO::PARAM_INT);
        $articles->execute();
        foreach($articles->fetchAll() as $c){
        	$art = $pdo->prepare("SELECT * FROM `help_articles` WHERE `id`=? LIMIT 1");
			$art->bindValue(1, (int)$c['article_id'], PDO::PARAM_INT);
            $art->execute();
            $a = $art->fetch();
			?>
			<div class="a-link" style="font-size: 13px;" id="a-link-<?php echo $a['id'];?>" onclick="toggleCont('<?php echo $a['id'];?>')"><?php echo $a['title_bg']; ?></div>
			<div class="ArticleDiv" id="a-<?php echo $a['id'];?>">
				<form enctype="multipart/form-data" action="" method="post">
					<table cellspacing="10">
						<tr>
							<td valign="top">
								<input type="hidden" name="articleid" value="<?php echo $a['id'];?>">		
								<div style="font-size: 13px;">Заглавие bg</div>
								<div style="margin: 0px 0 10px;" ><input name="title_BG"  style="width:300px; height: 26px;" value="<?php echo $a['title_bg']; ?>"/></div>		
								<div style="font-size: 13px;">Заглавие en</div>
								<div style="margin: 0px 0 10px;" ><input name="title_EN"  style="width:300px; height: 26px;" value="<?php echo $a['title_en']; ?>"/></div>
                                <?php
									if($a['type'] == "photo"){
								?>					
										<?php if($a['media'] != "") {?>
											<img id="article_image<?php echo $a['id'];?>" src="<?php echo url.$a['media'];?>" style="width:300px;"/>
										<?php } ?>
										<input type="hidden" style="width:300px; height: 26px;" name="media_hidden" id="article_image_hidden<?php echo $a['id'];?>" value="<?php echo $a['media']; ?>"/><br />
										<input type="file" name="edit_upload_img" style="margin-top: 5px;"/><br />
								<?php
									}else {
									?>
										<div>Видео линк</div>
										<input type="text" style="width:300px; height: 26px;" name="media_hidden" value="<?php echo $a['media']; ?>"/><br />
									<?php
									}
								?>
								<div class="a-tags">
									<div style="margin-top: 10px">Тагове</div>
									<?php
									$tags = array();
									$tag = $pdo->prepare("SELECT * FROM `help_tags_zapisi` WHERE `article_id` = ?");
									$tag -> bindValue(1, (int)$a['id'], PDO::PARAM_INT);
									$tag -> execute();
									foreach ($tag ->fetchAll() as $t) {
										$tags[] = $t['tag_id'];
									}

									$stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `cat_id` = ? ORDER BY `id` ASC");
									$stm -> bindValue(1, (int)$_GET['id'], PDO::PARAM_INT);
									$stm -> execute();
									foreach ($stm->fetchAll() as $c) {
										echo '<input type="checkbox" name="article_tags[]" value="'.$c['id'].'" ';
										if(in_array($c['id'], $tags)) echo "checked";
										echo ' > '.$c['name_bg'].'<br />';
									}
									?>
								</div>
							</td>
							<td valign="top">								
								<textarea name="text_bg" style="width:800px;height:420px;resize: none;"><?php echo $a['text_bg']; ?></textarea><BR />
								<textarea name="text_en" style="width:800px;height:420px;resize: none;"><?php echo $a['text_en']; ?></textarea><BR />
								<textarea name="text_ro" style="width:800px;height:420px;resize: none;"><?php echo $a['text_ro']; ?></textarea><BR />
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<button type="submit" name="SaveArticle" class="button-save">save</button>								
								<button type="submit" name="DeleteArticle" class="button-cancel">delete</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<?php
		}
	} else {
		if(isset($err) && $err != ""){
			echo "<div class='erroradd'>".$err."</div>";
		}
		?>
		<form enctype="multipart/form-data" action="" method="post">
			<?php
			$stm = $pdo->prepare("SELECT `value` FROM `dict` WHERE `key` = 'info_img' ");
			$stm -> execute();
			$i = $stm->fetch();
			$img = $i['value'];
			echo '<img src="'.url.$img.'" alt="">';
			?>
			<br />
			<input type="file" name="upload_img" style="margin-top: 5px;"/><br />
			<button type="submit" style="margin-top: 15px;" class="button-save" name="SaveImg">save</button>
			<button style="margin-left: 10px;" class="button-cancel" type="button" onclick="location.reload(); return false;">cancel</button>
		</form>
		<?php
	}
?>
</div>
</div>
<div style="clear:both;height: 60px;"></div>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>

<script>
    $(document).ready(function(){
        $(".input-tab-trigger-en-help").click(function(){
            $(".input-tab-bg-help").hide();
            $(".input-tab-ro-help").hide();
            $(".input-tab-en-help").show();
            $(".input-tab-trigger-en-help").css("background-color","#666666");
            $(".input-tab-trigger-en-help").css("color","#fff");
            $(".input-tab-trigger-bg-help").css("background-color","#fff");
            $(".input-tab-trigger-bg-help").css("color","#333");
            $(".input-tab-trigger-ro-help").css("background-color","#fff");
            $(".input-tab-trigger-ro-help").css("color","#333");
        });

        $(".input-tab-trigger-bg-help").click(function(){
            $(".input-tab-bg-help").show();
            $(".input-tab-en-help").hide();
            $(".input-tab-ro-help").hide();
            $(".input-tab-trigger-en-help").css("background-color","#fff");
            $(".input-tab-trigger-en-help").css("color","#333");
            $(".input-tab-trigger-ro-help").css("background-color","#fff");
            $(".input-tab-trigger-ro-help").css("color","#333");
            $(".input-tab-trigger-bg-help").css("background-color","#666666");
            $(".input-tab-trigger-bg-help").css("color","#fff");
        });

        $(".input-tab-trigger-ro-help").click(function(){
            $(".input-tab-ro-help").show();
            $(".input-tab-bg-help").hide();
            $(".input-tab-en-help").hide();
            $(".input-tab-trigger-en-help").css("background-color","#fff");
            $(".input-tab-trigger-en-help").css("color","#333");
            $(".input-tab-trigger-bg-help").css("background-color","#fff");
            $(".input-tab-trigger-bg-help").css("color","#333");
            $(".input-tab-trigger-ro-help").css("background-color","#666666");
            $(".input-tab-trigger-ro-help").css("color","#fff");
        });

        $(".input-tab-bg-help-tag").keyup(function () {
            var val_bg = $('.input-tab-bg-help-tag').val();
            $('.input-tab-en-help-tag').val(val_bg);
            $('.input-tab-ro-help-tag').val(val_bg);
        });

        $(".input-tab-trigger-en-help-tag").click(function(){
            $(".input-tab-bg-help-tag").hide();
            $(".input-tab-ro-help-tag").hide();
            $(".input-tab-en-help-tag").show();
            $(".input-tab-trigger-en-help-tag").css("background-color","#666666");
            $(".input-tab-trigger-en-help-tag").css("color","#fff");
            $(".input-tab-trigger-bg-help-tag").css("background-color","#fff");
            $(".input-tab-trigger-bg-help-tag").css("color","#333");
            $(".input-tab-trigger-ro-help-tag").css("background-color","#fff");
            $(".input-tab-trigger-ro-help-tag").css("color","#333");
        });

        $(".input-tab-trigger-bg-help-tag").click(function(){
            $(".input-tab-bg-help-tag").show();
            $(".input-tab-en-help-tag").hide();
            $(".input-tab-ro-help-tag").hide();
            $(".input-tab-trigger-en-help-tag").css("background-color","#fff");
            $(".input-tab-trigger-en-help-tag").css("color","#333");
            $(".input-tab-trigger-ro-help-tag").css("background-color","#fff");
            $(".input-tab-trigger-ro-help-tag").css("color","#333");
            $(".input-tab-trigger-bg-help-tag").css("background-color","#666666");
            $(".input-tab-trigger-bg-help-tag").css("color","#fff");
        });

        $(".input-tab-trigger-ro-help-tag").click(function(){
            $(".input-tab-ro-help-tag").show();
            $(".input-tab-en-help-tag").hide();
            $(".input-tab-bg-help-tag").hide();
            $(".input-tab-trigger-en-help-tag").css("background-color","#fff");
            $(".input-tab-trigger-en-help-tag").css("color","#333");
            $(".input-tab-trigger-bg-help-tag").css("background-color","#fff");
            $(".input-tab-trigger-bg-help-tag").css("color","#333");
            $(".input-tab-trigger-ro-help-tag").css("background-color","#666666");
            $(".input-tab-trigger-ro-help-tag").css("color","#fff");
        });

        $(".input-tab-trigger-en-edit-tag").click(function(){
            $(".input-tab-bg-edit-tag").hide();
            $(".input-tab-ro-edit-tag").hide();
            $(".input-tab-en-edit-tag").show();
            $(".input-tab-trigger-en-edit-tag").css("background-color","#666666");
            $(".input-tab-trigger-en-edit-tag").css("color","#fff");
            $(".input-tab-trigger-bg-edit-tag").css("background-color","#fff");
            $(".input-tab-trigger-bg-edit-tag").css("color","#333");
            $(".input-tab-trigger-ro-edit-tag").css("background-color","#fff");
            $(".input-tab-trigger-ro-edit-tag").css("color","#333");
        });

        $(".input-tab-trigger-bg-edit-tag").click(function(){
            $(".input-tab-bg-edit-tag").show();
            $(".input-tab-en-edit-tag").hide();
            $(".input-tab-ro-edit-tag").hide();
            $(".input-tab-trigger-en-edit-tag").css("background-color","#fff");
            $(".input-tab-trigger-en-edit-tag").css("color","#333");
            $(".input-tab-trigger-ro-edit-tag").css("background-color","#fff");
            $(".input-tab-trigger-ro-edit-tag").css("color","#333");
            $(".input-tab-trigger-bg-edit-tag").css("background-color","#666666");
            $(".input-tab-trigger-bg-edit-tag").css("color","#fff");
        });

        $(".input-tab-trigger-ro-edit-tag").click(function(){
            $(".input-tab-ro-edit-tag").show();
            $(".input-tab-bg-edit-tag").hide();
            $(".input-tab-en-edit-tag").hide();
            $(".input-tab-trigger-en-edit-tag").css("background-color","#fff");
            $(".input-tab-trigger-en-edit-tag").css("color","#333");
            $(".input-tab-trigger-bg-edit-tag").css("background-color","#fff");
            $(".input-tab-trigger-bg-edit-tag").css("color","#333");
            $(".input-tab-trigger-ro-edit-tag").css("background-color","#666666");
            $(".input-tab-trigger-ro-edit-tag").css("color","#fff");
        });

        $(".input-tab-trigger-en-edit-tag-2").click(function(){
            $(".input-tab-bg-edit-tag-2").hide();
            $(".input-tab-ro-edit-tag-2").hide();
            $(".input-tab-en-edit-tag-2").show();
            $(".input-tab-trigger-en-edit-tag-2").css("background-color","#666666");
            $(".input-tab-trigger-en-edit-tag-2").css("color","#fff");
            $(".input-tab-trigger-bg-edit-tag-2").css("background-color","#fff");
            $(".input-tab-trigger-bg-edit-tag-2").css("color","#333");
            $(".input-tab-trigger-ro-edit-tag-2").css("background-color","#fff");
            $(".input-tab-trigger-ro-edit-tag-2").css("color","#333");
        });

        $(".input-tab-trigger-bg-edit-tag-2").click(function(){
            $(".input-tab-bg-edit-tag-2").show();
            $(".input-tab-en-edit-tag-2").hide();
            $(".input-tab-ro-edit-tag-2").hide();
            $(".input-tab-trigger-en-edit-tag-2").css("background-color","#fff");
            $(".input-tab-trigger-en-edit-tag-2").css("color","#333");
            $(".input-tab-trigger-ro-edit-tag-2").css("background-color","#fff");
            $(".input-tab-trigger-ro-edit-tag-2").css("color","#333");
            $(".input-tab-trigger-bg-edit-tag-2").css("background-color","#666666");
            $(".input-tab-trigger-bg-edit-tag-2").css("color","#fff");
        });

        $(".input-tab-trigger-ro-edit-tag-2").click(function(){
            $(".input-tab-ro-edit-tag-2").show();
            $(".input-tab-bg-edit-tag-2").hide();
            $(".input-tab-en-edit-tag-2").hide();
            $(".input-tab-trigger-en-edit-tag-2").css("background-color","#fff");
            $(".input-tab-trigger-en-edit-tag-2").css("color","#333");
            $(".input-tab-trigger-bg-edit-tag-2").css("background-color","#fff");
            $(".input-tab-trigger-bg-edit-tag-2").css("color","#333");
            $(".input-tab-trigger-ro-edit-tag-2").css("background-color","#666666");
            $(".input-tab-trigger-ro-edit-tag-2").css("color","#fff");
        });

        $(".input-tab-trigger-en-new-info").click(function(){
            $(".input-tab-bg-new-info").hide();
            $(".input-tab-ro-new-info").hide();
            $(".input-tab-en-new-info").show();
            $(".input-tab-trigger-en-new-info").css("background-color","#666666");
            $(".input-tab-trigger-en-new-info").css("color","#fff");
            $(".input-tab-trigger-bg-new-info").css("background-color","#fff");
            $(".input-tab-trigger-bg-new-info").css("color","#333");
            $(".input-tab-trigger-ro-new-info").css("background-color","#fff");
            $(".input-tab-trigger-ro-new-info").css("color","#333");
        });

        $(".input-tab-trigger-bg-new-info").click(function(){
            $(".input-tab-bg-new-info").show();
            $(".input-tab-en-new-info").hide();
            $(".input-tab-ro-new-info").hide();
            $(".input-tab-trigger-en-new-info").css("background-color","#fff");
            $(".input-tab-trigger-en-new-info").css("color","#333");
            $(".input-tab-trigger-ro-new-info").css("background-color","#fff");
            $(".input-tab-trigger-ro-new-info").css("color","#333");
            $(".input-tab-trigger-bg-new-info").css("background-color","#666666");
            $(".input-tab-trigger-bg-new-info").css("color","#fff");
        });

        $(".input-tab-trigger-ro-new-info").click(function(){
            $(".input-tab-ro-new-info").show();
            $(".input-tab-bg-new-info").hide();
            $(".input-tab-en-new-info").hide();
            $(".input-tab-trigger-en-new-info").css("background-color","#fff");
            $(".input-tab-trigger-en-new-info").css("color","#333");
            $(".input-tab-trigger-bg-new-info").css("background-color","#fff");
            $(".input-tab-trigger-bg-new-info").css("color","#333");
            $(".input-tab-trigger-ro-new-info").css("background-color","#666666");
            $(".input-tab-trigger-ro-new-info").css("color","#fff");
        });
    });
</script>