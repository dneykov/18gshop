<?php
require '__top.php';
$page_current = 'users';

require('template/__top.php');
/* @var $artikul artikul_admin */
?>

<style>
    #tpmenu{
        idth: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #tpmenu a{
        color: #333;
        font-size: 13px;
        margin:0 15px;
    }

    #tpmenu a:hover {
        color: rgb(248, 147, 29);
        text-decoration: none;
    }
    .users td{
        font-size: 9pt;
        white-space: nowrap;
        overflow: hidden;
        o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        height: 20px;
    }
    .users {
        margin-left: 10px;
        max-width: 1000px;
        border: none;
        margin-bottom: 50px;
    }
    .users tr {
        height: 20px;
    }
    .users a {
        color: #333;
    }
    .users tr td a{
        width: 100%;
        display: block;
    }
   .users tr:nth-child(even){
        background-color: #fff;
        color: #333;
    }
    .users tr:nth-child(odd){
        background-color: #f2f2f2;
        color: #333;
    }
    #tablehead:hover {
        background-color: #999 !important;
    }
    .productLink:hover{
        text-decoration:none;
    }
    .users{
        border-collapse: collapse;
    }
</style>
<div id="tpmenu">
    <?php if($__user->permission_check('администратори', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users_admin.php" <?php  ?>>администратори</a>
    <?php } ?>
    <?php if($__user->permission_check('потребители', 'r', true,true)) { ?>
        <a href="<?php echo url_admin; ?>users.php">клиенти</a>
<!--        <a href="--><?php //echo url_admin; ?><!--partners.php">партньори</a>-->
        <a href="<?php echo url_admin; ?>newsletter.php" style="color: orange;">абонирани за новини</a>
    <?php } ?>
</div>
<table class="users" cellpadding="3">
    <tr id="tablehead" style="background-color: #666666; color: #fff;">
        <td style="padding-left:5px;width: 150px; padding-right:10px;">И-мейл</td>
        <td style="padding-left:5px;width: 150px; padding-right:10px;"></td>
        <td style="padding-left:5px;width: 100px; padding-right:10px;">Абониран на</td>
        <td style="padding-left:5px;width: 50px; padding-right:10px;;"></td>
        <td></td>
    </tr>
        <?php
        $stm = $pdo->prepare('SELECT * FROM `newsletter_members` ORDER BY `id` DESC');
        $stm -> execute();
        $subscribers = $stm->fetchAll();
        foreach($subscribers as $user) : ?>
        <tr>
            <td style="padding-left:5px;max-width: 250px; padding-right:10px;" class="ime-na-email-newsletter-klient-<?php echo $user['id']; ?>"><?php echo $user['email']; ?></td>
            <td style="padding-left:5px;max-width: 200px; padding-right:10px;"></td>
            <td style="padding-left:5px;max-width: 200px; padding-right:10px;"><?php echo date("d-m-Y",$user['created_at']); ?></td>
            <td style="padding-left:5px;max-width: 50px; padding-right:10px;text-align: center;"></td>
            <td style="padding-left:5px;padding-right:10px"><a href="#" onclick="unsubscribe(<?php echo $user['id']; ?>);return false;"><?php lang_delete_x(); ?></a></td>
        </tr>
    <?php endforeach; ?>
</table>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>