<?php
include '__top.php';
if(isset($_GET['ord'])){
    //var_dump($_GET['ord']);
    foreach($_GET['ord'] as $_id => $_ordr){
        //echo $_ordr;
        $stm = $pdo -> prepare('UPDATE `vid` SET `ordr`=? WHERE `id`=?');
        $stm -> bindValue(1, $_ordr, PDO::PARAM_INT);
        $stm -> bindValue(2, $_id, PDO::PARAM_INT);
        $stm -> execute();
    }
    ?>

    <script type="text/javascript">
        parent.$.fancybox.close();
        parent.location.reload();
    </script>
    <?php
}else{
    ?>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/core.js"></script>
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/events.js"></script>
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/css.js"></script>
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/coordinates.js"></script>
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/drag.js"></script>
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/dragsort.js"></script>
        <script language="JavaScript" type="text/javascript" src="ajax/tool-man/cookies.js"></script>
        <style type="text/css">
            body{
                background-color: #fff;
                color: #333;
                font-family: Verdana, sans-serif;
                font-size: 13px;
                padding: 0;
                margin: 0;
            }
             .button-save{
                width: 100px;
                height: 23px;
                background-color: #666666;
                padding-bottom: 3px;
                color: #fff;
                border: none;
                cursor: pointer;
            }
            .button-save:hover{
                background-color: #ff9207;
            }
            .button-cancel{
                width: 100px;
                height: 23px;
                background-color: #666666;
                padding-bottom: 3px;
                color: #fff;
                border: none;
                cursor: pointer;
                margin-left: 10px;
            }
            .button-cancel:hover{
                background-color: #ff9207;
            }
            div > div{
                padding:0px 20px;
                height: inherit;
            }
            ul{
                list-style-type:none;
                margin:20px 0px;
                padding:0px;
            }
            li{
                cursor:move;
                margin:0px;
                padding:0px;
                color: #333;
                font-family: Verdana, sans-serif;
                font-size: 13px;
            }
            input{

                float:left;
            }
            p{
                display:block;
                clear:both;
                margin:70px 20px 0px;
                width:300px;
                color: #333;
                font-family: Verdana, sans-serif;
                font-size: 13px;
            }
            html {
                overflow-x: hidden;
                overflow-y: auto;
            }
            .header {
                background-color: #666666;
                height: 30px;
            }
            .header span {
                color: #fff;
                line-height: 28px;
                display: inline-block;
                font-family: Verdana, sans-serif;
                font-size: 14px;
            }
            #main {
                max-width: 800px;
                width: 95%;
                margin: 0;
                font-family: Verdana, sans-serif;
                font-size: 14px;
                height: inherit;
            }
            #modeList {
                height: inherit;
                display: inline-block;
            }
        </style>
    </head>
    <body>
    <div id="main">
        <div class="header" style="background-color: #666666;">
            <span>Сортиране видове</span>
        </div>
        <script language="JavaScript" type="text/javascript"><!--
            var dragsort = ToolMan.dragsort();
            var junkdrawer = ToolMan.junkdrawer();
            window.onload = function() {
                junkdrawer.restoreListOrder("modeList");
                dragsort.makeListSortable(document.getElementById("modeList"),verticalOnly)
            }
            function verticalOnly(item) {
                item.toolManDragGroup.verticalOnly()
            }
            function speak(id, what) {
                var element = document.getElementById(id);
                element.innerHTML = 'Clicked ' + what;
            }

            function save(id){
                var list=document.getElementById(id);
                var items = list.getElementsByTagName("li");
                var array = new Array();
                var rp="?";
                for (var i = 0, n = items.length; i < n; i++) {
                    var item = items[i];
                    //alert(item.getAttribute("categoryID"))
                    //window.location = '?id=1';
                    rp=rp+"&ord["+item.getAttribute("categoryID")+"]="+(i+1);
                }
                //alert(rp)
                window.location =rp;
            }
            //-->
        </script>
        <?php
        $stm = $pdo -> prepare('SELECT * FROM `vid` where `id_kategoriq`=? ORDER BY `ordr`');
        $stm -> bindValue(1, $_GET['cat'], PDO::PARAM_INT);
        $stm -> execute();
        if($stm->rowCount() == 0 ) $category = NULL;
        else{
            echo "<div>";
            echo "<ul id='modeList'>";
            foreach ($stm->fetchAll() as $c) {
                echo '<li categoryID="'.$c['id'].'">';
                echo $c['ime_'.lang_prefix];
                echo '</li>';
            }
            echo "</ul>";
        }
        ?>
        <div style="clear:both;"></div>
        <input type="button" value="Save" class="button-save" onClick="save('modeList')"/>
        <input type="button" value="cancel" class="button-cancel" onClick="save('modeList')"/>
    </div>
    </body>
    </html>
    <?php
}
?>
