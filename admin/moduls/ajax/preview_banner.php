<?php
	require '../../__top.php';
	$id = (int)$_POST['id'];

	$stm = $pdo->prepare('SELECT * FROM `zz_banners` WHERE id = ? LIMIT 1');
	$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
	$stm -> execute();
	$v = $stm -> fetch();
	$adv_texts = unserialize($v['adv_texts_temp_' . lang_prefix]);
?>

<img id="adv_file" style="width: 1260px; height: 400px;" src="<?php echo url.$v['image']; ?>">
	<?php
		foreach($adv_texts as $text){

			list($r, $g, $b) = sscanf($text['bgcolor'], "#%02x%02x%02x");

			echo '<div class="adv-text adv-place-'.$text['place'].'" style="background-color: rgba('.$r.', '.$g.', '.$b.', ';
				if((int)$text['opacity'] == 100) echo '1';
				else echo '0.'.$text['opacity'];
			echo '); color: '.$text['color'].'">';
			echo stripcslashes($text['content']);
			echo '</div>';
		}
	?>
</div>