<?php
	require '../../__top.php';

	$id = $_POST['id'];
	$place = $_POST['place'];

	$stm = $pdo->prepare('SELECT * FROM `zz_banners` WHERE id = ? LIMIT 1');
	$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
	$stm -> execute();
	$adv = $stm -> fetch();

    foreach ($__languages as $key => $v)  {
        $adv_texts = unserialize($adv['adv_texts_temp_' . $v->getPrefix()]);

        unset($adv_texts['text-'.$place]);

        $stm = $pdo->prepare('UPDATE `zz_banners` SET `adv_texts_temp_' . $v->getPrefix() . '` = ? WHERE id = ? LIMIT 1');
        $stm -> bindValue(1, serialize($adv_texts), PDO::PARAM_STR);
        $stm -> bindValue(2, (int)$id, PDO::PARAM_INT);
        $stm -> execute();
    }
?>