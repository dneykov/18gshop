<?php
	require '../../__top.php';

	$postData = array();
	foreach ($_POST as $key => $value) {
        $postData[str_replace(array("new-text-", "text-"), "", $key)] = $value;
    }
    
	$id = $postData['id'];
	$place = $postData['place'];
	$color = $postData['color'];
	$bgcolor = $postData['bgcolor'];
	$opacity = $postData['opacity'];

	$stm = $pdo->prepare('SELECT * FROM `zz_banners` WHERE id = ? LIMIT 1');
	$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
	$stm -> execute();
	$adv = $stm -> fetch();

    foreach ($__languages as $key => $v)  {
        $adv_texts = unserialize($adv['adv_texts_temp_' . $v->getPrefix()]);

        if(!isset($adv_texts['text-'.$place])) {
            $adv_texts['text-'.$place] = array();
        }

        $adv_texts['text-'.$place]['place'] = $place;
        $adv_texts['text-'.$place]['color'] = $color;
        $adv_texts['text-'.$place]['bgcolor'] = $bgcolor;
        $adv_texts['text-'.$place]['opacity'] = $opacity;
        $adv_texts['text-'.$place]['content'] = str_replace('\"','\'',$postData['content_' . $v->getPrefix()]);

        $stm = $pdo->prepare('UPDATE `zz_banners` SET `adv_texts_temp_' . $v->getPrefix() . '` = ? WHERE id = ? LIMIT 1');
        $stm -> bindValue(1, serialize($adv_texts), PDO::PARAM_STR);
        $stm -> bindValue(2, (int)$id, PDO::PARAM_INT);
        $stm -> execute();
    }
?>
<br />
<div id="text-link-<?php echo $place; ?>" class="texts-links" onclick="toggleTexts('<?php echo $place; ?>')"><?php echo strip_tags(str_replace('\"','\'',$postData['content_' . lang_prefix]), '<div><p><strong><em><span>'); ?></div>
<div id="text-<?php echo $place; ?>" class="baner-texts">
	<table>
		<tr>
			<td style="padding-right: 30px; font-size: 13px">
				Позиция
			</td>
			<td  style="padding-right: 30px; font-size: 13px">
				Текст
			</td>
			<td  style="padding-right: 30px; font-size: 13px">
				Контейнер
			</td>
			<td  style="padding-right: 30px; font-size: 13px">
				Плътност <span class="text-show-opacity-<?php echo $place; ?>"><?php echo $opacity; ?></span> %
			</td>
		</tr>
		<tr>
			<td>
				<table>
					<?php 
					for($i = 1; $i <= 9; $i++){
						if($i == 1){
							echo "<tr>";
						}
						echo '<td><input type="radio" value="1"';
							if($i == (int)$place) echo " checked";
							else echo " disabled";
						echo '></td>';
						if($i%3 == 0){
							echo "</tr><tr>";
						}
						if($i == 9){
							echo "</tr>";
						}
					}
					?>
				</table>
			</td>
			<td>
				<input type="color" name="text-color" id="text-color-<?php echo $place; ?>" value="<?php echo $color; ?>" style="cursor:pointer">
			</td>
			<td>
				<input type="color" name="text-bgcolor" id="text-bgcolor-<?php echo $place; ?>" value="<?php echo $bgcolor; ?>" style="cursor:pointer">
			</td>
			<td style="font-size: 13px; cursor: pointer">
				0 <input type="range"name="text-opacity" id="text-opacity-<?php echo $place; ?>" value="<?php echo $opacity; ?>" min="00" max="100" step="10" onchange="showPrecentageChange('<?php echo $place; ?>');"> 100
			</td>
		</tr>
	</table>
    <?php foreach ($__languages as $key => $v)  { ?>
        <div><?php echo _($v->getName()); ?></div>
        <textarea style="width:1000px;height:50px;" class="texts-class" id="text-content-<?php echo $place; ?>_<?php echo $v->getPrefix(); ?>" name="text-content_<?php echo $v->getPrefix(); ?>"><?php echo str_replace('\"','\'',$postData['content_' . $v->getPrefix()]); ?></textarea><br />
    <?php } ?>
	<input type="submit" value="apply" id="text-update" style="margin-top: 10px;" onclick="EditText('<?php echo $place; ?>'); return false;">
	<input type="submit" value="delete" style="margin-top: 10px;" onclick="DeleteText('<?php echo $place; ?>'); return false;">
</div>