<?php
require '../__top.php';
require '../template/__top.php';
?>
<script>
    $(document).ready(function(){
        $('#addcollection').click(function() {
            var name = $('#name').val();
            var err = "";
            var flag = false;
            if(name == ""){
                err += "Трябва да въведете име на колекциятa.";
                flag = true;
            }
            if(flag == false){
                $.ajax({
                    type: "POST",
                    url: "collections/ajax_add.php",
                    data: "name="+name,
                    cache: false,
                    success: function(){
                        location.reload();
                    }
                });
            }
            else {
                $('#errors').html("<span style='display: block; margin-top: 5px; font-size: 12px; color: red;'>"+err+"</span>");
            }
        });
        $('#canceladdcollection').click(function(){
            location.reload();
        });
        $('#canceleditcollection').click(function(){
            location.reload();
        });
    });
    function ShowAddCollection(){
        if ($("#addCollectionDiv").is(":hidden")) {
            $("#addCollectionDiv").fadeIn(300);
        } else {
            $("#addCollectionDiv").hide(300);
        }
    }
    function editCollection(id){
        var ID = "#respDiv"+id;
        if ($(ID).is(":hidden")) {
            if($(".respDIV").is(':visible')){
                $(".respDIV").hide();
            }
            $("#actionsDiv"+id).hide();
            $(ID).show();
            $("#edit_name"+id).show();
            $(".collection_original_name_"+id).hide();
        } else {
            $(ID).hide();
            $("#actionsDiv"+id).show();
            $("#edit_name"+id).hide();
            $(".collection_original_name_"+id).show();
        }
    }
    function updateCollection(id){
        var name = $('#edit_name'+id).val();
        $.ajax({
            type: "POST",
            url: "collections/ajax_update_collection.php",
            data: "id="+id+"&name="+name,
            cache: false,
            success: function(){
                location.reload();
            }
        });
    }
    function deleteCollection(id){
        ime_na_kol = $(".collection_original_name_"+id).text();
        $(".modal-body").html(ime_na_kol + ' <button class="button-cancel" onclick="hideModal();" style="margin-top: 100px; display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deleteCollectionAjax(\'' + id + '\');" style="display: block; margin-top: 100px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
        $(".modal").show();
    }
    function deleteCollectionAjax(id){
        $.ajax({
            type: "POST",
            url: "collections/ajax_delete_collection.php",
            data: "id="+id,
            cache: false,
            success: function(html){
                location.reload();
            }
        });
    }
    function hideModal(){
        $('.modal').hide();
    }
    function cancelUpdateCollection(id){
        editCollection(id);
    }
</script>
<style>
    .respDIV{
        display: none;
    }
    #addCollectionDiv {
        display: none;
        margin-left: 10px;
        margin-top: 52px;
    }
    #addCollectionDiv table tr td {
        font-size: 13px;
    }
    #promos {
        margin-left: 10px;
        margin-top: 53px;
    }
    #promos div table tr td {
        font-size: 13px;
    }
    .actionsDiv {
    }
    #addCollectionDiv input, #addCollectionDiv select{
        height: 20px;
    }
    .subMenu a{
        color:#333;
        display:inline-block;
        margin:0 15px;
        font-size: 13px;
    }
    .subMenu a:hover, .subMenu a.active{
        color:#F7931E;
    }
</style>

<a class="addbutton nov" href="#" onclick="ShowAddCollection(); return false;">+ колекция</a>
<div id="addCollectionDiv">
    <div id="alt"></div>
    <table>
        <tr>
            <td width="130">
                колекция *
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" id="name" style="width: 300px; height: 26px;"/><br />
            </td>
            <td>
        </tr>
        <tr>
            <td>
                <div id="errors"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top: 20px;">
                <button id="addcollection" class="button-save">save</button>
                <button id="canceladdcollection" class="button-cancel" style="margin-left: 5px;">cancel</button>
            </td>
        </tr>
    </table>
</div>
<div id="promos">
    <?php
    $stm = $pdo->prepare('SELECT * FROM `collections` ORDER BY `name`');
    $stm -> execute();
    foreach($stm->fetchAll() as $p){
        $stm_artikuli = $pdo->prepare('SELECT * FROM `artikuli` WHERE `collection_id` = ?');
        $stm_artikuli -> bindValue(1, $p['id'], PDO::PARAM_INT);
        $stm_artikuli -> execute();
        $artikuli = $stm_artikuli->fetchAll();
        ?>
        <div style="border: 1px solid #CCC; margin-top: 20px; width: 700px;">
            <table style="margin: 5px; width: 525px;">
                <tr style="height: 26px;">
                    <td>
                        <div class="collection_original_name_<?php echo $p['id']?>"><?php echo $p['name'];?></div>
                        <input type='text' id='edit_name<?php echo $p['id']?>' style="display: none; width: 300px;" value="<?php echo $p['name'];?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="margin-top: 10px;"></div>
                        <?php
                        foreach ($artikuli as $a) {
                            $artikul = new artikul_admin($a);

                            echo "<a href='" . url_admin . "product.php?id=" . $artikul->getId() . "' style='margin-bottom: 5px; display: inline-block;'><div style='width: 42px; height: 42px; background-color: #fff; display: flex; margin-right: 5px; float: left; align-items: center;'><img style='max-width: 42px; height: auto; max-height: 42px; display: block; text-align: center; margin: 0 auto; vertical-align: middle;' src='" . url . $artikul->getKartinka_t() . "' alt='' /></div></a>";
                        }
                        ?>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td colspan="2">
                        <div class="actionsDiv" id="actionsDiv<?php echo $p['id']?>">
                            <a href="#" onclick="editCollection('<?php echo $p['id']?>'); return false;" style="display: block; text-align: center; line-height: 25px; font-size: 13px; float: left; padding-bottom: 0px;" class="editCollection_<?php echo $p['id'];?> button-save">edit</a>
                            <a href="#" onclick="deleteCollection('<?php echo $p['id']?>'); return false;" style="display: block; text-align: center; line-height: 25px; font-size: 13px; float: left; padding-bottom: 0px; margin-left: 10px;" class="deleteCollection_<?php echo $p['id'];?> button-cancel">delete</a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="respDIV" id="respDiv<?php echo $p['id']?>">
                            <a href="#" onclick="updateCollection('<?php echo $p['id']?>')" style="display: block; text-align: center; line-height: 25px; font-size: 13px; float: left; padding-bottom: 0px; margin-top: -2px;" class="updateEditCollection_<?php echo $p['id'];?> button-save">save</a>
                            <a href="#" onclick="cancelUpdateCollection('<?php echo $p['id']?>')" style="display: block; text-align: center; line-height: 25px; font-size: 13px; float: left; padding-bottom: 0px; margin-left: 10px; margin-top: -2px; margin-bottom: 2px;" class="cancelEditCollection_<?php echo $p['id'];?> button-save">cancel</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <?php
    }
    ?>
</div>
<div style="clear:both;height: 60px;"></div>
<div style="clear:both;height: 60px;"></div>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>