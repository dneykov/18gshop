<?php
$page_current = 'orders';

require('template/__top.php');

?>
<script>
	$(document).ready(function(){
		$('#addCountry').click(function() {
			var name = $('#name').val();
			var delivery = $('#delivery').val();
			var freedelivery = $('#freedelivery').val();
			var err = "";
			var flag = false;
			if(name == ""){
				err += "Трябва да въведете име. ";
				flag = true;
			}
			if(delivery == ""){
				err += "Трябва да въведете цена на доставката. ";
				flag = true;
			}
			if(freedelivery == ""){
				err += "Трябва да въведете цена за безплатна доставка. ";
				flag = true;
			}
			if(flag == false){
				$.ajax({
		            type: "POST",
		            url: "moduls/locations/ajax_add.php",
		            data: "name="+name+"&delivery="+delivery+"&freedelivery="+freedelivery,
		            cache: false,
		            success: function(html){
		            	location.reload();
		            }
		        });
			} else {
				$('#errors').html("<span style='display: block; margin: 5px; font-size: 12px; color: red;'>"+err+"</span>");
			}
		});
		$('#cancelAddCountry').click(function(){
			location.reload();
		});
    });
	function ShowAddCountry(){
        if ($("#addCountryDiv").is(":hidden")) {
            $("#addCountryDiv").fadeIn(300);
        } else {
            $("#addCountryDiv").hide(300);
        }
	}
	function editCountry(id){
		var ID = "#respDiv"+id;
		var lID = "#actionsDiv"+id;
        if ($(ID).is(":hidden")) {
            if($(".respDIV").is(':visible')){
            	$(".actionsDiv").fadeIn();
                $(".respDIV").hide();
            }
            $(lID).hide(0);
            $(ID).fadeIn(300);
        } else {
        	$(lID).fadeIn(300);
            $(ID).hide(0);
        }
	}
	function updateCountry(id){
		var name = $('#name'+id).val();
		var delivery = $('#delivery'+id).val();
		var freedelivery = $('#freedelivery'+id).val();
		$.ajax({
            type: "POST",
            url: "moduls/locations/ajax_update_country.php",
            data: "id="+id+"&name="+name+"&delivery="+delivery+"&freedelivery="+freedelivery,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}
	function deleteCountry(id){
	    ime_na_kol = $(".ime_na_order_delivery_" + id).text();
	    $(".modal-body").html('<div style="width: 100%; height: 40px;">' + ime_na_kol + '</div> <button class="button-cancel" onclick="hideModal();" style="position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deleteCountry_Ajax(\'' + id + '\');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
	    $(".modal").show();
	}
	function deleteCountry_Ajax(id){
	    $.ajax({
            type: "POST",
            url: "moduls/locations/ajax_delete_country.php",
            data: "id="+id,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}
	function cancelUpdateCountry(id){
		editCountry(id);
	}
</script>
<style type="text/css">
	table td{
		font-size: 10pt;
	}
	.subMenu{
		width: 100%;
	    height: 13px;
	    margin-top: -11px;
	    position: relative;
	    top: -13px;
	    padding-bottom: 10px;
	    padding-top: 0px;
	    box-shadow: 1px 0px 1px #ababab;
	    border-bottom: 1px solid #767676;
	    background-color: #f2f2f2;
	}
	.subMenu a{
		color:#F7931E;
		font-size: 13px;
		margin:0 15px;
	}
	.subMenu a:hover{
		color:#F7931E !important;
	}
	.productLink:hover{
		text-decoration:none;
	}
	.orders{
		border-collapse: collapse;
	}

	.addbutton {
		margin: 30px 5px 30px;
		display: block;
		width: 100px;
	}

	#countrys {
		margin-left: 10px;
	}
	#addCountryDiv {
		display: none;
		margin-left: 5px;
	}

	.respDIV{
		display: none;
	}

	#addCountryDiv input{
		height: 26px;
	}

	.actionsDiv {
		margin-top: 10px;
	}

	#addCountryDiv input{
		height: 26px;
	}

</style>
<div class="subMenu">
    <a href="<?php echo url_admin; ?>orders.php" <?php if((isset($_GET['approved']))||(isset($_GET['done']))||(isset($_GET['cancel']))||(isset($_GET['return']))) echo 'style="color: #333;"' ?>>Нови <?php echo order_admin::getCountForWaiting(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?approved" <?php if(!isset($_GET['approved'])) echo 'style="color: #333;"' ?>>Одобрени <?php echo order_admin::getCountForApproved(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?done" <?php if(!isset($_GET['done'])) echo 'style="color: #333;"' ?>>Изпълнени <?php echo order_admin::getCountForDone(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?cancel" <?php if(!isset($_GET['cancel'])) echo 'style="color: #333;"' ?>>Отказани <?php echo order_admin::getCountForCanceled(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?return" <?php if(!isset($_GET['return'])) echo 'style="color: #333;"' ?>>Върнати <?php echo order_admin::getCountForReturned(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?statistik" style="color: #333;">Отчет</a>
    <a href="<?php echo url_admin; ?>orders.php?delivery">Доставка</a>
    <a href="<?php echo url_admin; ?>orders.php?valute" style="color: #333;">Настройки</a>
</div>
<div id="errors"></div>
<a class="addbutton" href="#" onclick="ShowAddCountry(); return false;" style="color: #fff;background-color: #666666;width: 100px;height: 25px;display: block;text-align: center;margin: 0px 10px -5px;font-size: 13px;line-height: 22px;">+ държава</a>
<div id="addCountryDiv">
	<div id="alt"></div>
	<br>
	<table>
		<tr>
			<td width="230">
				Държава: *
			</td>
			<td>
				<input type="text" id="name" style="width: 200px;"/><br />
			</td>
			<td>
		</tr>
		<tr>
			<td>
				Цена за доставка: *
			</td>
			<td style="padding-top: 5px;">
				<input type="text" id="delivery" style="width: 180px;"/> лв.
			</td>
		</tr>
		<tr>
			<td>
				Безплатна доставка при поръчка над: *
			</td>
			<td style="padding-top: 5px;">
				<input type="text" id="freedelivery" style="width: 180px;"/> лв.
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 30px;">
				<button id="addCountry" class="button-save">save</button>
				<button id="cancelAddCountry" class="button-cancel" style="margin-left: 10px;">cancel</button>
			</td>
		</tr>
	</table>
</div>
<div id="countrys">
	<?php
		$stmcountrys = $pdo->prepare('SELECT * FROM `locations`');
		$stmcountrys -> execute();
		foreach($stmcountrys->fetchAll() as $c){
			?>
			<div style="border: 1px solid #CCC; margin-top: 20px; width: 400px;padding-left: 5px;">
				<table>
					<tr>
						<td width="300">
							Държава:
						</td>
						<td class="ime_na_order_delivery_<?php echo $c['id'];?>">
							<?php echo $c['country_name'];?>
						</td>
						<td>
					</tr>
					<tr>
						<td>
							Цена за доставка:
						</td>
						<td style="padding-top: 5px;">
							<?php echo $c['delivery_price'];?> лв.
						</td>
					</tr>
					<tr>
						<td>
							Безплатна доставка при поръчка над:
						</td>
						<td style="padding-top: 5px;">
							<?php echo $c['free_delivery'];?> лв.
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="actionsDiv" id="actionsDiv<?php echo $c['id'];?>">
								<a href="#" onclick="editCountry('<?php echo $c['id'];?>'); return false;" class="editPromo button-save" style="display: block; line-height: 25px; text-align: center; padding-bottom: 0px; float: left;">edit</a>
								<?php if($c['default'] != 1 && $c['id'] != 2){ ?>
									 <a href="#" onclick="deleteCountry('<?php echo $c['id']?>'); return false;" class="deletePromo button-cancel" style="display: block; line-height: 25px; text-align: center; padding-bottom: 0px; float: left; margin-left: 10px;">delete</a>
								<?php } ?>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="respDIV" id="respDiv<?php echo $c['id']?>">
								<table>
									<tr>
										<td width="230">
											Държава: *
										</td>
										<td>
											<?php
											if($c['default'] == 1){
											?>
												<span style="display: block; width: 200px;"><?php echo $c['country_name'];?></div>
												<input type="hidden" id="name<?php echo $c['id'];?>" style="width: 200px;" value="<?php echo $c['country_name'];?>"/><br />
											<?php
											} else {
											?>
												<input type="text" id="name<?php echo $c['id'];?>" style="width: 200px;" value="<?php echo $c['country_name'];?>"/><br />
											<?php
											}
											?>
										</td>
										<td>
									</tr>
									<tr>
										<td>
											Цена за доставка: *
										</td>
										<td style="padding-top: 5px;">
											<input type="text" id="delivery<?php echo $c['id'];?>" style="width: 170px;" value="<?php echo $c['delivery_price'];?>"/> лв.
										</td>
									</tr>
									<tr>
										<td>
											Безплатна доставка при поръчка над: *
										</td>
										<td style="padding-top: 5px;">
											<input type="text" id="freedelivery<?php echo $c['id'];?>" style="width: 170px;" value="<?php echo $c['free_delivery'];?>"/> лв.
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding-top: 10px;">
											<a href="#" style="display: block; line-height: 25px; text-align: center; padding-bottom: 0px; float: left;" onclick="updateCountry('<?php echo $c['id']?>')" class="button-save">update</a> 
											<a href="#" style="display: block; line-height: 25px; text-align: center; padding-bottom: 0px; float: left; margin-left: 10px;" onclick="cancelUpdateCountry('<?php echo $c['id']?>')" class="button-cancel">cancel</a>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<?php
		}
	?>
</div>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>