<?php
require '../__top.php';
require '../template/__top.php';

if(isset ($_POST['submit'])){

    po_taka_update_multilanguage('text_', 'text_', 'privacy', (int)$_POST['id']);

}

$stm = $pdo->prepare('SELECT * FROM `privacy`');
$stm -> execute();
$data= $stm->fetchall();

?>
<style>
    #navigation {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #navigation a {
        margin-left: 10px;
        margin-right: 30px;
        font-size: 13px;
        color: #333;
    }
</style>
<div id="navigation">
    <a href="<?php echo url."admin/help.php"?>">Oсновна информация</a>
    <a href="<?php echo url."admin/moduls/about.php"?>">Kак да поръчам</a>
    <a href="<?php echo url."admin/files.php"?>">Ценови листи</a>
    <a href="<?php echo url."admin/moduls/privacy.php"?>" style="color: #F8931F">Лични данни</a>
</div>
<script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor",
        theme_advanced_buttons2 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo url_template_folder; ?>images/css4.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        language : 'bg',

        theme_advanced_resizing : false
    });
</script>
<form action="" method="POST" enctype="multipart/form-data" style="margin-left:15px;">
    <table><tr><td>


                <div style="color:#F8931F;margin:15px 0;font-size: 13px;">Лични данни</div>
                <?php
                foreach ($__languages as $v) {
                    ?>
                    <div style="margin-top:10px;font-size: 13px;"><?php if($v->getName() == 'английски') { echo 'Английски'; } else { echo $v->getName(); }?></div>
                    <textarea name="text_<?php echo $v->getPrefix(); ?>" style="font-family: Verdana;font-size: 13px;width: 1200px;resize:none;height:400px;" rows="15"><?php echo $data[0]['text_'.$v->getPrefix()]; ?></textarea>
                    <input type="hidden" value="<?php echo $data[0]['id']; ?>" name="id" />
                    <?php
                }
                ?>


                <br>
                <button name="submit" type="submit" class="button-save" style="margin-top: 15px;">save</button>
                <button name="cancel" type="submit" class="button-cancel" onclick="cancel();return false;">cancel</button>
    </table>


</form>
<div style="clear:both;height: 60px;"></div>
