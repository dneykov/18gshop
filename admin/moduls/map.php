<?php require '../__top.php';

$pat = array(array('url' => '#', 'ime' => 'магазини'));


if (isset ($_GET['delete'])) {
    $__user->permission_check('контакти', 'rw');

    $id = $_GET['delete'];

    $stm = $pdo->prepare('DELETE FROM `zz_regioni_magazini` WHERE `id` = ? LIMIT 1');
    $stm->bindValue(1, $id, PDO::PARAM_INT);
    $stm->execute();

    if ($stm->rowCount() > 0) {
        po_taka_set_status('изтрит магазин');

        ?>
        <meta http-equiv="refresh" content="0;url="/>
        <?php
        exit;
    }

}

if (isset($_POST['save'])) {
    $grad = 1;
    $stm = $pdo->prepare('INSERT INTO `zz_regioni_magazini`(`id_zz_regioni`,`googleaddress`) VALUES (?,?)');
    $stm->bindValue(1, $grad, PDO::PARAM_INT);
    $stm->bindValue(2, $_POST['googleaddres'], PDO::PARAM_STR);
    $stm->execute();

    $last_id = $pdo->lastInsertId();

    po_taka_update_multilanguage('nov_address_', 'address_', 'zz_regioni_magazini', $last_id);
    po_taka_update_multilanguage('nov_info_', 'info_', 'zz_regioni_magazini', $last_id);
    po_taka_update_multilanguage('nov_magazin_', 'ime_', 'zz_regioni_magazini', $last_id);

    if (isset ($_POST['images'])) {
        if (sizeof($_POST['images']) > 0) {
            foreach ($_POST['images'] as $image) {
                $stm = $pdo->prepare('INSERT INTO `shop_images` (`image`,`shop_id`) VALUES (?,?)');
                $stm->bindValue(1, $image, PDO::PARAM_STR);
                $stm->bindValue(2, $_POST['updateid'], PDO::PARAM_INT);
                $stm->execute();
            }
        }
    }
}


require '../template/__top.php';

?>
<script type="text/javascript">
    function reloadGoogleMap(inp) {
        $(inp).closest("form").children().find(".googlemap").load("googleaddress.php", {'googleaddress': $(inp).val()});
    }
</script>
<?php

if (isset ($_GET['grad'])) $grad = (int)$_GET['grad'];
else $grad = 1;


if (isset ($_POST['updateid'])) {
    $__user->permission_check('контакти', 'rw');

    $stm = $pdo->prepare('UPDATE `zz_regioni_magazini` SET `id_zz_regioni`=?,`googleaddress`=? where id=?');
    $stm->bindValue(1, $grad, PDO::PARAM_INT);
    //$stm -> bindValue(2, $_POST['update_magazin'], PDO::PARAM_STR);
    //$stm -> bindValue(3, $_POST['address'], PDO::PARAM_STR);
    $stm->bindValue(2, $_POST['googleaddres'], PDO::PARAM_STR);
    //$stm -> bindValue(5, $_POST['info'], PDO::PARAM_STR);
    $stm->bindValue(3, $_POST['updateid'], PDO::PARAM_INT);
    $stm->execute();

    po_taka_update_multilanguage('update_magazin_', 'ime_', 'zz_regioni_magazini', $_POST['updateid']);
    po_taka_update_multilanguage('address_', 'address_', 'zz_regioni_magazini', $_POST['updateid']);
    po_taka_update_multilanguage('info_', 'info_', 'zz_regioni_magazini', $_POST['updateid']);

    $stm = $pdo->prepare('DELETE FROM `shop_images` where `shop_id`=?');
    $stm->bindValue(1, $_POST['updateid'], PDO::PARAM_INT);
    $stm->execute();


    if (isset ($_POST['images'])) {
        if (sizeof($_POST['images']) > 0) {
            foreach ($_POST['images'] as $image) {
                $stm = $pdo->prepare('INSERT INTO `shop_images` (`image`,`shop_id`) VALUES (?,?)');
                $stm->bindValue(1, $image, PDO::PARAM_STR);
                $stm->bindValue(2, $_POST['updateid'], PDO::PARAM_INT);
                $stm->execute();
            }
        }
    }
}


?>
<style>
    td {
        font-size: 13px;
    }

    input {
        font-size: 14px;
    }

    input, textarea {
        margin-bottom: 10px;
    }

    .mceLayout {
        margin-bottom: 10px !important;
    }

    .mceContentBody p {
        font-size: 13px;
        font-family: 'Verdana', sans-serif;
    }
</style>
<div id="errors"></div>
<a href="#" onclick="$('#new_address').slideToggle();"
   style="color: #fff;background-color: #666666;width: auto;margin-left: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: none;text-align: center;float: left;font-size: 13px;line-height: 25px;margin-top: 13px;">+
    контакт</a>
<div id="new_address" style="display: none;font-size: 13px;margin-left: 10px;">
    <form action="" method="post">
        <table>
            <?php
            foreach ($__languages as $lng) {
                ?>
                <tr>
                    <td>Име <?php echo $lng->getName(); ?></td>
                </tr>
                <tr>
                    <td><input type="text" style="width:400px;" name="nov_magazin_<?php echo $lng->getPrefix(); ?>">
                    </td>
                </tr>
                <?php
            }
            ?>
            <?php
            foreach ($__languages as $lng) {
                ?>
                <tr>
                    <td>Адрес <?php echo $lng->getName(); ?></td>
                </tr>
                <tr>
                    <td><input type="text" style="width:400px;" name="nov_address_<?php echo $lng->getPrefix(); ?>">
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td>Google maps Локация</td>
            </tr>
            <tr>

                <td><input name="googleaddres" id="g_adres" style="width:500px;"/></td>
            </tr>
            <?php
            foreach ($__languages as $lng) {
                ?>
                <tr>
                    <td>Инфо <?php echo $lng->getName(); ?></td>
                </tr>
                <tr>
                    <td rowspan="3"><textarea type="text" style="width:400px;height:215px;"
                                              name="nov_info_<?php echo $lng->getPrefix(); ?>"></textarea></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>
                <?php
            }
            ?>
        </table>

        <input type="submit" id="save" value="save" name="save">
        <input type="reset" value="cancel" id="reset">

    </form>
</div>
<?php

$stm = $pdo->prepare('SELECT * FROM `zz_regioni_magazini` ORDER BY `ime_' . lang_prefix . '` ASC');
$stm->execute();

if ($stm->rowCount() > 0) {
    foreach ($stm->fetchAll() as $g) {


        //<hr/>


        ?>
        <style>
            table input {
                margin-bottom: 10px;
            }
        </style>
        <form action="" method="post"
              style="border: 1px solid #fff; padding: 34px 9px; padding-top:10px;margin: 0px 0 0 0px; width: 967px;">
            <table>
                <tr>

                    <td>
                        <table style="margin-left: -5px;">
                            <tr>
                                <td colspan="2">
                                    <div class="input-tab-holder" style="margin-top: 10px; width: 455px;">
                                        <div class="input-tab-title">Име</div>
                                        <?php
                                        $inputs = "";
                                        foreach (array_reverse($__languages) as $v) {
                                            echo '<div class="input-tab-trigger-' . $v->getName() . '-map-ime">' . $v->getName() . '</div>';
                                            $inputs = $inputs . '<input class="input-tab-' . $v->getName() . '-map-ime" name="update_magazin_' . $v->getPrefix() . '" value="' . $g['ime_' . $v->getPrefix()] . '" style="width: 455px;" type="text">';
                                        }
                                        ?>
                                    </div>
                                    <?php echo $inputs; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="input-tab-holder" style="margin-top: 10px; width: 455px;">
                                        <div class="input-tab-title">Адрес</div>
                                        <?php
                                        $inputs = "";
                                        foreach (array_reverse($__languages) as $v) {
                                            echo '<div class="input-tab-trigger-' . $v->getName() . '-map-adres">' . $v->getName() . '</div>';
                                            $inputs = $inputs . '<input class="input-tab-' . $v->getName() . '-map-adres" name="address_' . $v->getPrefix() . '" value="' . $g['address_' . $v->getPrefix()] . '" style="width: 455px;" type="text">';
                                        }
                                        ?>
                                    </div>
                                    <?php echo $inputs; ?>
                                </td>
                            </tr>

                            <tr>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="input-tab-holder" style="margin-top: 10px; width: 455px;">
                                        <div class="input-tab-title">Инфо</div>
                                        <?php
                                        $inputs = "";
                                        foreach (array_reverse($__languages) as $v) {
                                            echo '<div class="input-tab-trigger-' . $v->getName() . '-map-info">' . $v->getName() . '</div>';
                                            $inputs = $inputs . '<textarea type="text" style="width:455px;height:215px;font-size:13px !important; font-family: Verdana, sans-serif;padding-left:10px;border-color: #666;-webkit-appearance: none;border: 1px solid #666;" class="input-tab-' . $v->getName() . '-map-info" name="info_' . $v->getPrefix() . '" >' . $g['info_' . $v->getPrefix()] . '</textarea>';
                                        }
                                        ?>
                                    </div>
                                    <?php echo $inputs; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" style="">
                        <div style="margin:13px 10px;width:640px;" class="googlemap">
                            <?php
                            $google = $g;
                            include('googleaddress.php');
                            ?>
                        </div>
                        <div class="google-address" style="margin-left: 10px; margin-top: 29px;">
                            <span style="margin-bottom: -15px; display: block;">Google maps Локация </span><br/>
                            <input name="googleaddres" value="<?php echo $g['googleaddress']; ?>"
                                   style="border: 1px solid #666;width:500px; height: 26px; padding-left: 5px;"
                                   onchange="javascript:reloadGoogleMap($(this));"/>
                        </div>


                    </td>
                </tr>
            </table>
            <input type="hidden" name="updateid" value="<?php echo $g['id']; ?>"/>

            <div id="shopImages" style="margin-top: 50px; width: 450px;">
                <?php
                $stm = $pdo->prepare('SELECT * FROM `shop_images` WHERE `shop_id`=?');
                $stm->bindValue(1, $g['id'], PDO::PARAM_INT);
                $stm->execute();

                if ($stm->rowCount() > 0) {
                    foreach ($stm->fetchAll() as $img) {
                        echo '<div style="width:200px;display:inline-block;margin:20px;"><div style="float: right;color:red;cursor: pointer;display:inline-block;" onclick="javascript:$(this).parent().remove();">x</div><img style="width:200; height:125px;" src="' . url . $img['image'] . '"/><input type="hidden" name="images[]" value="' . $img['image'] . '"/></div>';
                    }
                }

                ?>
            </div>

            <button type="submit" class="button-save" style="margin-top: 40px;"><?php echo lang_save ?></button>
            <button id="cancelForm" onclick="return false;" class="button-cancel">cancel</button>
            <button style="display: none;" class="button-cancel" type="button"
                    onclick="if(confirm('Изтриване на контакт')) location.href='?delete=<?php echo $g['id']; ?>'; else return false;">
                Delete
            </button>
        </form>
        <div style="clear: both;"></div>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#newimageform2').ajaxForm({
                    success: function (data) {
                        $('#shopImages').append(data);
                    }
                });

                $('#newShopImage').change(function () {
                    $("#newimageform2").submit();
                });

            });
        </script>

        <form action="imagepreview.php" method="post" id="newimageform2" enctype="multipart/form-data"
              style="margin-left: 10px;position: absolute;top: 495px;font-size: 13px;color: #333;">
            нова снимка на магазина
            <input type="file" name="newShopImage" id="newShopImage"/>
        </form>
        <?php


    }
}

?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#reset').click(function () {
            $('#new_address').slideUp();
            $('#errors span').hide();
        });
        $('#save').click(function (e) {

            tinyMCE.triggerSave();
            var magazin = $('#magazin').val();
            var adres = $('#adres').val();
            var g_adres = $('#g_adres').val();
            var info = $('#info').val();
            var err = "Моля въведете ";
            var flag = false;
            if (magazin == "") {
                err += "име,";
                flag = true;
            }
            if (adres == "") {
                err += "адрес,";
                flag = true;
            }
            if (g_adres == "") {
                err += "google адрес,";
                flag = true;
            }
            if (info == "") {
                err += "информация";
                flag = true;
            }
            if (flag != false) {
                e.preventDefault();
                $('#errors').html("<span style='display: block; margin-left: 10px;margin-top: 5px; font-size: 12px; color: red;'>" + err.replace(/^,|,$/g, '') + "</span>");
            } else {
                return true;
            }
        });


    });
</script>

<script>
    $(document).ready(function () {
        $(".input-tab-trigger-ro-map-ime").click(function () {
            $(".input-tab-bg-map-ime").hide();
            $(".input-tab-en-map-ime").hide();
            $(".input-tab-ro-map-ime").show();
            $(".input-tab-trigger-ro-map-ime").css("background-color", "#666666");
            $(".input-tab-trigger-ro-map-ime").css("color", "#fff");
            $(".input-tab-trigger-bg-map-ime").css("background-color", "#fff");
            $(".input-tab-trigger-bg-map-ime").css("color", "#333");
            $(".input-tab-trigger-en-map-ime").css("background-color", "#fff");
            $(".input-tab-trigger-en-map-ime").css("color", "#333");
        });

        $(".input-tab-trigger-en-map-ime").click(function () {
            $(".input-tab-bg-map-ime").hide();
            $(".input-tab-ro-map-ime").hide();
            $(".input-tab-en-map-ime").show();
            $(".input-tab-trigger-en-map-ime").css("background-color", "#666666");
            $(".input-tab-trigger-en-map-ime").css("color", "#fff");
            $(".input-tab-trigger-bg-map-ime").css("background-color", "#fff");
            $(".input-tab-trigger-bg-map-ime").css("color", "#333");
            $(".input-tab-trigger-ro-map-ime").css("background-color", "#fff");
            $(".input-tab-trigger-ro-map-ime").css("color", "#333");
        });

        $(".input-tab-trigger-bg-map-ime").click(function () {
            $(".input-tab-bg-map-ime").show();
            $(".input-tab-en-map-ime").hide();
            $(".input-tab-ro-map-ime").hide();
            $(".input-tab-trigger-en-map-ime").css("background-color", "#fff");
            $(".input-tab-trigger-en-map-ime").css("color", "#333");
            $(".input-tab-trigger-ro-map-ime").css("background-color", "#fff");
            $(".input-tab-trigger-ro-map-ime").css("color", "#333");
            $(".input-tab-trigger-bg-map-ime").css("background-color", "#666666");
            $(".input-tab-trigger-bg-map-ime").css("color", "#fff");
        });

        $(".input-tab-trigger-ro-map-adres").click(function () {
            $(".input-tab-bg-map-adres").hide();
            $(".input-tab-en-map-adres").hide();
            $(".input-tab-ro-map-adres").show();
            $(".input-tab-trigger-ro-map-adres").css("background-color", "#666666");
            $(".input-tab-trigger-ro-map-adres").css("color", "#fff");
            $(".input-tab-trigger-bg-map-adres").css("background-color", "#fff");
            $(".input-tab-trigger-bg-map-adres").css("color", "#333");
            $(".input-tab-trigger-en-map-adres").css("background-color", "#fff");
            $(".input-tab-trigger-en-map-adres").css("color", "#333");
        });

        $(".input-tab-trigger-en-map-adres").click(function () {
            $(".input-tab-bg-map-adres").hide();
            $(".input-tab-ro-map-adres").hide();
            $(".input-tab-en-map-adres").show();
            $(".input-tab-trigger-en-map-adres").css("background-color", "#666666");
            $(".input-tab-trigger-en-map-adres").css("color", "#fff");
            $(".input-tab-trigger-bg-map-adres").css("background-color", "#fff");
            $(".input-tab-trigger-bg-map-adres").css("color", "#333");
            $(".input-tab-trigger-ro-map-adres").css("background-color", "#fff");
            $(".input-tab-trigger-ro-map-adres").css("color", "#333");
        });

        $(".input-tab-trigger-bg-map-adres").click(function () {
            $(".input-tab-bg-map-adres").show();
            $(".input-tab-en-map-adres").hide();
            $(".input-tab-ro-map-adres").hide();
            $(".input-tab-trigger-en-map-adres").css("background-color", "#fff");
            $(".input-tab-trigger-en-map-adres").css("color", "#333");
            $(".input-tab-trigger-ro-map-adres").css("background-color", "#fff");
            $(".input-tab-trigger-ro-map-adres").css("color", "#333");
            $(".input-tab-trigger-bg-map-adres").css("background-color", "#666666");
            $(".input-tab-trigger-bg-map-adres").css("color", "#fff");
        });

        $(".input-tab-trigger-ro-map-info").click(function () {
            $(".input-tab-bg-map-info").hide();
            $(".input-tab-en-map-info").hide();
            $(".input-tab-ro-map-info").show();
            $(".input-tab-trigger-ro-map-info").css("background-color", "#666666");
            $(".input-tab-trigger-ro-map-info").css("color", "#fff");
            $(".input-tab-trigger-bg-map-info").css("background-color", "#fff");
            $(".input-tab-trigger-bg-map-info").css("color", "#333");
            $(".input-tab-trigger-en-map-info").css("background-color", "#fff");
            $(".input-tab-trigger-en-map-info").css("color", "#333");
        });

        $(".input-tab-trigger-en-map-info").click(function () {
            $(".input-tab-bg-map-info").hide();
            $(".input-tab-ro-map-info").hide();
            $(".input-tab-en-map-info").show();
            $(".input-tab-trigger-en-map-info").css("background-color", "#666666");
            $(".input-tab-trigger-en-map-info").css("color", "#fff");
            $(".input-tab-trigger-bg-map-info").css("background-color", "#fff");
            $(".input-tab-trigger-bg-map-info").css("color", "#333");
            $(".input-tab-trigger-ro-map-info").css("background-color", "#fff");
            $(".input-tab-trigger-ro-map-info").css("color", "#333");
        });

        $(".input-tab-trigger-bg-map-info").click(function () {
            $(".input-tab-bg-map-info").show();
            $(".input-tab-en-map-info").hide();
            $(".input-tab-ro-map-info").hide();
            $(".input-tab-trigger-en-map-info").css("background-color", "#fff");
            $(".input-tab-trigger-en-map-info").css("color", "#333");
            $(".input-tab-trigger-ro-map-info").css("background-color", "#fff");
            $(".input-tab-trigger-ro-map-info").css("color", "#333");
            $(".input-tab-trigger-bg-map-info").css("background-color", "#666666");
            $(".input-tab-trigger-bg-map-info").css("color", "#fff");
        });
    });
</script>
<div style="clear:both;height: 60px;"></div>