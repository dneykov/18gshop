<?php
require '../__top.php';
require '../template/__top.php';

if(isset ($_POST['submit'])){
    /*** CENTER ***/
    $stm_select_right = $pdo->prepare('SELECT * FROM `header_links` WHERE type = ?');
    $stm_select_right -> bindValue(1, $_POST['type_center'], PDO::PARAM_STR);
    $stm_select_right -> execute();

    if($stm_select_right->rowCount() == 0) {
        $stm_right = $pdo->prepare('INSERT INTO `header_links` (type) VALUES( ? )');
        $stm_right -> bindValue(1, $_POST['type_center'], PDO::PARAM_STR);
        $stm_right -> execute();
        $lastId = $pdo->lastInsertId();
        po_taka_update_multilanguage('text_center_', 'text_', 'header_links', (int) $lastId);
        po_taka_update_multilanguage('url_center_', 'url_', 'header_links', (int) $lastId);
    } else {
        po_taka_update_multilanguage('text_center_', 'text_', 'header_links', $_POST['id_center']);
        po_taka_update_multilanguage('url_center_', 'url_', 'header_links', $_POST['id_center']);
    }
    /*** END CENTER ***/
}

$stm = $pdo->prepare('SELECT * FROM `header_links`');
$stm -> execute();
$data= $stm->fetchall();

?>
<style>
    #navigation {
        width: 100%;
        height: 20px;
        background: #808080;
        margin-top: -7px;
    }
    #navigation a {
        margin: 0 15px;
        display:block;
        float: left;
        line-height: 17px;
        font-size: 13px;
        color: #FFF;
    }
</style>
<?php
require 'adv_menu.php';
?>
<form action="" method="POST" enctype="multipart/form-data" style="margin-left:10px;">
    <table>
        <?php
        foreach ($__languages as $v) {
            ?>
            <tr>
                <td>
                    <div style="margin-top:10px;font-size: 13px;">Текст <?php echo $v->getName(); ?></div>
                    <input type="text" name="text_center_<?php echo $v->getPrefix(); ?>" style="margin-right:5px;font-family: Verdana;font-size: 13px;width: 1260px;resize:none;height:26px;" value="<?php if(!empty($data)) echo $data[0]['text_'.$v->getPrefix()] ?>">
                    <br>
                    <div style="margin-top:10px;font-size: 13px;">URL <?php echo $v->getName(); ?></div>
                    <input type="text" name="url_center_<?php echo $v->getPrefix(); ?>" style="margin-right:5px;font-family: Verdana;font-size: 13px;width: 1260px;resize:none;height:26px;" value="<?php if(!empty($data)) echo $data[0]['url_'.$v->getPrefix()] ?>">
                    <br><br>
                    <input type="hidden" value="center" name="type_center" />
                    <input type="hidden" value="<?php if(!empty($data)) echo $data[0]['id'] ?>" name="id_center" />
                </td>
            </tr>
            <?php
        }
        ?>

    </table>
    <br/>
    <button name="submit" type="submit" class="button-save">save</button>
    <button name="cancel" type="submit" class="button-cancel" onclick="cancel();return false;">cancel</button>

</form>