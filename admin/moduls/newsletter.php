<?php
require '../__top.php';
require dir_root_admin.'/template/__top.php';

if(isset($_POST['addMember'])){
    $id = $_GET['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];


    $err = "";
    $ernum = 0;
    if($email == ""){
        if($ernum != 0) $err .= ", ";
        $err .= "имейл адрес";
        $ernum++;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if($ernum != 0) $err .= ", ";
        $err .= "валиден имейл адрес";
        $ernum++;
    }


    if($err == ""){
        $stm = $pdo->prepare("INSERT INTO `newsletter_members_admin`(`id_group`,`name`, `email`) VALUES (:id,:name,:email)");

        $stm -> bindValue(":id", (int)$id, PDO::PARAM_INT);
        $stm -> bindValue(":name", $name, PDO::PARAM_STR);
        $stm -> bindValue(":email", $email, PDO::PARAM_STR);

        $stm -> execute();

        $_POST = [];
    }


}

if(isset($_POST['SaveMember'])){
    $id = $_POST['memberid'];
    $name =$_POST['name'];
    $email = $_POST['email'];

    $err = "";
    $ernum = 0;
    if($email == ""){
        if($ernum != 0) $err .= ", ";
        $err .= "имейл адрес";
        $ernum++;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if($ernum != 0) $err .= ", ";
        $err .= "валиден имейл адрес";
        $ernum++;
    }



    $stm = $pdo->prepare("UPDATE `newsletter_members_admin` SET `name`=:name,`email`=:email WHERE `id`=:id");

    $stm -> bindValue(":id", (int)$id, PDO::PARAM_INT);
    $stm -> bindValue(":name", $name, PDO::PARAM_STR);
    $stm -> bindValue(":email", $email, PDO::PARAM_STR);

    $stm -> execute();

    $_POST = [];
}
if(isset($_POST['DeleteMember'])){
    $id = $_POST['memberid'];

    $stm = $pdo->prepare("DELETE FROM `newsletter_members_admin` WHERE `id`=:id");
    $stm -> bindValue(":id", (int)$id, PDO::PARAM_INT);
    $stm -> execute();

    $_POST = [];

}


?>
<script type="text/javascript">
    $(document).ready(function(){


        $('#AddCatBtn').click(function() {
            var nameBG = $('#newCategoryBG').val();
            $.ajax({
                type: "POST",
                url: "newsletter/ajax_new_category.php",
                data: "namebg="+nameBG,
                cache: false,
                success: function(html){
                    window.location.href = base_url + 'admin/moduls/newsletter.php?id='+html;
                }
            });
        });
        $('#EditCatBtn').click(function() {
            var nameBG = $('#editCategoryBG').val();
            var id = $('#CategoryId').val();
            $.ajax({
                type: "POST",
                url: "newsletter/ajax_edit_category.php",
                data: "namebg="+nameBG+"&id="+id,
                cache: false,
                success: function(html){
                    location.reload();
                }
            });
        });
        $('#CancelAddCatBtn').click(function() {
            $('#newCategoryBG').val("");
            ShowAddCategory();
        });
        $('#CancelEditCatBtn').click(function() {
            $('#editCategoryBG').val("");
            editCategory();
        });


        $('#CancelAddMember').click(function() {
            ShowAddArticle();
        });

        $('.DeleteMember').click(function() {
            var id = $(this).attr('id');
            var checkstr =  confirm("Изтриване на "+$('#name_'+id).val()+"\n\nСигурни ли сте?");
            if(checkstr == true) {
                return true
            } else {
                return false;
            }
        });

    });

    function ShowAddCategory(){
        if ($("#AddCategory").is(":hidden")) {
            $("#AddCategory").fadeIn(300);
        } else {
            $("#AddCategory").hide(300);
        }
    }
    function ShowAddArticle(){
        $('.ArticleDiv').slideUp("fast");
        if ($("#AddArticleDiv").is(":hidden")) {
            $("#AddArticleDiv").fadeIn(300);
        } else {
            $("#AddArticleDiv").hide(300);
        }
    }
    function editCategory(){
        if ($("#editCategory").is(":hidden")) {
            $("#editCategory").fadeIn(300);
        } else {
            $("#editCategory").hide(300);
        }
    }

    function deleteCategory(id){
        var id = $('#CategoryId').val();
        $(".modal-body").html('<div style="width: 100%; height: 40px;">' + $('#cat'+id+' a').html() + '</div> <button class="button-cancel" onclick="hideModal();" style="position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deleteCategory_Ajax(\'' + id + '\');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
        $(".modal").show();
    }
    function deleteCategory_Ajax(id){
            $.ajax({
                type: "POST",
                url: "newsletter/ajax_delete_category.php",
                data: "id="+id,
                cache: false,
                success: function(html){
                    history.pushState({}, 'title', base_url + 'admin/moduls/newsletter.php?id='+html);
                    location.reload();
                }
            });
    }
    function toggleCont(x) {
        if($('.ArticleDiv').is(':visible')) {
            $('.ArticleDiv').slideUp("fast");
            $('.a-state').html('+');
        }
        if($('#a-'+x).is(":hidden")) {
            $('#a-'+x).slideDown("fast");
            $('#a-state-'+x).html('-');
        } else {
            $('#a-'+x).hide();
            $('#a-state-'+x).html('+');
        }
    };
</script>
<style>
    #leftCol {
        margin-left: 5px;
        float: left;
        width: 300px;
        font-size: 13px;
    }
    #leftCol a{
        margin-left: 5px;
    }
    #leftCol a:hover{
        text-decoration: none;
        color: #f8931d;
    }
    .mail-label {
        color: #f8931d;
        font-size: 12px;
    }
    #leftCol button {
        margin-top: 10px;
        margin-bottom: 20px;
    }
    #rightCol {
        float: left;
    }
    #help_cats {
        height: 20px;
        line-height: 10px;
        color: #fff;
        width: 100%;
    }
    #AddCategory {
        padding-left: 5px;
        padding-top: 5px;
        display: none;
    }
    #AddTag {
        padding-left: 5px;
        padding-top: 5px;
        display: none;
    }
    .addbutton {
        display: block;
        margin: 5px 0 15px 0;
        font-size: 13px;
    }
    .addbutton:hover {
        text-decoration: none;
        color: #f8931d;
    }
    .addbutton:nth-child(1){
        margin-top: -20px;
    }
    #editCategory {
        margin-left: 5px;
        display: none;
    }
    #AddArticleDiv {
        display: none;
        margin-bottom: 50px;
    }
    #AddArticleDiv label, .ArticleDiv label {
        font-size: 13px;
        display: block;
        margin-bottom: 2px;
    }
    .ArticleDiv {
        border: 1px solid #ccc;
        border-top:none;
        margin-bottom: 20px;
        width: 500px;
        display: none;
        font-size: 14px;
        padding: 10px;
    }
    .a-state {
        position: absolute;
        top:2px;
        right:10px;
    }
    .a-link {
        margin-top: 10px;
        cursor: pointer;
        height: 15px;
        font-size: 14px;
        padding-bottom: 5px;
        border-bottom: 1px solid #ccc;
        padding-left: 10px;
        width: 512px;
        position: relative;
    }
    .editTag {
        margin-left: 5px;
        display: none;
    }
    .tags {
        font-size: 13px !important;
        cursor: pointer;
    }
    .tags:hover {
        color: #f8931d;
    }
    .erroradd {
        font-size: 11px;
        color: red;
    }
    #navigation {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #navigation a {
        display: block;
        float: left;
        margin: 0 15px;
        font-size: 13px;
        color: #333;
        line-height: 23px;
    }
    input[type=text] {
        font-size: 13px;
    }
    input[type=submit]{
        margin-top: 25px;
    }
    .users td{
        font-size: 9pt;
        white-space: nowrap;
        overflow: hidden;
        o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        height: 20px;
    }
    .users {
        max-width: 1000px;
        border: none;
        margin-bottom: 50px;
    }
    .users tr {
        height: 20px;
    }
    .users a {
        color: #333;
    }
    .users tr td a{
        width: 100%;
        display: block;
    }
    .users tr:nth-child(even){
        background-color: #fff;
        color: #333;
    }
    .users tr:nth-child(odd){
        background-color: #f2f2f2;
        color: #333;
    }
    .users tr:hover td > a{
        color: #FFF !important;
    }
    #tablehead:hover {
        background-color: #999 !important;
    }
    .productLink:hover{
        text-decoration:none;
    }
    .users{
        border-collapse: collapse;
    }
</style>
<div id="navigation">
    <a href="<?php echo url."admin/moduls/create-newsletter.php"; ?>">Нов бюлетин</a>
    <a href="<?php echo url."admin/moduls/newsletter-history.php"; ?>">История</a>
    <a href="<?php echo url."admin/moduls/newsletter.php"; ?>" style="color: #F8931F">Мейл лист</a>
</div>
<div id="errors"></div>
<div id="leftCol">
    <a class="addbutton" href="#" onclick="ShowAddCategory(); return false;" style="color: #fff;background-color: #666666;margin-top: 7px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;width:100px;font-size: 13px;line-height: 25px;">+ група</a>
    <div id="AddCategory">
        <input id="newCategoryBG" type="text"><br />
        <button id="AddCatBtn" class="button-save">save</button>
        <button id="CancelAddCatBtn" class="button-cancel">cancel</button>
    </div>
    <?php
    $stm = $pdo->prepare("SELECT * FROM `newsletter_groups` ORDER BY `name_bg` ASC");
    $stm -> execute();
    foreach ($stm->fetchAll() as $c) {
        echo "<div class='categories' style='margin-top: 10px;' id='cat".$c['id']."'>";
        echo "<a href='".url."admin/moduls/newsletter.php?id=".$c['id']."' ";
        if(isset($_GET['id']) && $_GET['id'] == $c['id']){
            echo "style='color: #f8931d'";
        }
        echo ">".$c['name_bg']."</a>";
        if(isset($_GET['id']) && $_GET['id'] == $c['id']){
            echo "<a href='#' onclick='editCategory()'><span style='color: #80B543;font-size: 8pt;'>E</span></a>";
            echo "<a href='#' onclick='deleteCategory()'><span style='color: red;font-size: 8pt;'>X</span></a>";
        }
        echo "</div>";
        if(isset($_GET['id']) && $_GET['id'] == $c['id']){
            echo "<div id='editCategory'>";
            echo "<input id='editCategoryBG' type='text' value='".$c['name_bg']."'><br />";
            echo "<button id='EditCatBtn' class='button-save'>save</button> ";
            echo "<button id='CancelEditCatBtn' class='button-cancel'>cancel</button>";
            echo "</div>";
        }
    }
    ?>
</div>
<div id="rightCol">
    <script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode : "textareas",
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,cut,copy,paste,pastetext,pasteword,|,undo,redo,link,unlink,forecolor,|,bullist,numlist,|,outdent,indent",
            theme_advanced_buttons2 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo url_template_folder; ?>images/style_map.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Style formats
            style_formats : [
                {title : 'Bold text', inline : 'b'},
                {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
                {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
                {title : 'Example 1', inline : 'span', classes : 'example1'},
                {title : 'Example 2', inline : 'span', classes : 'example2'},
                {title : 'Table styles'},
                {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
            ],

            language : 'bg',

            theme_advanced_resizing : false
        });
    </script>
    <?php
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        echo "<input id='CategoryId' type='hidden' value='".$id."'>";
        if(isset($err) && $err != ""){
            echo "<div class='erroradd'>Моля въведете ".$err."</div>";
            ?>
            <style>
                #AddArticleDiv {
                    display: block;
                }
            </style>
            <?php
        }
        $stm = $pdo->prepare("SELECT * FROM `newsletter_groups` WHERE `id` = ? ORDER BY `name_bg` ASC");
        $stm -> bindValue(1, $id, PDO::PARAM_INT);
        $stm -> execute();
        $group_name = $stm->fetch()['name_bg'];
        $stm = $pdo->prepare("SELECT * FROM `newsletter_members_admin` WHERE `id_group` = ? ORDER BY `name` ASC ");
        $stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
        $stm -> execute();
        $subscribers = $stm->fetchAll();
        $totalSubscribers = count($subscribers);
        ?>
        <?php if($totalSubscribers < 360) : ?>
            <a class="addbutton" href="#" onclick="ShowAddArticle(); return false;" style="color: #fff;background-color: #666666;margin-top: 7px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;width:150px;font-size: 13px;line-height: 25px;">+ мейл в <?php echo $group_name; ?></a>
        <?php else : ?>
            <span style="font-size: 13px; color: #b3b3b3; display: inline-block;margin:5px 0 15px 0;">Достигнат е лимита за група. Максималният брой потребители за една група 360.</span>
        <?php endif; ?>

        <div id="AddArticleDiv">
            <form enctype="multipart/form-data" action="" method="post">
                <label for="name">Име</label>
                <input type="text" id="name" name="name" style="margin-bottom: 10px;width: 260px;" value="<?php if(isset($_POST['name'])) echo $_POST['name']; ?>">
                <label for="name">E-mail *</label>
                <input type="text" id="email" name="email" style="width: 260px;" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
                <br />
                <button type="submit" class="button-save" name="addMember" style="margin-top: 10px;">save</button>
                <button type="reset" class="button-cancel" name="CancelAddMember" id="CancelAddMember">cancel</button>
            </form>
        </div>
        <table class="users" style="margin-top: 0px;" cellpadding="3">
            <tr id="tablehead" style="background-color: #666666;color: #fff;">
                <td style="padding-left:5px;width: 250px; padding-right:10px;">Име</td>
                <td style="padding-left:5px;width: 250px; padding-right:10px;">И-мейл</td>
                <td></td>
            </tr>
            <?php
            foreach ($subscribers as $a) {
                ?>
                <tr>
                    <td style="padding-left:5px;max-width: 250px; padding-right:10px;" class="ime_na_newsletter_user_<?php echo $a['id']; ?>"><?php echo $a['name']; ?></td>
                    <td style="padding-left:5px;max-width: 200px; padding-right:10px;"><?php echo $a['email']; ?></td>
                    <td style="padding-left:5px;padding-right:10px"><a href="#" onclick="deleteNewsleterUser(<?php echo $a['id']; ?>);return false;"><?php lang_delete_x(); ?></a></td>
                </tr>
            <?php } ?>
        </table>
        <?php
    } else {
        $stm = $pdo->prepare("SELECT * FROM `newsletter_members_admin` ORDER BY `name` ASC ");
        $stm -> execute();
        ?>
        <table class="users" cellpadding="3" style="margin-top: 7px;">
            <tr id="tablehead" style="background-color: #666666;color: #fff;;">
                <td style="padding-left:5px;width: 250px; padding-right:10px;">Име</td>
                <td style="padding-left:5px;width: 250px; padding-right:10px;">И-мейл</td>
                <td></td>
            </tr>
            <?php
            foreach ($stm->fetchAll() as $a) {
                ?>
                    <tr>
                        <td style="padding-left:5px;max-width: 250px; padding-right:10px;" class="ime_na_newsletter_user_<?php echo $a['id']; ?>"><?php echo $a['name']; ?></td>
                        <td style="padding-left:5px;max-width: 250px; padding-right:10px;"><?php echo $a['email']; ?></td>
                        <td style="padding-left:5px;padding-right:10px"><a href="#" onclick="deleteNewsleterUser(<?php echo $a['id']; ?>);return false;"><?php lang_delete_x(); ?></a></td>
                    </tr>
            <?php } ?>
        </table>
    <?php
    }
    ?>
</div>
<div style="clear:both;height: 60px;"></div>

        <div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>