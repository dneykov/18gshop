<?php
require '../../__top.php';

$cat = "";
if($_POST['cat'] != "blank") $cat = $_POST['cat'];
$vid = "";
if($_POST['vid'] != "blank") $vid = $_POST['vid'];
$model = "";
if($_POST['model'] != "blank") $model = $_POST['model'];
$name = $_POST['name'];
$perc = $_POST['perc'];

$aquery = "SELECT `id`, `cena`, `add_artikuli` FROM `artikuli` WHERE ";

$v = 0;
if($_POST['cat'] != "blank") {
	$aquery .= "`id_kategoriq` = :cat";
	$v++;
}
if($_POST['vid'] != "blank") {
	if($v != 0) $aquery .= " AND ";
	$aquery .= "`id_vid` = :vid";
	$v++;
}
if($_POST['model'] != "blank") {
	if($v != 0) $aquery .= " AND ";
	$aquery .= "`id_model` = :model";
	$v++;
}

$stm = $pdo->prepare($aquery);

if($_POST['cat'] != "blank") {
	$stm -> bindValue(":cat", (int)$_POST['cat'], PDO::PARAM_INT);
}
if($_POST['vid'] != "blank") {
	$stm -> bindValue(":vid", (int)$_POST['vid'], PDO::PARAM_INT);
}
if($_POST['model'] != "blank") {
	$stm -> bindValue(":model", (int)$_POST['model'], PDO::PARAM_INT);
}

$stm -> execute();

$perc = $_POST['perc'];
$artikuli = "";
$n = 1;
foreach($stm -> fetchAll() as $c){
	$artikul = new artikul((int)$c['id']);
	$addartikuli = $c['add_artikuli'];

	if($addartikuli == ""){
		$cena = $artikul->getCena();
		(float)$cena;

		$newcena = (float)$cena - $cena*($perc/100);
		$stm = $pdo->prepare('UPDATE `artikuli` SET `cena_promo` = :cena WHERE `id` = :id');
		$stm -> bindValue(":id", (int)$artikul->getId(), PDO::PARAM_INT);
		$stm -> bindValue(":cena", $newcena, PDO::PARAM_STR);
		$stm -> execute();

		if($n != 1) $artikuli .= ", ";
		$artikuli .= $c['id'];
		$n++;
	}
}

$query = 'INSERT INTO `promo` (`ime`, `perc`, `artikuli`';

if($_POST['cat'] != "blank") {
	$query .= ', `category_id`';
}
if($_POST['vid'] != "blank") {
	$query .= ', `vid_id`';
}
if($_POST['model'] != "blank") {
	$query .= ', `model_id`';
}

$query .= ')';
$query .= ' VALUES (:ime,:perc,:artikuli';

if($_POST['cat'] != "blank") {
	$query .= ",:cat";
}
if($_POST['vid'] != "blank") {
	$query .= ",:vid";
}
if($_POST['model'] != "blank") {
	$query .= ",:model";
}

$query .= ')';
$stm = $pdo->prepare($query);

$stm -> bindValue(":ime", $_POST['name'], PDO::PARAM_STR);
$stm -> bindValue(":perc", $perc, PDO::PARAM_INT);
$stm -> bindValue(":artikuli", $artikuli, PDO::PARAM_STR);

if($_POST['cat'] != "blank") {
	$stm -> bindValue(":cat", (int)$_POST['cat'], PDO::PARAM_INT);
}
if($_POST['vid'] != "blank") {
	$stm -> bindValue(":vid", (int)$_POST['vid'], PDO::PARAM_INT);
}
if($_POST['model'] != "blank") {
	$stm -> bindValue(":model", (int)$_POST['model'], PDO::PARAM_INT);
}

$stm -> execute();

echo "ok";