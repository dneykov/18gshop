<?php
	require '../../__top.php';
	$id = (int)$_POST['id'];
	$stm = $pdo->prepare('SELECT `artikuli` FROM `promo` WHERE `id` = ? LIMIT 1');
	$stm -> bindValue(1, $id, PDO::PARAM_INT);
	$stm -> execute();
	$a = $stm->fetch();
	$ar = $a['artikuli'];
	$arr = explode(",", $ar);
	foreach ($arr as $v) {
		$v = (int)$v;
		$newcena = 0;
		$stm = $pdo->prepare('UPDATE `artikuli` SET `cena_promo` = :cena WHERE `id` = :id');
		$stm -> bindValue(":id", $v, PDO::PARAM_INT);
		$stm -> bindValue(":cena", $newcena, PDO::PARAM_STR);
		$stm -> execute();
	}

	$stm = $pdo->prepare('DELETE FROM `promo` WHERE `id` = :id LIMIT 1');
	$stm -> bindValue(":id", $id, PDO::PARAM_INT);
	$stm -> execute();
?>