<?php
	require '../../__top.php';
	$id = (int)$_POST['id'];
	$perc = (int)$_POST['perc'];
	$stm = $pdo->prepare('SELECT `artikuli` FROM `promo` WHERE `id` = ? LIMIT 1');
	$stm -> bindValue(1, $id, PDO::PARAM_INT);
	$stm -> execute();
	$a = $stm->fetch();
	$ar = $a['artikuli'];
	$arr = explode(",", $ar);
	foreach ($arr as $v) {
		$v = (int)$v;
		$stma = $pdo->prepare('SELECT `cena` FROM `artikuli` WHERE `id` = ? LIMIT 1');
		$stma -> bindValue(1, $v, PDO::PARAM_INT);
		$stma -> execute();
		$c = $stma->fetch();
		$cena = $c['cena'];

		$newcena = (float)$cena - $cena*($perc/100);

		$stm = $pdo->prepare('UPDATE `artikuli` SET `cena_promo` = :cena WHERE `id` = :id');
		$stm -> bindValue(":id", $v, PDO::PARAM_INT);
		$stm -> bindValue(":cena", $newcena, PDO::PARAM_STR);
		$stm -> execute();
	}

	$stm = $pdo->prepare('UPDATE `promo` SET `perc` = :perc WHERE `id` = :id LIMIT 1');
	$stm -> bindValue(":id", $id, PDO::PARAM_INT);
	$stm -> bindValue(":perc", $perc, PDO::PARAM_INT);
	$stm -> execute();

?>