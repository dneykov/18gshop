<?php 
require '../../__top.php';
if(isset($_POST['cat']) && $_POST['cat'] != "blank"){
?>
<script>
	$(document).ready(function(){
		$('#vid').change(function() {
			var cat = $('#category').val();
			var vid = $('#vid').val();
			$.ajax({
	            type: "POST",
	            url: "promo/ajax_vid_model.php",
	            data: "cat="+cat+"&vid="+vid,
	            cache: false,
	            success: function(html){
	            	$('#modelbox').html(" ");
	            	$('#modelbox').append(html);
	            }
	        });
		});
    });
</script>
<select id="vid" style="width: 300px; height: 26px; border: 1px solid #666666; font-family: 'Verdana',sans-serif; color: #333;">
	<option value="blank"></option>
	<?php
		$stmvid = $pdo->prepare('SELECT `id`, `ime_4c9a99e1a510a` FROM `vid` WHERE `id_kategoriq` = ? ORDER BY `id` ASC');
		$stmvid -> bindValue(1, (int)$_POST['cat'], PDO::PARAM_INT);
	    $stmvid -> execute();
	    foreach($stmvid -> fetchAll() as $c){
	    	echo "<option value='".$c['id']."'>";
	    	echo $c['ime_4c9a99e1a510a'];
	    	echo "</option>";
	    }
	?>
</select>
<?php
}else if($_POST['cat'] == "blank"){
?>
<select id="vid" style="width: 200px;">
	<option value="blank"></option>
</select>
<?php
}
?>