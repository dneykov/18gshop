<?php
require '../__top.php';
ini_set('max_execution_time', 0);
unset($_SESSION['crop_thumb_image_location']);
unset($_SESSION['crop_large_image_location']);
unset($_SESSION['crop_html_img']);
unset($_SESSION['crop_html_img_t']);
unset($_SESSION['crop_html_img_b']);
unset($_SESSION['crop_html_img_input_hidden']);

$_SESSION['crop_upload_dir']='../../../images/newsletter';

$_SESSION['crop_width']=240;
$_SESSION['crop_height']=160;

if(isset($_POST['sendNewsletter'])){
    $groups = isset($_POST['groups']) ? $_POST['groups'] : array();
    $subject = $_POST['subject'];
    $content = $_POST['content'];
    $url = $_POST['url'];
    $img = isset($_POST['adv_file_new']) ? $_POST['adv_file_new'] : "";


    $err = "";
    $ernum = 0;
    if(count($groups) == 0){
        if($ernum != 0) $err .= ", ";
        $err .= "получател";
        $ernum++;
    }
    if($subject == ""){
        if($ernum != 0) $err .= ", ";
        $err .= "заглавие (относно)";
        $ernum++;
    }
    if($content == ""){
        if($ernum != 0) $err .= ", ";
        $err .= "съдържание";
        $ernum++;
    }


    if($err == ""){
        $stm = $pdo->prepare("INSERT INTO `newsletter_history`(`subject`, `recipients`, `content`, `img`, `url`) VALUES (:subject,:recipients,:content,:img,:url)");

        $stm -> bindValue(":subject", $subject, PDO::PARAM_INT);
        $stm -> bindValue(":recipients", json_encode($groups), PDO::PARAM_INT);
        $stm -> bindValue(":content", $content, PDO::PARAM_STR);
        $stm -> bindValue(":img", $img, PDO::PARAM_STR);
        $stm -> bindValue(":url", $url, PDO::PARAM_STR);

        $stm -> execute();

        $stm = $pdo -> prepare('UPDATE `newsletter_history` SET `created_at` = ?');
        $stm -> bindValue(1, time(), PDO::PARAM_INT);
        $stm -> execute();

        //Send emails to all recipients
        $all_groups = "";
        foreach($groups as $group) {
            $all_groups .= "'" . $group . "', ";
        }
        $all_groups = substr($all_groups, 0, -2);

        $stm = $pdo->prepare("SELECT * FROM `newsletter_members_admin` WHERE `id_group` IN ($all_groups)");
        $stm -> execute();
        $all_recipients = $stm->fetchAll();

        $mail = new PHPMailer();
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPKeepAlive = true;
        $mail->SMTPSecure = 'tls';
        $mail->Host = NEWSLETTER_MAIL_HOST;
        $mail->Port = 587;
        $mail->Username = NEWSLETTER_MAIL;
        $mail->Password = NEWSLETTER_MAIL_PASSWORD;
        $mail->CharSet="utf-8";
        $mail->From = mail_orders;
        $mail->FromName = "18gshop";
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        $mail->IsHTML(TRUE);                                  // set email format to HTML
        $mail->Subject = $subject;
        $mail->AddAddress("newsletter@18gshop.com");

        if($img != "") {
            if($url != "") {
                $msg = '<div style="height: 30px;font-size: 16px;background-color: #0071B9;color: #fff;line-height: 30px;max-width: 1024px;padding-left: 10px;"><strong>'.$subject.'</strong></div><br/>';
                $msg .= '<a href="'.$url.'"><img src="'. url . str_replace('.jpg','_b.jpg',$img) .'" style="max-width: 1024px;"/></a><br/><br/>';
            } else {
                $msg = '<div style="height: 30px;font-size: 16px;background-color: #0071B9;color: #fff;line-height: 30px;max-width: 1024px;padding-left: 10px;"><strong>'.$subject.'</strong></div><br/>';
                $msg .= '<img src="'. url . str_replace('.jpg','_b.jpg',$img) .'" style="max-width: 1024px;"/><br/><br/>';
            }
        } else {
            if($url != "") {
                $msg = '<div style="height: 30px;font-size: 16px;background-color: #0071B9;color: #fff;line-height: 30px;max-width: 1024px;padding-left: 10px;"><a href="'.$url.'" style="text-decoration: none;color: #fff;"><strong>'.$subject.'</strong></a></div><br/>';
            } else {
                $msg = '<div style="height: 30px;font-size: 16px;background-color: #0071B9;color: #fff;line-height: 30px;max-width: 1024px;padding-left: 10px;"><strong>'.$subject.'</strong></div><br/>';
            }
        }
        $msg .= $content . '<br/>';
        $mail->Body = $msg;

        foreach ($all_recipients as $recipient) {
            $mail->AddBCC($recipient['email']);
        }

        $mail->Send();
        $_POST = [];

        ?>
        <meta http-equiv="Refresh" content="0;URL=<?php echo url_admin ?>/moduls/newsletter-history.php" />
        <?php
        exit;
    }

}

$stm = $pdo->prepare("SELECT * FROM `newsletter_groups` ORDER by `name_bg`");
$stm -> execute();
$groups = $stm->fetchAll();
$stm = $pdo->prepare("SELECT * FROM `newsletter_history` ORDER BY id DESC LIMIT 0,1");
$stm -> execute();
$lastNewsletter = $stm->fetch();
$lastNewsletterTime = $lastNewsletter['created_at'];
require dir_root_admin.'/template/__top.php';
?>
<style>
    #createNewsletter {
        padding: 10px;
        padding-top: 0px;
        font-size: 13px;
        margin-left: 0px;
    }
    #createNewsletter label {
        font-size: 13px;
    }
    #createNewsletter ul {
        margin-top: 2px;
        list-style: none;
    }
    #createNewsletter ul input {
        margin-right: 5px;
    }
    #navigation {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #navigation a {
        display: block;
        float: left;
        margin: 0 15px;
        font-size: 13px;
        color: #333;
        line-height: 23px;
    }
    input[type=text] {
        font-size: 13px;
    }
    input[type=submit]{
        margin-top: 25px;
    }
    .erroradd {
        font-size: 11px;
        color: red;
    }
</style>
<div id="navigation">
    <a href="<?php echo url."admin/moduls/create-newsletter.php"; ?>" style="color: #F8931F">Нов бюлетин</a>
    <a href="<?php echo url."admin/moduls/newsletter-history.php"; ?>">История</a>
    <a href="<?php echo url."admin/moduls/newsletter.php"; ?>">Мейл лист</a>
</div>
<div id="errors"></div>
<div id="RightCol">
    <script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode : "textareas",
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,link,unlink,forecolor,|,bullist,numlist,|,outdent,indent",
            theme_advanced_buttons2 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo url_template_folder; ?>images/style_map.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Style formats
            style_formats : [
                {title : 'Bold text', inline : 'b'},
                {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
                {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
                {title : 'Example 1', inline : 'span', classes : 'example1'},
                {title : 'Example 2', inline : 'span', classes : 'example2'},
                {title : 'Table styles'},
                {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
            ],

            language : 'bg',

            theme_advanced_resizing : false
        });
    </script>
    <div id="createNewsletter">
        <?php
        if(isset($err) && $err != ""){
            echo "<div class='erroradd'>Моля въведете ".$err."</div><br />";
        }
        ?>
        <form enctype="multipart/form-data" action="" method="post">
            Получатели*
            <ul>
                <?php foreach($groups as $key => $group) : ?>
                    <li><input type="radio" <?php if(isset($_POST['groups'][$key]) && $_POST['groups'][$key] == $group['id']) echo "checked";  ?>  value="<?php echo $group['id']; ?>" id="group_<?php echo $group['id']; ?>" name=groups[]" /><label
                            for="group_<?php echo $group['id']; ?>"><?php echo $group['name_bg']; ?></label></li>
                <?php endforeach; ?>
            </ul>
            <br>
            <label for="subject" style="margin-bottom: 3px; display: inline-block;">Заглавие (Относно)*</label>
            <br>
            <input size="61" type="text" id="subject" name="subject" value="<?php if(isset($_POST['subject'])) echo $_POST['subject']; ?>" />
            <br>
            <br>
            <label for="url" style="margin-bottom: 3px; display: inline-block;">Линк</label>
            <br>
            <input size="61" type="text" id="url" name="url" value="<?php if(isset($_POST['url'])) echo $_POST['url']; ?>" />
            <br />
            <br />
            <div id="article_image" >
                <img id="adv_img_new" src="<?php if(isset($_POST['adv_file_new']) && $_POST['adv_file_new'] != "") { echo url.'/'.$_POST['adv_file_new']; } else { echo url.'images/blank_400X300.png'; } ?>" style="max-width:400px;display:block;margin-bottom: 5px;"/>
                <a class="thickbox" style="color: #fff;background-color: #666666;margin-top: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;width:100px;font-size: 13px;line-height: 25px;" href='crop/new_crop_newsletter.php?crop_html_img=adv_img_new&crop_width=770&crop_height=300&crop_html_img_input_hidden=adv_file_new&keepThis=true&TB_iframe=true&height=580&width=1050'>+ снимка</a>
                <input type="hidden" name="adv_file_new" value="<?php if(isset($_POST['adv_file_new'])) echo $_POST['adv_file_new'];?>" id="adv_file_new">
            </div>
            <br />
            <label for="content" style="margin-bottom: 3px; display: inline-block;">Съдържание*</label>
            <br />
            <textarea name="content" id="content" cols="69" rows="25"><?php if(isset($_POST['content'])) echo $_POST['content']; ?></textarea>
            <?php if(time() > $lastNewsletterTime + 3600) : ?>
                <button type="submit" name="sendNewsletter" class="button-save" style="margin-top: 15px;">send</button>
                <button type="reset" name="newsletterReset" class="newsletterReset button-cancel" onclick="$('#adv_img_new').attr('src', '<?php echo url.'images/blank_400X300.png'; ?>');">reset</button>
            <?php else : ?>
                <div style="color: #ff0000;margin-top: 20px;">Може да изпратите един бюлетин в рамките на 1 час. <br/>Моля опитайте пак след <?php echo gmdate("i", ($lastNewsletterTime + 3660) - time()); ?> минути.</div>
            <?php endif; ?>
        </form>
    </div>
</div>
<div style="clear:both;height: 60px;"></div>