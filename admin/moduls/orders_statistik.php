<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$page_current = 'orders';

$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "TotalТurnover"');
$stm -> execute();
$totalTurnover=$stm->fetch();
$totalTurnover=$totalTurnover['value'];


$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "TotalProfit"');
$stm -> execute();
$totalProfit=$stm->fetch();
$totalProfit=$totalProfit['value'];

require('template/__top.php');

function getChartSiteYearDate()
{
    global $pdo;
    $i=0;
    $number=365;
    $today=strtotime(date("Y-m-d", time() - 86400*$number));
    $stm = $pdo->prepare("SELECT COUNT(id) as totalOrders, SUM(cost) as totalPrice, CAST(date_done AS DATE) as date FROM orders WHERE date_done > ? GROUP BY CAST(date_done AS DATE) ORDER BY date_done");
    $stm -> bindValue(1, date("Y-m-d",$today), PDO::PARAM_STR);
    $stm -> execute();
    $orders = $stm->fetchAll();

    foreach ($orders as $order) {
        if($i==0)
        {
            $start=date("M-Y",strtotime($order['date']));
        }
        $i++;
        $end=date("M-Y",strtotime($order['date']));
    }
    $storage="'".$start." - ".$end."'";
    echo $storage;
}

function getSiteChartForWeek()
{
    global $pdo;
    $groupOrders = [];
    $number=30;
    $today=strtotime(date("Y-m-d", time() - 86400*$number));
    $storage="[";
    $n=0;
    $stm = $pdo->prepare("SELECT id, cost, details, CAST(date_done AS DATE) as date FROM orders WHERE date_done > ? ORDER BY date_done");
    $stm -> bindValue(1, date("Y-m-d",$today), PDO::PARAM_STR);
    $stm -> execute();
    $orders = $stm->fetchAll();
    
    foreach ($orders as $key => $order) {
        $orderDetails = unserialize($order['details']);
        $delivery = $orderDetails['header']['delivery'];

        if (is_numeric($delivery)) $orders[$key]['cost'] -= $delivery;
    }

    foreach ($orders as $order) {
        if (!isset($groupOrders[$order['date']])) $groupOrders[$order['date']] = [];
        $groupOrders[$order['date']]['totalPrice'] += $order['cost'];
        $groupOrders[$order['date']]['date'] = $order['date'];
        $groupOrders[$order['date']]['totalOrders'] += 1;
    }

    foreach ($groupOrders as $order) {
        $dt=date("d.m",strtotime($order['date']));
        $storage.='{ date: "' . $dt . '", income: ' . $order['totalPrice']. '},';
    }
    $storage[strlen($storage)]="]";
    return $storage.";";
}

function getSiteChartForYear()
{
    global $pdo;
    $groupOrders = [];
    $number=365;
    $today=strtotime(date("Y-m-d", time() - 86400*$number));
    $storage="[";
    $n=0;
    $ob = array();
    $stm = $pdo->prepare("SELECT id, cost, details, CAST(date_done AS DATE) as date FROM orders WHERE date_done > ? ORDER BY date_done");
    $stm -> bindValue(1, date("Y-m-d",$today), PDO::PARAM_STR);
    $stm -> execute();
    $orders = $stm->fetchAll();

    foreach ($orders as $key => $order) {
        $orderDetails = unserialize($order['details']);
        $delivery = $orderDetails['header']['delivery'];

        if (is_numeric($delivery)) $orders[$key]['cost'] -= $delivery;
    }

    foreach ($orders as $order) {
        if (!isset($groupOrders[$order['date']])) $groupOrders[$order['date']] = [];
        $groupOrders[$order['date']]['totalPrice'] += $order['cost'];
        $groupOrders[$order['date']]['date'] = $order['date'];
        $groupOrders[$order['date']]['totalOrders'] += 1;
    }

    foreach ($groupOrders as $order) {
        $month=date_format(date_create($order['date']), 'm-Y');
        if (isset($ob[$month]))
        {
            $ob[$month][0]+=0;
            $ob[$month][1]+=$order['totalPrice'];
        }
        else
        {
            $ob[$month][0]=0;
            $ob[$month][1]=$order['totalPrice'];
            $ob[$month][2]=$month;
        }
    }
    $c=0;
    foreach ($ob as $o)
    {
        $newArray[$c][0]=$o[2];
        $newArray[$c][1]=$o[0];
        $newArray[$c][2]=$o[1];
        $c++;
    }

    for ($c=0; $c<count($newArray);$c++)
    {
        $storage.='{ date: "' . $newArray[$c][0] . '", income: ' . $newArray[$c][2]. '},';
    }

    $storage[strlen($storage)]="]";
    return $storage.";";
}

?>
<style type="text/css">
	table td{
		font-size: 10pt;
	}
	.subMenu{
		width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
	}
	.subMenu a{
		color:#F7931E;
		font-size: 13px;
		margin:0 15px;
	}
	.subMenu a:hover{
		color:#F7931E !important;
	}
	.productLink:hover{
		text-decoration:none;
	}
	.orders{
		border-collapse: collapse;
	}
</style>
<div class="subMenu">
    <a href="<?php echo url_admin; ?>orders.php" <?php if((isset($_GET['approved']))||(isset($_GET['done']))||(isset($_GET['cancel']))||(isset($_GET['return']))) echo 'style="color: #333;"' ?>>Нови <?php echo order_admin::getCountForWaiting(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?approved" <?php if(!isset($_GET['approved'])) echo 'style="color: #333;"' ?>>Одобрени <?php echo order_admin::getCountForApproved(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?done" <?php if(!isset($_GET['done'])) echo 'style="color: #333;"' ?>>Изпълнени <?php echo order_admin::getCountForDone(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?cancel" <?php if(!isset($_GET['cancel'])) echo 'style="color: #333;"' ?>>Отказани <?php echo order_admin::getCountForCanceled(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?return" <?php if(!isset($_GET['return'])) echo 'style="color: #333;"' ?>>Върнати <?php echo order_admin::getCountForReturned(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?statistik">Отчет</a>
    <a href="<?php echo url_admin; ?>orders.php?delivery" style="color: #333;">Доставка</a>
    <a href="<?php echo url_admin; ?>orders.php?valute" style="color: #333;">Настройки</a>
</div>
<div style="margin-left: 5px">
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/knockout-2.2.1.js"></script>
    <script src="js/globalize.js"></script>
    <script src="js/dx.chartjs.js"></script>
    <div id="admin_body" style='margin-left:10px;'>
        <?php
        //echo '<br /><span>Оборот: '.$totalTurnover.' лв.</span>';
        //echo '<br /><span>Печалба: '.$totalProfit.' лв.</span><br>';
        ?>
        <script type="text/javascript">
            //JAVASCRIPT SITE CHART
            $(function ()  {
                var dataSource =<?php echo getSiteChartForWeek();?>
                var start=dataSource[0].date;
                var end=dataSource[dataSource.length-1].date;
                $("#date").html(start+ " - " + end);
                var myPalette = ['#666666', '#808080', '#AF8A53', '#955F71', '#859666', '#7E688C'];
                DevExpress.viz.core.registerPalette('mySuperPalette', myPalette);
                var chart = $("#siteChart").dxChart({
                    dataSource: dataSource,
                    palette: myPalette,
                    commonSeriesSettings: {
                        argumentField: 'date',
                        type: 'bar'
                    },
                    commonAxisSettings: {
                        label: {
                            font: {color: '#666666', size: 10},
                            overlappingBehavior: { mode: 'ignore'}
                        }
                    },
                    series: [
                        { valueField: 'income', name: 'Оборот' },
                    ],
                    legend: {
                        visible: false
                    },
                    label: {
                        visible: true,
                    },
                    tooltip: {
                        enabled: true,
                        customizeText: function () {
                            return this.seriesName +" "+ this.valueText + " лв.";
                        },
                        font: {size: 16}
                    }
                });
            });
            //year
            $(function ()  {
                var dataSource =<?php echo getSiteChartForYear();?>
                var interData=<?php getChartSiteYearDate();?>;
                $("#Ydate").html(interData);
                var myPalette = ['#666666', '#808080', '#AF8A53', '#955F71', '#859666', '#7E688C'];
                DevExpress.viz.core.registerPalette('mySuperPalette', myPalette);
                var chart = $("#siteChartYear").dxChart({
                    dataSource: dataSource,
                    palette: myPalette,
                    commonSeriesSettings: {
                        argumentField: 'date',
                        type: 'bar'
                    },
                    commonAxisSettings: {
                        label: {
                            font: {color: '#666666', size: 10},
                            overlappingBehavior: { mode: 'ignore'}
                        }
                    },
                    series: [
                        { valueField: 'income', name: 'Оборот' },
                    ],
                    legend: {
                        visible: false
                    },
                    label: {
                        visible: true,
                    },
                    tooltip: {
                        enabled: true,
                        customizeText: function () {
                            return this.seriesName +" "+ this.valueText + " лв.";
                        },
                        font: {size: 16}
                    }
                });
            });
        </script>
        <br>
        <div id="buttons" style="font-size: 13px; margin-top: -18px;"><span class="_30" style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #F7931E;float: left;">месец</span>&nbsp;&nbsp;<span class="_365" style="display: inline-block;color: white;font-size: 13px;height: 25px;line-height: 23px;width: 78px;text-align: center; background-color: #666;float: left;">година</span></div>
        <div id="site" style="">
            <div class="content">
                <span id="date" style="font-size: 13px;"></span>
                <div class="pane">
                    <div class="long-title"><h3></h3></div>
                    <div id="siteChart" style="width: 99%; height: 220px; color: #ffffff;"></div>
                </div>
            </div>
        </div>
        <div id="siteY">
            <div class="content">
                <span id="Ydate" style="font-size: 13px;"></span>
                <div class="pane">
                    <div class="long-title"><h3></h3></div>
                    <div id="siteChartYear" style="width: 99%; height: 220px; color: #ffffff;"></div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <style>
            text
            {
                margin-top: -20px;
                width: 20px !important;
            }
            .dxLegend
            {
                margin-left: -100px;
            }
            #buttons{
                float:left;
                margin-right: 128px;
                margin-left: -15px;
            }
            #buttons span
            {
                margin-left: 10px
            }
            #buttons span:hover
            {
                cursor: pointer;
            }
            #siteY
            {
                position: relative;
                visibility: hidden;
            }
            #site
            {

            }
            #date
            {
                float: right;
                margin-right: 60px;
            }
            #Ydate
            {
                float: right;
                margin-right: 60px;
            }
            .label
            {
                font-size: 10px;
                float: left;
                margin-left:10px;
                width: 100px;
            }
            .allLabels
            {
                margin-top: 7px;
                margin-left: 5px;
            }
            .catLabels
            {
                margin-top: 7px;
                margin-left: 7px;
            }
            .catLabel
            {
                font-size: 10px;
                float: left;
                margin-left:19px;
                width: 100px;
            }
            ._30
            {
                color: #F7931E;
                cursor: pointer;
            }
            ._365
            {
                cursor: pointer;
            }
        </style>

    </div>
    <script>
        //еvents
        $("._30").click(function (){
            $("#siteY").css("visibility", "hidden");
            $("#siteY").hide();
            $("#site").css("visibility", "visible");
            $("#site").show();
            $("._30").css("background-color", "#F7931E");
            $("._365").css("background-color", "#666");
        });
        $("._365").click(function (){
            $("#site").css("visibility", "hidden");
            $("#site").hide();
            $("#siteY").css("visibility", "visible");
            $("#siteY").show();
            $("._30").css("background-color", "#666");
            $("._365").css("background-color", "#F7931E");
        });
    </script>

</div>
