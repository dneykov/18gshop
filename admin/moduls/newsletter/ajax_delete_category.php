<?php
require "../../__top.php";

$id = $_POST['id'];

$stm = $pdo->prepare("DELETE FROM `newsletter_groups` WHERE `id`=?");
$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
$stm -> execute();
$stm = $pdo->prepare("DELETE FROM `newsletter_members_admin` WHERE `id_group`=?");
$stm -> bindValue(1, (int)$id, PDO::PARAM_INT);
$stm -> execute();
$stm = $pdo->prepare("SELECT `id` FROM `newsletter_groups` ORDER BY `name_bg` ASC LIMIT 1");
$stm->execute();
$id = $stm->fetch();
echo $id['id'];