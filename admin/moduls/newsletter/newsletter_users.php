<?php
require "../../__top.php";

if(isset ($_GET['delete'])) {
    $stm = $pdo->prepare("DELETE FROM `newsletter_members_admin` WHERE `id`=?");
    $stm -> bindValue(1, $_GET['id'], PDO::PARAM_INT);
    $stm -> execute();
    exit;
}