<?php
require "../../__top.php";

$nameBG = $_POST['namebg'];
$id = $_POST['id'];

$stm = $pdo->prepare("UPDATE `newsletter_groups` SET `name_bg`=? WHERE `id`=?");
$stm -> bindValue(1, $nameBG, PDO::PARAM_STR);
$stm -> bindValue(2, (int)$id, PDO::PARAM_INT);
$stm -> execute();
?>