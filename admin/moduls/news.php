<?php

require '../__top.php';

if (isset ($_POST['updateid']) && !isset($err)) {
    $__user->permission_check('новини', 'rw');
    $oldNews = new news((int)$_POST['updateid']);

    $stm = $pdo->prepare('UPDATE `news` SET `url`=?,`image` = ?, `Type` = ? where id=?');
    $stm->bindValue(1, $_POST['url'], PDO::PARAM_STR);
    if ((isset($_POST['type'])) && ($_POST['type'] == 'video')) {
        $stm->bindValue(2, str_replace('watch?v=', 'embed/', $_POST["news_image"]), PDO::PARAM_STR);
        var_dump($_POST["news_image"]);
    } else {
        $stm->bindValue(2, $_POST['news_image'], PDO::PARAM_STR);
    }
    $stm->bindValue(3, $_POST['type_' . $_POST['updateid']], PDO::PARAM_STR);
    $stm->bindValue(4, $_POST['updateid'], PDO::PARAM_INT);

    $stm->execute();

    po_taka_update_multilanguage('text_', 'text_', 'news', (int)$_POST['updateid']);
    po_taka_update_multilanguage('title_', 'title_', 'news', (int)$_POST['updateid']);

    header('Location: ' . url . 'admin/moduls/news.php');
    exit;
}

if (!isset($_GET['type'])) {
    $_GET['type'] = 'news';
}

if (isset ($_GET['delete'])) {
    $__user->permission_check('новини', 'rw');
    $id = $_GET['delete'];

    $stmForDelete = $pdo->prepare('SELECT * FROM `news` WHERE `id` = ? LIMIT 1');
    $stmForDelete->bindValue(1, $id, PDO::PARAM_INT);
    $stmForDelete->execute();
    $newsForDeletion = $stmForDelete->fetch();

    $thumbnailFilePath = pathinfo(dir_root . $newsForDeletion['image']);
    $extension = pathinfo(dir_root . $newsForDeletion['image'], PATHINFO_EXTENSION);
    $resizeFilePath = pathinfo(dir_root . str_replace("thumbnail", "resize", $newsForDeletion['image']));


    if ($newsForDeletion['image'] != 'images/blank_400X300.png') {
        delete_file(dir_root . $newsForDeletion['image']);
        delete_file(dir_root . str_replace("thumbnail", "resize", $newsForDeletion['image']));
        delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_tt." . $extension);
        delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_t." . $extension);
        delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_b." . $extension);
        delete_file($resizeFilePath['dirname'] . "/" . $resizeFilePath['filename'] . "_org." . $extension);
    }

    $stm = $pdo->prepare('DELETE FROM `news` WHERE `id` = ? LIMIT 1');
    $stm->bindValue(1, $id, PDO::PARAM_INT);
    $stm->execute();

    $news_type_name = "новина";
    if ($_GET['type'] == 'video') {
        $news_type_name = 'видео';
    }
    if ($_GET['type'] == 'friends') {
        $news_type_name = 'приятел';
    }

    if ($stm->rowCount() > 0) {
        po_taka_set_status('изтрита ' . $news_type_name);

        ?>
        <meta http-equiv="refresh" content="0;url="/>
        <?php
        exit;
    }
}

if (isset ($_POST['new_text']) && !isset($err)) {

    $err = "";
    $ernum = 0;
    $etex = false;
    $etit = false;
    foreach ($__languages as $key => $v) {
        if ($_POST['title_' . $v->getPrefix()] == "" && $etit == false) {
            $err .= "заглавие за Български и Английски";
            $etit = true;
            $ernum++;
        }
        if ($_POST['new_text_' . $v->getPrefix()] == "" && $etex == false) {
            if ($ernum != 0) $err .= ", ";
            $err .= "текст за Български и Английски";
            $etex = true;
        }
    }
    if ($err == "") {
        $query = "INSERT INTO `news`(`url`,`image`,`Type`";
        foreach ($__languages as $key => $v) {
            $query .= ", `text_" . $v->getPrefix() . "`";
            $query .= ", `title_" . $v->getPrefix() . "`";
        }
        $query .= ") VALUES (?,?,?";
        foreach ($__languages as $key => $v) {
            $query .= ",?";
            $query .= ",?";
        }
        $query .= ")";
        $stm = $pdo->prepare($query);
        $stm->bindValue(1, $_POST['url'], PDO::PARAM_STR);
        if ((isset($_POST['type'])) && ($_POST['type'] == 'video')) {
            $stm->bindValue(2, str_replace('watch?v=', 'embed/', $_POST["new_image"]), PDO::PARAM_STR);
        } else {
            $stm->bindValue(2, $_POST['new_image'], PDO::PARAM_STR);
        }
        $stm->bindValue(3, $_POST['type'], PDO::PARAM_STR);
        $n = 4;
        foreach ($__languages as $key => $v) {
            $stm->bindValue($n, $_POST['new_text_' . $v->getPrefix()], PDO::PARAM_STR);
            $n++;
            $stm->bindValue($n, $_POST['title_' . $v->getPrefix()], PDO::PARAM_STR);
            $n++;
        }
        $stm->execute();
        $lastId = $pdo->lastInsertId();

        if (isset($_POST['newsletter']) && $_POST['newsletter'] == 1) {
            $stm = $pdo->prepare('SELECT * FROM newsletter_members');
            $stm->execute();
            $members = $stm->fetchAll();
            $allEmails = array_map(function ($entry) {
                return $entry['email'];
            }, $members);

            $placeholders = implode(', ', array_fill(0, count($allEmails), "(?, ?, ?)"));
            $sql = "INSERT INTO `newsletter_queue` (`news_id`, `subscriber_id`, `email`) VALUES $placeholders";
            $stm = $pdo->prepare($sql);
            $i = 1;
            foreach ($members as $member) {
                $stm->bindValue($i++, $lastId);
                $stm->bindValue($i++, $member['id']);
                $stm->bindValue($i++, $member['email']);
            }
            $stm->execute();
        }

    }
}

function _is_youtube($url)
{
    return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url) || preg_match('/youtube\.com\/embed/i', $url) || preg_match('/youtube\.com\/v/i', $url));
}

function _is_vimeo($url)
{
    return (preg_match('/vimeo\.com/i', $url));
}

function youtube_id_from_url($url)
{
    $pattern =
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x';
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}

require '../template/__top.php';
?>
    <script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode: "textareas",
            theme: "advanced",
            plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,cut,copy,paste,pastetext,pasteword,|,undo,redo,link,unlink,forecolor,|,bullist,numlist,|,outdent,indent",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: true,

            // Example content CSS (should be your site CSS)
            //content_css : "<?php //echo url_template_folder; ?>//images/css3.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "lists/template_list.js",
            external_link_list_url: "lists/link_list.js",
            external_image_list_url: "lists/image_list.js",
            media_external_list_url: "lists/media_list.js",

            // Style formats
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],

            language: 'bg',

            theme_advanced_resizing: false
        });
    </script>
    <a class="addbutton" href="#"
       style="color: #fff;background-color: #666666;width: auto;margin-left: 10px;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;float: left;font-size: 13px;line-height: 25px;margin-top: 13px;">+
        новина</a>
    <div style="clear: both;"></div>
<?php if (isset($err) && $err != "") { ?>
    <div style=" color: red; font-size: 12px; margin-left: 10px; margin-top: 10px; padding-bottom: 0px;">
        <?php echo "Моля въведете " . $err; ?>
    </div>
    <style>
        .newform {
            display: block;
        }
    </style>
<?php } ?>
    <form class="newform" action="" method="post" enctype="multipart/form-data"
          style="border: solid 1px; margin: 15px 0 0 10px;width: 1006px;font-size: 13px;">
        <table cellspacing="10">
            <tr>
                <td valign="top">
                    <?php foreach ($__languages as $key => $v) { ?>
                        <div style="font-size:13px;">Заглавие <?php echo $v->getName(); ?></div>
                        <div style="margin: 0px 0 10px;font-size:13px;"><input
                                    name="title_<?php echo $v->getPrefix(); ?>" style="width:400px; height: 26px;"
                                    value="<?php if (isset($_POST['title_' . $v->getPrefix()])) echo $_POST['title_' . $v->getPrefix()]; ?>"/>
                        </div>
                    <?php } ?>


                    <div style="font-size:13px;">Линк</div>
                    <div style="margin: 0px 0 15px;font-size:13px;"><input name="url" style="height: 26px;width:400px;"
                                                                           value="<?php if (isset($_POST['url'])) echo $_POST['url']; ?>"/>
                    </div>
                    <img id="new_image"
                         src="<?php if (isset($_POST['new_image']) && isset($_POST['type']) && $_POST['type'] == "news") echo url . $_POST['new_image']; else echo url . 'images/blank_400X300.png'; ?>"
                         style="width:400px;height:300px;"/>
                    <div><br/>
                        <select name="type" id="new_type" style="height: 26px;">
                            <option value="news">Снимка</option>
                            <option value="video" <?php if (isset($_POST['new_image']) && isset($_POST['type']) && $_POST['type'] == "video") echo "selected"; ?>>
                                youtube/vimeo видео
                            </option>
                        </select>
                        <script type="text/javascript">
                            $('#new_type').change(function () {
                                if ($(this).val() == "video") {
                                    $('#new_image_hidden').val('');
                                    $('#new_thickbox').css("display", "none");
                                    $('#new_image').css("display", "none");
                                    $('#labelvideo').css("display", "block");
                                    $('#new_image_hidden').detach().attr('type', 'text').insertAfter("#new_thickbox");
                                    $('#newsletter').attr('checked', false);
                                    $('.newsletter-hide').hide();
                                } else {
                                    $('#new_image_hidden').val('images/blank_400X300.png');
                                    $('#new_image_hidden').detach().attr('type', 'hidden').insertAfter("#new_thickbox");
                                    $('#new_thickbox').css("display", "block");
                                    $('#new_image').css("display", "block");
                                    $('#labelvideo').css("display", "none");
                                    $('.newsletter-hide').show();
                                }
                            });
                        </script>
                    </div>
                    <?php
                    if (isset($_POST['new_image']) && isset($_POST['type']) && $_POST['type'] == "video") {
                        ?>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#new_thickbox').css("display", "none");
                                $('#new_image').css("display", "none");
                                $('#new_image_hidden').detach().attr('type', 'text').insertAfter("#new_thickbox");
                            });
                        </script>
                        <?php
                    }
                    ?>
                    <style>
                        #labelvideo {
                            display: none;
                            margin-top: 20px;
                        }

                        #labelvideo2 {
                            display: none;
                        }
                    </style>
                    <div id="labelvideo" style="font-size:13px;">URL</div>
                    <input style="width:400px;" id="new_image_hidden" type="hidden" name="new_image"
                           value="<?php if (isset($_POST['new_image'])) echo $_POST['new_image']; else echo 'images/blank_400X300.png'; ?>"/>
                    <input type="hidden" name="new_text"/>
                    <a style="color: #fff;background-color: #666666;width: auto;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;float: left;font-size: 13px;line-height: 25px;margin-top: 20px;"
                       id="new_thickbox" class="thickbox"
                       href="../moduls/crop/new_crop.php?crop_width=400&crop_height=300&crop_html_img=new_image&crop_html_img_input_hidden=new_image&crop_upload_dir=../../../images/news/&keepThis=true&TB_iframe=true&height=650&width=1280">качи
                        снимка</a><br><br>
                </td>
                <td valign="top">
                    <?php foreach ($__languages as $key => $v) { ?>
                        <textarea style="font-size:13px; height:420px; width: 570px;"
                                  name="new_text_<?php echo $v->getPrefix(); ?>"
                                  style="width:1100px;height:420px;resize: none;"><?php if (isset($_POST['new_text_' . $v->getPrefix()])) echo $_POST['new_text_' . $v->getPrefix()]; ?></textarea>
                        <BR/>
                    <?php } ?>

                </td>
            </tr>
            <tr>
                <td colspan=2>
                    <?php if ($__user->permission_check('потребители', 'r', true, true)) { ?>
                        <input type="checkbox" value="1" checked="checked" id="newsletter" class="newsletter-hide"
                               name="newsletter"/>
                        <div class="newsletter-hide" style="font-size: 13px;display: inline-block;">бюлетин</div>
                        <br/>
                    <?php } ?>
                    <button style="margin-top:10px;" type="submit" class="button-save"><?php echo lang_save ?></button>
                    <button onclick="location.reload(); return false;" class="button-cancel">cancel</button>

                </td>
            </tr>

        </table>


    </form>


<?php
if (isset ($_GET['page'])) $page = (int)$_GET['page'];
else $page = 0;
$stm = $pdo->prepare('SELECT * FROM `news`');
$stm->execute();
$news_per_page = 21;
$news_count = $stm->rowCount();


$stm = $pdo->prepare('SELECT * FROM `news`  ORDER BY `news_date` DESC LIMIT :page,:perpage');
$stm->bindValue(':page', $page * $news_per_page, PDO::PARAM_INT);
$stm->bindValue(':perpage', $news_per_page, PDO::PARAM_INT);
//$stm -> bindValue(':type', $_GET['type'], PDO::PARAM_STR);
$stm->execute();

if (isset($_GET['newid'])) {
    $stm = $pdo->prepare('SELECT * FROM `news` WHERE `id`=:id');
    $stm->bindValue(':id', $_GET['newid'], PDO::PARAM_INT);
    $stm->execute();
    $new = $stm->fetch();
    ?>
    <form style="border:solid 1px; margin:15px 0 0 10px;float:left;font-size: 13px;" action="" method="post"
          enctype="multipart/form-data">
        <table cellspacing="10">
            <tr>
                <td valign="top">
                    <?php foreach ($__languages as $key => $v) { ?>
                        <div style="font-size:13px;">Заглавие <?php echo $v->getName(); ?></div>
                        <div style="margin: 0px 0 10px;font-size:13px;"><input
                                    class="ime_na_news_<?php echo $new['id']; ?>_<?php echo $v->getName(); ?>"
                                    name="title_<?php echo $v->getPrefix(); ?>"
                                    value="<?php echo $new['title_' . $v->getPrefix()]; ?>"
                                    style="width:400px; height: 26px;"/></div>
                    <?php } ?>

                    <div style="font-size:13px;">Линк</div>
                    <div style="margin: 0px 0 15px;font-size:13px;"><input name="url" value="<?php echo $new['url']; ?>"
                                                                           style="width:400px;height: 26px;"/></div>
                    <?php if ($new['Type'] == 'news') { ?>
                        <img id="news_image_<?php echo $new['id']; ?>" style="width:400px; height:300px"
                             src="<?php echo url . $new['image']; ?>"/>
                        <input style="width:400px;" id="image_hidden_<?php echo $new['id']; ?>" type="hidden"
                               name="news_image" value="<?php echo $new['image']; ?>"/>
                        <div><br/>
                            <select name="type_<?php echo $new['id']; ?>" style="height: 26px;"
                                    id="type_<?php echo $new['id']; ?>">
                                <option value="news">Снимка</option>
                                <option value="video">youtube/vimeo video</option>
                            </select>
                            <script type="text/javascript">
                                $('#type_<?php echo $new['id']; ?>').change(function () {
                                    if ($(this).val() == "video") {
                                        $('#image_hidden_<?php echo $new['id']; ?>').val('');
                                        $('#thickbox_<?php echo $new['id']; ?>').css("display", "none");
                                        $('#image_hidden_<?php echo $new['id']; ?>').detach().attr('type', 'text').insertAfter("#thickbox_<?php echo $new['id']; ?>").focus();
                                        $('#labelvideo2').css("display", "block");
                                    } else {
                                        $('#image_hidden_<?php echo $new['id']; ?>').val('<?php echo $new['image']; ?>');
                                        $('#image_hidden_<?php echo $new['id']; ?>').detach().attr('type', 'hidden').insertAfter("#thickbox_<?php echo $new['id']; ?>").focus();
                                        $('#thickbox_<?php echo $new['id']; ?>').css("display", "block");
                                        $('#labelvideo2').css("display", "none");
                                    }
                                });
                            </script>
                            <br/><br/>
                            <div id="labelvideo2" style="font-size:13px;">URL</div>
                            <a style="color: #fff;background-color: #666666;width: auto;padding-left: 7px;padding-right: 7px;height: 25px;display: block;text-align: center;float: left;font-size: 13px;line-height: 25px;margin-top: 13px;"
                               id="thickbox_<?php echo $new['id']; ?>" class="thickbox"
                               href="../moduls/crop/new_crop.php?crop_width=400&crop_height=300&crop_html_img=news_image_<?php echo $new['id']; ?>&crop_html_img_input_hidden=news_image_<?php echo $new['id']; ?>&crop_large_image_location=../../../<?php echo str_replace("thumbnail", "resize", $new['image']); ?>&crop_upload_dir=../../../images/news/&keepThis=true&TB_iframe=true&height=650&width=1280">редактиране
                                на снимката</a>
                        </div>
                    <?php } else { ?>
                        <iframe style="width:400px; height:225px" src="<?php echo $new['image']; ?>"
                                frameborder="0"></iframe>
                        <div><br/>
                            <select style="height: 26px;" name="type_<?php echo $new['id']; ?>"
                                    id="type_<?php echo $new['id']; ?>">
                                <option value="video">youtube/vimeo video</option>
                                <option value="news">Снимка</option>
                            </select>
                            <script type="text/javascript">
                                $('#type_<?php echo $new['id']; ?>').change(function () {
                                    if ($(this).val() == "video") {
                                        $('#image_hidden_<?php echo $new['id']; ?>').val('<?php echo $new['image']; ?>');
                                        $('#thickbox_<?php echo $new['id']; ?>').css("display", "none");
                                        $('#image_hidden_<?php echo $new['id']; ?>').detach().attr('type', 'text').insertAfter("#thickbox_<?php echo $new['id']; ?>").focus();
                                        $('#labelvideo2').css("display", "block");
                                    } else {
                                        $('#image_hidden_<?php echo $new['id']; ?>').val('/images/blank_400X300.png');
                                        $('#image_hidden_<?php echo $new['id']; ?>').detach().attr('type', 'hidden').insertAfter("#thickbox_<?php echo $new['id']; ?>").focus();
                                        $('#thickbox_<?php echo $new['id']; ?>').css("display", "block");
                                        $('#labelvideo2').css("display", "none");
                                    }
                                });
                            </script>
                            <br/><br/>
                            <input style="width:400px;" id="image_hidden_<?php echo $new['id']; ?>" type="text"
                                   name="news_image" value="<?php echo $new['image']; ?>"/>
                            <a style="display:none;" id="thickbox_<?php echo $new['id']; ?>" class="thickbox"
                               href="../moduls/crop/new_crop.php?crop_width=400&crop_height=300&crop_html_img=news_image_<?php echo $new['id']; ?>&crop_html_img_input_hidden=news_image_<?php echo $new['id']; ?>&crop_large_image_location=../../../<?php echo str_replace("thumbnail", "resize", $new['image']); ?>&crop_upload_dir=../../../images/news/&keepThis=true&TB_iframe=true&height=650&width=1280">редактиране
                                на снимката</a>
                        </div>
                    <?php } ?>
                </td>
                <td valign="top">
                    <?php foreach ($__languages as $key => $v) { ?>
                        <textarea name="text_<?php echo $v->getPrefix(); ?>"
                                  style="width:570px; height:420px;resize: none;font-size:13px;"><?php echo $new['text_' . $v->getPrefix()]; ?></textarea>
                        <br/>
                    <?php } ?>
                </td>
            </tr>

        </table>
        <input type="hidden" name="updateid" value="<?php echo $new['id']; ?>"/>
        <button style="margin-left:10px;line-height: 25px;" class="button-save"
                type="submit"><?php echo lang_save ?></button>
        <button type="button" class="button-cancel" style="line-height: 25px;" onclick="history.back();">cancel</button>
        <br/><br/>
    </form>
    <?php
} else {
    $rowCount = 0;
    echo '<div>';
    foreach ($stm->fetchAll() as $n) {
        if ($rowCount % 3 == 0) echo '<div class="row">';
        if ($n['image'] != 'images/blank_400X300.png') {
            $image = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $n['image']);
        } else {
            $image = $n['image'];
        }
        echo '<a href="?newid=' . $n['id'] . '"><div style="display: inline-block; width: 320px; margin-left: 10px; margin-top: 30px; margin-bottom: 20px; margin-right: 10px; margin-bottom: 10px; border-bottom: 1px solid #ccc;">';
        $date = new DateTime($n['news_date']);
        echo '<span class="date" style="float: left; margin-top: 5px; margin-bottom: 5px; font-size:10pt;color:#b2b2b2;">' . date_format($date, 'd-m-Y') . '</span>';
        echo '<div style="float: right; margin-top: 5px;"><span style="color: #8CC43F; font-size: 14px;" href="?newid=' . $n['id'] . '">E</span><span style="color: red; font-size: 14px; margin-left: 5px;" onclick="deleteNews(' . $n['id'] . ',\'' . $_GET['type'] . '\'); return false;">X</span></div><div style="clear:both;"></div>';
        if ($n['Type'] == 'news') {
            echo '<img style="max-width:320px; max-height:240px;margin:0px 20px 10px 0px; float: left;" src="' . url . $image . '"/>';
        } else {
            if (_is_vimeo($n['image'])) {
                $link = str_replace('https://vimeo.com/', 'https://vimeo.com/api/v2/video/', $n['image']) . '.php';

                $html_returned = unserialize(file_get_contents($link));

                $thumb_url = $html_returned[0]['thumbnail_large'];
                echo '<img style="width:320px;margin:0px 20px 10px 0px; float: left;" src="' . $thumb_url . '"/>';

            } elseif (_is_youtube($n['image'])) {
                $src = str_replace('watch?v=', 'embed/', $n['image']);
                echo '<img style="max-width:320px; max-height:225px;margin:0px 20px 10px 0px; float: left;" src="https://img.youtube.com/vi/' . youtube_id_from_url($n['image']) . '/0.jpg"/>';
            }
        }
        echo '<span class="ime_na_news_' . $n['id'] . '_' . lang_directory . '" style="font-size: 16px; float: left; min-height: 40px;"> ' . $n['title_' . lang_prefix] . '</span>';
        echo '</div></a>';
        $rowCount++;
        if ($rowCount % 3 == 0) echo '</div>';
    }
    ?>
    <br><br>
    <center>
        <div style="clear:both; height: 20px;"></div>
        <div class="bottom_lincs">
            <?php
            if ($news_count > $news_per_page) {
                foreach (po_taka_pagination($page, $news_count, $news_per_page, '&page=', '&lt;', '&gt;') as $v) {
                    $purl = $v['url'];
                    ?>


                    <?php if ($v['current']) : ?>

                        <a href="<?php echo $purl; ?>" class="bottom_lincs_activ"><?php echo $v['number']; ?></a>

                    <?php else : ?>

                        <a href="<?php echo $purl; ?>" class="bottom_lincs"><?php echo $v['number']; ?></a>

                    <?php endif; ?>


                <?php }
            }
            ?>
        </div>
        <div style="clear:both; height: 20px;"></div>
    </center>


    <div class="modal">

        <div class="modal-content">
            <div class="modal-top-lenta">
                <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                <div class="modal-top-close" onclick="hideModal();">×</div>
            </div>

            <div class="modal-body">

            </div>
        </div>
    </div>
    <?php
}