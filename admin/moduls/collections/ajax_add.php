<?php
require '../../__top.php';

$name = $_POST['name'];

$query = 'INSERT INTO `collections` (`name`) VALUES (:name)';
$stm = $pdo->prepare($query);
$stm->bindValue(":name", $_POST['name'], PDO::PARAM_STR);

$stm->execute();