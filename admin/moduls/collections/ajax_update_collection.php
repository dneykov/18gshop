<?php
require '../../__top.php';
$id = (int)$_POST['id'];
$name = $_POST['name'];

$stm = $pdo->prepare('UPDATE `collections` SET `name` = :name WHERE `id` = :id LIMIT 1');
$stm -> bindValue(":id", $id, PDO::PARAM_INT);
$stm -> bindValue(":name", $name, PDO::PARAM_INT);
$stm -> execute();

?>