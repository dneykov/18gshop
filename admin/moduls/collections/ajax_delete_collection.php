<?php
require '../../__top.php';
$id = (int)$_POST['id'];

$stm = $pdo->prepare('UPDATE `artikuli` SET `collection_id` = 0 WHERE `collection_id` = :id');
$stm -> bindValue(":id", $id, PDO::PARAM_INT);
$stm -> execute();

$stm = $pdo->prepare('DELETE FROM `collections` WHERE `id` = :id LIMIT 1');
$stm -> bindValue(":id", $id, PDO::PARAM_INT);
$stm -> execute();