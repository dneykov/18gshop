<?php

require '../__top.php';



// unset($_SESSION['crop_thumb_image_location']);
// unset($_SESSION['crop_large_image_location']);
// unset($_SESSION['crop_html_img']);

// $_SESSION['crop_upload_dir']='../../../images_adv/';

// $_SESSION['crop_width']=1000;
// $_SESSION['crop_height']=400;

try{

// $__upload_folder = 'images_adv/';
// $__upload_folder_fll_path = dir_root.'images_adv/';

if(isset ($_POST['adv_new'])) {
	
	   $url = $_POST['adv_url_new'];
	   $url = trim($url);
	   
	   if(isset($_POST['adv_text_new'])){
		   $adv_text = $_POST['adv_text_new'];
	   }else{
			$adv_text = '';
	   }
	   
	   $adv_text = stripslashes($adv_text);			   
	   $adv_text = trim($adv_text);   

	   	$err = "";
		$media = "";
		if(isset($_FILES['adv_file_new']) && ($_FILES['adv_file_new']['size'] > 0 )){
	        $time=time();
	        $max_file_size = 6000000;
	        $upload_required = true;
	        $upload_dir = '/www/18gshop.com/www/root/images/banners/';
	        $dir = 'images/banners/';

	        $upload_img=$_FILES['adv_file_new'];
	        
	        switch($upload_img['error']){
	            case UPLOAD_ERR_INI_SIZE:
	                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
	                break 2;

	            case UPLOAD_ERR_PARTIAL:
	                $err .= 'Грешка при качването на файла. Моля, опитайте отново.';
	                break 2;

	            case UPLOAD_ERR_NO_FILE:
	                $err .='Не сте избрали файл за качване.';
	                break 2;
	        }
	        
	        if($upload_img['size'] > $max_file_size) {
	                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
	        }
	        
	        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png');
	        $fileName 	= $_FILES['adv_file_new']['name'];
	        $fileExt 	= strtolower(substr($fileName, strpos($fileName,'.'), strlen($fileName)-1));
	        if(!in_array($fileExt,$allowed_filetypes)){
	            $err .= "Форматът на снимката е неподходящ. Моля, изберете друга снимка";
	        }
	        
	        if($err == "" && $upload_img['error']==0){
	            $time=time();
	            $rand=rand(1,30000);	

	            if(!move_uploaded_file($upload_img['tmp_name'],$upload_dir.$rand."_".$time.$fileExt)){
	                echo $upload_img['tmp_name'];
	                $err .= "Грешка при преместването на файла!";
	                $upload_img['error']++;
	            }
	            $file_name=$rand."_".$time.$fileExt;
	            $image = new Imagick($upload_dir.$file_name);
	            $image -> resizeImage(2560,800,Imagick::FILTER_LANCZOS,1);
	            $image -> writeImage($upload_dir.$file_name);
	            $media = $dir.$file_name;
	            $image -> destroy();
	            
	            $file_name=$rand."_".$time.$fileExt;
	            $image = new Imagick($upload_dir.$file_name);
	            $image -> resizeImage(1000,0,Imagick::FILTER_LANCZOS,1);
	            $image -> writeImage($upload_dir.$rand."_".$time."_t".$fileExt);
	            $media = $dir.$file_name;
	            $image -> destroy();
	        }
	    } 

	   $stm = $pdo->prepare('INSERT INTO `zz_banners` (podredba, image, url) VALUES(10,?, ? )');               
	  
	   $stm -> bindValue(1, $media, PDO::PARAM_STR);

	   $stm -> bindValue(2, $url, PDO::PARAM_STR);

	   $stm -> execute();
	$lastId = $pdo->lastInsertId();
		po_taka_update_multilanguage('adv_text_new_', 'adv_text_', 'zz_banners', (int) $lastId);

	
	
}



if(isset ($_POST['adv_submit'])) {


    $stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=10 order by `id` DESC' );
    $stm -> execute();
  
    foreach ($stm->fetchAll() as $v)
    {
         if(isset ($_POST['adv_delete_'.$v['id']])) {

			 delete_file(dir_root.$v['image']);

			 $stm = $pdo->prepare('DELETE FROM `zz_banners` WHERE `id` = ? ');
			 $stm -> bindValue(1, $v['id'], PDO::PARAM_INT);
			 $stm -> execute();

        }


        else {
           $stm = $pdo -> prepare('UPDATE `zz_banners` SET `url` = ?, `image` = ? WHERE `id` = ? ');

           $url = $_POST['adv_url_'.$v['id']];
           $url = trim($url);
		   
           $stm -> bindValue(1, $url);

           	$err = "";
			if(isset($_FILES['adv_file_new_'.$v['id']]) && ($_FILES['adv_file_new_'.$v['id']]['size'] > 0 )){
				$media = "";
		        $time=time();
		        $max_file_size = 6000000;
		        $upload_required = true;
		        $upload_dir = '/www/18gshop.com/www/root/images/banners/';
		        $dir = 'images/banners/';

		        $upload_img=$_FILES['adv_file_new_'.$v['id']];
		        
		        switch($upload_img['error']){
		            case UPLOAD_ERR_INI_SIZE:
		                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
		                break 2;

		            case UPLOAD_ERR_PARTIAL:
		                $err .= 'Грешка при качването на файла. Моля, опитайте отново.';
		                break 2;

		            case UPLOAD_ERR_NO_FILE:
		                $err .='Не сте избрали файл за качване.';
		                break 2;
		        }
		        
		        if($upload_img['size'] > $max_file_size) {
		                $err .= 'Размерът на файла е прекалено голям. Не може да е повече от '.$max_file_size.'B.';
		        }
		        
		        $allowed_filetypes = array('.jpg', '.jpeg', '.gif', '.bmp', '.png');
		        $fileName 	= $_FILES['adv_file_new_'.$v['id']]['name'];
		        $fileExt 	= strtolower(substr($fileName, strpos($fileName,'.'), strlen($fileName)-1));
		        if(!in_array($fileExt,$allowed_filetypes)){
		            $err .= "Форматът на снимката е неподходящ. Моля, изберете друга снимка";
		        }
		        
		        if($err == "" && $upload_img['error']==0){
		            $time=time();
		            $rand=rand(1,30000);	

		            if(!move_uploaded_file($upload_img['tmp_name'],$upload_dir.$rand."_".$time.$fileExt)){
		                echo $upload_img['tmp_name'];
		                $err .= "Грешка при преместването на файла!";
		                $upload_img['error']++;
		            }
		            $file_name=$rand."_".$time.$fileExt;
		            $image = new Imagick($upload_dir.$file_name);
		            $image -> resizeImage(2560,800,Imagick::FILTER_LANCZOS,1);
		            $image -> writeImage($upload_dir.$file_name);
		            $media = $dir.$file_name;
		            $image -> destroy();
		            
		            $file_name=$rand."_".$time.$fileExt;
		            $image = new Imagick($upload_dir.$file_name);
		            $image -> resizeImage(1000,0,Imagick::FILTER_LANCZOS,1);
		            $image -> writeImage($upload_dir.$rand."_".$time."_t".$fileExt);
		            $media = $dir.$file_name;
		            $image -> destroy();
		        }
		        $stm -> bindValue(2, $media, PDO::PARAM_STR);
		    } else {
		    	$stm -> bindValue(2, $_POST['adv_file_old_'.$v['id']], PDO::PARAM_STR);
		    }
           	$stm -> bindValue(3, $v['id'], PDO::PARAM_INT);

           	$stm -> execute();
			po_taka_update_multilanguage('adv_text_'.$v['id'].'_', 'adv_text_', 'zz_banners', (int) $v['id']);
        }


    }

	if(isset ($_POST['adv_delete_background'])){
		copy(dir_root.'images/white1x1.jpg',dir_root.'images/background.jpg');
	}

}

$stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=1 order by `id` DESC');
$stm -> execute();
$banners = $stm -> fetchAll();

$stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=10 order by `id` DESC');
$stm -> execute();
$categories_banners = $stm -> fetchAll();
} catch (Exception $e){
    greshka($e);
}
require dir_root_admin_template.'__top.php';
require 'adv_menu.php';
?>

<?php //require 'adv_menu.php'; ?>
<style>

</style>
<a href="#" class="addbutton" style="font-size: 14px;display:block;margin-top: 10px;">+ нов банер</a>
<form class="newform" style="margin:10px;width:500px;font-size: 14px;padding-bottom: 60px;" action="" method="post" enctype="multipart/form-data">

		<div>
			<img id="adv_file_new" src="<?php echo url?>/images/white1x1.png" style="width:500px;height:275px;display:block;"/> <br/>
			<!-- <a  class="thickbox" href='crop/new_crop.php?crop_html_img=adv_file_new&crop_width=1000&crop_height=400&crop_html_img_input_hidden=adv_file_new&keepThis=true&TB_iframe=true&height=650&width=1280'>+ снимка</a> -->
			<input type="file" name="adv_file_new" style="margin-top: 5px;">
		</div>
			
		<br />
		<div>url</div>
		<div><input type="text" style="width:500px;" name="adv_url_new"></div>
	
		<?php 
			foreach ($__languages as $lang) { 
				?>
					<br />
					<div>текст <?php echo $lang->getName(); ?></div>
					<div><textarea style="width:500px;" name="adv_text_new_<?php echo $lang->getPrefix(); ?>"></textarea></div>
				<?php
			
			}
		?>	
		<div><input type="submit" name="adv_new" value="save"> <input type="submit" value="<?php echo lang_cancel; ?>" onclick="cancel();return false;'"></div>
		<br /><br />
</form>

<form style="margin:-40px 10px 10px 10px;" action="" method="post" enctype="multipart/form-data"><?php

echo '<br>';

foreach ($categories_banners as $v)
{
    ?>
    <div style="float:left; margin:40px 40px 0px 0px;font-size: 14px;" id="adv_<?php echo $v['id']; ?>" >
		<span style="color: #F7931E">2560x800</span><div style="float:right;">изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"></div>
		<br />
		<img id="adv_file_<?php echo $v['id']; ?>" src="<?php echo url.htmlspecialchars( $v['image']); ?>" style="width:500px" /><br>
		<!-- <a  class="thickbox" href='crop/new_crop.php?crop_html_img_b=adv_file_<?php echo $v['id']; ?>&crop_width=1000&crop_height=400&crop_html_img_input_hidden=adv_file_<?php echo $v['id']; ?>&crop_large_image_location=<?php echo "../../../".str_replace("thumbnail","resize",$v['image']); ?>&keepThis=true&TB_iframe=true&height=650&width=1280'>Редактирай снимката</a> -->
		<input type="file" name="adv_file_new_<?php echo $v['id']; ?>" value="<?php echo $v['image']; ?>" style="margin-top: 5px;"><br>
		<input type="hidden" name="adv_file_old_<?php echo $v['id']; ?>" value="<?php echo $v['image']; ?>"><br>
		url<br>
		<input type="text" style="width:500px;" name="adv_url_<?php echo $v['id']; ?>" value="<?php echo htmlspecialchars($v['url']); ?>"><br>
		<?php 
			foreach ($__languages as $lang) { 
				?>
					<br />
					<div>текст <?php echo $lang->getName(); ?></div>
					<div><textarea style="width:500px;" name="adv_text_<?php echo $v['id']; ?>_<?php echo $lang->getPrefix(); ?>"><?php echo $v['adv_text_'.$lang->getPrefix()]; ?></textarea></div>
				<?php
			
			}
		?>	
	</div>
    <?php
		/* изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"> */
   

}
?>
	<div style="clear:both;"></div>
	<div>
	    <input type="submit" name="adv_submit" value="<?php echo lang_save; ?>">
	    <input type="button" value="<?php echo lang_cancel; ?>" onclick="window.location='<?php echo addslashes(url_admin); ?>'">
    </div>
</form>

<script type="text/javascript">

function adv_delete(id){

is_checkbox = $('[name=adv_delete_'+id+']').attr('checked');

if(is_checkbox) {
    //$('[name=adv_url_'+id+']').val('');
    $('[name=adv_url_'+id+']').attr('disabled', 'true');

    //$('[name=adv_file_'+id+']').val('');
    $('[name=adv_file_'+id+']').attr('disabled', 'true');

	$('[name=adv_text_'+id+']').attr('disabled', 'true');
	
	$('#adv_'+id).css('opacity', '0.5');

}
else {
    $('[name=adv_url_'+id+']').removeAttr('disabled');
    $('[name=adv_file_'+id+']').removeAttr('disabled');
    $('[name=adv_text_'+id+']').removeAttr('disabled');
    $('#adv_'+id).css('opacity', '1');
}


}
</script>
