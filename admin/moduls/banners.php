<?php

require '../__top.php';



unset($_SESSION['crop_thumb_image_location']);
unset($_SESSION['crop_large_image_location']);
unset($_SESSION['crop_html_img']);

$_SESSION['crop_upload_dir'] = '../../../images/banners';

$_SESSION['crop_width'] = 160;
$_SESSION['crop_height'] = 400;

try{

$__upload_folder = 'images/banners';
$__upload_folder_fll_path = dir_root . 'images/banners';


if(isset ($_POST['adv_new'])) {
    $url = $_POST['adv_url_new'];
    $url = trim($url);

    if(isset($_POST['adv_text_new'])){
       $adv_text = $_POST['adv_text_new'];
    }else{
        $adv_text = '';
    }

    $adv_text = stripslashes($adv_text);
    $adv_text = trim($adv_text);

    $stm = $pdo->prepare('INSERT INTO `zz_banners` (podredba, image, url) VALUES(1,?, ? )');

    $stm -> bindValue(1, $_POST['adv_file_new'], PDO::PARAM_STR);

    $stm -> bindValue(2, $url, PDO::PARAM_STR);

    $stm -> execute();
    $lastId = $pdo->lastInsertId();
    po_taka_update_multilanguage('adv_text_new_', 'adv_text_', 'zz_banners', (int) $lastId);
}



if(isset ($_POST['adv_submit'])) {


    $stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=1 order by `id` DESC' );
    $stm -> execute();

    foreach ($stm->fetchAll() as $v)
    {
         if(isset ($_POST['adv_delete_'.$v['id']])) {
             $thumbnailFilePath = pathinfo(dir_root . $v['image']);
             $resizeFilePath = pathinfo(dir_root . str_replace("thumbnail", "resize", $v['image']));

			 delete_file(dir_root.$v['image']);
			 delete_file(dir_root . str_replace("thumbnail", "resize", $v['image']));
             delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_tt." . $thumbnailFilePath['extension']);
             delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_t." . $thumbnailFilePath['extension']);
             delete_file($thumbnailFilePath['dirname'] . "/" . $thumbnailFilePath['filename'] . "_b." . $thumbnailFilePath['extension']);
             delete_file($resizeFilePath['dirname'] . "/" . $resizeFilePath['filename'] . "_org." . $resizeFilePath['extension']);

			 $stm = $pdo->prepare('DELETE FROM `zz_banners` WHERE `id` = ? ');
			 $stm -> bindValue(1, $v['id'], PDO::PARAM_INT);
			 $stm -> execute();

        } else {
            $stm = $pdo -> prepare('UPDATE `zz_banners` SET `url` = ?, `image` = ? WHERE `id` = ? ');

            $url = $_POST['adv_url_'.$v['id']];
            $url = trim($url);



            $stm -> bindValue(1, $url);

            $stm -> bindValue(2, $_POST['adv_file_'.$v['id']], PDO::PARAM_STR);
            $stm -> bindValue(3, $v['id'], PDO::PARAM_INT);

            $stm -> execute();
            po_taka_update_multilanguage('adv_text_'.$v['id'].'_', 'adv_text_', 'zz_banners', (int) $v['id']);
        }


    }



}






    
$stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=1 order by `id` DESC');
$stm -> execute();


$banners = $stm -> fetchAll();





} catch (Exception $e){
    greshka($e);
}

require dir_root_admin_template.'__top.php';


require 'adv_menu.php';
?>
<a href="#" class="nov addbutton" style="margin-top: 0px; margin-left: 10px;">+ нов банер</a>
<div style="clear: both;"></div>
<form class="newform" style="margin:10px;width:225px;font-size: 14px;" action="" method="post" enctype="multipart/form-data">

		<div>
			<img id="adv_file_new" src="<?php echo url?>/images/white1x1.png" style="width:160px;height:400px;display:block;"/> <br/>
			<a class="thickbox"  style="margin-bottom: 10px;color: #fff;background-color: #666666;width: 100px;height: 25px;display: block;text-align: center;float: left;font-size: 13px;line-height: 22px;" href='crop/new_crop.php?crop_html_img=adv_file_new&crop_width=160&crop_height=400&crop_html_img_input_hidden=adv_file_new&keepThis=true&TB_iframe=true&height=650&width=1280'>+ снимка</a>
            <br>
			<input type="hidden" name="adv_file_new" value="/images/white1x1.png">
		</div>
			
		<br />
		<div style="font-size: 13px;">url</div>
		<div><input type="text" style="width:160px;" name="adv_url_new"></div>
	
		<div><button type="submit" name="adv_new" style="margin-top: 10px;" class="button-save">save</button> <button type="submit" class="button-cancel" onclick="cancel();return false;'"><?php echo lang_cancel; ?></button></div>
		<br /><br />
</form>

<form style="margin:-20px 10px 10px 10px;"action="" method="post" enctype="multipart/form-data"><?php

echo '<br>';
$n = 0;
foreach ($banners as $v)
{
    ?>
    <div style="float:left; margin:40px 40px 0px 0px;font-size: 14px;" id="adv_<?php echo $v['id']; ?>" >
		<span style="color: #F7931E">160x400</span><div style="float:right;">изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"></div>
		<br />
		<img id="adv_file_<?php echo $v['id']; ?>" src="<?php echo url.htmlspecialchars( $v['image']); ?>" style="height:400px;width:160px" /><br>
		<a  class="thickbox" href='crop/new_crop.php?crop_html_img_b=adv_file_<?php echo $v['id']; ?>&crop_width=160&crop_height=400&crop_html_img_input_hidden=adv_file_<?php echo $v['id']; ?>&crop_large_image_location=<?php echo "../../../".str_replace("thumbnail","resize",$v['image']); ?>&keepThis=true&TB_iframe=true&height=650&width=1280'>Редактирай снимката</a>
        <br>
		<input type="hidden" name="adv_file_<?php echo $v['id']; ?>" value="<?php echo $v['image']; ?>"><br>
		url<br>
		<input type="text" style="width:160px;" name="adv_url_<?php echo $v['id']; ?>" value="<?php echo htmlspecialchars($v['url']); ?>"><br>
		
	</div>
    <?php
		/* изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"> */
   
	$n++;
}

if($n == 0) echo '<div style="clear:both;height:20px;"></div>';
/*
echo '<br><br><div style="clear:both;"></div>';

foreach ($banners as $v)
{
    ?>
    <div style="float:left; margin:0px 70px 70px 0px;" id="adv_<?php echo $v['id']; ?>" >
		240x160<!-- <div style="float:right;">изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"></div> -->
		<br />
		<img id="adv_file_<?php echo $v['id']; ?>" src="<?php echo url.htmlspecialchars(preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $v['image'] )); ?>" style="width:240px;height:160px;" /><br>
		<a  class="thickbox" href='crop/new_crop.php?crop_html_img_b=adv_file_<?php echo $v['id']; ?>&crop_html_img_input_hidden=adv_file_<?php echo $v['id']; ?>&crop_large_image_location=<?php echo "../../../".str_replace("thumbnail","resize",$v['image']); ?>&crop_width=240&crop_height=160&keepThis=true&TB_iframe=true&height=650&width=1280'>редактиране</a>
		<input type="hidden" name="adv_file_<?php echo $v['id']; ?>" value="<?php echo $v['image']; ?>"><br>
		url<br>
		<input type="text" style="width:240px;" name="adv_url_<?php echo $v['id']; ?>" value="<?php echo htmlspecialchars($v['url']); ?>"><br>
		<?php foreach ($__languages as $lang) { 
			echo 'текст '.$lang->getName().'<br />';
		?>	
			<input style="width:240px;resize:none;" name="adv_text_<?php echo $v['id'].'_'.$lang->getPrefix(); ?>" value="<?php echo htmlspecialchars($v['adv_text_'.$lang->getPrefix()]); ?>" /><br>
		<?php }?>

	</div>
    <?php
		изтрий <input type="checkbox" value="1" name="adv_delete_<?php echo $v['id']; ?>" onchange="adv_delete(<?php echo $v['id']; ?>)"> 
   

}
*/
?>
	<div style="clear:both"></div>
    <br><br><br><br>
    <button type="submit" name="adv_submit" class="button-save"><?php echo lang_save; ?></button>
    <button class="button-cancel" onclick="window.location='<?php echo addslashes(url_admin); ?>'"><?php echo lang_cancel; ?></button>
</form>

<script type="text/javascript">

function adv_delete(id){

is_checkbox = $('[name=adv_delete_'+id+']').attr('checked');

if(is_checkbox) {
    //$('[name=adv_url_'+id+']').val('');
    $('[name=adv_url_'+id+']').attr('disabled', 'true');

    //$('[name=adv_file_'+id+']').val('');
    $('[name=adv_file_'+id+']').attr('disabled', 'true');

	$('[name=adv_text_'+id+']').attr('disabled', 'true');
	
	$('#adv_'+id).css('opacity', '0.5');

}
else {
    $('[name=adv_url_'+id+']').removeAttr('disabled');
    $('[name=adv_file_'+id+']').removeAttr('disabled');
    $('[name=adv_text_'+id+']').removeAttr('disabled');
    $('#adv_'+id).css('opacity', '1');
}


}
</script>
<div style="clear:both;height: 60px;"></div>
