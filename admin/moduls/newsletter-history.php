<?php
require '../__top.php';
require dir_root_admin.'/template/__top.php';

if(isset($_POST['delete']) && $_POST['delete'] == 'delete') {
    $stm = $pdo->prepare('DELETE FROM `newsletter_history` WHERE `id` = ? LIMIT 1');
    $stm -> bindValue(1, $_POST['nid'], PDO::PARAM_INT);
    $stm -> execute();
}

?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#AddCatBtn').click(function() {
            var nameBG = $('#newCategoryBG').val();
            $.ajax({
                type: "POST",
                url: "newsletter/ajax_new_category.php",
                data: "namebg="+nameBG,
                cache: false,
                success: function(html){
                    window.location.href = '/admin/moduls/newsletter.php?id='+html;
                }
            });
        });
        $('#EditCatBtn').click(function() {
            var nameBG = $('#editCategoryBG').val();
            var id = $('#CategoryId').val();
            $.ajax({
                type: "POST",
                url: "newsletter/ajax_edit_category.php",
                data: "namebg="+nameBG+"&id="+id,
                cache: false,
                success: function(html){
                    location.reload();
                }
            });
        });
        $('#CancelAddCatBtn').click(function() {
            $('#newCategoryBG').val("");
            ShowAddCategory();
        });
        $('#CancelEditCatBtn').click(function() {
            $('#editCategoryBG').val("");
            editCategory();
        });


        $('#CancelAddMember').click(function() {
            ShowAddArticle();
        });

        $('.deleteNewsletter').click(function() {
            var id = $(this).attr('id');
//            var checkstr =  confirm("Изтриване на "+$('#a-link-'+id+" .name").html()+"\n\nСигурни ли сте?");
//            if(checkstr == true) {
//                return true
//            } else {
//                return false;
//            }
            var ime_na_kol = $('#a-link-'+id+" .name").html();
            $(".modal-body").html('<div style="width: 100%; height: 40px;">' + ime_na_kol + '</div> <button class="button-cancel" onclick="hideModal();" style="position: relative;top: 60px;display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deleteNewsletterAction('+ id +');" style="display: block;position: relative;top: 60px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
            $(".modal").show();

            return false;
        });
    });

    function deleteNewsletterAction(id) {
        $("#a-" + id + " form").submit();
    }

    function ShowAddCategory(){
        if ($("#AddCategory").is(":hidden")) {
            $("#AddCategory").fadeIn(300);
        } else {
            $("#AddCategory").hide(300);
        }
    }
    function ShowAddArticle(){
        $('.ArticleDiv').slideUp("fast");
        if ($("#AddArticleDiv").is(":hidden")) {
            $("#AddArticleDiv").fadeIn(300);
        } else {
            $("#AddArticleDiv").hide(300);
        }
    }
    function editCategory(){
        if ($("#editCategory").is(":hidden")) {
            $("#editCategory").fadeIn(300);
        } else {
            $("#editCategory").hide(300);
        }
    }
    function deleteCategory(){
        var id = $('#CategoryId').val();
        var checkstr =  confirm("Изтриване на "+$('#cat'+id+' a').html()+"\n\nСигурни ли сте?");
        if(checkstr == true) {
            $.ajax({
                type: "POST",
                url: "newsletter/ajax_delete_category.php",
                data: "id="+id,
                cache: false,
                success: function(html){
                    history.pushState({}, 'title', '/admin/moduls/newsletter.php?id='+html);
                    location.reload();
                }
            });
        } else {
            return false;
        }

    }
    function toggleCont(x) {
        if($('.ArticleDiv').is(':visible')) {
            $('.ArticleDiv').slideUp("fast");
            $('.a-state').html('+');
        }
        if($('#a-'+x).is(":hidden")) {
            $('#a-'+x).slideDown("fast");
            $('#a-state-'+x).html('-');
        } else {
            $('#a-'+x).hide();
            $('#a-state-'+x).html('+');
        }
    }
</script>
<style>
    #leftCol {
        margin-left: 5px;
        padding-top: 30px;
        float: left;
        width: 200px;
        font-size: 13px;
    }
    #leftCol a{
        margin-left: 5px;
    }
    #leftCol a:hover{
        text-decoration: none;
        color: #f8931d;
    }
    .mail-label {
        color: #f8931d;
        font-size: 12px;
    }
    #leftCol button {
        margin-top: 10px;
        margin-bottom: 20px;
    }
    #rightCol {
        padding-top: 13px;
        padding-left: 15px;
        float: left;
    }
    #help_cats {
        height: 20px;
        line-height: 10px;
        color: #fff;
        width: 100%;
    }
    #AddCategory {
        padding-left: 5px;
        padding-top: 5px;
        display: none;
    }
    #AddTag {
        padding-left: 5px;
        padding-top: 5px;
        display: none;
    }
    .addbutton {
        display: block;
        margin: 5px 0 15px 0;
        font-size: 13px;
    }
    .addbutton:hover {
        text-decoration: none;
        color: #f8931d;
    }
    .addbutton:nth-child(1){
        margin-top: -20px;
    }
    #editCategory {
        margin-left: 5px;
        display: none;
    }
    #AddArticleDiv {
        display: none;
        margin-bottom: 50px;
    }
    #AddArticleDiv label, .ArticleDiv label {
        font-size: 14px;
        color: #F8931F;
        margin-bottom: 2px;
    }
    .ArticleDiv {
        border: 1px solid #ccc;
        border-top:none;
        margin-bottom: 20px;
        width: 770px;
        display: none;
        font-size: 13px;
        padding: 10px;
    }

    .ArticleDiv p{
        font-size: 13px;
    }
    .a-state {
        position: absolute;
        top:2px;
        right:10px;
    }
    .a-link {
        margin-top: 10px;
        cursor: pointer;
        height: 15px;
        font-size: 14px;
        padding-bottom: 5px;
        border-bottom: 1px solid #ccc;
        padding-left: 10px;
        width: 782px;
        position: relative;
    }
    .editTag {
        margin-left: 5px;
        display: none;
    }
    .tags {
        font-size: 13px !important;
        cursor: pointer;
    }
    .tags:hover {
        color: #f8931d;
    }
    .erroradd {
        font-size: 11px;
        color: red;
    }
    #navigation {
        width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
    }
    #navigation a {
        display: block;
        float: left;
        margin: 0 15px;
        font-size: 13px;
        color: #333;
        line-height: 23px;
    }
    input[type=text] {
        font-size: 13px;
    }
    input[type=submit]{
        margin-top: 25px;
    }
</style>
<div id="navigation">
    <a href="<?php echo url."admin/moduls/create-newsletter.php"; ?>">Нов бюлетин</a>
    <a href="<?php echo url."admin/moduls/newsletter-history.php"; ?>" style="color: #F8931F">История</a>
    <a href="<?php echo url."admin/moduls/newsletter.php"; ?>">Мейл лист</a>
</div>
<div id="errors"></div>
<div id="RightCol">
    <script type="text/javascript" src="<?php echo url; ?>js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode : "textareas",
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,cut,copy,paste,pastetext,pasteword,|,undo,redo,link,unlink,forecolor,|,bullist,numlist,|,outdent,indent",
            theme_advanced_buttons2 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo url_template_folder; ?>images/style_map.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Style formats
            style_formats : [
                {title : 'Bold text', inline : 'b'},
                {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
                {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
                {title : 'Example 1', inline : 'span', classes : 'example1'},
                {title : 'Example 2', inline : 'span', classes : 'example2'},
                {title : 'Table styles'},
                {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
            ],

            language : 'bg',

            theme_advanced_resizing : false
        });
    </script>
    <?php
        $stm = $pdo->prepare("SELECT * FROM `newsletter_history` ORDER BY `subject` ASC ");
        $stm -> execute();
        foreach ($stm->fetchAll() as $a) {
            ?>
            <div class="a-link" id="a-link-<?php echo $a['id'];?>" onclick="toggleCont('<?php echo $a['id'];?>')"><span id="a-state-<?php echo $a['id']; ?>" class="a-state">+</span><span class="name"><?php echo $a['subject']; ?></span></div>
            <div class="ArticleDiv" id="a-<?php echo $a['id'];?>">
                <form enctype="multipart/form-data" action="" method="post">
                    <label for="name">Изпратен на</label>
                    <?php
                    $all_groups = "";
                    foreach(json_decode($a['recipients']) as $group) {
                        $all_groups .= "'" . $group . "', ";
                    }
                    $all_groups = substr($all_groups, 0, -2);

                    $_stm = $pdo->prepare("SELECT `name_bg` FROM `newsletter_groups` WHERE `id` IN ($all_groups) ORDER BY `name_bg` ASC ");
                    $_stm -> execute();
                    $recipients = $_stm->fetchAll();
                    foreach ($recipients as $recipient) {
                        if ($recipient === end($recipients)) {
                            echo $recipient['name_bg'];
                        } else {
                            echo $recipient['name_bg'] . ", ";
                        }
                    }
                    ?>
                    <br/>
                    <label>Заглавие</label>
                    <?php echo $a['subject']; ?>
                    <br/>
                    <label>Съдържание</label>
                    <?php if($a['img'] != "") : ?>
                        <img id="adv_img_new" src="<?php echo url.'/'.$a['img']; ?>" style="max-width:770px;display:block;margin-bottom: 10px;margin-top:7px;"/>
                    <?php endif; ?>
                    <?php echo $a['content']; ?>
                    <input type="hidden" name="nid" value="<?php echo $a['id']; ?>"/>
                    <input type="hidden" name="delete" value="delete"/>
                    <input type="submit" name="delete" class="deleteNewsletter button-cancel" id="<?php echo $a['id']; ?>" value="delete">
                </form>
            </div>
            <?php
        }
    ?>
</div>
<div style="clear:both;height: 60px;"></div>
<div class="modal">
    <div class="modal-content">
        <div class="modal-top-lenta">
            <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
            <div class="modal-top-close" onclick="hideModal();">×</div>
        </div>

        <div class="modal-body">

        </div>
    </div>
</div>