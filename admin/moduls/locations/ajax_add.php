<?php
require '../../__top.php';

$name = $_POST['name'];
$delivery = $_POST['delivery'];
$freedelivery = $_POST['freedelivery'];

$stm = $pdo->prepare("INSERT INTO `locations`(`country_name`, `delivery_price`, `free_delivery`) VALUES (?,?,?)");
$stm -> bindValue(1, $name, PDO::PARAM_STR);
$stm -> bindValue(2, $delivery, PDO::PARAM_INT);
$stm -> bindValue(3, $freedelivery, PDO::PARAM_INT);
$stm -> execute();

?>