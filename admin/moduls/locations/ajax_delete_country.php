<?php
	require '../../__top.php';
	$id = (int)$_POST['id'];
	$stm = $pdo->prepare('DELETE FROM `locations` WHERE `id` = :id LIMIT 1');
	$stm -> bindValue(":id", $id, PDO::PARAM_INT);
	$stm -> execute();
?>