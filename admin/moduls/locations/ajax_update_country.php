<?php
	require '../../__top.php';
	$id = (int)$_POST['id'];
	$name = $_POST['name'];
	$delivery = $_POST['delivery'];
	$freedelivery = $_POST['freedelivery'];

	$stm = $pdo->prepare('UPDATE `locations` SET `country_name` = :name, `delivery_price` = :delivery, `free_delivery` = :freedelivery  WHERE `id` = :id LIMIT 1');
	$stm -> bindValue(":id", $id, PDO::PARAM_INT);
	$stm -> bindValue(":name", $name, PDO::PARAM_STR);
	$stm -> bindValue(":delivery", $delivery, PDO::PARAM_INT);
	$stm -> bindValue(":freedelivery", $freedelivery, PDO::PARAM_INT);
	$stm -> execute();

?>