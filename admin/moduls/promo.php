<?php 
require '../__top.php';
require '../template/__top.php';
?>
<script>
	$(document).ready(function(){
		$('#category').change(function() {
			var selected = $('#category').val();
			$.ajax({
	            type: "POST",
	            url: "promo/ajax_category_vid.php",
	            data: "cat="+selected,
	            cache: false,
	            success: function(html){
	            	$('#vidbox').html(" ");
	            	$('#vidbox').append(html);
	            }
	        });
	        $.ajax({
	            type: "POST",
	            url: "promo/ajax_category_model.php",
	            data: "cat="+selected,
	            cache: false,
	            success: function(html){
	            	$('#modelbox').html(" ");
	            	$('#modelbox').append(html);
	            }
	        });
		});
		$('#vid').change(function() {
			var cat = $('#category').val();
			var vid = $('#vid').val();
			$.ajax({
	            type: "POST",
	            url: "promo/ajax_vid_model.php",
	            data: "cat="+cat+"&vid="+vid,
	            cache: false,
	            success: function(html){
	            	$('#modelbox').html(" ");
	            	$('#modelbox').append(html);
	            }
	        });
		});
		$('#addpromo').click(function() {
			var category = $('#category').val();
			var vid = $('#vid').val();
			var model = $('#model').val();
			var perc = $('#percents').val();
			var name = $('#name').val();
			var cpr = perc*1;
			var err = "";
			var flag = false;
			if((cpr < 1) || (cpr > 100)){
				err += "Намалението трябва да бъде от 1% до 100%. ";
				flag = true;
			}
			if(name == ""){
				err += "Трябва да въведете име. ";
				flag = true;
			}
			if(category == "blank"){
				err += "Трябва да изберете категория. ";
				flag = true;
			}
			if(flag == false){
				$.ajax({
		            type: "POST",
		            url: "promo/ajax_add.php",
		            data: "cat="+category+"&vid="+vid+"&model="+model+"&perc="+perc+"&name="+name,
		            cache: false,
		            success: function(html){
		            	location.reload();
		            }
		        });
			}
			else {
				$('#errors').html("<span style='display: block; margin-top: 5px; font-size: 12px; color: red;'>"+err+"</span>");
			}
		});
		$('#canceladdpromo').click(function(){
			location.reload();
		});
		$('#canceleditpromo').click(function(){
			location.reload();
		});
    });
	function ShowAddPromo(){
        if ($("#addPromoDiv").is(":hidden")) {
            $("#addPromoDiv").fadeIn(300);
        } else {
            $("#addPromoDiv").hide(300);
        }
	}
	function hideModal(){
        $('.modal').hide();
	}
	function editPromo(id){
		$("#actionsDiv"+id).hide();
		
		var ID = "#respDiv"+id;
        if ($(ID).is(":hidden")) {
            if($(".respDIV").is(':visible')){
                $(".respDIV").hide();
            }
            $("#actionsDiv"+id).hide();
            $(ID).show();
            $("#editperc"+id).show();
			$(".promo_original_namalenie_"+id).hide();
        } else {
            $(ID).hide();
            $("#actionsDiv"+id).show();
            $("#editperc"+id).hide();
			$(".promo_original_namalenie_"+id).show();
        }
	}
	function updatePromo(id){
		var perc = $('#editperc'+id).val();
		$.ajax({
            type: "POST",
            url: "promo/ajax_update_promo.php",
            data: "id="+id+"&perc="+perc,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}
	function deletePromo(id){
		ime_na_kol = $(".ime_na_kol_" + id).text();
		$(".modal-body").html(ime_na_kol + ' <button class="button-cancel" onclick="hideModal();" style="margin-top: 100px; display: block; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px; margin-left: 10px; margin-right: 10px;">No</button> <button class="button-save" id="promo-modal-yes" onclick="deletePromoAjax(\'' + id + '\');" style="display: block; margin-top: 100px; text-align: center; line-height: 23px; font-size: 13px; float: right; padding-bottom: 0px;">Yes</button>');
		$(".modal").show();
	}
	function deletePromoAjax(id){
		$.ajax({
            type: "POST",
            url: "promo/ajax_delete_promo.php",
            data: "id="+id,
            cache: false,
            success: function(html){
            	location.reload();
            }
        });
	}
	function cancelupdatePromo(id){
		editPromo(id);
	}
</script>
<style>
	.addbutton {
		margin: 0px 10px 0px;
		display: block;
		width: 100px;
		font-size: 13px;
	}
	.respDIV{
		display: none;
	}
	#addPromoDiv {
		display: none;
		margin-left: 10px;
		margin-top: 40px;
	}
	#addPromoDiv table tr td {
		font-size: 13px;
	}
	#promos {
		margin-left: 10px;
		margin-top: 40px;
	}
	#promos div table tr td {
		font-size: 13px;
	}
	.actionsDiv {
		margin-top: 10px;
	}
	#addPromoDiv input, #addPromoDiv select{
		height: 20px;
	}
	.subMenu{
		width: 100%;
	    height: 13px;
	    margin-top: -11px;
	    position: relative;
	    top: -13px;
		padding-bottom: 10px;
    	padding-top: 0px;
    	box-shadow: 1px 0px 1px #ababab;border-bottom: 1px solid #767676;
    	background-color: #f2f2f2;
	}
	.subMenu a{
		color:#333;
		display:inline-block;
		margin:0 15px;
		font-size: 13px;
	}
	.subMenu a:hover, .subMenu a.active{
		color:#F7931E;
	}
</style>
<div class="subMenu">
    <a class="active" href="<?php echo url_admin; ?>moduls/promo.php">Групови промоции</a>
    <a href="<?php echo url_admin; ?>groups.php">Пакети</a>
</div>

<a class="addbutton nov" href="#" onclick="ShowAddPromo(); return false;">+ промоция</a>
<div id="addPromoDiv">
	<div id="alt"></div>
	<table>
		<tr>
			<td style="width:300px; height: 26px;">
				име *
			</td>
			<td>
				<input type="text" id="name" style="width: 300px; height: 26px;"/><br />
			</td>
			<td>
		</tr>
		<tr>
			<td style="height: 26px;">
				намаление (1 - 100%) *
			</td>
			<td style="padding-top: 5px;">
				<input type="text" id="percents" style="width: 300px; height: 26px;"/>
			</td>
		</tr>
		<tr>
			<td style="height: 26px;">
				категория *
			</td>
			<td style="padding-top: 5px; height: 26px;">
				<div id="categorybox">
					<select id="category" style="width: 300px; height: 26px; border: 1px solid #666666; font-family: 'Verdana',sans-serif; color: #333;">
						<option value="blank"></option>
						<?php
							$stm = $pdo->prepare('SELECT `id`, `ime_4c9a99e1a510a` FROM `kategorii` ORDER BY `id` ASC');
						    $stm -> execute();
						    foreach($stm->fetchAll() as $c){
						    	echo "<option value='".$c['id']."'>";
						    	echo $c['ime_4c9a99e1a510a'];
						    	echo "</option>";
						    }
						?>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td style="height: 26px;">
				вид
			</td>
			<td style="padding-top: 5px;">
				<div id="vidbox">
					<select id="vid" style="width: 300px; height: 26px; border: 1px solid #666666; font-family: 'Verdana',sans-serif; color: #333;">
						<option value="blank"></option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				марка
			</td>
			<td style="padding-top: 5px;">
				<div id="modelbox">
					<select id="model" style="width: 300px; height: 26px; border: 1px solid #666666; font-family: 'Verdana',sans-serif; color: #333;">
						<option value="blank"></option>
				    </select>
				</div>
			</td>
		</tr>
		<tr>
			<td style="width: 300px;"><div id="errors"></div></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 30px;">
				<button id="addpromo" class="button-save" style="line-height: 23px;">save</button>
				<button id="canceladdpromo" class="button-cancel" style="line-height: 23px;">cancel</button>
			</td>
		</tr>
	</table>
</div>
<div id="promos">
	<?php
		$stmpromos = $pdo->prepare('SELECT * FROM `promo`');
		$stmpromos -> execute();
		foreach($stmpromos->fetchAll() as $p){
			?>
			<div style="border: 1px solid #CCC; margin-top: 20px; width: 700px; height: 195px;">
				<table style="margin: 5px;">
					<tr>
						<td width="300px;" style="height: 26px;">
							име
						</td>
						<td width="300px;" class="ime_na_kol_<?php echo $p['id']?>">	
							<?php echo $p['ime'];?>
						</td>
					</tr>
					<tr>
						<td style="height: 26px;">
							намаление
						</td>
						<td>
							<div class="promo_original_namalenie_<?php echo $p['id']?>"><?php echo $p['perc'];?>%</div>
							<input type='text' id='editperc<?php echo $p['id']?>' style="display: none; width: 300px;" value="<?php echo $p['perc'];?>">
						</td>
					</tr>
					<tr>
						<td style="height: 26px;">
							категория
						</td>
						<td>
							<?php
								if($p['category_id'] != 0){
									$getcat = $pdo->prepare('SELECT * FROM `kategorii` WHERE `id` = :cat');
									$getcat -> bindValue(":cat", (int)$p['category_id'], PDO::PARAM_INT);
						    		$getcat -> execute();
						    		$c = $getcat->fetch();
									echo $c['ime_4c9a99e1a510a'];
								}
								else {
									echo "не е избрано";
								}
							?>
						</td>
					</tr>
					<tr>
						<td style="height: 26px;">
							вид
						</td>
						<td>
						<?php
							if($p['vid_id'] != 0){
								$getcat = $pdo->prepare('SELECT * FROM `vid` WHERE `id` = :vid');
								$getcat -> bindValue(":vid", (int)$p['vid_id'], PDO::PARAM_INT);
					    		$getcat -> execute();
					    		$c = $getcat->fetch();
								echo $c['ime_4c9a99e1a510a'];
							}
							else {
								echo "не е избрано";
							}
						?>
						</td>
					</tr>
					<tr>
						<td style="height: 26px;">
							марка
						</td>
						<td>
							<?php
								if($p['model_id'] != 0){
									$getcat = $pdo->prepare('SELECT * FROM `model` WHERE `id` = :model');
									$getcat -> bindValue(":model", (int)$p['model_id'], PDO::PARAM_INT);
						    		$getcat -> execute();
						    		$c = $getcat->fetch();
									echo $c['ime'];
								}
								else {
									echo "не е избрано";
								}
							?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="actionsDiv" id="actionsDiv<?php echo $p['id']?>">
								<a href="#" onclick="editPromo('<?php echo $p['id'];?>'); return false;" class="editPromo_<?php echo $p['id'] ?> button-save" style="display: block; text-align: center; line-height: 23px; font-size: 13px; float: left; padding-bottom: 0px;">edit</a>  
								<a href="#" class="deletePromo_<?php echo $p['id']; ?> button-cancel" onclick="deletePromo('<?php echo $p['id'];?>');" style="display: block; text-align: center; line-height: 23px; font-size: 13px; float: left; padding-bottom: 0px; margin-left: 10px;">delete</a>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="respDIV" id="respDiv<?php echo $p['id']?>">
								<a href="#" onclick="updatePromo('<?php echo $p['id']?>')" style="display: block; text-align: center; line-height: 23px; font-size: 13px; float: left; padding-bottom: 0px; margin-top: 8px;" class="button-save">save</a>
								<a href="#" onclick="cancelupdatePromo('<?php echo $p['id']?>')" style="display: block; text-align: center; line-height: 23px; font-size: 13px; float: left; padding-bottom: 0px; margin-left: 10px; margin-top: 8px;" class="button-cancel">cancel</a>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<?php
		}
	?>
</div>
<div style="clear:both;height: 60px;"></div>

		<div class="modal">

            <div class="modal-content">
                <div class="modal-top-lenta">
                    <div class="modal-top-lenta-text">Are you sure you want to delete this?</div>
                    <div class="modal-top-close" onclick="hideModal();">×</div>
                </div>

                <div class="modal-body">
                    
                </div>
            </div>
        </div>