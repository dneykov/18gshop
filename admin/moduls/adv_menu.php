<style type="text/css">
	.subMenu{
		width: 100%;
	    height: 13px;
	    margin-top: -11px;
	    position: relative;
	    top: -13px;
	    padding-bottom: 10px;
	    padding-top: 0px;
	    box-shadow: 1px 0px 1px #ababab;
	    border-bottom: 1px solid #767676;
	    background-color: #f2f2f2;
	}
	.subMenu a{
		color:#333;
		display:inline-block;
		margin:0 15px;
		font-size: 13px;
	}
	.subMenu a:hover, .subMenu a.active{
		color:#F7931E;
	}
</style>
<div class="subMenu">
    <a <?php if(!isset($_GET['banners']) && !isset($_GET['bannersthree']) && $_SERVER['SCRIPT_NAME'] != '/admin/moduls/header_links.php'){echo 'class="active"';}?> href="<?php echo url_admin; ?>moduls/adv.php">Интро</a>
	<a <?php if(isset($_GET['bannersthree'])){echo 'class="active"';}?> href="<?php echo url_admin; ?>moduls/adv.php?bannersthree" >Банери 4</a>
	<a <?php if(isset($_GET['banners'])){echo 'class="active"';}?> href="<?php echo url_admin; ?>moduls/adv.php?banners" >Банери продукти</a>
    <a <?php if($_SERVER['SCRIPT_NAME'] == '/admin/moduls/header_links.php'){echo 'class="active"';}?> href="<?php echo url."admin/moduls/header_links.php"?>">Хедър линк</a>
</div>
