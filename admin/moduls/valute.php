<?php
$page_current = 'orders';

require('template/__top.php');

$isDepositValid = true;

if(isset($_POST['update'])){
	$euro = $_POST['val'];
    $lei = $_POST['leiVal'];
	$deposit = $_POST['deposit'];
	$iban = $_POST['iban'];

	if($deposit < 1 || $deposit > 100) {
		$isDepositValid = false;
	}

	if($isDepositValid) {
		$stm = $pdo->prepare('UPDATE `dict` SET `value`=? WHERE `key`=?');
		$stm->bindValue(1, $euro, PDO::PARAM_STR);
		$stm->bindValue(2, "euro", PDO::PARAM_STR);
		$stm->execute();

        $stm = $pdo->prepare('UPDATE `dict` SET `value`=? WHERE `key`=?');
        $stm->bindValue(1, $lei, PDO::PARAM_STR);
        $stm->bindValue(2, "lei", PDO::PARAM_STR);
        $stm->execute();

		$stm = $pdo->prepare('UPDATE `dict` SET `value`=? WHERE `key`=?');
		$stm->bindValue(1, $deposit, PDO::PARAM_STR);
		$stm->bindValue(2, "deposit", PDO::PARAM_STR);
		$stm->execute();

        $stm = $pdo->prepare('UPDATE `dict` SET `value`=? WHERE `key`=?');
        $stm->bindValue(1, $iban, PDO::PARAM_STR);
        $stm->bindValue(2, "iban", PDO::PARAM_STR);
        $stm->execute();

		foreach ($__languages as $v) {
			if (isset ($_POST["preorder_" . $v->getPrefix()])) {
				$new_names[$v->getPrefix()] = $_POST["preorder_" . $v->getPrefix()];
			} else {
				$new_names[$v->getPrefix()] = '';
			}

            if (isset ($_POST["preorder_" . $v->getPrefix()])) {
                $bank_details[$v->getPrefix()] = $_POST["bank_details_" . $v->getPrefix()];
            } else {
                $bank_details[$v->getPrefix()] = '';
            }

			$stm = $pdo->prepare('UPDATE `dict` SET `value` = ? WHERE `key` = ?');
			$stm->bindValue(1, stripslashes($new_names[$v->getPrefix()]));
			$stm->bindValue(2, "preorder_" . $v->getPrefix());
			$stm->execute();

            $stm = $pdo->prepare('UPDATE `dict` SET `value` = ? WHERE `key` = ?');
            $stm->bindValue(1, stripslashes($bank_details[$v->getPrefix()]));
            $stm->bindValue(2, "bank_details_" . $v->getPrefix());
            $stm->execute();
		}
	}
}
?>
<style type="text/css">
	table td{
		font-size: 13px;
    	color: #333;
    	font-family: 'Verdana', sans-serif;
	}
	.subMenu{
		width: 100%;
        height: 13px;
        margin-top: -11px;
        position: relative;
        top: -13px;
        padding-bottom: 10px;
        padding-top: 0px;
        box-shadow: 1px 0px 1px #ababab;
        border-bottom: 1px solid #767676;
        background-color: #f2f2f2;
	}
	.subMenu a{
		color:#F7931E;
		font-size: 13px;
		margin:0 15px;
	}
	.productLink:hover{
		text-decoration:none;
	}
	.orders{
		border-collapse: collapse;
	}
	#wrapper {
		padding: 10px;
	}
	hr {
		margin: 20px 0	;
	}
    input[type=text] {
        width: 180px;
    }
    textarea{
    	font-size: 13px;
    	color: #333;
    	font-family: 'Verdana', sans-serif;
    }
</style>
<script type="text/javascript">
	$( document ).ready(function() {
		$("#deposit").keydown(function(e){
			var keyPressed;
			if (!e) var e = window.event;
			if (e.keyCode) keyPressed = e.keyCode;
			else if (e.which) keyPressed = e.which;
			var hasDecimalPoint = (($(this).val().split('.').length-1)>0);
			if ( keyPressed == 46 || keyPressed == 8 ||((keyPressed == 190||keyPressed == 110)&&(!hasDecimalPoint)) || keyPressed == 9 || keyPressed == 27 || keyPressed == 13 ||
				// Allow: Ctrl+A
				(keyPressed == 65 && e.ctrlKey === true) ||
				// Allow: home, end, left, right
				(keyPressed >= 35 && keyPressed <= 39)) {
				// let it happen, don't do anything
				return;
			}
			else {
				// Ensure that it is a number and stop the keypress
				if (e.shiftKey || (keyPressed < 48 || keyPressed > 57) && (keyPressed < 96 || keyPressed > 105 )) {
					e.preventDefault();
				}
			}

		});
	});
</script>
<div class="subMenu">
    <a href="<?php echo url_admin; ?>orders.php" <?php if((isset($_GET['approved']))||(isset($_GET['done']))||(isset($_GET['cancel']))||(isset($_GET['return']))) echo 'style="color: #333;"' ?>>Нови <?php echo order_admin::getCountForWaiting(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?approved" <?php if(!isset($_GET['approved'])) echo 'style="color: #333;"' ?>>Одобрени <?php echo order_admin::getCountForApproved(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?done" <?php if(!isset($_GET['done'])) echo 'style="color: #333;"' ?>>Изпълнени <?php echo order_admin::getCountForDone(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?cancel" <?php if(!isset($_GET['cancel'])) echo 'style="color: #333;"' ?>>Отказани <?php echo order_admin::getCountForCanceled(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?return" <?php if(!isset($_GET['return'])) echo 'style="color: #333;"' ?>>Върнати <?php echo order_admin::getCountForReturned(); ?></a>
    <a href="<?php echo url_admin; ?>orders.php?statistik" style="color: #333;">Отчет</a>
    <a href="<?php echo url_admin; ?>orders.php?delivery" style="color: #333;">Доставка</a>
    <a href="<?php echo url_admin; ?>orders.php?valute">Настройки</a>
</div>
<?php

	$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "euro"');
	$stm -> execute();
	$v=$stm->fetch();
	$euro=$v['value'];

    $stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "lei"');
    $stm -> execute();
    $v=$stm->fetch();
    $lei=$v['value'];

    $stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "deposit"');
    $stm -> execute();
    $v=$stm->fetch();
    $deposit=$v['value'];

    $stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "iban"');
    $stm -> execute();
    $v=$stm->fetch();
    $iban=$v['value'];

	$preorderDescription = array();
	foreach ($__languages as $v) {
		$stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "preorder_' . $v->getPrefix() . '"');
		$stm -> execute();
		$result=$stm->fetch();
		$preorderDescription[$v->getPrefix()] = $result['value'];
	}

    $bankDetails = array();
    foreach ($__languages as $v) {
        $stm = $pdo->prepare('SELECT `value` FROM `dict` where `key` = "bank_details_' . $v->getPrefix() . '"');
        $stm -> execute();
        $result=$stm->fetch();
        $bankDetails[$v->getPrefix()] = $result['value'];
    }
?>
<div id="wrapper">
	<form action="" method="post">
		<?php if(isset($_POST['update']) && !$isDepositValid) : ?>
			<p style="color: #f00;font-size: 13px; margin-bottom: 5px;">Аванса трябва да бъде между 1 и 100%.</p>
		<?php endif; ?>
		<p style="font-size: 13px; margin-bottom: 5px;"><strong>Валутен курс</strong></p>
		<span style="width: 60px; display: inline-block; font-size: 13px; margin-bottom: 10px;">1 euro </span><input type="text" name="val" value="<?php echo $euro; ?>"> <span style="font-size: 13px;">лв.</span>
        <br>
        <span style="width: 60px; display: inline-block; font-size: 13px;">1 lei </span><input type="text" name="leiVal" value="<?php echo $lei; ?>"> лв.
		<hr />
		<p style="font-size: 13px; margin-bottom: 5px;"><strong>Pre-order</strong></p>
		<span style="display: inline-block; width: 60px; font-size: 13px;">Аванс </span><input type="text" name="deposit" id="deposit" value="<?php echo $deposit; ?>"> <span style="font-size: 13px;">%</span>
		<br/>
		<br/>
		<?php foreach ($__languages as $key => $v)  { ?>
			<div style="clear:both; height: 10px; font-size: 13px;"></div>
			<label for="" style="font-size: 13px;">Pre-order информация <?php echo $v->getName(); ?></label><br/>
			<textarea rows="5" cols="45" id="pre-order" name="preorder_<?php echo $v->getPrefix(); ?>" class="pre-order"><?php if(isset($_POST['preorder_'.$v->getPrefix().''])) echo $_POST['preorder_'.$v->getPrefix().'']; else echo $preorderDescription[$v->getPrefix()]; ?></textarea>
		<?php } ?>
        <hr />
        <p style="font-size: 13px; margin-bottom: 5px; font-size: 13px;"><strong>Банкови детайли</strong></p>
        <span style="display: inline-block; font-size: 13px; width: 60px;">IBAN </span><input type="text" name="iban" id="iban" value="<?php echo $iban; ?>">
        <br/>
        <br/>
        <?php foreach ($__languages as $key => $v)  { ?>
            <div style="clear:both; height: 10px"></div>
            <label for="" style="font-size: 13px;">Детайли <?php echo $v->getName(); ?></label><br/>
            <textarea rows="5" cols="45" id="bank-details" name="bank_details_<?php echo $v->getPrefix(); ?>" class="bank-details"><?php if(isset($_POST['bank_details_'.$v->getPrefix().''])) echo $_POST['bank_details_'.$v->getPrefix().'']; else echo $bankDetails[$v->getPrefix()]; ?></textarea>
        <?php } ?>
		<br/>
		<button style="margin: 20px 0 0 0; display: inline-block;" type="submit" class="button-save" name="update">save</button>
		<button style="margin: 20px 0 0 0; display: inline-block;" type="button" class="button-cancel" onclick="location.reload();" name="update">cancel</button>
		<div style="height: 45px;"></div>
	</form>
</div>