<?php
/*
* Copyright (c) 2008 http://www.webmotionuk.com / http://www.webmotionuk.co.uk
* "PHP & Jquery image upload & crop"
* Date: 2008-11-21
* Ver 1.2
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
* IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
* THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
require '../../__top.php';
error_reporting (E_ALL ^ E_NOTICE);
//session_start(); //Do not remove this

if(isset($_GET["id"])){
	$_SESSION['atikul_id']=$_GET["id"];
}
if(isset($_GET["crop_html_img"])){
	$_SESSION['crop_html_img']=$_GET["crop_html_img"];
}
if(isset($_GET["crop_html_img_b"])){
	$_SESSION['crop_html_img_b']=$_GET["crop_html_img_b"];
}
if(isset($_GET["crop_html_a"])){
	$_SESSION['crop_html_a']=$_GET["crop_html_a"];
}

if(isset($_GET["crop_html_img_input_hidden"])){
	$_SESSION['crop_html_img_input_hidden']=$_GET["crop_html_img_input_hidden"];
}

if(isset($_GET["crop_large_image_location"])){
	$_SESSION['crop_large_image_location']=$_GET["crop_large_image_location"];
}

if(isset($_GET["crop_thumb_image_location"])){
	$_SESSION['crop_thumb_image_location']=$_GET["crop_thumb_image_location"];
}
if(isset($_GET["crop_width"])){
	$_SESSION['crop_width']=$_GET["crop_width"];
}
if(isset($_GET["crop_height"])){
	$_SESSION['crop_height']=$_GET["crop_height"];
}

//only assign a new timestamp if the session variable is empty
if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0){
    $_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s')); //assign the timestamp to the session variable
	$_SESSION['user_file_ext']= "";
}
#########################################################################################################
# CONSTANTS																								#
# You can alter the options below																		#
#########################################################################################################
if(isset($_SESSION['crop_upload_dir'])){
	$upload_dir = $_SESSION['crop_upload_dir'];
}else{
	$upload_dir = "../../../images/banners"; 				// The directory for the images to be saved in
}

$upload_path = $upload_dir."/";				// The path to where the image will be saved
$large_image_prefix = "resize_"; 			// The prefix name to large image
$thumb_image_prefix = "thumbnail_";			// The prefix name to the thumb image
$large_image_name = $large_image_prefix.$_SESSION['random_key'];     // New name of the large image (append the timestamp to the filename)
$thumb_image_name = $thumb_image_prefix.$_SESSION['random_key'];     // New name of the thumbnail image (append the timestamp to the filename)
$max_file = "256"; 							// Maximum file size in MB
$max_width = "1280";							// Max width allowed for the large image
if(isset($_SESSION['crop_width'])){
	$thumb_width = $_SESSION['crop_width'];
}else{
	$thumb_width = "750";						// Width of thumbnail image
}
if(isset($_SESSION['crop_height'])){
	$thumb_height = $_SESSION['crop_height'];
}else{
	$thumb_height = "400";						// Height of thumbnail image
}

// Only one of these image types should be allowed for upload
$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
$allowed_image_ext = array_unique($allowed_image_types); // do not change this
$image_ext = "";	// initialise variable, do not change this.
foreach ($allowed_image_ext as $mime_type => $ext) {
    $image_ext.= strtoupper($ext)." ";
}


##########################################################################################################
# IMAGE FUNCTIONS																						 #
# You do not need to alter these functions																 #
##########################################################################################################
function resizeImage($image,$width,$height,$scale,$newWidth=-1,$newHeight=-1) {
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	if($newWidth != -1){
		$newImageWidth = $newWidth;
	}else{
		$newImageWidth = round($width * $scale);
	}
	if($newHeight != -1){
		$newImageHeight = $newHeight;
	}else{
		$newImageHeight = round($height * $scale);
	}
	//$newImageWidth = ceil($width * $scale);
	//$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	imagecolorallocatealpha($newImage,255,0,0,60);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
	
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$image); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$image,100); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$image);  
			break;
    }
	
	chmod($image, 0777);
	return $image;
}
//You do not need to alter these functions
function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale,$newWidth=-1,$newHeight=-1){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	
	if($newWidth != -1){
		$newImageWidth = $newWidth;
	}else{
		$newImageWidth = round($width * $scale);
	}
	if($newHeight != -1){
		$newImageHeight = $newHeight;
	}else{
		$newImageHeight = round($height * $scale);
	}
	
	//$newImageWidth = ceil($width * $scale);
	//$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	imagecolorallocatealpha($newImage,255,0,0,60);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,100); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}
//You do not need to alter these functions
function getHeight($image) {
	$size = getimagesize($image);
	$height = $size[1];
	return $height;
}
//You do not need to alter these functions
function getWidth($image) {
	$size = getimagesize($image);
	$width = $size[0];
	return $width;
}

//Image Locations
if(isset($_SESSION['crop_large_image_location'])){
	$large_image_location = $_SESSION['crop_large_image_location'];
	//unset($_SESSION['crop_large_image_location']);
	$path_parts = pathinfo($large_image_location);
	$large_image_name=$path_parts['filename'];
	
	$_SESSION['user_file_ext']=".".$path_parts['extension'];
}else{
	$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
}
/*
var_dump($_SESSION['crop_large_image_location']);
var_dump($large_image_location);*/

if(isset($_SESSION['crop_thumb_image_location'])){
	$thumb_image_location = $_SESSION['crop_thumb_image_location'];
	$path_parts = pathinfo($_SESSION['crop_thumb_image_location']);
	$thumb_image_name=$path_parts['filename'];

	$_SESSION['user_file_ext']=".".$path_parts['extension'];
}else{
	$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION['user_file_ext'];
}

if((!file_exists($large_image_location))&&isset($_SESSION['old_image'])){	
	/*$artikul= new artikul((int)$_SESSION['atikul_id']);
	$large_image_location="../../../".str_replace("thumbnail","resize",$artikul->getKatinka());*/
	$large_image_location="../../../".$_SESSION['old_image'];
	$path_parts = pathinfo($large_image_location);
	$large_image_name=$path_parts['filename'];
	
	$_SESSION['user_file_ext']=".".$path_parts['extension'];
	
}

//Create the upload directory with the right permissions if it doesn't exist
if(!is_dir($upload_dir)){
	mkdir($upload_dir, 0777);
	chmod($upload_dir, 0777);
}

//Check to see if any images with the same name already exist
if (file_exists($large_image_location)){
	if(file_exists($thumb_image_location)){
		$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION['user_file_ext']."\" alt=\"Thumbnail Image\"/>";
	}else{
		$thumb_photo_exists = "";
	}
   	$large_photo_exists = "<img src=\"".$large_image_location."\" alt=\"Large Image\"/>";
} else {
   	$large_photo_exists = "";
	$thumb_photo_exists = "";
}

if (isset($_POST["upload"])) { 
	/*if(isset($large_image_name)){
		$large_image_location = $upload_path.$large_image_name;
	}else{
		
	}*/
	$large_image_name=$large_image_prefix.$_SESSION['random_key'];
	$large_image_location = $upload_path.$large_image_name;
	if (file_exists($large_image_location.$_SESSION['user_file_ext'])) {
		unlink($large_image_location.$_SESSION['user_file_ext']);
		//echo $large_image_location.$_SESSION['user_file_ext'];
	}

	//Get the file information
	$userfile_name = $_FILES['image']['name'];
	$userfile_tmp = $_FILES['image']['tmp_name'];
	$userfile_size = $_FILES['image']['size'];
	$userfile_type = $_FILES['image']['type'];
	$filename = basename($_FILES['image']['name']);
	$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
	
	//Only process if the file is a JPG, PNG or GIF and below the allowed limit
	if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
		
		foreach ($allowed_image_types as $mime_type => $ext) {
			//loop through the specified image types and if they match the extension then break out
			//everything is ok so go and check file size
			if($file_ext==$ext && $userfile_type==$mime_type){
				$error2 = "";
				break;
			}else{
				$error2 = "Only <strong>".$image_ext."</strong> images accepted for upload<br />";
			}
		}
		//check if the file size is above the allowed limit
		if ($userfile_size > ($max_file*1048576)) {
			$error2.= "Images must be under ".$max_file."MB in size";
		}
		
	}else{
		$error2= "Select an image for upload";
	}
	//Everything is ok, so we can upload the image.
	if (strlen($error2)==0){
		
		if (isset($_FILES['image']['name'])){
			//this file could now has an unknown file extension (we hope it's one of the ones set above!)
			$org_image_location = $large_image_location."_org.".$file_ext;
			$large_image_location = $large_image_location.".".$file_ext;
			$thumb_image_location = $thumb_image_location.".".$file_ext;
			
			
			//put the file ext in the session so we know what file to look for once its uploaded
			$_SESSION['user_file_ext']=".".$file_ext;
			
			move_uploaded_file($userfile_tmp, $large_image_location);
			chmod($large_image_location, 0777);
			
			copy($large_image_location, $org_image_location);
			chmod($org_image_location	, 0777);
			$_SESSION['org_image_location']=$org_image_location;
			
			$width = getWidth($large_image_location);
			$height = getHeight($large_image_location);
			//Scale the image if it is greater than the width set above
			$uploaded=$large_image_location;
			if ($width > $max_width){
					$image = new Imagick($_SESSION['org_image_location']);
					// $image->setImageColorSpace(Imagick::COLORSPACE_RGB); 
					$image->thumbnailImage($max_width, 0);
					$uploaded=$large_image_location;
					$image->writeImage($uploaded);
			
			}
			$_SESSION['crop_large_image_location']=$uploaded;
			//Delete the thumbnail file so the user can create a new one
			if (file_exists($thumb_image_location)) {
				unlink($thumb_image_location);
			}

            if (file_exists($org_image_location)) {
                unlink($org_image_location);
            }
		}
		//Refresh the page to show the new uploaded image
		header("location:".$_SERVER["PHP_SELF"]);
		exit();
	}
}

if (isset($_POST["upload_thumbnail"]) && strlen($large_photo_exists)>0) {
	//Get the new coordinates to crop the image.
	$x1 = $_POST["x1"];
	$y1 = $_POST["y1"];
	$x2 = $_POST["x2"];
	$y2 = $_POST["y2"];
	$w = $_POST["w"];
	$h = $_POST["h"];
	
	
	
	//if(!(empty($x1)||empty($x2)||empty($y1)||empty($y2)||empty($w)||empty($h))){
	if(!(empty($x2)||empty($y2)||empty($w)||empty($h))){
		if($large_image_location!=str_replace($thumb_image_prefix,$large_image_prefix,$thumb_image_location)){
			if (copy($large_image_location,str_replace($thumb_image_prefix,$large_image_prefix,$thumb_image_location))) {
				//unlink($large_image_location);
				//var_dump(str_replace($thumb_image_prefix,$large_image_prefix,$thumb_image_location));
				
			}
		}	
		
		$_SESSION['org_image_location']= preg_replace('|^(.+)\.(.+)$|is', '$1_org.$2', $large_image_location );
		if(!file_exists( $_SESSION['org_image_location'] )){
			 $_SESSION['org_image_location']=$large_image_location;
		}
		
		$image = new Imagick($large_image_location);
		// $image->setImageColorSpace(Imagick::COLORSPACE_RGB); 
		$image->cropImage($w,$h,$x1,$y1);
		$image->thumbnailImage($thumb_width,$thumb_height);
		$cropped_b=substr($thumb_image_location,0,-4).'_b.jpg';
		$image->writeImage($cropped_b);
		
		$image = new Imagick($cropped_b);
		// $image->setImageColorSpace(Imagick::COLORSPACE_RGB); 
		$image->thumbnailImage(480, 0);
		$cropped=$thumb_image_location;
		$image->writeImage($cropped);
		
		$image = new Imagick($cropped_b);
		// $image->setImageColorSpace(Imagick::COLORSPACE_RGB); 
		$image->thumbnailImage(480, 0);
		$cropped_t=substr($thumb_image_location,0,-4).'_t.jpg';
		$image->writeImage($cropped_t);
		
		$image = new Imagick($cropped_b);
		// $image->setImageColorSpace(Imagick::COLORSPACE_RGB); 	
		$image->thumbnailImage(160, 0);
		$cropped_tt=substr($thumb_image_location,0,-4).'_tt.jpg';
		$image->writeImage($cropped_tt);
		
		//Scale the image to the thumb_width set above
				//$scale = $thumb_width/$w;
		//echo $large_image_location;
				//$cropped_b = resizeThumbnailImage(substr($thumb_image_location,0,-4).'_b.jpg',  $large_image_location,$w,$h,$x1,$y1,$scale,$thumb_width,$thumb_height);
				//$thumb_image_location_size=getimagesize(substr($thumb_image_location,0,-4).'_b.jpg');
				//$cropped = resizeThumbnailImage($thumb_image_location,  substr($thumb_image_location,0,-4).'_b.jpg',$thumb_width ,$thumb_height,0,0,640/($thumb_image_location_size[0]),640);
		
		
		
		
		
				//$cropped_t = resizeThumbnailImage(substr($thumb_image_location,0,-4).'_t.jpg',substr($thumb_image_location,0,-4).'_b.jpg',$thumb_width ,$thumb_height,0,0,320/($thumb_image_location_size[0]),320);
				//$cropped_tt = resizeThumbnailImage(substr($thumb_image_location,0,-4).'_tt.jpg',substr($thumb_image_location,0,-4).'_b.jpg',$thumb_width ,$thumb_height,0,0,160/($thumb_image_location_size[0]),160);
		//$cropped = resizeThumbnailImage($thumb_image_location."_t", $large_image_location,$w,$h,$x1,$y1,$scale);
		//$cropped = resizeThumbnailImage($thumb_image_location."_b", $large_image_location,$w,$h,$x1,$y1,$scale);
		//Reload the page again to view the thumbnail
	/*}else{
		$thumb_image_location_size=getimagesize($large_image_location);
		$cropped2 = resizeThumbnailImage(substr($large_image_location,0,-4).'_t.jpg', $large_image_location,$thumb_width ,$thumb_height,0,0,360/($thumb_image_location_size[0]));
		$cropped=$large_image_location;
	}*/
	$cropped_b=substr($cropped_b ,9);
	$cropped_t=substr($cropped_t ,9);
	$cropped=substr($cropped,9);
	$cropped=$cropped;
	/*if(isset($_SESSION['atikul_id'])){
		$stm = $pdo->prepare('UPDATE `artikuli` SET `kartinka` = ? WHERE `id` = ? LIMIT 1');
		$stm -> bindValue(1, $cropped, PDO::PARAM_STR);
		$stm -> bindValue(2,  $_SESSION['atikul_id'], PDO::PARAM_INT);
		$stm -> execute();		
	}	*/
	//session_destroy();
	//session_start();
	
	$_SESSION['crop_image']=$cropped;
	//echo $cropped;
		?>
			<script type="text/javascript">
				
				window.parent.$("<?php if(isset($_SESSION['crop_html_img'])){echo '#'.$_SESSION['crop_html_img'];}else{ echo '#product_main_image img';} ?>").attr("src","<?php echo url.$cropped.'?'.rand();?>");
				window.parent.$("<?php if(isset($_SESSION['crop_html_img_b'])){echo '#'.$_SESSION['crop_html_img_b'];}else{ echo '#product_main_image img';} ?>").attr("src","<?php echo url.$cropped_b.'?'.rand();?>");
				window.parent.$("<?php if(isset($_SESSION['crop_html_a'])){echo '#'.$_SESSION['crop_html_a'];}else{ echo '#product_main_image';} ?>").attr("href","<?php echo url.$cropped_b.'?'.rand();?>");
				window.parent.$("<?php if(isset($_SESSION['crop_html_img_input_hidden'])){echo '#'.$_SESSION['crop_html_img_input_hidden'];}else{ echo '#product_main_image';} ?>").siblings("input[type='hidden']").attr("value","<?php echo $cropped;?>");
				self.parent.tb_remove();
			</script>
		<?php
	//header("location:".$_SESSION["callBackUrl"]);
	
	//header("location:".$_SERVER["PHP_SELF"]);
	unset($_SESSION['crop_thumb_image_location']);
	unset($_SESSION['crop_large_image_location']);
	unset($_SESSION['crop_html_img']);
	unset($_SESSION['atikul_id']);
	unset($_SESSION['user_file_ext']);
	unset($_SESSION['random_key']);

	$_SESSION['crop_large_image_location']=str_replace($thumb_image_prefix,$large_image_prefix,$thumb_image_location);

	exit();
	}
}

/*
if ($_GET['a']=="delete" && strlen($_GET['t'])>0){
//get the file locations 
	$large_image_location = $upload_path.$large_image_prefix.$_GET['t'];
	$thumb_image_location = $upload_path.$thumb_image_prefix.$_GET['t'];
	if (file_exists($large_image_location)) {
		unlink($large_image_location);
	}
	if (file_exists($thumb_image_location)) {
		unlink($thumb_image_location);
	}
	header("location:".$_SERVER["PHP_SELF"]);
	exit(); 
}
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="generator" content="WebMotionUK" />
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/imgareaselect-default.css" />
	
	<script type="text/javascript" src="<?php echo url; ?>js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.imgareaselect.js"></script>
	
</head>
<body>
<style type="text/css">

            body{
                
                color: #FFF;
                  font-family: Verdana, sans-serif;
                  font-size: 12pt;
                  width: 1024px;
                  padding: 0;
                  margin: 0;
					text-align: center ;

			}
</style>
<div style="text-align: left ;margin:0px;">
<?php
//Only display the javacript if an image has been uploaded
if(strlen($large_photo_exists)>0){
	$current_large_image_width = getWidth($large_image_location);
	$current_large_image_height = getHeight($large_image_location);?>
<script type="text/javascript">
function preview(img, selection) { 
	var scaleX = <?php echo $thumb_width;?> / selection.width; 
	var scaleY = <?php echo $thumb_height;?> / selection.height; 
	
	$('#thumbnail + div > img').css({ 
		width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
		height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});
	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
} 

$(document).ready(function () { 
	$('#save_thumb').click(function() {
		var x1 = $('#x1').val();
		var y1 = $('#y1').val();
		var x2 = $('#x2').val();
		var y2 = $('#y2').val();
		var w = $('#w').val();
		var h = $('#h').val();
		if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
			alert("Първо трябва да селектирате зона за изрязване");
			return false;
		}else{
			return true;
		}
	});
}); 


$(window).load(function () { 
	window.ias=$('#thumbnail').imgAreaSelect({ instance: true,aspectRatio: '<?php echo $thumb_width;?>:<?php echo $thumb_height;?>', onSelectChange: preview, minWidth:<?php echo $thumb_width;?>, minHeight:<?php echo $thumb_height;?> }); 
	$('#isbig').change(function() {
		if($('#isbig').is(':checked')){
			window.ias.setOptions({  minWidth:<?php echo $thumb_width;?>, minHeight:<?php echo $thumb_height;?> });
			window.ias.update();			
		}else{
			window.ias.setOptions({  minWidth:1, minHeight: 1 });
			window.ias.update();
		}
	});
});

</script>
<?php }?>
<?php
//Display error message if there are any
//if(strlen($error2)>0){
//	echo "<ul><li><strong>Error!</strong></li><li>".$error2."</li></ul>";
//}
/*if(strlen($large_photo_exists)>0 && strlen($thumb_photo_exists)>0){
	echo $large_photo_exists."&nbsp;".$thumb_photo_exists;
	echo "<p><a href=\"".$_SERVER["PHP_SELF"]."?a=delete&t=".$_SESSION['random_key'].$_SESSION['user_file_ext']."\">Delete images</a></p>";
	echo "<p><a href=\"".$_SERVER["PHP_SELF"]."\">Upload another</a></p>";
	//Clear the time stamp session and user file extension
	$_SESSION['random_key']= "";
	$_SESSION['user_file_ext']= "";
}else{*/
	?>
	
	<script type="text/javascript"> 		
		$(function(){
			$("#fil1").change(function(){
				$("#form1").submit();
			});
		});
	</script>
	<form id="form1" name="photo" enctype="multipart/form-data" action="<?php echo $_SERVER["PHP_SELF"]; if(isset($_GET["id"])) echo '?id='.$_GET["id"]; ?>" method="post" style="height:40px;background-color: #f2f2f2;color:#333;color: #333; margin-bottom:3px;padding:10px 10px 0;top:0px;position: fixed;width:100%;z-index: 5000;" >
		<span style="color:#333;">Избери снимка.</span>	
		<input type="file" name="image" size="30" id="fil1"/><input type="hidden" name="upload" /> 
		<?php
					if(isset($_GET['id'])){
						echo '<input type="hidden" name="id" value="'.$_GET['id'].'"/>';
					}else if(isset($_POST['id'])){
						echo '<input type="hidden" name="id" value="'.$_POST['id'].'"/>';
					}
				?>		
	</form>
	

	<?php
		if(strlen($large_photo_exists)>0){?>
		<form  style="position: fixed;width:100%;top: 10px;left: 660px;z-index: 5000;" name="thumbnail" action="<?php echo $_SERVER["PHP_SELF"]; if(isset($_GET["id"])) echo '?id='.$_GET["id"]; ?>" method="post">
				<input type="hidden" name="x1" value="" id="x1" />
				<input type="hidden" name="y1" value="" id="y1" />
				<input type="hidden" name="x2" value="" id="x2" />
				<input type="hidden" name="y2" value="" id="y2" />
				<input type="hidden" name="w" value="" id="w" />
				<input type="hidden" name="h" value="" id="h" />
				<?php
					if(isset($_GET['id'])){
						echo '<input type="hidden" name="id" value="'.$_GET['id'].'"/>';
					}else if(isset($_POST['id'])){
						echo '<input type="hidden" name="id" value="'.$_POST['id'].'"/>';
					}
				?>
				<input type="submit" style="width: 100px; height: 25px; background-color: #666666; padding-bottom: 3px;color: #fff; border: none;cursor: pointer;"name="upload_thumbnail" value="save" id="save_thumb" />
				<input style="margin-left:10px;" type="button" value="cancel" style="width: 100px; height: 25px; background-color: #666666; padding-bottom: 3px;color: #fff; border: none;cursor: pointer;" id="save_thumb" onclick="self.parent.tb_remove();" />
				<span style="margin-left:45px;"><input id="isbig" type="checkbox" name="isbig" checked="yes"  /> снимката е по-голяма от <?php echo $thumb_width;?> х <?php echo $thumb_height;?> пиксела </span>
		</form>
			
		<div align="center" style="background-color: #fff;margin-top: 50px;">
			<img src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'].'?'.rand();;?>" style="float: left;" id="thumbnail" alt="Create Thumbnail" />
			
			<!--<div style="margin-top:40px;border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:<?php echo $thumb_width;?>px; height:<?php echo $thumb_height;?>px;">
				<img src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext']?>" style="position: relative;" alt="Thumbnail Preview" />
			</div> -->
			
			<br style="clear:both;"/>
			
		</div>
	
	<?php 	} 
/*}*/ ?>
<!-- Copyright (c) 2008 http://www.webmotionuk.com -->

</div>

</body>
</html>
