<?php

/* @var $pdo pdo */
/* @var $stm PDOStatement */

require '__top.php';

try{

$stm = $pdo -> prepare('SELECT * FROM `kategorii` ORDER BY `ordr` ');
$stm -> execute();

if($stm->rowCount() == 0 ) $kategorii = NULL;
else{

    foreach ($stm->fetchAll() as $v) {
        $temp_kategoriq = new kategoriq($v);
        $kategoriq_fe = $temp_kategoriq;


        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id_kategoriq` = ? ORDER BY RAND() LIMIT 4');
        $stm -> bindValue(1, $temp_kategoriq->getId(), PDO::PARAM_INT);
        $stm -> execute();

        if($stm -> rowCount()) foreach ($stm -> fetchAll() as $artikuli_kartiki_fe) {
            $artikuli_kartiki = new artikul($artikuli_kartiki_fe);
            $kategoriq_kartinki[$temp_kategoriq->getId()][] = url.$artikuli_kartiki->getKartinka_t();

        }


    $stm = $pdo->prepare('SELECT * FROM `vid` WHERE `id_kategoriq` = ? ');
    $stm -> bindValue(1, $v['id'], PDO::PARAM_INT);
    $stm -> execute();

    if($stm->rowCount() == 0 ) $vidove_fe = NULL;

    else
    {
        foreach ($stm->fetchAll() as $g)
            {

                $vidove_fe[] = new vid($g);


   
            } unset($g); // foreach ($stm->fetchAll()

            
    }

    $kategorii[] = Array('kategoriq' => new kategoriq($v), 'videove' => $vidove_fe);


    unset($vidove_fe);
  } unset($v); // kategorii foreach




} // if($stm->rowCount() == 0 ) echo 'Нямаме категории';




}
catch (Exception $e){
    greshka($e);
}

require 'template/index.php';

?>
