<?php
require '__top.php';


if (!isset ($_REQUEST['category'])) greshka('kategoriq neposo4ena', 'Не е посочена категория');
// проверка има ли я категорията

$category = new kategoriq((int)$_REQUEST['category']);

if (isset ($_GET['new_vid_name'])) {

    $names = $_GET['new_vid_name'];


    if (count($names) != (count($__languages))) greshka('вид имена масив не е голям колкото броя езици');


    if (isset($_GET['virtual'])) {
        $sql = 'INSERT INTO `vid` (`id_kategoriq`, `is_virtual` ';
    } else {
        $sql = 'INSERT INTO `vid` (`id_kategoriq` ';
    }

    if ($__languages) foreach ($__languages as $v) {
        $sql .= ' , `ime_' . $v->getPrefix() . '` ';
    }


    if (isset($_GET['virtual'])) {
        $sql .= ' ) VALUES( ? , ? ';
    } else {
        $sql .= ' ) VALUES( ? ';
    }

    if ($__languages) foreach ($__languages as $v) {
        $sql .= ' , ? ';
    }

    $sql .= ' )';


    $stm = $pdo->prepare($sql);

    $stm->bindValue(1, $category->getId(), PDO::PARAM_INT);
    if (isset($_GET['virtual'])) {
        $stm->bindValue(2, $_GET['virtual'], PDO::PARAM_INT);
    }


    foreach ($names as $k => $v) {
        if (isset($_GET['virtual'])) {
            $stm->bindValue($k + 3, $names[$k], PDO::PARAM_STR);
        } else {
            $stm->bindValue($k + 2, $names[$k], PDO::PARAM_STR);
        }
    }


    $stm->execute();

    exit;

}


?>


<div class="input-tab-holder" style="margin-top: 10px;">
    <div class="input-tab-title">вид</div>
    <?php
    $inputs = "";
    if ($__languages) foreach (array_reverse($__languages) as $v) {
        echo '<div class="input-tab-trigger-' . $v->getName() . '-vid">' . $v->getName() . '</div>';
        $inputs = $inputs . '<input class="new_vid_name input-tab-' . $v->getName() . '-vid" type="text">';
    }
    ?>
</div>
<?php echo $inputs; ?>
<br>
<input type="checkbox" id="virtual" name="virtual"> <label for="virtual" style="font-size: 13px; color: #333;">виртуален вид</label>
<br>
<div class="error">Моля въведете вид</div>

<button type="button" name="category_edit_submit" style="margin-top: 10px;" onclick="new_vid_send()"
        class="button-save"><?php echo lang_save; ?></button>
<button class="button-cancel" id="cancel-vid" style="margin-left: 5px;"
        onclick="new_vid_cancel()"><?php echo lang_cancel ?></button>

<script>
    $(document).ready(function () {
        $(".input-tab-bg-vid").keyup(function () {
            var val_bg = $('.input-tab-bg-vid').val();
            $('.input-tab-en-vid').val(val_bg);
            $('.input-tab-ro-vid').val(val_bg);
        });

        $(".input-tab-trigger-ro-vid").click(function () {
            $(".input-tab-bg-vid").hide();
            $(".input-tab-en-vid").hide();
            $(".input-tab-ro-vid").show();
            $(".input-tab-trigger-ro-vid").css("background-color", "#666666");
            $(".input-tab-trigger-ro-vid").css("color", "#fff");
            $(".input-tab-trigger-en-vid").css("background-color", "#fff");
            $(".input-tab-trigger-en-vid").css("color", "#333");
            $(".input-tab-trigger-bg-vid").css("background-color", "#fff");
            $(".input-tab-trigger-bg-vid").css("color", "#333");
        });

        $(".input-tab-trigger-en-vid").click(function () {
            $(".input-tab-bg-vid").hide();
            $(".input-tab-ro-vid").hide();
            $(".input-tab-en-vid").show();
            $(".input-tab-trigger-en-vid").css("background-color", "#666666");
            $(".input-tab-trigger-en-vid").css("color", "#fff");
            $(".input-tab-trigger-bg-vid").css("background-color", "#fff");
            $(".input-tab-trigger-bg-vid").css("color", "#333");
            $(".input-tab-trigger-ro-vid").css("background-color", "#fff");
            $(".input-tab-trigger-ro-vid").css("color", "#333");
        });

        $(".input-tab-trigger-bg-vid").click(function () {
            $(".input-tab-bg-vid").show();
            $(".input-tab-en-vid").hide();
            $(".input-tab-ro-vid").hide();
            $(".input-tab-trigger-en-vid").css("background-color", "#fff");
            $(".input-tab-trigger-en-vid").css("color", "#333");
            $(".input-tab-trigger-ro-vid").css("background-color", "#fff");
            $(".input-tab-trigger-ro-vid").css("color", "#333");
            $(".input-tab-trigger-bg-vid").css("background-color", "#666666");
            $(".input-tab-trigger-bg-vid").css("color", "#fff");
        });

    });
</script>
