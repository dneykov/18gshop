<?php

$__page = 'new_language';

require '__top.php';

$__user->permission_check('езици','r');


if(isset ($_GET['delete'])){


 $__user->permission_check('езици','rw');


 $id = (int)$_GET['delete'];

 $language = new language($id);

 if($language->getDefault() == 1) {
     po_taka_set_status('Не може да се изтрие дефаулт езика');
     exit;
 }

 $prefix = $language->getPrefix();

 $stm = $pdo->prepare("

DELETE FROM `languages` WHERE `id` = ? LIMIT 1 ;


ALTER TABLE `artikuli` DROP `dop_info_{$prefix}`  ;

ALTER TABLE `etiketi` DROP `ime_{$prefix}`  ;

ALTER TABLE `etiketi_grupa` DROP `ime_{$prefix}`  ;


ALTER TABLE `kategorii` DROP `ime_{$prefix}`  ;

ALTER TABLE `vid` DROP `ime_{$prefix}` ;



");
   
 $stm -> bindValue(1, $language->getId(), PDO::PARAM_INT);

 $stm -> execute();

 // tuka tr da triq i etiketi cache :/ 


po_taka_set_status('изтрит език');

 exit;
}







$new_lang_errors = NULL;
if(isset ($_GET['submit'],$_GET['new_lang_name'], $_GET['new_lang_prefix'])){


$__user->permission_check('езици','rw');


$name = $_GET['new_lang_name'];
$template_dir = $_GET['new_lang_prefix'];
$prefix = uniqid();


$name = trim($name);

if(mb_strlen($name) < 1) {
    $new_lang_errors[] = 'въведи вярно име';
}

if(!preg_match( '|^[a-zA-Z]+$|is', $template_dir) ){
    $new_lang_errors[] = 'Въведи точно име на темплейта';
}


if($new_lang_errors == NULL) {

    $pdo->beginTransaction();

    $stm = $pdo->prepare('SELECT COUNT(`id`) AS `LANG_COUNT` FROM `languages` WHERE `default` = 1');
    $stm -> execute();
    $language_isset_default = $stm->fetch();
    $language_isset_default = $language_isset_default[0];

    $stm = $pdo->prepare('SELECT `order` FROM `languages` ORDER BY `order` DESC LIMIT 1');
    $stm -> execute();
    if($stm -> rowCount() == 0 ) $new_lang_order = 0;
    else {
        $new_lang_order = $stm -> fetch();
        $new_lang_order = $new_lang_order[0] + 1;
    }


    $language_isset_default = 1 - (int)$language_isset_default;


    if($language_isset_default > 1 || $language_isset_default < 0 ) greshka('gre6ka s defauta na ezicite');


    $stm = $pdo->prepare('INSERT INTO `languages`(`prefix`, `name`, `template_folder`, `default`, `order`, `enabled`) VALUES(?, ?, ?, ?, ?, 1 )');
    $stm -> bindValue(1, $prefix, PDO::PARAM_STR);
    $stm -> bindValue(2, $name, PDO::PARAM_STR);
    $stm -> bindValue(3, $template_dir, PDO::PARAM_STR);
    $stm -> bindValue(4, $language_isset_default, PDO::PARAM_INT);
    $stm -> bindValue(5, $new_lang_order, PDO::PARAM_INT);
    $stm -> execute();


    $pdo->exec("ALTER TABLE `artikuli` ADD `dop_info_{$prefix}` VARCHAR( 255 ) NOT NULL");

    $pdo->exec("ALTER TABLE `etiketi` ADD `ime_{$prefix}` VARCHAR( 255 ) NOT NULL");

    $pdo->exec("ALTER TABLE `etiketi_grupa` ADD `ime_{$prefix}` VARCHAR( 255 ) NOT NULL");


    $pdo->exec("ALTER TABLE `kategorii` ADD `ime_{$prefix}` VARCHAR( 255 ) NOT NULL");

    $pdo->exec("ALTER TABLE `vid` ADD `ime_{$prefix}` VARCHAR( 255 ) NOT NULL");



    $pdo->commit();

    setcookie('status_info', 'Добавен нов език');
    ?>
<meta http-equiv="Refresh" content="0;URL=<?php echo url_admin; ?>new_language.php" />
    <?php
    exit;


    
}





/*

ALTER TABLE `artikuli` ADD `test` VARCHAR( 255 ) NOT NULL
ALTER TABLE `artikuli` DROP `test`
i dop.info

ALTER TABLE `etiketi` ADD `ime_en` VARCHAR( 255 ) NOT NULL
ALTER TABLE `etiketi` DROP `ime_en`


ALTER TABLE `etiketi_grupa` ADD `ime_em` VARCHAR( 255 ) NOT NULL
ALTER TABLE `etiketi_grupa` DROP `ime_em`

ALTER TABLE `kategorii` ADD `ime_en` VARCHAR( 255 ) NOT NULL
ALTER TABLE `kategorii` DROP `ime_en`

ALTER TABLE `vid` ADD `ime_en` VARCHAR( 255 ) NOT NULL
ALTER TABLE `vid` DROP `ime_en`


*/







}





require 'template/new_language.php';

?>
