<?php 
	require '__top.php';
	$stm = $pdo->prepare('SELECT `id` FROM `orders`');
	$stm -> execute();
	foreach ($stm->fetchAll() as $v) {

		$order = new order_admin((int)$v['id']);

		$orderDetails = array();
		$orderProducts = $order->getProducts();
		$contactInformation = $order->getContactInformation();

		$priceAll = 0;

		$da_ids = null;

		foreach($orderProducts as $key => $val){
			if(!isProductAvailable((int)$key)){
				continue;
			}	
			if($da_ids != null && $da_ids[(int)$key] == (int)$key){
				continue;
			}			
			$products_info = array();
			$products = "";					
			$artikul = new artikul((int)$key);

			$products = $artikul->getId();
			$products_info[$artikul->getId()]['marka'] = $artikul->getIme_marka();
        	$products_info[$artikul->getId()]['model'] = $artikul->getIme();

			$promocena=$artikul->getCena_promo();
			if($promocena==0){
				$priceAll = $priceAll+($artikul->getCena())*$orderProducts[$key]['count'];
				$orderProducts[$key]['price'] = $artikul->getCena();
			}else{
				$priceAll = $priceAll+($artikul->getCena_promo())*$orderProducts[$key]['count'];
				$orderProducts[$key]['price'] = $artikul->getCena_promo();
			}

			if($artikul->getAddArtikuli() != "") {
				$da = explode(",", $artikul->getAddArtikuli());
				$orderProducts[$key]['add_artikuli'] = "";
				foreach($da as $d) {
					$dr = new artikul((int)$d);
                    $orderProducts[$key]['add_artikuli'] = $orderProducts[$key]['add_artikuli'].", ".$d;
                    $da_ids[$d] = (int)$d;
                    $products .= ", ".$dr->getId();
	                $products_info[$dr->getId()]['marka'] = $dr->getIme_marka();
	                $products_info[$dr->getId()]['model'] = $dr->getIme();
                }
			}

			if(isset($orderProducts[$key]['options'])){
				$valll = "";
				foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal){		
                    $expl_arr = explode(",", $optionVal);
                    foreach($expl_arr as $keyy => $vall) {
                        if($keyy == 0){
                            $artikulid = (int)$vall;
                        }
                        if($keyy == 1 && $artikulid == $key){
                        	if($vall != "XL"){
	                        	$etiket = new etiket((int)$vall);
	                        	$valll = $etiket->getIme();
	                        } else {
	                        	$valll = $vall;
	                        }
                        }
                    }
                    $orderProducts[$key]['options'][$optionkey] = $artikulid.", ".$valll;
                }
            }

			$orderProducts[$key]['ids']=$products;
   			$orderProducts[$key]['info']=$products_info;
   			$orderProducts[$key]['type']="product";
		}

		$ds = $pdo->prepare("SELECT * FROM `locations` WHERE `country_name` = ? LIMIT 1");
		$ds -> bindValue(1, serialize($order->getCountryName()), PDO::PARAM_STR);
        $ds -> execute();
        $d = $ds->fetch();
        $dostavka_cena = $d['delivery_price'];
        $dostavka_free = $d['free_delivery'];
        if($dostavka_free<=$priceAll){
            $delivery = 0;
        } else {
        	$delivery = $dostavka_cena;
        }

		$contactInformation['delivery'] = $delivery;
		$priceAll += $delivery;

		$orderDetails['details'] = $orderProducts;
		$orderDetails['header'] = $contactInformation;

		$stm = $pdo->prepare('UPDATE `orders` SET `details` = ?, `cost` = ? WHERE `id`=?;');
		$stm -> bindValue(1, serialize($orderDetails), PDO::PARAM_STR);
		$stm -> bindValue(2, number_format($priceAll, 2, '.', ''), PDO::PARAM_STR);
		$stm -> bindValue(3, $v['id'], PDO::PARAM_INT);
		$stm -> execute();

	}
?>