<?php
require '__top.php';

try{

if(!isset ($_REQUEST['etiket_grupa'])) greshka('Няма етикет-grupa номер');
else $etiket_nomer = (int)$_REQUEST['etiket_grupa'];

if(!isset ($_REQUEST['etiket_grupa'])) greshka('etiketgrupa REQUEST');

$etiket_grupa = new etiket_grupa((int)$_REQUEST['etiket_grupa']);


if(isset ($_GET['new_tag_names'])){

    $names = $_GET['new_tag_names'];
    if( count($names) != ( count($__languages)  ) ) greshka('категория имена масив не е голям колкото броя езици');
    $sql = 'INSERT INTO `etiketi` (`id_etiketi_grupa` ';
    if($__languages) foreach ($__languages as $v){
        $sql .= ' , `ime_'.$v-> getPrefix().'` ';
    }

    $sql .= ') VALUES( ? ';

    foreach ($__languages as $v) {
        $sql .= ' , ? ';
    }

    $sql .= ') ';

    $stm = $pdo->prepare($sql);
    $stm -> bindValue(1, $etiket_grupa->getId(), PDO::PARAM_INT);

    foreach ($names as $k => $v) {
        $stm -> bindValue($k+2, $v, PDO::PARAM_STR);
    }

    $stm -> execute();
    exit;
}



}
catch (Exception $e){
    greshka($e);
}
?>
    <div class="input-tab-holder" style="margin-top: 10px;float: left;height: auto;overflow: hidden;">
        <div class="input-tab-title"><?php echo $etiket_grupa->getIme(); ?></div>
        <?php
            $inputs = "";
                if($__languages) foreach (array_reverse($__languages) as $v){
                     echo '<div class="input-tab-trigger-' . $v->getName() . '-etiket">' . $v->getName() . '</div>';
                     $inputs = $inputs . '<input class="new_etiket_ime input-tab-' . $v->getName() . '-etiket" id="new_etiket_ime'.$etiket_nomer.'" type="text">';
                }
        ?>
    </div>
    <?php echo $inputs; ?>

    <div class="error">Моля въведете <?php echo $etiket_grupa->getIme(); ?></div>
    <button type="button" onclick="new_etiket_send(<?php echo $etiket_nomer; ?>)" class="button-save" style="margin-top: 10px;"><?php echo lang_save; ?></button>
    <input type="hidden" name="etiket_nomer<?php echo $etiket_nomer; ?>" value="<?php echo $etiket_nomer; ?>">
    <button type="button" onclick="new_etiket_cancel(<?php echo $etiket_nomer; ?>);" class="button-cancel"><?php echo lang_cancel; ?></button>

     <script>
        $(document).ready(function(){
            $(".input-tab-bg-etiket").keyup(function () {
                var val_bg = $('.input-tab-bg-etiket').val();
                $('.input-tab-en-etiket').val(val_bg);
                $('.input-tab-ro-etiket').val(val_bg);
            });

            $(".input-tab-trigger-ro-etiket").click(function(){
                $(".input-tab-bg-etiket").hide();
                $(".input-tab-en-etiket").hide();
                $(".input-tab-ro-etiket").show();
                $(".input-tab-trigger-ro-etiket").css("background-color","#666666");
                $(".input-tab-trigger-ro-etiket").css("color","#fff");
                $(".input-tab-trigger-bg-etiket").css("background-color","#fff");
                $(".input-tab-trigger-bg-etiket").css("color","#333");
                $(".input-tab-trigger-en-etiket").css("background-color","#fff");
                $(".input-tab-trigger-en-etiket").css("color","#333");
            });

            $(".input-tab-trigger-en-etiket").click(function(){
                $(".input-tab-bg-etiket").hide();
                $(".input-tab-ro-etiket").hide();
                $(".input-tab-en-etiket").show();
                $(".input-tab-trigger-en-etiket").css("background-color","#666666");
                $(".input-tab-trigger-en-etiket").css("color","#fff");
                $(".input-tab-trigger-bg-etiket").css("background-color","#fff");
                $(".input-tab-trigger-bg-etiket").css("color","#333");
                $(".input-tab-trigger-ro-etiket").css("background-color","#fff");
                $(".input-tab-trigger-ro-etiket").css("color","#333");
            });

            $(".input-tab-trigger-bg-etiket").click(function(){
                $(".input-tab-bg-etiket").show();
                $(".input-tab-en-etiket").hide();
                $(".input-tab-ro-etiket").hide();
                $(".input-tab-trigger-en-etiket").css("background-color","#fff");
                $(".input-tab-trigger-en-etiket").css("color","#333");
                $(".input-tab-trigger-ro-etiket").css("background-color","#fff");
                $(".input-tab-trigger-ro-etiket").css("color","#333");
                $(".input-tab-trigger-bg-etiket").css("background-color","#666666");
                $(".input-tab-trigger-bg-etiket").css("color","#fff");
            });

        });
    </script>
