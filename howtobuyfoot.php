<?php
    require '__top.php';
?>
<html>
    <head>
        <title>Как да поръчам</title>
        <style>
            body{
                font-family: Verdana;
                background-color: #FFF;
                margin: 0;
                padding: 0;
                overflow: hidden;
            }
            #head {
                width: 100%;
                background-color: #333;
                height: 25px;
                color: #FFF;
                font-family: Verdana;
                font-weight: normal;
                font-size: 15px;
                border-bottom: 3px solid #FF671F;
            }
            #head span {
                line-height: 28px;
                display: block;
                height: 28px;
                padding-left: 10px;
                width: 200px;
                text-align: center;
                background-color: #FF671F;
            }
            #howtobuy {
                height: 100%;
                overflow-y: scroll;
            }
        </style>
    </head>   
    <body>
        <div id="head">
            <span>Как да поръчам</span>
        </div>
        <div id="howtobuy">
            <?php
                $stm = $pdo->prepare('SELECT text_'.lang_prefix.' FROM `zz_about` where `id`=2');
                $stm -> execute();
                $data= $stm->fetchall();
                $data=$data[0][0];
                echo '<div style="margin:10px;word-wrap: break-word;">'.$data.'</div>';
            ?>
        </div>
    </body>
</html>