<?php
require '../__top.php';

$stm = $pdo->prepare('SELECT * FROM `artikuli`');
$stm -> execute();

foreach ($stm->fetchAll() as $v) {
    $artikul = new artikul_admin($v);
    if (isset($_GET['search'])) {
        $artikul->cache_search();
    } else {
        $artikul->cache_update();
    }
}