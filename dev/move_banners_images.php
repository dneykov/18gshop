<?php
exit;
set_time_limit(10000);
require '../__top.php';

$stm = $pdo->prepare("SELECT * FROM `zz_banners`");
$stm->execute();
$banners = $stm->fetchAll();

foreach ($banners as $banner) {
    $fieldsToUpdate = [];
    $mainImage = '/Users/neykov/code/18gshop/' . $banner['image'];
    $mainImageInfo = pathinfo($mainImage);
    $mainImage = $mainImageInfo['dirname'] . '/' . $mainImageInfo['basename'];
    $mainImage_resize = str_replace("thumbnail", "resize", $mainImage);
    $mainImage_t = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $mainImage);
    $mainImage_b = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $mainImage);
    $mainImage_tt = preg_replace('|^(.+)\.(.+)$|is', '$1_tt.$2', $mainImage);

    // Files to move
    $newMainImagePath = str_replace(['uploads_images', 'upload_images', 'images_adv', 'images/banners'], 'images/banners', $mainImage);
    $newMainImagePath_resize = str_replace(['uploads_images', 'upload_images', 'images_adv', 'images/banners'], 'images/banners', $mainImage_resize);
    $newMainImagePath_t = str_replace(['uploads_images', 'upload_images', 'images_adv', 'images/banners'], 'images/banners', $mainImage_t);
    $newMainImagePath_b = str_replace(['uploads_images', 'upload_images', 'images_adv', 'images/banners'], 'images/banners', $mainImage_b);
    $newMainImagePath_tt = str_replace(['uploads_images', 'upload_images', 'images_adv', 'images/banners'], 'images/banners', $mainImage_tt);

    if (file_exists($mainImage) ||
        file_exists($mainImage_resize) ||
        file_exists($mainImage_t) ||
        file_exists($mainImage_b) ||
        file_exists($mainImage_tt)) {
        if (!is_dir(dirname($newMainImagePath))) {
            mkdir(dirname($newMainImagePath), 0777, true);
        }

        rename($mainImage, $newMainImagePath);
        rename($mainImage_resize, $newMainImagePath_resize);
        rename($mainImage_t, $newMainImagePath_t);
        rename($mainImage_b, $newMainImagePath_b);
        rename($mainImage_tt, $newMainImagePath_tt);

        $fieldsToUpdate['image'] = pathinfo($banner['image'], PATHINFO_DIRNAME) . '/' . pathinfo($banner['image'], PATHINFO_BASENAME);
        $fieldsToUpdate['image'] = str_replace(['uploads_images', 'upload_images', 'images_adv', 'images/banners'], 'images/banners', $fieldsToUpdate['image']);
    }


    if (count($fieldsToUpdate) > 0) {
        $sql = 'UPDATE `zz_banners` SET ';
        foreach ($fieldsToUpdate as $key => $field) {
            $sql .= $key . '=:' . $key . ',';
        }
        $sql = rtrim($sql, ',');
        $sql .= ' WHERE `id` = :id';
        echo '<pre>';
        print_r($sql);
        echo '</pre>';

        $stm = $pdo->prepare($sql);
        foreach ($fieldsToUpdate as $key => $field) {
            $stm->bindValue($key, $field, PDO::PARAM_STR);
        }
        $stm->bindValue(':id', $banner['id'], PDO::PARAM_INT);
        $stm->execute();
    }
}
