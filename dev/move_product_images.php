<?php
exit;
set_time_limit(10000);
require '../__top.php';

$stm = $pdo->prepare("SELECT * FROM `artikuli`");
$stm->execute();
$products = $stm->fetchAll();

foreach ($products as $product) {
    $fieldsToUpdate = [];
    $mainImage = '/Users/neykov/code/18gshop/' . $product['kartinka'];
    $mainImageInfo = pathinfo($mainImage);
    $mainImage = $mainImageInfo['dirname'] . '/' . $mainImageInfo['basename'];
    $mainImage_resize = str_replace("thumbnail", "resize", $mainImage);
    $mainImage_t = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $mainImage);
    $mainImage_b = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $mainImage);
    $mainImage_tt = preg_replace('|^(.+)\.(.+)$|is', '$1_tt.$2', $mainImage);

    // Files to move
    $newMainImagePath = str_replace(['uploads_images', 'upload_images'], 'images/products', $mainImage);
    $newMainImagePath_resize = str_replace(['uploads_images', 'upload_images'], 'images/products', $mainImage_resize);
    $newMainImagePath_t = str_replace(['uploads_images', 'upload_images'], 'images/products', $mainImage_t);
    $newMainImagePath_b = str_replace(['uploads_images', 'upload_images'], 'images/products', $mainImage_b);
    $newMainImagePath_tt = str_replace(['uploads_images', 'upload_images'], 'images/products', $mainImage_tt);


    if (file_exists($mainImage)) {
        if (!is_dir(dirname($newMainImagePath))) {
            mkdir(dirname($newMainImagePath), 0777, true);
        }

        rename($mainImage, $newMainImagePath);
        rename($mainImage_resize, $newMainImagePath_resize);
        rename($mainImage_t, $newMainImagePath_t);
        rename($mainImage_b, $newMainImagePath_b);
        rename($mainImage_tt, $newMainImagePath_tt);

        $fieldsToUpdate['kartinka'] = pathinfo($product['kartinka'], PATHINFO_DIRNAME) . '/' . pathinfo($product['kartinka'], PATHINFO_BASENAME);
        $fieldsToUpdate['kartinka'] = str_replace(['uploads_images', 'upload_images'], 'images/products', $fieldsToUpdate['kartinka']);
    }

    for ($q = 1; $q <= 12; $q++) {
        if (empty($product['img' . $q])) continue;

        $image = '/Users/neykov/code/18gshop/' . $product['img' . $q];
        $imageInfo = pathinfo($image);
        $image = $imageInfo['dirname'] . '/' . $imageInfo['basename'];
        $image_resize = str_replace("thumbnail", "resize", $image);
        $image_t = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $image);
        $image_b = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $image);
        $image_tt = preg_replace('|^(.+)\.(.+)$|is', '$1_tt.$2', $image);

        // Files to move
        $newImagePath = str_replace(['uploads_images', 'upload_images'], 'images/products', $image);
        $newImagePath_resize = str_replace(['uploads_images', 'upload_images'], 'images/products', $image_resize);
        $newImagePath_t = str_replace(['uploads_images', 'upload_images'], 'images/products', $image_t);
        $newImagePath_b = str_replace(['uploads_images', 'upload_images'], 'images/products', $image_b);
        $newImagePath_tt = str_replace(['uploads_images', 'upload_images'], 'images/products', $image_tt);

        if (file_exists($image)) {
            if (!is_dir(dirname($imageInfo))) {
                mkdir(dirname($imageInfo), 0777, true);
            }

            rename($image, $newImagePath);
            rename($image_resize, $newImagePath_resize);
            rename($image_t, $newImagePath_t);
            rename($image_b, $newImagePath_b);
            rename($image_tt, $newImagePath_tt);

            $fieldsToUpdate['img' . $q] = pathinfo($product['img' . $q], PATHINFO_DIRNAME) . '/' . pathinfo($product['img' . $q], PATHINFO_BASENAME);
            $fieldsToUpdate['img' . $q] = str_replace(['uploads_images', 'upload_images'], 'images/products', $fieldsToUpdate['img' . $q]);
        }
    }

    if (count($fieldsToUpdate) > 0) {
        $sql = 'UPDATE `artikuli` SET ';
        foreach ($fieldsToUpdate as $key => $field) {
            $sql .= $key . '=:' . $key . ',';
        }
        $sql = rtrim($sql, ',');
        $sql .= ' WHERE `id` = :id';

        $stm = $pdo->prepare($sql);
        foreach ($fieldsToUpdate as $key => $field) {
            $stm->bindValue($key, $field, PDO::PARAM_STR);
        }
        $stm->bindValue(':id', $product['id'], PDO::PARAM_INT);
        $stm->execute();
    }
}
