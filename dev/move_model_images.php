<?php
exit;
set_time_limit(10000);
require '../__top.php';

$stm = $pdo->prepare("SELECT * FROM `model`");
$stm->execute();
$models = $stm->fetchAll();

foreach ($models as $model) {
    $fieldsToUpdate = [];
    $mainImage = '/Users/neykov/code/18gshop/' . $model['image'];
    $mainImageInfo = pathinfo($mainImage);
    $mainImage = $mainImageInfo['dirname'] . '/' . $mainImageInfo['basename'];
    $mainImage_grayscale = str_replace('_color', '', $mainImage);

    // Files to move
    $newMainImagePath = str_replace(['uploads_images', 'upload_images', 'images_modes'], 'images/models', $mainImage);
    $newMainImagePath_grayscale = str_replace(['uploads_images', 'upload_images', 'images_modes'], 'images/models', $mainImage_grayscale);

    if (file_exists($mainImage)) {
        if (!is_dir(dirname($newMainImagePath))) {
            mkdir(dirname($newMainImagePath), 0777, true);
        }

        rename($mainImage, $newMainImagePath);
        rename($mainImage_grayscale, $newMainImagePath_grayscale);

        $fieldsToUpdate['image'] = pathinfo($model['image'], PATHINFO_DIRNAME) . '/' . pathinfo($model['image'], PATHINFO_BASENAME);
        $fieldsToUpdate['image'] = str_replace(['uploads_images', 'upload_images', 'images_modes'], 'images/models', $fieldsToUpdate['image']);
    }


    if (count($fieldsToUpdate) > 0) {
        $sql = 'UPDATE `model` SET ';
        foreach ($fieldsToUpdate as $key => $field) {
            $sql .= $key . '=:' . $key . ',';
        }
        $sql = rtrim($sql, ',');
        $sql .= ' WHERE `id` = :id';
        echo '<pre>';
        print_r($sql);
        echo '</pre>';

        $stm = $pdo->prepare($sql);
        foreach ($fieldsToUpdate as $key => $field) {
            $stm->bindValue($key, $field, PDO::PARAM_STR);
        }
        $stm->bindValue(':id', $model['id'], PDO::PARAM_INT);
        $stm->execute();
    }
}
