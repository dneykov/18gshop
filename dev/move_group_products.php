<?php
exit;
set_time_limit(10000);
require '../__top.php';

$stm = $pdo->prepare("SELECT * FROM `group_products`");
$stm->execute();
$products = $stm->fetchAll();

foreach ($products as $product) {
    $fieldsToUpdate = [];
    $mainImage = '/Users/neykov/code/18gshop/' . $product['image'];
    $mainImageInfo = pathinfo($mainImage);
    $mainImage = $mainImageInfo['dirname'] . '/' . $mainImageInfo['basename'];
    $mainImage_resize = str_replace("thumbnail", "resize", $mainImage);
    $mainImage_t = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $mainImage);
    $mainImage_b = preg_replace('|^(.+)\.(.+)$|is', '$1_b.$2', $mainImage);
    $mainImage_tt = preg_replace('|^(.+)\.(.+)$|is', '$1_tt.$2', $mainImage);

    // Files to move
    $newMainImagePath = str_replace(['uploads_images', 'upload_images', 'images/banners'], 'images/bundle', $mainImage);
    $newMainImagePath_resize = str_replace(['uploads_images', 'upload_images', 'images/banners'], 'images/bundle', $mainImage_resize);
    $newMainImagePath_t = str_replace(['uploads_images', 'upload_images', 'images/banners'], 'images/bundle', $mainImage_t);
    $newMainImagePath_b = str_replace(['uploads_images', 'upload_images', 'images/banners'], 'images/bundle', $mainImage_b);
    $newMainImagePath_tt = str_replace(['uploads_images', 'upload_images', 'images/banners'], 'images/bundle', $mainImage_tt);


    if (file_exists($mainImage)) {
        if (!is_dir(dirname($newMainImagePath))) {
            mkdir(dirname($newMainImagePath), 0777, true);
        }

        rename($mainImage, $newMainImagePath);
        rename($mainImage_resize, $newMainImagePath_resize);
        rename($mainImage_t, $newMainImagePath_t);
        rename($mainImage_b, $newMainImagePath_b);
        rename($mainImage_tt, $newMainImagePath_tt);

        $fieldsToUpdate['image'] = pathinfo($product['image'], PATHINFO_DIRNAME) . '/' . pathinfo($product['image'], PATHINFO_BASENAME);
        $fieldsToUpdate['image'] = str_replace(['uploads_images', 'upload_images', 'images/banners'], 'images/bundle', $fieldsToUpdate['image']);
    }

    if (count($fieldsToUpdate) > 0) {
        $sql = 'UPDATE `group_products` SET ';
        foreach ($fieldsToUpdate as $key => $field) {
            $sql .= $key . '=:' . $key . ',';
        }
        $sql = rtrim($sql, ',');
        $sql .= ' WHERE `id` = :id';
        echo '<pre>';
        print_r($sql);
        echo '</pre>';

        $stm = $pdo->prepare($sql);
        foreach ($fieldsToUpdate as $key => $field) {
            $stm->bindValue($key, $field, PDO::PARAM_STR);
        }
        $stm->bindValue(':id', $product['id'], PDO::PARAM_INT);
        $stm->execute();
    }
}
