<?php
require '../__top.php';
$sql = 'SELECT * FROM `artikuli`';
$stm = $pdo -> prepare($sql);
$stm -> execute();

$products = $stm->fetchAll();

foreach ($products as $product) {
    $artikul = new artikul_admin($product);
    $artikul->cache_update();
    $artikul->cache_search();
}