<?php
define('connection_host', 'localhost');
define('connection_user', 'root');
define('connection_pass', '');
define('connection_db', '18gshop_2019');

define('url', 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://www.18gshop.test/');
define('urlm', 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://m.18gshop.test/');
define('mobile_domain', 'm.18gshop.test/');
define('www_mobile_domain', 'm.18gshop.test/');

define('dir_root', dirname(__FILE__) . '/'); // moje da se staticira

define('image_size_max_w', '800');
define('image_size_max_h', '600');

define('image_size_w', '400');
define('image_size_h', '300');

define('image_size_thumb_w', '200');
define('image_size_thumb_h', '150');

define('hash', 'a013658e30387f7a60cdaf0acf26850d');

define('mail_office', 'danielnkv@gmail.com');
define('mail_orders', 'danielnkv@gmail.com');

define('GOOGLE_MAPS_API_KEY', 'AIzaSyBVs74et8zvcDCJz8cjQdX4vvuFZOIO3wE');

define('NEWSLETTER_MAIL_HOST', 'lima.superhosting.bg');
define('NEWSLETTER_MAIL', 'noreply@18gshop.com');
define('NEWSLETTER_MAIL_PASSWORD', 'uD4U~MUf+^p5');
define('NEWSLETTER_FROM', 'noreply@18gshop.com');
define('NEWSLETTER_FROM_NAME', '18gshop.com');

define('MAIL_HOST', 'lima.superhosting.bg');
define('MAIL_USERNAME', 'noreply@18gshop.com');
define('MAIL_PASSWORD', 'uD4U~MUf+^p5');
define('MAIL_FROM', 'noreply@18gshop.com');
define('MAIL_FROM_NAME', '18gshop.com');

error_reporting(E_ALL ^ E_WARNING);

date_default_timezone_set('Europe/Sofia');

class PDOX extends PDO
{
    private static $con = NULL;

    public function __construct()
    {
        greshka('PDO construct error');
    }

    public static function vrazka()
    {
        if (PDOX::$con === NULL) {
            PDOX::$con = new PDO('mysql:host=' . connection_host . ';dbname=' . connection_db, connection_user, connection_pass,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::ATTR_PERSISTENT => true));
            return PDOX::$con;
        } else {
            return PDOX::$con;
        }
    }

} // PDOX class

ini_set('memory_limit', '256M');

try {
    $pdo = PDOX::vrazka();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    die("Cannot connect to database!");
}

require dir_root . '__functions.php';

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    greshka("n: $errno <br>error: $errstr <br>file: $errfile <br>line: $errline");
}

set_error_handler('myErrorHandler');


if (isset($_COOKIE['language'])) {
    $lang = (int)$_COOKIE['language'];
} else {
    $lang = NULL;
}

if (!defined('dir_root_admin')) {
    if ($__languages == NULL) {
        greshka(NULL, 'Няма въведен/активиран език');
    }
}

if ($__languages) foreach ($__languages as $v) {
    if ($v->getDefault() == 1) {
        define('lang_default_prefix', $v->getPrefix());
        define('lang_default_id', $v->getId());
    }
    if ($lang == $v->getId()) {
        define('lang_id', $v->getId());
        define('lang_prefix', $v->getPrefix());
        define('lang_directory', $v->getTemplate_folder());
    }
}

if (!defined('lang_id')) {
    if ($__languages) foreach ($__languages as $v) {
        if ($v->getDefault() == 1) {
            define('lang_id', $v->getId());
            define('lang_prefix', $v->getPrefix());
            define('lang_directory', $v->getTemplate_folder());
            $_COOKIE['language'] = $v->getId();
            break;
        }
    }
}
unset($lang);

//проверка за мобилна версия
if (isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME'] . "/" == mobile_domain || $_SERVER['SERVER_NAME'] . "/" == www_mobile_domain)) {
    if (defined('lang_directory')) define('dir_root_template', dir_root . 'template_m/' . lang_directory . '/');
} else {
    if (defined('lang_directory')) define('dir_root_template', dir_root . 'template/' . lang_directory . '/');
}

switch (lang_directory) {
    case "bg":
        define('CURRENCY_RATE', 1);
        break;
    case "en":
        $e = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :c LIMIT 1");
        $e->bindValue(':c', "euro", PDO::PARAM_STR);
        $e->execute();
        $eu = $e->fetch();

        define('CURRENCY_RATE', $eu['value']);
        break;
    case "ro":
        $e = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :c LIMIT 1");
        $e->bindValue(':c', "lei", PDO::PARAM_STR);
        $e->execute();
        $eu = $e->fetch();

        define('CURRENCY_RATE', $eu['value']);
        break;
    default:
        define('CURRENCY_RATE', 1);
        break;
}

$pat = get_pat();

session_start();

require dir_root_template . '__language.php';

//проверка за мобилна версия
if (isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME'] . "/" == mobile_domain || $_SERVER['SERVER_NAME'] . "/" == www_mobile_domain)) {
    define('url_template_folder', urlm . 'template_m/' . lang_directory . '/');
} else {
    define('url_template_folder', url . 'template/' . lang_directory . '/');
}
