<style type="text/css">
	#cont {
		margin: 5px auto;
		text-align: left;
		padding-bottom: 50px;
		font-size: 14px;
		padding-left: 10px;
		padding-right: 15px;
	}
	#tabs-bar {
		height: 20px;
		width: 100%;
		padding-bottom: 2px;
		border-bottom: 1px solid #FF671F;
		margin-bottom: 20px;
	}
	#tabs-bar a{
		margin-left: 5px;
		margin-right: 20px;
		font-size: 16px;
		color: #333;
	}
	.submit {
		display: block;
		background: none repeat scroll 0 0 #333;
		border: medium none;
		color: #fff;
		cursor: pointer;
		float: right;
		height: 36px;
		width: 180px;
		line-height: 33px;
		text-align: center;
		text-decoration: none;
		font-size: 13px;
	}
	.submit:hover {
		background: none repeat scroll 0 0 #FF671F;
	}
	input, textarea, select {
		font-size: 13px;
		padding: 5px 0px;
		border: 1px solid #CCC;
	}
</style>
<div id="where_am_i_box">
	<div id="where_am_i">
		<a href="<?php echo urlm; ?>" style="color: #333;">18gshop</a> <span style="color: #333;">/</span>
		<a href="<?php echo urlm.'/index.php?user'; ?>" style="color: #333;">account</a> <span style="color: #333;">/</span>
		<a href="<?php echo urlm.'/index.php?user'; ?>" style="color: #333;"><?php echo $__user->getName(); ?></a>
        <?php if($__user->is_partner()) : ?>
            <span style="color: #333;">/</span>
            <span style="color: #FF671F">partner</span>
        <?php endif; ?>
	</div>
</div>
<div id="tabs-bar">
	<a href="<?php echo urlm.'/index.php?user'; ?>" style="color: #FF671F;">account</a>
	<a href="<?php echo urlm.'/index.php?user&o'; ?>">orders
	<?php
		$stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ?");
        $stm->bindValue(1, $__user->getId(), PDO::PARAM_STR);
        $stm->execute();
        echo $stm->rowCount();
    ?>
	</a>
	<a  href="login_m.php?logout=1">logout</a>
</div>
<div id="cont">
<?php
	if(isset($__user)){
		?>
		<div class="userDetails">
			<div class="reg-field-left" style="margin-bottom: -10px; margin-top: 10px; border-width: 0px; text-align: left;">first name</div>
			<div class="reg-field-right" style="margin-bottom: -10px; margin-top: 10px; border-width: 0px; text-align: left;">last name</div>
			<input type="text" name="user_name" autocomplete="off" value="<?php echo $__user->getName(); ?>" disabled class="reg-field-left"/>
			<input type="text" name="user_name_last" autocomplete="off" value="<?php echo $__user->getName_last(); ?>" disabled class="reg-field-right"/>

			<div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">e-mail<span class="need">*</span></div>
			<div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">country<span class="need">*</span></div>
			<input type="text" autocomplete="off" name="user_mail" value="<?php echo $__user->getMail(); ?>" disabled class="reg-field-left"/>
			<select id="country" name="country" class="reg-field-right" style="padding: 3px !important;" disabled>
				<?php
					$stm = $pdo -> prepare("SELECT * FROM `locations`");
					$stm -> execute();
					foreach ($stm -> fetchAll() as $c) {
						echo "<option value='".$c['id']."'";
						if(($__user->getCountryId() == $c['id']) || (isset($_POST['country']) && $_POST['country'] == $c['id'])) echo " selected";
						echo ">".$c['country_name']."</option>";
					}
				?>
			</select>

			<div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">mobile phone<span class="need">*</span></div>
			<div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">city<span class="need">*</span></div>
			<input type="text" autocomplete="off" name="gsm" value="<?php echo $__user->getPhone_mobile(); ?>" disabled class="reg-field-left"/>
			<input type="hidden" autocomplete="off" name="telephone" value="<?php echo $__user->getPhone_home(); ?>" disabled class="reg-field-right"/>
			<input type="text" autocomplete="off" name="city" value="<?php echo $__user->getAdress_town(); ?>" disabled class="reg-field-right"/>

			<div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">street<span class="need">*</span></div>
			<div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">street number<span class="need">*</span></div>
			<input type="text" autocomplete="off" name="street" value="<?php echo $__user->getAdress_street(); ?>" disabled class="reg-field-street" style="width: 49% !important;"/>
			<input type="text" autocomplete="off" name="number" value="<?php echo $__user->getAdress_number(); ?>" disabled class="reg-field-number" style="width: 49% !important; float: right;"/>

			<div class="user-reg-field-three" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left; margin-left: 0% !important;">entrance</div>
			<div class="user-reg-field-three" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">floor</div>
			<div class="user-reg-field-three" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">apartment</div>
			<input type="text" autocomplete="off" name="vhod" value="<?php echo $__user->getAdress_vhod(); ?>" class="user-reg-field-three" disabled style="margin-left: 0% !important;"/>
			<input type="text" autocomplete="off" name="etaj" value="<?php echo $__user->getAdress_etaj(); ?>" class="user-reg-field-three" disabled />
			<input type="text" autocomplete="off" name="apartament" value="<?php echo $__user->getAdress_ap();?>" disabled class="user-reg-field-three"/>

            <?php if($__user->getAccountType() == '1') : ?>
                <div style="clear:both;"></div>
                <div id="legal-form" style="margin-top: 40px;">
                    <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">company name<span class="need">*</span></div>
                    <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">f.l.p<span class="need">*</span></div>
                    <input type="text" autocomplete="off" name="company_name" value="<?php echo $__user->getCompanyName(); ?>" class="reg-field-left" disabled/>
                    <input type="text" autocomplete="off" name="company_mol" value="<?php echo $__user->getCompanyMol(); ?>" class="reg-field-right" disabled/>
                    <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">bulstat<span class="need">*</span></div>
                    <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">vat<span class="need">*</span></div>
                    <input type="text" autocomplete="off" name="company_eik" value="<?php echo $__user->getCompanyEik(); ?>" class="reg-field-left" disabled/>
                    <input type="text" autocomplete="off" name="company_vat" value="<?php echo $__user->getCompanyVat(); ?>" class="reg-field-right" disabled/>
                    <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">city<span class="need">*</span></div>
                    <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">phone</div>
                    <input type="text" autocomplete="off" name="company_city" value="<?php echo $__user->getCompanyCity(); ?>" class="reg-field-left" disabled/>
                    <input type="text" autocomplete="off" name="company_phone" value="<?php echo $__user->getCompanyPhone(); ?>" class="reg-field-right" disabled/>
                    <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">street<span class="need">*</span></div>
                    <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">number</div>
                    <input type="text" autocomplete="off" name="company_street" value="<?php echo $__user->getCompanyStreet(); ?>" class="reg-field-left" disabled/>
                    <input type="text" autocomplete="off" name="company_number" value="<?php echo $__user->getCompanyNumber(); ?>" class="reg-field-right" disabled/>
                </div>
            <?php endif; ?>

            <div style="float: right; margin-top: 20px; width: 100%;">
                <a href="<?php echo urlm.'/index.php?user&r'; ?>" class="submit">edit</a>
            </div>
		</div>
	<?php
		echo "</div>";
	} else {
		echo "<center>Please log in your account.</center>";
	}
?>
</div>
