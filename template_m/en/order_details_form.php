<?php
$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :preorder LIMIT 1");
$stm->bindValue(':preorder', "bank_details_" . lang_prefix, PDO::PARAM_STR);
$stm->execute();
$bankDetailsDb = $stm->fetch();
$bankDetails = $bankDetailsDb['value'];

$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :iban LIMIT 1");
$stm->bindValue(':iban', 'iban', PDO::PARAM_STR);
$stm->execute();
$ibanDb = $stm->fetch();
$iban = $ibanDb['value'];
?>
<style type="text/css">
    #payment, #delivery_method {
        margin-top: 10px;
        text-align: left;
        display: inline-block;
        vertical-align: top;
    }

    #payment {
        margin-right: 40px;
    }

    #payment #details {
        display: none;
    }

    #payment span, #payment label, #delivery_method span, #delivery_method label {
        font-size: 13px;
    }

    #payment span, #delivery_method span {
        display: inline-block;
        margin-bottom: 5px;
    }

    #payment label, #delivery_method label {
        display: inline-block;
        margin-right: 5px;
    }

    #payment #details, #payment #details > div, #payment #details strong {
        font-size: 13px;
    }

    @media screen and (max-width: 630px) {
        .container-options {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-orient: vertical;
            -moz-box-orient: vertical;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            /* optional */
            -webkit-box-align: start;
            -moz-box-align: start;
            -ms-flex-align: start;
            -webkit-align-items: flex-start;
            align-items: flex-start;
        }

        .container-options #payment {
            -webkit-box-ordinal-group: 2;
            -moz-box-ordinal-group: 2;
            -ms-flex-order: 2;
            -webkit-order: 2;
            order: 2;
        }

        .container-options #delivery_method {
            -webkit-box-ordinal-group: 1;
            -moz-box-ordinal-group: 1;
            -ms-flex-order: 1;
            -webkit-order: 1;
            order: 1;
        }
    }

    @media screen and (max-width: 375px) {
        #payment {
            margin-right: 0;
        }
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[name=payment_type]').change(function () {
            if ($(this).val() == "bank") {
                $("#payment #details").show();
            } else {
                $("#payment #details").hide();
            }
        });

        $('#btn-paypal').click(function () {
            var scode = $('#scode').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_order_paypal.php",
                data: $("#form_order").serialize() + "&scode=" + scode,
                cache: false,
                success: function () {
                    window.location = '<?php echo urlm; ?>';
                }
            });
        });

        $('#country').change(function () {
            var id = $('#country').val();
            var cost = $('#ajax_cost').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax_delivery_en_paypal.php",
                data: "id=" + id + "&cost=" + cost,
                cache: false,
                success: function (html) {
                    $('#shipping_js').html(" ");
                    $('#shipping_js').append(html);
                }
            });
        });

        if ($("input[name=account_type]:checked").val() == '0') {
            $("#legal-form").hide();
            $("div.cart-price").css("margin-top", "0px");
            $("#legal-form :input").prop("disabled", true);
            $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
        } else if ($("input[name=account_type]:checked").val() == '1') {
            $("#legal-form").show();
            $("div.cart-price").css("margin-top", "-30px");
            $("#legal-form :input").prop("disabled", false);
        }
        $("input[name=account_type]").change(function () {
            if ($(this).val() == '0') {
                $("#legal-form").hide();
                $("div.cart-price").css("margin-top", "0px");
                $("#legal-form :input").prop("disabled", true);
                $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
            } else {
                $("#legal-form").show();
                $("div.cart-price").css("margin-top", "-30px");
                $("#legal-form :input").prop("disabled", false);
            }
        });
    });
</script>
<div id="info_tab"><p>delivery details</p></div>
<div id="content" style="padding-left: 0; padding-right: 0;">
    <form action="fancy_login.php?p=ordrdetails" method="post" style="margin:20px 10px 0;" id="form_order">
        <div class="top-row" style="margin-left: 5px; margin-bottom: 10px;">
            <input type="radio" id="individual" style="width: auto;" name="account_type"
                   value="0" <?php if (isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '0') echo "checked"; ?> />
            <label for="individual">Individual person</label>
            <input type="radio" id="legal_entity" style="width: auto;" name="account_type"
                   value="1" <?php if (isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '1') echo "checked" ?> />
            <label for="legal_entity">Legal entity</label>
        </div>
        <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">
            first name
        </div>
        <div class="reg-field-right"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">last name
        </div>
        <input type="text" autocomplete="off" value="<?php echo $__user->getName(); ?>" disabled
               class="reg-field-left"/>
        <input type="text" autocomplete="off" value="<?php echo $__user->getName_last(); ?>" disabled
               class="reg-field-right"/>

        <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">
            e-mail
        </div>
        <div class="reg-field-right"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">country
        </div>
        <input type="text" autocomplete="off" name="edit_mail"
               value="<?php if (isset($_POST['edit_mail'])) echo $_POST['edit_mail']; else {
                   echo $__user->getMail();
               } ?>" id="regemail" class="reg-field-left"/>
        <input type="hidden" autocomplete="off" name="register_pass"
               value="<?php if (isset($_POST['register_pass'])) echo $_POST['register_pass']; ?>" id="regpassword"
               style="width: 98%;  margin: 5px 0px 5px 0px;">
        <input type="hidden" autocomplete="off" name="register_passAgain"
               value="<?php if (isset($_POST['register_passAgain'])) echo $_POST['register_passAgain']; ?>"
               id="regpasswordagain" style="width: 98%;">
        <select id="country" name="country" class="reg-field-right" style="padding: 3px !important;">
            <?php
            $stm = $pdo->prepare("SELECT * FROM `locations`");
            $stm->execute();
            foreach ($stm->fetchAll() as $c) {
                echo "<option value='" . $c['id'] . "'>" . $c['country_name'] . "</option>";
            }
            ?>
        </select>

        <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">
            mobile phone
        </div>
        <div class="reg-field-right"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">city
        </div>
        <input type="text" autocomplete="off" name="gsm"
               value="<?php if (isset($_POST['gsm'])) echo $_POST['gsm']; else {
                   echo $__user->getPhone_mobile();
               } ?>" id="reggsm" class="reg-field-left"/>
        <input type="hidden" autocomplete="off" name="telephone"
               value="<?php if (isset($_POST['telephone'])) echo $_POST['telephone']; else {
                   echo $__user->getPhone_home();
               } ?>" id="regphone" class="reg-field-right"/>
        <input type="text" autocomplete="off" name="city"
               value="<?php if (isset($_POST['city'])) echo $_POST['city']; else {
                   echo $__user->getAdress_town();
               } ?>" id="regtown" class="reg-field-right"/>

        <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">
            street
        </div>
        <div class="reg-field-right"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">street address
        </div>
        <input type="text" autocomplete="off" name="street"
               value="<?php if (isset($_POST['street'])) echo $_POST['street']; else {
                   echo $__user->getAdress_street();
               } ?>" id="regstreet" class="reg-field-street" style="width: 49% !important;"/>
        <input type="text" autocomplete="off" name="number"
               value="<?php if (isset($_POST['number'])) echo $_POST['number']; else {
                   echo $__user->getAdress_number();
               } ?>" id="regstreetnumber" class="reg-field-number" style="width: 49% !important; float: right;"/>

        <div class="user-reg-field-three"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left; margin-left: 0% !important;">
            entrance
        </div>
        <div class="user-reg-field-three"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left; float: left">floor
        </div>
        <div class="user-reg-field-three"
             style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">apartment
        </div>
        <input type="text" autocomplete="off" name="vhod"
               value="<?php if (isset($_POST['vhod'])) echo $_POST['vhod']; else {
                   echo $__user->getAdress_vhod();
               } ?>" id="regentrance" class="user-reg-field-three" style="margin-left: 0% !important;"/>
        <input type="text" autocomplete="off" name="etaj"
               value="<?php if (isset($_POST['etaj'])) echo $_POST['etaj']; else {
                   echo $__user->getAdress_etaj();
               } ?>" id="regfloor" class="user-reg-field-three" style="float: left;"/>
        <input type="text" autocomplete="off" name="apartament"
               value="<?php if (isset($_POST['apartament'])) echo $_POST['apartament']; else {
                   echo $__user->getAdress_ap();
               } ?>" id="regflat" class="user-reg-field-three"/>
        <textarea placeholder="comment" name="comment"
                  style="width: 100%; height:73px; margin-top: 10px; border: 1px solid #CCC;"><?php if (isset($_POST['comment'])) echo $_POST['comment']; else 'коментар или данни за фактура'; ?></textarea>

        <div style="clear:both;"></div>
        <div id="legal-form" style="margin-top: 40px;margin-bottom: 20px;">
            <div class="reg-field-left"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">company name<span
                        class="need">*</span></div>
            <div class="reg-field-right"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">f.l.p<span
                        class="need">*</span></div>
            <input type="text" autocomplete="off" name="company_name"
                   value="<?php if (isset($_POST['company_name'])) echo $_POST['company_name']; else {
                       echo $__user->getCompanyName();
                   } ?>" class="reg-field-left"/>
            <input type="text" autocomplete="off" name="company_mol"
                   value="<?php if (isset($_POST['company_mol'])) echo $_POST['company_mol']; else {
                       echo $__user->getCompanyMol();
                   } ?>" class="reg-field-right"/>
            <div class="reg-field-left"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">bulstat<span
                        class="need">*</span></div>
            <div class="reg-field-right"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">vat<span
                        class="need">*</span></div>
            <input type="text" autocomplete="off" name="company_eik"
                   value="<?php if (isset($_POST['company_eik'])) echo $_POST['company_eik']; else {
                       echo $__user->getCompanyEik();
                   } ?>" class="reg-field-left">
            <input type="text" autocomplete="off" name="company_vat"
                   value="<?php if (isset($_POST['company_vat'])) echo $_POST['company_vat']; else {
                       echo $__user->getCompanyVat();
                   } ?>" class="reg-field-right">
            <div class="reg-field-left"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">city<span
                        class="need">*</span></div>
            <div class="reg-field-right"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">phone
            </div>
            <input type="text" autocomplete="off" name="company_city"
                   value="<?php if (isset($_POST['company_city'])) echo $_POST['company_city']; else {
                       echo $__user->getCompanyCity();
                   } ?>" class="reg-field-left"/>
            <input type="text" autocomplete="off" name="company_phone"
                   value="<?php if (isset($_POST['company_phone'])) echo $_POST['company_phone']; else {
                       echo $__user->getCompanyPhone();
                   } ?>" class="reg-field-right"/>
            <div class="reg-field-left"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">street<span
                        class="need">*</span></div>
            <div class="reg-field-right"
                 style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">number
            </div>
            <input type="text" autocomplete="off" name="company_street"
                   value="<?php if (isset($_POST['company_street'])) echo $_POST['company_street']; else {
                       echo $__user->getCompanyStreet();
                   } ?>" class="reg-field-left">
            <input type="text" autocomplete="off" name="company_number"
                   value="<?php if (isset($_POST['company_number'])) echo $_POST['company_number']; else {
                       echo $__user->getCompanyNumber();
                   } ?>" class="reg-field-right">
            <div style="clear:both;"></div>
        </div>
        <?php
        $sint = rand(2000, 9999);
        $scode = sha1("18g" . $sint . $__user->getName());

        $allcost = 0;
        $allPreorder = 0;
        if (isset($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $key => $val) {
                if ($_SESSION['cart'][$key]) {
                    $artikul = new artikul((int)$key);
                    if ($artikul->isPreorder()) {
                        $allPreorder += $_SESSION['cart'][$key]['preorder']['deposit_price'] * $_SESSION['cart'][$key]['count'];
                    } else {
                        $allPreorder += $_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count'];
                    }

                    $allcost += $_SESSION['cart'][$key]['price'] * $_SESSION['cart'][$key]['count'];
                } else {
                    $group = new paket((int)$key);
                    $allcost += $group->getPrice() * $_SESSION['cart'][$key]['count'];
                    $allPreorder += $group->getPrice() * $_SESSION['cart'][$key]['count'];
                }
            }
        }
        ?>
        <input type="hidden" id="scode" value="<?php echo $scode; ?>">
        <input type="hidden" id="ajax_cost" value="<?php echo $allPreorder / CURRENCY_RATE; ?>">
        <input type="hidden" name="buy" value="buy"/>
    </form>
    <div id="paypal-method">
        <form action="https://www.paypal.com/us/cgi-bin/webscr" method="post" target="_blank" id="form-paypal">
            <input type="hidden" name="cmd" value="_cart">
            <input type="hidden" name="business" value="todor@18gshop.com">
            <input type="hidden" name="upload" value="1">
            <input type="hidden" name="first_name" value="<?php echo $__user->getName(); ?>">
            <input type="hidden" name="last_name" value="<?php echo $__user->getName_last(); ?>">
            <input type="hidden" name="currency_code" value="EUR">
            <input type="hidden" name="cancel_return" value="<?php echo url; ?>">
            <input type="hidden" name="return" value="<?php echo url . 'order_details_paypal.php?s=' . $scode; ?>">
            <div id="shipping_js">
                <?php
                if ($dostavka_free > $allcost) {
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $dostavka_cena += $_SESSION['cart']['total_additional_delivery'];
                    }
                    ?>
                    <input type="hidden" name="shipping_1"
                           value="<?php echo number_format($dostavka_cena / CURRENCY_RATE, 2, '.', ''); ?>">
                    <?php
                } else {
                    if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                        $delivery = $_SESSION['cart']['total_additional_delivery'];
                        ?>
                        <input type="hidden" name="shipping_1"
                               value="<?php echo number_format($delivery / CURRENCY_RATE, 2, '.', ''); ?>">
                        <?php
                    }
                }
                ?>
            </div>
            <?php
            echo '<input type="hidden" name="upload" value="1">';

            if (isset($_SESSION['cart'])) {
                $n = 1;
                foreach ($_SESSION['cart'] as $key => $val) {
                    if ($key == "total_additional_delivery") continue;

                    if ($_SESSION['cart'][$key]['type'] == "product") {
                        $artikul = new artikul((int)$key);
                        $option = '';
                        if (isset($_SESSION['cart'][$key]['options'])) {
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0) {
                                        $artikulid = (int)$vall;
                                    }
                                    if ($keyy == 1 && $artikulid == $key) {
                                        $option = $vall;
                                    }
                                }
                            }
                        }
                        echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($artikul->getIme_marka()) . ' ' . stripslashes($artikul->getIme()) . ' size ' . $option . '">';
                        echo '<input type="hidden" name="item_number_' . $n . '" value="' . $artikul->getId() . '">';
                        echo '<input type="hidden" name="quantity_' . $n . '" value="' . (int)$_SESSION['cart'][$key]['count'] . '">';
                        echo '<input type="hidden" name="amount_' . $n . '" value="' . number_format((($artikul->getCena_promo() == 0) ? $artikul->getCena() / CURRENCY_RATE : $artikul->getCena_promo() / CURRENCY_RATE) * ((int)$_SESSION['cart'][$key]['count']), 2, '.', '') . '">';

                        $n++;

                        $dop_artikuli = $artikul->getAddArtikuli();
                        if ($dop_artikuli != "") {
                            $dop_art_arr = explode(",", $dop_artikuli);
                            foreach ($dop_art_arr as $dart) {
                                $dr = new artikul((int)$dart);
                                $option = '';
                                $a = 0;
                                foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                    $expl_arr = explode(",", $optionVal);
                                    foreach ($expl_arr as $keyy => $vall) {
                                        if ($keyy == 0 && $vall == $dr->getId()) {
                                            $artikulid = (int)$vall;
                                        }
                                        if ($keyy == 1 && $artikulid == $dr->getId() && $a == 0) {
                                            $option = $vall;
                                            $a = 1;
                                        }
                                    }
                                }
                                echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($dr->getIme_marka()) . ' ' . stripslashes($dr->getIme()) . ' size ' . $option . '">';
                                echo '<input type="hidden" name="item_number_' . $n . '" value="' . $dr->getId() . '">';
                                echo '<input type="hidden" name="amount_' . $n . '" value="0">';
                                $n++;
                            }
                        }
                    } else {
                        $group = new paket((int)$key);

                        echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($group->getName()) . '">';
                        echo '<input type="hidden" name="item_number_' . $n . '" value="' . $group->getId() . '">';
                        echo '<input type="hidden" name="quantity_' . $n . '" value="' . (int)$_SESSION['cart'][$key]['count'] . '">';
                        echo '<input type="hidden" name="amount_' . $n . '" value="' . number_format(($group->getPrice() / CURRENCY_RATE) * ((int)$_SESSION['cart'][$key]['count']), 2, '.', '') . '">';

                        $n++;
                        $artikuli = $group->getProducts();
                        foreach ($artikuli as $dart) {
                            $dr = new artikul((int)$dart);
                            $option = '';
                            $a = 0;
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0 && $vall == $dr->getId()) {
                                        $artikulid = (int)$vall;
                                    }
                                    if ($keyy == 1 && $artikulid == $dr->getId() && $a == 0) {
                                        $option = $vall;
                                        $a = 1;
                                    }
                                }
                            }
                            echo '<input type="hidden" name="item_name_' . $n . '" value="' . stripslashes($dr->getIme_marka()) . ' ' . stripslashes($dr->getIme()) . ' size ' . $option . '">';
                            echo '<input type="hidden" name="item_number_' . $n . '" value="' . $dr->getId() . '">';
                            echo '<input type="hidden" name="amount_' . $n . '" value="0">';
                            $n++;
                        }
                    }
                }
            }
            ?>
            <div style="margin-top:30px; margin-left: 10px;">
                <div id="button">
                    <input style="border: none; color: #fff;" type="submit" name="btnSubmit"
                           value="checkout"
                           class="button" id="btn-paypal">
                    <div id="form-loader"
                         style=" width: 300px; height: 35px; line-height: 32px; background: #FF671F url(images/ajax-loader.gif) no-repeat center center; display: none;"></div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </form>
    </div>
    <?php
    $tmp_msg = '';
    if ((isset($user_register_error)) && ($user_register_error)) foreach ($user_register_error as $v) {
        $tmp_msg = $tmp_msg . ', ' . $v;
    } ?>
    <?php if ((isset($order_address_data_error)) && ($order_address_data_error)) foreach ($order_address_data_error as $v) {
        $tmp_msg = $tmp_msg . ', ' . $v;
    }
    $tmp_msg = trim($tmp_msg, ', ');
    if (!empty($tmp_msg)) {
        echo '<div class="error">';
        echo $tmp_msg;
        echo '</div>';
    }
    ?>
</div>
