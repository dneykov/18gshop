<?php
require_once('klasove/Mobile_Detect.php');
$detect = new Mobile_Detect();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="<?php echo url; ?>images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo url; ?>images/favicon.ico" type="image/x-icon">
    <meta id="viewport" name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" type="text/css" href="<?php echo url_template_folder; ?>css/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url; ?>css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo url; ?>css/slick-theme.css">
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo url_template_folder; ?>js/jquery.watermark.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-ui-1.12.1.min.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo url_template_folder; ?>js/fancyBox/jquery.fancybox.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo url_template_folder; ?>js/fancyBox/jquery.fancybox.css?v=2.0.3" media="screen"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-buttons.css?v=2.0.3"/>
    <script type="text/javascript"
            src="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-buttons.js?v=2.0.3"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-thumbs.css?v=2.0.3"/>
    <script type="text/javascript"
            src="<?php echo url_template_folder; ?>js/fancyBox/helpers/jquery.fancybox-thumbs.js?v=2.0.3"></script>
    <script type="text/javascript" src="<?php echo url_template_folder; ?>js/script.js"></script>
    <script type="text/javascript" src="<?php echo url; ?>js/functions.js"></script>
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.1/cookieconsent.min.css"/>
    <link type="text/css" href="<?php echo url; ?>css/jquery-ui-1.12.1.min.css" rel="stylesheet"/>
    
    <?php
    if (isset($artikul)) {
        echo '<link rel="image_src" href="' . url . $artikul->getKartinka_t() . '" />';
    }
    ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2221202-12"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-2221202-12');
    </script>
    <title>18gshop


        <?php
        if (isset($page_title)) echo $page_title;
        if (isset($_GET['help'])) {
            echo " - INFORMATION";
            if (isset($_GET['c'])) {
                $stm = $pdo->prepare("SELECT * FROM `help_groups` WHERE `id` = ? LIMIT 1");
                $stm->bindValue(1, (int)$_GET['c'], PDO::PARAM_INT);
                $stm->execute();
                $cat = $stm->fetch();
                echo " - " . $cat['name_bg'];
            }
            if (isset($_GET['t'])) {
                $stm = $pdo->prepare("SELECT * FROM `help_tags` WHERE `id` = ? LIMIT 1");
                $stm->bindValue(1, (int)$_GET['t'], PDO::PARAM_INT);
                $stm->execute();
                $t = $stm->fetch();
                echo " - " . $t['name_bg'];
            }
        } else if (isset($_GET['products'])) {
            echo " - products";
        } else if (isset($_GET['termsofuse'])) {
            echo " - terms of use";
        } else if (isset($_GET['howtobuy'])) {
            echo " - how to buy";
        }
        if (isset($page_title) && $page_title == "") {
            echo " - ";
            $n = 1;
            foreach ($all_kategorii as $tmp_kategoriq) {
                if ($n > 1) echo ", ";
                echo $tmp_kategoriq->getIme();
                $n++;
            }
        }
        ?>

    </title>
    <script>
        $(document).ready(function () {
            $('.pricelist').fancybox({
                'type': 'iframe',
                'padding': 0,
                'width': 480,
                'height': 500,
                'fitToView': false,
                'autoSize': false
            });
        });
    </script>
</head>
<body>
<header>
    <div class="lang-icon"><img onclick="showLanguageDialog();"
                                src="<?php echo url_template_folder; ?>images/icons/lang.png"/></div>
    <h1 id="logo"><a href="<?php echo urlm; ?>"><img src="<?php echo url_template_folder; ?>images/Logo.png" alt="18gshop"></a>
    </h1>
    <div class="right_header">
        <div class="login_panel">
            <?php
            if ($__user) {
                echo '<a  href="' . urlm . 'index.php?user" title="' . $__user->getName() . '"><img src="' . url_template_folder . 'images/icons/avatar.png" alt="' . $__user->getName() . '"/></a>';
            } else {
                echo '<a  href="' . urlm . 'index.php?user&login"><img src="' . url_template_folder . 'images/icons/avatar.png" alt="профил"/></a>';
            }
            ?>
        </div>
        <?php if ($__user && $__user->is_partner() && $detect->isTablet()) : ?>
            <div class="help" style="margin-right: 15px;">
                <a href="<?php echo urlm; ?>index.php?contacts" title="contacts"><img
                            src="<?php echo url_template_folder; ?>images/icons/phone_m.png"
                            alt="contacts"/></a>
            </div>
            <div class="help">
                <a href="<?php echo urlm; ?>pricelist.php" class="pricelist" title="ценови листи">price lists</a>
            </div>
        <?php else : ?>
            <div class="help">
                <a href="<?php echo urlm; ?>index.php?contacts" title="contacts"><img
                            src="<?php echo url_template_folder; ?>images/icons/phone_m.png"
                            alt="contacts"/></a>
            </div>
        <?php endif; ?>
        <h1 id="card"><a class="cart" href="fancy_login_m.php?p=cart"><img
                        src="<?php echo url_template_folder; ?>images/icons/cart.png" alt="#" style="height: 20px;"></a>
        </h1>
        <span id="in_card">
                <?php
                if (isset($_SESSION['cart'])) {
                    $count = 0;
                    foreach ($_SESSION['cart'] as $key => $val) {
                        $count += $_SESSION['cart'][$key]['count'];
                    }
                    echo $count ? $count : '0';
                } else {
                    echo '0';
                }
                ?>
        </span>
    </div>
</header>
<div class="searchdiv">
    <a href="index.php?products">
        <div class="responsive-icon"></div>
    </a>
    <div style="position:absolute;height:0px; overflow:hidden; ">
        Username <input type="text" name="fake_safari_username">
    </div>
    <input id="text" name="text" onClick="this.value='';" class="search" onkeyup="search_ajax_mobile(this.value);"
           value="" autocomplete="off"/>
    <input type="button" id="close-search" onclick="search_ajax_close('#suggestions')" value="X">
</div>
<div style="padding-left: 5px; padding-right: 5px;">
    <div class="autocomplate" id="suggestions"></div>
</div>
