<div id="news_wrapper">
<?php
	if(isset($_GET['newsid'])){
		$active_news=$_GET['newsid'];
	}else{		
		$active_news=$news[0]->getID();
	}
	foreach($news as $n){
                $href = $n->getUrl();
                $tittle = $n->getTitle();
		echo '<div class="the_new_box">';
			$date = new DateTime($n->getDate());
                        echo '<div class="the_new_date" style="float: left; color: #888; margin-left: 5px;">'.date_format($date, 'd-m-Y').'</div>';
                        echo '<div style="clear:both; height: 10px;"></div>';
			echo '<div class="the_new_big_img" style="margin-bottom: 10px;">';
                        if($href != ''){
                            echo '<a TARGET="_blank" href="'.$n->getUrl().'" >';
                        }
                        echo '<img src="'.url.$n->getImage().'" class="thumbnail" alt="">';
                        if($href != ''){
                            echo '</a>';
                        }
			echo '</div>';
			echo '<div class="the_new_name_date">';
                        if($tittle != ''){  
                            echo	'<div class="the_new_name" style="margin-bottom: 10px;">';
                            if($href != ''){
                                echo '<a TARGET="_blank" href="'.$n->getUrl().'" style="color: #333;">';
                            }
                            echo '<strong>'.$n->getTitle().'</strong>';
                            if($href != ''){
                                echo '</a>';
                            }
                            echo '</div>';
                        }
			echo '<div style="clear: both;"></div>
				 </div>';
			echo '<div style="padding: 0 10px 0 10px;"><div class="the_new_text">'.$n->getText().'</div></div>';
		echo '</div>';
                echo '<div style="clear:both; height: 30px; border-bottom: 1px solid #CCC;"></div>';
                echo '<div style="clear:both; height: 30px;"></div>';
	}	
	?>
</div>