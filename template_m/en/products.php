<div id="categories_wrapper" style="margin-top: 10px; margin-bottom: 10px;">
    <?php
    if (isset($kategorii)&&(sizeof($kategorii)>0)){
        foreach($kategorii as $category){
            echo '<a class="category_box" style="color: #333;" onclick="return false;" onmousedown="javascript: toggleCont(\''.$category->getId().'\')">';
            echo '<div class="label">'.$category->getIme().'</div></a>';
            $vidove = $category->getVidove();
            echo '<div class="category_drop_box" style="background-color: 0071b9;" id="'.$category->getId().'">';
            echo '<div style="clear: both; height: 5px;"></div>';
            if(isset($vidove)&&(sizeof($vidove)>0)){
                foreach($vidove as $vid) {
                    echo '<a href="index.php?category='.$category->getId().'&mode='.$vid->getId().'" class="category_drop_item">'.$vid->getIme().'</a>';
                }
            }
            if($category->getBundleCount() > 0){
                echo '<a style="color: #FF671F;" href="index.php?bundles&category='.$category->getId().'" class="category_drop_item">bundles</a>';
            }
            echo '<div style="clear: both; height: 5px;"></div>';
            echo '</div>';
        }
    }
    $numberPromo = 0;
    $numberBundle = 0;
    if(isset($all_kategorii)&&sizeof($all_kategorii)>0) {
        foreach ($all_kategorii as $tmp_kategoriq) {
            $numberPromo += $tmp_kategoriq->getBroi_artikuli(null, true);
            $numberBundle += $tmp_kategoriq->getBundleCount();
        }
    }

    if($numberPromo > 0) {
        echo '<a class="category_box" style="color: #FF671F;" href="index.php?hotOffers">';
        echo '<div class="label">DISCOUNTS</div></a>';
    }
    if($numberBundle > 0) {
        echo '<a class="category_box" style="color: #FF671F;" href="index.php?bundles">';
        echo '<div class="label">BUNDLES</div></a>';
    }
    ?>
</div>