<?php

$stm = $pdo->prepare('SELECT * FROM `zz_regioni_magazini` LIMIT 1');
$stm -> execute();

$v=$stm->fetch();

if($_POST){
	if(isset($_POST['content'])&&($_POST['content']!='Message')){
		$content=trim($_POST['content']);
	}else{
		$content='';
	}
	if(isset($_POST['name'])&&($_POST['name']!='Name')){
		$name=trim($_POST['name']);
	}else{
		$name='';
	}
	if(isset($_POST['email'])&&($_POST['email']!='Email or Phone')&&(check_email_address($_POST['email']))){
		$email=trim($_POST['email']);
	}else{
		$email='';
	}
	if(empty($content)) $err[]='Message';
	if(empty($name)) $err[]='First Name';
	if(empty($email)) $err[]='E-mail';
	if($err===NULL){		
		$msg="Запитване от ".$name." ".$email."<br/>";
		$msg=$msg."Въпрос: ".htmlspecialchars($_POST['content']);
		
		
		$mail = new PHPMailer();
		$mail->Host = "localhost";  // specify main and backup server
		$mail->CharSet="utf-8"; 

		$mail->From = mail_office;
		$mail->FromName = $name;
		$mail->AddAddress(mail_office);		
		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		$mail->IsHTML(TRUE);                                  // set email format to HTML

		$mail->Subject = "Запитване от ".$name;
		$mail->Body    = $msg;
		
		
		$sendRez=$mail->Send();
	}
}

?>
	<div class="info-contacts-resize" style="min-height: 519px; overflow: hidden; box-sizing: border-box; padding-left: 10px; padding-right: 10px; margin: 15px auto;">
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo GOOGLE_MAPS_API_KEY; ?>"> </script>
		<script type="text/javascript">
			$(document).ready(function() {
				var geocoder = new google.maps.Geocoder();
				var center = new google.maps.LatLngBounds();
					geocoder.geocode({'address':'<?php echo $v['googleaddress']; ?>'},function(res,stat){
						if(stat==google.maps.GeocoderStatus.OK){
							// add the point to the LatLngBounds to get center point, add point to markers
							center.extend(res[0].geometry.location);
							var mapOptions = {
							  center: center.getCenter(),
							  zoom: 15,
							  mapTypeId: google.maps.MapTypeId.ROADMAP
							};
							var map = new google.maps.Map(document.getElementById("shop_<?php echo $v['id']; ?>"),
								mapOptions);
							var marker = new google.maps.Marker({
								position: center.getCenter(),
								map: map,
								icon: '<?php echo url; ?>/images/logo_gm.png'
							});
						}
					});


			});
		</script>
		<?php
			echo '<div class="map-google" style="float: left;overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);"  id="shop_'.$v['id'].'"></div>';
		?>

		<div class="contact-big-col">
		<?php

			//echo '<img style="width:813px;height:500px;margin:0px" src="http://maps.google.com/maps/api/staticmap?center='.$v['googleaddress'].'&zoom=14&size=813x500&maptype=roadmap&markers=color:blue|label:A|'.$v['googleaddress'].'&sensor=false"/>';
			echo '<div style="text-align: left; float:right; width:100%;white-space:pre-wrap;"><div style="text-align: left; float:right; width:100%;">'.nl2br(htmlspecialchars($v['address_'.lang_prefix])).'</div>';
			echo '<div style="text-align: left; float:right; width:100%;">'.$v['info_'.lang_prefix].'</div></div>';

		?>

		<script type="text/javascript">
			$(document).ready(function() {
				   $('#mailform input[name=name]').click(function() {
						$(this).val('');
					});
					$('#mailform input[name=email]').click(function() {
						$(this).val('');
					});
					$('#mailform input[name=subject]').click(function() {
						$(this).val('');
					});
					$('#mailform textarea').click(function() {
						$(this).text('');
					});
			});
		</script>
				<form id="mailform" name="mailform" action="" method="post" style="float:right;margin-top: 15px;margin-bottom:15px;clear:right;width: 100%;">
					<table style="width: 100%;">
						<tr>
							<td><input name="name" type="text" <?php if(isset($_POST['name'])){ echo 'value="'.$_POST['name'].'"';} else {if(isset($__user)){ echo 'value="'.$__user->getName().' '.$__user->getName_last().'"';}} ?> placeholder="Name" style="width: 97%" class="reg-field-left" /></td>
							<td align="right"><input name="email" type="text" <?php if(isset($_POST['email'])){ echo 'value="'.$_POST['email'].'"';} else{ if(isset($__user)){ echo 'value="'.$__user->getMail().'"';}}?> placeholder="E-mail" style="width: 97%" class="reg-field-right" /></td>
						</tr>
						<tr>
							<td colspan="2"><textarea name="content" style="width:100%;height:150px;box-sizing: border-box;padding:0 0 0 3px;margin:5px 0;font-size: 16px;border: 1px solid #CCC;color: #808080;" placeholder="Message"><?php if(isset($_POST['content'])) { echo $_POST['content'];}?></textarea></td>
						</tr>
					</table>
					<input value="send" type="submit" style="display: block;cursor: pointer;background:#FF671F;color:#fff;width: 206px;height: 36px;padding-bottom:3px;float:right;margin-top:5px; border: none;" />
				</form>
				<?php
					if((isset($sendRez))&&($sendRez)){
						echo '<div style="text-align: right;margin-right:10px;color:#FF671F; clear: both;font-size: 12px;">Question was sent successfully.</div>';
					}
					if((isset($err))&&(sizeof($err)>0)){
						$t='';
						foreach($err as $e){
							$t.=', '.$e	;

						}
						$t=trim($t,', ');
						echo '<div style="text-align: right;margin-right:10px;color:#FF671F; clear: both;font-size: 12px;">Please Enter '.$t.'</div>';
					}
				?>
		</div>
		<div style="clear:both;"></div>
	</div>