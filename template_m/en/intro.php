<style>
    .adv-text a {
        color: inherit;
        font-size: inherit;
        font-family: inherit;
    }
</style>
<?php

//Load advertises intro banners
$intro_banners = NULL;
$stm = $pdo->prepare('SELECT * FROM `zz_banners` where `podredba`=10 order by `id` DESC');
$stm->execute();

foreach ($stm->fetchAll() as $adv) {
    $temp_advertise = new advertise((int)$adv['id']);
    $intro_banners[] = $temp_advertise;
    unset($temp_advertise);
    unset($adv);
}
//End Load advertises intro banners
?>
<div class="big-banners" style="margin-bottom: 10px;">
    <div class="banners-slider">
        <?php
        if (($intro_banners) && (sizeof($intro_banners) > 0)) {
            echo '<div id="slider" style="margin: 0;">';
            $n = 1;
            foreach ($intro_banners as $intro_baner) {
                if ($intro_baner->isOnlyPartners()) {
                    if (!isset($__user)) continue;

                    if (isset($__user) && !$__user->is_partner()) continue;
                }

                echo '<li class="panel" style="width: 100% !important;"><div style="position:relative;">';
                $adv_texts = unserialize($intro_baner->getTexts());

                echo '<img class="small-banner-img bnr-big-' . $n . '" style="max-width:1000px;width:100%;margin:0px;" src="' . url . $intro_baner->getImage_b() . '" />';
                echo '<img class="big-banner-img bnr-big-' . $n . '" style="width:100%;margin:0px;" src="' . url . $intro_baner->getImage() . '" />';
                echo '<a class="banner_item" style="width:100%; height: 100%; position: absolute; top: 0; left: 0;margin:0px; z-index: 2"';
                if ($intro_baner->getDistinationURL() != '') echo ' href="' . $intro_baner->getDistinationURL();
                echo '"></a>';
                if (count($adv_texts) && is_array($adv_texts)) {
                    foreach ($adv_texts as $text) {
                        if ($text['content'] != "") {
                            list($r, $g, $b) = sscanf($text['bgcolor'], "#%02x%02x%02x");

                            echo '<div class="adv-text adv-place-' . $text['place'] . '" style="background-color: rgba(' . $r . ', ' . $g . ', ' . $b . ', 1';
                            echo '); color: ' . $text['color'] . ';">';
                            echo strip_tags($text['content'], "<a>");
                            echo '</div>';
                        }
                    }
                }
                echo '</div></li>';
                $n++;
            }
            echo '</ul></div>';

        }
        ?>
    </div>
</div>
<?php

if (isset($_COOKIE["CatSeen"])) {
    $broi = 6;
    $catSeen = array();
    $catSeen = unserialize(stripslashes(rawurldecode($_COOKIE["CatSeen"])));
    arsort($catSeen);
    $n = count($catSeen);
    $loops = 0;
    $kat_broi = 0;
    switch ($n) {
        case 1:
            $loops = 1;
            $kat_broi = $broi;
            break;
        case 2:
            $loops = 2;
            $kat_broi = (int)($broi / 2);
            break;
        case 3:
            $loops = 3;
            $kat_broi = (int)($broi / 3);
            break;
        default:
            $loops = 3;
            $kat_broi = (int)($broi / 3);
            break;
    }
}

if (isset($catSeen)) {
    $fn = 0;
    $rezultati = 0;
    $done = false;
    $products = null;
    foreach ($catSeen as $key => $value) {
        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `id_vid` = ? AND `IsOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 20');
        $stm->bindValue(1, (int)$key, PDO::PARAM_INT);
        $stm->execute();
        $rez = $stm->fetchAll();
        shuffle($rez);
        if ($fn == 0) {
            if ($loops * $kat_broi != $broi) {
                $rezane = $broi - ($loops * $kat_broi);
                $rez = array_slice($rez, 0, $kat_broi + $rezane);
            } else {
                $rez = array_slice($rez, 0, $kat_broi);
            }
        } else {
            $rez = array_slice($rez, 0, $kat_broi);
        }
        foreach ($rez as $n) {
            $products[] = new artikul($n);
            $rezultati++;
            if ($rezultati == $broi) {
                $done = true;
                break;
            }
        }
        $fn++;
        if ($done) break;
        if ($fn == $loops) break;
    }
    if (count($products) == 0) return;
    shuffle($products);
} else {
    $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `IsOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 36');
    $stm->execute();
    $rez = $stm->fetchAll();
    shuffle($rez);
    $rez = array_slice($rez, 0, 6);
    $products = null;
    foreach ($rez as $n) {
        $products[] = new artikul($n);
    }
}
//echo("<div id='info_tab' style='background-color: #333; font-size: 13px; text-transform: uppercase;'><p style='margin-left: 5px;'>preferred</p></div>");
echo "<div id='content' style='margin-top: 10px; margin-bottom: 10px;'><div id='articuli'>";
foreach ($products as $p) {
    echo "<div class='articul' style='margin-top: 7px;'>
					<a href='index.php?id=" . $p->getId() . "' style='color: #333;'>"; ?>
    <div class="promo">
        <?php
        if (($p->getCena_promo()) || ($p->getAddArtikuli() != "")) {
            $tmpraz = ($p->getCena() - $p->getCena_promo());
            if ($p->IsNew()) {
                echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; right: 20px; margin-left: 5px;">
                                <span class="rotate-90">' . '-' . round((($tmpraz / $p->getCena()) * 100), 0) . '%' . '</span>
                                </div>';
            } else {
                echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                <span class="rotate-90">' . '-' . round((($tmpraz / $p->getCena()) * 100), 0) . '%' . '</span>
                                </div>';
            }
        }

        if ($p->isPreorder() != "") {
            echo '<div style="float: left; background-color: #29AC92; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90">PRE</span>
                                    </div>';
        }

        if ($p->IsNew()) {
            echo '<div style="float: left; background-color: #01A0E2; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90" style="font-size: 11px; padding-top: 17px;">NEW</span>
                                    </div>';
        }
        ?>
    </div>
    <?php echo "<div class='articul_img'>
						<table class='imageCenter'><tbody><tr><td><img src='" . url . $p->getKartinka_t() . "'></td></tr></tbody></table>
					</div>
					<div class='articul_info'>
						<div class='articul_info_name'>" . $p->getIme_marka() . " / " . $p->getIme() . "</div>
				"; ?>
    <div class="proddiv-specification">
        <?php
        $linesSpecification = explode(PHP_EOL, $p->getSpecification());
        $newLinesSpecification = implode(PHP_EOL, array_slice($linesSpecification, 0, 2)) . PHP_EOL;
        if (count($newLinesSpecification) > 1) {
            echo nl2br($newLinesSpecification);
        }
        ?>
    </div>
    <?php if (isset($__user) && $__user->is_partner()) : ?>
        <?php if ($p->getCenaDostavna() > 0) : ?>
            <div class="articul_info_price"><span
                        style="margin-top:8px; font-size:12pt; font-style: italic; "><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCenaDostavna() / CURRENCY_RATE, 2, '.', ''));
                    echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
        <?php else : ?>
            <?php if ($p->getCena_promo()) : ?>
                <?php if ($p->isPreorder()) : ?>
                    <div class="articul_info_price"><span
                                style="font-size:12pt;font-style: italic;color:#FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                    <div class="articul_info_price" style="float: right; margin-right: 10px;"><span
                                style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:9pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                <?php else : ?>
                    <div class="articul_info_price"><span
                                style="font-size:12pt;font-style: italic;color:#fe1916;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                    <div class="articul_info_price" style="float: right; margin-right: 10px;"><span
                                style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:9pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                <?php endif; ?>
            <?php else : ?>
                <?php if ($p->isPreorder()) : ?>
                    <div class="articul_info_price"><span
                                style="margin-top:8px; font-size:12pt; font-style: italic;  color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                <?php else : ?>
                    <div class="articul_info_price"><span
                                style="margin-top:8px; font-size:12pt; font-style: italic; "><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php else : ?>
        <?php if ($p->getCena_promo()) : ?>
            <?php if ($p->isPreorder()) : ?>
                <div class="articul_info_price"><span
                            style="font-size:12pt;font-style: italic;color:#FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                <div class="articul_info_price" style="float: right; margin-right: 10px;"><span
                            style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:9pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
            <?php else : ?>
                <div class="articul_info_price"><span
                            style="font-size:12pt;font-style: italic;color:#fe1916;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                <div class="articul_info_price" style="float: right; margin-right: 10px;"><span
                            style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:9pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
            <?php endif; ?>
        <?php else : ?>
            <?php if ($p->isPreorder()) : ?>
                <div class="articul_info_price"><span
                            style="margin-top:8px; font-size:12pt; font-style: italic;  color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
            <?php else : ?>
                <div class="articul_info_price"><span
                            style="margin-top:8px; font-size:12pt; font-style: italic; "><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($p->getCena() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
    <?php
    echo("</div></a></div>");
}
echo("</div></div><div style='clear: both;'></div>");

if (isset($catSeen)) {
    $fn = 0;
    $rezultati = 0;
    $done = false;
    $products = null;
    $ids = array();
    foreach ($catSeen as $key => $value) {
        $stm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND `id_vid` = ? AND `IsOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 20');
        $stm->bindValue(1, (int)$key, PDO::PARAM_INT);
        $stm->execute();
        $rez = $stm->fetchAll();
        shuffle($rez);
        if ($fn == 0) {
            if ($loops * $kat_broi != $broi) {
                $rezane = $broi - ($loops * $kat_broi);
                $rez = array_slice($rez, 0, $kat_broi + $rezane);
            } else {
                $rez = array_slice($rez, 0, $kat_broi);
            }
        } else {
            $rez = array_slice($rez, 0, $kat_broi);
        }
        foreach ($rez as $n) {
            $products[] = new artikul($n);
            $rezultati++;
            $ids[] = $n;
            if ($rezultati == $broi) {
                $done = true;
                break;
            }
        }
        $fn++;
        if ($done) break;
        if ($fn == $loops) break;
    }
    if ($rezultati != $broi) {
        $stmm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND `IsOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 50');
        $stmm->execute();
        $rez = $stmm->fetchAll();
        shuffle($rez);
        foreach ($rez as $n) {
            if (!in_array($n, $ids)) {
                $products[] = new artikul($n);
                $rezultati++;
            }
            if ($rezultati == $broi) {
                break;
            }
        }
    }
    if (count($products) == 0) return;
    shuffle($products);
} else {
    $stmm = $pdo->prepare('SELECT * FROM `artikuli` WHERE `cena_promo` > 0 AND `IsOnline` = 1 AND `isAvaliable` = 1 ORDER BY id DESC LIMIT 36');
    $stmm->execute();
    $rez = $stmm->fetchAll();
    shuffle($rez);
    $rez = array_slice($rez, 0, 6);
    $products = null;
    foreach ($rez as $n) {
        $products[] = new artikul($n);
    }
}
echo("</div></div><div style='clear: both;'></div>");
?>
