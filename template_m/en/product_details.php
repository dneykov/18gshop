<?php
$isAvaliable = $artikul->isAvaliable();
$isOnline = $artikul->isOnline();

$stm = $pdo->prepare("SELECT * FROM `dict` WHERE `key` = :preorder LIMIT 1");
$stm->bindValue(':preorder', "preorder_" . lang_prefix, PDO::PARAM_STR);
$stm->execute();
$preOrderDb = $stm->fetch();
$preOrderConfirmationDescriptions = $preOrderDb['value'];

?>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.serialScroll-1.2.1.js"></script>
<script type="text/javascript">
    var addToCartRules = [];

    function validate(rules, preorder) {
        for (x in rules) {
            if ($("#" + rules[x][0]).val() == null || $("#" + rules[x][0]).val() == "") {
                alert('Please select ' + rules[x][2] + ' for ' + rules[x][1]);
                return false;
            }
        }

        if (preorder) {
            showPreorderConfirmation();
            return false;
        } else {
            return true;
        }
    }

    function updatePriceBySize(el) {
        var additional_price = 0;
        var option = $(el).attr("id").split("_");
        var productID = option[1];
        var tagID = option[3];
        if (typeof tagID === "undefined") {
            tagID = $("#price_tag").val();
        }

        $(".upgrade_option_prices").each(function (index, el) {
            additional_price += parseFloat($(el).val()) || 0;
        });

        $.ajax({
            type: "POST",
            url: "ajax/m_get_price_by_size_en.php",
            data: {
                product_id: productID,
                tag_id: tagID,
                additional_price: additional_price
            },
            cache: false,
            success: function (html) {
                var result = jQuery.parseJSON(html);
                $("#item_price").html(result.price);
                $(".delivery-box").html(result.delivery);
                if (parseInt(result.quantity) > 0) {
                    $(".quantity .count").html(result.quantity);
                    $(".quantity").css({"visibility":"visible"});
                } else {
                    $(".quantity").css({"visibility":"hidden"});
                }
                $('.warehouse-box .warehouse-code').html(result.code);
                $('.warehouse-box').show();
            }
        });
    }

    function showPreorderConfirmation() {
        $("#pre-order-dialog").dialog({
            autoOpen: true,
            title: "Pre-order",
            modal: true,
            width: 'auto',
            resizable: false,
            draggable: false,
            position: {
                my: "center",
                at: "center",
                of: window
            },
            buttons: {
                "Add to cart": function () {
                    $("#addtocart").submit();
                    $(this).dialog("close");
                },
                "Cancel": {
                    click: function () {
                        $(this).dialog("close");
                    },
                    text: 'Cancel',
                    class: 'btn-cancel'

                }
            },
            open: function (event, ui) {
                $('body').addClass('stop-scrolling');
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
            close: function () {
                $(this).dialog("destroy");
                $('body').removeClass('stop-scrolling');
            }
        });

//        return false;
    }
</script>
<div id="where_am_i_box" style="border-bottom: 1px solid #CCC;">
    <div id="where_am_i">
        <?php
        echo '<a onclick="history.back();">';
        echo '<img src="' . url_template_folder . 'images/back.png" alt="back" style="float: left; margin-top: 3px; margin-left: 5px; margin-right: 10px;">';
        echo '</a>';
        if (isset($kategoriq)) {
            echo '<a style="color: #333;" href="' . $__url . '?category=' . $kategoriq->getId() . '';
            if (isset($etiketi_grupi)) {
                foreach ($etiketi_grupi as $v) {
                    if (($v->getEtiketi())) {
                        foreach ($v->getEtiketi() as $e) {
                            if ($etiket && in_array($e->getId(), $etiket)) {
                                echo '&tag[]=' . $e->getId() . '';
                            }
                        }
                    }
                }
            }
            echo '">';
            echo mb_strtolower($kategoriq->getIme(), 'UTF-8') . '</a>';
            if (isset($vid)) {
                echo ' / <a style="color: #333;" href="' . $__url . '?category=' . $kategoriq->getId() . '';
                if (isset($vid)) {
                    echo '&mode=' . $vid->getId() . '';
                }
                if (isset($etiketi_grupi)) {
                    foreach ($etiketi_grupi as $v) {
                        if (($v->getEtiketi())) {
                            foreach ($v->getEtiketi() as $e) {
                                if ($etiket && in_array($e->getId(), $etiket)) {
                                    echo '&tag[]=' . $e->getId() . '';
                                }
                            }
                        }
                    }
                }
                echo '">' . mb_strtolower($vid->getIme(), 'UTF-8') . '</a>';
            }
        }
        ?>
    </div>
</div>
<?php
$brand = $artikul->getBrand();
$t = $artikul->getCena();
if ($artikul->getCena_promo() > 0) {
    $t = $artikul->getCena_promo();
}
$dop_artikuli = $artikul->getAddArtikuli();
?>
<div id="item_name">
    <div style="font-size: 13px; color: #999999; font-weight: normal; margin-bottom: 3px;"><?php echo $artikul->getTitle(); ?>
        &nbsp;<div style="display: inline-block; color:#999;font-size:10px;font-weight: normal">
            #<?php echo $artikul->getId(); ?>
            <span class="warehouse-box" style="<?php if (empty($artikul->getWarehouseCode())) : ?>display: none;<?php endif; ?>">
                / <span class="warehouse-code"><?php echo $artikul->getWarehouseCode(); ?></span>
            </span>
        </div>
    </div>
    <strong><?php echo $artikul->getIme_marka(); ?></strong> <?php echo $artikul->getIme(); ?>
</div>
<?php
if ($dop_artikuli != "") {
    $dop_art_arr = explode(",", $dop_artikuli);
    foreach ($dop_art_arr as $dart) {
        $dr = new artikul((int)$dart);
        if (empty($dr->getId())) continue;
        ?>
        <div class="plus">+</div>
        <div class="dop_item_name"><a href='<?php echo url . "index.php?id=" . $dr->getId(); ?>'
                                      target='_blank'><?php echo $dr->getIme_marka(); ?><?php echo $dr->getIme(); ?></a>
        </div>
        <?php
    }
}
?>
<?php
$img = $artikul->getKatinka();
list($width, $height, $type, $attr) = getimagesize($img);
if ($width > 320) {
    $ImgClass = "class='item_img'";
} else {
    $ImgClass = "class='item_img_small'";
}

?>
<div style="clear: both; height: 20px"></div>
<center><img alt="<?php echo $artikul->getIme_marka() . " " . $artikul->getIme(); ?>"
             src="<?php echo $artikul->getKatinka(); ?>" <?php echo $ImgClass; ?> ></center>
<script type="text/javascript">
    function moreImg() {
        if ($("#item_more_img_box").is(":hidden")) {
            $("#more_img_link").html('hide pictures');
            $("#item_more_img_box").slideDown("fast");
        } else {
            $("#more_img_link").html('more pictures');
            $("#item_more_img_box").hide();
            $("html, body").animate({scrollTop: 0}, "fast");
        }
    };
</script>
<?php
$art_images = $artikul->getKatinka_dopalnitelni();
$has_art_images = false;
if (isset($art_images) && (sizeof($art_images) > 0)) {
    foreach ($art_images as $image) {
        if (isset($image)) $has_art_images = true;
    }
}
if (isset($art_images) && (sizeof($art_images) > 0)) {
    echo '<div id="item_more_img_box">';
    foreach ($art_images as $image) {
        if (!is_null($image)) {
            echo '<img class="item_img" src="' . $image->getKatinka() . '"/>';
        }
    }
    echo '</div>';
}

if ($has_art_images) {
    echo '<div id="item_more_img_link" style=" float: left"><a style="color: #888;" href="#" id="more_img_link" onclick="return false;" onmousedown="javascript: moreImg();">more pictures</a></div>';
}

$manual = $artikul->getManual();
if ((isset($manual)) && (!empty($manual))) {
    echo '<div id="showmanuallink" style="margin-right: 10px; text-align: right;"><a href="#" style="color: #888;" onclick="return false;" onmousedown="javascript: showManual();">size chart</a></div>';
    echo '<div style="text-align: center;"><img id="imgmanual" style="display: none; width:100%; max-width:800px; margin: 0 auto;" src="' . url . $manual . '" alt=""/></div>';
    echo '<div id="hidemanuallink" style="margin-left: 10px; margin-top: 10px; margin-right: 10px; text-align: right; display: none;"><a href="#" style="color: #888;" onclick="return false;" onmousedown="javascript: hideManual();">hide</a></div>';
}
?>
<div id="item_box" style="margin-top: 30px; padding-top: 5px;">
    <div class="specification">
        <?php echo nl2br($artikul->getSpecification()); ?>
    </div>
    <br>
    <div id="item_options">
        <form id="addtocart" action="fancy_login_m.php?p=cart&id=<?php echo $artikul->getID(); ?>" method="post">
            <input type="hidden" name="addToCard" value="<?php echo $artikul->getID(); ?>"/>
            <input type="hidden" name="type" value="product"/>
            <input name="counts" value="1" type="hidden"/>
            <div style="clear: both;"></div>
            <?php
            if ($artikul->isAvaliable()) {
                $artikul_etiket_grup = NULL;
                $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ? ORDER BY `id` ');
                $stm->bindValue(1, $artikul->getId(), PDO::PARAM_INT);
                $stm->execute();
                foreach ($stm->fetchAll() as $v) {
                    $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ORDER BY `id` ');
                    $stm->bindValue(1, $v['id_etiket'], PDO::PARAM_INT);
                    $stm->execute();
                    $temp = new etiket($stm->fetch());
                    $temp_grupa = new etiket_grupa((int)$temp->getId_etiketi_grupa());
                    $artikul_etiket_grup[$temp->getId_etiketi_grupa()] = $temp_grupa;
                    unset($temp);
                    unset($temp_grupa);
                }
                if (isset($artikul_etiket_grup) && (sizeof($artikul_etiket_grup) > 0)) {
                    foreach ($artikul_etiket_grup as $v) {
                        if ($v->getSelect_menu()) {
                            if ($dop_artikuli != "") {
                                echo '<div style="margin-bottom: 10px; color: #808080; font-size: 13px; "><span class="ime_na_opciqta">' . $v->getIme() . '</span> for <span style="font-weight: bold; color: #444;">' . $artikul->getIme() . '</span></div>';
                            } else {
                                echo '<div style="padding-top: 15px; margin-bottom: 10px; color: #808080; font-size: 13px; text-align: right; padding-right: 5px;">' . $v->getIme() . '</div>';
                            }
                            if ($v->getSelect_menu()) {
                                $tmp_br_etiketi = 0;
                                $tmp_first_id = 0;
                                $tmp_first_ime = "";
                                echo '<input type="hidden" name="option' . $artikul->getId() . '" id="option' . $artikul->getId() . '"/>';
                                echo '<input type="hidden" name="price_tag" id="price_tag"/>';
                                echo '<div class="item_razmer" style="float: right;">';
                                if ($v->getEtiketi()) {
                                    foreach ($v->getEtiketi() as $e) {
                                        if ($artikul->getEtiketizapisi()) if (in_array($e->getId(), $artikul->getEtiketi_array_ids())) {
                                            $tmp_br_etiketi++;
                                            $tmp_first_ime = $e->getIme();
                                            $tmp_first_id = $e->getId();
                                            echo '<a id="option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . '" onclick="javascript:$(\'#option' . $artikul->getId() . '\').val(\'' . $e->getIme() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);updatePriceBySize(this);$(\'#price_tag\').val(\'' . $e->getId() . '\');return false;" href="#">' . $e->getIme() . '</a>';
//                                                                                echo '<a id="option_'.$artikul->getId().'_'.$v->getId().'_'.$e->getId().'" onclick="javascript:$(\'#option'.$artikul->getId().'\').val(\''.$e->getIme().'\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);return false;" href="#">'.$e->getIme().'</a>';
                                            if (isset($etiket) && in_array($e->getId(), $etiket)) {
                                                echo '
                                                                                        <script type="text/javascript">
                                                                                                updatePriceBySize( $("#option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . '") );
                                                                                                $(\'#price_tag\').val(\'' . $e->getId() . '\');
                                                                                                $(\'#option' . $artikul->getId() . '\').val(\'' . $e->getIme() . '\');
                                                                                                $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . ').toggleClass(\'active\',true);
                                                                                                $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $e->getId() . ').siblings().toggleClass(\'active\',false);
                                                                                        </script>
                                                                                        ';
                                            }
                                        }
                                    }
                                }
                                echo '</div>';
                                if ($tmp_br_etiketi == 1) {
                                    echo '
                                                        <script type="text/javascript">
                                                                updatePriceBySize( $("#option_'.$artikul->getId().'_'.$v->getId().'_'.$tmp_first_id.'") );
                                                                $(\'#price_tag\').val(\''.$tmp_first_id.'\');
                                                                $(\'#option' . $artikul->getId() . '\').val(\'' . $tmp_first_ime . '\');
                                                                $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $tmp_first_id . ').toggleClass(\'active\',true);
                                                                $(option_' . $artikul->getId() . '_' . $v->getId() . '_' . $tmp_first_id . ').siblings().toggleClass(\'active\',false);
                                                        </script>
                                                        ';
                                }
                                echo '
                                                <script type="text/javascript">
                                                        addToCartRules.push(["option' . $artikul->getId() . '","' . $artikul->getIme() . '","' . $v->getIme() . '"]);
                                                </script>
                                                ';
                            }
                        }
                    }
                }
                foreach ($artikul->getUpgradeGroups() as $upgradeGroup) {
                    if (count($artikul->getUpgradeOptions($upgradeGroup->getId())) > 0) {
                        $defaultUpgradeOption = $artikul->getDefaultUpgradeOptions($upgradeGroup->getId());

                        echo '<div class="upgrades" id="upgrage_group_' . $upgradeGroup->getId() . '">';
                        echo '<input type="hidden" name="upgrade_option[' . $upgradeGroup->getId() . '][upgrade_name]" id="upgrade_name_' . $upgradeGroup->getId() . '" value="' . $upgradeGroup->getIme() . '"/>';
                        echo '<input type="hidden" name="upgrade_option[' . $upgradeGroup->getId() . '][name]" id="upgrade_option_' . $upgradeGroup->getId() . '" value="' . $defaultUpgradeOption->getIme() . '"/>';
                        echo '<input type="hidden" class="upgrade_option_prices" name="upgrade_option[' . $upgradeGroup->getId() . '][additional_price]" id="upgrade_option_price_' . $upgradeGroup->getId() . '" value="0"/>';

                        echo '<div style="clear: both;"></div>';
                        echo '<div style="float: right; max-width: 360px; width: 100%; margin-right: 5px;">';
                        echo '<div style="margin-top: 15px;margin-bottom: 10px; color: #808080; font-size: 13px; text-align: right;" class="ime_na_opciqta">' . $upgradeGroup->getIme() . '</div>';
                        echo '<a class="active" style="width: calc(100% - 20px);text-align: left;margin-bottom: 8px;display: block;max-width: 360px;padding: 0 10px; font-size: 13px;" id="upgrade_' . $artikul->getId() . '_' . $defaultUpgradeOption->getId() . '" onclick="javascript:$(\'#upgrade_option_' . $upgradeGroup->getId() . '\').val(\'' . $defaultUpgradeOption->getIme() . '\');$(\'#upgrade_option_price_' . $upgradeGroup->getId() . '\').val(\'' . $defaultUpgradeOption->getPriceByUser() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);updatePriceBySize(this);return false;" href="#">' . $defaultUpgradeOption->getIme() . '<span style="text-align: right; display: inline-block; float: right;">' . lang_currency_prepend . ' 0 ' . lang_currency_append . '</span></a>';

                        foreach ($artikul->getUpgradeOptions($upgradeGroup->getId()) as $upgradeOption) {
                            echo '<a style="width: calc(100% - 20px);text-align: left;margin-bottom: 8px;display: block;max-width: 360px;padding: 0 10px; font-size: 13px;" id="upgrade_' . $artikul->getId() . '_' . $defaultUpgradeOption->getId() . '" onclick="javascript:$(\'#upgrade_option_' . $upgradeGroup->getId() . '\').val(\'' . $upgradeOption->getIme() . '\');$(\'#upgrade_option_price_' . $upgradeGroup->getId() . '\').val(\'' . $upgradeOption->getPriceByUser() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);updatePriceBySize(this);return false;" href="#">' . $upgradeOption->getIme() . '<span style="text-align: right; display: inline-block; float: right;">' . lang_currency_prepend . ' ' . number_format($upgradeOption->getPriceByUser() / CURRENCY_RATE, 2, '.', '') . ' ' . lang_currency_append . '</span></a>';
                        }
                        echo '</div>';

                        echo '</div>';
                    }
                }
            }
            ?>
            <div style="clear:both; height: 10px;"></div>
            <?php
            if ($dop_artikuli != "") {
                $dop_art_arr = explode(",", $dop_artikuli);
                foreach ($dop_art_arr as $dart) {
                    $dr = new artikul((int)$dart);
                    if ($dr->isAvaliable()) {
                        $add_artikul_etiket_grup = NULL;
                        $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ? ORDER BY `id` ');
                        $stm->bindValue(1, $dr->getId(), PDO::PARAM_INT);
                        $stm->execute();
                        foreach ($stm->fetchAll() as $v) {
                            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ORDER BY `id` ');
                            $stm->bindValue(1, $v['id_etiket'], PDO::PARAM_INT);
                            $stm->execute();
                            $temp = new etiket($stm->fetch());
                            $temp_grupa = new etiket_grupa((int)$temp->getId_etiketi_grupa());
                            $add_artikul_etiket_grup[$temp->getId_etiketi_grupa()] = $temp_grupa;
                            unset($temp);
                            unset($temp_grupa);
                        }
                        if (isset($add_artikul_etiket_grup) && (sizeof($add_artikul_etiket_grup) > 0)) {
                            foreach ($add_artikul_etiket_grup as $v) {
                                if ($v->getSelect_menu()) {
                                    echo '<div style="font-size: 13px; margin-top: 10px; margin-bottom: 10px; color: #808080;">' . $v->getIme() . ' for <span style="font-weight: bold; color: #444;">' . $dr->getIme() . '</span></div>';
                                    echo '<div class="item_razmer">';
                                    echo '<input type="hidden" name="option' . $dr->getId() . '" id="option' . $dr->getId() . '"/>';
                                    $etiketii = null;
                                    $pdo = PDOX::vrazka();
                                    $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id_etiketi_grupa` = :idgroupa ORDER BY `ordr`');
                                    $stm->bindValue(":idgroupa", $v->getId(), PDO::PARAM_INT);
                                    $stm->execute();
                                    if ($stm->rowCount() > 0)

                                        foreach ($stm->fetchAll() as $d) {
                                            $temp_etiket = new etiket((int)$d['id']);
                                            $etiketii[] = $temp_etiket;
                                            unset($temp_etiket);
                                        }

                                    if ($etiketii) {
                                        foreach ($etiketii as $e) {
                                            if ($dr->getEtiketizapisi()) if (in_array($e->getId(), $dr->getEtiketi_array_ids())) {
                                                echo '<a id="option_' . $dr->getId() . '_' . $v->getId() . '_' . $e->getId() . '" onclick="javascript:$(\'#option' . $dr->getId() . '\').val(\'' . $e->getIme() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);return false;" href="#">' . $e->getIme() . '</a>';
                                            }
                                        }
                                    }
                                    echo '</div>';
                                    echo '
                                        <script type="text/javascript">
                                                addToCartRules.push(["option' . $dr->getId() . '","' . $dr->getIme() . '","' . $v->getIme() . '"]);
                                        </script>
                                        ';
                                }
                            }
                        }
                        unset($add_artikul_etiket_grup);
                    }
                    unset($dr);
                }
            }
            ?>
            <?php if($artikul->totalQuantity() > 0) : ?>
                <div class="quantity" style="<?php if (empty($artikul->getWarehouseCode())) : ?>visibility: hidden;<?php endif; ?>height: 18px;float: right;margin-top: 5px;font-size:8pt;color: #808080;width: 170px;text-align: center;">available <span class="count" style="color: #FF671F;"><?php echo $artikul->totalQuantity(); ?></span><span style="color: #FF671F;"> pcs</span></div>
            <?php endif; ?>
            <div style="clear: both;"></div>
            <div id="item_price" style="width:  175px; text-align: center;">
                <?php if (isset($__user) && $__user->is_partner()) : ?>
                    <?php if ($artikul->getCenaDostavna() > 0) : ?>
                        <span class="prodpricepromo"
                              style="font-style: italic; color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCenaDostavna() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                    <?php else : ?>
                        <?php if ($artikul->getCena_promo()) : ?>
                            <span class="prodpricenormal"
                                  style="float:none;margin-right:10px;"><?php echo lang_currency_prepend; ?> <span
                                        style="color:#424242;font-size:12pt;font-style: italic;"> <?php $tmp_cen = explode(".", number_format($artikul->getCena(), 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></span>
                            <span class="prodpricepromo"
                                  style="font-style: italic; color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                        <?php else : ?>
                            <span class="prodpricepromo"
                                  style="font-style: italic; color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if ($artikul->getCena_promo()) : ?>
                        <span class="prodpricenormal"
                              style="float:none;color:#424242;font-size:12pt;font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></span>
                        <span class="prodpricepromo"
                              style="font-style: italic; color: #FF671F;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                    <?php else : ?>
                        <span class="prodpricepromo"
                              style="font-style: italic; color: #424244;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if (isset($__user) && $__user->is_partner()) : ?>
                    <?php if ($artikul->getCena_promo() > 0) : ?>
                        <div style="width: 175px;margin-top: 5px;margin-bottom: 5px; margin-right: 25px; font-size:8pt; color: #0071b9;">
                            End
                            user: <?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup><?php echo $tmp_cen[1]; ?></sup> <span
                                    style="font-size:10px;"> <?php echo lang_currency_append; ?></span></div>
                    <?php else : ?>
                        <div style="width: 175px;margin-top: 5px;margin-bottom: 5px; margin-right: 25px; font-size:8pt; color: #0071b9;">
                            End
                            user: <?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($artikul->getCena() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup><?php echo $tmp_cen[1]; ?></sup> <span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if($artikul->getCena_promo()) : ?>
                    <?php
                    $tmpraz = (($artikul->getCena() / CURRENCY_RATE) - ($artikul->getCena_promo() / CURRENCY_RATE));
                    $tmp_razf = explode(".", number_format($tmpraz, 2, '.', ''));
                    ?>
                    <div style="width: 175px;text-align: center;">
                        <span style="font-style: normal;font-size: 8pt;color: #FF671F;">you save <?php echo lang_currency_prepend; ?><?php echo $tmp_razf[0]; ?><sup><?php echo $tmp_razf[1]; ?></sup> <?php echo lang_currency_append;?> (-<?php echo round((($tmpraz / ($artikul->getCena() / CURRENCY_RATE)) * 100), 0); ?>%)</span>
                    </div>
                <?php endif; ?>
                <div style="clear:both;"></div>
            </div>
            <div style="clear:both;"></div>
            <?php
            if ($isAvaliable && $isOnline) {
                echo '<input style="' . ($artikul->isPreorder() ? 'background-color:#29AC92;' : '') . 'width:175px; float:right;" type="submit" class="btnaddprod" onclick="return validate(addToCartRules, ' . ($artikul->isPreorder() ? 'true' : 'false') . ');" value="add to cart"/>';
            } else {
                echo '<div style=" background: #a2a2a2; border: medium none; color: #FFFFFF; font-size: 16px; font-weight: normal; line-height: 35px; text-align: center; height: 35px; padding-left: 15px; padding-right: 15px;width: 175px;box-sizing: border-box;">sold-out</div>';
            }
            ?>
            <div style="clear: both;"></div>
        </form>
        <?php
        $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
        $ds->execute();
        $d = $ds->fetch();
        $dostavka_cena = $d['delivery_price'];
        $dostavka_free = $d['free_delivery'];
        $additional_delivery = $artikul->getAdditionalDelivery();

        if ($dop_artikuli != "") {
            $dop_art_arr = explode(",", $dop_artikuli);
            foreach ($dop_art_arr as $dart) {
                $dr = new artikul((int)$dart);
                if ($dr->isAvaliable()) {
                    $additional_delivery += $dr->getAdditionalDelivery();
                }
            }
        }

        $ac = 0;
        if ($artikul->getCena_promo() > 0) {
            $ac = $artikul->getCena_promo();
        } else {
            $ac = $artikul->getCena();
        }

        echo '<div class="delivery-box">';

//        if ($artikul->isPreorder()) {
//            if ($artikul->getCena_promo() > 0) {
//                echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">with free delivery</div>';
//            } else {
//                echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">with free delivery</div>';
//            }
//        } else if ($dostavka_free < $ac) {
//            if ($additional_delivery != '' && $additional_delivery > 0) {
/*                echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">delivery: ' . $additional_delivery . ' <?php echo lang_currency_append; ?></div>';*/
//            } else {
//                if ($artikul->getCena_promo() > 0) {
//                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">with free delivery</div>';
//                } else {
//                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">with free delivery</div>';
//                }
//            }
//        } else {
//            if ($additional_delivery != '' && $additional_delivery > 0) {
//                $total_delivery = $dostavka_cena + $additional_delivery;
/*                echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">delivery: ' . $total_delivery . ' <?php echo lang_currency_append; ?></div>';*/
//            } else {
//                if ($artikul->getCena_promo() > 0) {
/*                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">delivery: ' . $dostavka_cena . ' <?php echo lang_currency_append; ?></div>';*/
//                } else {
/*                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px; text-align: center;">delivery: ' . $dostavka_cena . ' <?php echo lang_currency_append; ?></div>';*/
//                }
//            }
//        }
        echo '</div>';
        ?>
        <div class="clear"></div>
        <?php if ($artikul->isPreorder()) : ?>
            <div style="margin-top: 15px; margin-bottom: 10px; font-size:10pt; color: #29AC92;"><?php echo $artikul->getPreorderDescription(); ?></div>
            <div id="pre-order-dialog">
                <?php echo $preOrderConfirmationDescriptions; ?>
            </div>​
        <?php endif; ?>
        <div style="clear: both;"></div>
    </div>
    <div id="collection_container">
        <?php if (count($artikul->getCollectionProducts())) : ?>
            <div id="collection_label">colors</div>
            <?php
            foreach ($artikul->getCollectionProducts() as $v) {
                $collectionProduct = new artikul((int)$v);
                if ((!$collectionProduct->isAvaliable() || !$collectionProduct->isOnline()) && $collectionProduct->getId() != $artikul->getId()) continue;
                $current = '';
                $border = 'border: 1px solid #ccc;';
                if ($collectionProduct->getId() == $artikul->getId()) $current = 'current';
                if ($collectionProduct->IsNew()) $border = 'border: 1px solid #01A0E2;';
                echo "<a title='" . $collectionProduct->getIme() . "' href='" . urlm . "index.php?id=" . $collectionProduct->getId() . "' style='margin-bottom: 3px; display: inline-block;'><div class='collection-item " . $current . "' style='" . $border . "'><img src='" . url . $collectionProduct->getKartinka_t() . "' alt='' /></div></a>";

            }
            ?>
        <?php endif; ?>
    </div>
    <div id="item_info" style="margin-top: 10px; padding-bottom: 5px;"><?php echo nl2br($artikul->getDescription()); ?></div>
</div>
