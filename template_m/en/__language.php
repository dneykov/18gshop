<?php

define('lang_login_error_pass_or_mail', 'You entered an invalid email or password!');

define('lang_login_register_error_mail_lenght_long', 'email too long');
define('lang_login_register_error_mail_invalid', 'invalid email');
define('lang_login_register_error_mail_already_used', 'mail@mail.com email address is already registtiran Please enter a different email address');

define('lang_login_register_error_pass_lenght_short', 'password is more than 4 characters');
define('lang_login_register_error_pass_lenght_long', 'password <200 characters');

define('lang_login_register_error_name_last_lenght_short', 'family must be at least 3 characters');
define('lang_login_register_error_name_last_lenght_long', 'family should be up to 200 characters');
define('lang_login_register_error_name_first_lenght_short', 'name must be at least 3 characters');
define('lang_login_register_error_name_first_lenght_long', 'name should be up to 200 characters');

define('lang_login_register_complete', 'zapisana reg');

define('lang_login_logout', 'exit');
define('lang_login_login', 'log in');

define('lang_error_passwords_not_match', 'wrong password confirmation');

define('lang_login_register_error_terms', 'general conditions must be accepted');
define('lang_login_register_error_privacy', 'processing privacy data should be accepted');

define('lang_order_address_data_error_gsm', 'invalid mobile phone');
define('lang_order_address_data_error_home_telephone', 'invalid phone');
define('lang_order_address_data_error_city', 'invalid city');
define('lang_order_address_data_error_street', 'invalid street');
define('lang_order_address_data_error_number', 'invalid street number');
define('lang_order_address_data_error_vhod', 'invalid enter');
define('lang_order_address_data_error_etaj', 'invlid floor');
define('lang_order_address_data_error_apartament', 'apartment invalid');

define('lang_success_save', 'changes have been saved');

define('lang_order_success_send', 'Your order is accepted');
define('lang_order_success_send_email', 'Order information is sent to ');
define('lang_currency_append', '');
define('lang_currency_prepend', '€');
define('lang_currency_coef', 1);
define('lang_order_comment_text',"Invoice or any additional information relating to order.");
define('lang_order_subject','Accepted order of ');
define('lang_order_approved', 'Approved order of ');
define('lang_order_approved_msg1', 'Your order has been approved and will be delivered.');
define('lang_order_approved_msg2', 'As an important customer for us we offer specially selected products of your choice.');
define('lang_order_msg1',"Your order will be processed within 4 working days and delivered to the address, specified by you. You can easily track the status of your order from your account. Thanks for buying from us!");
define('lang_from','from');
define('lang_order_msg_nom',"Your order number is: ");
define('lang_order_msg1_1',"You can easily track the status of your order from your account.");
define('lang_order_msg_kod',"Code");
define('lang_order_msg_art',"Item");
define('lang_order_msg_opt',"Options");
define('lang_order_msg_br',"Number");
define('lang_order_msg_pr',"Price");
define('lang_order_msg_preorder',"To be paid*");
define('lang_order_msg_deposit',"Paid in advance*");
define('lang_order_msg_total',"Total");
define('lang_order_msg_total2',"Total value of the contract:");
define('lang_free_shipping',"Free shipping");
define('lang_order_deliv',"Delivery");
define('lang_order_delivery',"delivery:");
define('with_free_delivery',"free delivery");
define('with_free_delivery_cart',"free delivery");
define('total_cart',"total");
define('lang_order_tmp_cash',"");
define('lang_order_msg3_cash',"");
define('lang_order_tmp_bank',"Payment method: Bank transfer (transfer total amount, including the cost of delivery, to the specified bank account)");
define('lang_order_tmp_bank_details',"Bank transfer details:");
define('lang_order_msg3_bank',"Your order will be delivered to your address within two business days of receipt of the payment. Upon receipt of the shipment, you owe nothing to the courier.");
define('lang_order_preorde_desc',"* pre-order");
define('lang_size', 'size:');
define('cash_on_delivery', 'Pay at delivery');
define('bank_transfer', 'Bank transfer');
define('payment_type', 'Payment type:');
define('iban', 'IBAN:');
define('bank_details', 'bank details:');
define('lang_order_company_name_short', 'company name must be at least 3 characters');
define('lang_order_company_name_long', 'company name should be up to 200 characters');
define('lang_order_company_city_error', 'not valid company city');
define('lang_order_company_street_error', 'not valid company address');
define('lang_order_company_mol_error', 'not valid f.l.p');
define('lang_order_company_eik_vat_error', 'not valid bulstat or vat');
define('pickup_delivery', 'Pickup from us');
define('lang_enduser_ref_price', 'End user:');
define('lang_in', 'in');
