<?php
	require dir_root_template.'_top.php';
?>
<div id="info_tab"><p>Order Details</p></div>
<div id="content" style="padding-left: 10px; padding-right: 15px;">
	<?php
		if(isset($sucessSend) && $sucessSend == true){
			echo "<div style='margin-top: 10px; margin-bottom: 20px; margin-left: 10px;'>Your order was send successfuly!</div>";
		}
	?>
	<form action="" method="post" style="margin:50px 10px 0;">
		<p>First Name <?php echo $__user->getName(); ?></p>
		<p>Last Name <?php echo $__user->getName_last(); ?></p>
		<input type="text" autocomplete="off" name="edit_mail" value="<?php if(isset($_POST['edit_mail'])) echo $_POST['edit_mail'];else{echo $__user->getMail();} ?>" id="regemail" style="width: 98%; margin: 5px 0px 5px 0px;"/>
		<input type="hidden" autocomplete="off" name="register_pass" value="<?php if(isset($_POST['register_pass'])) echo $_POST['register_pass']; ?>" id="regpassword" style="width: 98%;  margin: 5px 0px 5px 0px;">
		<input type="hidden" autocomplete="off" name="register_passAgain" value="<?php if(isset($_POST['register_passAgain'])) echo $_POST['register_passAgain']; ?>" id="regpasswordagain" style="width: 98%;">		
		<select id="country" name="country" style="width: 98%; margin: 0px 0px 5px 0px;">
					<?php
						$stm = $pdo -> prepare("SELECT * FROM `locations`");
						$stm -> execute();
						foreach ($stm -> fetchAll() as $c) {
							echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
						}
					?>
		</select>
		<input type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm'];else{echo $__user->getPhone_mobile();} ?>" id="reggsm" style="width: 98%;"/>
		<input type="hidden" autocomplete="off" name="telephone" value="<?php if(isset($_POST['telephone'])) echo $_POST['telephone']; else{echo $__user->getPhone_home();} ?>" id="regphone" style="width: 98%"/>
		<input type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; else{echo $__user->getAdress_town();} ?>" id="regtown" style="width: 98%; margin: 5px 0px 5px 0px;"/>
		<input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; else{echo $__user->getAdress_street();} ?>" id="regstreet" style="width: 98%; margin: 0px 0px 5px 0px;"/>
		<input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; else{echo $__user->getAdress_number();} ?>" id="regstreetnumber" style="width: 98%;"/>
		<div style="height: 5px;"></div>
		<input style="width: 30%;" type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" id="regentrance" style="width: 98%; margin: 5px 0px 5px 0px;"/>
		<input style="width: 30%;" type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['etaj'])) echo $_POST['etaj']; else{echo $__user->getAdress_etaj();} ?>" id="regfloor" style="width: 98%; margin: 5px 0px 5px 0px;"/>
		<input style="width: 30%;" type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; else{echo $__user->getAdress_ap();}?>" id="regflat" style="width: 98%; margin: 5px 0px 5px 0px;"/>				
		<textarea name="comment" style="width: 98%; height:73px; margin-top: 5px"><?php if(isset($_POST['comment'])) echo $_POST['comment']; else 'коментар или данни за фактура'; ?></textarea>
		<input type="hidden" name="buy" value="buy"/>
		<div style="margin-top:30px;">	
			<div id="button">
				<input id="register" style="border: none; color: #fff;" type="submit" name="submit" value="finish" class="button">	
			</div>
		</div>
		<div style="clear: both;"></div>
	</form>
	<?php
		$tmp_msg='';
		if((isset($user_register_error))&&($user_register_error)) foreach ($user_register_error as $v) {
			$tmp_msg= $tmp_msg.', '.$v;
		} ?>
	<?php if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
			$tmp_msg= $tmp_msg.', '.$v;
		} 
		$tmp_msg=trim($tmp_msg,', ');
		if(!empty($tmp_msg)){
			echo '<div class="error">';
				echo $tmp_msg;
			echo '</div>';
		}
	?>				
</div>
<?php
	require dir_root_template."footer_menu.php";	
?>