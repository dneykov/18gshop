<?php
if(isset($_GET['user']) && isset($_GET['r'])){
    if(isset($__user)){
        if(isset($_POST['submit'])){
            $user_edit_error = NULL;
            $order_address_data_error = NULL;

            $username_edit_mail = $_POST['user_mail'];

            if (!filter_var($username_edit_mail, FILTER_VALIDATE_EMAIL)) $user_edit_error[] = lang_login_register_error_mail_invalid;
            if(mb_strlen($username_edit_mail) > 200) $user_edit_error[] = lang_login_register_error_mail_lenght_long;
            $stm_check_mail = $pdo->prepare("SELECT `mail` FROM `members` WHERE LOWER(`mail`) = LOWER(?) and id != ? LIMIT 1");
            $stm_check_mail -> bindValue(1, $username_edit_mail, PDO::PARAM_STR);
            $stm_check_mail -> bindValue(2, $__user->getId(), PDO::PARAM_INT);
            $stm_check_mail -> execute();
            //ако името вече го има показва грешка
            if ($stm_check_mail->rowCount() >0 ) { $user_edit_error[] = str_replace("mail@mail.com", $username_edit_mail, lang_login_register_error_mail_already_used); }

            $changepass=false;

            if($_POST['user_pass'] != "" || $_POST['user_passAgain'] != "" || $_POST['user_oldPass'] != ""){
	            $input_old_pass = trim($_POST['user_oldPass']);
	            if($input_old_pass == "") $user_edit_error[] = "invalid password";

	            $stm_check_old_pass = $pdo->prepare("SELECT `password` FROM `members` WHERE `id`=? LIMIT 1");
	            $stm_check_old_pass -> bindValue(1, $__user->getId(), PDO::PARAM_INT);
	            $stm_check_old_pass -> execute();
	            $check_old_pass = $stm_check_old_pass->fetch();
	            $old_pass = $check_old_pass['password'];

	            if(sha1($input_old_pass) != $old_pass) $user_edit_error[] = "invalid password";
	            if(trim($_POST['user_pass']) == "") $user_edit_error[] = "invalid new password";
	            if(trim($_POST['user_pass']) != trim($_POST['user_passAgain'])) $user_edit_error[] = "new passwords are not equal";
           	}

            if(isset($_POST['user_pass'])&&isset($_POST['user_passAgain'])&&(trim($_POST['user_mail'])!="")&&(trim($_POST['user_passAgain'])!="")){
                if($_POST['user_pass']!=$_POST['user_passAgain']) {$user_edit_error[] = lang_error_passwords_not_match;}
                $username_edit_pass = $_POST['user_pass'];
                if(mb_strlen($username_edit_pass) < 4) $user_edit_error[] = lang_login_register_error_pass_lenght_short;
                if(mb_strlen($username_edit_pass) > 200) $user_edit_error[] = lang_login_register_error_pass_lenght_long;
                $changepass=true;
            }

            if($user_edit_error===NULL){
                if($changepass){
                    $username_edit_pass = sha1($username_edit_pass);

                    $stm = $pdo->prepare("UPDATE `members` SET `mail`=? , `password`=? WHERE id=?");
                    $stm->bindValue(1, $username_edit_mail, PDO::PARAM_STR);
                    $stm->bindValue(2, $username_edit_pass, PDO::PARAM_STR);
                    $stm->bindValue(3, $__user->getId(), PDO::PARAM_INT);

                    $stm->execute();
                } else {
                    $stm = $pdo->prepare("UPDATE `members` SET `mail`=? WHERE id=?");
                    $stm->bindValue(1, $username_edit_mail, PDO::PARAM_STR);
                    $stm->bindValue(2, $__user->getId(), PDO::PARAM_INT);
                    $stm->execute();
                }
            }

            if($_POST['account_type'] == '0') {
                $order_addr_gsm = $_POST['gsm'];
                $order_addr_city = $_POST['city'];
                $order_addr_street = $_POST['street'];
                $order_addr_number = $_POST['number'];
                $order_addr_vhod = $_POST['vhod'];
                $order_addr_etaj = $_POST['etaj'];
                $order_addr_apartament = $_POST['apartament'];

                if(mb_strlen($order_addr_gsm) < 9) $order_address_data_error[] = lang_order_address_data_error_gsm;
                if(mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

                if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
                if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

                if(mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
                if(mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

                if(mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
                if(mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;

                if(mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;
                if(mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;
                if(mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;

                if($order_address_data_error==null){
                    $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=?,`account_type`=?,`country`=? WHERE id=?");
                    $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
                    $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
                    $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
                    $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
                    $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
                    $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
                    $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
                    $stm->bindValue(8, $_POST['account_type'], PDO::PARAM_STR);
                    $stm->bindValue(9, $_POST['country'], PDO::PARAM_STR);
                    $stm->bindValue(10, $__user->getId(), PDO::PARAM_INT);

                    $stm->execute();
                    require '__members.php';
                }
            } else if($_POST['account_type'] == '1') {
                $order_addr_gsm = $_POST['gsm'];
                $order_addr_city = $_POST['city'];
                $order_addr_street = $_POST['street'];
                $order_addr_number = $_POST['number'];
                $order_addr_vhod = $_POST['vhod'];
                $order_addr_etaj = $_POST['etaj'];
                $order_addr_apartament = $_POST['apartament'];

                $company_name = $_POST['company_name'];
                $company_city = $_POST['company_city'];
                $company_street = $_POST['company_street'];
                $company_number = $_POST['company_number'];
                $company_mol = $_POST['company_mol'];
                $company_eik = $_POST['company_eik'];
                $company_vat = $_POST['company_vat'];
                $company_phone = $_POST['company_phone'];

                if (mb_strlen($order_addr_gsm) < 9) $order_address_data_error[] = lang_order_address_data_error_gsm;
                if (mb_strlen($order_addr_gsm) > 25) $order_address_data_error[] = lang_order_address_data_error_gsm;

                if (mb_strlen($order_addr_city) < 3) $order_address_data_error[] = lang_order_address_data_error_city;
                if (mb_strlen($order_addr_city) > 200) $order_address_data_error[] = lang_order_address_data_error_city;

                if (mb_strlen($order_addr_street) < 2) $order_address_data_error[] = lang_order_address_data_error_street;
                if (mb_strlen($order_addr_street) > 200) $order_address_data_error[] = lang_order_address_data_error_street;

                if (mb_strlen($order_addr_number) < 1) $order_address_data_error[] = lang_order_address_data_error_number;
                if (mb_strlen($order_addr_number) > 200) $order_address_data_error[] = lang_order_address_data_error_number;

                if (mb_strlen($order_addr_vhod) > 5) $order_address_data_error[] = lang_order_address_data_error_vhod;

                if (mb_strlen($order_addr_etaj) > 3) $order_address_data_error[] = lang_order_address_data_error_etaj;

                if (mb_strlen($order_addr_apartament) > 6) $order_address_data_error[] = lang_order_address_data_error_apartament;

                if (mb_strlen($company_name) < 3) $order_address_data_error[] = lang_order_company_name_short;
                if (mb_strlen($company_name) > 200) $order_address_data_error[] = lang_order_company_name_long;

                if (mb_strlen($company_city) < 2) $order_address_data_error[] = lang_order_company_city_error;
                if (mb_strlen($company_city) > 200) $order_address_data_error[] = lang_order_company_city_error;

                if (mb_strlen($company_street) < 2) $order_address_data_error[] = lang_order_company_street_error;
                if (mb_strlen($company_street) > 200) $order_address_data_error[] = lang_order_company_street_error;

                if (mb_strlen($company_mol) < 2) $order_address_data_error[] = lang_order_company_mol_error;
                if (mb_strlen($company_mol) > 200) $order_address_data_error[] = lang_order_company_mol_error;

                if (mb_strlen($company_eik) < 2 && mb_strlen($company_vat) < 2) $order_address_data_error[] = lang_order_company_eik_vat_error;
                if (mb_strlen($company_eik) > 200 && mb_strlen($company_vat) > 200) $order_address_data_error[] = lang_order_company_eik_vat_error;

                if($order_address_data_error==null){
                    $stm = $pdo->prepare("UPDATE `members` SET `phone_mobile`=?,`adress_town`=?,`adress_street`=?,`adress_number`=?,`adress_vhod`=?,`adress_etaj`=?,`adress_ap`=?,`country`=?,`company_name`=?,`company_city`=?,`company_street`=?,`company_number`=?,`company_mol`=?,`company_eik`=?,`company_vat`=?,`company_phone`=?,`account_type`=? WHERE id=?");
                    $stm->bindValue(1, $order_addr_gsm, PDO::PARAM_STR);
                    $stm->bindValue(2, $order_addr_city, PDO::PARAM_STR);
                    $stm->bindValue(3, $order_addr_street, PDO::PARAM_STR);
                    $stm->bindValue(4, $order_addr_number, PDO::PARAM_STR);
                    $stm->bindValue(5, $order_addr_vhod, PDO::PARAM_STR);
                    $stm->bindValue(6, $order_addr_etaj, PDO::PARAM_STR);
                    $stm->bindValue(7, $order_addr_apartament, PDO::PARAM_STR);
                    $stm->bindValue(8, $_POST['country'], PDO::PARAM_STR);
                    $stm->bindValue(9, $company_name, PDO::PARAM_STR);
                    $stm->bindValue(10, $company_city, PDO::PARAM_STR);
                    $stm->bindValue(11, $company_street, PDO::PARAM_STR);
                    $stm->bindValue(12, $company_number, PDO::PARAM_STR);
                    $stm->bindValue(13, $company_mol, PDO::PARAM_STR);
                    $stm->bindValue(14, $company_eik, PDO::PARAM_STR);
                    $stm->bindValue(15, $company_vat, PDO::PARAM_STR);
                    $stm->bindValue(16, $company_phone, PDO::PARAM_STR);
                    $stm->bindValue(17, $_POST['account_type'], PDO::PARAM_STR);
                    $stm->bindValue(18, $__user->getId(), PDO::PARAM_INT);

                    $stm->execute();
                    require '__members.php';
                }
            }
        }
    }
}
?>
<style type="text/css">
	#cont {
		margin: 5px auto;
		text-align: left;
		padding-bottom: 50px;
		font-size: 14px;
		padding-left: 10px;
		padding-right: 15px;
	}
	#tabs-bar {
		height: 20px;
		width: 100%;
		padding-bottom: 2px;
		border-bottom: 1px solid #FF671F;
		margin-bottom: 20px;
	}
	#tabs-bar a{
		margin-left: 5px;
		margin-right: 20px;
		font-size: 16px;
		color: #333;
	}
	.need {
		color: red;
	}
	#legend {
		margin-top: 10px;
		font-size: 14px;
    float: left;
	}
	.submit {
		display: inline-block;
		background: none repeat scroll 0 0 #FF671F;
		border: medium none;
		color: #fff;
		cursor: pointer;
		float: right;
		padding: 0 8px 2px;
		text-decoration: none;
		width:120px;
		float:right;
		height: 36px;
		line-height: 32px;
	}
	.back {
		display: inline-block;
		background: none repeat scroll 0 0 #333;
		border: medium none;
		color: #fff;
		cursor: pointer;
		float: right;
		height: 36px;
		width: 120px;
		line-height: 33px;
		text-align: center;
		text-decoration: none;
		float:right;
		font-size: 13px;
		margin-right: 10px;
	}
	.error{
		margin-top: 20px;
		width: 550px;
		color: #FE0000;
		font-size:9pt;
		clear:both;
    text-align: left;
	}
	input, textarea, select {
		font-size: 13px;
		padding: 5px 0px;
		border: 1px solid #CCC;
	}
	#cont form input:hover, #cont form input:focus, #cont form select:hover, #cont form select:focus {
		border: 1px solid #FF671F;
	}
</style>
<script>
    $(document).ready(function(){
        if($("input[name=account_type]:checked").val() == '0') {
            $("#legal-form").hide();
            $("div.cart-price").css("margin-top", "0px");
            $("#legal-form :input").prop("disabled", true);
            $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
        } else if($("input[name=account_type]:checked").val() == '1') {
            $("#legal-form").show();
            $("div.cart-price").css("margin-top", "-30px");
            $("#legal-form :input").prop("disabled", false);
        }
        $("input[name=account_type]").change(function () {
            if($(this).val() == '0') {
                $("#legal-form").hide();
                $("div.cart-price").css("margin-top", "0px");
                $("#legal-form :input").prop("disabled", true);
                $("#individual-form :input:not(.always-disabled)").prop("disabled", false);
            } else {
                $("#legal-form").show();
                $("div.cart-price").css("margin-top", "-30px");
                $("#legal-form :input").prop("disabled", false);
            }
        });
    });
</script>
<div id="where_am_i_box">
	<div id="where_am_i">
		<a href="<?php echo urlm; ?>" style="color: #333;">18gshop</a> <span style="color: #333;">/</span>
		<a href="<?php echo urlm.'index.php?user'; ?>" style="color: #333;">cont</a> <span style="color: #333;">/</span>
		<a href="<?php echo urlm.'index.php?user'; ?>" style="color: #333;"><?php echo $__user->getName(); ?></a> <span style="color: #333;">/</span>
        <?php if($__user->is_partner()) : ?>
            <span style="color: #FF671F">partner</span> <span style="color: #333;">/</span>
        <?php endif; ?>
		<a href="<?php echo urlm.'index.php?user&r'; ?>" style="color: #333;">edit</a>
	</div>
</div>
<div id="tabs-bar">
	<a href="<?php echo urlm.'index.php?user'; ?>" style="color: #FF671F;">cont</a>
	<a href="<?php echo urlm.'index.php?user&o'; ?>">ordinelor
	<?php
		$stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ?");
        $stm->bindValue(1, $__user->getId(), PDO::PARAM_STR);
        $stm->execute();
        echo $stm->rowCount();
    ?>
	</a>
	<a  href="login_m.php?logout=1">logout</a>
</div>
<div id="cont">
<?php
	if(isset($__user)){
		?>
        <form action="" method="post">
            <div class="top-row" style="margin-left: 5px; margin-bottom: 10px;">
                <input type="radio" id="individual" style="width: auto;" name="account_type" value="0" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '0') echo "checked"; ?> />
                <label for="individual">Individual person</label>
                <input type="radio" id="legal_entity" style="width: auto;" name="account_type" value="1" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; else if (!isset($_POST['account_type']) && $__user->getAccountType() == '1') echo "checked" ?> />
                <label for="legal_entity">Legal entity</label>
            </div>
            <div style="position:absolute;height:0px; overflow:hidden; ">
            Username <input type="text" name="fake_safari_username" >
            Password <input type="password" name="fake_safari_password">
            </div>
            <div class="reg-field-left" style="margin-bottom: -10px; border-width: 0px; text-align: left;">prenume</div>
            <div class="reg-field-right" style="margin-bottom: -10px; border-width: 0px; text-align: left;">nume</div>
            <input type="text" name="user_name" autocomplete="off" value="<?php if(isset($_POST['user_name'])) echo $_POST['user_name']; else echo $__user->getName(); ?>" disabled class="reg-field-left"/>
            <input type="text" name="user_name_last" autocomplete="off" value="<?php if(isset($_POST['user_name_last'])) echo $_POST['user_name_last']; else echo $__user->getName_last(); ?>" disabled class="reg-field-right"/>

            <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">e-mail<span class="need">*</span></div>
            <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">country<span class="need">*</span></div>
            <input type="text" autocomplete="off" name="user_mail" value="<?php if(isset($_POST['user_mail'])) echo $_POST['user_mail']; else echo $__user->getMail(); ?>"  class="reg-field-left"/>
            <select id="country" name="country" class="reg-field-right" style="padding: 3px !important;">
            <?php
                $stm = $pdo -> prepare("SELECT * FROM `locations`");
                $stm -> execute();
                foreach ($stm -> fetchAll() as $c) {
                    echo "<option value='".$c['id']."'";
                    if(($__user->getCountryId() == $c['id']) || (isset($_POST['country']) && $_POST['country'] == $c['id'])) echo " selected";
                    echo ">".$c['country_name']."</option>";
                }
            ?>
            </select>

            <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">telefon<span class="need">*</span></div>
            <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">city<span class="need">*</span></div>
            <input type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm']; else echo $__user->getPhone_mobile(); ?>" class="reg-field-left"/>
            <input type="hidden" autocomplete="off" name="telephone" value="<?php if(isset($_POST['telephone'])) echo $_POST['telephone']; else{echo $__user->getPhone_home();} ?>" class="reg-field-right"/>
            <input type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; else{echo $__user->getAdress_town();} ?>" class="reg-field-right"/>

            <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">parola<span class="need">*</span></div>
            <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">noua parola<span class="need">*</span></div>
            <input type="password" autocomplete="off" name="user_oldPass" class="reg-field-left"><br />
            <input type="password" autocomplete="off" name="user_pass" class="reg-field-right"><br />

            <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">strada<span class="need">*</span></div>
            <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">numar<span class="need">*</span></div>
            <input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; else{echo $__user->getAdress_street();} ?>" class="reg-field-street" style="width: 49% !important;"/>
            <input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; else{echo $__user->getAdress_number();} ?>" class="reg-field-number" style="width: 49% !important; float: right;"/>

            <div class="user-reg-field-three" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left; margin-left: 0% !important;">bl.</div>
            <div class="user-reg-field-three" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">et.</div>
            <div class="user-reg-field-three" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">ap.</div>
            <input type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; else{echo $__user->getAdress_vhod();} ?>" class="user-reg-field-three" style="margin-left: 0% !important;"/>
    		<input type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['etaj'])) echo $_POST['etaj']; else{echo $__user->getAdress_etaj();} ?>" class="user-reg-field-three"/>
    		<input type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; else{echo $__user->getAdress_ap();}?>" class="user-reg-field-three"/>

            <div style="clear:both;"></div>
            <div id="legal-form" style="margin-top: 40px;margin-bottom: 20px;">
                <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">company name<span class="need">*</span></div>
                <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">f.l.p<span class="need">*</span></div>
                <input type="text" autocomplete="off" name="company_name" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name']; else{echo $__user->getCompanyName();} ?>" class="reg-field-left"/>
                <input type="text" autocomplete="off" name="company_mol" value="<?php if(isset($_POST['company_mol'])) echo $_POST['company_mol']; else{echo $__user->getCompanyMol();} ?>" class="reg-field-right"/>
                <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">bulstat<span class="need">*</span></div>
                <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">vat<span class="need">*</span></div>
                <input type="text" autocomplete="off" name="company_eik" value="<?php if(isset($_POST['company_eik'])) echo $_POST['company_eik']; else{echo $__user->getCompanyEik();} ?>" class="reg-field-left">
                <input type="text" autocomplete="off" name="company_vat" value="<?php if(isset($_POST['company_vat'])) echo $_POST['company_vat']; else{echo $__user->getCompanyVat();} ?>" class="reg-field-right">
                <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">city<span class="need">*</span></div>
                <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">telefon</div>
                <input type="text" autocomplete="off" name="company_city" value="<?php if(isset($_POST['company_city'])) echo $_POST['company_city']; else{echo $__user->getCompanyCity();} ?>" class="reg-field-left"/>
                <input type="text" autocomplete="off" name="company_phone" value="<?php if(isset($_POST['company_phone'])) echo $_POST['company_phone']; else{echo $__user->getCompanyPhone();} ?>" class="reg-field-right"/>
                <div class="reg-field-left" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">strada<span class="need">*</span></div>
                <div class="reg-field-right" style="margin-bottom: -10px; margin-top: 0px; border-width: 0px; text-align: left;">numar</div>
                <input type="text" autocomplete="off" name="company_street" value="<?php if(isset($_POST['company_street'])) echo $_POST['company_street']; else{echo $__user->getCompanyStreet();} ?>" class="reg-field-left">
                <input type="text" autocomplete="off" name="company_number" value="<?php if(isset($_POST['company_number'])) echo $_POST['company_number']; else{echo $__user->getCompanyNumber();} ?>" class="reg-field-right">
            </div>

            <div id="legend">
                <span class="need">*</span> necessary
            </div>
            <div style="clear:both;"></div>

            <?php
                $tmp_msg='';
                if((isset($user_edit_error))&&($user_edit_error)) foreach ($user_edit_error as $v) {
                    $tmp_msg= $tmp_msg.', '.$v;
                }
                if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
                    $tmp_msg= $tmp_msg.', '.$v;
                }
                $tmp_msg=trim($tmp_msg,', ');
                if(!empty($tmp_msg)){
                    echo '<div class="error">';
                        echo $tmp_msg;
                    echo '</div>';
                }
            ?>
            <div style="float: right; width: 100%; margin-top: 20px;">
                <input id="register" type="submit" name="submit" value="save" class="submit">
                <a href="<?php echo urlm.'/index.php?user'; ?>" class="back">cancel</a>
            </div>
        </form>
		<?php
	} else {
		echo "<center>Please log in your account</center>";
	}
?>
</div>

