<div id="info_tab"><p>Cos de cumparaturi</p></div>
<script type="text/javascript">
    function change(id, num) {
        parent.$('.top_panel a.cart').load('<?php echo urlm; ?>/template_m/bg/top_menu_shopping_cart.php');
        var newid = id.substr(4);
        $.ajax({
            url: "cart.php",
            type: "POST",
            data: "updateNumber=" + num + "&podID=" + newid,
            success: function (data) {
                $('#in_card').html(data);
            }
        });
        parent.$('.top_panel a.cart').load('<?php echo urlm; ?>/template_m/bg/top_menu_shopping_cart.php');
        parent.$('.login_panel').load('<?php echo urlm; ?>/template_m/bg/top_menu_login.php');
    }

    var isProcessingIncreaseQty = false;
    var isProcessingDecreaseQty = false;

    function increaseQty(id) {
        if (isProcessingIncreaseQty) {
            return;
        }
        var num = parseInt($("#list" + id).val()) + 1;
        $("#list" + id).val(num);

        if (parseInt($("#list" + id).val()) > 1) {
            $("#remove_qty_" + id).css("visibility", "visible");
        }

        isProcessing = true;
        $.ajax({
            url: "cart_m.php",
            type: "POST",
            data: "updateNumber=" + num + "&podID=" + id,
            success: function (data) {
                isProcessingIncreaseQty = false;
                var result = jQuery.parseJSON(data);
                $('#in_card').html(result.count);
                $("#allcost").html(result.cost);
                $("#allcost_delivery").html(result.delivery);
            },
            error: function () {
                isProcessingIncreaseQty = false;
            }
        });
    }

    function decreaseQty(id) {
        if (parseInt($("#list" + id).val()) > 1) {
            if (isProcessingDecreaseQty) {
                return;
            }
            var num = parseInt($("#list" + id).val()) - 1;

            $("#list" + id).val(num);

            if (parseInt($("#list" + id).val()) == 1) {
                $("#remove_qty_" + id).css("visibility", "hidden");
            }

            isProcessing = true;
            $.ajax({
                url: "cart_m.php",
                type: "POST",
                data: "updateNumber=" + num + "&podID=" + id,
                success: function (data) {
                    isProcessingDecreaseQty = false;
                    var result = jQuery.parseJSON(data);
                    $('#in_card').html(result.count);
                    $("#allcost").html(result.cost);
                    $("#allcost_delivery").html(result.delivery);
                },
                error: function () {
                    isProcessingDecreaseQty = false;
                }
            });
        }
    }
</script>
<div id="content" style="max-width: 1024px;min-height:200px;">
    <FORM METHOD="POST" ACTION="fancy_login_m.php?p=ordrdetails">
        <input type="hidden" name="comment"/>
        <?php
        $nots = '<center>Nu aveti produse in cos.</center>';
        $all = 0;
        $allPreorder = 0;
        if (isset($_SESSION['cart']) && (sizeof($_SESSION['cart']) > 0)) {
            echo '<div id="articuli">';
            foreach ($_SESSION['cart'] as $key => $val) {
                if ($_SESSION['cart'][$key]['type'] == "product") {
                    $artikul = new artikul((int)$key);
                    $add_artikuli = $artikul->getAddArtikuli();
                    $style = '';
                    if ($add_artikuli != '') {
                        $style = "style='border-bottom: none;margin-bottom:0px;'";
                    }
                    $option = '';
                    $flag = false;
                    if (isset($_SESSION['cart'][$key]['options'])) {
                        foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                            $expl_arr = explode(",", $optionVal);
                            $option = '';
                            foreach ($expl_arr as $keyy => $vall) {
                                if ($keyy == 0) {
                                    $artikulid = (int)$vall;
                                }
                                if ($keyy == 1 && $artikulid == $key) {
                                    $option = "<strong> - " . $vall . "</strong>";
                                    $flag = true;
                                }
                            }
                        }
                    }
                    if ($flag == false) {
                        $option = "&nbsp;";
                    }

                    $price = number_format($_SESSION['cart'][$key]['price'] / CURRENCY_RATE, 2, '.', '');
                    $price = explode('.', $price);
                    $link = 'index.php';
                    $link .= '?id=' . $artikul->getid() . '';
                    $link .= '&category=' . $artikul->getid_kategoriq() . '';
                    $link .= '&mode=' . $artikul->getid_vid() . '';
                    echo '
					<div class="articulCart" ' . $style . '>
						<div class="articulContainer">
						<div class="articul_img" style="display: block; float: left">
							<a href="' . urlm . '/' . $link . '"><img alt="' . stripslashes($artikul->getIme()) . '" src="' . $artikul->getKartinka_t() . '"></a>
						</div>
						<div class="cart_articul_info">
							<div class="cart_articul_info_name"><strong>' . $artikul->getIme_marka() . '</strong> ' . $artikul->getIme() . ' ' . $option;
                    if (isset($_SESSION['cart'][$key]['upgrade_option'])) {
                        foreach ($_SESSION['cart'][$key]['upgrade_option'] as $upgrade_option) {
                            echo "<div>" . $upgrade_option['upgrade_name'] . ": " . $upgrade_option['name'] . "</div>";
                        }
                    } else {
                        echo '<br>';
                    }
                    if ($artikul->isPreorder()) {
                        echo '<span style="color: #FB671F;margin-top: 2px;">with pre-order <br>advance payment  ' . $_SESSION['cart'][$key]['preorder']['deposit_percent'] . '%</span>';
                    }
                    if ($_SESSION['cart'][$key]['count'] > 1) {
                        echo '<style>#remove_qty_' . $key . ' { visibility: visible; }</style>';
                    }
                    echo '</div><div class="cart_item_options not-selectable">
                                <div id="remove_qty_' . $key . '" class="qty remove_qty" onclick="decreaseQty(\'' . $key . '\')">-</div>
								<input style="width:25px;text-align: center; margin: 0 0 4px;border: 1px solid #999999;border-radius:0px;font-family: Verdana;font-size: 11pt;color: #808080;padding: 0;" type="text" onKeyUp="change(this.id, this.value);" value="' . $_SESSION['cart'][$key]['count'] . '" id="list' . $key . '" NAME="count' . $artikul->getId() . '" readonly/>
								<div class="qty add_qty" onclick="increaseQty(\'' . $key . '\')">+</div>
							</div>
							<div class="cart_articul_info_price">
							<span class="priceitem" id="price' . $artikul->getId() . '" >' . lang_currency_prepend . ' ' . $price[0] . '</span><sup style="font-size:8pt;">' . $price[1] . '</sup>' . lang_currency_append;
                        if ($artikul->isPreorder()) {
                            $deposit_price = number_format($_SESSION['cart'][$key]['preorder']['deposit_price'] / CURRENCY_RATE, 2, '.', '');
                            $deposit_price = explode('.', $deposit_price);

                            echo '<br><span style="color: #FB671F; width: 100%; display: inline-block; text-align: center;"><span class="priceitem" id="price' . $artikul->getId() . '" >' . lang_currency_prepend . ' ' . $deposit_price[0] . '</span><sup style="font-size:8pt;">' . $deposit_price[1] . '</sup>' . lang_currency_append . '</span>';
                        }
                        echo '</div>
						</div>
						<a style="height: 25px; padding-left: 5px; padding-right: 5px; line-height: 25px; color: #FF671F; display: inline-block; position: absolute; top: 27px; right: 15px;" href="fancy_login_m.php?p=cart&del=' . $key . '">X</a>
						<div style="clear: both;"></div>';
                    echo '</div>';
                    echo '<div class="articulMobileName"><strong>' . $artikul->getIme_marka() . '</strong> ' . $artikul->getIme() . ' ' . $option;
                    if (isset($_SESSION['cart'][$key]['upgrade_option'])) {
                        foreach ($_SESSION['cart'][$key]['upgrade_option'] as $upgrade_option) {
                            echo "<div>" . $upgrade_option['upgrade_name'] . ": " . $upgrade_option['name'] . "</div>";
                        }
                    }
                    if ($artikul->isPreorder()) {
                        echo '<br><span style="color: #FB671F;margin-top: 2px;">with pre-order <br>advance payment  ' . $_SESSION['cart'][$key]['preorder']['deposit_percent'] . '%</span>';
                    }
                    echo '</div>';
                    echo '</div>';
                    if ($add_artikuli != '') {
                        $add_art_arr = explode(",", $add_artikuli);
                        $n = 0;
                        foreach ($add_art_arr as $dd) {
                            $n++;
                        }
                    }
                    if ($add_artikuli != '') {
                        $add_art_arr = explode(",", $add_artikuli);
                        $d = 0;
                        foreach ($add_art_arr as $dd) {
                            $dr = new artikul((int)$dd);
                            if (empty($dr->getId())) continue;
                            $link = 'index.php';
                            $link .= '?id=' . $dr->getid() . '';
                            $link .= '&category=' . $dr->getid_kategoriq() . '';
                            $link .= '&mode=' . $dr->getid_vid() . '';
                            $option = '';
                            $flag = false;
                            if (isset($_SESSION['cart'][$key]['options'])) {
                                foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                    $expl_arr = explode(",", $optionVal);
                                    foreach ($expl_arr as $keyy => $vall) {
                                        if ($keyy == 0) {
                                            $artikulid = (int)$vall;
                                        }
                                        if ($keyy == 1 && $artikulid == (int)$dd) {
                                            $option = "<strong> - " . $vall . "</strong>";
                                            $flag = true;
                                        }
                                    }
                                }
                            }
                            $d++;
                            if ($n == $d) {
                                $style = "";
                            } else {
                                $style = "style='border-bottom: none; margin-bottom:0px;'";
                            }
                            echo '
	                        <div class="Cartplus" style="text-align:left;margin-top:0px;">+</div>
	                        <div class="articulCart" ' . $style . '>
							<div class="articul_img" style="display: block; float: left">
								<a href="' . urlm . '/' . $link . '"><img alt="' . stripslashes($dr->getIme()) . '" src="' . $dr->getKartinka_t() . '"></a>
							</div>
							<div class="cart_articul_info">
								<div class="cart_articul_info_name"><strong>' . $dr->getIme_marka() . '</strong> ' . $dr->getIme() . ' ' . $option . '</div>
								<div class="cart_item_options">
                                <div style="width:31px;height:10px;float:left;"></div>
								<input style="width:25px;text-align: center; margin: 0 0 4px;border: 1px solid #999999;border-radius:0px;font-family: Verdana;font-size: 11pt;color: #808080;padding: 0;" type="text" value="1" id="list5582_989445453" readonly="">
								</div>
							</div>
							<div style="clear: both;"></div>
							</div>
	                        ';
                        }
                    }
                    if ($artikul->isPreorder()) {
                        $allPreorder += $_SESSION['cart'][$key]['preorder']['deposit_price'] * ((int)$_SESSION['cart'][$key]['count']);
                        $all += $_SESSION['cart'][$key]['price'] * ((int)$_SESSION['cart'][$key]['count']);
                        $all /= CURRENCY_RATE;
                        $allPreorder /= CURRENCY_RATE;
                    } else {
                        $all += $_SESSION['cart'][$key]['price'] * ((int)$_SESSION['cart'][$key]['count']);
                        $allPreorder += $_SESSION['cart'][$key]['price'] * ((int)$_SESSION['cart'][$key]['count']);
                        $all /= CURRENCY_RATE;
                        $allPreorder /= CURRENCY_RATE;
                    }
                } else if ($_SESSION['cart'][$key]['type'] == "group") {
                    $group = new paket((int)$key);
                    $artikuli = $group->getProducts();
                    $price = number_format($_SESSION['cart'][$key]['price'] / CURRENCY_RATE, 2, '.', '');
                    $price = explode('.', $price);
                    $line = "";
                    $n = 0;
                    echo '<div class="bundle">';
                    foreach ($artikuli as $dd) {
                        $dr = new artikul((int)$dd);
                        $link = 'index.php';
                        $link .= '?id=' . $dr->getid() . '';
                        $link .= '&category=' . $dr->getid_kategoriq() . '';
                        $link .= '&mode=' . $dr->getid_vid() . '';
                        if ($n != 0) echo '<span style=" margin-left: 60px; font-size: 17px; height: 40px; display: inline-block; color: #424242;">+</span>';
                        echo '
                    <div class="articulCartGroup">
                        <div class="articul_img" style="display: block; float: left">
                            <a href="' . urlm . '/' . $link . '"><img alt="' . stripslashes($dr->getIme()) . '" src="' . $dr->getKartinka_t() . '" style="max-width:110px; max-height: 135px;" /></a>
                        </div>
                        <div class="cart_articul_info">
                            <div class="cart_articul_info_name">
                                <strong style="font-size:10pt;color:#000;">' . $dr->getIme_marka() . '</strong>
                                <span style="font-size:10pt;color:#424242" class="prodcat">' . $dr->getIme() . '</span>';
                        $flag = false;
                        if (isset($_SESSION['cart'][$key]['options'])) {
                            foreach ($_SESSION['cart'][$key]['options'] as $optionkey => $optionVal) {
                                $expl_arr = explode(",", $optionVal);
                                foreach ($expl_arr as $keyy => $vall) {
                                    if ($keyy == 0) {
                                        $artikulid = (int)$vall;
                                    }
                                    if ($keyy == 1 && $artikulid == (int)$dd) {
                                        echo ' - <strong>' . $vall . '</strong>';
                                        $flag = true;
                                    }
                                }
                            }
                        }
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                        $n++;
                    }
                    $line = "border-bottom: solid 1px #999;";
                    echo '
            <div class="articulCart">
                <div class="articul_img" style="display: block; float: left"></div>

                <div class="cart_articul_info">
                    <div class="cart_articul_info_name" style="font-size:10pt;color:#424242" class="prodcat">' . $group->getName() . '</div>
                    <div class="cart_item_options">
                    <div id="remove_qty_' . $key . '" class="qty remove_qty" onclick="decreaseQty(\'' . $key . '\')">-</div>
                    <input style="border: 1px solid #999999;font-family: Verdana;font-size: 11pt;color: #808080; padding: 0 0 0 3px;width:15px; margin: 0 0 4px;" type="text" onKeyUp="change(this.id, this.value);" value="' . $_SESSION['cart'][$key]['count'] . '" id="list' . $key . '" NAME="count' . $group->getId() . '" readonly/>
                    <div class="qty add_qty" onclick="increaseQty(\'' . $key . '\')">+</div>
                    </div>
                    <div class="cart_articul_info_price"><span class="priceitem" id="price' . $group->getId() . '" >' . lang_currency_prepend . ' ' . $price[0] . '</span><sup style="font-size:8pt;">' . $price[1] . '</sup>' . lang_currency_append . '</div>
                </div>
                <a style="height: 25px; padding-left: 5px; padding-right: 5px; line-height: 25px; color: #333; background-color: #E0E0E0; display: inline-block; position: absolute; top: 27px; right: 15px;" href="fancy_login.php?p=cart&del=' . $key . '">X</a>
            </div><div style="clear: both;"></div></div>';
                    $all += ($_SESSION['cart'][$key]['price']) * ((int)$_SESSION['cart'][$key]['count']);
                    $allPreorder += ($_SESSION['cart'][$key]['price']) * ((int)$_SESSION['cart'][$key]['count']);
                    $all /= CURRENCY_RATE;
                    $allPreorder /= CURRENCY_RATE;
                }
            }
            echo '</div>';
        }
        ?>
        <div style="clear:both;height:20px;"></div>
        <?php
        if (!isset($_SESSION['cart']) || (sizeof($_SESSION['cart']) <= 0))
            echo $nots;

        $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
        $ds->execute();
        $d = $ds->fetch();
        $dostavka_cena = $d['delivery_price'];
        $dostavka_free = $d['free_delivery'];

        if (isset($_SESSION['cart']) && (sizeof($_SESSION['cart']) > 0)) {
            if ($dostavka_free > $all) {
                if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
                    $dostavka_cena += $_SESSION['cart']['total_additional_delivery'];
                }

                $all += $dostavka_cena;
                $tmp = number_format($dostavka_cena, 2, '.', '');
                $tmp = explode('.', $tmp);
                ?>
                <!--                            <span id="allcost_delivery" style="margin-top:0px;font-size:12pt;width:150px;float:right;">delivery <span style="font-style:italic;">--><?php //echo $tmp[0];
                ?><!--</span><sup style="font-style:italic;font-size:8pt;">--><?php //echo $tmp[1];
                ?><!--</sup> --><?php //echo lang_currency_append;
                ?><!--</span>-->
                <?php
            } else {
//                        if (isset($_SESSION['cart']['total_additional_delivery']) && $_SESSION['cart']['total_additional_delivery'] > 0) {
//                            $tmp=number_format($_SESSION['cart']['total_additional_delivery'], 2, '.', '');
//                            $tmp= explode('.',$tmp);
//                            $delivery_text = '<span id="allcost_delivery" style="margin-top:0px;font-size:10pt;width:150px;float:right;">delivery <span style="font-style:italic;">' . $tmp[0] . '</span><sup style="font-style:italic;font-size:8pt;">' . $tmp[1] . '</sup> ' . lang_currency_append . '</span>';
//                        } else {
//                            $delivery_text = '<span id="allcost_delivery" style="margin-top:0px;font-size:10pt;width:150px;float:right;">free delivery</span>';
//                        }
//
//                        echo $delivery_text;
            }
            $total = number_format($allPreorder, 2, '.', '');
            $total = explode('.', $total);
            ?>
            <div style="height:30px;"></div>
            <span id="allcost" style="margin-top:0px;font-size:12pt;width: 165px;float:right;margin-right: 10px;">total <span
                        style="font-style:italic;"><?php echo lang_currency_prepend; ?><?php echo $total[0]; ?></span><sup
                        style="font-style:italic;font-size:8pt;"><?php echo $total[1]; ?></sup> <?php echo lang_currency_append; ?></span>
            <div style="clear: both; height: 10px;"></div>
            <?php
        }
        if ((isset($_SESSION['cart'])) && (!empty($_SESSION['cart']))) {
            if (isset($__user)) {
                ?>
                <INPUT TYPE="submit" style="border: none; color: #fff; width: 165px;" class="submit_button" id="submitshoppingcard"
                       href="fancy_login_m.php?p=ordrdetails" value="finalizare comanda"/ >
                <?php
            } else {
                ?>
                <a id="submitshoppingcard" class="submit_button" href="fancy_login_m.php?p=login">finalizare comanda</a>
                <?php
            }
            ?>
            <a onclick="history.back(1);"
               style="display: block;background-color: #FF671F; color: #FFF;float: left;font-size: 11pt;text-decoration: none;padding-left: 10px;padding-right: 10px;height: 35px;text-align: center; cursor: pointer;">continua<br>
                cumparaturile</a>
            <?php
        }
        ?>
        <div style=" clear: both; height: 20px"></div>
    </FORM>
</div>
