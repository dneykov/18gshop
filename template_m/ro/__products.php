<?php
function print_product($v)
{
    global $__user;
    $isAvaliable = $v->isAvaliable();
    $isOnline = $v->isOnline();
    ?>
    <div class='articul'>
        <div class="promo">
            <?php if($isAvaliable && $isOnline) : ?>
                <?php
                if(($v->getCena_promo())){
                    $tmpraz=($v->getCena() - $v->getCena_promo() );
                    if ($v->IsNew()) {
                        echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; right: 20px; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                    } else {
                        echo '<div style="float: left; background-color: #FF671F; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                        <span class="rotate-90">' . '-' . round((($tmpraz/$v->getCena())*100),0) . '%' . '</span>
                        </div>';
                    }
                }

                if($v->getAddArtikuli() != ""){
                    echo '<div style="float: left; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <img src="'.url.'images/icons/gift.png" alt="promo" />
                                    </div>';
                }

                if($v->isPreorder() != ""){
                    echo '<div style="float: left; background-color: #29AC92; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90">PRE</span>
                                    </div>';
                }

                if($v->IsNew()){
                    echo '<div style="float: left; background-color: #01A0E2; width: 16px; height: 40px; color: #fff; margin-left: 5px;">
                                    <span class="rotate-90" style="font-size: 11px; padding-top: 17px;">NEW</span>
                                    </div>';
                }
                ?>
            <?php endif; ?>
        </div>
        <div class='articul_img'>
            <table class="imageCenter">
                <tbody>
                <tr>
                    <td>
                        <a href="index.php?id=<?php echo $v->getId() . remove_querystring_var('&' . $_SERVER['QUERY_STRING'], 'id'); ?>"><img
                                    alt="" src="<?php echo url . $v->getKartinka_t(); ?>" border="0"/></a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class='articul_info'>
            <div class='articul_info_name'><?php echo $v->getIme_marka(); ?> / <?php echo $v->getIme(); ?></div>

            <div class="proddiv-specification">
                <?php
                $linesSpecification = explode(PHP_EOL, $v->getSpecification());
                $newLinesSpecification = implode(PHP_EOL, array_slice($linesSpecification, 0, 2)) . PHP_EOL;
                if (count($newLinesSpecification) > 1) {
                    echo nl2br($newLinesSpecification);
                }
                ?>
            </div>

            <?php if (isset($__user) && $__user->is_partner()) : ?>
                <?php if ($v->getCenaDostavna() > 0) : ?>
                    <div class="articul_info_price"><span
                                style="margin-top:8px; font-size:12pt; font-style: italic; "><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCenaDostavna() / CURRENCY_RATE, 2, '.', ''));
                            echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                <?php else : ?>
                    <?php if ($v->getCena_promo()) : ?>
                        <?php if ($v->isPreorder()) : ?>
                            <div class="articul_info_price"><span
                                        style="font-size:12pt;font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                            <div class="articul_info_price"><span
                                        style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                        <?php else : ?>
                            <div class="articul_info_price"><span
                                        style="font-size:12pt;font-style: italic;color:#fe1916;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                            <div class="articul_info_price"><span
                                        style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                        <?php endif; ?>
                    <?php else : ?>
                        <?php if ($v->isPreorder()) : ?>
                            <div class="articul_info_price"><span
                                        style="margin-top:8px; font-size:12pt; font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                        <?php else : ?>
                            <div class="articul_info_price"><span
                                        style="margin-top:8px; font-size:12pt; font-style: italic; "><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php else : ?>
                <?php if ($v->getCena_promo()) : ?>
                    <?php if ($v->isPreorder()) : ?>
                        <div class="articul_info_price"><span
                                    style="font-size:12pt;font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                        <div class="articul_info_price"><span
                                    style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                    <?php else : ?>
                        <div class="articul_info_price"><span
                                    style="font-size:12pt;font-style: italic;color:#fe1916;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena_promo() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                        <div class="articul_info_price"><span
                                    style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup></span><span
                                    style="font-size:10px;"><?php echo lang_currency_append; ?></span></div>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if ($v->isPreorder()) : ?>
                        <div class="articul_info_price"><span
                                    style="margin-top:8px; font-size:12pt; font-style: italic;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                    <?php else : ?>
                        <div class="articul_info_price"><span
                                    style="margin-top:8px; font-size:12pt; font-style: italic; "><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getCena() / CURRENCY_RATE, 2, '.', ''));
                                echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                                        style="font-size:10px;"><?php echo lang_currency_append; ?></span></span></div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <div style="clear: both;"></div>
        </div>
    </div>
    <?php
}

?>
<div id="content">
    <div id="articuli">
        <?php
        if (isset($artikuli) and (sizeof($artikuli) > 0) && (isset($artikul))) {
            if (isset($kategoriq)) {
                echo mb_strtolower($kategoriq->getIme(), 'UTF-8');
                if (isset($vid)) {
                    echo ' ' . mb_strtolower($vid->getIme(), 'UTF-8');
                }
                if (isset($model)) {
                    echo ' марка ' . mb_strtolower($model->getIme(), 'UTF-8');
                }
                if (isset($etiketi_grupi)) {
                    foreach ($etiketi_grupi as $v) {
                        if (($v->getEtiketi())) {
                            foreach ($v->getEtiketi() as $e) {
                                if ($etiket && in_array($e->getId(), $etiket)) {
                                    echo ' ' . mb_strtolower($v->getIme(), 'UTF-8') . ' ' . mb_strtolower($e->getIme(), 'UTF-8');
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!$artikuli) { ?><?php } else {
            echo '<div style="overflow:hidden;" class="all_products">';
            foreach ($artikuli as $v) {
                if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                if (isset($__user) && !$__user->is_partner() && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                print_product($v);
            } // foreach artikuli
            echo '</div>';
        } // ako ima artikuli

        $need_position = $min_prod_per_page - $artikul_broi;
        if ($artikul_broi < $min_prod_per_page) {
            if (isset ($_GET['hotOffers'])) {
                $tmppromo = true;
            } else {
                $tmppromo = false;
            }
            if (isset($etiket)) {
                if (isset($kategoriq) && isset($vid) && isset($model)) {
                    $add_artikuli = getArticuli($kategoriq, $vid, NULL, $need_position, $artikuli_idta, $etiket, 'id', $tmppromo);
                }
            } else {
                if (isset($kategoriq) && isset($vid) && isset($model)) {
                    $add_artikuli = getArticuli($kategoriq, $vid, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
                    if (!isset($add_artikuli) || ($add_artikuli['count'] <= 0)) {
                        $add_artikuli = getArticuli($kategoriq, NULL, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
                    }
                } else if (isset($kategoriq) && isset($vid)) {
                    $add_artikuli = getArticuli($kategoriq, NULL, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
                } else if (isset($kategoriq) && isset($model)) {
                    $add_artikuli = getArticuli($kategoriq, NULL, NULL, $need_position, $artikuli_idta, null, 'id', $tmppromo);
                }
            }
            if (isset($add_artikuli)) {
                $need_position -= $add_artikuli['count'];
            }

        }
        if (isset($add_artikuli) && ($add_artikuli['count'] > 0)) {
            echo '<div style="clear: both; height: 20px;"></div><div style="position: relative;background: none repeat scroll 0 0 #FFFFFF;color:#808080;text-align: left;"><div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">' . $add_artikuli['title'] . '</div></div>';
            foreach ($add_artikuli['itams'] as $v) {
                if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                if (isset($__user) && !$__user->is_partner() && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                print_product($v);
            }
        }

        if (($need_position > 0) && (sizeof($last_artikuli) > 0)) {
            echo '<div style="clear: both; height: 20px;"></div><div style="position: relative;background: none repeat scroll 0 0 #FFFFFF;color:#808080;text-align: left;"><div style="padding-right:10px;background:#FFFFFF;display:inline-block;position: relative;z-index: 1;">newest products</div></div>';
            $shown = 0;
            foreach ($last_artikuli as $v) {
                if (!isset($__user) && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                if (isset($__user) && !$__user->is_partner() && $v->getCenaDostavna() > 0 && $v->getCena() == 0) continue;

                print_product($v);
            }
        }
        ?>
    </div>
</div>
<div style="clear: both;"></div>
