<style type="text/css">
    #cont {
        margin: 5px auto;
        text-align: left;
        padding-bottom: 50px;
        font-size: 14px;
    }

    #tabs-bar {
        height: 20px;
        width: 100%;
        padding-bottom: 2px;
        border-bottom: 1px solid #FF671F;
        margin-bottom: 20px;
    }

    #tabs-bar a {
        margin-left: 5px;
        margin-right: 20px;
        font-size: 16px;
        color: #333;
    }

    .submit {
        display: block;
        background: none repeat scroll 0 0 #333;
        border: medium none;
        color: #fff;
        cursor: pointer;
        float: right;
        margin: 34px 0 0;
        height: 36px;
        width: 180px;
        line-height: 33px;
        text-align: center;
        text-decoration: none;
        font-size: 13px;
    }

    textarea {
        font-size: 13px;
        border: 1px solid #CCC;
    }

    .orderLink {
        width: 100%;
        height: 23px;
        color: #fff;
        cursor: pointer;
        background-color: #FF671F;
        margin-bottom: 10px;
        line-height: 23px;
        font-size: 12px;
    }

    .orderLink span {
        margin-left: 10px;
        margin-right: 10px;
    }

    .orderBox {
        padding: 5px;
        margin-bottom: 20px;
    }

    .orderCostBox {
        padding-bottom: 30px;
    }

    .orderDelivery {
        font-size: 13px;
        color: #333;
        margin-bottom: 5px;
        text-transform: uppercase;
    }

    .payment {
        font-size: 13px;
        color: #333;
        margin-bottom: 5px;
    }

    .orderCost {
        font-size: 13px;
        color: #333;
        margin-bottom: 5px;
        text-transform: uppercase;
    }

    .orderCommentBox {
        margin-bottom: 20px;
    }

    .orderComment {
        height: 130px;
        width: 100%;
        display: inline-block;
    }

    .orderComment:first-child {
        margin-right: 30px;
    }

    .orderCommentLabel {
        font-size: 12px;
        margin-bottom: 5px;
    }

    .orderTextarea {
        max-width: 470px;
        height: 100px;
    }

    .orderProductAdditional {
        position: absolute;
        left: 0px;
        top: 70px;
        font-weight: bold;
        font-size: 18px;
        color: #333333;
    }

    .articul {
        position: relative;
    }

    .orderProductPrice {
        float: right;
        width: 85px;
        display: inline-block;
    }
</style>
<div id="where_am_i_box">
    <div id="where_am_i">
        <a href="<?php echo urlm; ?>" style="color: #333;">18gshop</a> <span style="color: #333;">/</span>
        <a href="<?php echo urlm . 'index.php?user'; ?>" style="color: #333;">cont</a> <span
                style="color: #333;">/</span>
        <a href="<?php echo urlm . 'index.php?user'; ?>" style="color: #333;"><?php echo $__user->getName(); ?></a>
        <span style="color: #333;">/</span>
        <?php if ($__user->is_partner()) : ?>
            <span style="color: #FF671F">partner</span> <span style="color: #333;">/</span>
        <?php endif; ?>
        <a href="<?php echo urlm . 'index.php?user&o'; ?>" style="color: #333;">ordinelor</a>
    </div>
</div>
<div id="tabs-bar">
    <a href="<?php echo urlm . 'index.php?user'; ?>">cont</a>
    <a href="<?php echo urlm . 'index.php?user&o'; ?>" style="color: #FF671F;">ordinelor
        <?php
        $stm = $pdo->prepare("SELECT `id` FROM `orders` WHERE `user` = ? ORDER BY `id` DESC");
        $stm->bindValue(1, $__user->getId(), PDO::PARAM_STR);
        $stm->execute();
        echo $stm->rowCount();
        ?>
    </a>
    <a href="login_m.php?logout=1">logout</a>
</div>
<div id="cont">
    <?php
    if (isset($__user)) {
        if ($stm->rowCount() > 0) {
            foreach ($stm->fetchAll() as $a) {
                $order = new order((int)$a['id']);
                $contactInformation = $order->getContactInformation();
                $orderProducts = $order->getProducts();
                $deletedItems = $order->getDeletedItems();
                $orderCost = $order->getCost();
                ?>
                <div class="orderLink">
							<span style="font-weight: bold;">
								<?php
                                if ($order->getStatus() == 3) {
                                    echo "Еxecutate	ordin / Pre-order delivered";
                                } else if ($order->getStatus() == 2) {
                                    echo "Еxecutate	ordin";
                                } else if ($order->getStatus() == 1) {
                                    echo 'Oferte aprobate';
                                } else if ($order->getStatus() == 4) {
                                    echo 'Ordine anulată';
                                } else if ($order->getStatus() == 5) {
                                    echo 'Ordine returnată';
                                } else {
                                    echo 'Nou ordin';
                                }
                                ?>
							</span>
                    <span><?php echo $order->getNomer() . " "; ?></span>
                    <span><?php echo $order->getDate(); ?></span>
                </div>
                <div class="orderBox" id="order-<?php echo $order->getId(); ?>">
                    <div class="orderProducts">
                        <?php
                        if (isset($orderProducts)) {
                            foreach ($orderProducts as $key => $val) {
                                if (in_array((int)$key, $deletedItems)) {
                                    $orderCost = $orderCost - $orderProducts[$key]['price'] * $orderProducts[$key]['count'];
                                    continue;
                                }
                                if ($orderProducts[$key]['type'] == "product") {
                                    $artikul = new artikul((int)$key);
                                    $option = '';
                                    if (isset($orderProducts[$key]['options'])) {
                                        foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                            $expl_arr = explode(",", $optionVal);
                                            foreach ($expl_arr as $keyy => $vall) {
                                                if ($keyy == 0) {
                                                    $artikulid = (int)$vall;
                                                }
                                                if ($keyy == 1 && $artikulid == $key) {
                                                    if ($artikul->getTaggroup_dropdown()) {
                                                        foreach ($artikul->getTaggroup_dropdown() as $gr) {
                                                            $option .= " / " . $gr->getIme() . " " . $vall;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (isset($orderProducts[$key]['upgrade_option'])) {
                                        foreach ($orderProducts[$key]['upgrade_option'] as $upgrade_option) {
                                            $option .= " / " . $upgrade_option['upgrade_name'] . " " . $upgrade_option['name'];
                                        }
                                    }
                                    echo "<a href=" . urlm . "/index.php?id=" . $artikul->getId() . " target='_blank' style='color: #333;'>";
                                    echo "<div class='articul'>";
                                    echo "<div class='articul_img'>";
                                    echo "<table class='imageCenter'><tr><td><img src='../" . $artikul->getKartinka_t() . "' /></td></tr></table>";
                                    echo "</div>";
                                    echo "<div class='articul_info'><div class='articul_info_name'>" . $artikul->getIme_marka() . " / " . $artikul->getIme() . $option . " / " . $orderProducts[$key]['count'] . "pc.</div>";
                                    echo "<div class='orderProductPrice'>";
                                    ?>
                                    <span class="articul_info_price"
                                          style="font-size:18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format((float)$orderProducts[$key]['price'] / CURRENCY_RATE, 2, '.', ''));
                                        echo $tmp_cen[0]; ?> <sup
                                                style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                                    <?php
                                    echo "</div></div>";
                                    if ($artikul->isPreorder()) {
                                        $tmp_cen = explode(".", number_format($orderProducts[$key]['preorder']['deposit_price'] / CURRENCY_RATE, 2, '.', ''));
                                        $toPayAtDelivery = explode(".", number_format(($orderProducts[$key]['price'] - $orderProducts[$key]['preorder']['deposit_price']) / CURRENCY_RATE, 2, '.', ''));
                                        echo "<div style='clear: both; color: #FF671F;'>with pre-order<br/>
                                                                        advance " . $orderProducts[$key]['preorder']['deposit_percent'] . "% <span style='font-style: italic;'>" . lang_currency_prepend . " " . $tmp_cen[0] . " <sup style='font-size:8pt'>" . $tmp_cen[1] . "</sup> " . lang_currency_append . "</span> " . ($order->getStatus() == 2 || $order->getStatus() == 3 ? '<strong>paid</strong>' : '') . "<br/>
                                                                        left <span style='font-style: italic;'>" . lang_currency_prepend . " " . $toPayAtDelivery[0] . " <sup style='font-size:8pt'>" . $toPayAtDelivery[1] . "</sup> " . lang_currency_append . "</span> " . ($order->getStatus() == 3 ? '<strong>paid</strong>' : 'at delivery') . "</div>";
                                    }
                                    echo "</div></a>";
                                    $dop_artikuli = $artikul->getAddArtikuli();
                                    if ($dop_artikuli != "") {
                                        $dop_art_arr = explode(",", $dop_artikuli);
                                        foreach ($dop_art_arr as $dart) {
                                            $dr = new artikul((int)$dart);
                                            $option = '';
                                            $n = 0;
                                            if (isset($orderProducts[$key]['options'])) {
                                                foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                                    $expl_arr = explode(",", $optionVal);
                                                    foreach ($expl_arr as $keyy => $vall) {
                                                        if ($keyy == 0 && $vall == $dr->getId()) {
                                                            $artikulid = (int)$vall;
                                                        }
                                                        if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                                            $stm = $pdo->prepare("SELECT * FROM `etiketi` WHERE `id` = :id LIMIT 1");
                                                            $stm->bindValue(':id', (int)$vall, PDO::PARAM_STR);
                                                            $stm->execute();
                                                            $etk = $stm->fetch();
                                                            $etkk = $stm->rowCount();
                                                            if ($etkk > 0) {
                                                                $temp_etiket = $temp = new etiket((int)$vall);
                                                                $option = " / размер " . $temp_etiket->getIme();
                                                            }
                                                            $n = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            echo "<a href=" . urlm . "/index.php?id=" . $dr->getId() . " target='_blank' style='color: #333;'>";
                                            echo "<div class='articul'>";
                                            echo "<div class='articul_img'>";
                                            echo "<table class='imageCenter'><tr><td><img src='" . url . $dr->getKartinka_t() . "' /></td></tr></table>";
                                            echo "</div>";
                                            echo "<div class='articul_info'><div class='articul_info_name'>" . $dr->getIme_marka() . " / " . $dr->getIme() . $option . " / 1бр.</div>";
                                            echo "</div>";
                                            echo "</div></a>";
                                        }
                                    }
                                } else {
                                    $group = new paket((int)$key);
                                    $dop_art_arr = $group->getProducts();

                                    echo "<a href=" . urlm . "index.php?gid=" . $group->getId() . " target='_blank' style='color: #333;'>";
                                    echo "<div class='articul'>";
                                    echo "<div class='articul_img'>";
                                    echo "<table class='imageCenter'><tr><td><img src='" . urlm . $group->getKartinka_t() . "' /></td></tr></table>";
                                    echo "</div>";
                                    echo "<div class='articul_info'><div class='articul_info_name'>" . $group->getName() . " / " . $orderProducts[$key]['count'] . "pcs.</div>";
                                    echo "<div class='orderProductPrice' style='width: 120px;'>";
                                    ?>
                                    <span class="articul_info_price"
                                          style="font-size:18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format((float)$orderProducts[$key]['price'] / CURRENCY_RATE, 2, '.', ''));
                                        echo $tmp_cen[0]; ?> <sup style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                                    <?php
                                    echo "</div></div>";
                                    echo "</div></a>";
                                    foreach ($dop_art_arr as $dart) {
                                        $dr = new artikul((int)$dart);
                                        $option = '';
                                        $n = 0;
                                        if (isset($orderProducts[$key]['options'])) {
                                            foreach ($orderProducts[$key]['options'] as $optionkey => $optionVal) {
                                                $expl_arr = explode(",", $optionVal);
                                                foreach ($expl_arr as $keyy => $vall) {
                                                    if ($keyy == 0 && $vall == $dr->getId()) {
                                                        $artikulid = (int)$vall;
                                                    }
                                                    if ($keyy == 1 && $artikulid == $dr->getId() && $n == 0) {
                                                        $option = " / size " . $vall;
                                                        $n = 1;
                                                    }
                                                }
                                            }
                                        }
                                        echo "<a href=" . urlm . "index.php?id=" . $dr->getId() . " target='_blank' style='color: #333;'>";
                                        echo "<div class='articul'>";
                                        echo "<div class='orderProductAdditional'>+</div>";
                                        echo "<div class='articul_img'>";
                                        echo "<table class='imageCenter'><tr><td><img src='" . urlm . $dr->getKartinka_t() . "' /></td></tr></table>";
                                        echo "</div>";
                                        echo "<div class='articul_info'><div class='articul_info_name'>" . $dr->getIme_marka() . " / " . $dr->getIme() . $option . " / " . $orderProducts[$key]['count'] . "pcs.</div>";
                                        echo "</div></div></a>";
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="orderCommentBox">
                        <?php
                        if ($order->getComment() != "") {
                            echo '<div class="orderComment">';
                            echo '<div class="orderCommentLabel">comment</div>';
                            echo '<textarea class="orderTextarea" disabled>' . $order->getComment() . '</textarea>';
                            echo '</div>';
                        }
                        if ($order->getAdminComment() != "") {
                            echo '<div class="orderComment">';
                            echo '<div class="orderCommentLabel" style="color: #FF671F;">comment from us</div>';
                            echo '<textarea class="orderTextarea" disabled>' . $order->getAdminComment() . '</textarea>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                    <div class="orderCostBox">
                        <div class="payment">Payment type:
                            <span style="font-size:14px;color: #FF671F; display: inline-block; text-transform: none;">
                                    <?php
                                    switch ($order->getPaymentType()) {
                                        case "cash":
                                            echo "Pay at delivery";
                                            break;
                                        case "bank":
                                            echo "Bank transfer";
                                            break;
                                        default:
                                            echo "Pay at delivery";
                                            break;
                                    }
                                    ?>
                                    </span>
                        </div>
                        <?php if ($order->getPaymentType() == "bank") : ?>
                            <div class="payment">IBAN
                                <span style="font-size:14px;color: #FF671F; display: inline-block;text-transform: none;"><?php echo $order->getIban(); ?></span>
                            </div>
                        <?php endif; ?>
                        <?php if ($order->getPaymentType() == "bank") : ?>
                            <div class="payment">Bank details:
                                <br>
                                <span style="font-size:14px;color: #FF671F; display: inline-block;text-transform: none;"><?php echo nl2br($order->getBankDetails()); ?></span>
                            </div>
                        <?php endif; ?>
                        <div class="orderDelivery">delivery
                            <?php if ($contactInformation['delivery'] === 'pickup'): ?>
                                <span style="color: #FF671F;font-size:14px; display: inline-block;text-transform: none;">Pickup from us</span>
                            <?php else: ?>
                                <span style="font-size:18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format((int)$contactInformation['delivery'] / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                            <?php endif; ?>
                        </div>
                        <div class="orderCost">total
                            <?php if ($order->isPreorder()) : ?>
                                <span style="font-size: 18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($order->getPreorderCost() / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                            <?php else : ?>
                                <span style="font-size: 18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($orderCost / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                            <?php endif; ?>
                            <?php
                            if ($order->getPaypal() == 1) {
                                echo "<br />Paypal";
                                if ($order->getPaypalPaid() == 1) {
                                    echo ": paid";
                                }
                            }
                            ?>
                        </div>
                        <?php if ($order->isPreorder()) : ?>
                            <div class="orderCost">left
                                <span style="font-size: 18px;font-style: italic; display: inline-block;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format(($orderCost - $order->getPreorderCost()) / CURRENCY_RATE, 2, '.', ''));
                                    echo $tmp_cen[0]; ?> <sup
                                            style="font-size:8pt"><?php echo $tmp_cen[1]; ?></sup><span
                                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                                <?php if ($order->getStatus() == 3) : ?>
                                    <span style="color: #FF671F; font-size: 12px;"><strong>paid</strong></span>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
            }
        } else {
            echo "<div class='serctionTitle'>You dont have orders.</div>";
        }
    } else {
        echo "<center>Please log in your account</center>";
    }
    ?>
</div>