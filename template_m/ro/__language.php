<?php

define('lang_login_error_pass_or_mail', 'Vă rugăm să introduceți adresa de email sau parola valida!');

define('lang_login_register_error_mail_lenght_long', 'E-mail este prea lung');
define('lang_login_register_error_mail_invalid', 'Adresa de email nu este valida');
define('lang_login_register_error_mail_already_used', 'mail@mail.com exista deja un cont care are asociata acesta adresa de email, va rugam folositi o alta adresa de email');

define('lang_login_register_error_pass_lenght_short', 'Parola trebuie sa contina cel putin 4 caractere');
define('lang_login_register_error_pass_lenght_long', 'Parola trebuie sa contina maxim 200 de caractere');

define('lang_login_register_error_name_last_lenght_short', 'Numele trebuie sa contina cel putin 3 caractere');
define('lang_login_register_error_name_last_lenght_long', 'Numele trebuie sa contina maxim 200 de caractere');
define('lang_login_register_error_name_first_lenght_short', 'Prenumele trebuie sa contina cel putin 3 caractere');
define('lang_login_register_error_name_first_lenght_long', 'Prenumele trebuie sa contina maxim 200 de caractere');

define('lang_login_register_complete', 'zapisana reg');

define('lang_login_logout', 'logout');
define('lang_login_login', 'log in');

define('lang_error_passwords_not_match', 'wrong password confirmation');

define('lang_login_register_error_terms', 'va rugam acceptati termenii si conditiile');
define('lang_login_register_error_privacy', 'processing privacy data should be accepted');

define('lang_order_address_data_error_gsm', 'Numarul de telefon nu este valid');
define('lang_order_address_data_error_home_telephone', 'Numarul de telefon nu este valid');
define('lang_order_address_data_error_city', 'orasul nu este valid');
define('lang_order_address_data_error_street', 'strada nu este valida');
define('lang_order_address_data_error_number', 'numarul strazii nu este valid');
define('lang_order_address_data_error_vhod', 'intrare nu este valid');
define('lang_order_address_data_error_etaj', 'podea nu este valid');
define('lang_order_address_data_error_apartament', 'apartament nu este valid');

define('lang_success_save', 'changes have been saved');

define('lang_order_success_send', 'Your order is accepted');
define('lang_order_success_send_email', 'Order information is sent to ');
define('lang_currency_append', 'Lei');
define('lang_currency_prepend', '');
define('lang_currency_coef', 1);
define('lang_order_comment_text',"Invoice or any additional information relating to order.");
define('lang_order_subject','Accepted order of ');
define('lang_order_approved', 'Comanda aprobată de la ');
define('lang_order_approved_msg1', 'Comanda dvs. a fost aprobată și va fi livrată.');
define('lang_order_approved_msg2', 'Ca un client important pentru noi, vă oferim produse special selectate la alegere.');
define('lang_order_msg1',"Your order will be processed within 4 working days and delivered to the address, specified by you. You can easily track the status of your order from your account. Thanks for buying from us!");
define('lang_from','from');
define('lang_order_msg_nom',"Your order number is: ");
define('lang_order_msg1_1',"You can easily track the status of your order from your account.");
define('lang_order_msg_kod',"Product code");
define('lang_order_msg_art',"Produs");
define('lang_order_msg_opt',"Cod");
define('lang_order_msg_br',"Cantitate");
define('lang_order_msg_pr',"Pret");
define('lang_order_msg_preorder',"Residue*");
define('lang_order_msg_deposit',"Advance*");
define('lang_order_msg_total',"Total");
define('lang_order_msg_total2',"Total value of the contract:");
define('lang_free_shipping',"gratuit");
define('lang_order_deliv',"Transport");
define('lang_order_delivery',"delivery:");
define('with_free_delivery',"free delivery");
define('with_free_delivery_cart',"free delivery");
define('total_cart',"total");
define('lang_order_tmp_cash',"Comanda va fi livrata la adresa. Plata produsului comandat va fi facuta prin cadrul firmei de curierat.");
define('lang_order_msg3_cash',"Va rugam sa ne contactati in cazul in care exista intarzieri sau nereguli legate de expediere.");
define('lang_order_tmp_bank',"Payment method: Bank transfer (transfer total amount, including the cost of delivery, to the specified bank account)");
define('lang_order_tmp_bank_details',"Bank transfer details:");
define('lang_order_msg3_bank',"Your order will be delivered to your address within two business days of receipt of the payment. Upon receipt of the shipment, you owe nothing to the courier.");
define('lang_order_preorde_desc',"* numai pentru produse cu comanda prealabila.");
define('lang_size', 'size:');
define('cash_on_delivery', 'Pay at delivery');
define('bank_transfer', 'Bank transfer');
define('payment_type', 'Payment type:');
define('iban', 'IBAN:');
define('bank_details', 'bank details:');
define('lang_order_company_name_short', 'company name must be at least 3 characters');
define('lang_order_company_name_long', 'company name should be up to 200 characters');
define('lang_order_company_city_error', 'not valid company city');
define('lang_order_company_street_error', 'not valid company address');
define('lang_order_company_mol_error', 'not valid f.l.p');
define('lang_order_company_eik_vat_error', 'not valid bulstat or vat');
define('pickup_delivery', 'Pickup from us');
define('lang_enduser_ref_price', 'End user:');
define('lang_in', 'in');