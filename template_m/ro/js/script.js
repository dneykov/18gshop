jQuery(function($){
    $("#loginemail").watermark("E-mail");
    $("#loginpassword").watermark("Password");
    $("#regfirstname").watermark("First name*");
    $("#reglastname").watermark("Last name*");
    $("#regemail").watermark("E-mail");
    $("#regpassword").watermark("Password*");
    $("#regpasswordagain").watermark("Password again*");
    $("#reggsm").watermark("GSM*");
    $("#regtown").watermark("City*");
    $("#regstreet").watermark("Street*");
    $("#regstreetnumber").watermark("number*");
    $("#regentrance").watermark("en.");
    $("#regfloor").watermark("fl.");
    $("#regflat").watermark("ap.");
    $("#contname").watermark("Name");
    $("#contmail").watermark("E-mail or GSM");
    $("#conttitle").watermark("Subject");
    $("#conttext").watermark("Message");

    $("#company_name").watermark("Company name*");
    $("#company_mol").watermark("f.l.p*");
    $("#company_city").watermark("City*");
    $("#company_vat").watermark("Bulstat*");
    $("#company_eik").watermark("Vat*");
    $("#company_street").watermark("Street*");
    $("#company_number").watermark("Number");
    $("#company_phone").watermark("Phone");

    $('#slider').slick({
        autoplay: false,
        autoplaySpeed: 3000,
        arrows : false,
    });

    $('#footer-newsletter #submit').click(function (e) {
        e.preventDefault();
        var email = $('#email').val();
        var err = false;
        if( !validateEmail(email) || email == '') {
            $('.response').html('<span class="error">Моля въведете валиден имейл адрес.</span>');
            err = true;
        }
        if(err == false) {
            $.ajax({
                type: "POST",
                url: "ajax/subscribe.php",
                data: "email=" + email,
                cache: false,
                success: function (data) {
                    if(data != '') {
                        $('.response').html(data);
                        $('#email').val('');
                    }
                }
            });
        }
    });
});

function toggleCont(x, el) {
    if($('.category_drop_box').is(':visible')) {
        $('.category_drop_box').slideUp("fast");
        $('#bread a').css('color', '#ffffff');
    }
    if($('#'+x).is(":hidden")) {
        $('#'+x).slideDown("fast");
        $(el).css('color', '#FF671F');
    } else {
        $('#'+x).hide();
        $(el).css('color', '#ffffff');
    }
}
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}
function showManual() {
    $('#imgmanual').slideDown("fast");
    $('#showmanuallink').hide();
    $('#hidemanuallink').slideDown("fast");
};
function hideManual() {
    $('#imgmanual').hide();
    $('#showmanuallink').slideDown("fast");
    $('#hidemanuallink').hide();
};
function mini_search_ajax(a){
	koe = $('#suggestions');
	koe_ime = '#suggestions';
	koe.html('');
	koe.fadeIn(500);
	$.ajax({
        url:"ajax/search.php",
        type:"GET",
        data:"search="+urlencode_po_taka(a),
        success:function(msg){
		rez=false;
		txt = '';
		txt += ('<a href="#" style="color:#333; text-decoration:none; font-weight:bold; margin-top: 10px; margin-bottom: 10px; margin-right: 5px; float:right;" onclick="mini_search_ajax_close(\''+koe_ime+'\');return false;"><img src="../../../images/nav/X.png"></a>');
		txt += ('<div style="clear: both;"></div>');
		txt += ('<div style="width: 100%; color: #333">');
		tmpi=0;
        $(msg).find("model").each(function()
        {
			tmpi++;
			if(tmpi==1){
				txt += ('<a style="color: #333" href="index.php?id=' + ($(this).attr("url")) + '" >');
				txt += ('<div class="articul" style="height: 200px;">');
			}
			rez=true;
			txt += ('<div class="articul_img">');
				txt += ('<table class="imageCenter"><tbody><tr><td>');
					txt += ('<img src="');
					txt += ($(this).attr("image"));
					txt += ('" border="0" />');
				txt += ('</td></tr></tbody></table>');
			txt += ('</div> <div class="articul_info">');
				txt += ('<div class="articul_info_name">');
				txt += ($(this).attr("marka")+' / ');
				txt += ($(this).attr("name"));
				txt += ('</div>');
			txt += ('<div style="clear: both;"></div></div>');
			tmpi++;
			if(tmpi==2){
				tmpi=0;
				txt += ('</div></a>');
			}
		});
		txt += ('<div style="clear: both;"></div></div>');
		if(rez){
			koe.html(txt);
		}
       }
    });
}


function mini_search_ajax_close(a){
    a = $(a);
    a.fadeOut(200);
    a.html('');
}

function showLanguageDialog() {
    $("#lang-container").dialog({
        autoOpen: true,
        modal: true,
        width: '320px',
        height: '0',
        resizable: false,
        draggable: false,
        position: {
            my: "center",
            at: "center",
            of: window
        },
        buttons: {
            "Български": {
                click: function () {
                    language_set(32);
                },
                text: 'Български',
                class: 'btn-lang'
            },
            "English": {
                click: function () {
                    language_set(37);
                },
                text: 'English',
                class: 'btn-lang'
            }
        },
        open: function (event, ui) {
            var dialog = this;
            $('body').addClass('stop-scrolling');
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            $(".ui-dialog .ui-dialog-buttonpane").addClass("lang-buttonpane");
            $('.ui-widget-overlay').bind('click', function() {
                $(dialog).dialog('close');
            });
        },
        close: function () {
            $(this).dialog("destroy");
            $('body').removeClass('stop-scrolling');
            $(".ui-dialog .ui-dialog-buttonpane").removeClass("lang-buttonpane");
        }
    });
}
