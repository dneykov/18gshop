<?php

if($_POST){
    if(isset($_POST['content'])&&($_POST['content']!='Message')){
        $content=trim($_POST['content']);
    }else{
        $content='';
    }
    if(isset($_POST['name'])&&($_POST['name']!='First Name')){
        $name=trim($_POST['name']);
    }else{
        $name='';
    }
    if(isset($_POST['email'])&&($_POST['email']!='Email or Phone')&&(check_email_address($_POST['email']))){
        $email=trim($_POST['email']);
    }else{
        $email='';
    }
    if(empty($content)) $err[]='Message';
    if(empty($name)) $err[]='First Name';
    if(empty($email)) $err[]='E-mail';
    if($err===NULL){
        $msg="Запитване от ".$name." ".$email."<br/>";
        $msg=$msg."Въпрос: ".htmlspecialchars($_POST['content']);


        $mail = new PHPMailer();
        $mail->Host = "localhost";  // specify main and backup server
        $mail->CharSet="utf-8";

        $mail->From = mail_office;
        $mail->FromName = $name;
        $mail->AddAddress(mail_office);
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        $mail->IsHTML(TRUE);                                  // set email format to HTML

        $mail->Subject = "Запитване от ".$name;
        $mail->Body    = $msg;


        $sendRez=$mail->Send();
    }
}

?>
<div id="info_tab"><p>question</p></div>
<div style="clear: both; height: 10px;"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#mailform input[name=name]').click(function() {
            $(this).val('');
        });
        $('#mailform input[name=email]').click(function() {
            $(this).val('');
        });
        $('#mailform textarea').click(function() {
            $(this).text('');
        });
    });
</script>
<form id="mailform" name="mailform" action="" method="post">
    <div class="textinputdiv">
        <input class="textinput" id="contname" name="name" type="text" <?php if(isset($_POST['name'])){ echo 'value="'.$_POST['name'].'"';} else {if(isset($__user)){ echo 'value="'.$__user->getName().' '.$__user->getName_last().'"';}} ?>>
    </div>
    <div class="textinputdiv">
        <input class="textinput" id="contmail" name="email" type="text" <?php if(isset($_POST['email'])){ echo 'value="'.$_POST['email'].'"';} else{ if(isset($__user)){ echo 'value="'.$__user->getMail().'"';}}?>>
    </div>
    <div class="textareainputdiv">
        <textarea class="textareainput" name="content" id="conttext" style="width: 90%; color: #777;"><?php if(isset($_POST['content'])) { echo $_POST['content'];}?></textarea>
    </div>
    <div id="button">
        <input class="button" id="register" style="border: none; color: #FFF;" type="submit" name="submit" value="Send" class="submit">
    </div>
</form>
<div style="clear: both;"></div>
<?php
if((isset($sendRez))&&($sendRez)){
    echo '<center>Question was sent successfully<br /.</center>';
}
if((isset($err))&&(sizeof($err)>0)){
    $t='';
    foreach($err as $e){
        $t.=', '.$e ;
    }
    $t=trim($t,', ');
    echo '<center>Please Enter '.$t.'</center>';
}
?>
<div style="clear: both; height: 20px;"></div>
<div style="clear: both; height: 20px;"></div>