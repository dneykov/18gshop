<style type="text/css">
    .f-icon {
        float: left;
        width: 26px;
        height: 26px;
        margin-left: 5px;
        display: inline-block;
    }

    .f-icon img {
        width: 26px;
        height: 26px;
    }

    .f-label {
        float: left;
        display: inline-block;
        height: 26px;
        text-align: top;
        text-transform: uppercase;
        font-size: 18px !important;
        font-family: Verdana !important;
        margin-bottom: 3px;
        color: #333;
    }

    .label {
        font-size: 16px !important;
        text-transform: unset;
        cursor: pointer;
    }

    #infos {
        background-color: #333;
        padding-top: 1px;
    }

    .info_category_box {
        color: #666;
        padding-left: 5px;
        display: block;
        padding-bottom: 7px;
        font-size: 23px !important;
        text-transform: lowercase;
    }

    .info_category_box .label {
        font-size: 23px !important;
        text-transform: lowercase;
    }

    .cc-btn {
        background-color: #FF671F !important;
        border-radius: 0px !important;
        color: #fff !important;
        height: 19px !important;
        padding-bottom: 8px !important;
        padding-top: 4px !important;
    }

    .cc-window {
        border-radius: 0px !important;
    }

    @media screen and (max-width: 414px) and (orientation: portrait), screen and (max-width: 1024px) and (orientation: landscape) {
        .cc-window .cc-message {
            text-align: center !important;
        }

        .cc-window.cc-banner .cc-compliance {
            width: 100%;
            -ms-flex: 1;
            flex: 1;
        }
    }
</style>
<div style="clear:both; height: 20px;"></div>
<div id="infos">
    <div style="clear: both"></div>
    <div style="margin-top: 10px;">
        <?php
        $stm = $pdo->prepare("SELECT * FROM `help_groups` ORDER BY `id` ASC");
        $stm->execute();
        foreach ($stm->fetchAll() as $v) {
            $temp = $pdo->prepare("SELECT * FROM `help_articles` WHERE `cat_id` = ?");
            $temp->bindValue(1, (int)$v['id'], PDO::PARAM_INT);
            $temp->execute();
            if ($temp->rowCount() > 0) {
                echo '<a class="info_category_box" href="' . urlm . '/index.php?help&c=' . $v['id'] . '" ';
                if (isset($_GET['c']) && ($_GET['c'] == $v['id'])) {
                    echo "style='color: #FF671F'";
                }
                echo "><div class='label'>" . $v['name_en'] . "</div></a>";
            }
        }
        ?>
        <a class="info_category_box" href="index.php?howtobuy">
            <div class='label'>how to buy</div>
        </a>
    </div>
    <div id="footer-newsletter">
        <div class="f-label"><a href="index.php?contacts"
                                style="color:#c9c9c9;display: block; font-size: 18px; padding-top: 3px;">newsletter</a>
        </div>
        <div style="clear: both; height: 6px;"></div>
        <p>First to know about our new products and promotions. Subscribe to our newsletter!</p>

        <form action="" method="post">
            <input type="text" name="email" placeholder="e-mail" id="email"/>
            <input type="submit" name="submit" value="subscribe me" id="submit"/>
        </form>
        <div class="response">

        </div>
    </div>
</div>
<div id="lang-container"></div>
<footer>
    <span style="margin-top: 5px; display: block; color: #FFF;">18gshop © 2013 <a href="http://bigcoin.com/dev/"
                                                                                  style="display: inline-block; margin-left: 10px;color: #999; text-decoration: none; font-size: 13px; line-height: 13px;"
                                                                                  target="_blank">Created by WebMaster</a></span>
</footer>

<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.1/cookieconsent.min.js"></script>
<script type="text/javascript">
    $(function() {
        $(".searchdiv #text").focus(function () {
            $(window).scrollTop(0);
        });
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() > 38) {
            $('.searchdiv').css('position', 'fixed');
            $('.searchdiv').css('top', '0px');
            $(".big-banners").css('margin-top', '40px');
        } else {
            $('.searchdiv').css({'position': '', 'top': ''});
            $(".big-banners").css('margin-top', '0px');
        }
    }); //scroll
</script>
<script>window.cookieconsent.initialise({
        "palette": {
            "popup": {"background": "#333333"},
            "button": {"background": "#f1d600"}
        },
        "showLink": false,
        "theme": "classic",
        "content": {
            "message": "We are using cookies to give you the best experience on our site. <a href='index.php?privacy' target='_blank' style='color: #FF671F;'>Read more</a>",
            "dismiss": "Accept!"
        }
    });</script>
</body>
</html>
