<?php				
	require_once dir_root_template."_top.php";
	
	foreach($artikuli as $artikul){
		$categoryname = $artikul->getIme_vid();
	}
	echo '<h2 id="breadcrumb">'.$categoryname.'</h2>';

	foreach($artikuli as $artikul){
		echo '<article class="product_details">';
			echo '<img class="thumbnail" src="'.$artikul->getKatinka().'" />';
			echo '<div class="both"></div>';
			echo '<p>'.$artikul->getDescription().'</p>';
		echo '</article>';
	}

	require dir_root_template."footer_menu.php";
?>