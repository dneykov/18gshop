<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="<?php echo url_template_folder; ?>css/css3.css" type="text/css" >
    <script type="text/javascript" src="<?php echo url; ?>js/jquery-1.6.3.min.js"></script>
    <style type="text/css">
        #main_menu li {
            display: block;
            list-style: none outside none;
            height: 25px;
        }
        #main_menu li a {
            width: 100%;
            text-align: left;
            padding-left: 10px;
        }
        h3{
            color:#4d4d4d;
            font-size:16pt;
            margin:10px 10px 0;
            font-weight: normal;
            text-align:left;
        }
        .title a{
            color:#4d4d4d;
            font-weight: bold;
            font-size:12pt;
        }
        input, textarea {
            font-size: 13px;
        }
        input:hover, input:focus {
            border: 1px solid #FF671F;
        }
        .branch-item .name a {
            color: #333;
        }
        .branch-item .name a:hover {
            color: #FF671F;
        }
        .main span {
            display: block;
        }
        .main span a {
            color: #0071b9;
        }
    </style>
</head>
<body>
<ul id="main_menu">
    <li><a href="<?php echo url.'pricelist.php';?>" class="active">Ценови листи</a></li>
    <div style="height:3px;width: 100%;background: #FF671F;"></div>
</ul>
<div style="height:3px;color:#fcc900;background:#fcc900;"></div>
<div class="main" style="margin-top: 40px;">
    <?php if($storage_files) foreach ($storage_files as $file) { ?>
        <span style="padding-bottom: 5px; padding-left:10px;max-width: 500px; padding-right:10px;"><a href="<?php echo url . "storage/" . $file; ?>" target="_blank"><?php echo $file; ?></a></span>
    <?php } ?>
</div>
<div class="clear"></div>
</body>
</html>