<script type="text/javascript">
    jQuery(document).ready(function($) {
        if($("input[name=account_type]:checked").val() == '0') {
            $("#legal-form").hide();
            $('#legal-form :input').prop("disabled", true);
        } else if($("input[name=account_type]:checked").val() == '1') {
            $('#legal-form :input').prop("disabled", false);
            $("#legal-form").show();
        }
        $("input[name=account_type]").change(function () {
            if($(this).val() == '0') {
                $("#legal-form").hide();
                $('#legal-form :input').prop("disabled", true);
            } else {
                $('#legal-form :input').prop("disabled", false);
                $("#legal-form").show();
            }
        });
    });
</script>
<div id="content" style="padding-left: 10px; padding-right: 15px;">
	<p style="font-size: 16px;">Регистрацията е необходима, за да може бързо да обработим вашата поръчка</p>
	<p style="font-size: 16px; margin-bottom: -5px;">Ако вече сте наш клиент, моля натиснете <a style="color: #FF671F;" href="fancy_login_m.php?p=login"><strong>тук</strong></a></p>
	<form action="fancy_login_m.php?p=registation" method="post" style="margin:25px 10px 0;">
        <div class="top-row">
            <input type="radio" id="individual" style="width: auto;" name="account_type" value="0" checked <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "0") echo "checked"; ?> />
            <label for="individual">Физическо лице</label>
            <span class="radio-divider"></span>
            <input type="radio" id="legal_entity" style="width: auto;" name="account_type" value="1" <?php if(isset($_POST['account_type']) && $_POST['account_type'] == "1") echo "checked"; ?> />
            <label for="legal_entity">Юридическо лице</label>
        </div>
		<input type="text" autocomplete="off" name="register_name" value="<?php if(isset($_POST['register_name'])) echo $_POST['register_name']; ?>" id="regfirstname" class="reg-field-left"/>
		<input type="text" autocomplete="off" name="register_name_last" value="<?php if(isset($_POST['register_name_last'])) echo $_POST['register_name_last']; ?>" id="reglastname"  class="reg-field-right"/>
		<input type="password" autocomplete="off" name="register_pass" value="<?php if(isset($_POST['register_pass'])) echo $_POST['register_pass']; ?>" id="regpassword"  class="reg-field-left">
		<input type="password" autocomplete="off" name="register_passAgain" value="<?php if(isset($_POST['register_passAgain'])) echo $_POST['register_passAgain']; ?>" id="regpasswordagain" class="reg-field-right">
		<input type="text" autocomplete="off" name="register_mail" value="<?php if(isset($_POST['register_mail'])) echo $_POST['register_mail']; ?>" id="regemail"  class="reg-field-left"/>
		<select id="country" name="country" class="reg-field-right" style="padding: 3px !important;">
			<?php
				$stm = $pdo -> prepare("SELECT * FROM `locations`");
				$stm -> execute();
				foreach ($stm -> fetchAll() as $c) {
					echo "<option value='".$c['id']."'>".$c['country_name']."</option>";
				}
			?>
		</select>
		<input type="text" autocomplete="off" name="gsm" value="<?php if(isset($_POST['gsm'])) echo $_POST['gsm']; ?>" id="reggsm" class="reg-field-left"/>
		<input type="hidden" autocomplete="off" name="telephone" value="<?php if(isset($_POST['telephone'])) echo $_POST['telephone']; ?>" id="regphone" class="reg-field-right"/>
		<input type="text" autocomplete="off" name="city" value="<?php if(isset($_POST['city'])) echo $_POST['city']; ?>" id="regtown" class="reg-field-right"/>
		<input type="text" autocomplete="off" name="street" value="<?php if(isset($_POST['street'])) echo $_POST['street']; ?>" id="regstreet"class="reg-field-street"/>
		<input type="text" autocomplete="off" name="number" value="<?php if(isset($_POST['number'])) echo $_POST['number']; ?>" id="regstreetnumber" class="reg-field-number"/>
		<input type="text" autocomplete="off" name="vhod" value="<?php if(isset($_POST['vhod'])) echo $_POST['vhod']; ?>" id="regentrance" class="reg-field-three" style="margin-left: 0% !important;"/>
		<input type="text" autocomplete="off" name="etaj" value="<?php if(isset($_POST['etaj'])) echo $_POST['etaj']; ?>" id="regfloor" class="reg-field-three" style="float: left;"/>
		<input type="text" autocomplete="off" name="apartament" value="<?php if(isset($_POST['apartament'])) echo $_POST['apartament']; ?>" id="regflat" class="reg-field-three"/>
        <div style="clear: both;"></div>
        <div id="legal-form" style="margin-top: 40px;">
            <input type="text" autocomplete="off" name="company_name" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name']; ?>" id="company_name" class="reg-field-left"/>
            <input type="text" autocomplete="off" name="company_mol" value="<?php if(isset($_POST['company_mol'])) echo $_POST['company_mol']; ?>" id="company_mol"  class="reg-field-right"/>
            <input type="text" autocomplete="off" name="company_eik" value="<?php if(isset($_POST['company_eik'])) echo $_POST['company_eik']; ?>" id="company_eik"  class="reg-field-left">
            <input type="text" autocomplete="off" name="company_vat" value="<?php if(isset($_POST['company_vat'])) echo $_POST['company_vat']; ?>" id="company_vat"  class="reg-field-right">
            <input type="text" autocomplete="off" name="company_city" value="<?php if(isset($_POST['company_city'])) echo $_POST['company_city']; ?>" id="company_city"  class="reg-field-left"/>
            <input type="text" autocomplete="off" name="company_phone" value="<?php if(isset($_POST['company_phone'])) echo $_POST['company_phone']; ?>" id="company_phone" class="reg-field-right"/>
            <input type="text" autocomplete="off" name="company_street" value="<?php if(isset($_POST['company_street'])) echo $_POST['company_street']; ?>" id="company_street" class="reg-field-left">
            <input type="text" autocomplete="off" name="company_number" value="<?php if(isset($_POST['company_number'])) echo $_POST['company_number']; ?>" id="company_number" class="reg-field-right">
        </div>

        <div style=" margin-top: 15px;float: left;text-align: center;width: 100%;">
			<div style="margin: 0 auto; text-align: left;"><label for="con" style="font-size: 16px; color:#7c7c7c"><input id="con" name="terms" style="vertical-align: middle; width: 20px; margin-right: 5px; border: 1px solid #CCC;" type="checkbox" <?php if(isset($_POST['terms'])){echo ' checked="yes" ';} ?> >Съгласен съм с <a onclick="javascript:window.open('terms.php','mywin','left=20,top=20,width=100%,toolbar=0,scrollbars=1');return false;" href="terms_mobile.html" class="tgr" target="_blank" style="font-weight: bold;color:#333;font-size: 16px;">условията за ползване</a> на сайта на 18gshop</label></div>
            <div style="margin: 0 auto; text-align: left;"><label for="privacy" style="font-size: 16px; color:#7c7c7c"><input id="privacy" name="privacy" style="vertical-align: middle; width: 20px; margin-right: 5px; border: 1px solid #CCC;" type="checkbox" <?php if(isset($_POST['privacy'])){echo ' checked ';} ?> >Съгласен съм личните ми данни да бъдат <a href="<?php echo url; ?>index.php?privacy" class="tgr" target="_blank" style="font-weight: bold;color:#333;font-size: 16px;">обработени</a> от 18gshop</label></div>
            <div style="margin: 0 auto; text-align: left;"><label for="newsletter_radio" style="font-size: 16px; color:#7c7c7c"><input id="newsletter_radio" name="newsletter" style="vertical-align: middle; width: 20px; margin-right: 5px; border: 1px solid #CCC;" type="checkbox" <?php if(isset($_POST['newsletter'])){echo ' checked="yes" ';} ?> >Желая да получавам известия за новини, промоции и нови продукти</label></div>
			<div id="button">
				<input class="button" id="register" style="border: none; color: #FFF;" type="submit" name="submit" value="регистрирай ме" class="submit">
			</div>
		</div>
		<div style="clear: both; height: 10px;"></div>
		<p style="color: red; text-align: left;">* задължителни полета</p>
		<div style="clear: both; height: 50px;"></div>
	</form>
	<?php
		$tmp_msg='';
		if((isset($user_register_error))&&($user_register_error)) foreach ($user_register_error as $v) {
			$tmp_msg= $tmp_msg.', '.$v;
		}

        if((isset($order_address_data_error))&&($order_address_data_error)) foreach ($order_address_data_error as $v) {
			$tmp_msg= $tmp_msg.', '.$v;
		}

		$tmp_msg=trim($tmp_msg,', ');
		if(!empty($tmp_msg)){
			echo '<div class="error">';
				echo $tmp_msg;
			echo '</div>';
		}
	?>
</div>
