<?php
$group = new paket((int)$_GET['gid']);
$online = true;

foreach ($group->getProducts() as $product) {
    $artikul = new artikul((int)$product);

    if ($artikul->isAvaliable() === false) $online = false;
}

$upd = $pdo->prepare('UPDATE `group_products` SET `online` = ? WHERE `id` = ?');
$upd -> bindValue(1, (int)$online, PDO::PARAM_STR);
$upd -> bindValue(2, $group->getId(), PDO::PARAM_INT);
$upd -> execute();

$group = new paket((int)$_GET['gid']);
$isAvaliable = (bool)$group->getOnline();
?>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="<?php echo url; ?>js/jquery.serialScroll-1.2.1.js"></script>
<div id="where_am_i_box" style="border-bottom: 1px solid #CCC;">
    <div id="where_am_i">
        <?php
        echo '<a onclick="history.back();">';
        echo '<img src="' . url_template_folder . 'images/back.png" alt="back" style="float: left; margin-top: 3px; margin-left: 5px; margin-right: 10px;">';
        echo '</a>';
        echo '<span style="margin-top: 3px; display: inline-block;">пакетни оферти</span>';
        ?>
    </div>
</div>
<div id="item_name">
    <div style="font-size: 13px; color: #999999; font-weight: normal; margin-bottom: 3px;">
        <div style="display: inline-block; color:#999;font-size:10px;font-weight: normal">
            #B<?php echo $group->getId(); ?></div>
    </div>
    <?php echo $group->getName(); ?>
</div>
<div style="clear:both; margin-top:10px;">
    <div id="proddivdetail">
        <script type="text/javascript">
            var addToCartRules = [];

            function validate(rules) {
                for (x in rules) {
                    if ($("#" + rules[x][0]).val() == null || $("#" + rules[x][0]).val() == "") {
                        alert('Моля изберете ' + rules[x][2] + ' на ' + rules[x][1]);
                        return false;
                    }
                }
                return true;
            }

            function updatePriceBySize(el) {
                var gID = $("input[name=addToCard]").val();
                $.ajax({
                    type: "POST",
                    url: "ajax/get_price_by_size_group_m.php",
                    data: "gid=" + gID + "&" + $('.price_tag').serialize(),
                    cache: false,
                    success: function (html) {
                        var result = jQuery.parseJSON(html);
                        $(".price-box").html(result.price);
                        $(".delivery-box").html(result.delivery);
                    }
                });
            }

            $(document).ready(function () {
//                $('#addtocart').ajaxForm({
//                    success: function(responseText){
//                        $.fancybox({
//                            'type'			: 'iframe',
//                            'href' : '<?php //echo url ?>///fancy_login.php?p=cart',
//                            'padding'		: 0,
//                            'width'			: 800,
//                            'height'		: 800
//                        });
//                        $('#main_menu a.cart').load('<?php //echo url ?>///template/bg/top_menu_shopping_cart.php');
//                    }
//                });
                $('.questionsBar div:last').css('border', 'none');
                var discriptionh = $('.product_description').height();
                if (discriptionh > 170) {
                    $('.product_description').css({
                        position: 'relative',
                        height: '170px',
                        overflow: 'hidden'
                    });
                    $('.showmoredis').css({
                        display: 'block'
                    });
                }
                $('.showmoredis').click(function normalhdis() {
                    $('.product_description').css({
                        height: discriptionh + 25 + 'px',
                    });
                    $('.hidemoredis').css({
                        display: 'block'
                    });
                    $('.showoredis').css({
                        display: 'none'
                    });
                })
                $('.hidemoredis').click(function normalhdis() {
                    $('.product_description').css({
                        height: '170px',
                    });
                    $('.hidemoredis').css({
                        display: 'none'
                    });
                    $('.showoredis').css({
                        display: 'block'
                    });
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                })
            });

            function movetocenter(url, img, n) {
                image = new Image();
                image.src = img;
                image.onload = function () {
                    var width = image.width;
                    var nowImg = $('#nowcenterimg').val();
                    var nowy = $('#img' + nowImg).height();
                    var nowx = $('#img' + nowImg).width();
                    var ny = $('#img' + n).height();
                    var nx = $('#img' + n).width();
                    if (width > 320) {
                        var ImgClass = "class='item_img'";
                    } else {
                        var ImgClass = "class='item_img_small'";
                    }
                    var text = '<a id="proddetailscenterimg" class="product_a_image" href="' + url + '"><img ' + ImgClass + ' style="max-width: ' + width + 'px; min-width: 50px;" src="' + img + '" /></a>';
                    $("#proddetailscenterimg").replaceWith(text);
                    $('#img' + n).css({
                        border: '1px solid #FF671F'
                    });
                    $('#img' + nowImg).css({
                        border: '1px solid #FFFFFF'
                    });
                    $('#nowcenterimg').val(n);
                };
            }
        </script>
        <style>
            #proddetailscenterimg a {
                cursor: url(<?php echo url_template_folder; ?>images/spyglass_z.cur);
            }
        </style>
        <input type="hidden" name="nowcenterimg" id="nowcenterimg" value="big">
        <div class="proddetailimage">
            <?php
            $filename = $group->getKatinka();
            $width = 0;
            if (file_exists($filename)) {
                $size = getimagesize($group->getKatinka());
                $width = $size[0];
            }

            $uwsidth = $width;

            $artikuli = $group->getProducts();
            // foreach($artikuli as $a){
            //     $artikul = new artikul((int)$a);
            //     $art_images = $artikul->getKatinka_dopalnitelni(false);
            //     if(isset($art_images)){
            //         for( $i=1;$i<=12;$i++){
            //             $image=$art_images[$i];
            //             if(!is_null ($image)&&file_exists($image->getKatinka())){
            //                 $size = getimagesize($image->getKatinka());
            //                 $width = $size[0];
            //                 $uwsidth+=$width +10;
            //             }
            //         }
            //     }
            // }

            $img = $group->getKatinka();
            list($width, $height, $type, $attr) = getimagesize($img);
            if ($width > 320) {
                $ImgClass = "class='item_img'";
            } else {
                $ImgClass = "class='item_img_small'";
            }

            echo '<div id="proddetaildopimgbox" style="margin-right: 40px;">';
            echo '<div style="display:none;border: 1px solid #FF671F; margin-bottom: 20px;" id="imgbig"><a onclick="movetocenter(\'' . url_template_folder . 'group_look_image.php?id=' . $group->getId() . '&image=' . $group->getKartinka_b() . '&img=big\', \'' . $group->getKatinka() . '\', \'big\');return false;" href="#"><img class="prodimgtumb" style="max-width: 95px; margin-bottom: 0;" src="' . $group->getKartinka_t() . '"/></a></div>';
            $n = 1;

            echo '</div>';
            echo '<div id="proddetailbigimgbox" style="min-width: 50px;">';
            ?>
            <center><img alt="<?php echo $group->getName(); ?>"
                         src="<?php echo $group->getKatinka(); ?>" <?php echo $ImgClass; ?> ></center>

            <script type="text/javascript">
                function moreImg() {
                    if ($("#item_more_img_box").is(":hidden")) {
                        $("#more_img_link").html('скрий снимките');
                        $("#item_more_img_box").slideDown("fast");
                    } else {
                        $("#more_img_link").html('още снимки');
                        $("#item_more_img_box").hide();
                        $("html, body").animate({scrollTop: 0}, "fast");
                    }
                };
            </script>
            <?php
            echo '<div id="item_more_img_box">';
            foreach ($artikuli as $a) {
                $artikul = new artikul((int)$a);
                echo '<img class="item_img" src="' . $artikul->getKartinka_b() . '"/>';
            }
            echo '</div>';

            if (count($artikuli)) {
                echo '<div id="item_more_img_link"><a style="color: #888;" href="#" id="more_img_link" onclick="return false;" onmousedown="javascript: moreImg();">още снимки</a></div>';
            }
            ?>
            <div style="clear: both; height: 10px;"></div>
        </div>
    </div>
    <div id="item_box" style="margin-top: 30px; padding-top: 5px; padding-bottom: 20px;">
        <h3 class="prodinfoname">
            <style>
                .balloon {
                    display: inline-block;
                    border: 1px solid rgb(207, 207, 207);
                    margin-right: 4px;
                    width: 84px;
                    height: 63px;
                    background-color: #FFF;
                }

                .balloon:hover {
                    border: 1px solid #FF671F;
                }
            </style>
            <?php
            //            foreach($artikuli as $dart) {
            //                $dr = new artikul((int)$dart);
            //                echo "<div class='balloon'><a style='font-size: 13px;' href='".url."index.php?id=".$dr->getId()."' target='_blank'><table style='border-collapse:collapse;'><tr><td style='width:84px;height:63px;text-align:center;'><img src='".$dr->getKartinka_t()."' style='margin:0 auto; display: block; max-width: 84px; max-height: 63px;' alt='".$dr->getIme_marka()." ".$dr->getIme()."' title='".$dr->getIme_marka()." ".$dr->getIme()."'/></td></tr></table></a></div>";
            //            }
            //            echo '<div style="clear: both;"></div>';
            ?>
        </h3>
        <?php
        //        echo '<div class="questionsBar">';
        //        echo '<div style="padding-left:0px;"><a style="font-size: 13px;" class="thickbox" href="'.url.'fancy_qbs_group.php?p=howtobuy&id='.$group->getId().'">как<br />да поръчам?</a></div>';
        //        echo '<div><a style="font-size: 13px;" class="thickbox" href="'.url.'fancy_qbs_group.php'.'?id='.$group->getId().'&p=sendquestion">въпрос<br />за продукта</a></div>';
        //        echo '</div>';
        $t = $group->getPrice();
        ?>
        <form id="addtocart" action="fancy_login_m.php?p=cart&id=<?php echo $artikul->getID(); ?>" method="post"
              enctype="multipart/form-data">
            <input type="hidden" name="addToCard" value="<?php echo $group->getId(); ?>"/>
            <input type="hidden" name="type" value="group"/>
            <input name="counts" value="1" type="hidden"/>
            <div class="item_options">
                <?php
                $nn = 1;
                foreach ($artikuli as $dart) {
                    $dr = new artikul((int)$dart);
                    if ($dr->isAvaliable()) {
                        $add_artikul_etiket_grup = NULL;
                        $stm = $pdo->prepare('SELECT * FROM `etiketi_zapisi` WHERE `id_artikul` = ? ORDER BY `id` ');
                        $stm->bindValue(1, $dr->getId(), PDO::PARAM_INT);
                        $stm->execute();
                        foreach ($stm->fetchAll() as $v) {
                            $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id` = ? ORDER BY `id` ');
                            $stm->bindValue(1, $v['id_etiket'], PDO::PARAM_INT);
                            $stm->execute();
                            $temp = new etiket($stm->fetch());
                            $temp_grupa = new etiket_grupa((int)$temp->getId_etiketi_grupa());
                            $add_artikul_etiket_grup[$temp->getId_etiketi_grupa()] = $temp_grupa;
                            unset($temp);
                            unset($temp_grupa);
                        }
                        if (isset($add_artikul_etiket_grup) && (sizeof($add_artikul_etiket_grup) > 0)) {
                            foreach ($add_artikul_etiket_grup as $v) {
                                if ($v->getSelect_menu()) {
                                    if ($nn == 1) {
                                        //избери размер
                                        echo '<div style="font-size: 13px; margin-top: 15px; margin-bottom: 10px; color: #808080;"><a href="index.php?id=' . $dr->getId() . '" target="_blank" title="' . $dr->getTitle() . '"><img style="width: 90px;" src="' . $dr->getKartinka_t() . '" /></a><span style=" position: relative; top: -5px; left: 15px;">' . $v->getIme() . '</span></div>';
                                    } else {
                                        echo '<div style="font-size: 13px; margin-top: 15px; margin-bottom: 10px; color: #808080;"><a href="index.php?id=' . $dr->getId() . '" target="_blank" title="' . $dr->getTitle() . '"><img style="width: 90px;" src="' . $dr->getKartinka_t() . '" /></a><span style=" position: relative; top: -5px; left: 15px;">' . $v->getIme() . '</span></div>';
                                    }
                                    echo '<div class="item_razmer">';
                                    echo '<input type="hidden" name="option' . $dr->getId() . '" id="option' . $dr->getId() . '"/>';
                                    echo '<input type="hidden" name="price_tag[' . $dr->getId() . ']" class="price_tag" id="price_tag' . $dr->getId() . '"/>';
                                    $etiketii = null;
                                    $pdo = PDOX::vrazka();
                                    $stm = $pdo->prepare('SELECT * FROM `etiketi` WHERE `id_etiketi_grupa` = :idgroupa ORDER BY `ordr`');
                                    $stm->bindValue(":idgroupa", $v->getId(), PDO::PARAM_INT);
                                    $stm->execute();
                                    if ($stm->rowCount() > 0)

                                        foreach ($stm->fetchAll() as $d) {
                                            $temp_etiket = new etiket((int)$d['id']);
                                            $etiketii[] = $temp_etiket;
                                            unset($temp_etiket);
                                        }

                                    if ($etiketii) {
                                        foreach ($etiketii as $e) {
                                            if ($dr->getEtiketizapisi()) if (in_array($e->getId(), $dr->getEtiketi_array_ids())) {
                                                echo '<a id="option_' . $dr->getId() . '_' . $v->getId() . '_' . $e->getId() . '" onclick="javascript:$(\'#option' . $dr->getId() . '\').val(\'' . $e->getIme() . '\');$(this).toggleClass(\'active\',true);$(this).siblings().toggleClass(\'active\',false);$(\'#price_tag' . $dr->getId() . '\').val(\'' . $e->getId() . '\');updatePriceBySize(this);return false;" href="#">' . $e->getIme() . '</a>';
                                            }
                                        }
                                    }
                                    echo '</div>';
                                    echo '
                                                        <script type="text/javascript">
                                                                addToCartRules.push(["option' . $dr->getId() . '","' . $dr->getIme() . '","' . $v->getIme() . '"]);
                                                        </script>
                                                        ';

                                }
                            }
                        }
                        unset($add_artikul_etiket_grup);
                    }
                    unset($dr);
                    $nn++;
                }
                ?>
            </div>
            <div style="clear:both; height: 15px;"></div>
            <div style="text-align: center;width: 175px;margin-top: 12px;float: right;" class="price-box">
                <span class="prodpricenormal" style="float:none;margin-right:10px;"> <span
                            style="color:#424242;font-size:12pt;font-style: italic;"> <?php $tmp_cen = explode(".", number_format($group->getPrice_normal(), 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span
                                style="font-size:10px;">лв.</span></span></span>
                <span class="prodpricepromo"
                      style="font-style: italic; color: #FF671F;"><?php $tmp_cen = explode(".", number_format($group->getPrice(), 2, '.', ''));
                    echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                            style="font-size:10px;">лв.</span></span>
                <?php if (isset($__user) && $__user->is_partner()) : ?>
                    <div style="margin-bottom:5px; width: 175px; text-align: center;margin-top: 5px; margin-right: 25px; font-size:8pt; color: #0071b9;">
                        Краен
                        клиент: <?php $tmp_cen = explode(".", number_format($group->getPrice_reference(), 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup><?php echo $tmp_cen[1]; ?></sup> <span
                                style="font-size:10px;">лв.</span></div>
                <?php endif; ?>
                <?php
                $tmpraz = (($group->getPrice_normal() / CURRENCY_RATE) - ($group->getPrice() / CURRENCY_RATE));
                $tmp_razf = explode(".", number_format($tmpraz, 2, '.', ''));
                ?>
                <div style="width: 175px;text-align: center;">
                    <span style="font-style: normal;font-size: 8pt;color: #FF671F;">спестявате <?php echo lang_currency_prepend; ?> <?php echo $tmp_razf[0]; ?><sup><?php echo $tmp_razf[1]; ?></sup> <?php echo lang_currency_append;?> (-<?php echo round((($tmpraz / ($group->getPrice_normal() / CURRENCY_RATE)) * 100), 0); ?>%)</span>
                </div>
            </div>
            <?php
            if ($isAvaliable) {
                echo '<input onclick="return validate(addToCartRules);" class="btnaddprod" type="submit" value="добави в количката" style="float: right;clear: both;"/>';
                echo '<div style="clear:both;"></div>';
            } else {
                echo '<div style="margin-top: 2px; height: 30px; line-height: 30px; background-color: #333; cursor: default; width: 150px; text-align: center;" class="btnaddprod" >разпродадено</div>';
            }

            $ds = $pdo->prepare("SELECT * FROM `locations` WHERE `id` = 1 LIMIT 1");
            $ds->execute();
            $d = $ds->fetch();
            $dostavka_cena = $d['delivery_price'];
            $dostavka_free = $d['free_delivery'];
            $additional_delivery = 0;

            foreach ($group->getProducts() as $d) {
                $group_article = new artikul((int)$d);
                if ($group_article->isAvaliable()) {
                    $additional_delivery += $group_article->getAdditionalDelivery();
                }
            }

            $ac = 0;
            $ac = $group->getPrice();

            echo '<div class="delivery-box">';

            if ($dostavka_free < $ac) {
                if ($additional_delivery != '' && $additional_delivery > 0) {
                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px;text-align: center;">доставка: ' . $additional_delivery . ' лв.</div>';
                } else {
                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px;text-align: center;">с безплатна доставка</div>';
                }
            } else {
                if ($additional_delivery != '' && $additional_delivery > 0) {
                    $total_delivery = $dostavka_cena + $additional_delivery;
                    echo '<div style="float: right; margin-top: 5px; font-size:8pt; color: #808080;width: 175px;text-align: center;">доставка: ' . $total_delivery . ' лв.</div>';
                } else {
                    echo '<div style="float: right; margin-top: 5px font-size:8pt; color: #808080;width: 175px;text-align: center;">доставка: ' . $dostavka_cena . ' лв.</div>';
                }
            }
            echo '</div>';
            ?>
            <div style="clear:both;"></div>
        </form>
        <div class="product_description" style="margin-top: 20px;">
            <?php echo nl2br($group->getDescription()); ?>
        </div>
    </div>
</div>
<div style="clear:both;"></div>
</div>
<style>
    #p-details-left-col {
        text-align: left;
        display: inline-block;
        vertical-align: top;
        margin-left: 20px;
        width: 160px;
    }

    #p-details-right-col {
        text-align: left;
        width: 1280px;
        margin: 0 auto;
        vertical-align: top;
        padding-bottom: 30px;
    }

    .lastseen {
        width: 1280px;
        margin: 0 auto;
    }
</style>
</center>