<style type="text/css">
	#nav-bar {
		height: 20px;
		font-size: 12px;
		padding-bottom: 20px;
		width: 100%;
	}
	#nav-bar a{
		font-size: 12px;
	}
	.c-article {
		width: 100%;
		padding-bottom: 20px;
		display: none;
	}
	.a-name {
		display: block;
		padding-bottom: 5px;
		padding-top: 5px;
		padding-left: 25px;
		background: url(images/nav/str_i_f.png) 5px 10px no-repeat;
		padding-right: 10px;
		margin-top: 10px;
		border-bottom: 1px solid #CCC;
		color: #333;
	}
	.a-text {
		margin-top: 10px;
		padding-left: 5px;
		padding-right: 5px;
	}
	.a-text a {
		color: #333;
	}
	.a-img {
		width: 100%;
		margin-top: 10px;
	}
	.a-img img {
		width: 100%;
	}
	.catLink{
		display: block;
		height: 28px;
		line-height: 26px;
		padding-left: 10px;
	}
</style>
<script type="text/javascript">
	function toggleInfo(x) {
		if($('.c-article').is(':visible')) {
			$('.c-article').slideUp("fast");
			$('.a-name').css({
				'background': 'url(images/nav/str_i_f.png) 5px 10px no-repeat',
				'color': '#333333'
			});
		}
		setTimeout(function(){
			if($('#a-'+x).is(":hidden")) {
	        $('#a-'+x).slideDown("fast");
	       	$('#a-link-'+x).css({
	       		'background': 'url(images/nav/str_i_down.png) 5px 10px no-repeat',
	       		'color': '#FF671F'
	       	});
	       	$('html, body').animate({ scrollTop: ($('#a-'+x).offset().top - 30) }, 'slow');
		    } else {
		        $('#a-'+x).hide();
		        $('#a-link-'+x).css('background', 'url(images/nav/str_i_f.png) 5px 10px no-repeat');
		        $('#a-link-'+x).css('color', '#333333');
		    }
		}, 200);
	}; 
</script>
<div style="clear both; height: 20px;"></div>
<div class="f-label" style="padding-top: 2px; padding-left: 5px;">информация</div>
<div id="js-t" style="clear: both"></div>
<?php
	if(isset($_GET['c']) && !isset($_GET['t'])){
        $articles = $pdo->prepare("SELECT * FROM `help_articles` WHERE `cat_id`=? ORDER BY `id` ASC");
		$articles->bindValue(1, (int)$_GET['c'], PDO::PARAM_INT);
        $articles->execute();
        if($articles->rowCount() == 1) {
            ?>
            <style>
                .c-article {
                    display: block;
                }
                .a-name {
                    background: url(images/nav/str_i_down.png) 5px 10px no-repeat;
                    color: #FF671F;
                }
            </style>
        <?php
        }
        foreach($articles->fetchAll() as $c){
        	$url = $c['media'];
        	$type = $c['type'];

        	echo "<a href='#' onclick='toggleInfo(\"".$c['id']."\")'><div class='a-name' id='a-link-".$c['id']."'>".$c['title_bg']."</div></a>";
        	echo "<div class='c-article' id='a-".$c['id']."'>";
        	if($type == "photo" && $url != ""){
        		$size = getimagesize($url);
                $width = $size[0];
        		echo "<div class='a-img'><img src='".url.$url."'style='max-width: ".$width."px;'></div>";
        	} else if($type == "video" && $url != ""){
        		$src = str_replace('watch?v=','embed/',$url);
                echo '<iframe style="width:100%;height:290px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
            }
        	echo "<div class='a-text'>".$c['text_bg']."</div>";
        	echo "</div>";
        }
	} else if(isset($_GET['c']) && isset($_GET['t'])){
        $articles = $pdo->prepare("SELECT * FROM `help_tags_zapisi` WHERE `tag_id`=? ORDER BY `id` ASC");
		$articles->bindValue(1, (int)$_GET['t'], PDO::PARAM_INT);
        $articles->execute();
        if($articles->rowCount() == 1) {
            ?>
            <style>
                .c-article {
                    display: block;
                }
                .a-name {
                    background: url(images/nav/str_i_down.png) 5px 10px no-repeat;
                    color: #FF671F;
                }
            </style>
        <?php
        }
        foreach($articles->fetchAll() as $c){

        	$art = $pdo->prepare("SELECT * FROM `help_articles` WHERE `id`=? LIMIT 1");
			$art->bindValue(1, (int)$c['article_id'], PDO::PARAM_INT);
            $art->execute();
            $a = $art->fetch();

        	$url = $a['media'];
        	$type = $a['type'];

        	echo "<a href='#' onclick='toggleInfo(\"".$a['id']."\")'><div class='a-name' id='a-link-".$c['id']."'>".$a['title_bg']."</div></a>";
        	echo "<div class='c-article' id='a-".$a['id']."'>";
        	if($type == "photo" && $url != ""){
        		//$src = preg_replace('|^(.+)\.(.+)$|is', '$1_t.$2', $url );
        		$size = getimagesize($url);
                $width = $size[0];
        		echo "<div class='a-img'><img src='".url.$url."' style='max-width: ".$width."px;'></div>";
        	} else if($type == "video" && $url != ""){
        		$src = str_replace('watch?v=','embed/',$url);
                echo '<iframe style="width:100%;height:290px;margin:10px auto; display:block;" src="'.$src.'" frameborder="0"></iframe>';
            }
        	echo "<div class='a-text'>".$a['text_bg']."</div>";
        	echo "</div>";

        }
	}
?>