<?php
function print_packet($v)
{
    ?>
    <div class='articul'>
        <div class="promo">
            <div style="background-color: #FF671F; width: 16px; height: 40px; color: #fff;">
                <span class="rotate-90"><?php echo "-".round($v->getPercent(),0)."%"; ?></span>
            </div>
        </div>
        <div class='articul_img'>
            <table class="imageCenter">
                <tbody>
                <tr>
                    <td><a href="index.php?gid=<?php echo $v->getId() ?>"><img alt=""
                                                                               src="<?php echo url . $v->getKartinka_t(); ?>"
                                                                               border="0"/></a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class='articul_info'>
            <div class='articul_info_name' style="min-height: 75px;"><?php echo $v->getName(); ?></div>
            <div class="prodpricebox">
                <span class="articul_info_price"
                      style="font-size:12pt;font-style: italic;color:#fe1916;"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getPrice() / CURRENCY_RATE, 2, '.', ''));
                    echo $tmp_cen[0]; ?> <sup style="font-size:12px"><?php echo $tmp_cen[1]; ?></sup><span
                            style="font-size:10px;"><?php echo lang_currency_append; ?></span></span>
                <span class="articul_info_price" style="margin-right: 2px;"> <span
                            style="color:#424242;font-size:10pt;font-style: italic;  text-decoration: line-through"><?php echo lang_currency_prepend; ?> <?php $tmp_cen = explode(".", number_format($v->getPrice_normal() / CURRENCY_RATE, 2, '.', ''));
                        echo $tmp_cen[0]; ?> <sup style="font-size:10pt"><?php echo $tmp_cen[1]; ?></sup><span
                                style="font-size:10px;"><?php echo lang_currency_append; ?></span></span> </span>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>
    <?php
}

?>

<div style="overflow:hidden; min-height: 400px;" class="bundles products">
    <?php
    if ($bundleProducts) {
        echo '<div style="overflow:hidden;" class="all_products">';
        foreach ($bundleProducts as $v) {
            print_packet($v);
        }
        echo '</div>';
    }
    ?>
</div>
